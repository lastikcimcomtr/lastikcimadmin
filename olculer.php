<?php
	set_time_limit(0);
	/*Araç Markaları*/
	function markalar()
	{
		$url = "https://www.wheel-size.com/";
		$res = file_get_contents($url);

		preg_match_all('@<select id="auto_vendor" name="make" class="span4 selectpicker">(.*?)</select>@si', $res, $markalar);
		preg_match_all('@<option value="(.*?)" data-icon="(.*?)">(.*?)</option>@si', $markalar[1][0], $markalar);

		$markalarim = array();
		foreach ($markalar[0] as $key => $value) {
			$markalarim[$markalar[1][$key]] = $markalar[3][$key];
		}
		
		return $markalarim;
	}

	function yillar($marka = NULL)
	{
		$url = "https://www.wheel-size.com/finder/resource/?make=".$marka."&resource=year";
		$data = curl_download($url);
		preg_match_all('@<option value="(.*?)">(.*?)</option>@si', $data, $yillar);
		$yillarim = array();
		foreach ($yillar[0] as $key => $value) {
			$yillarim[$yillar[1][$key]] = $yillar[2][$key];
		}
		return $yillarim;
	}

	function modeller($marka = NULL, $yil = NULL)
	{
		$url 	= "https://www.wheel-size.com/finder/resource/?make=".$marka."&year=".$yil."&resource=model";
		$data 	= curl_download($url);
		preg_match_all('@<option value="(.*?)">(.*?)</option>@si', $data, $modeller);
		$modellerim = array();
		foreach ($modeller[0] as $key => $value) {
			$modellerim[$modeller[1][$key]] = $modeller[2][$key];
		}
		return $modellerim;
	}

	function olculer($marka = NULL, $yil = NULL, $model = NULL)
	{
		$url 	= "https://www.wheel-size.com/finder/search/by_model/?make=".$marka."&year=".$yil."&model=".$model;
		$data 	= curl_download($url);
		preg_match_all('@<tr id="(.*?)" class="(.*?)">(.*?)</tr>@si', $data, $olculer);
		
		$olculerim = array();
		$son_motor = NULL;
		foreach ($olculer[0] as $key => $value) {
			$olcum = array();
			preg_match_all('@<td>(.*?)</td>(.*?)<td data-front="(.*?)" data-rear="(.*?)">(.*?)</td>(.*?)<td data-front="(.*?)" data-rear="(.*?)">(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>@si', $olculer[3][$key], $detaylar);

			$motor = trim(strip_tags($detaylar[1][0]));
			$motor = preg_replace('/\s+/', ' ', $motor);

			if (strlen($motor) == 0) {
				$motor = $son_motor;
			} else {
				$son_motor = $motor;
			}
			
			$olcum['marka'] = $marka;
			$olcum['yil'] = $yil;
			$olcum['model'] = $model;

			$olcum['motor'] = $motor;
			$olcum['front_tire'] 	= trim($detaylar[3][0]);
			$olcum['rear_tire']		= trim($detaylar[4][0]);
			$olcum['front_rim']	= trim($detaylar[7][0]);
			$olcum['rear_rim']	= trim($detaylar[8][0]);
			$olcum['bolt'] 	= trim($detaylar[11][0]);
			$olcum['thd'] 	= trim(strip_tags($detaylar[13][0]));
			$olcum['cb'] 	= trim($detaylar[15][0]);

			$olculerim[] = $olcum;
		}
		return $olculerim;
	}

	function curl_download($Url){
	    // is cURL installed yet?
	    if (!function_exists('curl_init')){
	        die('Sorry cURL is not installed!');
	    }
	    // OK cool - then let's create a new cURL resource handle
	    $ch = curl_init();
	    // Now set some options (most are optional)
	    // Set URL to download
	    curl_setopt($ch, CURLOPT_URL, $Url);
	    // Set a referer
	    curl_setopt($ch, CURLOPT_REFERER, "https://www.wheel-size.com/");
	    // User agent
	    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
	    // Include header in result? (0 = yes, 1 = no)
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    // Should cURL return or print out the data? (true = return, false = print)
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    // Timeout in seconds
	    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

	    /**/
	    $headers = array(
	    	'Accept:*/*',
			'Accept-Language:tr-TR,tr;q=0.9,en-US;q=0.8,en;q=0.7,az;q=0.6,de;q=0.5,id;q=0.4,pt;q=0.3,ro;q=0.2,ru;q=0.1,vi;q=0.1,ar;q=0.1',
			'Connection:keep-alive',
			'Cookie:session_depth=www.wheel-size.com%3D2%7C965385423%3D2; bfp_sn=1511516311899; bafp_eg=undefined; bafp_ce=undefined; _ga=GA1.2.903271169.1511515624; _gid=GA1.2.1576167274.1511515624',
			'Host:www.wheel-size.com',
			'Referer:https://www.wheel-size.com/',
			'User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
			'X-Csrf-Token:MTcT4zLlhyZrwFI3QWbggjVuQ3bg8WT',
			'X-Requested-With:XMLHttpRequest'
	    );
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    /**/

	    // Download the given URL, and return output
	    $output = curl_exec($ch);
	    // Close the cURL resource, and free system resources
	    curl_close($ch);
	    return $output;
	}


	$markalarim = markalar();
	foreach ($markalarim as $key => $value) {

		if ($key == "acura") {

			$yillar 	= yillar($key);
			
			foreach ($yillar as $ykey => $yvalue) {			
				
				$modeller 		= modeller($key, $ykey);

				foreach ($modeller as $mkey => $mvalue) {
					
					$olculer 		= olculer($key, $ykey, $mkey);
					var_dump($olculer);

				}

			}
			
		}


	}

?>
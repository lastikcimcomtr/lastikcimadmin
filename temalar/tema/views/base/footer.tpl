        <![if lt IE 9]>
        <script src="{$theme_url}assets/global/plugins/respond.min.js"></script>
        <script src="{$theme_url}assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->


        <!-- BEGIN CORE PLUGINS -->
            <!-- Head.php Taşıdık -->
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/sweetalert.min.js?v=002" type="text/javascript"></script>
            {*}
        <script>
       /* var chart = AmCharts.makeChart('chartdiv', {
            'type': 'serial',
            'theme': 'light',
            'dataProvider': [
                {
                    'siparis': 'Ocak',
                    'visits': 86                            },
                {
                    'siparis': 'Şubat',
                    'visits': 70                            },
                {
                    'siparis': 'Mart',
                    'visits': 74                            },
                {
                    'siparis': 'Nisan',
                    'visits': 62                            },
                {
                    'siparis': 'Mayıs',
                    'visits': 71                            },
                {
                    'siparis': 'Haziran',
                    'visits': 73                            },
                {
                    'siparis': 'Temmuz',
                    'visits': 60                            },
                {
                    'siparis': 'Ağustos',
                    'visits': 56                            }, 
                {
                    'siparis': 'Eylül',
                    'visits': 64                            }, 
                {
                    'siparis': 'Ekim',
                    'visits': 55                            }, 
                {
                    'siparis': 'Kasım',
                    'visits': 65                            }, 
                {
                    'siparis': 'Aralık',
                    'visits': 70                            }
            ],
            'valueAxes': [{
                    'gridColor': '#FFFFFF',
                    'gridAlpha': 0.2,
                    'dashLength': 0
                }],
            'gridAboveGraphs': true,
            'startDuration': 1,
            'graphs': [{
                    'balloonText': '[[category]]: <b>[[value]]</b>',
                    'fillAlphas': 0.8,
                    'lineAlpha': 0.2,
                    'type': 'column',
                    'valueField': 'visits'
                }],
            'chartCursor': {
                'categoryBalloonEnabled': false,
                'cursorAlpha': 0,
                'zoomable': false
            },
            'categoryField': 'siparis',
            'categoryAxis': {
                'gridPosition': 'start',
                'gridAlpha': 0,
                'tickPosition': 'start',
                'tickLength': 20
            },
            'export': { 'enabled': true }
        });*/
        //# sourceURL=pen.js
        </script> 

        <script>



        //# sourceURL=pen.js
        </script> 
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/ckeditor/ckeditor.js"></script>
        {*}

        <!--
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        -->
        <script src="{$theme_url}assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>


        <!-- Emre Eklenenler -->
        <script src="{$theme_url}assets/global/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>

        <script src="{$theme_url}assets/global/plugins/ddslick/jquery.ddslick.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
        <!-- Emre Eklenenler -->

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{$theme_url}assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{$theme_url}assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{$theme_url}assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="{$theme_url}assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/select2/js/select2.min.js"></script>
        <script type="text/javascript" src="{$theme_url}assets/global/plugins/select2/js/select2.full.min.js"></script>

        <script type="text/javascript" src="{$theme_url}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
        <script src="{$theme_url}assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js"></script>
          <link href="{$theme_url}assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet"/>
          <link href="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs2.css" rel="stylesheet"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/tinymce@5.1.5/tinymce.min.js" referrerpolicy="origin">

        {include file="base/customscripts.tpl"}
    </body>
</html>
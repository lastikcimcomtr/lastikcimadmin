<style type="text/css">
    div.visual {
        height: 80px !important;
    }
</style>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-group fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number" style="font-size:1.3em"> {$yeni_musteri_day} (Gün) / {$yeni_musteri_week} (Hafta) / {$yeni_musteri_total}  (Toplam)</div>
                <div class="desc"> Müşteriler </div>
            </div>
            <a class="more" href="/members/index"> Üye Listesi
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number"> {$total_onay_bekleyen} </div>
                <div class="desc"> Onay Bekleyen Sipariş </div>
            </div>
            <a class="more" href="/orders/index/durumu/1"> Siparişler
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-bar-chart fa-icon-medium"></i>
            </div>
            <div class="details">
			   <div class="number" style="font-size:1.3em"> {$total_siparis_day} (Gün) / {$total_siparis_month} (Ay) / {$total_siparis}  (Toplam)</div>
                <div class="desc">  Siparişler </div>
            </div>
            <a class="more" href="/orders/index"> Toplam Sipariş
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow" style="background-color:#f5861d;">
            <div class="visual">
                <i class="fa fa-try fa-icon-medium"></i>
            </div>
            <div class="details">
                <div class="number"> ₺ </div>
                <div class="desc"> Toplam Satış Tutarı </div>
            </div>
            <a class="more" style="background-color:#dc730f" href="javascript:;"> Toplam Satış Tutarı <i class="m-icon-swapright m-icon-white"></i> </a>
        </div>
    </div>
</div>
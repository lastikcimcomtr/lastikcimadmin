{$module = $this->router->fetch_class()}
{$method = $this->router->fetch_method()}
{$segment = $this->uri->segment(3)}
{$uyemail = $this->extraservices->getMail()}
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Lastikcim Portal</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN LAYOUT FIRST STYLES  -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
    <!-- END LAYOUT FIRST STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="{$theme_url}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{$theme_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/pages/css/pricing.min.css" rel="stylesheet" type="text/css" />

    <link href="{$theme_url}assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css"/>
    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="{$theme_url}assets/global/plugins/select2/css/select2-bootstrap.min.css" />

    <!-- Emre Eklenen -->
    <link href="{$theme_url}assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
    <!-- Emre Eklenen -->

    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{$theme_url}assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{$theme_url}assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->

    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{$theme_url}assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/layouts/layout5/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    <link rel="shortcut icon" href="{$theme_url}favicon.ico" /> 

    <!-- Emre Taşınanlar -->
    <!-- BEGIN CORE PLUGINS -->
    <script src="{$theme_url}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- Emre Taşınanlar -->

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{$theme_url}assets/apps/plugins/sweet/sweetalert.css">
    <script type="text/javascript" src="{$theme_url}assets/apps/plugins/sweet/sweetalert.min.js"></script>
    <!-- Sweet Alert -->
    {literal}
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-PGB68Q8');</script>
      <!-- End Google Tag Manager -->
    {/literal}
    <style type="text/css">
    .dashboard-stat .visual {
        height: 50px;
    }
</style>

<style type="text/css">
.page-order-navi ul li {
    width: 20%;
}
</style>
<style type="text/css">
.page-header .navbar .navbar-nav li.open:hover > a, .page-header .navbar .navbar-nav li.open>a {
  color: #0095DA !important;
}
.page-header .navbar .navbar-nav .dropdown-menu li.active>a, .page-header .navbar .navbar-nav .dropdown-menu>li>a:focus, .page-header .navbar .navbar-nav .dropdown-menu>li>a:hover {
  color: #0095DA !important;
}
.page-header .navbar .navbar-nav> li:hover>a {
  background-color: #0095DA !important;
}
.page-header .navbar .navbar-nav> li.open:hover>a {
  background-color: #f9f9f9 !important;
}
</style>

<style type="text/css">
{literal}
#ortala {
    width:960px; margin:0 auto; padding:0 auto; display:block;
}
#onizleme { 
    position:absolute; border:3px solid #696767; background:#fff; padding:0px; display:none; color:#333; -moz-border-radius:5px;
}
.altlink{
    clear:both;margin-top:250px;
}
.islem_bilgilendirme span#islembilgisitext{
    font-size:11px; margin-right: 5px !important; margin-left:0px !important;
} 
.islem_bilgilendirme span{
    margin-left:0px !important;margin-right:0px !important;text-align:right;
}
.beyaz {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#FFF !important;
}
.durumu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#990066 !important;
}
.anasayfavitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#237BB1 !important;
}
.kampanyaliurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#FFCC00 !important;
}
.kategorivitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#EE8701 !important;
}
.markavitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#000000 !important;
}
.yeniurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#009900 !important;
}
.sponsorurunu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#CC0000 !important;
}
.populerurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#e15656 !important;
}
.gununurunu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#c5b96b !important;
}
.benzerurun {
    background-color:#4D84D4; color:white; height:20px; padding:2px 4px 0px 4px;
}
.hizliduzenle {
    background-color:#4D84D4; color:white; height:20px; padding:0px 4px 0px 4px;
}
.detayliduzenle {
    background-color:#DB53C8; color:white; height:20px; padding:2px 4px 0px 4px;
}
.sil {
    height:20px; padding:0px 5px; background-color:#E73434; color:white;border-radius:10px;
}
.table .btn {
    margin-right: 0px;
}
.bizimbutonlar {
    height: 20px; padding: 0px 5px 0 5px; font-size: 12px;
}

.page-header .navbar .navbar-nav>li>a {
    padding: 20px 25px;
    font-size: 18px;
}
.dropdown-menu {
    min-width: 135px!important;
}
.dropdown-menu .dropdown-menu>li {
    height: 50px;
}
.dropdown-menu .dropdown-menu>li>a {
    padding: 2px;
}
.dropdown-menu .dropdown-menu>li>a {
    padding: 25px!important;
}
.modal-body .col-md-12 {
    white-space: normal;
    line-height: 24px;
}
{/literal}
</style>
</head>
<!-- END HEAD -->	
<body class="page-header-fixed page-sidebar-closed-hide-logo">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGB68Q8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- BEGIN CONTAINER -->
    <div class="wrapper">

        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar mega-menu" role="navigation">
                <div class="container-fluid">
                    <div class="clearfix navbar-fixed-top bg-header relative-mobile" style="background-color: #f9f9f9 !important; border-bottom: 2px solid #0095DA !important;">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Menuu</span>
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- End Toggle Button -->
                        <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a id="index" href="/" class="p-logo">
                                <img src="{$theme_url}assets/logo.png" alt="Logo">
                            </a>
                        </div>
                        <!-- END LOGO -->
                        <!-- END SEARCH -->
                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="topbar-actions">
                             <div class="btn-group-img btn-group">
                                <a href="{site_url('pages/cacheclear')}" class="btn btn-sm dropdown-toggle"  style="background-color: #0095DA !important;width: 35px; margin-right: 7px;padding-top: 9px;" title="Geçici Verileri Sil">
                                   <i class="icon-trash" style="color: #fff !important;    font-size: 15px;"></i>
                                </a>
                            </div>
                            <!-- Begin Basket -->
                            <div class="btn-group-notification noti-clss btn-group" id="header_notification_bar">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: #0095DA !important;" data-hover="dropdown" data-close-others="true" onclick="window.location.assign('/sepet');">
                                    <i class="icon-basket" style="margin:0 0 0 -3px; color: #fff !important;"></i>
                                    <span class="badge basket-count" style="border-color: #0095DA !important;" >0</span>
                                </button>
                                <ul class="dropdown-menu-v2 t-basket">
                                    <li class="external">
                                        <h3>
                                            Sepetinizde <span class="bold basket-count">{count($sepettekiler)}</span> Ürün Var
                                        </h3>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller basket-product" style="height: 5px; padding: 0;" data-handle-color="#637283">
                                            {foreach $sepettekiler as $sepetteki}
                                            <li class="urun">
                                                <a href="javascript:;">
                                                    <span class="details">
                                                        <span class="label label-sm label-icon">
                                                            <img style="width:27px;" src="{$theme_url}assets/layouts/layout5/img/img-2.png">
                                                        </span> 
                                                        {$sepetteki->sto_kod} - {$sepetteki->odp_no}
                                                    </span>
                                                    <span class="time">{$sepetteki->miktar} Adet</span>
                                                </a>
                                            </li>
                                            {/foreach}
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Basket -->
                            <!-- BEGIN USER PROFILE -->
                            <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" style="background-color: #0095DA !important;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span style="color: #fff !important;">Hoşgeldin, CARI ADI</span>
                                </button>
                                <ul class="dropdown-menu-v2 user-btn" role="menu">
                                    <li>
                                        <a href="{site_url('home/profil')}">
                                            <i class="icon-user"></i> BAYI PROFİLİ
                                            <span class="badge badge-danger">1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" style="back">
                                            <i class="icon-envelope-open"></i> Görüş & ÖNERİLER
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{site_url('home/profil')}#sifre">
                                            <i class="icon-lock"></i> ŞİFRE DEĞİŞTİR </a>
                                        </li>
                                        <li>
                                            <a href="{site_url('home/logout')}">
                                                <i class="icon-key"></i> GÜVENLİ ÇIKIŞ </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END USER PROFILE -->
                                </div>
                                <!-- END TOPBAR ACTIONS -->
                            </div>
                            <!-- BEGIN HEADER MENU -->
                            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown dropdown-fw {if ($module == 'home' || $module == 'sepet' || $module == 'importers' || $module == 'montaj') && $method <> 'iletisim'}active open{/if}">
                                        <a href="javascript:;" class="text-uppercase">
                                            <i class="icon-home" style="display:none;"></i> Ana Sayfa
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-fw">
                                            <li class="{if $module == 'home' && $method == 'index'}active{/if}">
                                                <a href="{site_url('home/index')}">
                                                    <i class="fa fa-car" style="display:none;"></i> Ana Sayfa
                                                </a>
                                            </li>

                                            <li class="{if $module == 'montaj' && ($method == 'index' || $method == 'add')}active{/if}">
                                                <a href="{site_url('montaj/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Montaj Noktaları 
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'importers' && $method == 'excel'}active{/if}">
                                                <a href="{site_url('importers/excel')}">
                                                    <i style="display:none;" class="icon-plus"></i> Ürün Ekle (Excel)
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'importers' && $method == 'xml'}active{/if}">
                                                <a href="{site_url('importers/xml')}">
                                                    <i style="display:none;" class="icon-plus"></i> Ürün Ekle (XML)
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'home' && $method == 'entegrasyondurum'}active{/if}">
                                                <a href="{site_url('home/entegrasyondurum')}">
                                                    <i style="display:none;" class="icon-plus"></i> Entegrasyon
                                                </a> 
                                            </li>
                                 

                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw {if $module == 'products' || $module == 'marker' || $module == 'category' || $module == 'dimension' || $module == 'brand' || $module == 'comment'}active open{/if}">
                                        <a href="javascript:;" >
                                            <i class="icon-home" style="display:none;"></i> Ürünler
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                 
                                            <li class="dropdown{if $module == 'products' && ($method == 'index' || $method == 'siralama')} active{/if}">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> Ürünler
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{site_url('products/index/tip/lastik')}">Lastik</a> 
                                                </li>

                                                <li>
                                                    <a href="{site_url('products/index/tip/jant')}">Jant</a> 
                                                </li>

                                                <li>
                                                    <a href="{site_url('products/index/tip/mini-stepne')}">Mini Stepne</a> 
                                                </li>
                                                <li>
                                                    <a href="{site_url('products/siralama')}">Ürün Sıralamaları</a> 
                                                </li>
                                            </ul>
                                        </li>



                                            <li class="{if $module == 'comment' && $method == 'index'}active{/if}">
                                                <a href="{site_url('products/comment/index')}">
                                                    {$okunmayan_yorumlar = $this->extraservices->okunmayan_yorumlar()}
                                                    <i style="display:none;" class="icon-basket"></i> Yorumlar {if $okunmayan_yorumlar>0}<span class="text-danger">({$okunmayan_yorumlar})</span>{/if}
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'category' && $method == 'index'}active{/if}">
                                                <a href="{site_url('products/category/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Kategoriler 
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'brand' && ($method == 'index' || $method == 'patterns')}active{/if}">
                                                <a href="{site_url('products/brand/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Markalar 
                                                </a> 
                                            </li>


                                            <li class="{if $module == 'marker' && ($method == 'index' || $method == 'add'|| $method == 'edit')}active{/if}">
                                                <a href="{site_url('products/marker/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Etiketler 
                                                </a> 
                                            </li>

                                            <li class="dropdown{if ($module == 'products' && $method == 'add') || ($module == 'dimension' && $method == 'add')} active{/if}"">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Ürün Ekle
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                 <li class="{if $module == 'products' && $method == 'add' && $segment == 'lastik'}active{/if}">
                                                    <a href="{site_url('products/add/lastik')}">Lastik</a>
                                                </li>

                                                <li class="{if $module == 'products' && $method == 'add' && $segment == 'jant'}active{/if}">
                                                    <a href="{site_url('products/add/jant')}">Jant</a>
                                                </li>

                                                <li class="{if $module == 'products' && $method == 'add' && $segment == 'mini-stepne'}active{/if}">
                                                    <a href="{site_url('products/add/mini-stepne')}">Mini Stepne</a>
                                                </li>

                                            <li class="{if $module == 'dimension' && $method == 'add'}active{/if}">
                                                <a href="{site_url('products/dimension/add')}">
                                                    <i style="display:none;" class="icon-basket"></i> Lastik Ebatı Ekle
                                                </a>
                                            </li>
{*}
                                            <li class="{if $module == 'products' && $method == 'add' && $segment == 'zincir'}active{/if}">
                                                <a href="{site_url('products/add/zincir')}">
                                                    <i style="display:none;" class="icon-basket"></i> Zincir Ekle
                                                </a>
                                            </li>
                                            {*}
                                        </ul>
										
											 <li class="{if $module == 'products' && $method == 'debug'}active{/if}">
                                                <a href="{site_url('products/debug')}">
                                                    <i style="display:none;" class="icon-basket"></i> Hatalı Ürünler 
                                                </a> 
                                            </li>
                                    </li>

{*}
                                        

                                            <li class="{if $module == 'products' && $method == 'updatemultiproducts'}active{/if}">
                                                <a href="{site_url('products/updatemultiproducts')}">
                                                     Ürün Güncelleme
                                                </a>
                                            </li>
{*}
                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw{if $module == 'orders'  && $method <> 'uruntalepleri'} active open{/if}" >
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Siparişler
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                            <li class="{if $module == 'orders' && $method == 'index' }active{/if}"> 
                                                <a href="{site_url('orders/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Siparişler 
                                                </a> 
                                            </li> 
                                             <li class="{if $module == 'orders' && $method == 'odemebildirim' }active{/if}"> 
                                                <a href="{site_url('orders/odemebildirim')}">
                                                    <i style="display:none;" class="icon-basket"></i> Ödeme Bildirimleri
                                                </a> 
                                            </li>                                             
                                            <li class="{if $module == 'orders' && $method == 'iptaliade' }active{/if}"> 
                                                <a href="{site_url('orders/iptaliade')}">
                                                    <i style="display:none;" class="icon-basket"></i> İptal & İade Talepleri  
                                                </a> 
                                            </li> 
                             				<li class="{if $module == 'orders' && $method == 'posdetay' }active{/if}"> 
                                                <a href="{site_url('orders/posdetay')}">
                                                    <i style="display:none;" class="icon-basket"></i> Pos Detay
                                                </a> 
                                            </li> 
                                             
                                           	{*
                                           	<li class="">
                                                <a href="/admin/kargo-firmalari.html">
                                                    <i style="display:none;" class="icon-basket"></i> Kargo Firmaları 
                                                </a> 
                                            </li> 
                                            <li class="">
                                                <a href="/admin/iptal-iade.html">
                                                    <i style="display:none;" class="icon-basket"></i> İptal & İade Talepleri 
                                                </a> 
                                            </li>
                                            *}
                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw{if $module == 'users' ||  $module == 'groups' ||  $module == 'roles'} active open{/if}">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Kullanıcılar
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">


                                            <li class="{if $module == 'users' && $method == 'index' }active{/if}">
                                                <a href="{site_url('users/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Kullanıcılar
                                                </a>
                                            </li>

                                            <li class="{if $module == 'groups' && $method == 'index' }active{/if}">
                                                <a href="{site_url('users/groups/index')}">
                                                    <i style="display:none;" class="icon-basket"></i> Kullanıcı Grupları 
                                                </a> 
                                            </li>       

                                            <li class="{if $module == 'users' && $method == 'islem_kayitlari' }active{/if}">
                                                <a href="{site_url('users/islem_kayitlari')}">
                                                    <i style="display:none;" class="icon-basket"></i> İşlem Kayıtları 
                                                </a> 
                                            </li>   
                             
                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw {if $module == 'members' || ($module == 'home' && $method == 'iletisim') || ($module == 'orders' && $method == 'uruntalepleri')|| ($module == 'sayac' && ($method == 'index' || $method == 'banner_sayac'))} active open{/if}">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Müşteri {*}/ Bayi{*}Yönetimi
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                      {*}      <li class="{if $module == 'members' && ($method == 'groups')}active{/if}">
                                                <a href="{site_url('members/groups')}">
                                                    <i style="display:none;" class="icon-basket"></i> Üye / Bayi Grupları 
                                                </a> 
                                            </li>  {*}
                                            <li class="{if $module == 'members' && $method == 'add' }active{/if}">
                                                <a href="{site_url('members/add')}">
                                                    <i style="display:none;" class="icon-basket"></i> Yeni Müşteri {*}/ Bayi Kaydı {*}
                                                </a> 
                                            </li> 
                                            <li class="{if $module == 'members' && $method == 'crm' }active{/if}"> 
                                                <a href="{site_url('members/crm')}">
                                                    <i style="display:none;" class="icon-basket"></i>Crm
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'members' && $method == 'crmq' }active{/if}"> 
                                                <a href="{site_url('members/crmq')}">
                                                    <i style="display:none;" class="icon-basket"></i>Crm Soruları
                                                </a> 
                                            </li>
                                             <li class="{if $module == 'home' && $method == 'iletisim'}active{/if}">
                                                <a href="{site_url('home/iletisim')}">
                                                    <i class="fa fa-car" style="display:none;"></i> iletişim İstekleri
                                                </a>
                                            </li>
                                                        <li class="{if $module == 'orders' && $method == 'uruntalepleri' }active{/if}"> 
                                                <a href="{site_url('orders/uruntalepleri')}">
                                                    <i style="display:none;" class="icon-basket"></i>Ürün Talepleri
                                                </a> 
                                            </li> 
                                            <li class="{if $module == 'sayac' && $method == 'index' }active{/if}"> 
                                                <a href="{site_url('sayac/index/tarih/'|cat:date('d.m.Y-d.m.Y',time() - 86400))}">
                                                    <i style="display:none;" class="icon-basket"></i>Hareketler
                                                </a> 
                                            </li> 
                                            <li class="{if $module == 'sayac' && $method == 'banner_sayac' }active{/if}"> 
                                                <a href="{site_url('sayac/banner_sayac/tarih/'|cat:date('d.m.Y-d.m.Y',time() - 86400))}">
                                                    <i style="display:none;" class="icon-basket"></i>Banner Hareketleri
                                                </a> 
                                            </li> 
                                        </ul>
                                    </li>


                                    <li class="dropdown dropdown-fw {if $module == 'pages' || $module == 'faqs' || $module == 'news' || $module == 'anket'} active open{/if}">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> İçerik Yönetimi
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">

                                            <li class="dropdown {if $module == 'pages' && ($method == 'add' || $method == 'index' )}active{/if}">
                                             <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Sabit Sayfalar
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                {$sayfatleri = $this->extraservices->sayfaturleri()}
                                                {foreach $sayfatleri as $sayfat}
                                                <li class="{if $module == 'pages' && $method == 'index' && $segment == $sayfat->id}active{/if}">
                                                    <a href="{site_url('pages/index')}/{$sayfat->id}">{$sayfat->tip}</a> 
                                                </li>
                                                {/foreach}

                                                <li>
                                                    <a href="{site_url('faqs/index')}">Sık Sorulan Sorular</a> 
                                                </li>
                                                <li>
                                                 <a href="{site_url('pages/add')}">Yeni Sayfa Ekle</a> 
                                             </li>
                                         </ul>


                                     </li>
                                     <li class=" {if $module == 'anket'}active{/if}">
                                        <a href="{site_url('anket/index')}">Anket</a>
                                   </li>

                                      
										 

                                            <li class="dropdown {if $module == 'news' && ($method == 'index' || $method == 'add' || $method == 'edit')}active{/if}">
                                             <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Haberler ve Kampanyalar
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                             
                                                      <li>
													<a href="{site_url('news/index')}">
														<i style="display:none;" class="icon-plus"></i> Haber ve Kampanya Yönetimi
													</a> 
                                                </li>
                                                <li>
													<a href="{site_url('news/category')}">
														<i style="display:none;" class="icon-plus"></i> Haber Kategorileri
													</a> 
                                                </li>
                                         </ul>


                                            <li class="{if $module == 'news' && ($method == 'opportunities' || $method == 'addopportunity' || $method == 'editopportunity')}active{/if}">
                                                <a href="{site_url('news/opportunities')}">
                                                    <i style="display:none;" class="icon-plus"></i> Fırsat Ürünleri
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'news' && $method == 'pageskin'}active{/if}">
                                                <a href="{site_url('news/pageskin')}">
                                                    <i style="display:none;" class="icon-plus"></i> Page Skin
                                                </a> 
                                            </li>
											
                                            <li class="{if $module == 'news' && $method == 'banner_management'}active{/if}">
                                                <a href="{site_url('news/banner_management')}">
                                                    <i style="display:none;" class="icon-plus"></i> Banner Yönetimi
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'news' && $method == 'testimonials'}active{/if}">
                                                <a href="{site_url('news/testimonials')}">
                                                    <i style="display:none;" class="icon-plus"></i> Testimonials
                                                </a> 
                                            </li>

                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw {if $module == 'settings'} active open{/if}">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Ayarlar
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">

                                            {$gruplarim['menu'] = 1}
                                            {$gruplar = $this->extraservices->gruplar($gruplarim)}
                                            <li class="dropdown{if $module == 'settings'} active{/if}">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Seo Ayarları
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">


                                                    {foreach $gruplar as $grup}
                                                    {if $grup->seo_ayarlari == 1}
                                                    <li class="{if $module == 'settings' && $method == 'group' && $segment == $grup->id}active{/if}">
                                                        <a href="{site_url('settings/group')}/{$grup->id}">
                                                            <i style="display:none;" class="icon-plus"></i> {$grup->group_name}
                                                        </a> 
                                                    </li>
                                                    {/if}
                                                    {/foreach}
                                                </ul>
                                            </li>
                                            {foreach $gruplar as $grup}
                                            {if $grup->seo_ayarlari == 0}
                                            <li class="{if $module == 'settings' && $method == 'group' && $segment == $grup->id}active{/if}">
                                                <a href="{site_url('settings/group')}/{$grup->id}">
                                                    <i style="display:none;" class="icon-plus"></i> {$grup->group_name}
                                                </a> 
                                            </li>
                                            {/if}
                                            {/foreach}

                                            <li class="{if $module == 'settings' && $method == 'payments' }active{/if}">
                                                <a href="{site_url('settings/payments')}">
                                                    <i style="display:none;" class="icon-plus"></i> Ödeme Ayarları
                                                </a> 
                                            </li>

                                            <li class="{if $module == 'settings' && $method == 'mailtext' }active{/if}">
                                                <a href="{site_url('settings/mailtext')}">
                                                    <i style="display:none;" class="icon-plus"></i> Mail Metin Ayarları
                                                </a> 
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="dropdown dropdown-fw {if $module == 'campaigns'} active open{/if}">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Kampanya Yönetimi 
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
											<li class="dropdown {if $module == 'campaigns' && ($method == 'manage' || $method == 'reqgroups'|| $method == 'resgroups')}active{/if}">
												<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Kampanyalar
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
												<ul class="dropdown-menu" role="menu">
													
													<li>
														<a href="{site_url('campaigns/manage')}">Kampanyalar</a> 
													</li>
													<li>
														<a href="{site_url('campaigns/reqgroups')}">Kampanya Koşul Grupları</a> 
													</li>
													<li>
														<a href="{site_url('campaigns/resgroups')}">Kampanya Sonuç Grupları</a> 
													</li>
												</ul>
											</li>
                                            <li class="{if $module == 'campaigns' && ($method == 'points')}active{/if}">
                                                <a href="{site_url('campaigns/points')}">
                                                    <i style="display:none;" class="icon-basket"></i> Puan Sistemi 
                                                </a> 
                                            </li>

                                            <li class="dropdown {if $module == 'campaigns' && ($method == 'addgiftvoucher' || $method == 'giftvouchers' || $method == 'editgiftvoucher')}active{/if}">
												<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Hediye Çekleri
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
												<ul class="dropdown-menu" role="menu">
													<li>
														<a href="{site_url('campaigns/giftvouchers')}"> Hediye Çeki Yönetimi</a> 
													</li>
													<li>
														<a href="{site_url('campaigns/addordergiftvoucher')}">Sipariş Hediye Çeki Tanımı</a> 
													</li>
												</ul>
                                            </li>
											{*}
                                            <li class="{if $module == 'campaigns' && ($method == 'specialcampaigns')}active{/if}">
                                                <a href="{site_url('campaigns/specialcampaigns')}">
                                                    <i style="display:none;" class="icon-basket"></i> Özel Kampanyalar
                                                </a> 
												
                                            </li>
                                            {*}
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- END HEADER MENU -->
                        </div>
                        <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
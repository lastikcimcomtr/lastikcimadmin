{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">

                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr style="width:50%;">   
                                    <th  style="border-right: 0">

                                        <b>   {$toplam->sayfa}</b>  banner  <b>{$toplam->oturum}</b> kişi tarafından <b>{$toplam->ziyaret}</b> kez tıklandı
 
                                    </th> 
                                    <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">

                                    </th>
                                </tr>
                            </thead>
                        </table>

                        <div class="portlet-body">
                            <div class="tabbable-bordered">

                                <div class="tab-content">                               
                                    <div class="tab-pane active" id="tab_kategoriler">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="1%">Tıklama </th> 
                                                        <th width="1%">Hedef </th> 
                                                        <th>Banner Adı</th> 
                                                        <th width="1%">Resim URL</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {if count($veriler) > 0}
                                                    {foreach $veriler as $veri}
                                                    <tr>
                                                        {$parametreler = json_decode($veri->param)}
                                                        <td>{$veri->ziyaret}</td>
                                                        <td>{$parametreler->targets}</td> 
                                                        <td>{$parametreler->banner_type}</td> 
                                                        <td style="text-align: center">{$parametreler->img_src}</td> 
                                                    </tr>
                                                    {/foreach}
                                                    {else}
                                                    <tr>
                                                        <td colspan='10' style='text-align:center;'>Veri Bulunamadı </td>
                                                    </tr>
                                                    {/if}
                                                </tbody>
                                            </table>
                                              {$this->pagination->create_links()}
                                        </div> 
                                    </div>                             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->



        </div>
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
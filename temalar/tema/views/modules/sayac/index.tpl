{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Arama
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="Aç / Kapat"> </a> 
                        </div>
                    </div>
                    <div class="portlet-body tabs-below" >
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('sayac/index')}">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Tarih Aralığı</label>
                                    <div class="col-md-7" >                                                                       
                                        <input type="text" class="form-control" autocomplete = "off" name="tarih"  {if $tarih}value="{$tarih[0]}-{$tarih[1]}"{/if}>                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Adres İçersin</label>
                                    <div class="col-md-7" >                                                                       
                                        <input type="text" class="form-control" autocomplete = "off" name="url" placeholder="" value="{$url}" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-5 control-label"></label>
                                        <div class="col-md-7" style="padding-right: 0px">         
                                            <button type="submit" class="btn" style="background-color: #74a921;border-color: #74a921;width: 100%;color: white;">Arama Yapın</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">

                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr style="width:50%;">   
                                    <th  style="border-right: 0">

                                        <b>   {$toplam->sayfa}</b>  sayfa  <b>{$toplam->oturum}</b> kişi tarafından <b>{$toplam->ziyaret}</b> kez ziyaret edildi
 
                                    </th> 
                                    <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">

                                    </th>
                                </tr>
                            </thead>
                        </table>

                        <div class="portlet-body">
                            <div class="tabbable-bordered">

                                <div class="tab-content">                               
                                    <div class="tab-pane active" id="tab_kategoriler">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th width="1%">Görüntülenme </th> 
                                                        <th width="1%">Oturum </th> 
                                                        <th>Url</th> 
                                                        <th width="1%">Alarm</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {if count($veriler) > 0}

                                                    {foreach $veriler as $veri}
                                                    <tr>
                                                        <td style="text-align: center">{$veri->ziyaret}</td> 
                                                        <td style="text-align: center">{$veri->oturum}</td> 
                                                        <td><a href="{$veri->url}" target="_blank" >{$veri->url}</a> </td> 
                                                        <td style="text-align: center">{if $veri->class == 'products' && $veri->stok == 0}Stok Yok{else}-{/if}</td>
                                                    </tr>
                                                    {/foreach}
                                                    {else}
                                                    <tr>
                                                        <td colspan='10' style='text-align:center;'>Veri Bulunamadı </td>
                                                    </tr>
                                                    {/if}
                                                </tbody>
                                            </table>
                                              {$this->pagination->create_links()}
                                        </div> 
                                    </div>                             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->



        </div>
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
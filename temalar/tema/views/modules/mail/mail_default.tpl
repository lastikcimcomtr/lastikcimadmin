<table style="border:solid 1px #eaeaea;background-color:#ffffff" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td>
                {include file="mail_header.tpl"}
            </td>
        </tr>
        <tr>
        	<td>
                <table width="600" border="0" cellpadding="10" cellspacing="10" style="font-family:'' Lucida Grande '','' Lucida Sans Unicode '','' Lucida Sans '','' DejaVu Sans '',Verdana,sans-serif;font-size:12px;color:#646464">
                    <tbody>
                        <tr>
                            <td>
                     
                                {$text}
                                
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
        </tr>

        <tr>
        	<td>
        		{include file="mail_promotion.tpl"}
        	</td>
        </tr>
        <tr>
        	<td>
        		{include file="mail_footer.tpl"}
        	</td>
        </tr>
    </tbody>
</table>
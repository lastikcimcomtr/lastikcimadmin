<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{$mail_template->mail_basligi1}</title>
    <style type="text/css">
        body{
            width: 100%;
            font-family: "Arial","Arial Black",sans-serif;
            color: #333;
            text-align: center;
            background-color: #d3d3d3;
        }
        .full{
            width: 100%;
            text-align: center;
            color: #333;
            background-color: #d3d3d3;
        }
        .container{
            max-width: 700px;
            text-align: left;
            display: inline-block;
            background-color: #fff;
            color: #333;
            margin-bottom : 64px;
            margin-top : 32px;
        }
        .logo{
            padding: 19px 5% 24px 5%;
        }
        .logo > img{
            width: 200px;
        }
        .content{
            background-color: #f3f3f3;
            padding: 10px 5% 10px 5%;
        }
        .cont{
            padding: 10px 5% 10px 5%;
        }
        .upper_footer{
            padding: 38px 5% 26px 5%;
            font-size: 0.8em;
        }
        .footer{
            font-size: 18px;
            font-weight: 600;
            line-height: 21px;
            background-color: #f3f3f3;
            color: #0e76bc;
            padding: 10px 5% 10px 5%;
        }
        .footer > div{
            display: inline-block;
        }
        .footer > div > div{
            display: inline-block;
        }
        .footer .tel img{
            position: relative;
            top: 4px;
            margin-right: 8px;
        }
        .w-2{
            width: 2em;
        }
        .content .call{
            padding: 10px 0 5px 0;
            font-style: italic;
            font-weight: 600;
        }
        .content .message_title{
            padding: 5px 0 5px 0;
            font-weight: 500;
            font-size: 20px;
            color: #0e76bc;
        }
        .content .description{
            padding: 0.8em 0 1em 0;
            font-weight: 400;
            font-size: 15px;
        }
        .upper_footer > .mail{
            font-weight: 600;
            text-decoration: underline;
            color: #333333;
        }
        .footer a{
            text-decoration: none;
            color: #0e76bc;
        }
        .content a.id{
            color: #0e76bc;
        }
        table{
            text-align:left;
            width: 100%;
            line-height: 24px;
        }
        table thead{
            font-size: 1.2em;
            color: #0e76bc;
        }
        .cont{
            font-weight:600;
            display: block;
        }
        .cont .title{
            font-size: 20px;
            padding: 8px 0 8px 0;
            color: #0e76bc;
            font-weight:600;
            margin-bottom: 10px;
        }
        .cont .items .item{
            display: table;
            height: 100%;
            width: 100%;
        }
        .cont .blue-600{
            color: #0e76bc;
            font-weight:600 !important;
            font-size: 1.0em !important;
            border-top: 1px solid #ccc;
        }
        .cont .line{
            display: table;
            font-size: 11px;
            line-height: 25px;
            vertical-align: middle;
            font-weight:500;
            width: 100%;
            border-bottom: 1px solid #ccc;
            padding: 12px 0 12px 0 !important;
        }
        .cont .description{
            width: 46%;
            display: inline-block;
        }
        .cont .line-val{
            width: 35%;
            display: inline-block;
        }
        .cont .line-value{
            width: 64%;
            vertical-align: middle;
            display: inline-block;
        }
        .cont .line-info{
            width: 36%;
            vertical-align: middle;
            font-weight: 600;
            display: inline-block;
        }
        .cont .total{
            width: 19%;
            display: inline-block;
            text-align: right;
        }
		.cont .items{
			font-size: 11px;
		}
        .cont .items > .item .picture{
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            width: 10%;
        }
		.cont .items > .item .picture_span{
            display: inline-block;
            vertical-align: middle;
            width: 2%;
		}
        .cont .items > .item .name{
            display: inline-block;
            vertical-align: middle;
            width: 54%;
        }
        .cont .items > .item .amount{
            display: inline-block;
            vertical-align: middle;
            width: 14%;
        }
        .cont .items > .item .price{
            display: inline-block;
            vertical-align: middle;
            width: 20%;
            text-align: right;
        }
        .cont .items > .item .picture img{
            vertical-align: middle;
            display: inline-block;
            width: 100%;
        }
        .cont .w-50{
            display: table-cell;
            width: 50%;
        }
    </style>
</head>
<body>
{$urun_adetler=0}
{$data =  $order->data|json_decode}

{$urunler = $order->urunler|json_decode}

{$odemedata = $order->odemedata|json_decode}

{$odemebanka =  $this->extraservices->paytype($odemedata)}

{if  $odemedata->artitaksit}

{$installment  = ($odemedata->installment * 1 ) + ($odemedata->artitaksit * 1) }

{else}
{$installment  = $odemedata->installment}
{/if}
<!--[if mso]>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <tr><td>
        <img width="200px" src="http://admin.lastikcim.com.tr/temalar/tema/assets/logo.png" alt=""/></td></tr>
</table>
<table width="600" cellpadding="12" style="background-color: #f3f3f3;">
    <tr><td>
        {if $data->ticari_unvani} {$data->ticari_unvani} {else}{$data->adi} {$data->soyadi}{/if} Merhaba</td></tr>
    <tr><td>
        Siparişin Alınmıştır Teşekkürler!</td></tr>
    <tr><td>
        {$mail_template->mail_aciklamasi}
    </td></tr>
</table>
{$kargokdvoran = 18}
{$hizmetkdvoran = 18}
{$urunlertoplam = 0.00}
{$montajtoplam = 0}
{$urunlerkdv = 0}
{$urunlerkargo = 0}
{$toplam = 0}
{$hizmetler_is = false}
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="4" width="600">Satın Aldığın Ürünler</td></tr>
    </thead>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    {foreach from=$urunler item=urun}
    {if $urun->kdv_dahil == 1}
    {$kdahil = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
    {$kharic = kdvcikar($kdahil,$urun->kdv)}
    {$kdv = $kdahil -$kharic}
    {else}
    {$kharic = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
    {$kdahil = kdvhesapla($kharic,$urun->kdv)}
    {$kdv = $kdahil -$kharic}
    {/if}
    {$urunlerkdv = $urunlerkdv + $kdv}
    {if count($urun->hizmetler) > 0}{$hizmetler_is = true}{/if}
    {$urun_kdvsiz = kdvcikar($urun->fiyat,$urun->kdv)}
    {$urun_adetler = $urun_adetler + $urun->miktar}
    {$urunlertoplam = $urunlertoplam + $kharic}
    <tr>
        <td width="50"><img width="50" src="https://www.lastikcim.com.tr/resimler/{$urun->url_duzenle}.jpg" style="max-width: 1200px;"/></td>
        <td width="350">{$urun->urun_adi}</td>
        <td width="100">{$urun->miktar} Adet</td>
        <td width="100">{fiyatver(kdvcikar($urun->fiyat,$urun->kdv)  ,null,  $urun->indirim_orani)} TL</td>
    </tr>
    {/foreach}

    {if $hizmetler_is}
    {foreach from=$urunler item=urun}
    {foreach from=$urun->hizmetler item=hizmet}
    {$hizmetkdvdahil =  $hizmet->fiyat*$hizmet->adet }
    {$hizmetkdvharic =  kdvcikar($hizmetkdvdahil ,$hizmetkdvoran)}
    {$hizmetkdv =  $hizmetkdvdahil - $hizmetkdvharic}
    {$urunlerkdv = $urunlerkdv + $hizmetkdv}
    {$montajtoplam = $montajtoplam + hizmetkdvharic}
    {$firma_id = $hizmet->firma_id}
    {/foreach}
    {/foreach}
    {/if}

    {foreach from=$urunler item=urun}
    {if $urun->kargo_bedeli}
    {if $urun->kargo > 0}
    {$kargokdvoran = 18}
    {$kargokdvdahil =  $urun->kargo_bedeli*$urun->miktar}
    {$kargokdvharic =  $kargokdvdahil/(1+($kargokdvoran/100))}
    {$kargokdv =  $kargokdvdahil - $kargokdvharic }
    {$urunlerkdv = $urunlerkdv + $kargokdv}
    {$urunlerkargo = $urunlerkargo + $kargokdvharic}
    {/if}
    {else}
    {$kargokdvharic = 0}
    {/if}
    {/foreach}
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    {$indirim_sepet = $this->orders->siparis_hediyecekleri($order->id)}
    {foreach  from=$indirim_sepet item=indirim_ceki}
    <tr><td colspan="10">İndirim Çeki - {$indirim_ceki->cek_kodu}</td><td colspan="2">
        {if $indirim_ceki->dtip==1}
        {$indirim_ceki->dgr} %
        {else}
        {$indirim_ceki->dgr} TL
        {/if}</td></tr>
    {/foreach}
    {$toplam = $urunlertoplam + $urunlerkdv + $urunlerkargo + $montajtoplam}
    <tr><td colspan="10">TOPLAM TUTAR (KDV DAHİL)</td><td colspan="2">{fiyatver($odemedata->sepet_tutar)} TL</td></tr>

    {if $odemedata->installment > 1}
    {if $odemedata->komisyon}
    <tr><td colspan="10">Ödeme Komisyonu</td><td colspan="2">{$odemedata->komisyon|string_format:"%.2f"} TL</td></tr>
    {/if}
    {/if}
    <tr><td colspan="10">Ürünler Toplamı</td><td colspan="2">{$urunlertoplam|string_format:"%.2f"} TL</td></tr>
    <tr><td colspan="10">KDV</td><td colspan="2">{$urunlerkdv|string_format:"%.2f"} TL</td></tr>
    <tr><td colspan="10">Hizmet Bedeli</td><td colspan="2">{$montajtoplam|string_format:"%.2f"} TL</td></tr>
    <tr><td colspan="10">Kargo Bedeli</td><td colspan="2">{$urunlerkargo|string_format:"%.2f"} TL</td></tr>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Fatura Bilgileri</td></tr>
    </thead>
    <tbody>
    <tr><td colspan="4">İsim / Ünvan</td><td colspan="8"> {if $data->ticari_unvani}{$data->ticari_unvani}{else}{$data->adi} {$data->soyadi}{/if}</td></tr>
    <tr><td colspan="4">TC Kimlik / Vergi No</td><td colspan="8">{if $data->vergi_no}{$data->vergi_no}{else}{$data->tc_kimlik_no}{/if}</td></tr>
    <tr><td colspan="4">Açık Adres</td><td colspan="8">{if $data->kur_adres}{$data->kur_adres}{else}{$data->adres}{/if}</td></tr>
    <tr><td colspan="4">Şehir</td><td colspan="8">{if $data->kur_il}{$this->extraservices->ilbyid($data->kur_il)}{else}{$this->extraservices->ilbyid($data->il)}{/if}</td></tr>
    <tr><td colspan="4">İlçe</td><td colspan="8">{if $data->kur_ilce}{$this->extraservices->ilcebyid($data->kur_ilce)}{else}{$this->extraservices->ilcebyid($data->ilce)}{/if}</td></tr>
    <tr><td colspan="4">Posta Kodu</td><td colspan="8">{if $data->kur_posta_kodu}{$data->kur_posta_kodu}{else}{$data->posta_kodu}{/if}</td></tr></tbody>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Ödeme Bilgileri</td></tr>
    </thead>
    <tbody>
    <tr><td colspan="4">Ödeme Türü</td><td colspan="8">{$this->extraservices->odemetipiad($order->odeme_tipi)}</td></tr>
    <tr><td colspan="4">Taksit</td><td colspan="8">{$this->orders->posArtiTaksit($odemedata->sanalpos, $odemedata->installment)}</td></tr>
    <tr><td colspan="4">Banka</td><td colspan="8">{if $odemebanka}{$odemebanka}{/if}</td></tr>
    </tbody>
</table>

{if $data->teslimyeri == "montaj_noktasina"}
{$firma = $this->extraservices->montaj_noktalari($firma_id)}
{$is_montage_point = true}
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Teslimat Bilgileri - Montaj Noktasına</td></tr>
    </thead>
    <tbody>
    <tr><td colspan="4">Firma Adı</td><td colspan="8">{$firma->firma_adi}</td></tr>
    <tr><td colspan="4">Firma Adresi</td><td colspan="8">{$firma->firma_adresi}</td></tr>
    <tr><td colspan="4">Şehir</td><td colspan="8">{$firma->il}</td></tr>
    <tr><td colspan="4">İlçe</td><td colspan="8">{$firma->ilce}</td></tr>
    <tr><td colspan="4">Montaj Noktası Telefonu</td><td colspan="8">{$firma->firma_tel1}</td></tr></tbody>
</table>
{elseif  $data->teslimyeri == "teslim_adresine"}
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Teslimat Bilgileri</td></tr>
    </thead>
    <tbody>
    <tr><td colspan="4">Adı Soyadı</td><td colspan="8">{$data->teslim_edilecek_adi}</td></tr>
    <tr><td colspan="4">Açık Adres</td><td colspan="8">{$data->teslim_adres}</td></tr>
    <tr><td colspan="4">Şehir</td><td colspan="8">{$this->extraservices->ilbyid($data->teslim_il)}</td></tr>
    <tr><td colspan="4">İlçe</td><td colspan="8">{$this->extraservices->ilcebyid($data->teslim_ilce)}</td></tr></tbody>
    <tr><td colspan="4">Cep Telefonu</td><td colspan="8">{$data->teslim_edilecek_telefonu}</td></tr></tbody>
</table>
{else}
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Teslimat Bilgileri</td></tr>
    </thead>
    <tbody>
    <tr><td colspan="4">Adı Soyadı</td><td colspan="8">{if $data->ticari_unvani}{$data->ticari_unvani}{else}{$data->adi} {$data->soyadi}{/if}</td></tr>
    <tr><td colspan="4">Açık Adres</td><td colspan="8">{if $data->kur_adres}{$data->kur_adres}{else}{$data->adres}{/if}</td></tr>
    <tr><td colspan="4">Şehir</td><td colspan="8">{if $data->kur_il}{$this->extraservices->ilbyid($data->kur_il)}{else}{$this->extraservices->ilbyid($data->il)}{/if}</td></tr>
    <tr><td colspan="4">İlçe</td><td colspan="8">{if $data->kur_ilce}{$this->extraservices->ilcebyid($data->kur_ilce)}{else}{$this->extraservices->ilcebyid($data->ilce)}{/if}</td></tr></tbody>
    <tr><td colspan="4">Cep Telefonu</td><td colspan="8">{if $data->kur_cep_telefonu}{$data->kur_cep_telefonu}{else}{$data->cep_telefonu}{/if}</td></tr></tbody>
</table>
{/if}
<table width="600" cellpadding="15" style="background-color: #fff;">
    <thead>
    <tr><td colspan="12">Montaj Bilgileri</td></tr>
    </thead>
    <tr><td colspan="12">Montaj randevusu için müşteri hizmetleri seninle irtibata geçecektir. Dilersen 0850 811 11 01’den arayıp bizimle iletişime geçebilirsin.</td></tr>
</table>

<table width="600" cellpadding="0" style="background-color: #fff;">
    <tr>
        <td>
            <table cellpadding="15" style="background-color: #fff;">
                <thead>
                <tr><td>Mesafeli Satış Sözleşmesi</td></tr>
                </thead>
                <tr><td>
                    Detayları tekrar okumak istersen aşağıdaki linke tıkla.
                    <p><a target="_blank" href="https://www.lastikcim.com.tr/pages/mesafeli_satis_sozlesmesi/{$order->hash_value}">Mesafeli Satış Sözleşmesini Gör</a></p>
                </td></tr>
            </table>
        </td>
        <td>
            <table cellpadding="15" style="background-color: #fff;">
                <thead>
                <tr><td>Ön Bilgilendirme Formu</td></tr>
                </thead>
                <tr><td>
                    Detayları tekrar okumak istersen aşağıdaki linke tıkla.
                    <p><a target="_blank" href="https://www.lastikcim.com.tr/pages/on_bilgilendirme_formu/{$order->hash_value}">Ön Bilgilendirme Formunu Gör</a></p>
                </td></tr>
            </table>
        </td>
    </tr>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <tr><td>
        Farklı bir sorun varsa lütfen <a class="mail" href="mailto:destek@lastikcim.com.tr">destek@lastikcim.com.tr</a>’ye mail at.
    </td></tr>
</table>
<table cellpadding="15" style="background-color: #f3f3f3;">
    <tr><td><a href="https://www.lastikcim.com.tr" target="_blank">www.lastikcim.com.tr</a>
    </td><td><img style="max-height: 10px" src="https://www.lastikcim.com.tr/temalar/tema/assets/img/mail_images/tel_min.png"><a href="tel:00908508111101"> 0850 811 11 01</a></td></tr>
</table>
<![endif]-->
<!--[if !mso]> <!---->
{$kargokdvoran = 18}
{$hizmetkdvoran = 18}
{$urunlertoplam = 0.00}
{$montajtoplam = 0}
{$urunlerkdv = 0}
{$urunlerkargo = 0}
{$toplam = 0}
{$hizmetler_is = false}
<div class="full">
    <div class="container">
        <div class="logo">
            <img src="http://admin.lastikcim.com.tr/temalar/tema/assets/logo.png" alt=""/>
        </div>
        <div class="content">
            <div class="call">
                {if $data->ticari_unvani} {$data->ticari_unvani} {else}{$data->adi} {$data->soyadi}{/if} Merhaba
            </div>
            <div class="message_title">
                Siparişin Onaylandı!
            </div>
            <div class="description">
                {$mail_template->mail_aciklamasi}
            </div>
        </div>
        <div class="cont">
            <div class="title">Satın Aldığın Ürünler</div>
            <div class="items">
                {foreach from=$urunler item=urun}
                {if $urun->kdv_dahil == 1}
                {$kdahil = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
                {$kharic = kdvcikar($kdahil,$urun->kdv)}
                {$kdv = $kdahil -$kharic}
                {else}
                {$kharic = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
                {$kdahil = kdvhesapla($kharic,$urun->kdv)}
                {$kdv = $kdahil -$kharic}
                {/if}
                {$urunlerkdv = $urunlerkdv + $kdv}
                {if count($urun->hizmetler) > 0}{$hizmetler_is = true}{/if}
                {$urun_kdvsiz = kdvcikar($urun->fiyat,$urun->kdv)}
                {$urun_adetler = $urun_adetler + $urun->miktar}
                {$urunlertoplam = $urunlertoplam + $kharic}
                <div class="item">
                    <div class="picture">
                        <img src="https://www.lastikcim.com.tr/resimler/{$urun->url_duzenle}.jpg"/>
                    </div>
					<div class="picture_span"> </div>
                    <div class="name">
                        {$urun->urun_adi}
                    </div>
                    <div class="amount">
                        {$urun->miktar} Adet
                    </div>
                    <div class="price">
                        {fiyatver(kdvcikar($urun->fiyat,$urun->kdv)  ,null,  $urun->indirim_orani)} TL
                    </div>
                </div>
                {/foreach}
                {if $hizmetler_is}
                {foreach from=$urunler item=urun}
                {foreach from=$urun->hizmetler item=hizmet}
                {$hizmetkdvdahil =  $hizmet->fiyat*$hizmet->adet }
                {$hizmetkdvharic =  kdvcikar($hizmetkdvdahil ,$hizmetkdvoran)}
                {$hizmetkdv =  $hizmetkdvdahil - $hizmetkdvharic}
                {$urunlerkdv = $urunlerkdv + $hizmetkdv}
                {$montajtoplam = $montajtoplam + hizmetkdvharic}
                {$firma_id = $hizmet->firma_id}
                {/foreach}
                {/foreach}
                {/if}
                {foreach from=$urunler item=urun}
                {if $urun->kargo_bedeli}
                {if $urun->kargo > 0}
                {$kargokdvoran = 18}
                {$kargokdvdahil =  $urun->kargo_bedeli*$urun->miktar}
                {$kargokdvharic =  $kargokdvdahil/(1+($kargokdvoran/100))}
                {$kargokdv =  $kargokdvdahil - $kargokdvharic }
                {$urunlerkdv = $urunlerkdv + $kargokdv}
                {$urunlerkargo = $urunlerkargo + $kargokdvharic}
                {/if}
                {else}
                {$kargokdvharic = 0}
                {/if}
                {/foreach}
            </div>
        </div>
        {$toplam = $data->sepet_tutar}
        {if $odemedata->installment > 1}
        {if $odemedata->komisyon}
        {$toplam = $toplam + $odemedata->komisyon}
        {/if}
        {/if}
		{$havale_indirimi = 0}
		{if $order->temp_transfer}
			{$havale_indirimi = ($toplam * $order->temp_transfer / 100)}
		{/if}
		{$indirim_sepet = $this->orders->siparis_hediyecekleri($order->id)}
		{$toplam_2 = $toplam}
		{$p_set = false}
		{if $indirim_sepet}
		{$p_set = true}
		{/if}
        <div class="cont">
            <div class="line blue-600">
                <div class="description">{if $p_set}ÖDEME TUTARI{else}TOPLAM TUTAR (KDV DAHİL){/if}</div>
                <div class="line-val"></div>
                <div class="total">{fiyatver($toplam)} TL</div>
            </div>
        </div>
        <div class="cont">
            {foreach  from=$indirim_sepet item=indirim_ceki}
			<div class="line blue-600">
				<div class="description">Ödeme Çeki - {$indirim_ceki->cek_kodu}</div>
				<div class="line-val"></div>
				<div class="total">
					{if $indirim_ceki->dtip==1}
					{$indirim_ceki->dgr} %
					{else}
					{$toplam_2 = $toplam_2 + $indirim_ceki->dgr}
					-{$indirim_ceki->dgr} TL
					{/if}
				</div>
			</div>
            {/foreach}
			{if $p_set}
			<div class="line">
                <div class="description">TOPLAM TUTAR (KDV DAHİL)</div>
                <div class="line-val"></div>
                <div class="total">{fiyatver($toplam_2)} TL</div>
            </div>
			{/if}
            {if $odemedata->installment > 1}
            {if $odemedata->komisyon}
            <div class="line">
                <div class="description">Ödeme Komisyonu</div>
                <div class="line-val"></div>
                <div class="total">{$odemedata->komisyon|string_format:"%.2f"} TL</div>
            </div>
            {/if}
            {/if}
            <div class="line">
                <div class="description">Ürünler Toplamı</div>
                <div class="line-val"></div>
                <div class="total">{$urunlertoplam|string_format:"%.2f"} TL</div>
            </div>
            <div class="line">
                <div class="description">KDV</div>
                <div class="line-val"></div>
                <div class="total">{$urunlerkdv|string_format:"%.2f"} TL</div>
            </div>
            <div class="line">
                <div class="description">Montaj Bedeli</div>
                <div class="line-val"></div>
                <div class="total">{$montajtoplam|string_format:"%.2f"} TL</div>
            </div>
            <div class="line">
                <div class="description">Kargo Bedeli</div>
                <div class="line-val"></div>
                <div class="total">{$urunlerkargo|string_format:"%.2f"} TL</div>
            </div>
			{if $havale_indirimi}
				{if $havale_indirimi>0}
					<div class="line">
						<div class="description">Havale İndirimi ({$order->temp_transfer}%)</div>
						<div class="line-val"></div>
						<div class="total">-{$havale_indirimi|string_format:"%.2f"} TL</div>
					</div>					
				{/if}
			{/if}
        </div>
        <div class="cont">
            <div class="title">
                Fatura Bilgileri
            </div>
            <div class="line">
                <div class="line-info">İsim / Ünvan</div>
                <div class="line-value">{if $data->ticari_unvani}{$data->ticari_unvani}{else}{$data->adi} {$data->soyadi}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">TC Kimlik / Vergi No</div>
                <div class="line-value">{if $data->vergi_no}{$data->vergi_no}{else}{$data->tc_kimlik_no}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Açık Adres</div>
                <div class="line-value">{if $data->kur_adres}{$data->kur_adres}{else}{$data->adres}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Şehir</div>
                <div class="line-value">{if $data->kur_il}{$this->extraservices->ilbyid($data->kur_il)}{else}{$this->extraservices->ilbyid($data->il)}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">İlçe</div>
                <div class="line-value">{if $data->kur_ilce}{$this->extraservices->ilcebyid($data->kur_ilce)}{else}{$this->extraservices->ilcebyid($data->ilce)}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Posta Kodu</div>
                <div class="line-value">{if $data->kur_posta_kodu}{$data->kur_posta_kodu}{else}{$data->posta_kodu}{/if}</div>
            </div>
        </div>

        <div class="cont">
            <div class="title">
                Ödeme Bilgileri
            </div>
            <div class="line">
                <div class="line-info">Ödeme Türü</div>
                <div class="line-value">{$this->extraservices->odemetipiad($order->odeme_tipi)}</div>
            </div>
            <div class="line">
                <div class="line-info">Taksit</div>
                <div class="line-value">{$this->orders->posArtiTaksit($odemedata->sanalpos, $odemedata->installment)}</div>
            </div>
            <div class="line">
                <div class="line-info">Banka</div>
                <div class="line-value">{if $odemebanka}{$odemebanka}{/if}</div>
            </div>
        </div>

        <div class="cont">
            <div class="title">
                Teslimat Bilgileri
            </div>

            {if $data->teslimyeri == "montaj_noktasina"}
            {$firma = $this->extraservices->montaj_noktalari($firma_id)}
            {$is_montage_point = true}
            <div class="line">
                <div class="line-info">Firma Adı</div>
                <div class="line-value">{$firma->firma_adi}</div>
            </div>
            <div class="line">
                <div class="line-info">Firma Adresi</div>
                <div class="line-value">{$firma->firma_adresi}</div>
            </div>
            <div class="line">
                <div class="line-info">Şehir</div>
                <div class="line-value">{$firma->il}</div>
            </div>
            <div class="line">
                <div class="line-info">İlçe</div>
                <div class="line-value">{$firma->ilce}</div>
            </div>
            <div class="line">
                <div class="line-info">Montaj Noktası Telefonu</div>
                <div class="line-value">{$firma->firma_tel1}</div>
            </div>
            {elseif  $data->teslimyeri == "teslim_adresine"}
            <div class="line">
                <div class="line-info">Adı Soyadı</div>
                <div class="line-value">{$data->teslim_edilecek_adi}</div>
            </div>
            <div class="line">
                <div class="line-info">Açık Adres</div>
                <div class="line-value">{$data->teslim_adres}</div>
            </div>
            <div class="line">
                <div class="line-info">Şehir</div>
                <div class="line-value">{$this->extraservices->ilbyid($data->teslim_il)}</div>
            </div>
            <div class="line">
                <div class="line-info">İlçe</div>
                <div class="line-value">{$this->extraservices->ilcebyid($data->teslim_ilce)}</div>
            </div>
            <div class="line">
                <div class="line-info">Cep Telefonu</div>
                <div class="line-value">{$data->teslim_edilecek_telefonu}</div>
            </div>
            {else}

            <div class="line">
                <div class="line-info">Adı Soyadı</div>
                <div class="line-value">{if $data->ticari_unvani}{$data->ticari_unvani}{else}{$data->adi} {$data->soyadi}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Açık Adres</div>
                <div class="line-value">{if $data->kur_adres}{$data->kur_adres}{else}{$data->adres}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Şehir</div>
                <div class="line-value">{if $data->kur_il}{$this->extraservices->ilbyid($data->kur_il)}{else}{$this->extraservices->ilbyid($data->il)}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">İlçe</div>
                <div class="line-value">{if $data->kur_ilce}{$this->extraservices->ilcebyid($data->kur_ilce)}{else}{$this->extraservices->ilcebyid($data->ilce)}{/if}</div>
            </div>
            <div class="line">
                <div class="line-info">Cep Telefonu</div>
                <div class="line-value">{if $data->kur_cep_telefonu}{$data->kur_cep_telefonu}{else}{$data->cep_telefonu}{/if}</div>
            </div>
            {/if}
        </div>

        {if $data->teslimyeri == "montaj_noktasina"}
        <div class="cont">
            <div class="title">
                Montaj Bilgileri
            </div>
            <div class="line">
                Montaj randevusu için müşteri hizmetleri seninle irtibata geçecektir.
                Dilersen <a href="tel:00908508111101">0850 811 11 01</a>’den arayıp bizimle iletişime geçebilirsin.
            </div>
        </div>
        {/if}

        <div class="cont">
                <div class="title">
                    Mesafeli Satış Sözleşmesi
                </div>
                <div class="line">
                    Detayları tekrar okumak istersen aşağıdaki linke tıkla.<p>
                        <a target="_blank" href="https://www.lastikcim.com.tr/pages/mesafeli_satis_sozlesmesi/{$order->hash_value}">Mesafeli Satış Sözleşmesini Gör</a></p>
                </div>
        </div>
        <div class="cont">
		 <div class="title">
                    Ön Bilgilendirme Formu
                </div>
                <div class="line">
                    Detayları tekrar okumak istersen aşağıdaki linke tıkla.
                    <p>
                        <a target="_blank" href="https://www.lastikcim.com.tr/pages/on_bilgilendirme_formu/{$order->hash_value}">Ön Bilgilendirme Formunu Gör</a></p>
                </div>
		</div>
        <div class="upper_footer">
            Farklı bir sorun varsa lütfen <a class="mail" href="mailto:destek@lastikcim.com.tr">destek@lastikcim.com.tr</a>’ye mail at.
        </div>
        <div class="footer">
            <div class="website-address"><a href="https://www.lastikcim.com.tr" target="_blank">www.lastikcim.com.tr</a></div>
            <div class="w-2"></div>
            <div class="tel">
                <img src="https://www.lastikcim.com.tr/temalar/tema/assets/img/mail_images/tel_min.png">
                <div class="phone-number"><a href="tel:00908508111101">0850 811 11 01</a></div>
            </div>
        </div>
    </div></div>
<!-- <![endif]-->
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>{$iletisim_talep->urun_adi} Stoğa Geldi</title>
    <style type="text/css">
        body{
            width: 100%;
            font-family: "Arial","Arial Black",sans-serif;
            color: #333;
            text-align: center;
            background-color: #d3d3d3;
        }
        .full{
            width: 100%;
            text-align: center;
            color: #333;
            background-color: #d3d3d3;
        }
        .container{
            max-width: 700px;
            text-align: left;
            display: inline-block;
            background-color: #fff;
            color: #333;
            margin-bottom : 64px;
            margin-top : 32px;
        }
        .logo{
            padding: 19px 5% 24px 5%;
        }
        .logo > img{
            width: 200px;
        }
        .content{
            background-color: #f3f3f3;
            padding: 10px 5% 10px 5%;
        }
        .cont{
            padding: 10px 5% 10px 5%;
        }
        .upper_footer{
            padding: 38px 5% 26px 5%;
            font-size: 0.8em;
        }
        .footer{
            font-size: 18px;
            font-weight: 600;
            line-height: 21px;
            background-color: #f3f3f3;
            color: #0e76bc;
            padding: 10px 5% 10px 5%;
        }
        .footer > div{
            display: inline-block;
        }
        .footer > div > div{
            display: inline-block;
        }
        .footer .tel img{
            position: relative;
            top: 4px;
            margin-right: 8px;
        }
        .w-2{
            width: 2em;
        }
        .content .call{
            padding: 10px 0 5px 0;
            font-style: italic;
            font-weight: 600;
        }
        .content .message_title{
            padding: 5px 0 5px 0;
            font-weight: 500;
            font-size: 20px;
            color: #0e76bc;
        }
        .content .description{
            padding: 0.8em 0 1em 0;
            font-weight: 400;
            font-size: 15px;
        }
        .upper_footer > .mail{
            font-weight: 600;
            text-decoration: underline;
            color: #333333;
        }
        .footer a{
            text-decoration: none;
            color: #0e76bc;
        }
        .content a.id{
            color: #0e76bc;
        }
        table{
            text-align:left;
            width: 100%;
            line-height: 24px;
        }
        table thead{
            font-size: 1.2em;
            color: #0e76bc;
        }
        .cont{
            font-weight:600;
            display: block;
        }
        .cont .title{
            font-size: 20px;
            padding: 8px 0 8px 0;
            color: #0e76bc;
            font-weight:600;
            margin-bottom: 10px;
        }
        .cont .items .item{
            display: table;
            height: 100%;
            width: 100%;
        }
        .cont .blue-600{
            color: #0e76bc;
            font-weight:600 !important;
            font-size: 1.0em !important;
            border-top: 1px solid #ccc;
        }
        .cont .line{
            display: table;
            font-size: 14px;
            line-height: 25px;
            vertical-align: middle;
            font-weight:500;
            width: 100%;
            border-bottom: 1px solid #ccc;
            padding: 12px 0 12px 0 !important;
        }
        .cont .description{
            width: 46%;
            display: inline-block;
        }
        .cont .line-val{
            width: 40%;
            display: inline-block;
        }
        .cont .line-value{
            width: 64%;
            vertical-align: middle;
            display: inline-block;
        }
        .cont .line-info{
            width: 36%;
            vertical-align: middle;
            font-weight: 600;
            display: inline-block;
        }
        .cont .total{
            width: 14%;
            display: inline-block;
            text-align: right;
        }
        .cont .items > .item .picture{
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            width: 14%;
        }
        .cont .items > .item .name{
            display: inline-block;
            vertical-align: middle;
            width: 58%;
        }
        .cont .items > .item .amount{
            display: inline-block;
            vertical-align: middle;
            width: 14%;
        }
        .cont .items > .item .price{
            display: inline-block;
            vertical-align: middle;
            width: 14%;
            text-align: right;
        }
        .cont .items > .item .picture img{
            vertical-align: middle;
            display: inline-block;
            width: 100%;
        }
        .cont .w-50{
            display: table-cell;
            width: 50%;
        }
    </style>
</head>
<body>
<!--[if mso]>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <tr><td>
        <img width="200px" src="http://admin.lastikcim.com.tr/temalar/tema/assets/logo.png" alt=""/></td></tr>
</table>
<table width="600" cellpadding="12" style="background-color: #f3f3f3;">
    <tr><td>
        {$iletisim_talep->adi} {$iletisim_talep->soyadi} Merhaba</td></tr>
    <tr><td>
         Beklediğin ürün aşağıdaki fiyattan tedarik edilebilmektedir. Bu ürünü aşağıdaki fiyatla satın almak istiyorsanız <a href="tel:00908508111101">çağrı merkezimize</a> ulaşın. 
    </td></tr>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <tr>
        <td width="50"><img width="50" src="https://www.lastikcim.com.tr/resimler/{$iletisim_talep->url_duzenle}.jpg" style="max-width: 1200px;"/></td>
        <td width="350">{$iletisim_talep->urun_adi}</td>
        <td width="200">{$satis_fiyati} TL</td>
    </tr>
</table>
<table width="600" cellpadding="15" style="background-color: #fff;">
    <tr><td>
        Farklı bir sorun varsa lütfen <a class="mail" href="mailto:destek@lastikcim.com.tr">destek@lastikcim.com.tr</a>’ye mail at.
    </td></tr>
</table>
<table cellpadding="15" style="background-color: #f3f3f3;">
    <tr><td><a href="https://www.lastikcim.com.tr" target="_blank">www.lastikcim.com.tr</a>
    </td><td><img style="max-height: 10px" src="https://www.lastikcim.com.tr/temalar/tema/assets/img/mail_images/tel_min.png"><a href="tel:00908508111101"> 0850 811 11 01</a></td></tr>
</table>
<![endif]-->
<!--[if !mso]> <!---->
<div class="full">
    <div class="container">
        <div class="logo">
            <img src="http://admin.lastikcim.com.tr/temalar/tema/assets/logo.png" alt=""/>
        </div>
        <div class="content">
            <div class="call">
                {$iletisim_talep->adi} {$iletisim_talep->soyadi} Merhaba
            </div>
            <div class="message_title">
            </div>
            <div class="description">
               Beklediğin ürün aşağıdaki fiyattan tedarik edilebilmektedir. Bu ürünü aşağıdaki fiyatla satın almak istiyorsanız <a href="tel:00908508111101">çağrı merkezimize</a> ulaşın. 
            </div>
        </div>
		<div class="cont">
            <div class="title">Listendeki Ürünler</div>
			<div class="items">
				<div class="item">
					<div class="picture">
						<img src="https://www.lastikcim.com.tr/resimler/{$iletisim_talep->url_duzenle}.jpg"/>
					</div>
					<div class="name">
						&nbsp; {$iletisim_talep->urun_adi}
					</div>
					<div class="amount">
					</div>
					<div class="price">
						{$satis_fiyati} TL
					</div>
				</div>
			</div>
        </div>
        <div class="upper_footer">
            Farklı bir sorun varsa lütfen <a class="mail" href="mailto:destek@lastikcim.com.tr">destek@lastikcim.com.tr</a>’ye mail at.
        </div>
        <div class="footer">
            <div class="website-address"><a href="https://www.lastikcim.com.tr" target="_blank">www.lastikcim.com.tr</a></div>
            <div class="w-2"></div>
            <div class="tel">
                <img src="https://www.lastikcim.com.tr/temalar/tema/assets/img/mail_images/tel_min.png">
                <div class="phone-number"><a href="tel:00908508111101">0850 811 11 01</a></div>
            </div>
        </div>
    </div></div>
<!-- <![endif]-->
</body>
</html>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Cari Hesap Aç</h4>
</div>
<div class="modal-body">
	{$data =  $order->data|json_decode}
	{$urunler = $order->urunler|json_decode}

	{foreach from=$urunler item=urun}

		{if count($urun->hizmetler) > 0}
            {foreach from=$urun->hizmetler item=hizmet}
	            {if $hizmet->firma_id} 
	            	{$firma_id = $hizmet->firma_id}
	            {/if}
            {/foreach}
        {/if}

	{/foreach}

	{if $data->teslimyeri == "montaj_noktasina"}
		{$firma = $this->extraservices->montaj_noktalari($firma_id)}
		{$teslimat_adresi_il 	= $firma->il}
		{$teslimat_adresi_ilce 	= $firma->ilce}
		{$teslimat_adresi_tel1 	= $firma->firma_tel1}
		{$teslimat_adresi_tel2 	= "0000000000"}
		{$teslimat_adresi_pk 	= "00000"}
		{$teslimat_adresi_adres = $firma->firma_adresi|cat:" "|cat:$firma->firma_adi}
	{elseif  $data->teslimyeri == "teslim_adresine"}
		{$teslimat_adresi_il 	= $this->extraservices->il($data->teslim_il)}
		{$teslimat_adresi_ilce 	= $this->extraservices->ilce($data->teslim_ilce)}
		{$teslimat_adresi_tel1 	= $data->teslim_edilecek_telefonu}
		{$teslimat_adresi_tel2 	= $data->ev_telefonu}
		{$teslimat_adresi_pk 	= $data->teslim_posta_kodu}
		{$teslimat_adresi_adres = $data->teslim_adres|cat:" "|cat:$data->adi|cat:" "|cat:$data->soyadi}
	{else}
		{if $data->ticari_unvani}
			{$teslimat_adresi_il 	= $this->extraservices->il($data->kur_il)}
			{$teslimat_adresi_ilce 	= $this->extraservices->ilce($data->kur_ilce)}
			{$teslimat_adresi_tel1 	= $data->kur_cep_telefonu}
			{$teslimat_adresi_tel2 	= "0000000000"}
			{$teslimat_adresi_pk 	= $data->kur_posta_kodu}
			{$teslimat_adresi_adres = $data->kur_adres|cat:" "|cat:$data->ticari_unvani}
		{else}
			{$teslimat_adresi_il 	= $this->extraservices->il($data->il)}
			{$teslimat_adresi_ilce 	= $this->extraservices->ilce($data->ilce)}
			{$teslimat_adresi_tel1 	= $data->cep_telefonu}
			{$teslimat_adresi_tel2 	= $data->ev_telefonu}
			{$teslimat_adresi_pk 	= $data->posta_kodu}
			{$teslimat_adresi_adres = $data->adres|cat:" "|cat:$data->adi|cat:" "|cat:$data->soyadi}
		{/if}
	{/if}
	<div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-red-sunglo">
                <i class="fa fa-user"></i>
                <span class="caption-subject bold uppercase"> Cari Bilgileri </span>
            </div>
        </div>
        <div class="portlet-body form">
        	<form id="carihesapbilgileri" action="javascript:void(-1)" autocomplete="off">
		        <div class="form-body">
		            <div class="form-group">
		                <label for="cariunvan1">Cari Ünvan 1</label>
		                <input id="cariunvan1" name="cari_unvan_1" type="text" class="form-control" placeholder="Cari Ünvan 1" {if $data->ticari_unvani}value="{$data->ticari_unvani|mb_strtoupper}"{else}value="{$data->adi|mb_strtoupper} {$data->soyadi|mb_strtoupper}"{/if} />
		            </div>
		            <div class="form-group">
		                <label for="cariunvan2">Cari Ünvan 2</label>
		                <input id="cariunvan2" name="cari_unvan_2" type="text" class="form-control" placeholder="Cari Ünvan 2" />
		            </div>
		            <div class="form-group">
		            	<label for="carivergidairesi">Cari Vergi Dairesi</label>
		            	<input type="text" id="carivergidairesi" name="cari_vergi_dairesi" class="form-control" placeholder="Cari Vergi Dairesi" {if $data->ticari_unvani}value="{$data->vergi_dairesi|mb_strtoupper}"{else}value=""{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="cariverginumarasi">Cari Vergi Numarası</label>
		            	<input type="text" id="cariverginumarasi" name="cari_vergi_numarasi" class="form-control" placeholder="Cari Vergi Numarası" {if $data->ticari_unvani}value="{$data->vergi_no}"{else}value="{$data->tc_kimlik_no}"{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="cariemail">Cari E-mail</label>
		            	<input type="text" name="cari_email" id="cariemail" class="form-control" placeholder="Cari E-mail" value="{$data->email}" />
		            </div>
		        </div>
		    </form>
		</div>
	</div>
	<div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-red-sunglo">
                <i class="fa fa-archive"></i>
                <span class="caption-subject bold uppercase"> Fatura Adresi </span>
            </div>
        </div>
        <div class="portlet-body form">
        	<form id="faturaadresi" action="javascript:void(-1)" autocomplete="off">
		        <div class="form-body">
		            <div class="form-group">
		                <label for="cadde">Adres Satır 1</label>
		                <input id="cadde" name="cadde" type="text" class="form-control" placeholder="Adres Satır 1" {if $data->ticari_unvani}value="{substr($data->kur_adres, 0, 50)|mb_strtoupper}"{else}value="{substr($data->adres, 0, 50)|mb_strtoupper}"{/if} />
		            </div>
		            <div class="form-group">
		                <label for="sokak">Adres Satır 2</label>
		                <input id="sokak" name="sokak" type="text" class="form-control" placeholder="Adres Satır 2" {if $data->ticari_unvani}value="{substr($data->kur_adres, 50, 50)|mb_strtoupper}"{else}value="{substr($data->adres, 50, 50)|mb_strtoupper}"{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="posta_kodu">Posta Kodu</label>
		            	<input type="text" id="posta_kodu" name="posta_kodu" class="form-control" placeholder="Posta Kodu" {if $data->ticari_unvani}value="{$data->kur_posta_kodu}"{else}value="{$data->posta_kodu}"{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="ilce">İlçe</label>
		            	<input type="text" id="ilce" name="ilce" class="form-control" placeholder="İlçe" {if $data->ticari_unvani}value="{$firma->ilce|mb_strtoupper}"{else}value="{$this->extraservices->ilce($data->ilce)|mb_strtoupper}"{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="il">İl</label>
		            	<input type="text" id="il" name="il" class="form-control" placeholder="İl" {if $data->ticari_unvani}value="{$firma->il|mb_strtoupper}"{else}value="{$this->extraservices->il($data->il)|mb_strtoupper}"{/if} />
		            </div>
		            <div class="form-group">
		            	<label for="ulke">Ülke</label>
		            	<input type="text" id="ulke" name="ulke" class="form-control" placeholder="Ülke" value="Türkiye" />
		            </div>
		            <div class="form-group">
		            	<label for="ulke_kodu">Ülke Kodu</label>
		            	<input type="text" id="ulke_kodu" name="ulke_kodu" class="form-control" placeholder="Ülke Kodu" value="90" />
		            </div>
		            <div class="form-group">
		            	<label for="bolge_kodu">Bölge Kodu</label>
		            	<input type="text" id="bolge_kodu" name="bolge_kodu" class="form-control" placeholder="Bölge Kodu" value="{if $data->ticari_unvani}{caritelefon($data->kur_cep_telefonu, 1)}{else}{caritelefon($data->ev_telefonu, 1)}{/if}" />
		            </div>
		            <div class="form-group">
		            	<label for="tel_1">Tel 1</label>
		            	<input type="text" id="tel_1" name="tel_1" class="form-control" placeholder="Tel 1" value="{if $data->ticari_unvani}{caritelefon($data->kur_cep_telefonu, 2)}{else}{caritelefon($data->ev_telefonu, 2)}{/if}" />
		            </div>
		            <div class="form-group">
		            	<label for="tel_2">Tel 2</label>
		            	<input type="text" id="tel_2" name="tel_2" class="form-control" placeholder="Tel 2" value="{caritelefon($data->cep_telefonu)}" />
		            </div>
		        </div>
		    </form>
		</div>
	</div>
	<div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-red-sunglo">
                <i class="fa fa-send"></i>
                <span class="caption-subject bold uppercase"> Sevk Adresi </span>
            </div>
        </div>
        <div class="portlet-body form">
        	<form id="sevkadresi" action="javascript:void(-1)" autocomplete="off">
		        <div class="form-body">
		            <div class="form-group">
		                <label for="cadde">Adres Satır 1</label>
		                <input id="cadde" name="cadde" type="text" class="form-control" placeholder="Adres Satır 1" value="{substr($teslimat_adresi_adres, 0, 50)|mb_strtoupper}" />
		            </div>
		            <div class="form-group">
		                <label for="sokak">Adres Satır 2</label>
		                <input id="sokak" name="sokak" type="text" class="form-control" placeholder="Adres Satır 2" value="{substr($teslimat_adresi_adres, 50, 50)|mb_strtoupper}" />
		            </div>
		            <div class="form-group">
		            	<label for="posta_kodu">Posta Kodu</label>
		            	<input type="text" id="posta_kodu" name="posta_kodu" class="form-control" placeholder="Posta Kodu" value="{$teslimat_adresi_pk}" />
		            </div>
		            <div class="form-group">
		            	<label for="ilce">İlçe</label>
		            	<input type="text" id="ilce" name="ilce" class="form-control" placeholder="İlçe" value="{$teslimat_adresi_ilce|mb_strtoupper}" />
		            </div>
		            <div class="form-group">
		            	<label for="il">İl</label>
		            	<input type="text" id="il" name="il" class="form-control" placeholder="İl" value="{$teslimat_adresi_il|mb_strtoupper}" />
		            </div>
		            <div class="form-group">
		            	<label for="ulke">Ülke</label>
		            	<input type="text" id="ulke" name="ulke" class="form-control" placeholder="Ülke" value="TÜRKİYE" />
		            </div>
		            <div class="form-group">
		            	<label for="ulke_kodu">Ülke Kodu</label>
		            	<input type="text" id="ulke_kodu" name="ulke_kodu" class="form-control" placeholder="Ülke Kodu" value="90" />
		            </div>
		            <div class="form-group">
		            	<label for="bolge_kodu">Bölge Kodu</label>
		            	<input type="text" id="bolge_kodu" name="bolge_kodu" class="form-control" placeholder="Bölge Kodu" value="{caritelefon($teslimat_adresi_tel1, 1)}" />
		            </div>
		            <div class="form-group">
		            	<label for="tel_1">Tel 1</label>
		            	<input type="text" id="tel_1" name="tel_1" class="form-control" placeholder="Tel 1" value="{caritelefon($teslimat_adresi_tel1, 2)}" />
		            </div>
		            <div class="form-group">
		            	<label for="tel_2">Tel 2</label>
		            	<input type="text" id="tel_2" name="tel_2" class="form-control" placeholder="Tel 2" value="{caritelefon($teslimat_adresi_tel2)}" />
		            </div>
		        </div>
	        </form>
		</div>
	</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
    <button type="button" class="btn blue" id="carihesapacma" onclick="carihesapbilgileri()">Kaydet</button>
</div>
<script type="text/javascript">
	function carihesapbilgileri() {
		var carihesapbilgileri = $('form#carihesapbilgileri').serialize();
		var faturaadresi = $('form#faturaadresi').serialize();
		var sevkadresi = $('form#sevkadresi').serialize();
		$('button#carihesapacma').attr('disabled', 'true');
		$.ajax({
	  		type:'POST',
	  		dataType:'json',
	  		url:'{site_url('orders/mikroyacari/'|cat:$order->id)}',
	  		data: carihesapbilgileri,
	  		success:function(cevap){
	  			if (cevap.status) {

	  				/**/
	  				$.ajax({
				  		type:'POST',
				  		dataType:'json',
				  		url:'{site_url('orders/mikroyacariadres/'|cat:$order->id|cat:'/fatura')}',
				  		data: faturaadresi,
				  		success:function(cevap){
				  			if (cevap.status) {

				  				/**/
				  				$.ajax({
							  		type:'POST',
							  		dataType:'json',
							  		url:'{site_url('orders/mikroyacariadres/'|cat:$order->id|cat:'/sevk')}',
							  		data: sevkadresi,
							  		success:function(cevap){
							  			if (cevap.status) {
							  				location.reload();
							  			} else {
							  				$('button#carihesapacma').removeAttr('disabled');
							  			}
						  			}
			  					});
				  				/**/

				  			}
			  			}
  					});
	  				/**/

	  			}
	  		}
	  	});
	}
</script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Cari Sipariş Aç</h4>
</div>
<div class="modal-body">
	{if $spres['siparis_id_alinan'] && is_numeric($spres['siparis_id_alinan'])}
		<p>Siparişiniz {if $mikro == "tatko"}LP{else}ID{/if}-{$spres['siparis_id_alinan']} Sipariş Numarası ile Mikro Sistemine Kayıt Edilmiştir.</p>
	{/if}
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
</div>
<script type="text/javascript">
	setTimeout(function(){ 
		location.reload();
	}, 3000);
</script>
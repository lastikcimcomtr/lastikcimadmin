{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
	<div class="row" style="margin-bottom: 10px;">
		<div class="col-md-12">
			<form class="row" method="post" action="" autocomplete="off">
				<div class="form-group col-md-2">
					<label for="urun_adi">Ürün Adı</label>
					<input type="text" class="form-control" id="urun_adi" name="urun_adi" placeholder="Ürün Adı" value="{$sdata['urun_adi']}" />
				</div>
				<div class="form-group col-md-2">
					<label>Müşteri Adı</label>
					<input type="text" name="adi" class="form-control" value="{$sdata['adi']}" placeholder="Müşteri Adı" />
				</div>
				<div class="form-group col-md-2">
					<label>Müşteri Soyadı</label>
					<input type="text" name="soyadi" class="form-control" value="{$sdata['soyadi']}" placeholder="Müşteri Soyadı" />
				</div>
				<div class="col-md-12">
					<button type="submit" class="btn btn-success">Arama Yap</button>
				</div>
			</form>
		</div>
	</div>
    <div class="row">
       <div class="col-md-12"> 
          <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr >   
                        <th class="islem_bilgilendirme"  style="text-align: right;">
                         <span class="btn btn-sm btn-default hizliduzenle"><i class="fa fa-pencil"></i></span>      
                         <span id="islembilgisitext"> Mesaja Bak </span>
                     </th>
                 </tr>
             </thead>
         </table>

            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr> 
                        <th>İstenen Ürün</th>
                        <th>İstek Tipi</th>
                        <th>İstek Tarihi</th>
                        <th>Fiyat</th>
                        <th width="1%">Adı Soyadı</th> 
                        <th width="1%">Telefon</th> 
                        <th style="width:5px"></th> 
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$istekler item=istek}
                    <tr style="font-size: 11px !important;{if $istek->tedarik_fiyati||$istek->note}background-color:palegreen{/if}"> 
                        <td>{$istek->urun_adi}</td>
                        <td>{$istek->kategori}</td>
                        <td>{date('d.m.Y H:m',$istek->tarih)}</td>
                        <td>{$istek->urun_fiyat}</td>
                        <td title="{$istek->adi} {$istek->soyadi}">{truncate_str("`$istek->adi` `$istek->soyadi`",20)}</td>
                        <td>{$istek->cep_telefonu}</td>
                         <td>
                            <button class="btn btn-sm btn-default detayliduzenle" onclick="completeUrunTalep({$istek->id})" title="Dönüş Yapıldı">
                            <i class="fa fa-check"></i>
                        </button> 
                        <a class="btn btn-sm btn-default hizliduzenle" title="Mesaj Detayı" data-toggle="modal" href="#mesaj_{$istek->id}">
                            <i class="fa fa-pencil"></i>
                        </a>                                                
                        <div class="modal fade" id="mesaj_{$istek->id}" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Mesaj Detayı</h4>
                                    </div>
                                    <div class="modal-body" id="talep_{$istek->id}">
                                        <div class="row"><div class="col-md-3"><b>İstek Tipi</b>:</div><div class="col-md-9">{$istek->kategori}</div></div>
                                        <div class="row"><div class="col-md-3"><b>Tarih</b>:</div><div class="col-md-9">{date('d.m.Y H:mm',$istek->tarih)}</div></div>
                                        <div class="row"><div class="col-md-3"><b>Adı</b>:</div><div class="col-md-9">{$istek->adi}</div></div>
                                        <div class="row"><div class="col-md-3"><b>Soyadı</b>:</div><div class="col-md-9">{$istek->soyadi}</div></div>
                                        <div class="row"><div class="col-md-3"><b>Telefon</b>:</div><div class="col-md-9">{$istek->cep_telefonu}</div></div>
                                        <div class="row"><div class="col-md-3"><b>E posta</b>:</div><div class="col-md-9">{$istek->email}</div></div>
                                        <div class="row"><div class="col-md-12"><b>Mesaj</b>:</div><div class="col-md-12">{$istek->mesaj}</div></div>
                                        <div class="row"><div class="col-md-12"><b>Tedarikçi</b>:</div><div class="col-md-12">
										<input name="tedarikci" class="form-control" placeholder="tedarikci" value="{$istek->tedarikci}" /></div></div>
                                        <div class="row"><div class="col-md-12"><b>Tedarik Fiyatı</b>:</div><div class="col-md-12">
										<input name="tedarik_fiyati" class="form-control" placeholder="Tedarik Fiyatı" value="{$istek->tedarik_fiyati}" /></div></div>
                                        <div class="row"><div class="col-md-12"><b>Satış Fiyatı</b>:</div><div class="col-md-12">
										<input name="satis_fiyati" class="form-control" placeholder="Satış Fiyatı" value="{$istek->satis_fiyati}" /></div></div>
                                        <div class="row"><div class="col-md-12"><b>Not</b>:</div><div class="col-md-12">
										<textarea name="istek_note" class="form-control">{$istek->note}</textarea></div></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="modal-footer" style="border-top: 0px;padding-top: 0px;">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
                                        <button onclick="update_iletisim_talep({$istek->id});" type="button" class="btn blue" data-dismiss="modal">Kaydet</button>

                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>

                            <!-- /.modal-dialog -->
                        </div>

                        <a class="btn btn-sm btn-default hizliduzenle" style="background-color: #0ca916" title="İletişim isteğine çevir"  href="./iletisim_talebine_cevir/{$istek->id}">
                            <i class="fa fa-exchange"></i>
                        </a>       
                    </td>
                    </tr>
                    {/foreach}     
                </tbody>
            </table>       
			{$links}
        </div>
    </div>
</div>
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
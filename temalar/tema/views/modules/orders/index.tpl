{include file="base/header.tpl"}
{$method = $this->router->fetch_method()}
<style type="text/css">
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <form class="row" method="post" action="" autocomplete="off">
                            <div class="form-group col-md-2">
                                <label for="siparisid">Sipariş ID</label>
                                <input type="text" name="id" class="form-control" id="siparisid" placeholder="Sipariş ID" value="{$sdata['id']}" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Müşteri İsmi</label>
                                <input type="text" name="isim" class="form-control" value="{$sdata['isim']}" placeholder="Müşteri İsmi" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Cari Kodu</label>
                                <input type="text" name="cari" class="form-control" value="{$sdata['cari']}" placeholder="Müşteri Cari Kodu" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Mail Adresi</label>
                                <input type="text" name="email" class="form-control" value="{$sdata['email']}" placeholder="Müşteri Mail Adresi" />
                            </div>
                            {if $method <> "hatalilar"}
                            <div class="form-group col-md-2">
                                <label for="siparisdurumu">Sipariş Durumu</label>
                                <select class="form-control" name="durumu">
                                    <option value="">Seçim Yapınız</option>
                                    {foreach from=$durumlar item=durum }
                                    <option value="{$durum->id}" {if  $durum->id == $sdata['durumu']}selected="selected"{/if}>{$durum->durum}</option>
                                    {/foreach} 
                                </select>  
                            </div> 
                            <div class="form-group col-md-2">
                                <label for="durumguvenlik">Ödeme Tipi</label>
                                <select name="odeme_tipi" class="form-control" id="durumguvenlik">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" {if $sdata['odeme_tipi'] == "1"}selected{/if}>Havale</option>
                                    <option value="2" {if $sdata['odeme_tipi'] == "2"}selected{/if}>Kredi Kartı</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="durumodeme">Ödeme</label>
                                <select name="durum_odeme" class="form-control" id="durumodeme">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" {if $sdata['durum_odeme'] == "1"}selected{/if}>Başarılı</option>
                                    <option value="0" {if $sdata['durum_odeme'] == "0"}selected{/if}>Başarısız</option>
                                </select>
                            </div>
                            {/if}
                            <div class="form-group col-md-2">
                                <label for="postip">Pos</label>
                                <select name="pos_tip" class="form-control" id="postip">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="NestPay" {if $sdata['pos_tip'] == "NestPay"}selected{/if}>NestPay</option>
                                    <option value="Gvp" {if $sdata['pos_tip'] == "Gvp"}selected{/if}>Gvp</option>
                                    <option value="Posnet" {if $sdata['pos_tip'] == "Posnet"}selected{/if}>Posnet</option>
                                    <option value="ziraatmvp" {if $sdata['pos_tip'] == "ziraatmvp"}selected{/if}>ziraatmvp</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tarih Aralığı</label>
                                <div class="input-group input-large date-picker input-daterange" data-date="{date('d.m.Y')}" data-date-format="dd.mm.yyyy">
                                    <input type="text" class="form-control" name="tarih_min" value="{$sdata['tarih_min']}" />
                                    <span class="input-group-addon"> - </span>
                                    <input type="text" class="form-control" name="tarih_max" value="{$sdata['tarih_max']}" />
                                    <span style="border-width :1px;" class="input-group-addon text-danger" onclick="return $(this).closest('.form-group').find('input').val('')"> <i class="fa fa-times"></i> </span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Arama Yap</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">                   
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
		                    <tr> 
		                        <th> Ref No  </th>
		                        <th> Üye Adı Soyadı </th>
		                        <th width="10%"> Sipariş Durumu </th>
		                        <th width="8%"> Sipariş Tarihi </th> 
		                        <th width="5%"> T.Tutar </th> 
		                        <th width="5%"> Ö.Tutar </th>
                                <th width="1%"> Ödeme </th>
                                <th width="1%"> Detay </th>
                                <th width="1%"> T.Durumu </th>
		                        <th width="1%"> Cari </th>
		                        <th width="1%"> Lstkc </th>
		                        <th width="1%"> Tedarikci </th>
		                        <th> İşlemler </th> 
		                    </tr>
		                </thead>
                        <tbody>
                            <tbody>
                                {foreach $orders as $order}

                                {$data =  $order->data|json_decode}
			                    {$urunler = $order->urunler|json_decode}
			                    {$hizmetler = $urunler[0]->hizmetler}
			                    {$odemedata = $order->odemedata|json_decode}
			                    {$odemebanka =  $this->extraservices->paytype($odemedata)}

			                    {if  $odemedata->artitaksit}
			                    {$installment  = ($odemedata->installment * 1 ) + ($odemedata->artitaksit * 1) }
			                    {else}
			                    {$installment  = $odemedata->installment} 
			                    {/if}

                                <tr {if $order->durumu == 2} style="background-color:palegreen"{/if}{if $order->durumu == 11} style="background-color:#f9fe3e"{/if}{if $order->durumu == 4 || $order->durumu == 9} style="background-color:#ff6f6f"{/if}>
                                    <td>{$order->id}</td>
                                    <td title="{if $data->ticari_unvani} {$data->ticari_unvani} {else}{"`$data->adi` `$data->soyadi`"} {/if}">
									{if $data->ticari_unvani} {truncate_str($data->ticari_unvani,30)} {else}{truncate_str("`$data->adi` `$data->soyadi`",30)} {/if}</td>
                                    <td>
                                    	<select class="form-control" onchange="siparis_durum_degis({$order->id});" id="siparis_durumu_{$order->id}">
											{foreach from=$durumlar item=durum }
											<option value="{$durum->id}" {if  $durum->id == $order->durumu}selected="selected"{/if}>{$durum->durum}</option>
											{/foreach} 
										</select>                                    	
                                    </td>
                                    <td>{date('d.m.Y H:i',$order->tarih)}</td>
                                    <td>{$this->extraservices->toplamtutar($order->id)}</td>
                                    <td>{if $odemedata->son_fiyat} {$odemedata->sepet_tutar} {else} - {/if}</td>
                                    {if $order->posdetayid && $order->odeme_tipi == 2}
                                    <td align="center">{if $order->durum_odeme == 1}<i class="fa fa-check" style="color: green;"></i>{else}<i class="fa fa-times" style="color: red;"></i>{/if}</td>
                                    <td>
                                        <a href="{site_url('orders/posdetay/'|cat:$order->posdetayid)}" data-target="#detay" data-toggle="modal" class="btn btn-sm btn-warning">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                    {else if $order->odeme_tipi == 2}
                                    <td colspan="2" style="background-color: #fc8383;"><center><b>Hatalı</b></center></td>
                                    {else}
                                    <td colspan="2" style="background-color: #ededed;"><center><b>Havale</b></center></td>
                                    {/if}
                                    <td align="center">
                                        {if $order->kargo_tipi}{date('d.m.Y H:i',$order->kargo_tipi)}{else}-{/if}
                                    </td>
                                    <td align="center">
                                    	{if $order->caridata}
											{$carikod = $order->caridata|json_decode}
											{$carikod = $carikod->kod}
											<a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', '{$carikod} Kodu ile Bir Cari Hesap Zaten Açılmış', 'error');" title="cari hesap açılmış"><i class="fa fa-user"></i></a>
										{else}
											<a class="btn btn-icon-only btn-success" href="javascript:carihesapac({$order->id});"><i class="fa fa-user"></i></a>
										{/if}
                                    </td>
                                    <td align="center">
                                    	{if $order->siparis_data}
											{$siparis_id = $order->siparis_data|json_decode}
											{$siparis_id = $siparis_id->siparis_id_alinan}
											<a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', 'LC-{$siparis_id} Numarası ile Bir Sipariş Zaten Girilmiş', 'error');" title="sipariş işlenmiş"><i class="fa fa-gears"></i></a>
										{else}
											<a class="btn btn-icon-only btn-success" href="javascript:carisiparisac({$order->id});"><i class="fa fa-gears"></i></a>
										{/if}
                                    </td>
                                    <td align="center">
                                    	{if $order->tatko_siparis_data}
                                            {$tatko_siparis_data = $order->tatko_siparis_data|json_decode}
					                        <a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', '{$tatko_siparis_data->urun_maliyet} ₺ maliyet ile {$tatko_siparis_data->tedarikci} tedarikcisine gönderilmiş.', 'error');" title="sipariş işlenmiş"><i class="fa fa-envelope"></i></a>
				                        {else}
				                        	<a class="btn btn-icon-only btn-success" href="javascript:siparistedarikci({$order->id});"><i class="fa fa-envelope"></i></a>
				                        {/if}
                                        {if $order->ups_barcode}
                                            <a class="btn btn-icon-only btn-danger" href="javascript:barkodexist({$order->id});" title="sipariş işlenmiş"><i class="fa fa-barcode"></i></a>
                                        {else}
                                            <a class="btn btn-icon-only btn-success" href="javascript:siparisbarcode({$order->id});"><i class="fa fa-barcode"></i></a>
                                        {/if}
                                        {if $order->irsaliye_data}
                                            <a class="btn btn-icon-only btn-danger" href="javascript:irsaliyeexist({$order->id});" title="irsaliye işlenmiş"><i class="fa fa-file-text-o"></i></a>
                                        {else}
                                            <a class="btn btn-icon-only btn-success" href="javascript:siparisirsaliye({$order->id});"><i class="fa fa-file-text-o"></i></a>
                                        {/if}
                                    </td>
                                    <td>   
                                    	<a href="/orders/duzenle/{$order->id}" target="_blank" style="background-color: #71b100; border-color: #71b100; color: white;" title="Sipariş Düzenle" class="btn btn-sm btn-default">
					                        <i class="fa fa-pencil"></i>
					                    </a>                                 	
                                    	<a onclick="siparisYazdir( {$order->id} );" style="background-color: #703688; border-color: #703688; color: white;" title="Sipariş Detaylarını Yazdır" class="btn btn-sm btn-default" >
					                        <i class="fa fa-print"></i>
					                    </a>
                                        
                                        <a style="background-color: {if $hizmetler}#f5861d{else}#E73434{/if}; border-color: {if $hizmetler}#f5861d{else}#E73434{/if}; color: white;" data-toggle="modal" href="{site_url('orders/montaj_detay/'|cat:$order->id)}" data-target="#montaj_detay" title="Montaj Detayları" class="btn btn-sm btn-default">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                        
                                        <a style="background-color: {if $order->kargo_takip}#1BBC9B{else}#E73434{/if}; border-color: {if $order->kargo_takip}#1BBC9B{else}#E73434{/if}; color: white;" data-toggle="modal" href="{site_url('orders/kargo_detaylari/'|cat:$order->id)}" data-target="#kargo_detay" title="Kargo Detayları" class="btn btn-sm btn-default">
                                            <i class="fa fa-truck"></i>
                                        </a>

                                        <a style="background-color: {if $order->siparis_notu}#703688{else}#E73434{/if}; border-color: {if $order->siparis_notu}#703688{else}#E73434{/if}; color: white;" data-toggle="modal" href="{site_url('orders/siparis_notu/'|cat:$order->id)}" data-target="#siparis_notu" title="Sipariş Notu" class="btn btn-sm btn-default">
                                            <i class="fa fa-list-ol"></i>
                                        </a>
										
                                        <a style="background-color: {if $order->tedarik_not}#703688{else}#E73434{/if}; border-color: {if $order->siparis_notu}#703688{else}#E73434{/if}; color: white;" data-toggle="modal" href="{site_url('orders/tedarik_notu/'|cat:$order->id)}" data-target="#tedarik_notu" title="Tedarik Notu" class="btn btn-sm btn-default">
                                            <i class="fa fa-list-alt"></i>
                                        </a>

                                        <a style="background-color: {if $order->tedarikci_odeme}#1BBC9B{else}#E73434{/if}; border-color: {if $order->tedarikci_odeme}#1BBC9B{else}#E73434{/if}; color: white;" onclick="toggleTedarikciOdeme({$order->id},this);" title="Tedarikçi Ödeme" class="btn btn-sm btn-default">
                                            <i class="fa fa-money"></i>
                                        </a>                							
                                        {*
                                    	<a style="background-color: #f5861d; border-color: #f5861d; color: white;" data-toggle="modal" href="{site_url('orders/siparis_detaylari/'|cat:$order->id)}" data-target="#siparis_detay" title="Sipariş Detayları" class="btn btn-sm btn-default">
						                    <i class="fa fa-pencil-square-o"></i>
						                </a>
						                <a style="background-color: #4B80B6; border-color: #4B80B6; color: white;" data-toggle="modal" href="{site_url('orders/fatura_bilgileri/'|cat:$order->id)}" data-target="#fatura_bilgileri" title="Fatura Bilgileri" class="btn btn-sm btn-default">
						                    <i class="fa fa-pencil"></i>
						                </a>
                                        *}
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </tbody>
                    </table>
                    {$links}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="montaj_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kargo_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tedarik_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="fatura_bilgileri" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carihesap" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carisiparis" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="siparisbarcode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="siparisirsaliye" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $.ajaxSetup ({
        cache: false
    });
    function siparisirsaliye(id){
        var siparisurl = "{site_url('orders/siparisirsaliye')}/"+id;
        swal({
            title: 'Lastikcim İrsaliye',
            text: 'Sipariş İrsaliyesi Göndermek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {
            if (isConfirm) {
                $('#siparisirsaliye').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }
    function carihesapac(id) {
        var hesapurl = "{site_url('orders/carihesapac')}/"+id;
        swal({
            title: 'Cari Hesap',
            text: 'Mikroya Cari Hesap Açmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carihesap').modal('show').find('.modal-body').load(hesapurl);
            }

        });
    }

    function carisiparisac(id) {
        var siparisurl = "{site_url('orders/carisiparisac')}/"+id;
        swal({
            title: 'Lastikcim Sipariş',
            text: 'Lastikcim-e Cari Sipariş Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }



    function siparisbarcode(id) {
        var siparisurl = "{site_url('orders/siparisbarcode')}/"+id;
        swal({
            title: 'Lastikcim Barkod',
            text: 'Sipariş Barkodu Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#siparisbarcode').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }

    function caritatkosiparisac(id) {
        var tatkosiparisurl = "{site_url('orders/carisiparisac')}/"+id+'/tatko';
        swal({
            title: 'Tatko Sipariş',
            text: 'Tatko-ya Cari Sipariş Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(tatkosiparisurl);
            }

        });
    }

    function siparistedarikci(id){
        var tedarikcisiparisurl = "{site_url('orders/tedarikciyeiletformu')}/"+id;
        swal({
            title: 'Tedarikçiye Sipariş Aktar',
            text: 'Siparişi Tedarikçiye Aktarmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {
            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(tedarikcisiparisurl);
            }
        });
    }

    function barkodexist(siparis_id){
        swal({
            title: 'Barkod Zaten Mevcut',
            text: 'Barkodu Tekrar Oluşturmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            icon: 'warning',
            buttons: ["Hayır", "Yeni Barkod"]
        }).then((isConfirm) => {
            if (isConfirm) {
                siparisbarcode(siparis_id);
            }
        });
    }

    function irsaliyeexist(siparis_id){
        swal({
            title: 'İrsaliye Zaten Mevcut',
            text: 'İrsaliyeyi Tekrar Oluşturmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            icon: 'warning',
            buttons: ["Hayır", "Yeni İrsaliye"],
        }).then((isConfirm) => {
            if (isConfirm) {
                siparisirsaliye(siparis_id);
            }
        });
    }
</script>

<iframe id="frame_yazdir" name="frame_yazdir"  src="" style="visibility: hidden; width: 0px; height: 0px;"></iframe>

<script type="text/javascript">
	function siparisYazdir(id){
	    $('#frame_yazdir').attr("src","{site_url('orders/siparisdetay')}/" + id);
	    return false;
	} 

    function toggleTedarikciOdeme(order_id,element) {
        element = $(element);
        $.ajax({
            type:'POST',
            url:'/orders/toggle_tedarikci_odeme/',
            dataType: 'json',
            data: { order_id : order_id },
            success:function(cevap){
                if (cevap) {
                    if(cevap.status == 'error'){
                        element.css('background-color','#E73434').css('border-color','#E73434');
                    }else{
                        element.css('background-color','#1BBC9B').css('border-color','#1BBC9B');
                    }
                    swal({
                        title: 'Başarılı',
                        text: cevap.message,
                        icon: cevap.status,
                    });
                }
            }
        });
    }
</script>


<script type="text/javascript">
    function siparis_durum_degis(sipid) {
		var select = $("#siparis_durumu_"+sipid);
        var durum = $("#siparis_durumu_"+sipid+" option:selected").val();
        var send_data = {
            order_id:sipid,
            durum:durum
        };
        $.get( "{site_url('ajax/order_durum')}", send_data ).done(function( data ) {
			data = JSON.parse(data);
			if(data.status == 'error'){
                 select.val(data.durumu);
				 swal("Başarısız",data.msg,"warning");
                 {literal}
                
			}else{
				 swal({
				  title: "Başarılı",
				  text: "Sipariş Durumu Güncellendi.",
				  timer: 1200
				});
				 if(durum == 4 || durum==9){
				 	 dataLayer.push({
                  'ecommerce': {
                    'refund': {                       
                      'actionField': {{/literal}'id': sipid.toString()}         // Transaction ID. Required for purchases and refunds.
	                    }
	                  }
	                });
				 }
				if(durum == 9 || durum==4){
					select.closest('tr').css('background-color','#ff6f6f');
				}
				if(durum == 2){
					select.closest('tr').css('background-color','palegreen');
				}
			}
            // console.log(data);
        });
    }
</script>
{include file="base/footer_txt.tpl"}
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
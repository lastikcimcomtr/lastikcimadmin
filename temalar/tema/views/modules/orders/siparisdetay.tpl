 {$data =  $order->data|json_decode}
 {$urunler = $order->urunler|json_decode}
 {$odemedata = $order->odemedata|json_decode}
 {$odemebanka =  $this->extraservices->paytype($odemedata)}

  {if  $odemedata->artitaksit}
 {$installment  = ($odemedata->installment * 1 ) + ($odemedata->artitaksit * 1) }
 {else}
 {$installment  = $odemedata->installment} 
 {/if}
 <!DOCTYPE html>

 <html lang="tr">

 <head>
 	<meta charset="utf-8" />
 </head>
 <span class='PrinterFriendly'>
 	<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#F4F4F4;border-collapse:collapse;font-size:11px;font-family:arial;border:1px solid #D7D7D7;">
 		<tr>
 			<td style="padding-left:5px;"><b>Sipariş Bilgileri</b></td>
 		</tr>
 	</table>
 	<table width='700' cellspacing='2' cellpadding='2' align="center" style='font-size:11px;font-family:arial; padding:0px 0px 5px 0px; border-left:1px solid #D7D7D7; border-right:1px solid #D7D7D7;'>
 		<tr>
 			<td width='20%' valign="top"><b>Sipariş No</b></td>
 			<td valign='top' align='center' valign="top">:</td>
 			<td valign="top">{$order->id}</td>
 		</tr>
 		<tr>
 			<td width='20%' valign="top"><b>Sipariş Veren</b></td>
 			<td valign='top' align='center' valign="top">:</td>
 			<td valign="top">{if $data->ticari_unvani} {$data->ticari_unvani} {else}{$data->adi} {$data->soyadi}{/if}</td>
 		</tr>
 		<tr>
 			<td width='20%' valign="top"><b>İp Adresi</b></td>
 			<td valign='top' align='center' valign="top">:</td>
 			<td valign="top">{$order->ip}</td>
 		</tr>
 		<tr>
 			<td valign="top"><b>Ödeme Türü</b></td>
 			<td valign='top' align='center'>:</td>
 			<td valign="top">
 				{$order->odeme_tipi_adi} 
 				{if $odemebanka}{$odemebanka}{/if}
 				{$this->orders->posArtiTaksit($odemedata->sanalpos, $odemedata->installment)}
 			</td>
 		</tr>
 		<tr>
 			<td valign="top"><b>Sipariş Durumu</b></td>
 			<td valign='top' align='center'>:</td>
 			<td valign="top">{$order->durum}</td>
 		</tr> 
 		{*}<tr>
 			<td valign="top"><b>Kargo Seçenekleri</b></td>
 			<td valign='top' align='center'>:</td>
 			<td valign="top">Ups Kargo </td>
 		</tr>{*}
 		<tr>
 			<td valign="top"><b>Sipariş Tarihi</b></td>
 			<td valign='top' align='center'>:</td>
 			<td valign="top">{date('d.m.Y',$order->tarih)}</td>
 		</tr> 	
 	</table>

 	<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#F4F4F4;border-collapse:collapse;font-size:11px;font-family:arial;border:1px solid #D7D7D7;">
 		<tr>
 			<td style="padding:10px;font-size:13px; "> Sepetim </td>
 			<td style="padding:10px;font-size:13px; border-left: 1px solid #f4f4f4;"> Miktar </td>
 			<td style="font-size:13px; border-left: 1px solid #f4f4f4;text-align: right;padding-right: 50px"> Fiyat </td>
 		</tr>
 	</table>	

 	{$kargokdvoran = 18}
 	{$hizmetkdvoran = 18}
 	{$urunlertoplam = 0}
 	{$montajtoplam = 0}
 	{$urunlerkdv = 0}
 	{$urunlerkargo = 0}
 	{$toplam = 0}

 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;border-bottom:1px solid #D7D7D7;">
 		{foreach from=$urunler item=urun}

 		{if $urun->kdv_dahil == 1}
 		{$kdahil = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
 		{$kharic = kdvcikar($kdahil,$urun->kdv)}
 		{$kdv = $kdahil -$kharic}
 		{else}
 		{$kharic = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
 		{$kdahil = kdvhesapla($kharic,$urun->kdv)}
 		{$kdv = $kdahil -$kharic}
 		{/if}
 		{$urunlertoplam = $urunlertoplam + $kharic}
 		{$urunlerkdv = $urunlerkdv + $kdv}


 		{if $urun->kargo_bedeli}
 		{if $urun->kargo > 0}
 		{$kargokdvdahil =  $urun->kargo_bedeli*$urun->miktar}
 		{$kargokdvharic =  kdvcikar($kargokdvdahil ,$kargokdvoran)}
 		{$kargokdv =  $kargokdvdahil - $kargokdvharic }
 		{$urunlerkdv = $urunlerkdv + $kargokdv}
 		{$urunlerkargo = $urunlerkargo + $kargokdvharic}
 		{/if}
 		{/if}
 		<tr style='border-bottom: 1px solid #fff;'>
 			<td style='padding-left: 10px;padding-top: 20px;'>{$urun->urun_adi} ({$urun->stok_kodu}) ({{$urun->tedarikci}}) ({{$urun->urun_alis_fiyati}} ‎₺)</td>
 			<td style='padding-left: 10px;padding-top: 20px;border-left: 1px solid #fff;'>{$urun->miktar} Adet</td>
 			<td style='padding-top: 20px;border-left: 1px solid #fff;text-align: right;padding-right: 50px'>{fiyatver($urun->fiyat/(1 + (18/100)), $urun->fiyat_birim,$urun->indirim_orani)}</td>
 		</tr>
 		{if count($urun->hizmetler) > 0}
 		{foreach from=$urun->hizmetler item=hizmet}
 		{if $hizmet->firma_id} {$firma_id = $hizmet->firma_id}{/if} 
 		{$hizmetkdvdahil =  $hizmet->fiyat*$hizmet->adet }
 		{$hizmetkdvharic =  kdvcikar($hizmetkdvdahil ,$hizmetkdvoran)}
 		{$hizmetkdv =  $hizmetkdvdahil - $hizmetkdvharic}
 		{$urunlerkdv = $urunlerkdv + $hizmetkdv}
 		{$montajtoplam = $montajtoplam + $hizmetkdvharic}

 		<tr style='border-bottom: 1px solid #fff;'>
 			<td style='padding-left: 20px'>{$hizmet->montaj_hizmeti_adi}</td>
 			<td style='padding-left: 10px;border-left: 1px solid #fff;'>{$hizmet->adet} Adet</td>
 			<td style='border-left: 1px solid #fff;text-align: right;padding-right: 50px'>{fiyatver($hizmet->fiyat/(1 + (18/100)) ,$urun->fiyat_birim)}</td>
 			{*<td style='border-left: 1px solid #fff;text-align: right;padding-right: 50px'>{fiyatver($hizmet->fiyat  * $hizmet->adet ,$urun->fiyat_birim)}</td>*}
 		</tr>
 		{/foreach}
 		{/if}

 		{/foreach}   
 		{$toplam = $urunlertoplam + $urunlerkdv + $urunlerkargo + $montajtoplam}
 		<tr>
 			<td style='padding-top: 20px;' colspan="3"></td>
 		</tr> 


 	</table>



 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Ürünler Toplamı:</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{fiyatver($urunlertoplam, "TRY")}
 			</td>
 		</tr>
 	</table>

 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Hizmetler:</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{fiyatver($montajtoplam, "TRY")}
 			</td>
 		</tr>
 	</table>

 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Kargo Bedeli:</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{fiyatver($urunlerkargo, "TRY")}
 			</td>
 		</tr>
 	</table>

 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>KDV:</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{fiyatver($urunlerkdv, "TRY")}
 			</td>
 		</tr>
 	</table>
 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Ödeme Komisyonu:</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{fiyatver($odemedata->komisyon, "TRY")}
 			</td>
 		</tr>
 	</table>
	{$toplam_2 = $odemedata->sepet_tutar}
	{$p_set = false}
	{$indirim_sepet = $this->orders->siparis_hediyecekleri($order->id)}
	{if $indirim_sepet}
	{$p_set = true}
	{/if}
	{foreach  from=$indirim_sepet item=indirim_ceki}
 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Ödeme Çeki ({$indirim_ceki->cek_kodu}):</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
 				{if $indirim_ceki->dtip==1}
				{$indirim_ceki->dgr} %
				{else}
				{$toplam_2 = $toplam_2 + $indirim_ceki->dgr}
				-{fiyatver($indirim_ceki->dgr,"TRY")} TRY
				{/if}
 			</td>
 		</tr>
 	</table>
	{break}
	{/foreach}
	{if $p_set}
		<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Toplam Tutar (KDV DAHİL)</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
				{fiyatver($toplam_2,"TRY")}	
 			</td>
 		</tr>
 	</table>	
	{/if}
	{if $order->odeme_tipi == 1}
			{if $order->temp_transfer}
		<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>Havale İndirimi</b> 
 			</td>
 			<td width="26%" style='padding: 0pt 0pt 0pt 15px;'>
				{$order->temp_transfer}% (- {fiyatver(($order->temp_transfer)/100*$toplam,"TRY")})
 			</td>
 		</tr>
 	</table>
	{/if}
	{/if}

 	<table width="700" height="25" align="center" cellpadding="0" cellspacing="0" border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;border-left:1px solid #D7D7D7;border-right:1px solid #D7D7D7;">
 		<tr>
 			<td width="44%" style="padding-left:10px;">
 			</td>
 			<td width="30%" style='text-align:right;'>
 				<b>{if $p_set}Ödeme Tutarı{else}Toplam Tutar:{/if}</b>
 			</td>
 			{$toplamTaksit = $this->orders->posArtiTaksit($odemedata->sanalpos, $odemedata->installment, 1)}
 			<td width="15%" style='padding: 0pt 0pt 0pt 15px'>
 				{fiyatver($odemedata->sepet_tutar,"TRY")}		</td>
 				<td width="15%">{if $odemedata->installment} {$toplamTaksit} x {fiyatver($odemedata->sepet_tutar / $toplamTaksit,"TRY")}{/if}</td>
 			</tr>
 		</table>


 	</span>



 	<span class='PrinterFriendly'>
 		<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#F4F4F4;border-collapse:collapse;font-size:11px;font-family:arial;border:1px solid #D7D7D7;">
 			<tr>
 				<td style="padding-left:5px;"><b>Fatura Bilgileri</b></td>
 			</tr>
 		</table>
 		<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#fff;border-left:1px solid #D7D7D7;font-size:11px;font-family:arial;border-right:1px solid #D7D7D7; padding:5px;">

 			{if $data->ticari_unvani}
 			<tr>
 				<td width="120" valign="top"><strong>Ticari Ünvan</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->ticari_unvani}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adres</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->kur_adres} <br> {$this->extraservices->il($data->kur_il)} / {$this->extraservices->ilce($data->kur_ilce)} <br/>{$data->kur_posta_kodu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Telefon</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->kur_cep_telefonu}</td>
 			</tr> 	<tr>
 				<td valign="top"><strong>Vergi Dairesi</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{$data->vergi_dairesi}</td>
 			</tr>
 			<tr>
 				<td valign="top"><strong>Vergi No</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{$data->vergi_no}</td>
 			</tr>
 			<tr>
 				<td valign="top"><strong>E-mail</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{$data->kur_email}</td>
 			</tr>
 			<tr>
 				<td valign="top"><strong>E-Fatura Mükellefi</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{if $data->efatura == 'yes'}Evet{else}Hayır{/if}</td>
 			</tr>
 			{else}
 			<tr>
 				<td width="120" valign="top"><strong>Adı Soyadı</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->adi} {$data->soyadi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adres</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->adres} <br> {$this->extraservices->il($data->il)} / {$this->extraservices->ilce($data->ilce)} <br/>{$data->posta_kodu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Telefon</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->cep_telefonu}</td>
 			</tr> 
 			<tr>
 				<td valign="top"><strong>TC Kimlik No</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{$data->tc_kimlik_no}</td>
 			</tr>
 			<tr>
 				<td valign="top"><strong>E-mail</strong></td>
 				<td align="center" valign="top"><strong>:</strong></td>
 				<td>{$data->email}</td>
 			</tr>
 			{/if}




 		</table>
 		<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#F4F4F4;border-collapse:collapse;font-size:11px;font-family:arial;border:1px solid #D7D7D7;">
 			<tr>
 				<td style="padding-left:5px;"><b>Teslimat Bilgileri</b></td>
 			</tr>
 		</table>
 		<table width="700" height="30" align="center" cellpadding="0" cellspacing="0" border="0" style="background:#fff;border:1px solid #D7D7D7;font-size:11px;font-family:arial;padding:5px;">
  			{if $data->teslimyeri == "montaj_noktasina"}
 			{$firma = $this->extraservices->montaj_noktalari($firma_id)}
 			<tr>
 				<td width="120" valign="top"><strong>Montaj Noktası</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$firma->firma_adi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adresi</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$firma->firma_adresi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>İl / İlçe</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$firma->ilce} / {$firma->il}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Telefonu</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$firma->firma_tel1}</td>
 			</tr>
 			{elseif  $data->teslimyeri == "teslim_adresine"}
 			<tr>
 				<td width="120" valign="top"><strong>Adı Soyadı</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->teslim_edilecek_adi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Cep Telefonu</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->teslim_edilecek_telefonu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Posta Kodu</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->teslim_posta_kodu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>İl / İlçe</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$this->extraservices->il($data->teslim_il)} / {$this->extraservices->ilce($data->teslim_ilce)}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adres</strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->teslim_adres}</td>
 			</tr>
 			{else}
 			{if $data->ticari_unvani}
 			<tr>
 				<td width="120" valign="top"><strong>Ticari Ünvanı </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->ticari_unvani}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Telefon  </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->kur_cep_telefonu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Vergi Dairesi </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->vergi_dairesi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Vergi No  </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->vergi_no}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Posta Kodu </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->kur_posta_kodu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>İl / İlçe  </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$this->extraservices->il($data->kur_il)} / {$this->extraservices->ilce($data->kur_ilce)}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adres </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->kur_adres}</td>
 			</tr>

 			{else}
 			<tr>
 				<td width="120" valign="top"><strong>Adı Soyadı </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->adi} {$data->soyadi}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Cep Telefonu  </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->cep_telefonu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Ev Telefonu  </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->ev_telefonu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>TC Kimlik No </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->tc_kimlik_no}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Posta Kodu </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$data->posta_kodu}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>İl / İlçe </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td> {$this->extraservices->il($data->il)} / {$this->extraservices->ilce($data->ilce)}</td>
 			</tr>
 			<tr>
 				<td width="120" valign="top"><strong>Adres </strong></td>
 				<td align="center" valign="top" width="5"><strong>:</strong></td>
 				<td>{$data->adres}</td>
 			</tr>

 			{/if}
 			{/if}   





 		</table>
 	</span>
 	<script language="javascript">
 		window.print();
 	</script>

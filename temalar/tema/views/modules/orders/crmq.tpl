{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Crm Soruları</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{site_url('/')}">Anasayfa</a>
                </li>
                <li class="active">Crm Soruları</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut soruları görebilir, Yeni soru ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#soru_ekle">
                            <i class="fa fa-plus"></i>  
                            Soru Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active">Sorular</li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%">Sıra </th>  
                                                    <th width="80%">Soru </th> 
                                                    <th width="10%">Tip </th>
                                                    <th width="1%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {if count($sorular) > 0}

                                                {foreach $sorular as $soru}
                                                <tr>
                                                    <td>{$soru->id}</td> 
                                                    <td align="center">{$soru->sira}</td> 
                                                    <td>{$soru->soru}</td> 
                                                    <td>{if $soru->soru_tip == 'e-h'} 
                                                        Evet - Hayır
                                                        {elseif $soru->soru_tip == 'puan'} 
                                                        Puan
                                                        {elseif $soru->soru_tip == 'marka'} 
                                                        Marka 
                                                        {else} 
                                                        Yazı
                                                    {/if}</td>
                                                    <td>  
                                                        <a data-toggle="modal" href="#soruduzenle_{$soru->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>                                                        
                                                        <div class="modal fade" id="soruduzenle_{$soru->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title">Soru Düzenleme</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="tab-pane" id="tab_kategori_ekle">
                                                                            <form class="form-horizontal form-row-seperated" method="post" action="{site_url('orders/crm_soru_edit')}/{$soru->id}">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-4 control-label">Soru:
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-8">
                                                                                        <input type="text" class="form-control" name="soru" placeholder=""value="{$soru->soru}">                                            
                                                                                    </div>
                                                                                </div>

                                                                                <input type="hidden" name="soru_tip" value="{$soru->soru_tip}" />

                                                                                <div class="form-group">
                                                                                    <label class="col-md-4 control-label">Sıra No:
                                                                                        <span class="required"> * </span>
                                                                                    </label>
                                                                                    <div class="col-md-8" id="">
                                                                                        <input type="number" class="form-control" name="sira" min="1" value="{$soru->sira}">                                            
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <label class="col-md-4 control-label"></label>
                                                                                    <div class="col-md-8" id="kategorilerimiz">         
                                                                                        <button type="submit" class="btn blue">Düzenle</button> 
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div> 
                                                                    </div> 
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div> 
                                                        <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('orders/crm_soru_remove')}/{$soru->id}" title="Sil" class="btn btn-sm btn-default sil">
                                                            <i class="fa fa-times-circle"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                                {else}
                                                <tr>
                                                    <td colspan='10' style='text-align:center;'> Crm Sorusu Yok.</td>
                                                </tr>
                                                {/if}
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        <div class="modal fade in" id="soru_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Soru Ekle</h4>
                </div>
                <div class="modal-body">
                    <div class="tab-pane" id="tab_kategori_ekle">
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('orders/crm_soru_ekle')}">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Soru:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" >
                                    <input type="text" class="form-control" name="soru" placeholder=""  required="required">                                           
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Soru Tipi:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" >

                                    <select name="soru_tip" class="form-control" required="required">
                                        <option value="">Seçiniz</option>
                                        <option value="marka">Marka</option>
                                        <option value="e-h">Evet - Hayır</option>
                                        <option value="puan">Puan</option>
                                        <option value="text">Yazı</option>                                                                
                                    </select>                                           
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" id="">
                                    <input type="number" class="form-control" name="sira" min="1"   required="required">                                            
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Ekle</button> 
                                </div>
                            </div>
                        </form>
                    </div> 
                </div> 
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
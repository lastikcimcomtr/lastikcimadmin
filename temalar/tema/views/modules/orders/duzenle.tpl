{include file="base/header.tpl"}
{$method = $this->router->fetch_method()}
<style type="text/css">
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
				<form class="" enctype="multipart/form-data" method="post" action="">
					<div class="tabbable-bordered">
							<ul class="nav nav-tabs" id="tab_basliklari">
								<li class="active">
									<a href="#siparis_detay" data-toggle="tab"> Sipariş Detayları </a>
								</li>
								<li >
									<a href="#odemedata" data-toggle="tab"> Ödeme Detayları</a>
								</li>
								<li >
									<a href="#montaj_detay" data-toggle="tab"> Montaj Detayları</a>
								</li>
								<li >
									<a href="#iletisim" data-toggle="tab"> İletişim</a>
								</li>
								<li >
									<a href="#cari" data-toggle="tab"> Cari Bilgiler</a>
								</li>
							</ul>
							<div class="tab-content">
									<input style="display:none" name="siparis_id" value="{$order->id}">
									<div class="tab-pane  col-md-6 col-sm-12 active" id="siparis_detay">
										{foreach from=$order_data key=key item=od}
											{if in_array($key,['tanimli_adres','adres_isim','kur_tanimli_adres','kur_adres_isim','zaman','session'])}
											<div class="form-group" style="display:none">
												<label class="col-md-2 control-label">{$key}</label>
												<input class="form-control required_chk" type="text" name="siparisdata[{$key}]" value="{$od}"/>
											</div>
											{else}
												<div class="form-group">
												{if $key=='il'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->iller() item=il}
															<option value="{$il->id}" {if $il->id==$od}selected{/if} >{$il->il}</option>
														{/foreach}
													</select>
												{elseif $key=='ilce'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->ilceler($order_data->il) item=ilce}
															<option value="{$ilce->id}" {if $ilce->id==$od}selected{/if} >{$ilce->ilce}</option>
														{/foreach}
													</select>

												{elseif $key=='kur_il'}
												<label class="col-md-2 control-label">{$key}</label>
												<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->iller() item=il}
															<option value="{$il->id}" {if $il->id==$od}selected{/if} >{$il->il}</option>
														{/foreach}
												</select>
												{elseif $key=='kur_ilce'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->ilceler($order_data->kur_il) item=ilce}
															<option value="{$ilce->id}" {if $ilce->id==$od}selected{/if} >{$ilce->ilce}</option>
														{/foreach}
													</select>

												{elseif $key=='teslim_il'}
												<label class="col-md-2 control-label">{$key}</label>
												<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->iller() item=il}
															<option value="{$il->id}" {if $il->id==$od}selected{/if} >{$il->il}</option>
														{/foreach}
												</select>
												{elseif $key=='teslim_ilce'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->ilceler($order_data->teslim_il) item=ilce}
															<option value="{$ilce->id}" {if $ilce->id==$od}selected{/if} >{$ilce->ilce}</option>
														{/foreach}
													</select>
												{else}
														<label class="col-md-2 control-label">{$key}</label>
														<input class="form-control required_chk" type="text" name="siparisdata[{$key}]" value="{$od}"/>
												{/if}
												</div>
											{/if}
										{/foreach}
									</div>
									<div class="tab-pane  col-md-6 col-sm-12" id="odemedata">
										{foreach from=$odeme_data key=key item=odm}
												<div class="form-group">
													<label class="col-md-2 control-label">{$key}</label>
														<input class="form-control required_chk" type="text" name="odemedata[{$key}]" value="{$odm}"/>
												</div>
										{/foreach}
									</div>
									<div class="tab-pane" id="montaj_detay">
										<div class="col-md-6 col-sm-12" style="padding:0">
										{foreach from=$urun_data key=key item=urn}
											<div class="col-md-12">
												<h4>{$urn->urun_adi} </h4>
												<h4>STOK KODU {$urn->stok_kodu}  / ADET {$urn->miktar} / KATEGORİ {$urn->kategoriadi} </h4>
												{foreach from=$urn->hizmetler item=urn_hizmet}
												<div class="form-group" style="padding:0;">
													<label class="col-md-12 control-label" style="padding:0;font-weight:700;">{$urn_hizmet->firma_adi} - {$urn_hizmet->montaj_hizmeti_adi}</label>
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Firma ID</label>
													<input name="mnt[{$urn_hizmet->id}][firma_id]" class="form-control" value="{$urn_hizmet->firma_id}" disabled />
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Adet</label>
													<input name="mnt[{$urn_hizmet->id}][adet]" class="form-control " value="{$urn_hizmet->adet}" disabled />
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Fiyat</label>
													<input name="mnt[{$urn_hizmet->id}][fiyats]" class="form-control" value="{$urn_hizmet->fiyat}" disabled />
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">&nbsp;</label>
													<button class="btn" type="button" onclick="urn_hizmet_kaldir('{$urn_hizmet->id}', '{$order->id}' , '{$urn->id}')">Kaldır</button>
												</div>
												{/foreach}
											</div>
											<div class="col-md-12 yeni_mnt" id="urn_ym_{$urn->id}">
												<h5>YENİ HİZMET EKLE</h5>
												<input name="yeni[urun_id]" value="{$urn->id}" style="display:none"/>
												<input name="yeni[order_id]" value="{$order->id}" style="display:none"/>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-md-12 control-label" style="padding:0;font-weight:600;">Firma ID</label>
													<input name="yeni[firma_id]" id="firma_id" class="form-control" data-ym="urn_ym_{$urn->id}" placeholder="Firma ID" value="" />
												</div>
												<div class="form-group col-md-4 col-sm-12 yeni_hizmet" style="padding:0;display:none;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Hizmet</label>
													<select class="form-control" name="yeni[id_str]">
														<option>Standart Lastik Montajı</option>
													</select>
												</div>
												<div class="form-group col-md-3 col-sm-12 yeni_adet" style="padding:0;display:none;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Adet</label>
													<input name="yeni[adet]" value="1" class="form-control " value="{$urn_hizmet->adet}" />
												</div>
												<div class="form-group col-md-3 col-sm-12 yeni_ekle" style="padding:0;display:none;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">&nbsp;</label>
													<button class="btn" type="button" onclick="urn_hizmet_ekle('urn_ym_{$urn->id}')">Ekle</button>
												</div>
											</div>
										{/foreach}
										</div>
										<div class="col-md-6 col-sm-12">
											Montaj Noktaları
											<form class="col-md-12">
												<label>İl Seçiniz</label>
												<div class="select1 s2">
													<select name="il">
														{foreach from=$this->extraservices->iller() item=il}
														<option value="{$il->id}" {if $il->id == 34}selected{/if}>{$il->il}</option>
														{/foreach}
													</select>
												</div>
												<label>İlçe Seçiniz</label>
												<div class="select1 s2">
													<select name="ilce">
														<option value='0'>Tümü</option>
													</select>
												</div>
											</form>  
											<div class="col-md-12" id='firmalist' style="margin-top: 50px">
											</div>											
										</div>
									</div>
									<div class="tab-pane  col-md-6 col-sm-12" id="iletisim">
											<a href="https://lastikcim.com.tr/pages/mesafeli_satis_sozlesmesi/{$order->hash_value}" target="_blank">Mevcut Mesafeli Satış Sözleşmesini Görüntüle</a>
											<br />
											<br />
											<a href="https://lastikcim.com.tr/pages/on_bilgilendirme_formu/{$order->hash_value}" target="_blank">Mevcut Ön Bilgilendirme  Formunu Görüntüle</a>
											<br />
											<br />
											<button class="btn" type="button" onclick="sozlesme_ilet('{$order->id}')">Sözleşmeleri Müşteriye Tekrar İlet</button>
									</div>
									<div class="tab-pane  col-md-6 col-sm-12" id="cari">
										<div class="col-md-12">
											<a href="/orders/carisifirla/{$order->id}" target="_blank" style="background-color: #71b100; border-color: #71b100; color: white;" title="Sipariş Düzenle" class="btn btn-sm btn-default">
						                        <i class="fa fa-refresh"></i> Cari Veriyi Sıfırla
						                    </a>                 
										</div>
										<div class="col-md-12">
										<h6>Cari Veri:</h6>
										{if $order->caridata}
										{$caridata = json_decode($order->caridata)}
										<p>id - {$caridata->id}</p>
										<p>kod - {$caridata->kod}</p>
										<p>durum - {$caridata->status}</p>
										{else}
										-
										{/if}
										</div>
										<div class="col-md-12">
										<h6>Cari Adres Fatura:</h6>
										{if $order->cariadres_fatura}
										{$order->cariadres_fatura}
										{else}
										-
										{/if}
										</div>
										<div class="col-md-12">
										<h6>Cari Adres Sevk:</h6>
										{if $order->cariadresdata_sevk}
										{$order->cariadresdata_sevk}
										{else}
										-
										{/if}
										</div>
										<div class="col-md-12" style="margin-bottom: 20px">
											<h6>Cari Sipariş:</h6>
											{if $order->siparis_data}
											{$order->siparis_data}
											{else}
											-
											{/if} 
										</div>
									</div>


									<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group pull-right">
													<button type="submit" class="btn blue">Kaydet</button>
										</div> 
									</div>
									</div>
									<div class="clearfix"></div>
							</div>
					</div>
					</form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="montaj_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kargo_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="fatura_bilgileri" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carihesap" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carisiparis" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	function sozlesme_ilet(siparis_id){
		var send_data = {
            siparis_id:siparis_id
        };
		$.ajax({
            type:'POST',
            url:'/ajax/sozlesmeleri_ilet',
            data:send_data,
            dataType: 'json',
            success:function(cevap){
				window.location.href += "#montaj_detay";
				window.location.reload(true);
            }
        });
	}
    $(document).on('change', '#firma_id', function (){
		var firma_id = $(this).val();
		var ym = $(this).data('ym');
        $.get( "/ajax/firma_hizmetler/"+firma_id).done(function( data ) {
            var data = JSON.parse(data);
			console.log(data);
			var options = '';
			$.each( data, function( key, hizmet ) {
					options += '<option value="'+hizmet.id+'">'+hizmet.baslik+'</option>';
			});
			console.log(options);
			if(options != ''){
				$('#'+ym).find(".yeni_hizmet").css('display','block').find('select').html(options);
				$('#'+ym).find('.yeni_adet').css('display','block');
				$('#'+ym).find('.yeni_ekle').css('display','block');
			}else{
				$('#'+ym).find(".yeni_hizmet").css('display','none').find('select').html(options);
				$('#'+ym).find('.yeni_adet').css('display','none');
				$('#'+ym).find('.yeni_ekle').css('display','none');
			}
		});
	});
	function urn_hizmet_kaldir(hizmet_id,siparis_id,urun_id){
		var send_data = {
            hizmet_id:hizmet_id,
            siparis_id:siparis_id,
			urun_id:urun_id
        };
		$.ajax({
            type:'POST',
            url:'/ajax/urn_hizmet_sil',
            data:send_data,
            dataType: 'json',
            success:function(cevap){
				window.location.href += "#montaj_detay";
				window.location.reload(true);
            }
        });
	}
	function urn_hizmet_ekle(base){
		pdata = $('#'+base).find('select, textarea, input').serialize();
        $.ajax({
            type:'POST',
            url:'/ajax/urn_hizmet_ekle',
            data:pdata,
            dataType: 'json',
            success:function(cevap){
				window.location.href += "#montaj_detay";
				window.location.reload(true);
            }
        });
	}
    function siparis_durum_degis(sipid) {
		var select = $("#siparis_durumu_"+sipid);
        var durum = $("#siparis_durumu_"+sipid+" option:selected").val();
        var send_data = {
            order_id:sipid,
            durum:durum
        };
        $.get( "{site_url('ajax/order_durum')}", send_data ).done(function( data ) {
			data = JSON.parse(data);
			if(data.status == 'error'){
                 select.val(data.durumu);
				 swal("Başarısız",data.msg,"warning");
			}else{
				 swal({
				  title: "Başarılı",
				  text: "Sipariş Durumu Güncellendi.",
				  timer: 1200
				});
				if(durum == 9 || durum==4){
					select.closest('tr').css('background-color','#ff6f6f');
				}
				if(durum == 2){
					select.closest('tr').css('background-color','palegreen');
				}
			}
            // console.log(data);
        });
    }
</script>
{include file="base/footer_txt.tpl"}
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
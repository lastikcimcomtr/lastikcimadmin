{include file="base/header.tpl"}
<style type="text/css">
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <form class="row" method="post" action="" autocomplete="off">
                            <div class="form-group col-md-2">
                                <label for="siparisid">Sipariş ID</label>
                                <input type="text" name="siparis_id" class="form-control" id="siparisid" placeholder="Sipariş ID" value="{$sdata['siparis_id']}" />
                            </div>
                            <div class="form-group col-md-2">
                                <label for="islemid">İşlem ID</label>
                                <input type="text" name="islem_id" class="form-control" id="islemid" placeholder="İşlem ID" value="{$sdata['islem_id']}" />
                            </div>
                            <div class="form-group col-md-2">
                                <label for="durumguvenlik">3D</label>
                                <select name="durum_guvenlik" class="form-control" id="durumguvenlik">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" {if $sdata['durum_guvenlik'] == "1"}selected{/if}>Başarılı</option>
                                    <option value="0" {if $sdata['durum_guvenlik'] == "0"}selected{/if}>Başarısız</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="durumodeme">Ödeme</label>
                                <select name="durum_odeme" class="form-control" id="durumodeme">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" {if $sdata['durum_odeme'] == "1"}selected{/if}>Başarılı</option>
                                    <option value="0" {if $sdata['durum_odeme'] == "0"}selected{/if}>Başarısız</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="postip">Pos</label>
                                <select name="pos_tip" class="form-control" id="postip">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="NestPay" {if $sdata['pos_tip'] == "NestPay"}selected{/if}>NestPay</option>
                                    <option value="Gvp" {if $sdata['pos_tip'] == "Gvp"}selected{/if}>Gvp</option>
                                    <option value="Posnet" {if $sdata['pos_tip'] == "Posnet"}selected{/if}>Posnet</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Arama Yap</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">                   
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Sipariş ID </th>
                                <th> İşlem ID </th>
                                <th> Miktar </th>
                                <th> 3D </th>
                                <th> Ödeme </th>
                                <th> Pos </th>
                                <th width="1%">Detay</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <tbody>
                                {foreach $posdetaylar as $posdetay}
                                <tr>
                                    <td>{$posdetay->id}</td>
                                    <td>{$posdetay->siparis_id}</td>
                                    <td>{$posdetay->islem_id}</td>
                                    <td>{posmiktar($posdetay)}</td>
                                    <td>{if $posdetay->durum_guvenlik == 1}<i class="fa fa-check" style="color: green;"></i>{else}<i class="fa fa-times" style="color: red;"></i>{/if}</td>
                                    <td>{if $posdetay->durum_odeme == 1}<i class="fa fa-check" style="color: green;"></i>{else}<i class="fa fa-times" style="color: red;"></i>{/if}</td>
                                    <td>{$posdetay->pos_tip}</td>
                                    <td>
                                        <a href="{site_url('orders/posdetay/'|cat:$posdetay->id)}" data-target="#detay" data-toggle="modal" class="btn btn-sm btn-warning">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                </tr>
                                {/foreach}
                            </tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

{include file="base/footer_txt.tpl"}
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
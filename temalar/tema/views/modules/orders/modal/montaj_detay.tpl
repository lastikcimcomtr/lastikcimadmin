<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Montaj Detayları</h4>
</div>
<div class="modal-body">
    <form class="montaj_detay" action="javascript:void(-1)" autocomplete="off">
        {$montaj_arac_bilgisi = json_decode($order->montaj_arac_bilgisi)}
        <div class="form-body">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="marka">Marka</label>
                    <input id="marka" name="marka" type="text" class="form-control" placeholder="Marka" value="{$montaj_arac_bilgisi->marka}" />
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="marka">Model</label>
                    <input id="marka" name="model" type="text" class="form-control" placeholder="Model" value="{$montaj_arac_bilgisi->model}" />
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="marka">Yıl</label>
                    <input id="marka" name="model_yil" type="text" class="form-control" placeholder="Model Yılı" value="{$montaj_arac_bilgisi->model_yil}" />
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="marka">Motor</label>
                    <input id="marka" name="motor" type="text" class="form-control" placeholder="Motor" value="{$montaj_arac_bilgisi->motor}" />
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="marka">KM</label>
                    <input id="marka" name="kilometre" type="text" class="form-control" placeholder="Kilometre" value="{$montaj_arac_bilgisi->kilometre}" />
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="marka">Plaka</label>
                    <input id="marka" name="plaka" type="text" class="form-control" placeholder="Plaka Numarası 34VS1234" value="{$montaj_arac_bilgisi->plaka}" />
                </div>
            </div>
            {if !$order->montaj_arac_bilgisi}
                <div class="col-md-3">
                    <div class="form-group" style="margin-top: 23px">
                        <button class="button btn btn-primary" id="kyd_button">Kaydet</button>
                    </div>
                </div>
            {else}
                <div class="col-md-3">
                    <div class="form-group" style="margin-top: 23px">
                        <button class="button btn btn-success" id="kyd_button">Güncelle</button>
                    </div>
                </div>
            {/if}
            {if !$montaj_arac_bilgisi->email}
            <div class="col-md-3">
                <div class="form-group" style="margin-top: 23px">
                    <button type="button" class="button btn btn-warning" id="mail_send"><i class="fa fa-envelope fa-fw"></i>Mail Gönder</button>
                </div>
            </div>
            {else}
            <div class="col-md-3">
                <div class="form-group" style="margin-top: 23px">
                    <button type="button" class="button btn btn-danger" id="mail_send"><i class="fa fa-refresh fa-fw"></i>Tekrar Gönder</button>
                </div>
            </div>
            {/if}
        </div>
    </form>
    {$urunler = $order->urunler|json_decode}
    {$hizmetler = array()}
    {foreach $urunler as $urun}
        {$hizmetler[] = $urun->hizmetler}
        {$firma_adi = $urun->hizmetler[0]->firma_adi}
        {$firma_id  = $urun->hizmetler[0]->firma_id}
    {/foreach}

    {$firma_detay = $this->extraservices->montaj_noktalari($firma_id)}
    <table class="table table-striped table-hover table-bordered">    
    
        <thead>
            <tr> 
                <th colspan="4">{$firma_adi}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">{$firma_detay->firm_real_adresi}</td>
                <td>{$firma_detay->il}/{$firma_detay->ilce}</td>
                <td>{$firma_detay->firm_real_tel}</td>
            </tr>
        </tbody>
        <thead>
            <tr>
                <th>Hizmet</th>
                <th>Miktar</th>
                <th>Fiyat</th>
                <th>Toplam</th>
            </tr>
        </thead>
        <tbody>
            
            {foreach $hizmetler as $hizmet}
                {foreach $hizmet as $hzmt}
                <tr>
                    <td>{$hzmt->montaj_hizmeti_adi}</td>
                    <td>{$hzmt->adet}</td>
                    <td>{fiyatver($hzmt->fiyat, "TRY")}</td>
                    <td>{fiyatver($hzmt->adet * $hzmt->fiyat, "TRY")}</td>
                </tr>
                {/foreach}
            {/foreach}

        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button onclick="sendOrderMontaj({$order->id})" type="button" class="btn btn-info pull-left" data-dismiss="modal">Müşteriye Firma Bilgilerini İlet</button>
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
</div>


{literal}
<script type="text/javascript">
    var email_send = false;
    $('.montaj_detay').submit(function(){
        var data = {};
        data.marka = $(this).find('[name=marka]').val();
        data.model = $(this).find('[name=model]').val();
        data.model_yil = $(this).find('[name=model_yil]').val();
        data.motor = $(this).find('[name=motor]').val();
        data.kilometre = $(this).find('[name=kilometre]').val();
        data.plaka = $(this).find('[name=plaka]').val();
        data.email = email_send;
{/literal}        
        data.siparis_id = {$order->id};
{literal}        
        montaj_data_kaydet(data);
        return false;
    })

    function montaj_data_kaydet(data){
        var url = '/orders/montajDataKaydet/';
        $.ajax({
            type:'POST',
            dataType:'json',
            url:url,                        
            data: {data : data},
            success: function (textStatus, status) {
                if(textStatus.status == "false"){
                    alert(textStatus.type);
                }else{
                    $('#kyd_button').addClass('btn-success').removeClass('btn-primary').html('Güncelle');
                }
            },
            error: function(xhr, textStatus, error) {
                tt = false;
                alert(xhr.responseText + ' ' + error);
            }
        });
    }

    $('#mail_send').click(function(){
        email_send = true;
        $('.montaj_detay').submit();
{/literal}
        var url = '/orders/sendMontajMail/{$order->id}';
{literal}
        $.ajax({
            type:'POST',
            dataType:'json',
            url:url,                        
            data: {},
            success: function (textStatus, status) {
                $('#mail_send').addClass('disabled').addClass('btn-danger').removeClass('btn-warning').html('Mail Gönderildi');
            },
            error: function(xhr, textStatus, error) {
                tt = false;
                alert(xhr.responseText + ' ' + error);
            }
        });
    });
    function sendOrderMontaj(order_id){
        var url = '/orders/sendMusteriPointSms/'+order_id;
        $.ajax({
            type:'GET',
            dataType:'json',
            url:url,                        
            data: {},
            success: function (textStatus, status) {
            },
            error: function(xhr, textStatus, error) {
                //tt = false;
                //alert(xhr.responseText + ' ' + error);
            }
        });
    }
</script>
{/literal}
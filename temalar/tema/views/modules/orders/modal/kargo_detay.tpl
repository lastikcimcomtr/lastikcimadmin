{$kargodata = $order->kargo_takip|json_decode}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Kargo Detayları</h4>
</div>
<div class="modal-body">
	<div id="kargocevap" style="display: none;"></div>
	<form action="javascript:void(-1);" onsubmit="kargodetaygir();" id="kargoform" autocomplete="off">
		<div class="form-body">
        	<div class="form-group">
	            <label>Takip Numarası</label>
	            <input type="text" class="form-control" placeholder="Takip Numarası" value="{$kargodata->takipnumarasi}" name="takipnumarasi" />
	        </div>
	        <div class="form-group">
	        	<label>Kargo Firması</label>
	        	<select name="firma" class="form-control">
	        		<option value="">Seçiniz</option>
					{foreach $kargofirmalar as $kargo}
					<option value="{$kargo->id}" {if $kargodata->firma == $kargo->id}selected{/if}>{$kargo->firma_adi}</option>
					{/foreach}
				</select>
	        </div>
	    </div>
	    <div class="form-actions">
            <button type="submit" class="btn blue">Kaydet</button>
        </div>
	</form>

</div>

<script type="text/javascript">
    function kargodetaygir() {
        var fdata = $('form#kargoform').serialize();

        $.ajax({
	  		type:'POST',
	  		url:'{site_url('orders/kargo_detaylari/'|cat:$order->id)}',
	  		dataType: 'json',
	  		data:fdata,
	  		success:function(cevap){
	  			if (cevap) {
	  				$('div#kargocevap').html('<div class="alert alert-'+cevap.status+'"> '+cevap.message+' </div>');
	  				$('div#kargocevap').show();
	  			}
	  		}
	  	});

    }
</script>
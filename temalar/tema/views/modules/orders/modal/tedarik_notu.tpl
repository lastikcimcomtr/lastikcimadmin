{$kargodata = $order->kargo_takip|json_decode}
{$tedarik_notu = $order->tedarik_not|json_decode}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Tedarik Notu</h4>
</div>
<div class="modal-body">
	<div id="siparisnotucevap" style="display: none;"></div>
	<form action="javascript:void(-1);" onsubmit="tedariknotugir();" id="siparisnotuform" autocomplete="off">
		<div class="form-body">
        	<div class="form-group">
	            <label>Tedarikçi</label>
	            <input name="tedarikci" class="form-control" value="{$tedarik_notu->tedarikci}"></textarea>
	        </div>
        	<div class="form-group">
	            <label>Tedarik Notu</label>
	            <textarea name="tedarik_not" class="form-control" cols="30" rows="10">{$tedarik_notu->tedarik_not}</textarea>
	        </div>
	    </div>
	    <div class="form-actions">
            <button type="submit" class="btn blue">Kaydet</button>
        </div>
	</form>

</div>

<script type="text/javascript">
    function tedariknotugir() {
        var fdata = $('form#siparisnotuform').serialize();
        $.ajax({
	  		type:'POST',
	  		url:'{site_url('orders/tedarik_notu/'|cat:$order->id)}',
	  		dataType: 'json',
	  		data:fdata,
	  		success:function(cevap){
	  			if (cevap) {
	  				$('div#siparisnotucevap').html('<div class="alert alert-'+cevap.status+'"> '+cevap.message+' </div>');
	  				$('div#siparisnotucevap').show();
	  			}
	  		}
	  	});

    }
</script>
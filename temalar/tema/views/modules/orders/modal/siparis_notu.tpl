{$kargodata = $order->kargo_takip|json_decode}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Sipariş Notu</h4>
</div>
<div class="modal-body">
	<div id="siparisnotucevap" style="display: none;"></div>
	<form action="javascript:void(-1);" onsubmit="siparisnotugir();" id="siparisnotuform" autocomplete="off">
		<div class="form-body">
        	<div class="form-group">
	            <label>Sipariş Notu</label>
	            <textarea name="siparisnotu" class="form-control" cols="30" rows="10">{$order->siparis_notu}</textarea>
	        </div>
	    </div>
	    <div class="form-actions">
            <button type="submit" class="btn blue">Kaydet</button>
        </div>
	</form>

</div>

<script type="text/javascript">
    function siparisnotugir() {
        var fdata = $('form#siparisnotuform').serialize();

        $.ajax({
	  		type:'POST',
	  		url:'{site_url('orders/siparis_notu/'|cat:$order->id)}',
	  		dataType: 'json',
	  		data:fdata,
	  		success:function(cevap){
	  			if (cevap) {
	  				$('div#siparisnotucevap').html('<div class="alert alert-'+cevap.status+'"> '+cevap.message+' </div>');
	  				$('div#siparisnotucevap').show();
	  			}
	  		}
	  	});

    }
</script>
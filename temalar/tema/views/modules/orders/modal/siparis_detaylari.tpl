{$data =  $order->data|json_decode}
{$urunler = $order->urunler|json_decode}
{$odemedata = $order->odemedata|json_decode}
{$odemebanka =  $this->extraservices->paytype($odemedata)}

{if  $odemedata->artitaksit}
{$installment  = ($odemedata->installment * 1 ) + ($odemedata->artitaksit * 1) }
{else}
{$installment  = $odemedata->installment} 
{/if}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Sipariş Detayları</h4>
</div>
<div class="modal-body">
<table id="duzenleme_formu" style="width:100%;">
    <tr>
        <td style="width:15%;font-weight: 600;font-size: 12px;">  Üye Adı Soyadı </td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;"> {if $data->ticari_unvani} {$data->ticari_unvani} {else}{$data->adi} {$data->soyadi}{/if}  </td> 
    </tr>
    <tr>
        <td style="width:15%;font-weight: 600;font-size: 12px;">  İp Adresi </td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;"> {$order->ip}  </td>
    </tr>
    <tr>
        <td style="width:15%;font-weight: 600;font-size: 12px;">  E Posta Adresi</td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;"> {$data->email}  {$data->kur_email} </td>
    </tr>
    <tr>
        <td style="font-weight: 600;font-size: 12px;"> Ref No / Sipariş No </td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;"> {$order->id} </td>
    </tr>
    <tr>
        <td style="font-weight: 600;font-size: 12px;">Sipariş Durumu</td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;">{$order->durum}</td>
    </tr>
    <tr>
        <td style="font-weight: 600;font-size: 12px;">Ödeme İşlemi</td>
        <td style="width: 20px;text-align: center;"> : </td>
        <td style="font-size:12px;">{$order->odeme_tipi_adi} {if $odemebanka}{$odemebanka}{/if}</td>
    </tr>
      {*}  <tr>
            <td style="font-weight: 600;font-size: 12px;">Kargo Seçenekleri</td>
            <td style="width: 20px;text-align: center;"> : </td>
            <td style="font-size:12px;">Ups Kargo </td>
        </tr>{*}
        <tr>
            <td style="font-weight: 600;font-size: 12px;">Sipariş Tarihi</td>
            <td style="width: 20px;text-align: center;"> : </td>
            <td style="font-size:12px;">{date('d.m.Y',$order->tarih)}</td>
        </tr>
        <tr>
            <td colspan="3" style="font-weight: 600;font-size: 12px;">Sözleşme ve Önbilgilendirme Formu</td>
        </tr>
    </table>

    {$kargokdvoran = 18}
    {$hizmetkdvoran = 18}
    {$urunlertoplam = 0}
    {$montajtoplam = 0}
    {$urunlerkdv = 0}
    {$urunlerkargo = 0}
    {$toplam = 0}

    <table border="" style="width:100%;border:1px solid #dedede;margin-top:10px;">
        <thead style="background-color:#F4F4F4;border-bottom:1px solid #dedede;">
            <th style="padding:10px;font-size:13px; "> Sepetim </th>
            <th style="padding:10px;font-size:13px; border-left: 1px solid #f4f4f4;"> Miktar </th>
            <th style="font-size:13px; border-left: 1px solid #f4f4f4;text-align: right;padding-right: 50px"> Fiyat </th>
        </thead>                                                
        <tbody>
            {foreach from=$urunler item=urun}

            {if $urun->kdv_dahil == 1}
            {$kdahil = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
            {$kharic = kdvcikar($kdahil,$urun->kdv)}
            {$kdv = $kdahil -$kharic}
            {else}
            {$kharic = fiyatvernumber($urun->fiyat*$urun->miktar,  $urun->indirim_orani)}
            {$kdahil = kdvhesapla($kharic,$urun->kdv)}
            {$kdv = $kdahil -$kharic}
            {/if}
            {$urunlertoplam = $urunlertoplam + $kharic}
            {$urunlerkdv = $urunlerkdv + $kdv}


            {if $urun->kargo_bedeli}
            {if $urun->kargo > 0}
            {$kargokdvdahil =  $urun->kargo_bedeli*$urun->miktar}
            {$kargokdvharic =  kdvcikar($kargokdvdahil ,$kargokdvoran)}
            {$kargokdv =  $kargokdvdahil - $kargokdvharic }
            {$urunlerkdv = $urunlerkdv + $kargokdv}
            {$urunlerkargo = $urunlerkargo + $kargokdvharic}
            {/if}
            {/if}
            <tr style='border-bottom: 1px solid #fff;'>
                <td style='padding-left: 10px;padding-top: 20px;'>{$urun->urun_adi}  ({$urun->stok_kodu})</td>
                <td style='padding-left: 10px;padding-top: 20px;border-left: 1px solid #fff;'>{$urun->miktar} Adet</td>
                <td style='padding-top: 20px;border-left: 1px solid #fff;text-align: right;padding-right: 50px'>{fiyatver($urun->fiyat *$urun->miktar,$urun->fiyat_birim, $urun->indirim_orani)}</td>
            </tr>
            {if count($urun->hizmetler) > 0}
            {foreach from=$urun->hizmetler item=hizmet}
            {if $hizmet->firma_id} {$firma_id = $hizmet->firma_id}{/if} 
            {$hizmetkdvdahil =  $hizmet->fiyat*$hizmet->adet }
            {$hizmetkdvharic =  kdvcikar($hizmetkdvdahil ,$hizmetkdvoran)}
            {$hizmetkdv =  $hizmetkdvdahil - $hizmetkdvharic}
            {$urunlerkdv = $urunlerkdv + $hizmetkdv}
            {$montajtoplam = $montajtoplam + $hizmetkdvharic}

            <tr style='border-bottom: 1px solid #fff;'>
                <td style='padding-left: 20px'>{$hizmet->montaj_hizmeti_adi}</td>
                <td style='padding-left: 10px;border-left: 1px solid #fff;'>{$hizmet->adet} Adet</td>
                <td style='border-left: 1px solid #fff;text-align: right;padding-right: 50px'>{fiyatver($hizmet->fiyat  * $hizmet->adet ,$urun->fiyat_birim)}</td>
            </tr>
            {/foreach}
            {/if}

            {/foreach}   
            {$toplam = $urunlertoplam + $urunlerkdv + $urunlerkargo + $montajtoplam}
            <tr>
                <td style='padding-top: 20px;' colspan="3"></td>
            </tr>              
        </tbody>  
    </table>



    <table border="" style="width:100%;border:1px solid #dedede;margin-top: -1px;">
        <tbody>
            <tr>
                <td style="text-align:right;line-height:25px">
                    <table border="0" style="text-align:left;background:#fff;border-collapse:collapse;font-size:11px;font-family:arial;">
                        <tbody>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>Ürünler Toplamı:</b></td>
                                <td width="5%" colspan="2" style="padding: 0pt 0pt 0pt 16px"> {fiyatver($urunlertoplam, "TRY")}</td>
                            </tr>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>Hizmetler:</b></td>
                                <td width="5%" colspan="2" style="padding: 0pt 0pt 0pt 16px">{fiyatver($montajtoplam, "TRY")} </td>
                            </tr>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>Kargo Bedeli:</b></td>
                                <td width="5%" colspan="2" style="padding: 0pt 0pt 0pt 16px"> {fiyatver($urunlerkargo, "TRY")} </td>
                            </tr>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>KDV:</b></td>
                                <td width="5%" colspan="2" style="padding: 0pt 0pt 0pt 16px"> {fiyatver($urunlerkdv, "TRY")}</td>
                            </tr>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>Ödeme Komisyonu:</b></td>
                                <td width="5%" colspan="2" style="padding: 0pt 0pt 0pt 16px">{fiyatver($odemedata->komisyon, "TRY")} </td>
                            </tr>
                            <tr> 
                                <td width="20%" style="text-align:right;"><b>Toplam Tutar:</b></td>
                                <td width="5%" style="padding: 0pt 0pt 0pt 16px"> {fiyatver($odemedata->sepet_tutar,"TRY")}</td>
                                <td width="5%">{if $odemedata->installment} {$installment} x {fiyatver($odemedata->sepet_tutar / $installment,"TRY")}{/if}</td>
                            </tr>
                        </tbody>
                    </table>  
                </td>
            </tr>                                               
        </tbody>
    </table>
</div>
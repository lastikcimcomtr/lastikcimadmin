{include file="base/header.tpl"}
{$method = $this->router->fetch_method()}
<style type="text/css">
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
				<form class="" enctype="multipart/form-data" method="post" action="">
					<div class="tabbable-bordered">
							<ul class="nav nav-tabs" id="tab_basliklari">
								<li class="active">
									<a href="#siparis_detay" data-toggle="tab"> Sipariş Detayları </a>
								</li>
								<li >
									<a href="#odemedata" data-toggle="tab"> Ödeme Detayları</a>
								</li>
								<li >
									<a href="#montaj_detay" data-toggle="tab"> Montaj Detayları</a>
								</li>
							</ul>
							<div class="tab-content">
									<input style="display:none" name="siparis_id" value="{$order->id}">
									<div class="tab-pane  col-md-6 col-sm-12 active" id="siparis_detay">
										{foreach from=$order_data key=key item=od}
											{if in_array($key,['tanimli_adres','adres_isim','kur_tanimli_adres','kur_adres_isim','sepet_tutar','zaman','session'])}
											<div class="form-group" style="display:none">
												<label class="col-md-2 control-label">{$key}</label>
												<input class="form-control required_chk" type="text" name="siparisdata[{$key}]" value="{$od}"/>
											</div>
											{else}
												<div class="form-group">
												{if $key=='il'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->iller() item=il}
															<option value="{$il->id}" {if $il->id==$od}selected{/if} >{$il->il}</option>
														{/foreach}
													</select>
												{elseif $key=='ilce'}
													<label class="col-md-2 control-label">{$key}</label>
													<select class="form-control" name="siparisdata[{$key}]">
														{foreach from=$this->extraservices->ilceler($order_data->il) item=ilce}
															<option value="{$ilce->id}" {if $ilce->id==$od}selected{/if} >{$ilce->ilce}</option>
														{/foreach}
													</select>
												{else}
														<label class="col-md-2 control-label">{$key}</label>
														<input class="form-control required_chk" type="text" name="siparisdata[{$key}]" value="{$od}"/>
												{/if}
												</div>
											{/if}
										{/foreach}
									</div>
									<div class="tab-pane  col-md-6 col-sm-12" id="odemedata">
										{foreach from=$odeme_data key=key item=odm}
												<div class="form-group">
													<label class="col-md-2 control-label">{$key}</label>
														<input class="form-control required_chk" type="text" name="odemedata[{$key}]" value="{$odm}"/>
												</div>
										{/foreach}
									</div>
									<div class="tab-pane active" id="montaj_detay">
										{foreach from=$urun_data key=key item=urn}
											<div class="col-md-12">
												<h6>STOK_KODU {$urn->stok_kodu}</h6>
												
													{$urn->hizmetler|print_r}
												{foreach from=$urn->hizmetler item=urn_hizmet}
												<div class="form-group" style="padding:0;">
													<label class="col-md-12 control-label" style="padding:0;font-weight:700;">{$urn_hizmet->montaj_hizmeti_adi}</label>
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Adet</label>
													<input name="mnt[{$urn_hizmet->id}][adet]" class="form-control " value="{$urn_hizmet->adet}" />
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Fiyat</label>
													<input name="mnt[{$urn_hizmet->id}][fiyats]" class="form-control" value="{$urn_hizmet->fiyat}" disabled />
												</div>
												<div class="form-group col-md-2 col-sm-12" style="padding:0;">
													<label class="col-sm-12 control-label" style="padding:0;font-weight:600;">Firma</label>
													<input name="mnt[{$urn_hizmet->id}][firma_id]" class="form-control" value="{$urn_hizmet->firma_id}" />
												</div>
												{/foreach}
											</div>
										{/foreach}
									</div>
									<div class="col-md-6">
									<div class="form-group pull-right">
												<button type="submit" class="btn blue">Kaydet</button>
									</div> 
									</div>
									<div class="clearfix"></div>
							</div>
					</div>
					</form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="montaj_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kargo_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="fatura_bilgileri" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carihesap" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carisiparis" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


<script type="text/javascript">
    function siparis_durum_degis(sipid) {
		var select = $("#siparis_durumu_"+sipid);
        var durum = $("#siparis_durumu_"+sipid+" option:selected").val();
        var send_data = {
            order_id:sipid,
            durum:durum
        };
        $.get( "{site_url('ajax/order_durum')}", send_data ).done(function( data ) {
			data = JSON.parse(data);
			if(data.status == 'error'){
                 select.val(data.durumu);
				 swal("Başarısız",data.msg,"warning");
			}else{
				 swal({
				  title: "Başarılı",
				  text: "Sipariş Durumu Güncellendi.",
				  timer: 1200
				});
				if(durum == 9 || durum==4){
					select.closest('tr').css('background-color','#ff6f6f');
				}
				if(durum == 2){
					select.closest('tr').css('background-color','palegreen');
				}
			}
            // console.log(data);
        });
    }
</script>
{include file="base/footer_txt.tpl"}
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
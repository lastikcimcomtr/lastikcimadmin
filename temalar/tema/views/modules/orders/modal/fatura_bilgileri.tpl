{$data =  $order->data|json_decode}
{$urunler = $order->urunler|json_decode}
{$odemedata = $order->odemedata|json_decode}
{$odemebanka =  $this->extraservices->paytype($odemedata)}

{if  $odemedata->artitaksit}
{$installment  = ($odemedata->installment * 1 ) + ($odemedata->artitaksit * 1) }
{else}
{$installment  = $odemedata->installment} 
{/if}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Fatura Bilgileri</h4>
</div>
<div class="modal-body">
<table border="" style="width:100%;border:1px solid #dedede;margin-top:10px;">
    <thead style="background-color:#F4F4F4;">
        <th style="padding:10px;border-bottom: 1px solid #DEDEDE;font-size:12px;"> Fatura Adresi </th>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 10px;font-size:12px;">
                {if $data->ticari_unvani}
                <span style='font-weight:700'>Ticari Ünvanı  </span> : {$data->ticari_unvani}<br>
                <span style='font-weight:700'>Telefon  </span> : {$data->kur_cep_telefonu}<br>
                <span style='font-weight:700'>Vergi Dairesi  </span> : {$data->vergi_dairesi}<br>
                <span style='font-weight:700'>Vergi No  </span> : {$data->vergi_no}<br>
                <span style='font-weight:700'>Posta Kodu </span> : {$data->kur_posta_kodu}<br>
                <span style='font-weight:700'>İl / İlçe  </span> : {$this->extraservices->il($data->kur_il)} / {$this->extraservices->ilce($data->kur_ilce)}<br>
                <span style='font-weight:700'>Adres </span> : {$data->kur_adres}<br>
                {else}
                <span style='font-weight:700'>Adı Soyadı </span> : {$data->adi} {$data->soyadi}<br>
                <span style='font-weight:700'>Cep Telefonu  </span> : {$data->cep_telefonu}<br>
                <span style='font-weight:700'>Ev Telefonu  </span> : {$data->ev_telefonu}<br>
                <span style='font-weight:700'>TC Kimlik No </span> : {$data->tc_kimlik_no}<br>
                <span style='font-weight:700'>Posta Kodu </span> : {$data->posta_kodu}<br>
                <span style='font-weight:700'>İl / İlçe  </span> : {$this->extraservices->il($data->il)} / {$this->extraservices->ilce($data->ilce)}<br>
                <span style='font-weight:700'>Adres </span> : {$data->adres}<br>
                {/if}
            </td>
        </tr>                                   
    </tbody>
</table> 
<table border="" style="width:100%;border:1px solid #dedede;margin-top:-1px;">
    <thead style="background-color:#F4F4F4;">
        <th style="padding:10px; border-bottom: 1px solid #DEDEDE;font-size:12px;"> Teslimat Adresi </th>
    </thead>
    <tbody>
        <tr>
            <td style="padding: 10px;font-size:12px;">
               {if $data->teslimyeri == "montaj_noktasina"}
               {$firma = $this->extraservices->montaj_noktalari($firma_id)}
               <span style='font-weight:700'>Montaj Noktasına Teslim : </span>   {$firma->firma_adi}<br />
               <span style='font-weight:700'>Montaj Noktasına Adresi : </span>   {$firma->firma_adresi}<br />
               <span style='font-weight:700'>İl / İlçe : </span>   {$firma->ilce} / {$firma->il}<br />
               <span style='font-weight:700'>Montaj Noktasına Telefonu : </span>   {$firma->firma_tel1}<br />
               {elseif  $data->teslimyeri == "teslim_adresine"}
               <span style='font-weight:700'>Adı Soyadı </span> : {$data->adi} {$data->soyadi}<br>
               <span style='font-weight:700'>Cep Telefonu  </span> : {$data->cep_telefonu}<br>
               <span style='font-weight:700'>Ev Telefonu  </span> : {$data->cep_telefonu}<br>
               <span style='font-weight:700'>TC Kimlik No </span> : {$data->tc_kimlik_no}<br>
               <span style='font-weight:700'>Posta Kodu </span> : {$data->posta_kodu}<br>
               <span style='font-weight:700'>İl / İlçe  </span> : {$this->extraservices->il($data->il)} / {$this->extraservices->ilce($data->ilce)}<br>
               <span style='font-weight:700'>Adres </span> : {$data->adres}<br>
               {else}
               {if $data->ticari_unvani}
               <span style='font-weight:700'>Ticari Ünvanı  </span> : {$data->ticari_unvani}<br>
               <span style='font-weight:700'>Telefon  </span> : {$data->kur_cep_telefonu}<br>
               <span style='font-weight:700'>Vergi Dairesi  </span> : {$data->vergi_dairesi}<br>
               <span style='font-weight:700'>Vergi No  </span> : {$data->vergi_no}<br>
               <span style='font-weight:700'>Posta Kodu </span> : {$data->kur_posta_kodu}<br>
               <span style='font-weight:700'>İl / İlçe  </span> : {$this->extraservices->il($data->kur_il)} / {$this->extraservices->ilce($data->kur_ilce)}<br>
               <span style='font-weight:700'>Adres </span> : {$data->kur_adres}<br>
               {else}
               <span style='font-weight:700'>Adı Soyadı </span> : {$data->adi} {$data->soyadi}<br>
               <span style='font-weight:700'>Cep Telefonu  </span> : {$data->cep_telefonu}<br>
               <span style='font-weight:700'>Ev Telefonu  </span> : {$data->ev_telefonu}<br>
               <span style='font-weight:700'>TC Kimlik No </span> : {$data->tc_kimlik_no}<br>
               <span style='font-weight:700'>Posta Kodu </span> : {$data->posta_kodu}<br>
               <span style='font-weight:700'>İl / İlçe  </span> : {$this->extraservices->il($data->il)} / {$this->extraservices->ilce($data->ilce)}<br>
               <span style='font-weight:700'>Adres </span> : {$data->adres}<br>
               {/if}
               {/if}   
           </td>
       </tr>
    </tbody>
</table>
</div>
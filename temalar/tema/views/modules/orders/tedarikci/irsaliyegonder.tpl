<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Tedarikçiye İletim Formu</h4>
</div>
<div class="modal-body">
	<div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-red-sunglo">
                <i class="fa fa-user"></i>
                <span class="caption-subject bold uppercase"> Tedarikçi ve Ürün Bilgileri </span>
            </div>
        </div>
        {foreach $urunler as $urun}
        <div class="portlet-body form">
        	<form class="tedarikci_data" action="javascript:void(-1)" autocomplete="off">
		        <div class="form-body">
                    <div class="form-group">
                        <label for="siparis_no">Sipariş No</label>
                        <input style="background-color: #dedede" id="siparis_no" name="siparis_no" type="text" class="form-control" placeholder="Sipariş Numarası" value="{$order->id}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label for="musteri_isim">Müşteri İsim</label>
                        <input id="musteri_isim" name="musteri_isim" type="text" class="form-control" placeholder="Cari Ünvanı" value="{$fatura['cari_unvan1']}" />
                    </div>
                    <div class="form-group">
                        <label for="musteri_isim">Müşteri Vergi Dairesi</label>
                        <input style="background-color: #dedede" id="musteri_isim" name="musteri_vergi_daire" type="text" class="form-control" placeholder="Cari Ünvanı" value="{if $fatura['vdaire_adi']}{$fatura['vdaire_adi']}{else}-{/if}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label for="musteri_isim">Müşteri Vergi No</label>
                        <input style="background-color: #dedede" id="musteri_isim" name="musteri_vergi_no" type="text" class="form-control" placeholder="Cari Ünvanı" value="{$fatura['vdaire_no']}" disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="urun_adi">Ürün Adı</label>
                        <input style="background-color: #dedede" id="urun_adi" name="urun_adi" type="text" class="form-control" placeholder="Ürün Adı" value="{$urun->urun_adi}"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="stok_kodu">Ürün Kodu</label>
                        <input style="background-color: #dedede" id="stok_kodu" name="stok_kodu" type="text" class="form-control" placeholder="Ürün Adı" value="{$urun->stok_kodu}"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="urun_miktar">Ürün Adet</label>
                        <input id="urun_miktar" name="urun_miktar" type="text" class="form-control" placeholder="Ürün Adet" value="{$urun->miktar}"/>
                    </div>
                    <div class="form-group">
                        <label for="tedarikci">Tedarikçi Adı</label>
                        <select id="tedarikci" name="tedarikci" class="form-control">
                            <option value="-1">GÖNDERME</option>
                            {foreach $tum_tedarikciler as $tedarikci}
                                <option value="{$tedarikci->kod}" {if $urun->tedarikci==$tedarikci->kod}selected{/if}>{$tedarikci->adi}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="musteri_adres">Fatura Adres</label>
                        <input style="background-color: #dedede"  id="musteri_adres" name="musteri_adres_fatura" type="text" class="form-control" placeholder="Fatura Adresi" value="{$fatura['cari_unvan1']} - {$fatura['tam_adres']} {$fatura['il_ilce']} TEL:{$fatura['telefon']}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label for="musteri_adres">Sevk Adres</label>
                        <input id="musteri_adres" name="musteri_adres_sevk" type="text" class="form-control" placeholder="Sevk Adresi" value="{$musteri['tam_adres']} {$musteri['il_ilce']} TEL:{$musteri['telefon']}" />
                    </div>
                </div>
                <div class="form-body">
                    <button type="button" class="btn default copy">Kopyala</button>
                </div>
		    </form>
		</div>
	</div>
    {/foreach}
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
    <button type="button" class="btn blue" id="carihesapacma" onclick="tedarikciler_gonder()">Kaydet</button>
</div>
{literal}
<script type="text/javascript">

    var tt = null;
    function tedarikciler_gonder(){
        var send_data = [];
        $('.tedarikci_data').each(function(){
            var data = {};
            data.siparis_no = $(this).find('[name=siparis_no]').val();
            data.musteri_isim = $(this).find('[name=musteri_isim]').val();
            data.urun_adi = $(this).find('[name=urun_adi]').val();
            data.stok_kodu = $(this).find('[name=stok_kodu]').val();
            data.urun_miktar = $(this).find('[name=urun_miktar]').val();
            data.urun_maliyet = $(this).find('[name=urun_maliyet]').val();
            data.tedarikci = $(this).find('[name=tedarikci]').val();
            data.musteri_adres_fatura = $(this).find('[name=musteri_adres_fatura]').val();
            data.musteri_adres_sevk = $(this).find('[name=musteri_adres_sevk]').val();
            data.musteri_vergi_daire = $(this).find('[name=musteri_vergi_daire]').val();
            data.musteri_vergi_no = $(this).find('[name=musteri_vergi_no]').val();
            data.unix_time = Date.now();
            if(data.tedarikci != -1){
                send_data.push(data);
            }
        }).promise().done(function(){
            if(send_data.length < 1){
                alert('Gönderim öğesi bulunmuyor.');
            }else{
                tedarikci_gonder(send_data);
            }
        });
    }
    $('.copy').click(function(){
        var form = $(this).closest('form');
        var form_clone = form.clone();
        form_clone.find('.copy').replaceWith('<button type="button" class="btn btn-danger remove">Kaldır</button>');
        form_clone.find('.remove').click(function(){
            $(this).closest('form').remove();
        });
        form.after(form_clone);
    });
	function tedarikci_gonder(data){
        console.log(data);
        var url = '/orders/createWaybillPdf/';
        $.ajax({
            type:'POST',
            dataType:'json',
            url:url,                        
            data: {data : data},
            success: function (textStatus, status) {
                if(textStatus.status == "false"){
                    tt = null;
                    alert(textStatus.type);
                }else{
                    tt = setTimeout(function(){ location.reload(); }, 1000);
                }
            },
            error: function(xhr, textStatus, error) {
                tt = false;
                alert(xhr.responseText + ' ' + error);
            }
        });
    }
</script>
{/literal}
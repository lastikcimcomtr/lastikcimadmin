<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Tedarikçiye İletim Formu</h4>
</div>
<div class="modal-body">
	<div class="portlet light bordered">
        <div class="portlet-title">
        	<div class="caption font-red-sunglo">
                <i class="fa fa-user"></i>
                <span class="caption-subject bold uppercase"> Tedarikçi ve Ürün Bilgileri </span>
            </div>
        </div>
        {foreach $urunler as $urun}
        <div class="portlet-body form">
        	<form class="tedarikci_data" action="javascript:void(-1)" autocomplete="off">
		        <div class="form-body">
                    <div class="form-group">
                        <label for="siparis_no">Sipariş No</label>
                        <input style="background-color: #dedede" id="siparis_no" name="siparis_no" type="text" class="form-control" placeholder="Sipariş Numarası" value="{$order->id}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label for="musteri_isim">Müşteri İsim</label>
                        <input id="musteri_isim" name="musteri_isim" type="text" class="form-control" placeholder="Cari Ünvanı" value="{$musteri['cari_unvan1']}" />
                    </div>
                    <div class="form-group">
                        <label for="urun_adi">Ürün Adı</label>
                        <input style="background-color: #dedede" id="urun_adi" name="urun_adi" type="text" class="form-control" placeholder="Ürün Adı" value="{$urun->urun_adi}"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="stok_kodu">Ürün Kodu</label>
                        <input style="background-color: #dedede" id="stok_kodu" name="stok_kodu" type="text" class="form-control" placeholder="Ürün Adı" value="{$urun->stok_kodu}"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="stok_kodu">Üretim Tarihi</label>
                        <input style="background-color: #dedede" id="uretim_tarihi" name="uretim_tarihi" type="text" class="form-control" placeholder="Ürün Adı" value="{$urun->uretim_tarihi}"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="urun_miktar">Ürün Adet</label>
                        <input style="background-color: #dedede" id="urun_miktar" name="urun_miktar" type="text" class="form-control" placeholder="Ürün Adet" value="{$urun->miktar}" disabled="disabled" />
                    </div>
                    <div class="form-group">
                        <label for="urun_maliyet">Ürün Maliyet</label>
                        <input id="urun_maliyet" name="urun_maliyet" type="text" class="form-control disabled" placeholder="Ürün Maliyet Fiyatı" value="{$urun->urun_alis_fiyati}" />
                    </div>
                    <div class="form-group">
                        <label for="tedarikci">Tedarikçi Adı</label>
                        <select id="tedarikci" name="tedarikci" class="form-control">
                            {foreach $tum_tedarikciler as $tedarikci}
                                <option value="{$tedarikci->kod}" {if $urun->tedarikci==$tedarikci->kod}selected{/if}>{$tedarikci->adi}</option>
                            {/foreach}
                            {foreach $castrol_extra as $castrol_ext}
                                <option value="{$castrol_ext->id}" selected>{$castrol_ext->adi}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="musteri_adres">Müşteri Adres</label>
                        <input id="musteri_adres" name="musteri_adres" type="text" class="form-control" placeholder="Cari Adresi" value="{$musteri['tam_adres']} {$musteri['il_ilce']} TEL:{$musteri['telefon']}" />
                    </div>
                </div>
		    </form>
		</div>
	</div>
    {/foreach}
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
    <button type="button" class="btn blue" id="carihesapacma" onclick="tedarikciler_gonder()">Kaydet</button>
</div>
<script type="text/javascript">
    var tt = null;
    function tedarikciler_gonder(){
        $('.tedarikci_data').each(function(){
            var data = {};
            data.siparis_no = $(this).find('[name=siparis_no]').val();
            data.musteri_isim = $(this).find('[name=musteri_isim]').val();
            data.urun_adi = $(this).find('[name=urun_adi]').val();
            data.stok_kodu = $(this).find('[name=stok_kodu]').val();
            data.urun_miktar = $(this).find('[name=urun_miktar]').val();
            data.uretim_tarihi = $(this).find('[name=uretim_tarihi]').val();
            data.urun_maliyet = $(this).find('[name=urun_maliyet]').val();
            data.tedarikci = $(this).find('[name=tedarikci]').val();
            data.musteri_adres = $(this).find('[name=musteri_adres]').val();
            data.unix_time = Date.now();
            tedarikci_gonder(data);
        }).promise().done(function(){
            tt = setTimeout(function(){ location.reload(); }, 1500);
        });
    }
	function tedarikci_gonder(data){
        var url = '{site_url('orders/tedarikciyegonder/')}';
        $.ajax({
            type:'POST',
            dataType:'json',
            url:url,                        
            data: data,
            success: function (textStatus, status) {
                if(textStatus.status == "false"){
                    tt = null;
                    alert(textStatus.type);
                }
            },
            error: function(xhr, textStatus, error) {
                tt = false;
                alert(xhr.responseText + ' ' + error);
            }
        });
    }
</script>
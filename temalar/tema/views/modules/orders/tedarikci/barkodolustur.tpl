<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Barkod Oluşturma Formu</h4>
</div>
<div class="modal-body">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-user"></i>
                <span class="caption-subject bold uppercase"> Teslimat ve Depo Bilgileri</span>
            </div>
        </div>
        {foreach $urunler as $urun}
        {if sizeof($urun->ekstra_msg) > 0}
            <div class="portlet-body form text-danger">
                {foreach $urun->ekstra_msg as $msg}
                    <p><b>{$msg}</b></p>
                {/foreach}
            </div>
        {/if}
        <div class="portlet-body form">
            <form class="tedarikci_data" action="javascript:void(-1)" autocomplete="off" id="{$urun->id}">
                <div class="form-body">
                    <div class="form-group">
                        <label for="kargo_firma_{$urun->id}">Kargo Firması</label>
                        <select id="kargo_firma_{$urun->id}" name="kargo_firma" class="form-control">
                            <option value="CEVA" selected>CEVA</option>
                            <option value="UPS">UPS</option>
                            <option value="MNG">MNG</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="siparis_no_{$urun->id}">Sipariş No</label>
                        <input style="background-color: #dedede" id="siparis_no_{$urun->id}" name="siparis_no" type="text"
                               class="form-control" placeholder="Sipariş Numarası" value="{$order->id}"
                               disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="tedarikci_{$urun->id}">Gönderim Depo Adı</label>
                        <select id="tedarikci_{$urun->id}" name="depo" class="form-control">
                            <option value="-1">Seçiniz</option>
                            {foreach $tum_depolar as $depo}
                                <option value="{$depo->id}"
                                        {if $urun->depo_data[0]->id==$depo->id}selected{/if}>{$depo->depo_adi}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="musteri_isim_{$urun->id}">Müşteri İsim</label>
                        <input style="background-color: #dedede" id="musteri_isim_{$urun->id}" name="musteri_isim" type="text"
                               class="form-control" placeholder="Cari Ünvanı" value="{$musteri['cari_unvan1']}"
                               disabled="disabled"/>
                    </div>
                    <div class="row">
                        <div class="col-md-8 p-0">
                            <div class="form-group">
                                <label for="urun_adi_{$urun->id}">Ürün</label>
                                <input style="background-color: #dedede;" id="urun_adi_{$urun->id}" name="urun_adi" type="text"
                                       class="form-control" placeholder="Ürün Adı" value="{$urun->urun_adi}"
                                       disabled="disabled"/>
                            </div>
                        </div>
                        <div class="col-md-4 p-0">
                            <div class="form-group">
                                <label for="urun_miktar_{$urun->id}">Ürün Adet</label>
                                <input style="background-color: #dedede;" id="urun_miktar_{$urun->id}" name="urun_miktar"
                                       type="text" class="form-control" placeholder="Ürün Adet"
                                       value="{$urun->miktar} Adet" disabled="disabled"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="stok_kodu_{$urun->id}">Ürün Kodu</label>
                        <input style="background-color: #dedede" id="stok_kodu_{$urun->id}" name="stok_kodu" type="text"
                               class="form-control" placeholder="Ürün Adı" value="{$urun->stok_kodu}"
                               disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label for="musteri_adres_{$urun->id}">Müşteri Adres</label>
                        <textarea style="background-color: #dedede" rows=4 class="form-control" id="musteri_adres_{$urun->id}"
                                  name="musteri_adres" placeholder="Cari Adresi"
                                  disabled="disabled">{$musteri['tam_adres']|trim} {$musteri['il_ilce']} TEL:{$musteri['telefon']}
                        </textarea>
                    </div>
                </div>
            </form>
        </div>
    {/foreach}
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
    <button type="button" class="btn blue" id="carihesapacma" onclick="kargo_gonder()">Kaydet</button>
</div>
{literal}
<script type="text/javascript">
    var tt = null;
    var barkod_veri = [];
    function kargo_gonder() {
        var kargo_firm_exp = '';
        $('.tedarikci_data').each(function () {
            let data = {};
            data.kargo_firma = $(this).find('[name=kargo_firma]').val();
            kargo_firm_exp = data.kargo_firma;
            data.siparis_no = $(this).find('[name=siparis_no]').val();
            data.depo = $(this).find('[name=depo]').val();
            data.urun_adi = $(this).find('[name=urun_adi]').val();
            data.stok_kodu = $(this).find('[name=stok_kodu]').val();
            data.urun_miktar = $(this).find('[name=urun_miktar]').val();
            data.musteri_adres = $(this).find('[name=musteri_adres]').val();
            barkod_veri.push(data);
        }).promise().done(function () {
            barkod_olustur(barkod_veri);
            //if (kargo_firm_exp !== 'MNG') {
                tt = setTimeout(function () {
                    location.reload();
                }, 1500);
            //}
        });
    }

    function barkod_olustur(data) {
        {/literal}
        var url = "{site_url('orders/siparisbarcodecreate/')}";
        {literal}
        $.ajax({
            type: 'POST',
            url: url,
            dataType: "text",
            data: {'data': data},
            success: function (cevap) {
                if (cevap.status) {
                    //location.reload();
                }
            }
        });
    }
</script>
{/literal}
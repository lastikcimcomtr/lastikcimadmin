<table class="table" cellpadding="5">
    <thead>
    <tr>
        <th>DURUM KODU</th>
        <th>AÇIKLAMA</th>
        <th>ŞUBE</th>
        <th>TARIH</th>
    </tr>
    </thead>
    <tbody>
    {foreach $result as $line}
    <tr>
        <td>{$line->NewStatusCode}</td>
        <td>{$line->Description}</td>
        <td>{$line->BranchName}</td>
        <td>{$line->ProcessingTime}</td>
    </tr>
    {/foreach}
    </tbody>

</table>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Pos İşlem Detayları</h4>
</div>
<div class="modal-body">
    {$data = json_decode($posdetaylar->data)}
    <table class="table table-striped table-hover table-bordered">    
    {foreach $data as $dkey => $dvalue}
        <thead>
            <tr> 
                <th colspan="2"> {$dkey} </th>
            </tr>
        </thead>
        <tbody>
            {foreach $dvalue as $dvalue_key => $dvalue_value}
            <tr>
                <td>{$dvalue_key}</td>
                <td style="word-break: break-all; white-space: normal;">{$dvalue_value}</td>
            </tr>
            {/foreach}
        </tbody>
    {/foreach}
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Kapat</button>
</div>
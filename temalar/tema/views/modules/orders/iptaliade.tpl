{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <style>
        .modal-dialog{
            width: 50%;
        }
    </style>
    <div class="row">
       <div class="col-md-12"> 
          <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <style> .islem_bilgilendirme span#islembilgisitext{ font-size:11px;margin-right: 5px !important; margin-left:0px !important;} .islem_bilgilendirme span{ margin-left:0px !important;margin-right:0px !important;text-align:right;} </style> 
                    <tr style="width:50%;">                         
                        <th class="islem_bilgilendirme" style="text-align:right;" align="right">
                         <span style="height:20px;padding:0px 5px 0 5px;background-color:#4B80B6;color:white;" href="#" title="Sipariş Notu" class="btn btn-sm btn-default">
                                <i class="fa fa-list-ol"></i>
                            </span>
                            <span id="islembilgisitext"> Ödeme Notu  </span>
                        <span style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                <i class="fa fa-times"></i>
                            </span>      
                            <span id="islembilgisitext"> Sil </span>
                        </th> 
                    </tr>
                </thead>
            </table>
            <style> #duzenleme_formu tr{ line-height: 25px; } </style>
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr> 
                        <th style="width: 12%;"> Ref No  </th>
                        <th> Üye Adı Soyadı </th>
                        <th > Sipariş Durumu </th>
                        <th> İade Talep Tarihi </th> 
                        <th> Sipariş Tarihi </th> 
                        <th> İade Nedeni </th> 
                        <th> Toplam Tutar </th> 
                        <th style="min-width:235px;width:235px;">  </th> 
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$orders item=order}
                    {$data =  $order->data|json_decode}
                    {$urunler = $order->urunler|json_decode}
                    {$odemedata = $order->odemedata|json_decode}
                    {$odemebanka =  $this->extraservices->paytype($odemedata)}
                    <tr> 
                        <td>{$order->id}</td>
                        <td>{if $data->ticari_unvani} {$data->ticari_unvani} {else}{$data->adi} {$data->soyadi}{/if}</td>
                        <td> {if $order->durumu} 
                            <span>
                            <select onchange="siparis_durum_degis({$order->id});" id="siparis_durumu_{$order->id}"  style="float:left;height: 27px;padding: 3px;margin-top: -5px;margin-right: 5px;">
                               {foreach from=$durumlar item=durum }
                               <option value="{$durum->id}" {if  $durum->id == $order->durumu}selected="selected"{/if}>{$durum->durum}</option>
                               {/foreach} 
                           </select>
                       </span>
                       <span id="siparis_durumu_sonuc_{$order->id}" style="float:left;">
                       </span>{else}
                        Sipariş İd Yanlış Kontrol Edin
                       {/if}
                   </td>
                   <td> {date('d.m.Y',$order->iade_talep_tarih)}</td>
                   <td> {date('d.m.Y',$order->tarih)}</td>
                   <td> {$order->iade_sebep}</td>
                   <td> {$odemedata->son_fiyat}</td>
                   <td>

                            <a style="height:20px;padding:0px 5px 0 5px;background-color: #32c5d2;border-color: #32c5d2;color: #fff;" data-toggle="modal" data-target="#siparis_notu_{$order->id}" title="Sipariş Notu" class="btn btn-sm btn-default">
                                <i class="fa fa-list-ol"></i>
                            </a>
                            <div id="siparis_notu_{$order->id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"> İade Notu </h4>
                                        </div>
                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="form-kutu">Not:</div>
                                                </div>
                                                <div class="col-sm-8">
                                                   {$order->iade_mesaj}
                                                </div>
                                                <div class="col-sm-offset-2"></div>
                                            </div>
                                    
                                        </div>

                                    </div>

                                </div>
                            </div>

                           {*} <a onclick="siparisYazdir( {$order->id} );" style="height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;" href="#" title="Çıktı Al" class="btn btn-sm btn-default">
                                <i class="fa fa-print"></i>
                            </a>*}
                            <a style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                <i class="fa fa-times"></i>
                            </a>                
                        </td>
                    </tr>
                    {/foreach}     
                </tbody>
            </table>       
        </div>
    </div>
</div>
<iframe id="frame_yazdir" name="frame_yazdir"  src="" style="visibility:hidden;width:0px;height:0px;"></iframe>
<script type="text/javascript">




    function siparis_durum_degis(sipid){
        var durum = $("#siparis_durumu_"+sipid+" option:selected").val();

        var send_data = {
            order_id:sipid ,
            durum:durum
        };

        $.get( "{site_url('ajax/order_durum')}", send_data ).done(function( data ) {

            if(durum == 3){
                $("#siparis_durumu_"+sipid).css('max-width','60%'); 
                $("#siparis_durumu_sonuc_"+sipid).html('<a style="height: 27px;padding: 6px 5px 0 5px;background-color:#DB53C8;color:white;width: 27px;margin-top: -5px;" data-toggle="modal" data-target="#siparis_kargo_no_'+sipid+'" title="Kargo Numarası Güncelle" class="btn btn-sm btn-default"><i class="fa fa-pencil-square-o"></i></a>');
                var kargo_numarasi = $("#siparis_durumu_"+sipid).attr("data-kargo");
                var kargo_firmasi = $("#siparis_durumu_"+sipid).attr("data-kargofirma");
                var kargo_no_html   = '<div id="siparis_kargo_no_'+sipid+'" class="modal fade" role="dialog">';
                kargo_no_html       += '<div class="modal-dialog">';
                kargo_no_html       += '<div class="modal-content" style="width: 50%;margin-left: 30%;margin-top: 30%;">';
                kargo_no_html       += '<div class="modal-header">';
                kargo_no_html       += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                kargo_no_html       += '<h4 class="modal-title"> Kargo Takip Kodu </h4>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '<div class="modal-body">';
                kargo_no_html       += '<div class="row">';
                kargo_no_html       += '<div class="col-sm-5"><div class="form-kutu">Kargo Firması : </div></div>';
                kargo_no_html       += '<div class="col-sm-5">';
                kargo_no_html       += '<select name="firma" class="form-control" >';
                {foreach from=$kargofirmalar item=firma}
                kargo_no_html       += '<option value="{$firma->id}">{$firma->firma_adi}</option>';
                {/foreach}
                kargo_no_html       += '</select>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '<div class="row" style="margin-top:5px;">';
                kargo_no_html       += '<div class="col-sm-5"><div class="form-kutu">Kargo Numarası : </div></div>';
                kargo_no_html       += '<div class="col-sm-5">';
                kargo_no_html       += '<input type="text" class="form-control" id="kargo_no_'+sipid+'" value = "'+kargo_numarasi+'" />';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '<div class="row">';
                kargo_no_html       += '<div class="col-sm-10 col-sm-offset-2" style="padding-top: 15px;font-size: 20px;">';
                kargo_no_html       += '<div class="form-kutu" id="siparis_kargo_sonuc_'+sipid+'"></div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '<div class="modal-footer">';
                kargo_no_html       += '<button type="button" class="btn btn-default" onclick="kargo_takip_no('+sipid+',$(\'#kargo_no_'+sipid+'\').val(),$(\'#firma_no_'+sipid+'\').val());">Kaydet</button>';
                kargo_no_html       += '<button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';
                kargo_no_html       += '</div>';

                $("#siparis_durumu_sonuc_"+sipid).append('<a style="height: 27px;padding: 6px 5px 0 5px;background-color:#7EC91D;color:white;width: 27px;margin-top: -5px;" title="Sipariş Durumu Güncellendi" class="btn btn-sm btn-default"><i class="fa fa-check"></i></a>');
                $("#siparis_durumu_sonuc_"+sipid).append(kargo_no_html);
            }else{
               $("#siparis_durumu_"+sipid).css('max-width','80%'); 
               $("#siparis_durumu_sonuc_"+sipid).html('<a style="height: 27px;padding: 6px 5px 0 5px;background-color:#7EC91D;color:white;width: 27px;margin-top: -5px;" title="Sipariş Durumu Güncellendi" class="btn btn-sm btn-default"><i class="fa fa-check"></i></a>');
           }



       });
}

</script>







</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
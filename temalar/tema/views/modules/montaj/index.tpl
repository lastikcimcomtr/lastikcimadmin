{include file="base/header.tpl"}
<style type="text/css">
.edit{
    height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;
}
.sil{
    height:20px; padding:0px 5px 0 5px; background-color:#E73434; color:white; border-radius:10px;
}

</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a href="{site_url('montaj/add')}" class="btn red btn-outline sbold"> 
                        <i class="fa fa-plus"></i> 
                        Montaj Noktası Ekle 
                    </a>
                </div>
            </div>
        </div>
        <div class="row portlet box blue">
                 <h2 class="portlet-title"><i class="icon-repairKeys"></i>Montaj Noktalarımız</h2>   
            <div class="col-md-12 portlet-body"> 
                <div class="container">
                                
                    <form class="col-md-3">
                        <label>İl Seçiniz</label>
                        <div class="select1 s2">
                            <select name="il">
                                {foreach from=$this->extraservices->iller() item=il}
                                <option value="{$il->id}" {if $il->id == 34}selected{/if}>{$il->il}</option>
                                {/foreach}
                            </select>
                        </div>
                        <label>İlçe Seçiniz</label>
                        <div class="select1 s2">
                            <select name="ilce">
                                <option value='0'>Tümü</option>
                            </select>
                        </div>
                    </form>
                    <div class="col-md-9">
                        <div id="map" style="height: 300px">
                        </div>
                    </div>                    
                    <div class="col-md-12" id='firmalist' style="margin-top: 50px">
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
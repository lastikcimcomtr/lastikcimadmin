{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
		<div class="note note-{$status}">
			<p> {$message} </p>
		</div>
		{/if}
		<div class="row">
			<div class="col-md-12"> 
				<form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off" >
					<div class="col-md-5">
						<div class="form-body">

							<div class="form-group">
								<label class="col-md-4 control-label">Firma Adı : <span class="required"> * </span></label>
								<div class="col-md-8">
									<input type="text" class="form-control required_chk" name="firma_adi" placeholder="Firma Adını Giriniz" required="required" value="{$firma->firma_adi}" />                                              
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Sıra : <span class="required"> * </span></label>
								<div class="col-md-8">
									<input type="number" class="form-control required_chk" name="sira" placeholder="Firma Sırası Giriniz" required="required" value="{$firma->sira}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Firma Türü : <span class="required"> * </span></label>
								<div class="col-md-8">
									<select name="type" class="form-control" required="required">
										<option value=""  {if $firma->type == ""}selected{/if}> Seçiniz </option>
										<option value="1" {if $firma->type == 1}selected{/if}>Yetkili Bayi</option>
										<option value="2" {if $firma->type == 2}selected{/if}>Anlaşmalı Nokta</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Nokta Tipi : </label>
								<div class="col-md-8">
									<select name="nokta_tipi" class="form-control">
										<option value=""  {if $firma->type == ""}selected{/if}> Seçiniz </option>
										{foreach from=$this->extraservices->nokta_tipleri() item=tip}
											<option value="{$tip->id}" {if $firma->nokta_tipi == $tip->id}selected{/if}>{$tip->adi}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Bulunduğu İl : <span class="required"> * </span></label>
								<div class="col-md-8">
									<select name="firma_il" class="form-control">
										{foreach from=$this->extraservices->iller() item=il}
										<option value="{$il->id}" {if $il->id == $firma->firma_il}selected{/if}>{$il->il}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-md-4 control-label">Bulunduğu İlçe : <span class="required"> * </span></label>
								<div class="col-md-8">
									<select name="firma_ilce" class="form-control">
										<option value='0'>Tümü</option>
										{foreach from=$this->extraservices->ilceler($firma->firma_il) item=ilce}
										<option value="{$ilce->id}" {if $ilce->id == $firma->firma_ilce}selected{/if}>{$ilce->ilce}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-md-4 control-label">Alternatif İlçeler : </label>
								<div class="col-md-8">
										{$alternatif_ilceler = explode(',',$firma->firma_alternatif_ilce)}
									<select name="firma_alternatif_ilce[]" class="form-control" multiple="multiple">
										<option value='0'>Tümü</option>
										{foreach from=$this->extraservices->ilceler($firma->firma_il) item=ilce}
										<option value="{$ilce->id}" {if in_array($ilce->id,$alternatif_ilceler)}selected{/if}>{$ilce->ilce}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Adress : <span class="required"> * </span></label>
								<div class="col-md-8">
									<textarea class="form-control" name="firma_adresi" required="required" rows="3">{$firma->firma_adresi}</textarea>                                                
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Firma Telefon 1 : <span class="required"> * </span></label>
								<div class="col-md-8">
									<input type="text" class="form-control required_chk phonemask"  name="firma_tel1" placeholder="Firma Telefonunu Giriniz" required="required" value="{$firma->firma_tel1}"/>                                              
								</div>
							</div>      <div class="form-group">
								<label class="col-md-4 control-label">Firma Telefon 2 : <span class="required"> * </span></label>
								<div class="col-md-8">
									<input type="text" class="form-control phonemask" name="firma_tel2" placeholder="Firma Telefonunu Giriniz" value="{$firma->firma_tel2}"/>                                              
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Firma Gerçek Adı: </label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="firm_real_name" placeholder="Firma Gerçek Adını Giriniz" value="{$firma->firm_real_name}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Firma Gerçek Telefon: </label>
								<div class="col-md-8">
									<input type="text" class="form-control phonemask" name="firm_real_tel" placeholder="Firma Gerçek Telefonunu Giriniz" value="{$firma->firm_real_tel}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Firma Gerçek Mail:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="firm_real_mail" placeholder="Firma Gerçek mail adresini Giriniz" value="{$firma->firm_real_mail}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Firma Gerçek Adres : </label>
								<div class="col-md-8">
									<textarea class="form-control" name="firm_real_adresi" required="required" rows="3">{$firma->firm_real_adresi}</textarea>                                                
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">13-17 İnç (HP-STD) montaj bedeli (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="hp_std_montaj" placeholder="13-17 İnç (HP-STD) montaj bedelini Giriniz" value="{$firma->hp_std_montaj}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">UHP montaj bedeli (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="uhp_montaj" placeholder="HP montaj bedelini Giriniz" value="{$firma->uhp_montaj}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">4x4/SUV montaj bedeli (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="suv_montaj" placeholder="4x4/SUV montaj bedelini Giriniz" value="{$firma->suv_montaj}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Hafif Ticari montaj bedeli (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="hticari_montaj" placeholder="Hafif Ticari montaj bedelini Giriniz" value="{$firma->hticari_montaj}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Lastik Saklama Ticari montaj bedeli (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="saklama" placeholder="Lastik Saklama Ticari montaj bedelini Giriniz" value="{$firma->saklama}" />                                              
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">İrtibat Sorumlu Adı (Not Amaçlıdır):</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="irtibat_sorumlu" placeholder="İrtibat sorumlusu adını Giriniz" value="{$firma->irtibat_sorumlu}" />                                              
								</div>
							</div>
							<div class="form-group"> 
								<label class="col-md-4 control-label">Sözleşme Durumu: </label>
								<div class="col-md-8">
									<select name="sozlesme" class="form-control">
										<option value='0' {if $firma->sozlesme==0}selected{/if}>YOK</option>
										<option value='1' {if $firma->sozlesme==1}selected{/if}>VAR</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label">Latitude : </label>
								<div class="col-md-8">						
								   <input type="text"  class="form-control required_chk" placeholder="Latitude Giriniz" name="lat"  value="{$firma->lat}"/>                                              
								</div>
							</div>	

							<div class="form-group">
								<label class="col-md-4 control-label">Longitude: </label>
								<div class="col-md-8">	
								<input type="text" class="form-control required_chk"  placeholder="Longitude Giriniz" name="lng"  value="{$firma->lng}"/>                                              
								</div> 
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label">Logo : </label>
												        	{if $firma->firma_logo}
				        	<div class="col-md-1">
				        		<a href="{$this->extraservices->fullresimurl('montaj_resim', {$firma->firma_logo})}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="{$this->extraservices->fullresimurl('montaj_resim', {$firma->firma_logo})}">
                                </a>
				        	</div>
				        	{/if}

								<div class="col-md-8">
									<input type="file" name="montaj_resim" />
								</div>
							</div>
						</div>
					</div>

					<div class="form-group col-md-7">
						<div id="map" style="height: 480px">
						</div>
					</div> 
					<div class="form-group col-md-12"> 
					</div> 
					
					{foreach from=$this->montaj->hizmetler() item=hizmet}
					<div class="form-group col-md-6">
						<div class="col-md-1">
							<input type="checkbox" name="hizmet[{$hizmet->id_str}][check]" data-sfiyat="hizmet[{$hizmet->id_str}][fiyat]" {if isset($hizmetler->{$hizmet->id_str})} checked {/if}/>
						</div>
						<label class="col-md-3 control-label" style="padding-top: 0px;">{$hizmet->baslik}</label>
						<div class="col-md-4">
							{$hizmet_mvct = $hizmetler->{$hizmet->id_str}}
							<input type="number" name="hizmet[{$hizmet->id_str}][fiyat]" placeholder="Hizmet Ücreti" value="{$hizmet_mvct['fiyat']}" {if !isset($hizmetler->{$hizmet->id_str}) }  style="display: none" {/if}/>
						</div>
						<div class="col-md-4" style="padding-right:10px">
							<input type="text"  placeholder="Not Alanı" name="hizmet[{$hizmet->id_str}][note]" class="note_p" value="{$hizmet_mvct['note']}" {if !isset($hizmetler->{$hizmet->id_str}) }style="display:none"{/if}/>
						</div>
					</div>
					{/foreach}



					<div class="form-body">
						<div class="form-group">
							<label class="col-md-11 control-label"></label>
							<div class="col-md-1">
								<button type="submit" class="btn blue">Kaydet</button>
							</div>
						</div>
					</div>   
				</form> 
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
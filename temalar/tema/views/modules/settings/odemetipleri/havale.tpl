
<style type="text/css">
	.modal-body .form-group {
		padding: 5px !important;
	}
	.islem_bilgilendirme span#islembilgisitext{
		font-size:11px;
		margin-right: 5px !important; 
		margin-left:0px !important;
	} 
	.islem_bilgilendirme span{
		margin-left:0px !important;
		margin-right:0px !important;
		text-align:right;
	} 
</style>

<div class="row">
    <div class="col-md-12" id="sonuc" style="font-size: 20px;font-weight: 600;margin: 10px 0;"></div>
</div>

<div class="row">
	<div class="col-md-12"> 
		<table class="table table-striped table-hover table-bordered" style="    background-color: #E9EDEF;">
            <thead>
                <tr style="width:50%;">                         
                    <th class="islem_bilgilendirme" style="text-align:left;">
                    	IBAN Kullanılan Banka Hesapları
                    </th> 
                </tr>
            </thead>
        </table>
	</div>
	<div class="col-md-12"> 
		<table class="table table-striped table-hover">
            <thead>
                <tr style="width:50%;">                         
                    <th class="islem_bilgilendirme" style="border-bottom:none;">
                    	<a class="btn red btn-outline sbold" data-toggle="modal" href="#hesap__ekle">
                            <i class="fa fa-plus"></i> Ekle
                        </a>
                        <div class="modal fade in" id="hesap__ekle" tabindex="-1" role="hesap__ekle" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Hesap Ekle</h4>
                                    </div>
                                    <div class="modal-body">
										<div class="tab-pane" id="tab_kategori_ekle">
                                            <form class="form-horizontal form-row-seperated" id="iban_hesap_ekle" method="post" onsubmit="iban_hesap_ekle();">
                                                <div class="row">
										        	<div class="col-md-12" style="display: none;" id="iban_ekle_cevap"></div>
										        </div>

										        <div class="form-group">
                                                    <label class="col-md-4 control-label">Banka ismi : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_banka_ismi" placeholder="">                                            
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Hesap Türü : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_hesap_turu" placeholder="">                                            
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Hesap Sahibi : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_hesap_sahibi" placeholder="">                                            
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">IBAN No : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_iban" placeholder="">                                            
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Şube Adı : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_sube_adi" placeholder="">                                            
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Şube Kodu : <span class="required"> * </span></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="text" class="form-control" id="iban_sube_kodu" placeholder="">                                            
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label>
                                                    <div class="col-md-8" id="kategori_adi">
                                                        <input type="button" class="btn btn-primary" value="Ekle" onclick="iban_hesap_ekle();">   
                                                        <button type="button" class="btn dark btn-outline" onclick="yenile();" data-dismiss="modal">Kapat</button>                                         
                                                    </div>
                                                </div> 
                                            </form>
                                        </div> 
                                    </div> 
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </th> 
                </tr>
            </thead>
        </table>
	</div>
	<div class="col-md-12"> 
		<div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr> 
                        <th style="width: 15%;"> Banka İsmi </th>
                        <th style="width: 15%;"> Hesap Türü </th>
                        <th style="width: 20%;"> Hesap Sahibi </th>
                        <th style="width: 15%;"> Şube Adı </th>
                        <th style="width: 10%;"> Şube Kodu </th>
                        <th style="width: 20%;"> IBAN Numarası </th>
                        <th style="width:2%;"></th>
                        <th style="width:2%;"></th>
                    </tr>
                </thead>
                <tbody>
                   {foreach from=$bankalar item=bk}
                	<tr>
            		 	<td> {$bk->banka_ismi} </td>
                        <td> {$bk->hesap_turu} </td>
                        <td> {$bk->hesap_sahibi} </td> 
                        <td> {$bk->sube_adi} </td> 
                        <td> {$bk->sube_kodu} </td> 
                        <td> {$bk->iban} </td>
                        <td>
                        	<a style="height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;" data-toggle="modal" href="#banka_hesabi_{$bk->id}" title="Düzenle" class="btn btn-sm btn-default">
                                <i class="fa fa-pencil-square-o"></i>
                            </a> 
                            <div class="modal fade in" id="banka_hesabi_{$bk->id}" tabindex="-1" role="banka_hesabi_{$bk->id}" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Hesap Düzenle</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="tab-pane" id="tab_kategori_ekle">
                                                <form class="form-horizontal form-row-seperated" id="iban_hesap_guncelle" method="post" action="javascript:void(0);" onsubmit="iban_hesap_guncelle({$bk->id});">
                                                    <div class="row">
                                                        <div class="col-md-12" style="display: none;" id="iban_hesap_guncelle{$bk->id}"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Banka ismi : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_banka_ismi{$bk->id}" placeholder="" value="{$bk->banka_ismi}">                                            
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Hesap Türü : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_hesap_turu{$bk->id}" placeholder="" value="{$bk->hesap_turu}">                                            
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Hesap Sahibi : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_hesap_sahibi{$bk->id}" placeholder="" value="{$bk->hesap_sahibi}">                                            
                                                        </div>
                                                    </div> 
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">IBAN No : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_iban{$bk->id}" placeholder="" value="{$bk->iban}">                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Şube Adı : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_sube_adi{$bk->id}" placeholder="" value="{$bk->sube_adi}">                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Şube Kodu : <span class="required"> * </span></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="text" class="form-control" id="iban_sube_kodu{$bk->id}" placeholder="" value="{$bk->sube_kodu}">                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label"></label>
                                                        <div class="col-md-8" id="kategori_adi">
                                                            <input type="submit" class="btn btn-primary" value="Kaydet">                                            
                                                        </div>
                                                    </div> 
                                                </form>
                                            </div> 
                                        </div> 
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>                        	
                        </td>
                        <td>
                        	<a style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="javascript:bankasil({$bk->id})" title="Sil" class="btn btn-sm btn-default">
                                <i class="fa fa-times"></i>
                            </a> 
                        </td>
                	</tr>
               {/foreach}
                </tbody>
            </table>
        </div>
	</div>
</div>

<script> 

	function iban_hesap_ekle(){
		var banka_ismi 	 = $("#iban_banka_ismi").val();
		var hesap_turu 	 = $("#iban_hesap_turu").val();
        var iban         = $("#iban_iban").val();
        var hesap_sahibi = $("#iban_hesap_sahibi").val(); 

        var sube_adi         = $("#iban_sube_adi").val();
		var sube_kodu 		 = $("#iban_sube_kodu").val();

		var query = "sube_adi="+sube_adi+"&sube_kodu="+sube_kodu+"&banka_ismi="+banka_ismi+"&hesap_turu="+hesap_turu+"&iban="+iban+"&hesap_sahibi="+hesap_sahibi+"&tip=2";
		if(banka_ismi != "" && hesap_turu != "" && iban != "" && hesap_sahibi != "" ){			
			$.get( "{site_url('ajax/bankahesapekle')}?"+query, function( data ) {
            	$("div#iban_ekle_cevap").html('<div class="note note-success"><p> '+data+' </p></div>');
        	});
            $("div#iban_ekle_cevap").show(); 
            yenile();
		}else{
			$("div#iban_ekle_cevap").html('<div class="note note-success"><p> Zorunlu Alanları Doldurun </p></div>');
			$("div#iban_ekle_cevap").show();
		}
	}

function bankasil(id){
    var query = "id="+id;
     $.get( "{site_url('ajax/bankasil')}?"+query, function( data ) {
               $("div#sonuc").html('<div class="note note-success"><p> '+data+' </p></div>');
            });
      
            yenile();
}

    function iban_hesap_guncelle(id){
        var banka_ismi   = $("#iban_banka_ismi"+id).val();
        var hesap_turu   = $("#iban_hesap_turu"+id).val();
        var iban         = $("#iban_iban"+id).val();
        var hesap_sahibi = $("#iban_hesap_sahibi"+id).val(); 

        var sube_adi         = $("#iban_sube_adi"+id).val();
        var sube_kodu        = $("#iban_sube_kodu"+id).val();

        var query = "sube_adi="+sube_adi+"&sube_kodu="+sube_kodu+"&id="+id+"&banka_ismi="+banka_ismi+"&hesap_turu="+hesap_turu+"&iban="+iban+"&hesap_sahibi="+hesap_sahibi+"&tip=2";
        if(banka_ismi != "" && hesap_turu != "" && iban != "" && hesap_sahibi != "" ){
            
            $.get( "{site_url('ajax/bankahesapguncelle')}?"+query, function( data ) {
                $("div#iban_hesap_guncelle"+id).html('<div class="note note-success"><p> '+data+' </p></div>');
            });
            $("div#iban_hesap_guncelle"+id).show();
            yenile();
        }else{
            $("div#iban_hesap_guncelle"+id).html('<div class="note note-success"><p> Zorunlu Alanları Doldurun </p></div>');
            $("div#iban_hesap_guncelle"+id).show();
        }
    }

	function yenile() {
		setTimeout(function() {

			window.location.reload();
		}, 1000);
	}

</script>
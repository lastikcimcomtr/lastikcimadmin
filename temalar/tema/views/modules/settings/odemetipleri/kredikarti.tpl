
<style type="text/css">
.modal-body .form-group {
	padding: 5px !important;
}
.islem_bilgilendirme span#islembilgisitext{
	font-size:11px;
	margin-right: 5px !important; 
	margin-left:0px !important;
} 
.islem_bilgilendirme span{
	margin-left:0px !important;
	margin-right:0px !important;
	text-align:right;
} 
</style>
				{$postipleri = $this->settings->postipleri() }
<div class="row">
	<div class="col-md-12" id="paypal_guncelleme_sonuc" style="font-size: 20px;font-weight: 600;margin: 10px 0;">sonuc</div>
</div>

<div class="row">
	<div class="col-md-12"> 
		<table class="table table-striped table-hover table-bordered" style="    background-color: #E9EDEF;">
			<thead>
				<tr style="width:50%;">                         
					<th class="islem_bilgilendirme" style="text-align:left;">
						Sanal Pos Listesi 
					</th> 
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-12"> 
		<table class="table table-striped table-hover">
			<thead>
				<tr style="width:50%;">                         
					<th class="islem_bilgilendirme" style="border-bottom:none;">
						<a class="btn red btn-outline sbold" data-toggle="modal" href="#sp_ekle">
							<i class="fa fa-plus"></i> Ekle
						</a>
						<div class="modal fade in" id="sp_ekle" tabindex="-1" role="sp_ekle" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Sanal Pos Ekle</h4>
									</div>
									<div class="modal-body">
										<div class="tab-pane" id="tab_kategori_ekle">
											<form class="form-horizontal form-row-seperated" id="sanalpos_ekle" method="post" action="javascript:void(0);" onsubmit="sp_ekle();">
												<div class="row">
													<div class="col-md-12" style="display: none;" id="sp_ekle_cevap"></div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Banka ismi : <span class="required"> * </span></label>
													<div class="col-md-8" id="kategori_adi">
														<input type="text" class="form-control" name="banka" id="sp_banka_ismi" placeholder="">                                            
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Kredi Kartı : <span class="required"> * </span></label>
													<div class="col-md-8" id="kategori_adi">
														<input type="text" class="form-control" name="pos" id="sp_pos">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-4 control-label">Pos Tipi <span class="required"> * </span></label>
													<div class="col-md-8" id="kategori_adi">
														<select class="form-control" name="pos_tip"  data-sp='' data-bolum='sanalpos_ekle'>
																<option value="" data-veri=''>Seçiniz</option>
															{foreach from=$postipleri item=pos}
															<option value="{$pos->id}" data-veri='{$pos->data}'>{$pos->id}</option>
															{/foreach}
														</select>
													</div>
												</div>  
											<div class="sanalpos_ekle">
							
												</div>

												<div class="form-group">
													<label class="col-md-4 control-label"></label>
													<div class="col-md-8">
														<input type="submit" class="btn btn-primary" value="Kaydet">   
														<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>                                         
													</div>
												</div> 
											</form>
										</div> 
									</div> 
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
					</th> 
				</tr>
			</thead>
		</table>
	</div>
	<div class="col-md-12"> 
		<div class="table-responsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr> 
						<th style="width: 1%;text-align:center;"> ID </th>
						<th style="width: 20%;"> Banka </th>
						<th style="width: 70%;"> Kart </th> 
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						<th style="width:1%;"></th>
						{*} <th style="width:1%;"></th>{*}
					</tr>
				</thead>
				<tbody> 

	

					{foreach from=$sanalposlar item=sp}

					<tr>
						<td style="text-align: center;"> {$sp->id} </td>
						<td> {ucwords($sp->banka)} </td>
						<td> {ucwords($sp->pos)} </td> 
						<td style="width:1%;">
							{if $sp->durumu == 1}
							{$drenk = "3385B7"}
							{$gun = 0}
							{else}
							{$gun = 1}
							{$drenk = "ffffff"}
							{/if}
							<a style="width:20px;height:20px;padding:0px 5px 0 5px;background-color:#{$drenk};color:white;" onclick="pos_durum_degis({$sp->id},'durumu',{$gun});" title="Durumu" class="btn btn-sm btn-default">
								<i class=""></i>
							</a>
						</td>  
						<td style="width:1%;">
							{if $sp->ana_pos == 1}
							{$drenk = "CC0000"}
							{$gun = 0}
							{else}
							{$gun = 1}
							{$drenk = "ffffff"}
							{/if}

							<a style="width:20px;height:20px;padding:0px 5px 0 5px;background-color:#{$drenk};color:white;" onclick="pos_durum_degis({$sp->id},'ana_pos',{$gun});" title="Ana Pos" class="btn btn-sm btn-default">
								<i class=""></i>
							</a>
						</td>  
                       {*} <td style="width:1%;">
                        	           		{if $sp->payu_pos == 1}
							{$drenk = "EE8701"}
							{$gun = 0}
							{else}
							{$gun = 1}
							{$drenk = "ffffff"}
							{/if}
				
                        	<a style="width:20px;height:20px;padding:0px 5px 0 5px;background-color:#{$drenk};color:white;" onclick="pos_durum_degis({$sp->id},'payu_pos',{$gun});" title="Payu Pos" class="btn btn-sm btn-default">
                                <i class=""></i>
                            </a>
                        </td>   {*}  


            <td>
                <a data-toggle="modal" href="#sanalpos_resmi_{$sp->id}"  class="btn btn-sm btn-default detayliduzenle" title="Resim Düzenle">
                    <span aria-hidden="true" class="icon-picture"></span>
                </a> 
                <div class="modal fade" id="sanalpos_resmi_{$sp->id}" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content" style="width: 508px;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Kart Logosu</h4>
                            </div>
                            <div class="modal-body"> 
                                <form method="post" enctype="multipart/form-data" action="{site_url('settings/editspimage')}/{$sp->id}">

                                	<div class="form-group row">
                                		<div class="fileinput-preview thumbnail  col-md-4" data-trigger="fileinput" style="width: 165px;height: 40px;padding: 0px;">
                                			{if $sp->resim}
                                			<img src="{$this->extraservices->fullresimurl('sanalpos_resim', $sp->resim)}" alt="" />
                                			{else}
                                			<img src="http://www.placehold.it/165x40/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                			{/if}
                                		</div>
                                		<div class="col-md-6">
                                			<span class="btn red btn-outline btn-file" style="height: 40px;padding-top: 9px;margin-left: 28px;">
                                				<span class="fileinput-new"> Resim Seçiniz </span>
                                				<span class="fileinput-exists"> Değiştir </span>
                                				<input type="file" name="resim"> </span>
                                			</div>
                                			<div class="form-group  col-md-2" style="margin: 0px">
                                				<button type="submit" class="btn blue" style="height: 40px;margin-top: -5px;">Kaydet</button>
                                			</div>
                                		</div>   

                                   {*} <div class="form-group col-md-8">                                                                                    
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail  col-md-6" data-trigger="fileinput" style="width: 180px; height: 45px;">
                                                {if $sp->resim}
                                                <img src="{$this->extraservices->fullresimurl('sanalpos_resim', $sp->resim)}" alt="" />
                                                {else}
                                                <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                {/if}
                                            </div>
                                            <div class="col-md-6">
                                                <span class="btn red btn-outline btn-file">
                                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                                    <span class="fileinput-exists"> Değiştir </span>
                                                    <input type="file" name="resim"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group  col-md-4">
                                        <button type="submit" class="btn blue">Kaydet</button>
                                    </div>{*}
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </td>


                        <td style="width:1%;">
                        	<a style="width:25px;height:20px;padding:0px 5px 0 5px;background-color:#3385B7;color:white;" data-toggle="modal" href="#banka_oranlar_{$sp->id}" title="Oranlar" class="btn btn-sm btn-default">
                        		<i class="fa fa-pencil"></i>
                        	</a>
                        	<div class="modal fade in" id="banka_oranlar_{$sp->id}" tabindex="-1" role="banka_oranlar_{$sp->id}" aria-hidden="true">
                        		<div class="modal-dialog">

                        			{$oranlar = $sp->oranlar}
                        			{$oranlar = json_decode($oranlar)} 

                        			<div class="modal-content" style="width:125%">
                        				<div class="modal-header">
                        					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        					<h4 class="modal-title">Oranlar</h4>
                        				</div>
                        				<div class="modal-body">
                        					<div class="tab-pane" id="tab_kategori_ekle">
                        						<form class="form-horizontal form-row-seperated" id="sanalpos_oranlar_ekle_{$sp->id}" method="post" action="javascript:void(0);" onsubmit="sp_oran_ekle({$sp->id});">
                        							<div class="row">
                        								<div class="col-md-12" style="display: none;" id="sp_oranlar_cevap_{$sp->id}"></div>
                        							</div>
                        							<div class="row">
                        								<div class="col-md-12">
                        									<div class="note note-success"><p> Buradaki Oranlar (%) Yüzde cinsinden yazılacaktır </p></div>
                        								</div>
                        							</div>

                        							<div class="form-group">
                        								<label class="col-md-3 control-label">Oranlar : </label>
                        								<div class="col-md-9" id="kategori_adi">
                        									<div class="row"> 
                        										{for $o=0 to 11}
                        										<div class="col-md-4" style="background: #c9f0f3;  padding-top: 4px; padding-bottom: 0px; border: white; border-style: solid;    border-width: 2px;">
                        											<div class="row"> 
                        												<span class="col-md-4" style="float:left;font-size: 20px;line-height: 40px;padding-left: 40px;">{($o+1)|str_pad:2:"0":$smarty.const.STR_PAD_LEFT} .</span>
                        												<span class="col-md-8" style="float:left;width:60%;"><input type="text" name="oran[{$o}]" class="form-control" value="{$oranlar->oran[$o]}"></span>
                        											</div>
                        											<div class="row"> 
                        												<span class="col-md-4" style="float:left;font-size: 12px;line-height: 40px;padding-right: 3px;">Artı Taksit</span>
                        												<span class="col-md-8" style="float:left;width:60%;"><input type="text" name="oran_arti[{$o}]" class="form-control" value="{$oranlar->oran_arti[$o]}"></span>
                        											</div>
                        										</div> 
                        										{/for}


                        									</div>
                        								</div>
                        							</div> 

                        							<div class="row">
                        								<div class="col-md-12">
                        									<div class="note note-success"><p> Ondalık için (.) nokta kullanınız, (,) virgül kullanmayınız. </p></div>
                        								</div>
                        							</div>	
													{*}
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" style="text-align:left;">+ Taksit Seçenekleri : </label>
                                                        <div class="col-md-9" id="kategori_adi">
															<div class="row">  
																<div class="col-md-1" style="padding:0px">
																	<input type="text" name="fiyat_taksit_min" id="fiyat_taksit_min" class="form-control" value="{$oranlar->fiyat_taksit_min}">
																</div>    
																<div class="col-md-2" style="padding:0px 5px;line-height:30px;text-align:center;">
																	TL Üzeri
																</div>  
																<div class="col-md-1" style="padding:0px;">
																	<input type="text" name="taksit_ilk" id="taksit_ilk" class="form-control" value="{$oranlar->taksit_ilk}"> 
																</div>   
																<div class="col-md-1" style="padding:0px 5px;line-height:30px;text-align:center;">
																	- 
																</div>  
																<div class="col-md-1" style="padding:0px;">
																	<input type="text" name="taksit_son" id="taksit_son" class="form-control" value="{$oranlar->taksit_son}"> 
																</div> 
																<div class="col-md-2" style="padding:0px 5px;line-height:30px;text-align:center;">
																	Arası + 
																</div>  
																<div class="col-md-1" style="padding:0px;">
																	<input type="text" name="arti_taksit" id="arti_taksit" class="form-control" value="{$oranlar->arti_taksit}"> 
																</div> 
																<div class="col-md-2" style="padding:0px 5px;line-height:30px;text-align:center;">
																	Taksit
																</div> 
															</div>
                                                        </div>
                                                    </div> 												
                                                    {*}
                                                    <div class="form-group">
                                                    	<label class="col-md-3 control-label"></label>
                                                    	<div class="col-md-9" id="kategori_adi">
                                                    		<input type="submit" class="btn btn-primary" value="Kaydet">   
                                                    		<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>                                         
                                                    	</div>
                                                    </div> 

                                                </form>
                                            </div> 
                                        </div> 
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div> 
                        </td>    
                        <td>
                        	<a style="height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;" data-toggle="modal" href="#banka_pos_bilgileri{$sp->id}" title="Düzenle" class="btn btn-sm btn-default">
                        		<i class="fa fa-pencil-square-o"></i>
                        	</a> 
                        	<div class="modal fade in" id="banka_pos_bilgileri{$sp->id}" tabindex="-1" role="banka_pos_bilgileri{$sp->id}" aria-hidden="true">
                        		<div class="modal-dialog">
                        			<div class="modal-content">
                        				<div class="modal-header">
                        					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        					<h4 class="modal-title">Pos Bilgileri</h4>
                        				</div>
                        				<div class="modal-body">
                        					<div class="tab-pane" id="tab_kategori_ekle">
                        						<form class="form-horizontal form-row-seperated" id="sp_guncelle_{$sp->id}" method="post" action="javascript:void(0);" onsubmit="sp_guncelle({$sp->id});">
                        							<div class="row">
                        								<div class="col-md-12" style="display: none;" id="sp_guncelle_cevap_{$sp->id}"></div>
                        							</div>
                        							<div class="form-group">
                        								<label class="col-md-4 control-label">Banka ismi : <span class="required"> * </span></label>
                        								<div class="col-md-8" id="kategori_adi">
                        									<input type="text" class="form-control" name="banka" id="sp_banka_ismi_{$sp->id}" placeholder="" value="{$sp->banka}">                                            
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="col-md-4 control-label">Kredi Kartı : <span class="required"> * </span></label>
                        								<div class="col-md-8" id="kategori_adi">
                        									<input type="text" class="form-control" name="pos"  value="{$sp->pos}">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="col-md-4 control-label">Pos Tipi <span class="required"> * </span></label>
                        								<div class="col-md-8" id="kategori_adi">
                        									<select class="form-control" name="pos_tip"  data-sp='{$sp->data}' data-bolum='sp_guncelle_{$sp->id}'>
                        										{foreach from=$postipleri item=pos}
                        										<option value="{$pos->id}" data-veri='{$pos->data}'{if $sp->pos_tip == $pos->id}selected="selected"{$pveri = $pos->data|json_decode:true}{/if}>{$pos->id}</option>
                        										{/foreach}
                        									</select>
                        								</div>
                        							</div>  
                        							{$sp_js = $sp->data|json_decode}

                        							<div class="sp_guncelle_{$sp->id}">
                        								{foreach from=$pveri item=pp key=key}
                        								<div class="form-group">
                        									<label class="col-md-4 control-label">{$pp['title']} : </label>
                        									<div class="col-md-8">
                        										<input type="text" class="form-control" name="data[{$key}]"  value="{if $sp_js->{$key}}{$sp_js->{$key}}{else}{$pp['default']} {/if}">
                        									</div>
                        								</div>
                        								{/foreach}
                        							</div>

                        							<div class="form-group">
                        								<label class="col-md-4 control-label"></label>
                        								<div class="col-md-8">
                        									<input type="submit" class="btn btn-primary" value="Kaydet">   
                        									<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>                                         
                        								</div>
                        							</div> 
                        						</form>
                        					</div> 
                        				</div> 
                        			</div>
                        			<!-- /.modal-content -->
                        		</div>
                        		<!-- /.modal-dialog -->
                        	</div>                        	
                        </td>
						<td>
                        	<a style="height:20px;padding:0px 5px 0 5px;background-color:#0fd9f5;color:white;" data-toggle="modal" href="#banka_pos_aciklama{$sp->id}" title="Pos Açıklama" class="btn btn-sm btn-default">
                        		<i class="fa fa-pencil-square"></i>
                        	</a> 
                        	<div class="modal fade in" id="banka_pos_aciklama{$sp->id}" tabindex="-1" role="banka_pos_aciklama{$sp->id}" aria-hidden="true">
                        		<div class="modal-dialog modal-lg">
                        			<div class="modal-content">
                        				<div class="modal-header">
                        					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        					<h4 class="modal-title">Pos Açıklama</h4>
                        				</div>
                        				<div class="modal-body">
                        					<div class="tab-pane" id="tab_kategori_ekle">
                        						<form class="form-horizontal form-row-seperated" id="sp_guncelle_aciklama{$sp->id}" method="post" action="javascript:void(0);" onsubmit="sp_guncelle_aciklama({$sp->id});">
                        							<div class="row">
                        								<div class="col-md-12" style="display: none;" id="spa_guncelle_cevap_{$sp->id}"></div>
                        							</div>
                        							<div class="form-group">
                        								<label class="col-md-2 control-label">Pos Açıklama Başlığı: <span class="required"> * </span></label>
                        								<div class="col-md-10" id="kategori_adi">
															<textarea class="ckeditor form-control" name="pos_aciklama_kisa" id="sp_banka_ismi_{$sp->id}">{$sp->pos_aciklama_kisa}</textarea>                                           
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="col-md-2 control-label">Pos Açıklama : <span class="required"> * </span></label>
                        								<div class="col-md-10" id="kategori_adi">
															<textarea class="ckeditor form-control" name="pos_aciklama" id="sp_banka_ismi_{$sp->id}">{$sp->pos_aciklama}</textarea>                                           
                        								</div>
                        							</div>
													<div class="form-group">
														<label class="col-md-2 control-label">Min. Kamp. Tutar : <span class="required"> * </span></label>
														<div class="col-md-10" id="kategori_adi">
															<input type="number" class="form-control" name="kampanya_min_tutar" id="sp_banka_ismi_{$sp->id}" value="{$sp->kampanya_min_tutar}" required>
														</div>
													</div>

                        							<div class="form-group">
                        								<label class="col-md-8 control-label"></label>
                        								<div class="col-md-4">
                        									<input type="submit" class="btn btn-primary" value="Kaydet">   
                        									<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>                                         
                        								</div>
                        							</div> 
                        						</form>
                        					</div> 
                        				</div> 
                        			</div>
                        			<!-- /.modal-content -->
                        		</div>
                        		<!-- /.modal-dialog -->
                        	</div>                        	
                        </td>
                        <td>
                        	<a style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" onclick="spsil({$sp->id}); return false;" href="#" title="Sil" class="btn btn-sm btn-default">
                        		<i class="fa fa-times"></i>
                        	</a> 
                        </td>
                    </tr> 
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>

<script> 
	function pos_durum_degis(id, durum, islem){
		$.get( "{site_url('ajax/postdurum')}?durum="+durum+"&id="+id+"&islem="+islem, function( data ) {
			var html = jQuery.parseJSON(data);
			console.log(html);
			yenile();
		});
	}
	function sp_oran_ekle(id){
		var query = $("#sanalpos_oranlar_ekle_"+id).serialize();
		// console.log(form_data); 
		$.get( "{site_url('ajax/posoran')}?"+query+"&id="+id, function( data ) {
			var html = jQuery.parseJSON(data);
			$("div#sp_oranlar_cevap_"+id).html('<div class="note note-success"><p> '+html+' </p></div>');
		});
		$("div#sp_oranlar_cevap_"+id).show();
		yenile();
	}
	function sp_ekle(){
		var query = $("#sanalpos_ekle").serialize();
		$.get( "{site_url('ajax/posekle')}?"+query, function( data ) {
			var html = jQuery.parseJSON(data);
			$("div#sp_ekle_cevap").html('<div class="note note-success"><p> '+html+' </p></div>');
		});
		$("#sp_ekle_cevap").show();
	yenile();
	}

	function sp_guncelle(id){
		var query = $("#sp_guncelle_"+id).serialize();
		$.get( "{site_url('ajax/posguncelle')}?id="+id+"&"+query, function( data ) {
			var html = jQuery.parseJSON(data); 
			$("#sp_guncelle_cevap_"+id).html('<div class="note note-success"><p> '+html+' </p></div>');
			$("#sp_guncelle_cevap_"+id).show();
			//yenile();
		});
	} 
	function sp_guncelle_aciklama(id){
		var query = $("#sp_guncelle_aciklama"+id).serialize();
		$.get( "{site_url('ajax/posguncelleaciklama')}?id="+id+"&"+query, function( data ) {
			var html = jQuery.parseJSON(data); 
			$("#spa_guncelle_cevap_"+id).html('<div class="note note-success"><p> '+html+' </p></div>');
			$("#spa_guncelle_cevap_"+id).show();
			//yenile();
		});
	} 
 function spsil(id){
     swal({
        title: "Emin misin?",
        text: "Sanal posu silmek istiyor musunuz?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Hayır", "Evet"],
    })
     .then((willDelete) => {
        if (willDelete) {
          location.href = "{site_url('settings/spsil')}/" + id;
      }
  });
 }  
</script>
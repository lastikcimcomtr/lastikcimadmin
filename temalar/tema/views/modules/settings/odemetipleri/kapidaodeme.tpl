{if $status && $message}
<div class="note note-{$status}">
	<p> {$message} </p>
</div>
{/if}
<form class="form-horizontal form-row-seperated" method="post" action="">
	<div class="form-body">

		{foreach $ayarlar as $ayar}
		<div class="form-group">
			<label class="col-md-3 control-label">{$ayar->title} : </label>
			<input type="hidden" name="parametreler[]" value="{$ayar->name}" />
			{if $ayar->input == "textarea"}
			<div class="col-md-5">
				<textarea class="form-control" name="{$ayar->name}" rows="10">{$ayar->value}</textarea>
			</div>
			{elseif $ayar->input == "text"}
			<div class="col-md-5">
				<input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
			</div>
			{elseif $ayar->input == "switch"}
			<div class="col-md-1">
				<input type="checkbox" {if $ayar->value == 1}checked{/if} class="make-switch" data-size="small" data-on-color="success" data-on-text='<i class="fa fa-check"></i> Açık' data-off-color="danger" data-off-text='<i class="fa fa-times"></i> Kapalı' name="{$ayar->name}" value="1" />
			</div>
			{else}
			<div class="col-md-5">
				<input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
			</div>
			{/if}
			{if $ayar->description}
			<div class="modal fade bs-modal-md" id="{$ayar->name}" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">{$ayar->title}</h4>
						</div>
						<div class="modal-body" style="line-height: 25px;"> {$ayar->description} </div>
						<div class="modal-footer">
							<button type="button" class="btn btn-sm blue" data-dismiss="modal">Kapat</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<span class="col-md-3"> 
				<a data-toggle="modal" href="#{$ayar->name}" class="btn btn-circle btn-sm blue"><i class="fa fa-question"></i></a>
			</span>
			{/if}
		</div>
		{/foreach}
	</div>
	<div class="form-body">
		<div class="form-group">
			<label class="col-md-3 control-label"></label>
			<div class="col-md-9">
				<button type="submit" class="btn blue">Kaydet</button>
			</div>
		</div>
	</div>
</form>
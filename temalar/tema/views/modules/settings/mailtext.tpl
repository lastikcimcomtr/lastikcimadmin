{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <div class="breadcrumbs">
            <h1>Mail Metin Ayarları</h1>    
        </div>
        <!-- END BREADCRUMBS --> 
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12"> 
                <div class="portlet">
                    <div class="portlet-title" style="min-height: 0px;" >
                        <div class="caption" id="uye_bayi_kayit_sonuc" style="padding:0;margin:10px 0px;"></div> 
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered"> 
                            <ul class="nav nav-tabs" id="tab_basliklari"> 
                                {foreach from=$siparisDurumlari item=durumadi name="siparisdurumu"} 
                                    <li {if $updTab == $durumadi->id}class="active"{/if}>
                                        <a href="#siparisdurumu_{$durumadi->id}" data-toggle="tab">{$durumadi->durum}</a>
                                    </li>  
                                {/foreach}
                            </ul>
                            <div class="tab-content" style="padding: 20px;"> 
                                {foreach from=$siparisDurumlari item=durumadi name="siparisdurumu"} 
                                    {$mailDatalarim = ""}
                                    <div class="tab-pane{if $updTab == $durumadi->id} active{/if}" id="siparisdurumu_{$durumadi->id}">
                                        <div class="form-body">
                                            {$mailDatalarim = $this->settings->getSiparisData($durumadi->id)->value|json_decode}
                                            <form class="form-horizontal form-row-seperated" id="uye_bayi_detayli_duzenleme" method="post" action="">
                                                <input type="hidden" name="durumID" value="{$durumadi->id}">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Mail Başlığı 1</label>
                                                    <div class="col-md-5">
                                                        <input type="text" autocomplete="off" class="form-control" name="mail_basligi1" value="{$mailDatalarim->mail_basligi1}" placeholder="Mail Tasarımı Başlığı Giriniz">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Mail Başlığı 2</label>
                                                    <div class="col-md-5">
                                                        <input type="text" autocomplete="off" class="form-control" name="mail_basligi2" value="{$mailDatalarim->mail_basligi2}" placeholder="Mail Tasarımı Başlığı Giriniz">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Mail Açıklaması</label>
                                                    <div class="col-md-8 col-sm-12">
                                                        <textarea class="ckeditor form-control" name="mail_aciklamasi" required="required" rows="6">{$mailDatalarim->mail_aciklamasi}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"></label>
                                                    <div class="col-md-9">
                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>  
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
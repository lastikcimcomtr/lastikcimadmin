{include file="base/header.tpl"}
<div class="container-fluid">
	<div class="page-content">

		<div class="breadcrumbs">
			<h1>Ödeme Ayarları </h1>    
		</div>
		<!-- END BREADCRUMBS --> 
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<form class="form-horizontal form-row-seperated" id="uye_bayi_detayli_duzenleme" method="post" action="">
					<div class="portlet">
						<div class="portlet-title" style="min-height: 0px;" >
							<div class="caption" id="uye_bayi_kayit_sonuc" style="padding:0;margin:10px 0px;">  </div>
                    <!-- <div class="actions btn-set">  
                        <button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Kaydet</button>  
                    </div> -->
                </div>
                <div class="portlet-body">
                	<div class="tabbable-bordered"> 
                		<ul class="nav nav-tabs" id="tab_basliklari"> 
                			{foreach from=$odemetipleri item=odemetipi name="odemetip"}
                            {if $odemetipi->viewfile}
                            <li {if $smarty.foreach.odemetip.first}class="active"{/if}><a href="#odemetipi_{$odemetipi->id}" data-toggle="tab">{$odemetipi->odeme_tipi}</a></li> 
                            {/if}
                            {/foreach}
                        </ul>
                        <div class="tab-content">                              
                           {foreach from=$odemetipleri item=odemetipi name="odemetip2"}
                           {if $odemetipi->viewfile}
                           <div class="tab-pane{if $smarty.foreach.odemetip2.first} active{/if}" id="odemetipi_{$odemetipi->id}">
                            {include file="odemetipleri/"|cat:$odemetipi->viewfile|cat:".tpl"}
                        </div>  
                        {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div> 
</div>
</div>

{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>      
		                    	<th>
		                    		<a href="{site_url('faqs/add')}"><i class="fa fa-plus"></i> Soru Ekle</a>
		                    	</th>                   
		                        <th class="islem_bilgilendirme" style="text-align: right;">
		                            
		                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Durum Değiştir">
		                        		<i class=""></i>
		                    		</span>
		                            <span id="islembilgisitext"> Aktif / Pasif </span>

		                            <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </span> 
		                            <span id="islembilgisitext"> Düzenle  </span>

		                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </span>
		                            <span id="islembilgisitext"> Sil </span>

		                        </th> 
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="1%"> ID </th>
		                        <th> Soru Başlık </th> 
		                        <th width="5%"> Soru Sıra </th> 
		                        <th width="10%"> Soru Kategori </th> 
		                        <th width="1%"> 
		                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Soru Durum Değiştir">
		                        		<i class=""></i>
		                    		</a>  
		                		</th> 
								<th width="1%"> 
									<a href="#" title="Soru Düzenle" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>                                              	
								</th>
		                        <th width="1%">
		                        	<a href="#" title="Soru Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($sorular) > 0}
			                	{foreach $sorular as $soru}
			                    <tr> 
			                        <td> {$soru->id} </td>
			                        <td> {$soru->baslik} </td>                        
			                        <td> {$soru->sira} </td> 
			                        <td> {$soru->kategori_adi} </td> 
									
									<td id="durum_guncelle"> 
										<a onclick="durum_degis({$soru->id}, 'sss_sorular', '#sorudurumu{$soru->id}');" id="sorudurumu{$soru->id}" class="btn btn-circle btn-icon-only btn-default {if $soru->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif"><i class=""></i></a> 
									</td>
									<td> 
										<a href="{site_url('faqs/edit')}/{$soru->id}" title="Soru Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
			                        <td>  
			                            <a onclick="return confirm('Soruyu silmek istediğinize emin misiniz?');" href="{site_url('faqs/remove')}/{$soru->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="7"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
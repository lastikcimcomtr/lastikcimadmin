{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off" id="haberform">
				    <div class="form-body">
						<div class="form-group">
				            <label class="col-md-2 control-label">Banner Adı: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="name" placeholder="Banner Adı" value="{$banner->name}"/>                                                
				            </div>
				        </div> 
				        <div class="form-group">
				            <label class="col-md-2 control-label">Banner Yeri : <span class="required"> * </span></label>
				            <div class="col-md-4">
							<select name="base_id" class="form-control">
								{foreach $bannerBase as $banner_base}
									<option value="{$banner_base->id}" {if $banner->base_id==$banner_base->id}selected{/if}>{$banner_base->banner_name}</option>
								{/foreach}
								</select>
				            </div>
				        </div>        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Banner Tipi : <span class="required"> * </span></label>
				            <div class="col-md-4">
							<select name="type" class="form-control">
								<option value="GEN" {if $banner->type=='GEN'}selected{/if}>GENEL</option>
								<option value="MDL" {if $banner->type=='MDL'}selected{/if}>Kategori Marka</option>
								<option value="DSN" {if $banner->type=='DSN'}selected{/if}>Desen</option>
								<option value="ETK" {if $banner->type=='ETK'}selected{/if}>Etiket</option>
							</select>
				            </div>
				        </div>        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Parametre 1: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="param_1" placeholder="Kategori,Desen veya Etiket ID" value="{$banner->param_1}" />                                                
				            </div>
				        </div>        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Parametre 2: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="param_2" placeholder="Marka Id" value="{$banner->param_2}" />                                                
				            </div>
				        </div>   
				        <div class="form-group">
				            <label class="col-md-2 control-label">Link: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="href" placeholder="Link" value="{$banner->href}" />                                                
				            </div>
				        </div>   
				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	{if $banner->src}
				        	<div class="col-md-1">
				        		<a href="{$banner->src}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="/uploads/banner/{$banner->src}">
                                </a>
				        	</div>
				        	{/if}
				        	<div class="col-md-4">
								<input type="file" name="src"/>
				        	</div>
				        </div>						
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>
				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
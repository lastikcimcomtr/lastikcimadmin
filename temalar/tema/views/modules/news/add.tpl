{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off"  id="haberform">

				    <div class="form-body">
				         
				        <div class="form-group">
				            <label class="col-md-2 control-label">Başlık : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="baslik" placeholder="Haber Başlığı Giriniz" required="required" value="" />                                                
				            </div>
				            <div class="col-md-2">
				            	<input id='anasayfahidden' type='hidden' value='0' name='ana_sayfa'>
				            	 <input id='anasayfa'  type="checkbox" class="form-control required_chk" name="ana_sayfa" value="1"/> Ana sayfada göster
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Url : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="sef_url" placeholder="" required="required" value="" />        
				            </div>
				            <div class="col-md-2">
				             <button type="button" onclick="sefurl('baslik')">Başlıktan otomatik doldur</button>  
				             </div>                                     
				        </div>
						
						<div class="form-group">
				            <label class="col-md-2 control-label">Açıklama : <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <textarea name="aciklama" class="form-control" required="required" placeholder="Haber Kısa Açıklaması"></textarea>
				            </div>
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	<div class="col-md-4">
								<input type="file" name="haber_resim" required="required" />
				        	</div>
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim İç : </label>
				        	<div class="col-md-4">
								<input type="file" name="haber_resim_ic"  />
				        	</div>
				        </div>
						
				        <div class="form-group">
							<label class="col-md-2 control-label">Detay : <span class="required"> * </span></label>
							<div class="col-md-6">
								<textarea class="ckeditor form-control" name="detay" required="required" rows="6"></textarea>                                                
							</div>
						</div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Ana Kategori : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="kategori" class="form-control form-filter input-sm required_chk" required="required">
				                	{foreach from=$this->news->kategoriler() item=kategori}
										{if $kategori->id<=15}
				                		<option value="{$kategori->id}">{$kategori->adi}</option>
										{/if}
				                	{/foreach}
				                </select>
				            </div>
				        </div>
						
						
				        <div class="form-group">
				            <label class="col-md-2 control-label">Alt Kategoriler : </label>
				            <div class="col-md-2">
				                <select style="height:128px;" name="alt_cats[]" class="form-control form-filter input-sm select2_altcat" multiple>
				                	{foreach from=$this->news->kategoriler() item=kategori}
				                		<option value="{$kategori->id}">{$kategori->adi}</option>
				                	{/foreach}
				                </select>
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sıra : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="number" class="form-control required_chk" name="sira" placeholder="Haber Sırasını Giriniz" min="1" required="required" value="1" />                                                
				            </div>
				        </div>

   				        <div class="form-group">
				            <label class="col-md-2 control-label">Extra Not : </label>
				            <div class="col-md-2">
				                <input type="text" class="form-control" name="extra_notes" placeholder="Extra Not(Tarih Yerine Gösterilir)" min="1" value="" />
				            </div>
				        </div>
				     
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue" onclick="savemce()">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
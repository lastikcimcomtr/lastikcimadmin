{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off">

				    <div class="form-body">
				        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Başlık : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="baslik" placeholder="Ürün Başlığı Giriniz" required="required" value="" />                                                
				            </div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Açıklama : <span class="required"> * </span></label>
							<div class="col-md-6">
								<textarea class="ckeditor form-control" name="aciklama" required="required" rows="6"></textarea>                                                
							</div>
						</div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Url : <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="url" placeholder="" required="required" value="" />        
				            </div>                                    
				        </div>
						

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	<div class="col-md-4">
								<input type="file" name="urun_resim" required="required" />
				        	</div>
				        </div>


				        <div class="form-group">
				            <label class="col-md-2 control-label">Sıra : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="number" class="form-control required_chk" name="sira" placeholder="Haber Sırasını Giriniz" min="1" required="required" value="1" />                                                
				            </div>
				        </div>

				     
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>
		                    	<th>
		                    		<a href="{site_url('news/addopportunity')}">
                                        <i class="fa fa-plus" style="margin-right: 5px;"></i> Yeni Ürün Ekle
                                    </a> 
		                    	</th>                    
		                        <th class="islem_bilgilendirme" style="text-align: right;">
		                            
		                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Durum Değiştir">
		                        		<i class=""></i>
		                    		</span>
		                            <span id="islembilgisitext"> Aktif / Pasif </span>

		                            <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </span> 
		                            <span id="islembilgisitext"> Düzenle  </span>

		                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </span>
		                            <span id="islembilgisitext"> Sil </span>

		                        </th> 
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="1%"> ID </th>
		                        <th> Başlık </th> 
		                        <th width="5%"> Sıra </th> 
		                        <th width="10%"> Url </th> 
		                        <th width="1%"> 
		                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Ürün Durum Değiştir">
		                        		<i class=""></i>
		                    		</a>  
		                		</th> 
								<th width="1%"> 
									<a href="#" title="Ürün Düzenle" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>                                              	
								</th>
		                        <th width="1%">
		                        	<a href="#" title="Ürün Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($urunler) > 0}
			                	{foreach $urunler as $urun}
			                    <tr> 
			                        <td> {$urun->id} </td>
			                        <td> {$urun->baslik} </td>                        
			                        <td> {$urun->sira} </td> 
			                        <td> {$urun->url} </td> 
									
									<td id="durum_guncelle"> 
										<a onclick="durum_degis({$urun->id}, 'firsaturunleri', '#urundurumu{$urun->id}');" id="urundurumu{$urun->id}" class="btn btn-circle btn-icon-only btn-default {if $urun->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif"><i class=""></i></a> 
									</td>
									<td> 
										<a href="{site_url('news/editopportunity')}/{$urun->id}" title="Haber Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
			                        <td>  
			                            <a onclick='swal({
        title: "Emin misin?",
        text: "Silmek istiyor musunuz?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Hayır", "Evet"],
    })
     .then((willDelete) => {
      if (willDelete) {
          location.href = "{site_url("news/removeopportunity")}/{$urun->id}";
      }
  });' href="#" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="7"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
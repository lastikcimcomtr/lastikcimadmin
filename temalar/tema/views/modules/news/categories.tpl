{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>
		                    	<th>
		                    		<a href="{site_url('news/add_category')}">
                                        <i class="fa fa-plus" style="margin-right: 5px;"></i> Yeni Haber Kategorisi Ekle
                                    </a> 
		                    	</th>            
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="2%"> ID </th>
		                        <th width="15%"> Kategori Adı </th> 
		                        <th width="15%"> Kategori Slug </th> 
		                        <th width="2%"> Üst Kategori ID </th> 
		                        <th width="15%"> Üst Kategori Adı </th> 
								<th width="2%"> 
									<a href="#" title="Kategori Düzenle" class="btn btn-sm btn-default kategoriduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>                                              	
								</th>
		                        <th width="2%">
		                        	<a href="#" title="Kategori Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($categories) > 0}
			                	{foreach $categories as $category}
			                    <tr> 
			                        <td> {$category->id} </td>
			                        <td> {$category->adi} </td>           
			                        <td> {$category->sef_url} </td>                    
			                        <td> {if $category->ust_id}{$category->ust_id}{else}-{/if} </td> 
			                        <td> {if $category->ust_id}{$this->news->categories(['id'=>$category->ust_id],true)->adi}{else}-{/if}</td> 
									<td> 
										<a href="{site_url('news/cat_edit')}/{$category->id}" title="Haber Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
			                        <td>  
			                            <a onclick="return confirm('Kategoriyi silmek istediğinize emin misiniz?');" href="{site_url('news/category_remove')}/{$category->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="11"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
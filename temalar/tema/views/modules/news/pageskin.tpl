{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off">
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label">Url : <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="url" placeholder="" required="required" value="{$pageSkins->url}" />        
				            </div>                                    
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	{if $pageSkins->img}
				        	<div class="col-md-1">
				        		<a href="{$pageSkins->img}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="{$pageSkins->img}">
                                </a>
				        	</div>
				        	{/if}
				        	<div class="col-md-4">
								<input type="file" name="urun_resim"/>
				        	</div>
				        </div>
		                <input type="hidden" class="form-control" name="img" placeholder="" value="{$pageSkins->img}" />        
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
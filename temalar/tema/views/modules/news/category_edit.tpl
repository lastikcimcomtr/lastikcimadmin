{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off" id="haberform">
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label">Kategori Adı : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="adi" placeholder="Kategori Adı Giriniz" required="required" value="{$kategori->adi}" />                                                
				            </div>
				        </div>        
						<div class="form-group">
				            <label class="col-md-2 control-label">Kategori Url : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="sef_url" placeholder="Kategori Url Giriniz" required="required" value="{$kategori->sef_url}" />                                                
				            </div>
				        </div>
						<div class="form-group">
				            <label class="col-md-2 control-label">Üst Kategori : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="ust_id" class="form-control form-filter input-sm required_chk" required="required">
				                	{foreach from=$this->news->kategoriler() item=kategori_s}
				                		<option value="{$kategori_s->id}" {if $kategori->ust_id == $kategori_s->id}selected{/if}>{$kategori_s->adi}</option>
				                	{/foreach}
				                </select>
				            </div>
				        </div>
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>
				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
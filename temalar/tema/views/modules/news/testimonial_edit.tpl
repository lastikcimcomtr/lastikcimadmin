{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off" id="haberform">
				    <div class="form-body">
						<div class="form-group">
				            <label class="col-md-2 control-label">Testimonial Başlığı: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="title" placeholder="Testimonial Başlığı" value="{$testimonial->title}"/>                                                
				            </div>
				        </div> 
						<div class="form-group">
				            <label class="col-md-2 control-label">Mesaj: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="message" placeholder="Testimonial Başlığı" value="{$testimonial->message}"/>                                                
				            </div>
				        </div> 
						<div class="form-group">
				            <label class="col-md-2 control-label">Puan: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="rate" placeholder="Testimonial Başlığı" value="{$testimonial->rate}"/>                                                
				            </div>
				        </div> 
						<div class="form-group">
				            <label class="col-md-2 control-label">Kullanıcı Adı: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="user_name" placeholder="Testimonial Başlığı" value="{$testimonial->user_name}"/>                                                
				            </div>
				        </div> 	
						<div class="form-group">
				            <label class="col-md-2 control-label">Kullanıcı Başlığı: </label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="user_title" placeholder="Testimonial Başlığı" value="{$testimonial->user_title}"/>                                                
				            </div>
				        </div> 				        			        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Durum : <span class="required"> * </span></label>
				            <div class="col-md-4">
							<select name="active" class="form-control">
									<option value="1" {if $testimonial->active=='1'}selected{/if}>Aktif</option>
									<option value="0" {if $testimonial->active=='0'}selected{/if}>Pasif</option>
								</select>
				            </div>
				        </div>        				
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>
				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="2%"> ID </th>
		                        <th width="5%"> Başlık </th> 
		                        <th width="5%"> Mesaj </th> 
		                        <th width="5%"> Puan </th> 
		                        <th width="3%"> Kullanıcı Adı </th> 
		                        <th width="3%"> Kullanıcı Başlığı </th> 
								<th width="2%"> 
				           			<a title="Durum" class="btn btn-circle btn-icon-only btn-default beyaz">
		                            </a>                                    	
								</th>
		                        <th width="2%">
	                        		<a href="#" title="Banner Düzenle" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>  
		                        	<a href="#" title="Banner Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($testimonials) > 0}
			                	{foreach $testimonials as $testimonial}
			                    <tr> 
			                        <td> {$testimonial->id} </td>
			                        <td> {$testimonial->title} </td>
			                        <td title="{$testimonial->message}"> {$testimonial->message|truncate:126} </td>           
			                        <td> {$testimonial->rate} </td>      
			                        <td> {$testimonial->user_name} </td>    
			                        <td> {$testimonial->user_title} </td>                         
									<td> 
										<a onclick="toggle_testimonial({$testimonial->id},this)" title="Durum" class="btn btn-circle btn-icon-only btn-default {if $testimonial->active == 1}durumu{else}beyaz{/if}">
			                            </a>                                       	
									</td>
			                        <td>    
										<a href="{site_url('news/testimonial_edit')}/{$testimonial->id}" title="Banner Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>   
			                            <a onclick="return confirm('Testimonial\'ı silmek istediğinize emin misiniz?');" href="{site_url('news/testimonial_remove')}/{$testimonial->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="11"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>
		                    	<th>
		                    		<a href="{site_url('news/banner_add')}">
                                        <i class="fa fa-plus" style="margin-right: 5px;"></i> Yeni Banner Ekle
                                    </a> 
		                    	</th>            
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="2%"> ID </th>
		                        <th width="5%"> Banner Adı </th> 
		                        <th width="5%"> Banner Tipi </th> 
		                        <th width="5%"> Banner Yeri </th> 
		                        <th width="3%"> Parametre 1 </th> 
		                        <th width="3%"> Parametre 2 </th> 
		                        <th width="5%"> Resim Yolu </th> 
		                        <th width="5%"> Link </th> 
		                        <th width="5%"> Boyut </th> 
								<th width="2%"> 
									<a href="#" title="Banner Düzenle" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>                                              	
								</th>
		                        <th width="2%">
		                        	<a href="#" title="Banner Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($bannerDeploy) > 0}
			                	{foreach $bannerDeploy as $banner}
			                    <tr> 
			                        <td> {$banner->id} </td>
			                        <td> {$banner->name} </td>
			                        <td> {$banner->type} </td>           
			                        <td> {$banner->banner_name} </td>      
			                        <td> {$banner->param_1} </td>    
			                        <td> {$banner->param_2} </td>          
			                        <td> {$banner->src} </td>                   
			                        <td> {$banner->href} </td>                
			                        <td> {$banner->width}x{$banner->height} </td>                
									<td> 
										<a href="{site_url('news/banner_edit')}/{$banner->id}" title="Banner Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
			                        <td>  
			                            <a onclick="return confirm('Banner\'ı silmek istediğinize emin misiniz?');" href="{site_url('news/banner_remove')}/{$banner->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="11"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
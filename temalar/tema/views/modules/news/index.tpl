{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>
		                    	<th>
		                    		<a href="{site_url('news/add')}">
                                        <i class="fa fa-plus" style="margin-right: 5px;"></i> Yeni Haber Ekle
                                    </a> 
		                    	</th>                    
		                        <th class="islem_bilgilendirme" style="text-align: right;">
		                            
									<span class="btn btn-circle btn-icon-only btn-default anasayfavitrini">
										<i class=""></i>
									</span>
									<span id="islembilgisitext">Ana Sayfada Göster</span>

		                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Durum Değiştir">
		                        		<i class=""></i>
		                    		</span>
		                            <span id="islembilgisitext"> Aktif / Pasif </span>

		                            <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </span> 
		                            <span id="islembilgisitext"> Düzenle  </span>

		                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </span>
		                            <span id="islembilgisitext"> Sil </span>

		                        </th> 
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="1%"> ID </th>
		                        <th> Haber Başlık </th> 
		                        <th width="5%"> Haber Sıra </th> 
		                        <th width="10%"> Haber Kategori ID</th> 
		                        <th width="5%"> Güncellenme </th> 
		                        <th width="5%"> Eklenme </th> 
		                         <th width="1%"> 
		                        	<a class="btn btn-circle btn-icon-only btn-default anasayfavitrini" title="Ana Sayfada Göster">
		                        		<i class=""></i>
		                    		</a>  
		                		</th> 
		                        <th width="1%"> 
		                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Haber Durum Değiştir">
		                        		<i class=""></i>
		                    		</a>  
		                		</th> 
								<th width="1%"> 
									                                            	
								</th>
								<th width="1%"> Etiketler </th>
		                        <th width="1%">
		                        	<a href="#" title="Haber Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($haberler) > 0}
			                	{foreach $haberler as $haber}
			                    <tr> 
			                        <td> {$haber->id} </td>
			                        <td> {$haber->baslik} </td>                        
			                        <td> {$haber->sira} </td> 
			                        <td> {$haber->kategori}</td> 
			                        <td> {if $haber->guncellenme}{date('d.m.Y', $haber->guncellenme)}{else}-{/if} </td> 
			                        <td> {date('d.m.Y', $haber->eklenme)} </td> 
									
									<td> 
										<a onclick="birsifir({$haber->id}, 'haberler','ana_sayfa', '#anasayfavitrini{$haber->id}' ,'anasayfavitrini');" id="anasayfavitrini{$haber->id}" class="btn btn-circle btn-icon-only btn-default {if $haber->ana_sayfa == 1}anasayfavitrini{else}beyaz{/if}" title="Aktif / Pasif"><i class=""></i></a> 
									</td>
									<td id="durum_guncelle"> 
										<a onclick="durum_degis({$haber->id}, 'haberler', '#haberdurumu{$haber->id}');" id="haberdurumu{$haber->id}" class="btn btn-circle btn-icon-only btn-default {if $haber->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif"><i class=""></i></a> 
									</td>
									<td> 
										<a href="{site_url('news/edit')}/{$haber->id}" title="Haber Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
									
			                        <td> 
									<a class="btn btn-sm btn-default hizliduzenle" title="Etiketler" id="etiketler_btn_{$haber->id}"
                                               data-toggle="modal" onclick="init_select2(this);" href="#etiketler_{$haber->id}">
                                                <i class="fa fa-tags"></i>
                                            </a>
											 <div class="modal fade" id="etiketler_{$haber->id}" role="basic" aria-hidden="true">
                                                <form method="post" action="javascript:void(0);" onsubmit="save_haber_etiketler({$haber->id},this)"
                                                      id="etiketler_{$haber->id}" autocomplete="off">
                                                    <input type="hidden" name="haber_id" value="{$haber->id}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Etiketler</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                    <div class="form-body">
                                                                        <div id="form_cevap_{$haber->id}"></div>
                                                                        <div class="form-group"
                                                                             style="display: inline-block;width: 100%">
                                                                            <label class="col-md-4 control-label font-lg"
                                                                                   style="margin-top:5px">
                                                                                Etiket Adı
                                                                            </label>
                                                                            <div id="kategorilerimiz" class="col-md-7">
                                                                                <select name="etiket_adi"
                                                                                        class="form-control etiket_select2">
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-1"
                                                                                 style="padding-left: 0">
                                                                                <button type="button" onclick="etiket_ekle(this)" class="btn btn-md btn-primary">
                                                                                    Ekle
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list list-unstyled font-lg">
                                                                            {foreach $this->news->haber_etiketler($haber->id) as $etiket}
                                                                                <li class="item"
                                                                                    style="border-bottom:1px dimgrey solid">
                                                                                    {$etiket->adi}
                                                                                    <button onclick="$(this).closest('li').remove();" style="margin-top:3px"
                                                                                            class="btn btn-danger btn-xs pull-right">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </button>
                                                                                    <input type="hidden"  name="etiket_id[]" value="{$etiket->et_id}">
                                                                                </li>
                                                                            {/foreach}
                                                                        </ul>
                                                                    </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="modal-footer"
                                                                 style="border-top: 0px;padding-top: 0px;">
                                                                <button type="button" class="btn dark btn-outline"
                                                                        data-dismiss="modal">Kapat
                                                                </button>
                                                                <button type="button" onclick="save_haber_etiketler(this)" class="btn blue"
                                                                        style="background-color: #f5861d;border-color: #f5861d;"
                                                                        data-dismiss="modal">Kaydet
                                                                </button>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                </form>
                                                <!-- /.modal-dialog -->
                                            </div>
									</td>
			                        <td>  
			                            <a onclick="return confirm('Haberi silmek istediğinize emin misiniz?');" href="{site_url('news/remove')}/{$haber->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="11"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
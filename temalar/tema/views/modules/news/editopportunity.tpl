{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off">

				    <div class="form-body">
				        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Başlık : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="baslik" placeholder="Ürün Başlığı Giriniz" required="required" value="{$urun->baslik}" />                                                
				            </div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Açıklama : <span class="required"> * </span></label>
							<div class="col-md-6">
								<textarea class="ckeditor form-control" name="aciklama" required="required" rows="6">{$urun->aciklama}</textarea>                                                
							</div>
						</div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Url : <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="url" placeholder="" required="required" value="{$urun->url}" />        
				            </div>                                     
				        </div>						

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	{if $urun->resim}
				        	<div class="col-md-1">
				        		<a href="{$this->extraservices->fullresimurl('haber_resim', {$urun->resim})}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="{$this->extraservices->fullresimurl('haber_resim', {$urun->resim})}">
                                </a>
				        	</div>
				        	{/if}
				        	<div class="col-md-4">
								<input type="file" name="urun_resim" />
				        	</div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sıra : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="number" class="form-control required_chk" name="sira" placeholder="Ürün Sırasını Giriniz" min="1" required="required" value="{$urun->sira}" />                                                
				            </div>
				        </div>

				     
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
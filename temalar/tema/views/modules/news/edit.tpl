{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off" id="haberform">

				    <div class="form-body">
				        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Başlık : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="baslik" placeholder="Haber Başlığı Giriniz" required="required" value="{$haber->baslik}" />                                                
				            </div>
				             <div class="col-md-2">
				             	<input id='anasayfahidden' type='hidden' value='0' name='ana_sayfa'>
				            	 <input id='anasayfa' type="checkbox" class="form-control required_chk" name="ana_sayfa" value="1" {if $haber->ana_sayfa}checked{/if}/> Ana sayfada göster
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Url : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="sef_url" placeholder="" required="required" value="{$haber->sef_url}" />        
				            </div>
				            <div class="col-md-2">
				             <button type="button" onclick="sefurl('baslik')">Başlıktan otomatik doldur</button>  
				             </div>                                     
				        </div>
						
						<div class="form-group">
				            <label class="col-md-2 control-label">Açıklama : <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <textarea name="aciklama" class="form-control" required="required" placeholder="Haber Kısa Açıklaması">{$haber->aciklama}</textarea>
				            </div>
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim : <span class="required"> * </span></label>
				        	{if $haber->resim}
				        	<div class="col-md-1">
				        		<a href="{$this->extraservices->fullresimurl('haber_resim', {$haber->resim})}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="{$this->extraservices->fullresimurl('haber_resim', {$haber->resim})}">
                                </a>
				        	</div>
				        	{/if}
				        	<div class="col-md-4">
								<input type="file" name="haber_resim" />
				        	</div>
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Resim İç : </label>
				        	{if $haber->resim_ic}
				        	<div class="col-md-1 resim_ic_img">
				        		<a href="{$this->extraservices->fullresimurl('haber_resim', {$haber->resim_ic})}" rel="popupacil" target="_blank">
                                    <img rel="popupacil" border="0" width="100%" src="{$this->extraservices->fullresimurl('haber_resim', {$haber->resim_ic})}">
                                </a>
				        	</div>
				        	{/if}
				        	<div class="col-md-4">
								<input type="file" name="haber_resim_ic" value="" />
								<input  type="test" name="haber_ic_resim_sil" value="false"  style="display:none;"/>
				        	</div>
								<i  class="fa fa-times text-danger" onclick="$(this).closest('div').find('input').first().val(null);$(this).closest('div').find('input').last().val('true');$(this).closest('div').find('.resim_ic_img').remove()"></i>
				        </div>
						
				        <div class="form-group">
							<label class="col-md-2 control-label">Detay : <span class="required"> * </span></label>
							<div class="col-md-6">
								<textarea class="ckeditor form-control" name="detay" required="required" rows="6">{$haber->detay}</textarea>                                                
							</div>
						</div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Kategori : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="kategori" class="form-control form-filter input-sm required_chk" required="required">
				                		{foreach from=$this->news->kategoriler() item=kategori}
										{if $kategori->id<=15}
				                		<option value="{$kategori->id}"  {if $haber->kategori == $kategori->id}selected{/if}>{$kategori->adi}</option>
										{/if}
				                	{/foreach}
				                </select>
				            </div>
				        </div>

						<div class="form-group">
						{$haber_kategoriler = $this->news->haber_kategoriler($haber->id)}
				            <label class="col-md-2 control-label">Alt Kategoriler : </label>
				            <div class="col-md-2">
				                <select style="height:128px;" name="alt_cats[]" class="form-control form-filter input-sm select2_altcat" multiple>
				                	{foreach from=$this->news->kategoriler() item=kategori}
				                		<option value="{$kategori->id}" {if in_array($kategori->id,$haber_kategoriler)}selected{/if}>{$kategori->adi}</option>
				                	{/foreach}
				                </select>
				            </div>
				        </div>
						
				        <div class="form-group">
				            <label class="col-md-2 control-label">Sıra : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="number" class="form-control required_chk" name="sira" placeholder="Haber Sırasını Giriniz" min="1" required="required" value="{$haber->sira}" />                                                
				            </div>
				        </div>
   				        <div class="form-group">
				            <label class="col-md-2 control-label">Extra Not : </label>
				            <div class="col-md-2">
				                <input type="text" class="form-control" name="extra_notes" placeholder="Extra Not(Tarih Yerine Gösterilir)" min="1" value="{$haber->extra_notes}" />
				            </div>
				        </div>

				     
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue" onclick="savemce()">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
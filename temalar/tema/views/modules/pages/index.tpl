{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12"> 
				<div class="table-responsive">
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr>                         
		                        <th class="islem_bilgilendirme" style="text-align: right;">
		                            
		                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Durum Değiştir">
		                        		<i class=""></i>
		                    		</span>
		                            <span id="islembilgisitext"> Göster / Gösterme </span>

		                            <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </span> 
		                            <span id="islembilgisitext"> Düzenle  </span>

		                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </span>
		                            <span id="islembilgisitext"> Sil </span>

		                        </th> 
		                    </tr>
		                </thead>
		            </table>
					<style> #duzenleme_formu tr{ line-height: 25px; } </style>
		            <table class="table table-striped table-hover table-bordered">
		                <thead>
		                    <tr> 
		                        <th width="1%"> ID </th>
		                        <th> Sayfa Adı </th> 
		                        <th> Sayfa URL </th> 
		                        <th width="5%"> Sayfa Sıra </th> 
		                        <th width="5%"> Sayfa Yeri </th> 
		                        <th width="5%"> Sayfa Türü </th>
		                        <th width="5%"> Güncellenme </th> 
		                        <th width="5%"> Eklenme </th> 
		                        <th width="1%"> 
		                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Sayfayı Göster veya Gösterme">
		                        		<i class=""></i>
		                    		</a>  
		                		</th> 
								<th width="1%"> 
									<a href="#" title="Sayfayı Düzenle" class="btn btn-sm btn-default hizliduzenle">
		                                <i class="fa fa-pencil-square-o"></i>
		                            </a>                                              	
								</th>
		                        <th width="1%">
		                        	<a href="#" title="Sil" class="btn btn-sm btn-default sil">
		                                <i class="fa fa-times"></i>
		                            </a> 
		                        </th> 
		                    </tr>
		                </thead>
		                <tbody>
		                	{if count($sayfalar) > 0}
			                	{foreach $sayfalar as $sayfa}
			                    <tr> 
			                        <td> {$sayfa->id} </td>
			                        <td> {$sayfa->sayfa_adi} </td>                        
			                        <td> - </td> 
			                        <td> {$sayfa->sayfa_sira} </td> 
			                        <td> {$sayfa->sayfa_turu_adi} </td> 
			                        <td> {$sayfa->sayfa_yeri_adi} </td> 
			                        <td> {if $sayfa->guncellenme_tarihi}{date('d.m.Y', $sayfa->guncellenme_tarihi)}{else}-{/if} </td> 
			                        <td> {date('d.m.Y', $sayfa->eklenme_tarihi)} </td> 
									
									<td id="durum_guncelle"> 
										<a onclick="durum_degis({$sayfa->id},'sayfalar', '#sayfadurumu{$sayfa->id}');" id="sayfadurumu{$sayfa->id}" class="btn btn-circle btn-icon-only btn-default {if $sayfa->durum == 1}durumu{else}beyaz{/if}" title="Göster / Gösterme"><i class=""></i></a> 
									</td>
									<td> 
										<a href="{site_url('pages/edit')}/{$sayfa->id}" title="Sayfayı Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                <i class="fa fa-pencil-square-o"></i>
			                            </a>                                              	
									</td>
			                        <td>  
			                            <a onclick="return confirm('Sayfayı silmek istediğinize emin misiniz?');" href="{site_url('pages/remove')}/{$sayfa->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                <i class="fa fa-times"></i>
			                            </a>                             
			                        </td>
			                    </tr> 
			                	{/foreach}
			                {else}
		                    <tr>
		                        <td colspan="11"><center>Kayıtlı İçerik Bulunamadı</center></td>
		                    </tr>
		                    {/if}
		                </tbody>
		            </table>
		        </div>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12"> 
		        <form class="form-horizontal form-row-seperated" method="post" action="" autocomplete="off" onclick="savemce()">

				    <div class="form-body">
				        
				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Adı: <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="sayfa_adi" placeholder="Sayfanın Adını Giriniz" required="required" value="" />                                                
				            </div>
				        </div>
						
				        <div class="form-group">
							<label class="col-md-2 control-label">Sayfa İçerik: <span class="required"> * </span></label>
							<div class="col-md-6">
								<textarea class="ckeditor form-control" name="sayfa_icerik" required="required" rows="6"></textarea>                                                
							</div>
						</div>

						<div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Başlık: <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="sayfa_title" placeholder="Sayfanın Başlığını Giriniz" required="required" value="" />                                                
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Açıklama: <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="sayfa_desc" placeholder="Sayfanın Açıklamasını Giriniz" required="required" value="" />                                                
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Kelimeleri: <span class="required"> * </span></label>
				            <div class="col-md-4">
				                <input type="text" class="form-control required_chk" name="sayfa_keyw" placeholder="Sayfanın ilgili Olduğu Kelimeleri Giriniz" required="required" value="" />                                                
				            </div>
				        </div>
						
				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Türü:<span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="sayfa_turu" class="form-control form-filter input-sm required_chk " required="required">
				                    {foreach $sayfaturleri as $sayfaturu}
				                    	<option value="{$sayfaturu->id}">{$sayfaturu->tip}</option>
				                    {/foreach}
				                </select>
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Yeri:<span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="sayfa_yeri" class="form-control form-filter input-sm required_chk " required="required">
				                    {foreach $sayfayerleri as $sayfayeri}
				                    	<option value="{$sayfayeri->id}">{$sayfayeri->yer}</option>
				                    {/foreach}
				                </select>
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Göster: <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <select style="height:34px;" name="durum" class="form-control form-filter input-sm required_chk" required="required">
				                    <option value="1">Göster</option>
				                    <option value="0">Gösterme</option>
				                </select>
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Sayfa Sıra: <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" name="sayfa_sira" placeholder="Sayfanın Sırasını Giriniz" required="required" value="1" />                                                
				            </div>
				        </div>

				     
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue" onclick="savemce()">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
{$excelicin['name'] = 'excel_dosya'}
{$exceldizin = $this->extraservices->ayarlar($excelicin, TRUE)->value}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}

        <div class="row">
            <div class="col-md-12">
                <form action="{site_url('importers/add_excel')}" method="post" enctype="multipart/form-data">
                    <table class="table table-striped table-hover table-bordered" >
                        <tr>
                            <td>Yeni Excel Gönder</td>
                            <td> <input type="file" name="dosya" accept=".xlsx, .xls, .csv"></td>
                            <td><select name="parametre">
                                <option> Parametreleri Şuradan Kopyala</option>
                                {foreach $excels as $excel}
                                <option value="{$excel->id}">{$excel->dosya}</option>
                                {/foreach}
                            </select></td>
                            <td> <input type="submit" value="Dosyayı Gönder" ></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>

        <div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
                    <thead>
                        <tr>
                            <th width="1%"> ID </th>
                            <th width="50%"> Dosya </th>
                            <th width="40%"> Zaman </th>
                            <th width="1%"> Urunleri Ekle </th>
                            <th width="1%"> Parametreler </th>
                            <th width="1%"> Sil </th>
                        </tr>
                    </thead>
                        <tbody>
                            {foreach $excels as $excel}
                            {$excelurl = $exceldizin|cat:$excel->dosya}
                            <tr>
                                <td> {$excel->id} </td>
	                            <td> <a href="{$excelurl}" target="_blank">{$excelurl}</a> </td>
	                            <td> {date('d.m.Y', $excel->zaman)} </td>
                                    <td> <center> <a href="#" onclick="urunekle({$excel->id})" title="Urun Ekle" class="btn btn-default hizliduzenle"> <i class="fa fa-bars"></i> </a> </center> </td>
	                            <td> <center> <a href="{site_url('importers/excel/'|cat:$excel->id)}" title="Parametreler" class="btn btn-default hizliduzenle"> <i class="fa fa-bars"></i> </a> </center> </td>
	                            <td> <center> <a href="{site_url('importers/remove/'|cat:$excel->id)}" onclick="return confirm('Silmek istediğinizden emin misiniz?');" title="Sil" class="btn btn-default sil"> <i class="fa fa-times"></i> </a> </center> </td>
                            </tr>
                            {/foreach}
                        </tbody>
                </table>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
	<div class="page-content">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">
			
					<div class="portlet" style="margin-top: -30px;">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="fa fa-shopping-cart"></i> Taban & Yanak & Jant Çapı Ekleme  -->
							</div>
							<div class="actions btn-set">  
							</div>
						</div>
						<div class="portlet-body">
							<form method="post" action="{site_url('importers/listekaydet')}">
								<input type="hidden" name="kategori" , value="{$kategori}">
								<div class="form-body">						 
									<div class="form-group">                                             
										<label class="col-md-2 control-label">  
											Ekleyeceğiniz Markayı Seçin
										</label> 
										<div class="col-md-3" id="kategorilerimiz">
											<select  name="marka"  class="form-control form-filter input-sm select2me">
												<option value="">Seçiniz</option>
												{foreach $markalar as $marka}
													<option value="{$marka->id}">{$marka->adi}</option>
												{/foreach}
											</select>												
										</div>
									</div> 							
								</div>
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-2 control-label"></label>
										<div class="col-md-10" id="kategorilerimiz">
											<button class="btn btn-success" type="submit"><i class="fa fa-check"></i>Seçili Ürünleri Ekle</button> 
										</div>
									</div>
								</div>
							<table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
								<thead>
									<tr>
										<th style="width:0.1%; line-height: auto;"> 
											<input type="checkbox" id="checkAll" class="icheck" style="display:none" />
										</th>
					
										{foreach $veriler[1] as $key => $anahtar}
										{$parametre = $parametreler[$key] }
														<th> <div class="col-md-12">{$parametre->adi}</div> </th>
										{/foreach}
						
										
									</tr>
								</thead>
								<tbody>

									{foreach from=$veriler item=veri key=i}
									<tr>
										<td> 
											<input type="checkbox" id="u_{$i}" class="icheck" name="urun[{$i}][secim]" checked="checked"  value ="{$i}"/>
										</td>
										{foreach from=$veri item=deger key=parametre}
										<td>
											<input type="text" value="{$deger}"  name="urun[{$i}][{$parametre}]"/>
										</td>
										{/foreach}
									</tr>
									{/foreach}
									<tr>
										<td colspan="{count($parametreler)+1}"><button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Seçili Ürünleri Ekle</button> </td>
									</tr>
								</tbody> 
							</table>
					</form>
						</div>
					</div>
			

			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
	</div>
	{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
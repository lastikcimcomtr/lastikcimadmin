{include file="base/header.tpl"}
{$xmlicin['name'] = 'xml_dosya'}
{$xmldizin = $this->extraservices->ayarlar($xmlicin, TRUE)->value}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
		    <p> {$message} </p>
		</div>
        {/if}
        <div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
                    <thead>
                        <tr>
                            <th width="1%"> ID </th>
                            <th width="50%"> Dosya </th>
                            <th width="40%"> Zaman </th>
                            <th width="1%"> Parametreler </th>
                            <th width="1%"> Sil </th>
                        </tr>
                    </thead>
                        <tbody>
                            {foreach $xmls as $xml}
                            {$xmlurl = $xmldizin|cat:$xml->dosya}
                            <tr>
                                <td> {$xml->id} </td>
	                            <td> <a href="{$xmlurl}" target="_blank">{$xmlurl}</a> </td>
	                            <td> {date('d.m.Y', $xml->zaman)} </td>
	                            <td> <center> <a href="{site_url('importers/xml/'|cat:$xml->id)}" title="Parametreler" class="btn btn-default hizliduzenle"> <i class="fa fa-bars"></i> </a> </center> </td>
	                            <td> <center> <a href="{site_url('importers/remove/'|cat:$xml->id)}" onclick="return confirm('Silmek istediğinizden emin misiniz?');" title="Sil" class="btn btn-default sil"> <i class="fa fa-times"></i> </a> </center> </td>
                            </tr>
                            {/foreach}
                        </tbody>
                </table>
			</div>
		</div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
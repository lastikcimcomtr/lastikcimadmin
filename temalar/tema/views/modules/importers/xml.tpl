{include file="base/header.tpl"}
{$dosyaparametreleri = json_decode($dosya->parametreler)}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        {if $status && $message}
        <div class="note note-{$status}">
            <p> {$message} </p>
        </div>
        {/if}
        <div class="row">
            <div class="col-md-12"> 
                <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="" autocomplete="off">

                    <div class="form-body">
                        {$ornekveri = $ornekveri}
                        {foreach $parametreler as $parametre}
                            {if $parametre->input_type <> "vitrin"}
                                {if $parametre->input_type == "fiyat"}
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{$parametre->adi}</label>
                                        <div class="col-md-3">
                                            <select name="{$parametre->silik_id}" data-placeholder="{$parametre->adi}" class="form-control form-filter input-sm select2me">
                                                <option value=""> Seçiniz </option>
                                                {foreach $anahtarlar as $anahtar}
                                                <option value="{$anahtar}" {if $dosyaparametreleri->{$parametre->silik_id} == $anahtar}selected{/if}> {$anahtar} ( {$ornekveri->{$anahtar} }) </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            {$birim_adi = $parametre->silik_id|cat:"_birim"}
                                            <select name="{$parametre->silik_id}_birim" data-placeholder="Birim" class="form-control form-filter input-sm select2me">
                                                <option value=""> Seçiniz </option>
                                                {foreach $kurlar as $kur}
                                                <option value="{$kur->kod}" {if $dosyaparametreleri->{$birim_adi} == $kur->kod}selected{/if}> {$kur->kod} </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                {elseif $parametre->input_type == "durum"}                                  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{$parametre->adi}</label>
                                        <div class="col-md-3">
                                            <select style="height:34px;" name="{$parametre->silik_id}" data-placeholder="{$parametre->adi}" class="form-control form-filter input-sm select2me">
                                                <option value=""> Seçiniz </option>
                                                <option value="1" {if $dosyaparametreleri->{$parametre->silik_id} == 1}selected{/if}> Aktif </option>
                                                <option value="0" {if $dosyaparametreleri->{$parametre->silik_id} == 0}selected{/if}> Pasif </option>
                                            </select>
                                        </div>
                                    </div>
                                {elseif $parametre->input_type == "yapisi"}
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{$parametre->adi}</label>
                                        <div class="col-md-3">
                                            <select name="{$parametre->silik_id}" data-placeholder="{$parametre->adi}" class="form-control form-filter input-sm select2me">
                                                <option value=""> Seçiniz </option>
                                                <option value="r" {if $dosyaparametreleri->{$parametre->silik_id} == "r"}selected{/if}> R </option>
                                                <option value="zr" {if $dosyaparametreleri->{$parametre->silik_id} == "zr"}selected{/if}> ZR </option>
                                                <option value="-" {if $dosyaparametreleri->{$parametre->silik_id} == "-"}selected{/if}> - </option>
                                            </select>
                                        </div>
                                    </div>
                                {else}
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">{$parametre->adi}</label>
                                        <div class="col-md-3">
                                            <select name="{$parametre->silik_id}" data-placeholder="{$parametre->adi}" class="form-control form-filter input-sm select2me">
                                                <option value=""> Seçiniz </option>
                                                {foreach $anahtarlar as $anahtar}
                                                <option value="{$anahtar}" {if $dosyaparametreleri->{$parametre->silik_id} == $anahtar}selected{/if}> {$anahtar} ( {$ornekveri->{$anahtar} }) </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                {/if}
                            {/if}
                        {/foreach}
                     
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <button type="submit" class="btn blue">Kaydet</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            {*
            <div class="col-md-12">
                <table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
                    <thead>
                        <tr>
                            <th style="width:0.1%; line-height: auto;"> 
                                <input type="checkbox" id="checkAll" class="icheck" />
                            </th>
                            {foreach $anahtarlar as $anahtar}
                            <th width="10%"> <div class="col-md-12">{$anahtar}</div> </th>
                            {/foreach}
                        </tr>
                    </thead>
                        <tbody>
                            {$i = 0}
                            {foreach $veriler as $urun}
                            {$urun = (object)$urun}
                            <tr>
                                <td style="line-height: 33px;"> 
                                    <input type="checkbox" name="urunler[{$i}][durum]" class="urun_id icheck" value="1" />
                                </td>
                                {foreach $anahtarlar as $anahtar}
                                <th width="10%"> <div class="col-md-12">{$urun->{$anahtar}}</div> </th>
                                {/foreach}
                            </tr>
                            {$i= $i+1}
                            {/foreach}
                            <tr>
                                <td colspan="{count($anahtarlar)+1}"><button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Kaydet</button>  </td>
                            </tr>
                        </tbody>
                </table>
            </div>
            *}
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
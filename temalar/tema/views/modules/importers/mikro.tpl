{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Ürün Ara 
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="expand" data-original-title="Aç / Kapat"> </a> 
                        </div>
                    </div>
                    <div class="portlet-body tabs-below">
                        <form class="form-horizontal form-row-seperated" method="post" action="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Adı</label>
                                    <div class="col-md-9" id="kategorilerimiz">                                                                                                                    
                                        <input type="text" class="form-control" name="sdata[sto_isim]" placeholder="" value="{$sdata['sto_isim']}" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kodu</label>
                                    <div class="col-md-9" id="kategorilerimiz">                                                                                                                    
                                        <input type="text" class="form-control" name="sdata[sto_kod]" placeholder="" value="{$sdata['sto_kod']}" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Kategori</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="kategori" name="sdata[sto_anagrup_kod]" data-placeholder="Kategoriler" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $kategoriler as $kategori}
                                                <option {if $sdata['sto_anagrup_kod'] == $kategori->kod}selected{/if} value="{$kategori->kod}">{$kategori->kategori}</option>
                                            {/foreach}
                                        </select>
                                        <div id="alt_kategori"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Marka</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="marka" name="sdata[sto_marka_kodu]" data-placeholder="Markalar" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $markalar as $marka}
                                                <option {if $sdata['sto_marka_kodu'] == $marka->kod}selected{/if} value="{$marka->kod}">{$marka->marka}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Desen</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="desen" name="sdata[lastikdeseni]" data-placeholder="Desenler" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $desenler as $desen}
                                                <option {if $sdata['lastikdeseni'] == $desen->lastikdeseni}selected{/if} value="{$desen->lastikdeseni}">{$desen->lastikdeseni}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Taban</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="taban" name="sdata[tabani]" data-placeholder="Tabanlar" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $tabanlar as $taban}
                                                <option {if $sdata['tabani'] == $taban->tabani}selected{/if} value="{$taban->tabani}">{$taban->tabani}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Yanak</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="yanak" name="sdata[yanagi]" data-placeholder="Yanaklar" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $yanaklar as $yanak}
                                                <option {if $sdata['yanagi'] == $yanak->yanagi}selected{/if} value="{$yanak->yanagi}">{$yanak->yanagi}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Jant</label>
                                    <div class="col-md-9" id="kategorilerimiz">
                                        <select id="jant" name="sdata[jantcapi]" data-placeholder="Jantlar" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $jantlar as $jant}
                                                <option {if $sdata['jantcapi'] == $jant->jantcapi}selected{/if} value="{$jant->jantcapi}">{$jant->jantcapi}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 col-md-offset-8">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-9" id="kategorilerimiz">         
                                        <button type="submit" class="btn blue btn-block">Ürün Ara</button> 
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        
                        <div class="tab-content" style="border:none; padding-top: 0px;">
                            <div class="tab-pane active" id="overview_1">
                                <div class="table-responsive">
                                    <div id="cokluurunguncellemecevap" style="padding: 20px 0px; display: none;"></div>
                                    <form action="javascript:void(-1);" autocomplete="off" method="POST" id="cokluurunguncelleme" onsubmit="coklurunformgonder()">
                                        <table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
                                            <thead>
                                                <tr>
                                                    <th style="width:0.1%; line-height: auto;"> 
                                                        <input type="checkbox" id="checkAll" class="icheck" />
                                                    </th>
                                                    <th width="10%"> <div class="col-md-12">Stok Kodu</div> </th>
                                                    <th width="20%"> <div class="col-md-12">Ürün Adı</div> </th>
                                                    <th width="10%"> <div class="col-md-12">Stok Adedi</div> </th>
                                                    <th width="10%"> <div class="col-md-12">Alış Fiyat</div> </th>
                                                </tr>
                                            </thead>
                                                <tbody>
                                                    {foreach $stoklar as $urun}
                                                    	{foreach $urun as $upkey => $upvalue}
                                                    		{if !is_numeric($upkey)}
                                                    		<input type="hidden" name="urunler[{$urun->sto_kod}][{$upkey}]" value="{$upvalue}" />
                                                    		{/if}
                                                    	{/foreach}
                                                    <tr>
                                                        <td style="line-height: 33px;"> 
                                                            <input type="checkbox" name="urunler[{$urun->sto_kod}][durum]" class="urun_id icheck" value="1" />
                                                        </td>
                                                        <td> 
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->sto_kod}][sto_kod]" value="{$urun->sto_kod}">
                                                            </div> 
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->sto_kod}][sto_isim]" value="{$urun->sto_isim}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->sto_kod}][miktar]" value="{$urun->miktar}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <div class="input-group input-group-sm">
                                                                    <span class="input-group-addon" id="sizing-addon1">{$urun->doviz}</span>
                                                                    <input type="text" class="form-control" name="urunler[{$urun->sto_kod}][fiyat]" value="{$urun->fiyat}" aria-describedby="sizing-addon1">
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    {/foreach}
                                                    <tr>
                                                        <td colspan="5"><button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Kaydet</button>  </td>
                                                    </tr>
                                                </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div> 
                        </div>

                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}

</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
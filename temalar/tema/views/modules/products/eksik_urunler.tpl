{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>

                                    <th width="1%"> Stok</th>
                                    <th width="1%"> Miktar</th>
                                    <th> Eski Miktar</th>
                                    <th> Fiyat</th>
                                    <th> Eski Fiyat</th>
                                    <th> Model</th>
                                    <th> Marka</th>
                                    <th> Dot</th>
                                    <th> Tatko</th>
                                    <th> Lastikcim</th>
                                    <th width="1%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach $urunler as $urun}
                                    <tr>
                                        <td>
                                            {$urun->stok}
                                        </td>
                                        <td> {$urun->miktar} </td>
                                        <td> {$urun->e_miktar}  </td>
                                        <td> {$urun->fiyat} </td>
                                        <td> {$urun->e_fiyat} </td>
                                        <td> {$urun->model} </td>
                                        <td> {$urun->marka} </td>
                                        <td> {$urun->dot} </td>
                                        <td> {$urun->zaman} </td>
                                        <td> {$urun->sistem} </td>
                                        <td>
                                            <a onclick="return confirm('Ürünü Görmezden Gelmek İstediğinize Emin Minisiniz?');"
                                               href="{site_url('products/remove_ent')}/{$urun->id}" title="Sil"
                                               class="btn btn-sm btn-default sil">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                            {$this->pagination->create_links()}
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <script>

        </script>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Desenler</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{site_url('/')}">Anasayfa</a>
                </li>
                <li>
                    <a href="{site_url('products/index')}">Ürünler</a>
                </li>
                <li>
                    <a href="{site_url('products/brand/index')}">Markalar</a>
                </li>
                <li class="active">Desenler</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut desenleri görebilir, Yeni desen ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#desen_ekle">
                            <i class="fa fa-plus"></i>  
                            Desen Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Desenler {if $markasi} - {$markasi->adi}{/if}</a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="30%"> Desen Adı </th> 
                                                    <th width="15%"> Kategori </th>
                                                    <th width="15%"> Marka </th>
                                                    <th width="10%"> Tip </th>
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="8%"> <center>Ürünler</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {if count($desenler) > 0}

                                                    {foreach $desenler as $desen}

                                                        <tr>
                                                            <td>{$desen->id}</td> 
                                                            <td align="center">{$desen->sira}</td> 
                                                            <td>{$desen->adi}</td> 
                                                            <td>{agaclar($desen->kategori_adlari)}</td>
                                                            <td>{agaclar($desen->marka_adlari)}</td>
                                                            <td>{$desen->tip}</td>                                                           
                                                            <td>
                                                                <a onclick="durum_degis({$desen->id},'desenler', '#desendurum{$desen->id}');" id="desendurum{$desen->id}" class="btn btn-circle btn-icon-only btn-default {if $desen->durum == 1}durumu{else}beyaz{/if}" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_{$desen->id}"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_{$desen->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                           														<form method="post" enctype="multipart/form-data" action="{site_url('products/brand/editpatternimage')}/{$desen->id}">
	                                                                            	<div class="form-group">						                                                            
						                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
						                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
						                                                                    	{if $desen->resim}
						                                                                    	<img src="{$this->extraservices->fullresimurl('desen_resim', $desen->resim)}" alt="" />
						                                                                    	{else}
						                                                                    	<img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
						                                                                    	{/if}
						                                                                    </div>
						                                                                    <div>
						                                                                        <span class="btn red btn-outline btn-file">
						                                                                            <span class="fileinput-new"> Resim Seçiniz </span>
						                                                                            <span class="fileinput-exists"> Değiştir </span>
						                                                                            <input type="file" name="resim"> </span>
						                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
						                                                                    </div>
						                                                                </div>
							                                                        </div>
							                                                        <div class="form-group">
							                                                        	<button type="submit" class="btn blue">Kaydet</button>
							                                                        </div>
							                                                    </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_{$desen->id}_2"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_{$desen->id}_2" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">2. Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                                <form method="post" enctype="multipart/form-data" action="{site_url('products/brand/editpatternimage')}/{$desen->id}/resim_2">
                                                                                    <div class="form-group">                                                                                    
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                                                                                                {if $desen->resim}
                                                                                                <img src="{$this->extraservices->fullresimurl('desen_resim', $desen->resim_2)}" alt="" />
                                                                                                {else}
                                                                                                <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                                {/if}
                                                                                            </div>
                                                                                            <div>
                                                                                                <span class="btn red btn-outline btn-file">
                                                                                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                                    <span class="fileinput-exists"> Değiştir </span>
                                                                                                    <input type="file" name="resim"> </span>
                                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_{$desen->id}_3"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_{$desen->id}_3" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">3. Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                                <form method="post" enctype="multipart/form-data" action="{site_url('products/brand/editpatternimage')}/{$desen->id}/resim_3">
                                                                                    <div class="form-group">                                                                                    
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                                                                                                {if $desen->resim}
                                                                                                <img src="{$this->extraservices->fullresimurl('desen_resim', $desen->resim_3)}" alt="" />
                                                                                                {else}
                                                                                                <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                                {/if}
                                                                                            </div>
                                                                                            <div>
                                                                                                <span class="btn red btn-outline btn-file">
                                                                                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                                    <span class="fileinput-exists"> Değiştir </span>
                                                                                                    <input type="file" name="resim"> </span>
                                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>

                                                            <td align="center">                                                 
                                                                <a href="#urunler" class="btn red btn-outline sbold bizimbutonlar" title="Ürünler">
                                                                    Ürünler
                                                                </a>
                                                            </td>
                                                            <td>  
                                                                <a data-toggle="modal" href="#desenduzenle_{$desen->id}" title="Ürün Açıklaması Ekle" class="btn btn-sm btn-default hizliduzenle">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>   
                                                                <a href="{site_url('products/brand/editpattern')}/{$desen->id}" title="Desen Düzenle" class="btn btn-sm btn-default detayliduzenle">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>                                                
                                                                <a href="{site_url('products/brand/modernedit')}/{$desen->id}" style="background-color: #229922 !important" title="Desen Açıklama Düzenle" class="btn btn-sm durumu btn-primary ">
                                                                    <i class="fa fa-pencil-square"></i>
                                                                </a>                          
                                                                <a href="{site_url('products/brand/renderpattern')}/{$desen->id}" title="Desen Açıklama Düzenle" class="btn btn-sm btn-default detayliduzenle durumu">
                                                                    <i class="fa fa-pencil-square"></i>
                                                                </a>                                               
                                                                <div class="modal fade" id="desenduzenle_{$desen->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Desen Düzenleme</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="tab-pane" id="tab_kategori_ekle">
                                                                                    <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/brand/editpattern')}/{$desen->id}">
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">Desen Adı:
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-8" id="kategori_adi">
                                                                                                <input type="text" class="form-control" name="adi" placeholder="" value="{$desen->adi}">                                            
                                                                                            </div>
                                                                                        </div>
                                                                                        {if $desen->tip}
                                                                                        <input type="hidden" name="tip" value="{$desen->tip}" />
                                                                                        {else}
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">
                                                                                                Desen Tipi
                                                                                            </label>
                                                                                            <div class="col-md-8">
                                                                                                <select name="tip" class="form-control" required>
                                                                                                    <option value="">Seçiniz</option>
                                                                                                    {foreach $tipler as $tip}
                                                                                                        <option {if $markasi->tip == $tip->adi}selected{/if} value="{$tip->adi}">{$tip->adi}</option>
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        {/if}                                                          
                                                                                        {*}<div class="form-group">
                                                                                            <label class="col-md-4 control-label">Desen Açıklaması:
                                                                                            </label>
                                                                                            <div class="col-md-8">
                                                                                                <textarea  class="form-control" name="desen_aciklama">{$desen->desen_aciklama}</textarea>
                                                                                            </div>
                                                                                        </div>{*}
 

                                                                                        <div class="form-group">
                                                                                            <label class="col-md-12 control-label">Desen Ürün Açıklaması:
                                                                                            </label>
                                                                                            <div class="col-md-12">
                                                                                                <textarea  class="ckeditor form-control" name="urun_aciklama" row="6">{$desen->urun_aciklama}</textarea>
                                                                                            </div>
                                                                                        </div>                																					
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">Sıra No:
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-8" id="">
                                                                                                <input type="number" class="form-control" name="sira" min="1" value="{$desen->sira}">                                            
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label"></label>
                                                                                            <div class="col-md-8" id="kategorilerimiz">         
                                                                                                <button type="submit" class="btn blue">Düzenle</button> 
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div> 
                                                                            </div> 
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div> 
                                                                <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('products/brand/removepattern')}/{$desen->id}" title="Sil" class="btn btn-sm btn-default sil">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    {/foreach}
                                                {else}
                                                <tr>
                                                    <td colspan='10' style='text-align:center;'> Bu Markaya Ait Desen Yok.</td>
                                                </tr>
                                                {/if}
                                            </tbody>
                                        </table>
                                        {$this->pagination->create_links()}
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        <!-- Desen Ekle -->
        <div class="modal fade in" id="desen_ekle" tabindex="-1" role="desen_ekle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Desen Ekle</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/brand/addpattern')}">
                            <input type="hidden" name="marka" value="{$markasi->id}" />
                            <input type="hidden" name="ust" value="0" />
                            {if $markasi->tip}
                            <input type="hidden" name="tip" value="{$markasi->tip}" />
                            {else}
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Desen Tipi
                                </label>
                                <div class="col-md-8">
                                    <select name="tip" class="form-control form-filter input-sm select2me" required>
                                        <option value="">Seçiniz</option>
                                        {foreach $tipler as $tip}
                                            <option value="{$tip->adi}">{$tip->adi}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            {/if}
                            <div class="form-group">                                             
                                <label class="col-md-4 control-label">  
                                    Kategori 
                                </label> 
                                <div class="col-md-8" id="kategorilerimiz">
                                    <select id="kategori" name="kategori" data-placeholder="Kategoriler" onchange="kategoriler(1, 'lastik', 'kategori')" class="form-control form-filter input-sm select2me">
                                        <option value="">Seçiniz</option>
                                        {foreach $kategoriler as $kategori}
                                        <option value="{$kategori->id}">{$kategori->adi}</option>
                                        {/foreach}
                                    </select>
                                    <div id="alt_kategori"></div>                                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Mevsim
                                </label>
                                <div class="col-md-8">
                                    <select name="mevsim" data-placeholder="Mevsimler" class="form-control form-filter input-sm select2me" required>
                                        <option value="">Seçiniz</option>
                                        {foreach $mevsimler as $mevsim}
                                            <option value="{$mevsim->id}">{$mevsim->adi}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="adi" placeholder="">                                            
                                </div>
                            </div>
                        

                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="form-control" name="desen_aciklama"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Ürün Açıklaması:
                           </label>
                                <div class="col-md-8">
                                    <textarea  class="form-control" name="urun_aciklama"></textarea>
                                </div>
                            </div>


                            <div class="form-group" >
                                <label class="col-md-4 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" id="">
                                    <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Ekle</button> 
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Kategori Ekle -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
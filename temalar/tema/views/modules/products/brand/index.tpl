{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Markalar</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{site_url('/')}">Anasayfa</a>
                </li>
                <li>
                    <a href="{site_url('products/index')}">Ürünler</a>
                </li>
                <li class="active">Markalar</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut markaları görebilir, Yeni marka ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#marka_ekle">
                            <i class="fa fa-plus"></i>  
                            Marka Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Markalar {if $markasi} - {$markasi->adi}{/if}</a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="60%"> Marka Adı </th> 
                                                    <th width="10%"> Tip </th>
                                                    <th width="8%"> <center>Marka Gruplaması</center> </th>                                               
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                               
                                                    <th width="8%"> <center>Desenler</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {if count($markalar) > 0}

                                                    {foreach $markalar as $marka}
                                                        <tr>
                                                            <td>{$marka->id}</td> 
                                                            <td align="center">{$marka->sira}</td> 
                                                            <td>{$marka->adi}</td> 
                                                            <td>{$marka->tip}</td>
                                                            <td>
                                                                <select name="fiyat_grup" class="form-control" data-marka="{$marka->id}">
                                                                <option value="">Seçiniz</option>
                                                                <option {if $marka->fiyat_grup == "T1"}selected{/if} value="T1">T1</option>
                                                                <option {if $marka->fiyat_grup == "T2"}selected{/if} value="T2">T2</option>
                                                                <option {if $marka->fiyat_grup == "T3"}selected{/if} value="T3">T3</option>
                                                                
                                                            </select>
                                                            </td>
                                                            {*<td align="center">
                                                                <a href="{site_url('products/brand/index')}/{$marka->id}" class="btn btn-sm red btn-outline sbold bizimbutonlar">Alt Markalar</a>
                                                            </td>     *}                                                       
                                                            <td>
                                                                <a onclick="durum_degis({$marka->id},'markalar', '#markadurumu{$marka->id}');" id="markadurumu{$marka->id}" class="btn btn-circle btn-icon-only btn-default {if $marka->durum == 1}durumu{else}beyaz{/if}" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#marka_resmi_{$marka->id}"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="marka_resmi_{$marka->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Marka Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                           														<form method="post" enctype="multipart/form-data" action="{site_url('products/brand/editbrandimage')}/{$marka->id}">
	                                                                            	<div class="form-group">						                                                            
						                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
						                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
						                                                                    	{if $marka->resim}
						                                                                    	<img src="{$this->extraservices->fullresimurl('marka_resim', $marka->resim)}" alt="" />
						                                                                    	{else}
						                                                                    	<img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
						                                                                    	{/if}
						                                                                    </div>
						                                                                    <div>
						                                                                        <span class="btn red btn-outline btn-file">
						                                                                            <span class="fileinput-new"> Resim Seçiniz </span>
						                                                                            <span class="fileinput-exists"> Değiştir </span>
						                                                                            <input type="file" name="resim"> </span>
						                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
						                                                                    </div>
						                                                                </div>
							                                                        </div>
							                                                        <div class="form-group">
							                                                        	<button type="submit" class="btn blue">Kaydet</button>
							                                                        </div>
							                                                    </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>
                                 
                                                            <td align="center">                                                 
                                                                <a href="{site_url('products/brand/patterns')}/{$marka->id}" class="btn btn-sm red btn-outline sbold bizimbutonlar">Desenler</a>
                                                            </td>
                                                            <td>  
                                                                <a  href="{site_url('products/brand/editbrand')}/{$marka->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle" target="_blank">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>                                                        
                                                             
                                                                <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('products/brand/remove')}/{$marka->id}" title="Sil" class="btn btn-sm btn-default sil">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    {/foreach}
                                                {else}
                                                <tr>
                                                    <td colspan='10' style='text-align:center;'> Bu Markaya Ait Alt Marka Yok.</td>
                                                </tr>
                                                {/if}
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
        <!-- Marka Ekle -->
        <div class="modal fade in" id="marka_ekle" tabindex="-1" role="marka_ekle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Marka Ekle</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/brand/addbrand')}">
                            <input type="hidden" name="ust" value="{$ust}" />
                            {if $markasi->tip}
                            <input type="hidden" name="tip" value="{$markasi->tip}" />
                            {else}
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Marka Tipi
                                </label>
                                <div class="col-md-8">
                                    <select name="tip" class="form-control" required>
                                        <option value="">Seçiniz</option>
                                        {foreach $tipler as $tip}
                                            <option value="{$tip->adi}">{$tip->adi}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            {/if}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Marka Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="adi" placeholder="">                                            
                                </div>
                            </div>
                        
                            <div class="form-group" >
                                <label class="col-md-4 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" id="">
                                    <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Aktif Kategoriler
                                </label>
                                <div class="col-md-8">
                                    <select name="kategoriler[]" class="form-control multiple" multiple="multiple" required>
                                        <option value="">Seçiniz</option>
                                        {foreach $kategoriler as $kategori}
                                            <option value="{$kategori->adi}">{$kategori->adi}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Ekle</button> 
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Kategori Ekle -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
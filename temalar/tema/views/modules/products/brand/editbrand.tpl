{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                                            <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/brand/editbrand')}/{$marka->id}">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Marka Adı:
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3" id="kategori_adi">
                                                        <input type="text" class="form-control" name="adi" placeholder="" value="{$marka->adi}">                                            
                                                    </div>
                                                </div>
                                                {if $marka->tip}
                                                <input type="hidden" name="tip" value="{$marka->tip}" />
                                                {else}
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Marka Tipi
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select name="tip" class="form-control" required>
                                                            <option value="">Seçiniz</option>
                                                            {foreach $tipler as $tip}
                                                            <option {if $marka->tip == $tip->adi}selected{/if} value="{$tip->adi}">{$tip->adi}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                {/if}          
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Sıra No:
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3" id="">
                                                        <input type="number" class="form-control" name="sira" min="1" value="{$marka->sira}">                                            
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Marka Açıklama:
                                                    </label>
                                                    <div class="col-md-9" style="max-width: 850px;">
                                                        <textarea  class="ckeditor form-control" name="marka_aciklama" row="6">{$marka->marka_aciklama}</textarea>
                                                    </div>
                                                </div>                                                                                  

												<div class="form-group">
													<label class="col-md-3 control-label">Marka Etiket:
														<span> </span>
													</label>
													<div class="col-md-3">
														<input type="text" class="form-control" name="keywords" placeholder="Marka Etiketi" value="{$marka->keywords}">                                            
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Aktif Kategoriler
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select name="kategoriler[]" class="form-control multiple" multiple="multiple" required>
                                                            <option value="">Seçiniz</option>
                                                            {$active_categories = explode(',',$marka->kategoriler)}
                                                            {foreach $kategoriler as $kategori}
                                                                <option value="{$kategori->adi}" {if in_array($kategori->adi, $active_categories)}selected{/if}>{$kategori->adi}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"></label>
                                                    <div class="col-md-9" id="kategorilerimiz">         
                                                        <button type="submit" class="btn blue">Düzenle</button> 
                                                    </div>
                                                </div>
                                            </form>
                                      

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        {include file="base/footer_txt.tpl"}
    </div>
    {include file="base/quicksidebar.tpl"}
    {include file="base/footer.tpl"}
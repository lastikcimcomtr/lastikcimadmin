{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->

        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-3 p-0" style="font-size: 9px">
                <img src="http://admin.lastikcim.com.tr/temalar/tema/assets/img/page_design.png?v=0.02" class="img-fluid" style="width: 100%">
            </div>
            <div class="col-md-9">
                <div class="portlet">
                    <div class="portlet-body">
                        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="{site_url('products/brand/renderpattern')}/{$desen->id}">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-10" id="kategori_adi">
                                    <input type="text" class="form-control" name="adi" placeholder="" value="{$desen->adi}" disabled>                                            
                                </div>
                            </div>
                            {foreach from=$input_parse key=k item=input_row}
                                {foreach from=$input_row key=k_2 item=input_group}
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">{$input_group['label']}:
                                        </label>
                                        <div class="col-md-10">
                                            {if $input_group['type'] == 'input'}
                                            <input type="{$input_group['input-type']}"  class="form-control" name="row[{$k}][{$k_2}][{$input_group['name']}]" placeholder="{$input_group['placeholder']}" />
                                            {else}
                                            <textarea class="form-control ckeditor" name="row[{$k}][{$k_2}][{$input_group['name']}]" placeholder="{$input_group['placeholder']}"></textarea>
                                            {/if}
                                        </div>
                                    </div>    
                                    {if sizeOf($input_group['sub_input'])>0}
                                        {$input_group=$input_group['sub_input']}
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{$input_group['label']}:
                                            </label>
                                            <div class="col-md-10">
                                                {if $input_group['type'] == 'input'}
                                                <input type="{$input_group['input-type']}"  class="form-control" name="row[{$k}][{$k_2}][{$input_group['name']}]" placeholder="{$input_group['placeholder']}" />
                                                {else}
                                                <textarea class="form-control ckeditor" name="row[{$k}][{$k_2}][{$input_group['name']}]" placeholder="{$input_group['placeholder']}"></textarea>
                                                {/if}
                                            </div>
                                        </div>   
                                    {/if}
                                {/foreach}
                            {/foreach}


                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Oluştur</button> 
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
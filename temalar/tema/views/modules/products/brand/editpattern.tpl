{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->

        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">

                    <div class="portlet-body">



                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/brand/editpattern')}/{$desen->id}">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="adi" placeholder="" value="{$desen->adi}">                                            
                                </div>
                            </div>
                            {if $desen->tip}
                            <input type="hidden" name="tip" value="{$desen->tip}" />
                            {else}
                            <div class="form-group">
                                <label class="col-md-3 control-label">
                                    Desen Tipi
                                </label>
                                <div class="col-md-3">
                                    <select name="tip" class="form-control" required>
                                        <option value="">Seçiniz</option>
                                        {foreach $tipler as $tip}
                                        <option {if $markasi->tip == $tip->adi}selected{/if} value="{$tip->adi}">{$tip->adi}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            {/if}                    
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="">
                                    <input type="number" class="form-control" name="sira" min="1" value="{$desen->sira}">                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Kategorisi Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="ckeditor form-control" name="desen_aciklama">{$desen->desen_aciklama}</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Ürün Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="ckeditor form-control" name="urun_aciklama" row="6">{$desen->urun_aciklama}</textarea>
                                </div>
                            </div>                                             

							<div class="form-group">
                                <label class="col-md-3 control-label">Desen Keywords:
                                    <span> </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="keywords" placeholder="" value="{$desen->keywords}">                                            
                                </div>
                            </div>

							<div class="form-group">
								<label class="col-md-3 control-label">Mevsim:
								</label>
								<div class="col-md-3 id="">
									<select name="mevsim" class="form-control">
										<option value="">Yok</option>
										<option value="4m" {if $desen->mevsim=="4m"}selected{/if}>4m</option>
										<option value="kis" {if $desen->mevsim=="kis"}selected{/if}>kis</option>
										<option value="yaz" {if $desen->mevsim=="yaz"}selected{/if}>yaz</option>
									</select>                                        
								</div>
							</div>	

                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Düzenle</button> 
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
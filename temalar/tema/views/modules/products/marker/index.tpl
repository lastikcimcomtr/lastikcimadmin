{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <div class="row">

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">

                      <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th  style="border-right: 0">

                                  <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="{site_url('products/marker/add')}">
                                    <i class="fa fa-plus"></i>  
                                    Etiket Ekle 
                                </a>

                            </th> 
                            <th><form method="get" style="display: inline-flex"><input style="width: 140px" class="form-control" name="etiket-ara" value="{$ara}" />
                                <button class="btn btn-primary">Ara</button></form></th>
                            <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
<span class="btn btn-circle btn-icon-only btn-default durumu"></span>
                                 <span id="islembilgisitext">Acıklaması Var</span>   
                                 <span class="btn btn-circle btn-icon-only btn-default beyaz"></span>
                                 <span id="islembilgisitext">Acıklaması Yok</span>   
                             <span class="btn btn-sm btn-default detayliduzenle"><i class="fa fa-pencil-square-o"></i></span>      
                             <span id="islembilgisitext">Düzenle</span>

                         </th>
                     </tr>
                 </thead>
             </table>

             <div class="portlet-body">
                <div class="tabbable-bordered">

                    <div class="tab-content">                               
                        <div class="tab-pane active" id="tab_kategoriler">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="1%">id</th> 
                                            <th width="48%">Etiket</th> 
                                            <th width="48%">Url </th> 
                                            <th width="1%"></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if count($etiketler) > 0}

                                        {foreach $etiketler as $etiket}
                                        <tr>
                                            <td style="text-align: center;">{$etiket->id}</td> 
                                            <td >{$etiket->adi}</td> 
                                            <td >{$etiket->sef_url}</td> 
                                            <td>
                                                <span class="btn btn-circle btn-icon-only btn-default {if $etiket->etiket_aciklama|count_characters > 20}durumu{else}beyaz{/if}"></span>
                                             <a href="{site_url('products/marker/modernedit/'|cat:$etiket->id)}"  style="background:green;" class="btn btn-sm btn-default detayliduzenle" title="Düzenle" target="_blank" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>

                                               <a href="{site_url('products/marker/edit/'|cat:$etiket->id)}"   class="btn btn-sm btn-default detayliduzenle" title="Düzenle" target="_blank" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>

                                        </tr>
                                        {/foreach}
                                        {else}
                                        <tr>
                                            <td colspan='10' style='text-align:center;'>Veri Bulunamadı </td>
                                        </tr>
                                        {/if}
                                    </tbody>
                                </table>
                                {$this->pagination->create_links()}
                            </div> 
                        </div>                             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->



</div>
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->

        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">

                    <div class="portlet-body">



                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/marker/add')}">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Etiket Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="adi" placeholder="" >                                            
                                </div>
                            </div>                            <div class="form-group">
                                <label class="col-md-3 control-label">Etiket Url:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="sef_url" placeholder="" >                                            
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label">Etiket Keywords:
                                    <span> </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="keywords" placeholder="" >                                            
                                </div>
                            </div>
							<div class="form-group">
                                <label class="col-md-3 control-label">Etiket Description:
                                    <span> </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="description" placeholder="" >                                            
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Etiket Listesi Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="ckeditor form-control" name="etiket_aciklama"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru 1:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_1_soru" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cevap 1:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_1_cevap" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru 2:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_2_soru" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cevap 2:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_2_cevap" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru 3:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_3_soru" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cevap 3:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_3_cevap" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru 4:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_4_soru" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cevap 4:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_4_cevap" placeholder="" >        
                                </div>
                            </div>           
                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru 5:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_5_soru" placeholder="" >        
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Cevap 5:
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="soru_5_cevap" placeholder="" >        
                                </div>
                            </div>                    
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Kaydet</button> 
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
<!DOCTYPE html> 
<html lang="tr">
<head>
    <meta charset="utf-8" />
    <title>Lastikcim</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/tinymce@5.1.5/tinymce.min.js"></script>
    <script src="/temalar/tema/assets/apps/plugins/tinymce/plugin/plugin.js"></script>
    <link rel="shortcut icon" href="favicon.ico" /> 
</head>
<body class="">
<div class="container-fluid">
    <div class="page-content">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                        {if isset($saved)}
                        <div class="alert alert-primary">
                            {$saved}
                        </div>
                        {/if}
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/marker/modernedit')}/{$etiket->id}">
                            <div class="form-group">
                                <label class="col-md-3 control-label">{$etiket->adi}
                                    <span class="required"> * </span>
                                </label>
                            </div>       
                            <div class="form-group">
                                <label class="col-md-3 control-label">Etiket Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="tinymce form-control" name="etiket_aciklama">{$etiket->etiket_aciklama}</textarea>
                                </div>
                            </div>   

                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn btn-primary blue">Düzenle</button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
     <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/dc840779d0.js" crossorigin="anonymous"></script>
    {literal}
    <script type="text/javascript">
        var tbpKey = 'O/2t0LY/m1CDEzh3C9heyCxWD05rhPmQi8mB/0revfYU98KfzI+Wespvcgixm6oG1+MJzX46YgvYd8zs0xJlCe8CHAKDni4nQopeRSkJw7g=';
        var base_url = location.protocol + '//' + location.host + '/';
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'advlist autolink bootstrap link image lists charmap print preview help table code',
            toolbar: [
            'undo redo | bootstrap',
            'cut copy paste | styleselect | alignleft aligncenter alignright alignjustify | bold italic | link image | preview | tools code | help'],
            contextmenu: "link image imagetools table spellchecker | bootstrap",
            file_picker_types: 'file image media',
            bootstrapConfig: {
                url: base_url + '/temalar/tema/assets/apps/plugins/tinymce/plugin/',
                iconFont: 'fontawesome5',
                imagesPath: '/uploads/images/',
                key: tbpKey
            },
            formats: {
            alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-left'},
            aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-center'},
            alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-right'},
            alignjustify: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-justify'},
            bold: {inline : 'strong'},
            italic: {inline : 'em'},
            underline: {inline : 'u'},
            sup: {inline : 'sup'},
            sub: {inline : 'sub'},
            strikethrough: {inline : 'del'}
            },
            style_formats_autohide: true,
            height : "380",
            images_upload_url : '/home/uploadImage',
            images_upload_base_path: '/uploads/images',
            automatic_uploads : false,
        });
    </script>
    {/literal}
</body>
</html>
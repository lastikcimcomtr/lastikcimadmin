{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Ürün Ara 
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="expand" data-original-title="Aç / Kapat"> </a> 
                        </div>
                    </div>
                    <div class="portlet-body tabs-below">
                        <form class="form-horizontal form-row-seperated" method="post" action="">
                            <input type="hidden" name="tip" value="{$tip}" />
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Kategori</label>
                                    <div class="col-md-6" id="kategorilerimiz">
                                        <select id="kategori" name="kategori" data-placeholder="Kategoriler" onchange="kategoriler(1, '{$tip}', 'kategori')" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $kategoriler as $kategori}
                                                <option value="{$kategori->id}">{$kategori->adi}</option>
                                            {/foreach}
                                        </select>
                                        <div id="alt_kategori"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Ürün Adı</label>
                                    <div class="col-md-6" id="kategorilerimiz">                                                                                                                    
                                        <input type="text" class="form-control" autocomplete = "off" name="urun_adi" placeholder="" value="" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Ürün ID</label>
                                    <div class="col-md-6" id="kategorilerimiz">                                                                                                                    
                                        <input type="text" class="form-control" autocomplete = "off"  name="id" placeholder="" value="" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-1 control-label"></label>
                                        <div class="col-md-10" id="kategorilerimiz">         
                                            <button type="submit" class="btn blue">Ürün Ara</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        
                        <div class="tab-content" style="border:none; padding-top: 0px;">
                            <div class="tab-pane active" id="overview_1">
                                <div class="table-responsive">
                                    <div id="cokluurunguncellemecevap" style="padding: 20px 0px; display: none;"></div>
                                    <form action="javascript:void(-1);" autocomplete="off" method="POST" id="cokluurunguncelleme" onsubmit="coklurunformgonder()">
                                        <table class="table table-striped table-hover table-bordered" style="margin-bottom: 0px;">
                                            <thead>
                                                <tr>
                                                    <th style="width:0.1%; line-height: auto;"> 
                                                        <input type="checkbox" id="checkAll" class="icheck" />
                                                    </th>
                                                    <th width="10%"> <div class="col-md-12">Stok Kodu</div> </th>
                                                    <th width="20%"> <div class="col-md-12">Ürün Adı</div> </th>
                                                    <th width="10%"> <div class="col-md-12">Stok Adedi</div> </th>
                                                    <th width="10%"> <div class="col-md-12">KDV</div> </th>
                                                    <th width="5%"> <div class="col-md-12" style="white-space: nowrap;">KDV Dahil</div> </th>
                                                    <th width="10%"> <div class="col-md-12">Alış Fiyat</div> </th>
                                                    <th width="10%"> <div class="col-md-12">Fiyat 1</div></th>
                                                    <th width="10%"> <div class="col-md-12">Fiyat 2</div></th>
                                                    <th width="10%"> <div class="col-md-12">Fiyat 3</div></th>
                                                </tr>
                                            </thead>
                                                <tbody>
                                                    {foreach $urunler as $urun}
                                                    <tr>
                                                        <td style="line-height: 33px;"> 
                                                            <input type="checkbox" name="urunler[{$urun->id}][durum]" class="urun_id icheck" value="1" />
                                                        </td>
                                                        <td> 
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->id}][stok_kodu]" value="{$urun->stok_kodu}">
                                                            </div> 
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->id}][urun_adi]" value="{$urun->urun_adi}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->id}][stok_miktari]" value="{$urun->stok_miktari}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" name="urunler[{$urun->id}][kdv]" value="{$urun->kdv}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <label for="urunler[<?php echo $urun->id; ?>][kdvdahil]">
                                                                <input type="checkbox" name="urunler[{$urun->id}][kdv_dahil]" value="1" {if $urun->kdv_dahil == 1}checked{/if} class="icheck"> 
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <div class="input-group input-group-sm">
                                                                    <span class="input-group-addon" id="sizing-addon1">{$urun->urun_alis_fiyati_birim}</span>
                                                                    <input type="text" class="form-control" name="urunler[{$urun->id}][urun_alis_fiyati]" value="{$urun->urun_alis_fiyati}" aria-describedby="sizing-addon1">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <div class="input-group input-group-sm">
                                                                    <span class="input-group-addon" id="sizing-addon1">{$urun->fiyat_birim}</span>
                                                                    <input type="text" class="form-control" name="urunler[{$urun->id}][fiyat]" value="{$urun->fiyat}" aria-describedby="sizing-addon1">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <div class="input-group input-group-sm">
                                                                    <span class="input-group-addon" id="sizing-addon1">{$urun->fiyat_2_birim}</span>
                                                                    <input type="text" class="form-control" name="urunler[{$urun->id}][fiyat_2]" value="{$urun->fiyat_2}" aria-describedby="sizing-addon1">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="col-md-12">
                                                                <div class="input-group input-group-sm">
                                                                    <span class="input-group-addon" id="sizing-addon1">{$urun->fiyat_3_birim}</span>
                                                                    <input type="text" class="form-control" name="urunler[{$urun->id}][fiyat_3]" value="{$urun->fiyat_3}" aria-describedby="sizing-addon1">
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    {/foreach}
                                                    <tr>
                                                        <td colspan="10"><button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Kaydet</button>  </td>
                                                    </tr>
                                                </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div> 
                        </div>

                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
	<div class="page-content">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">

				<form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-shopping-cart"></i> Ürün Kopyalama Formu 
							</div>
						</div>
						<div class="portlet-body">
							{if $status && $message}
							<div class="note note-{$status}">
								<p> {$message} </p>
							</div>
							{/if}
							<div class="col-md-4">
								<h4>Kopyalanacak Ürün Bilgileri</h4>
								<div class="form-group">
									<label>Ürün Adı</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->urun_adi}" disabled="disabled" />
								</div>
								<div class="form-group">
									<label>Ürün Stok Kodu</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->stok_kodu}" disabled="disabled" />
								</div>
								<div class="form-group">
									<label>Üretim Tarihi</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->uretim_tarihi}" disabled="disabled" />
								</div>
								<div class="form-group">
									<label>Ürün XML KODU</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->xml_stok_kodu}" disabled="disabled" />
								</div>
								<div class="form-group">
									<label>Ürün Alternatif XML KODU</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->xml_stok_kodu_alt}" disabled="disabled" />
								</div>
								<div class="form-group">
									<label>Ürün Tedarikçi</label>
								<input style="background-color: #dedede" type="text" class="form-control" name="urun_adi" placeholder="" value="{$product->tedarikci}" disabled="disabled" />
								</div>
							</div>
							<div class="col-md-4 col-md-offset-1">
								<h4>Yeni Oluşturulacak Ürün Bilgileri</h4>
								<div class="form-group">
									<label>Ürün Stok Kodu</label>
								<input type="text" class="form-control" name="stok_kodu" placeholder="" value="{$product->stok_kodu}" />
								</div>
								<div class="form-group">
									<label>Üretim Tarihi</label>
								<input type="text" class="form-control" name="uretim_tarihi" placeholder="" value="{$product->uretim_tarihi}"  />
								</div>
								<div class="form-group">
									<label>Ürün XML KODU</label>
								<input type="text" class="form-control" name="xml_stok_kodu" placeholder="" value="{$product->xml_stok_kodu}"  />
								</div>
								<div class="form-group">
									<label>Ürün Alternatif XML KODU</label>
								<input type="text" class="form-control" name="xml_stok_kodu_alt" placeholder="" value="{$product->xml_stok_kodu_alt}"  />
								</div>
								<div class="form-group">
									<label>Ürün Tedarikçi</label>
								<input type="text" class="form-control" name="tedarikci" placeholder="" value="{$product->tedarikci}"  />
								</div>
								<span class="mb-3">Ürün kopyalanandıktan sonra stoğu 0 olarak belirlenecek ve girmiş stok kodlarını kullanacaktır. 
									Ürün düzenleme sayfasına yönlendirileceksiniz ve burada ürünün stok miktari gibi bilgilerini girmeniz gerekmektedir.
								</span>
								<hr>
								<div class="form-group pull-right">
									<button class="btn btn-primary">Kopyala</button>
								</div>
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>
			<!-- END PAGE BASE CONTENT -->
	</div>
		{include file="base/footer_txt.tpl"}
	</div>
	{include file="base/quicksidebar.tpl"}
	{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                       Hatalı Ürünler
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th width="1%"><input type="checkbox"></th>
                                    <th width="1%"> Resim</th>
                                    <th width="1%"> ID</th>
                                    <th> Ürün adı</th>
                                    <th> Stok Kodu</th>
                                    <th> Fiyat</th>
                                    <th> Güncelleme</th>
                                    <th> Kategori</th>
                                    <th width="1%">
                                        <span class="btn btn-circle btn-icon-only btn-default durumu"
                                              title="Durumu"></span>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default anasayfavitrini"
                                           title="Anasayfa Vitrini"></a>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default kampanyaliurun"
                                           title="Kampanyalı Ürün"></a>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default kategorivitrini"
                                           title="Kategori Vitrini"></a>
                                    </th>

                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default markavitrini"
                                           title="Marka Vitrini"></a>
                                    </th> {*}
                                        <th width="1%">
                                            <a class="btn btn-circle btn-icon-only btn-default yeniurun" title="Yeni Ürün"></a> 
                                        </th>
                                        <th width="1%">
                                            <a class="btn btn-circle btn-icon-only btn-default sponsorurunu" title="Sponsor Ürünü"></a> 
                                        </th>
                                        <th width="1%">
                                            <a class="btn btn-circle btn-icon-only btn-default populerurun" title="Popüler Ürün"></a> 
                                        </th>
                                        <th width="1%">
                                            <a class="btn btn-circle btn-icon-only btn-default gununurunu" title="Günü Ürünü"></a> 
                                        </th>{*}
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach $urunler as $urun}
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>
                                            {if $urun->resim_1}
                                                <a href="{$this->extraservices->fullresimurl('urun_resim', {$urun->resim_1})}"
                                                   rel="popupacil" target="_blank">
                                                    <img rel="popupacil" border="0" width="30" height="20"
                                                         src="{$this->extraservices->fullresimurl('urun_resim', {$urun->resim_1})}">
                                                </a>
                                            {else}
                                                <a href="{$this->extraservices->fullresimurl('urun_resim', 'resim_yok.jpg')}"
                                                   rel="popupacil" target="_blank">
                                                    <img rel="popupacil" border="0" width="30" height="20"
                                                         src="{$this->extraservices->fullresimurl('urun_resim', 'resim_yok.jpg')}">
                                                </a>
                                            {/if}
                                        </td>
                                        <td> {$urun->id} </td>
                                        <td> {$urun->urun_adi}  </td>
                                        <td> {$urun->stok_kodu} </td>
                                        <td> {fiyatver($urun->fiyat, $urun->fiyat_birim)} </td>
                                        <td> {date('d.m.Y H:i:s', $urun->apizaman)} </td>
                                        <td> {$urun->tip} </td>
                                        <td id="durum_guncelle">
                                            <a onclick="urunparametreguncelle({$urun->id}, 'durumu', {$urun->durumu},'{$urun->tip}', 'durumu');"
                                               id="durumu_{$urun->id}"
                                               class="btn btn-circle btn-icon-only btn-default {if $urun->durumu == 1}durumu{else}beyaz{/if}"
                                               title="Durumu"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle({$urun->id}, 'anasayfa_vitrini', {$urun->anasayfa_vitrini}, '{$urun->tip}', 'anasayfavitrini');"
                                               id="anasayfa_vitrini_{$urun->id}"
                                               class="btn btn-circle btn-icon-only btn-default {if $urun->anasayfa_vitrini == 1}anasayfavitrini{else}beyaz{/if}"
                                               title="Anasayfa Vitrini"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle({$urun->id}, 'kapmanyali_urunler', {$urun->kapmanyali_urunler},'{$urun->tip}', 'kampanyaliurun');"
                                               id="kapmanyali_urunler_{$urun->id}"
                                               class="btn btn-circle btn-icon-only btn-default {if $urun->kapmanyali_urunler == 1}kampanyaliurun{else}beyaz{/if}"
                                               title="Kampanyalı Ürün"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle({$urun->id}, 'kategori_vitrini', {$urun->kategori_vitrini},'{$urun->tip}', 'kategorivitrini');"
                                               id="kategori_vitrini_{$urun->id}"
                                               class="btn btn-circle btn-icon-only btn-default {if $urun->kategori_vitrini == 1}kategorivitrini{else}beyaz{/if}"
                                               title="Kategori Vitrini"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle({$urun->id}, 'marka_vitrini', {$urun->marka_vitrini},'{$urun->tip}', 'markavitrini');"
                                               id="marka_vitrini_{$urun->id}"
                                               class="btn btn-circle btn-icon-only btn-default {if $urun->marka_vitrini == 1}markavitrini{else}beyaz{/if}"
                                               title="Marka Vitrini"></a>
                                        </td>

                                        {*} <td>
                                             <a onclick="urunparametreguncelle({$urun->id}, 'yeni_urun', {$urun->yeni_urun},'{$urun->tip}', 'yeniurun');" id="yeni_urun_{$urun->id}" class="btn btn-circle btn-icon-only btn-default {if $urun->yeni_urun == 1}yeniurun{else}beyaz{/if}" title="Yeni Ürün"></a>
                                         </td>
                                         <td>
                                             <a onclick="urunparametreguncelle({$urun->id}, 'sponsor_urunu', {$urun->sponsor_urunu},'{$urun->tip}', 'sponsorurunu');" id="sponsor_urunu_{$urun->id}" class="btn btn-circle btn-icon-only btn-default {if $urun->sponsor_urunu == 1}sponsorurunu{else}beyaz{/if}" title="Sponsor Ürünü"></a>
                                         </td>
                                         <td>
                                             <a onclick="urunparametreguncelle({$urun->id}, 'populer_urun', {$urun->populer_urun},'{$urun->tip}', 'populerurun');" id="populer_urun_{$urun->id}" class="btn btn-circle btn-icon-only btn-default {if $urun->populer_urun == 1}populerurun{else}beyaz{/if}" title="Popüler Ürün"></a>
                                         </td>
                                         <td>
                                             <a onclick="urunparametreguncelle({$urun->id}, 'gunun_urunu', {$urun->gunun_urunu},'{$urun->tip}', 'gununurunu');" id="gunun_urunu_{$urun->id}" class="btn btn-circle btn-icon-only btn-default {if $urun->gunun_urunu == 1}gununurunu{else}beyaz{/if}" title="Günün Ürünü"></a>
                                         </td>{*}
                                        <td>
                                            <a class="btn btn-sm btn-default benzerurun" title="Benzer Ürün Ekle"
                                               onclick="return confirm('Belirttiğiniz ürün için bir kopya ürün oluşturulacaktır. Oluşturulacak sayfada yapacağınız değişiklikler ile sisteme yeni bir ürün kaydedilecektir.');"
                                               href="">
                                                <i class="fa fa-plus-square"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-default hizliduzenle" title="Hızlı Düzenle"
                                               data-toggle="modal" href="#hizli_duzenle">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <div class="modal fade" id="hizli_duzenle" tabindex="-1" role="basic"
                                                 aria-hidden="true">
                                                <form method="post" action="javascript:void(0);"
                                                      id="hizli_duzenle_{$urun->id}"
                                                      onsubmit="hizliduzenle({$urun->id});" autocomplete="off">
                                                    <input type="hidden" name="tip" value="{$urun->tip}"/>
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Hızlı Düzenle</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form>
                                                                    <div class="form-body">
                                                                        <div id="form_cevap_{$urun->id}"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-4 control-label">
                                                                                Ürün Adı
                                                                            </label>
                                                                            <div class="col-md-8" id="kategorilerimiz"
                                                                                 style="margin-top:5px;">
                                                                                <input type="text" class="form-control"
                                                                                       name="urun_adi"
                                                                                       value="{$urun->urun_adi}"/>
                                                                            </div>
                                                                        </div>
                                                                        {*
                                                                        <div class="form-group"> 
                                                                            <label class="col-md-4 control-label">
                                                                                Sıra No
                                                                            </label>                                                       
                                                                            <div class="col-md-8" id="kategorilerimiz" style="margin-top:5px;">
                                                                                <input type="text" class="form-control" name="sira" value="{$urun->sira}" />                                                           
                                                                            </div> 
                                                                        </div> 
                                                                        *}
                                                                    </div>
                                                                </form>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="modal-footer"
                                                                 style="border-top: 0px;padding-top: 0px;">
                                                                <button type="button" class="btn dark btn-outline"
                                                                        data-dismiss="modal">Kapat
                                                                </button>
                                                                <button type="submit" class="btn green"
                                                                        style="background-color: #f5861d;border-color: #f5861d;">
                                                                    Kaydet
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                </form>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-default hizliduzenle" title="Etiketler" id="etiketler_btn_{$urun->id}"
                                               data-toggle="modal" onclick="init_select2(this);" href="#etiketler_{$urun->id}">
                                                <i class="fa fa-tags"></i>
                                            </a>
                                            <div class="modal fade" id="etiketler_{$urun->id}" role="basic" aria-hidden="true">
                                                <form method="post" action="javascript:void(0);" onsubmit="save_etiketler({$urun->id},this)"
                                                      id="etiketler_{$urun->id}" autocomplete="off">
                                                    <input type="hidden" name="urun_id" value="{$urun->id}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Etiketler</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                    <div class="form-body">
                                                                        <div id="form_cevap_{$urun->id}"></div>
                                                                        <div class="form-group"
                                                                             style="display: inline-block;width: 100%">
                                                                            <label class="col-md-4 control-label font-lg"
                                                                                   style="margin-top:5px">
                                                                                Etiket Adı
                                                                            </label>
                                                                            <div id="kategorilerimiz" class="col-md-7">
                                                                                <select name="etiket_adi"
                                                                                        class="form-control etiket_select2">
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-1"
                                                                                 style="padding-left: 0">
                                                                                <button type="button" onclick="etiket_ekle(this)" class="btn btn-md btn-primary">
                                                                                    Ekle
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list list-unstyled font-lg">
                                                                            {foreach $this->products->urun_etiketler($urun->stok_kodu) as $etiket}
                                                                                <li class="item"
                                                                                    style="border-bottom:1px dimgrey solid">
                                                                                    {$etiket->adi}
                                                                                    <button onclick="$(this).closest('li').remove();" style="margin-top:3px"
                                                                                            class="btn btn-danger btn-xs pull-right">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </button>
                                                                                    <input type="hidden"  name="etiket_id[]" value="{$etiket->id}">
                                                                                </li>
                                                                            {/foreach}
                                                                        </ul>
                                                                        {*
                                                                        <div class="form-group">
                                                                            <label class="col-md-4 control-label">
                                                                                Sıra No
                                                                            </label>
                                                                            <div class="col-md-8" id="kategorilerimiz" style="margin-top:5px;">
                                                                                <input type="text" class="form-control" name="sira" value="{$urun->sira}" />
                                                                            </div>
                                                                        </div>
                                                                        *}
                                                                    </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="modal-footer"
                                                                 style="border-top: 0px;padding-top: 0px;">
                                                                <button type="button" class="btn dark btn-outline"
                                                                        data-dismiss="modal">Kapat
                                                                </button>
                                                                <button type="button" onclick="save_etiketler(this)" class="btn blue"
                                                                        style="background-color: #f5861d;border-color: #f5861d;"
                                                                        data-dismiss="modal">Kaydet
                                                                </button>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                </form>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>
                                            <a href="{site_url('products/edit')}/{$urun->id}" title="Detaylı Düzenle"
                                               class="btn btn-sm btn-default detayliduzenle">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a onclick="return confirm('Silmek istediğinizden emin misiniz?');"
                                               href="{site_url('products/remove')}/{$urun->id}" title="Sil"
                                               class="btn btn-sm btn-default sil">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                            {$this->pagination->create_links()}
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <script>

        </script>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<div class="container-fluid">
	<div class="page-content">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">
				<form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="javascript:void(0);">
					<div class="portlet" style="margin-top: -30px;">
						<div class="portlet-title">
							<div class="caption">
								<!-- <i class="fa fa-shopping-cart"></i> Taban & Yanak & Jant Çapı Ekleme  -->
							</div>
							<div class="actions btn-set">  
							</div>
						</div>
						<div class="portlet-body">
							
							<div class="form-body">
								<div class="form-group">
									<div class="col-md-2" id="kategorilerimiz">
										<select id="tip" name="tip" class="form-control form-filter input-sm select2me" required="required">
											<option value="taban">Taban</option>
											<option value="yanak">Yanak</option>
											<option value="jant_capi">Jant Çapı</option>
											<option value="kullanim_turu">Kullanım Türü</option>
										</select>
									</div>
									<div class="col-md-3" id="kategorilerimiz">
										<input type="text" id='veri' class="form-control" name="deger" placeholder="Taban Değerini Giriniz" required="required"> 
									</div>
								</div> 
								<div class="form-group">                                             
									<label class="col-md-2 control-label">  
										Kategori 
									</label> 
									<div class="col-md-3" id="kategorilerimiz">
										<select id="kategori" name="kategori" data-placeholder="Kategoriler" onchange="kategoriler(1, 'lastik', 'kategori')" class="form-control form-filter input-sm select2me">
											<option value="">Seçiniz</option>
											{foreach $kategoriler as $kategori}
											<option value="{$kategori->id}">{$kategori->adi}</option>
											{/foreach}
										</select>
										<div id="alt_kategori"></div>													
									</div>
								</div> 							
							</div> 
							<div class="form-body">
								<div class="form-group">
									<label class="col-md-2 control-label"></label>
									<div class="col-md-10" id="kategorilerimiz">
										<button type="button" class="btn blue" onclick="veri_ekle();">Kaydet</button> 
									</div>
								</div> 
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr> 
											<th style="width:5%;" id="tiptext"></th> 
											<th>Kategori</th>  
											<th style="width: 5%;"> </th>
										</tr>
									</thead>
									<tbody id="liste">  
										<tr><td colspan="3"> Kategori Seçiniz..</td></tr>
									</tbody>
								</table>
							</div> 
						</div>
					</div>
				</form>

			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
	</div>
	{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
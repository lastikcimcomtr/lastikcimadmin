{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Yorumlar </a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
							                <thead>
							                    <tr>                         
							                        <th class="islem_bilgilendirme" style="text-align:right;" align="right">
							                            
							                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Yorumu Göster veya Gösterme">
							                        		<i class=""></i>
							                    		</span>
							                            <span id="islembilgisitext"> Göster / Gösterme </span>

							                            <span href="#" title="Yorum Düzenle" class="btn btn-sm btn-default hizliduzenle">
							                                <i class="fa fa-pencil-square-o"></i>
							                            </span> 
							                            <span id="islembilgisitext"> Düzenle  </span>

							                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
							                                <i class="fa fa-times"></i>
							                            </span>
							                            <span id="islembilgisitext"> Sil </span>

							                        </th> 
							                    </tr>
							                </thead>
							            </table>

                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr> 
							                        <th width="1%"> ID </th>
							                        <th> Başlık </th> 
							                        <th> Ürün </th> 
							                        <th> Üye </th> 
							                        <th> Şehir </th>
							                        <th> Mail </th>
							                        <th> Evet </th>
							                        <th> Hayır </th>
							                        <th width="1%"> Tarih </th> 
							                        <th width="1%"> 
							                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Yorumu Göster veya Gösterme">
							                        		<i class=""></i>
							                    		</a>  
							                		</th> 
													<th width="1%"> 
														<a href="#" title="Yorumu Düzenle" class="btn btn-sm btn-default hizliduzenle">
							                                <i class="fa fa-pencil-square-o"></i>
							                            </a>                                              	
													</th>
													<th width="1%"> 
														<a href="#" title="Cevap Ver" class="btn btn-sm btn-default detayliduzenle">
							                                <i class="fa fa-comment"></i>
							                            </a>                                              	
													</th>
							                        <th width="1%">
							                        	<a href="#" title="Sil" class="btn btn-sm btn-default sil">
							                                <i class="fa fa-times"></i>
							                            </a> 
							                        </th> 
							                    </tr>
                                            </thead>
                                            <tbody>
                                                {if count($yorumlar) > 0}

                                                    {foreach $yorumlar as $yorum}
                                                        <tr>
                                                        	<td> {$yorum->id} </td>
                                                        	<td title="{$yorum->baslik}"> {truncate_str($yorum->baslik,50)} </td>
                                                        	<td> {$yorum->urun_adi} </td>
                                                        	<td title="{if $yorum->musteri}{truncate_str($yorum->musteri_isim|cat:" "|cat:$yorum->musteri_soyisim)}{else}{truncate_str($yorum->isim|cat:" "|cat:$yorum->soyisim)}{/if}"
															> {if $yorum->musteri}{truncate_str($yorum->musteri_isim|cat:" "|cat:$yorum->musteri_soyisim)}{else}{truncate_str($yorum->isim|cat:" "|cat:$yorum->soyisim)}{/if} </td>
                                                        	<td> {if $yorum->musteri}{$yorum->musteri_sehir}{else}{$yorum->sehir}{/if} </td>
                                                        	<td> {if $yorum->email}{$yorum->email}{else}-{/if}</td>
                                                        	<td> {$yorum->evet} </td>
                                                        	<td> {$yorum->hayir} </td>
                                                        	<td> {date('d.m.Y', $yorum->tarih)} </td>
                                                        	<td>
                                                        		<a onclick="durum_degis({$yorum->id}, 'yorumlar', '#yorumdurumu{$yorum->id}');" id="yorumdurumu{$yorum->id}" class="btn btn-circle btn-icon-only btn-default {if $yorum->durum == 1}durumu{else}beyaz{/if}" title="Göster / Gösterme">
                                                        			<i class=""></i>
                                                        		</a> 
                                                        	</td>
                                                        	<td>
                                                        		<a data-toggle="modal" href="#yorumduzenle_{$yorum->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                                        <i class="fa fa-pencil-square-o"></i>
			                                                    </a>                                                        
			                                                    <div class="modal fade" id="yorumduzenle_{$yorum->id}" tabindex="-1" role="basic" aria-hidden="true">
			                                                        <div class="modal-dialog">
			                                                            <div class="modal-content">
			                                                                <div class="modal-header">
			                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                                                    <h4 class="modal-title">Yorum Düzenleme</h4>
			                                                                </div>
			                                                                <div class="modal-body">
			                                                                    <div class="tab-pane" id="tab_kategori_ekle">
			                                                                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/comment/edit')}/{$yorum->id}" autocomplete="off">
			                                                                            
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label">Başlık :
			                                                                                    <span class="required"> * </span>
			                                                                                </label>
			                                                                                <div class="col-md-8" id="kategori_adi">
			                                                                                    <input type="text" class="form-control" name="baslik" placeholder="" value="{$yorum->baslik}">                                            
			                                                                                </div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4 control-label">Açıklama : 
			                                                                            		<span class="required"> * </span>
			                                                                            	</label>
			                                                                            	<div class="col-md-8">
			                                                                            		<textarea class="form-control" name="aciklama" rows="5">{$yorum->aciklama}</textarea>
			                                                                            	</div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4 control-label">Puan : 
			                                                                            		<span class="required"> * </span>
			                                                                            	</label>
			                                                                            	<div class="col-md-2">
			                                                                            		<select name="puan" class="form-control">
			                                                                            			{for $i=1 to 5}
			                                                                            			<option {if $yorum->puan == $i}selected{/if} value="{$i}">{$i}</option>
			                                                                            			{/for}
			                                                                            		</select>
			                                                                            	</div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label"></label>
			                                                                                <div class="col-md-8" id="kategorilerimiz">         
			                                                                                    <button type="submit" class="btn blue">Düzenle</button> 
			                                                                                </div>
			                                                                            </div>

			                                                                        </form>
			                                                                    </div> 
			                                                                </div> 
			                                                            </div>
			                                                            <!-- /.modal-content -->
			                                                        </div>
			                                                        <!-- /.modal-dialog -->
			                                                    </div> 
                                                        	</td>
															<td>
																<a  data-toggle="modal" href="#yorumcevap_{$yorum->id}" title="Cevap Ver" class="btn btn-sm btn-default detayliduzenle">
																	<i class="fa fa-comment"></i>
																</a>           
																
																<div class="modal fade" id="yorumcevap_{$yorum->id}" tabindex="-1" role="basic" aria-hidden="true">
			                                                        <div class="modal-dialog">
			                                                            <div class="modal-content">
			                                                                <div class="modal-header">
			                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                                                    <h4 class="modal-title">Yorum Cevap</h4>
			                                                                </div>
			                                                                <div class="modal-body">
			                                                                    <div class="tab-pane" id="tab_kategori_ekle">
			                                                                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/comment/answer')}/{$yorum->id}" autocomplete="off">
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4">Başlık :
			                                                                                </label>
			                                                                                <div class="col-md-8" id="kategori_adi">
																									{$yorum->baslik}																							
			                                                                                </div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Açıklama : 
			                                                                            	</label>
			                                                                            	<div class="col-md-8" style="white-space: normal; ">
			                                                                            		{$yorum->aciklama}
			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Puan : 
			                                                                            	</label>
			                                                                            	<div class="col-md-2">
																								{$yorum->puan }
			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Cevap : 
			                                                                            	</label>
			                                                                            	<div class="col-md-8">
			                                                                            		<textarea class="form-control" name="cevap" rows="5"></textarea>
			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label"></label>
			                                                                                <div class="col-md-8" id="kategorilerimiz">         
			                                                                                    <button type="submit" class="btn blue">Gönder</button> 
			                                                                                </div>
			                                                                            </div>

			                                                                        </form>
			                                                                    </div> 
			                                                                </div> 
			                                                            </div>
			                                                            <!-- /.modal-content -->
			                                                        </div>
			                                                        <!-- /.modal-dialog -->
			                                                    </div> 
															</td>
                                                        	<td>
                                                        		<a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('products/comment/remove')}/{$yorum->id}" title="Sil" class="btn btn-sm btn-default sil">
			                                                        <i class="fa fa-times-circle"></i>
			                                                    </a>
                                                        	</td>
                                                        </tr>
                                                    {/foreach}

                                                {else}
                                                <tr>
                                                    <td colspan='11' style='text-align:center;'> Ürünlere yapılmış bir yorum bulunamadı. </td>
                                                </tr>
                                                {/if}
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
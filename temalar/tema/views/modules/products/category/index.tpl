{include file="base/header.tpl"}


<style type="text/css">
.editable-input input {
    height: 34px;
    width: 100px!important;
}
</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Kategoriler</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{site_url('/')}">Anasayfa</a>
                </li>
                <li>
                    <a href="{site_url('products/index')}">Ürünler</a>
                </li>
                <li class="active">Kategoriler</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut kategorileri görebilir, Yeni kategori ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#kategori_ekle">
                            <i class="fa fa-plus"></i>  
                            Kategori Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Kategoriler {if $kategorisi} - {$kategorisi->adi}{/if}</a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="60%"> Kategori Adı </th>
                                                    <th width="1%"> Alt </th>
                                                    <th width="10%"> Kargo Ücretleri </th>
                                                    <th width="8%"> <center>Alt Kategoriler</center> </th>                                                    
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Kategori Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="8%"> <center>Ürünler</center> </th>
                                                    <th width="8%"> <center>Parametreler</center> </th>
                                                    <th width="8%"> <center>Vitrin Düzenle</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {if count($kategoriler) > 0}

                                                {foreach $kategoriler as $kategori}
                                                <tr>
                                                    <td>{$kategori->id}</td> 
                                                    <td align="center">{$kategori->sira}</td> 
                                                    <td>{$kategori->adi}</td>
                                                    <td>{$kategori->altkatsayisi}</td>
                                                    <td>                                                       
                                                        <a data-toggle="modal" href="#kategori_kargo_{$kategori->id}"  class="btn btn-sm red btn-outline sbold bizimbutonlar">Kargo Fiyatı Ayarla</a> 
                                                                <div class="modal fade" id="kategori_kargo_{$kategori->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">{$kategori->adi} Kategorisi Kargo Fiyatları</h4>
                                                                                </div>
                                                                            <table class="table table-striped table-hover table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="width:30%;">{if $kategori->tip == "lastik"}Jant Çapı{elseif  $kategori->tip == "jant"}Ebat{else}Ürün Tipi{/if}</th> 
                                                                                        <th style="width:30%;"> Fiyatı </th> 
                                                                                        <th style="width:1%;"> </th> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody> 

                                                                                    {foreach from=$this->category->kargofiyatlari($kategori->id) item=fiyat}
                                                                                        <tr class="parametre-1">
                                                                                            <td style="vertical-align: middle;">
                                                                                                {if $kategori->tip == "mini-stepne"}
                                                                                                {if $fiyat->deger == 1}Yol Seti{else}Mini Stepne{/if}
                                                                                                {else}
                                                                                                {$fiyat->deger}
                                                                                                {/if}                                                                                  
                                                                                            </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <a href="#" class="inlineedit" id="{$fiyat->deger}" data-type="number"  data-emptytext="0" data-pk="{$fiyat->veriler}" data-pk="{$fiyat->deger}" data-url="{site_url('ajax/kargofiyati/'|cat:$kategori->id)}" data-title="Fiyatı">
                                                                                             {$fiyat->fiyat}</a>
                                                                                            <i class="fa fa-try"></i> 
                                                                                        </td> 

                                                                                        <td style="vertical-align: middle;text-align: center;">
                                                                                           {*} <a style="margin-right:0px;background-color:#D93D5E;color:white;height:30px;padding:5px 7px;" onclick="prm_sil(1);" title="Sil" class="btn btn-sm btn-default">
                                                                                                <i class="fa fa-times-circle"></i>
                                                                                            </a>{*}
                                                                                        </td>  
                                                                                    </tr>
                                                                                    {/foreach}
                                                                                    
                                                                                </tbody>
                                                                        </table>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                                {*
                                                                {if $kategori->altkatsayisi > 0}
                                                                    <a href="{site_url('products/category/index')}/{$kategori->id}" class="btn btn-sm red btn-outline sbold bizimbutonlar">Alt Kategoriler</a>
                                                                {/if}
                                                                *}
                                                                <a href="{site_url('products/category/index')}/{$kategori->id}" class="btn btn-sm red btn-outline sbold bizimbutonlar">Alt Kategoriler</a>
                                                            </td>                                                            
                                                            <td>
                                                                <a onclick="durum_degis({$kategori->id},'kategoriler', '#kategoridurumu{$kategori->id}');" id="kategoridurumu{$kategori->id}" class="btn btn-circle btn-icon-only btn-default {if $kategori->durum == 1}durumu{else}beyaz{/if}" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#kategor_resmi_{$kategori->id}"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="kategor_resmi_{$kategori->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Kategori Resmi Ekleme ( 416 x 122)</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                             <form method="post" enctype="multipart/form-data" action="{site_url('products/category/editcategoryimage')}/{$kategori->id}">
                                                                              <div class="form-group">						                                                            
                                                                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 416px; height: 122px;">
                                                                                       {if $kategori->resim}
                                                                                       <img src="{$this->extraservices->fullresimurl('kategori_resim', $kategori->resim)}" alt="" />
                                                                                       {else}
                                                                                       <img src="http://www.placehold.it/416x122/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                       {/if}
                                                                                   </div>
                                                                                   <div>
                                                                                      <span class="btn red btn-outline btn-file">
                                                                                          <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                          <span class="fileinput-exists"> Değiştir </span>
                                                                                          <input type="file" name="resim"> </span>
                                                                                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                      </div>
                                                                                  </div>
                                                                              </div>
                                                                              <div class="form-group">
                                                                                <button type="submit" class="btn blue">Kaydet</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    </td>
                                                    <td align="center">                                                 
                                                        <a href="#urunler" class="btn red btn-outline sbold bizimbutonlar" title="Ürünler">
                                                            Ürünler
                                                        </a>
                                                    </td>     
                                                    <td align="center">   
                                                        {if $kategori->altkatsayisi == 0}                                              
                                                        <a data-toggle="modal" href="#parametreler_{$kategori->id}"  class="btn red btn-outline sbold bizimbutonlar" title="Parametreler">
                                                            Parametreler
                                                        </a>
                                                        <div class="modal" id="parametreler_{$kategori->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog  modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title">Parametre Düzenleme</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="tab-pane" id="tab_kategori_ekle">
                                                                            <form method="post" action="javascript:void(-1);" id="kpd_{$kategori->id}">
                                                                                <div class="tabbable-bordered">
                                                                                    <ul class="nav nav-tabs" id="tab_basliklari">
                                                                                        {$i=0}
                                                                                        {foreach from=$parametreler[$kategori->tip]  key=grupAdi item=parametreGrup name=pgrup}
                                                                                        <li class="{if $smarty.foreach.pgrup.first}active{/if}">
                                                                                            <a href="#tab_parametre_{$kategori->id}_{$i}" data-toggle="tab"> {$grupAdi} </a>
                                                                                        </li>
                                                                                        {$i=$i+1}
                                                                                        {/foreach}
                                                                                    </ul>      
                                                                                    <div class="tab-content">
                                                                                       {$i=0}
                                                                                       {foreach from=$parametreler[$kategori->tip] item=parametreGrup key=grupAdi name=pgrup}
                                                                                       <div class="tab-pane {if $smarty.foreach.pgrup.first}active{/if} row" id="tab_parametre_{$kategori->id}_{$i}"> 
                                                                                        {$i=$i+1}
                                                                                        {foreach from=$parametreGrup item=parametre}
                                                                                        <label class="col-md-4 control-label" style="text-align: left;"><input type="checkbox" {if strpos($parametre->categories, '|'|cat:$kategori->id|cat:'|') !== false} checked="true" {/if} name="parametre[]" value="{$parametre->id}"> {$parametre->adi}</label>
                                                                                        {/foreach}
                                                                                    </div>
                                                                                    {/foreach}                                                                                         
                                                                                </div>

                                                                            </div> 
                                                                            <div class="row">
                                                                            	<div class="col-md-12" style="text-align: right;margin-top: 10px;">
                                                                            		<button onclick="parametreDuzenle('kpd_{$kategori->id}' ,{$kategori->id})" class="btn blue">Düzenle</button>
                                                                            	</div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div> 
                                                    </div> 

                                                    {else}
                                                    -
                                                    {/if}
                                                </td>
                                                <td align="center">                                                 
                                                    <a href="#Vitrin" class="btn red btn-outline sbold bizimbutonlar" title="Vitrin Düzenle">
                                                        Vitrin Düzenle
                                                    </a>
                                                </td>
                                                <td>  
                                                    <a data-toggle="modal" href="#katduzenle_{$kategori->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>                                                        
                                                    <div class="modal fade" id="katduzenle_{$kategori->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title">Kategori Düzenleme</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="tab-pane" id="tab_kategori_ekle">
                                                                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/category/editcategory')}/{$kategori->id}">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Kategori Adı:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8" id="kategori_adi">
                                                                                    <input type="text" class="form-control" name="adi" placeholder="" value="{$kategori->adi}">                                            
                                                                                </div>
                                                                            </div>
                                                                            {if $kategori->tip}
                                                                            <input type="hidden" name="tip" value="{$kategori->tip}" />
                                                                            {else}
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">
                                                                                    Kategori Tipi
                                                                                </label>
                                                                                <div class="col-md-8">
                                                                                    <select name="tip" class="form-control" required>
                                                                                        <option value="">Seçiniz</option>
                                                                                        {foreach $tipler as $tip}
                                                                                        <option {if $kategori->tip == $tip->adi}selected{/if} value="{$tip->adi}">{$tip->adi}</option>
                                                                                        {/foreach}
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            {/if}                                                                                        
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Sıra No:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8" id="">
                                                                                    <input type="number" class="form-control" name="sira" min="1" value="{$kategori->sira}">                                            
                                                                                </div>
                                                                            </div>
                                                                            {if $kategori->altkatsayisi == 0}     
                                                                           <div class="form-group">
                                                                                <label class="col-md-4 control-label">Merchants Id
                                                                                </label>
                                                                                <div class="col-md-8" id="">
                                                                                    <input type="text" class="form-control" name="merchants_id"  value="{$kategori->merchants_id}">                                            
                                                                                </div>
                                                                            </div>
                                                                            {/if}

                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label"></label>
                                                                                <div class="col-md-8" id="kategorilerimiz">         
                                                                                    <button type="submit" class="btn blue">Düzenle</button> 
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div> 
                                                    <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('products/category/remove')}/{$kategori->id}" title="Sil" class="btn btn-sm btn-default sil">
                                                        <i class="fa fa-times-circle"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            {/foreach}
                                            {else}
                                            <tr>
                                                <td colspan='10' style='text-align:center;'> Bu Kategoriye Ait Alt Kategori Yok.</td>
                                            </tr>
                                            {/if}
                                        </tbody>
                                    </table>
                                </div> 
                            </div>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <!-- Kategori Ekle -->
    <div class="modal fade in" id="kategori_ekle" tabindex="-1" role="kategori_ekle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Kategori Ekle</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/category/addcategory')}">
                        <input type="hidden" name="ust" value="{$ust}" />
                        {if $kategorisi->tip}
                        <input type="hidden" name="tip" value="{$kategorisi->tip}" />
                        {else}
                        <div class="form-group">
                            <label class="col-md-4 control-label">
                                Kategori Tipi
                            </label>
                            <div class="col-md-8">
                                <select name="tip" class="form-control" required>
                                    <option value="">Seçiniz</option>
                                    {foreach $tipler as $tip}
                                    <option value="{$tip->adi}">{$tip->adi}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        {/if}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Kategori Adı:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="adi" placeholder="">                                            
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="col-md-4 control-label">Sıra No:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8" id="">
                                <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8" id="kategorilerimiz">         
                                <button type="submit" class="btn blue">Ekle</button> 
                            </div>
                        </div>
                    </form>
                </div> 
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Kategori Ekle -->
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.inlineedit').editable(); 
    });
</script>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<style type="text/css">


    .asagi{      
    height: 20px;
    padding: 0px 5px;
    background-color: #E73434;
    color: white;
    border-radius: 10px;
    padding-top: 1px;
    }

    .yukari{
            height: 20px;
    padding: 0px 5px;
    background-color: #3eb927;
    color: white;
    border-radius: 10px;
    padding-top: 1px;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12"> 
             <div class="portlet">

               {$siralamaparam['m.fiyat_grup'] = "Marka Grup"}
               {$siralamaparamdizi['m.fiyat_grup'] = '{"siralama_tip":"deger","siraorder":"desc","tavsiye":1,"var-yok":"Var Yok","deger":"Aynı Marka Gurubunda Olan","desc":{"var-yok":"En Üste","deger":"En Üste"},"asc":{"var-yok":"En Altta","deger":"En Altta"}}'}

               {$siralamaparam['marka_idler'] = "Marka id Sıralaması"}
               {$siralamaparamdizi['marka_idler'] = '{"siralama_tip_text":1, "siralama_tip":"1","siraorder":"asc","listeleme":1,"tavsiye":1,"var-yok":"Var Yok","deger":"deger","desc":{ "var-yok":"En Üste","deger":"En Üste"},"asc":{ "var-yok":"En Altta","deger":"En Altta"}}'}

               {$siralamaparam['kategori_idler'] = "Kategori id Sıralaması"}
               {$siralamaparamdizi['kategori_idler'] = '{"siralama_tip_text":1, "siralama_tip":"1","siraorder":"asc","listeleme":1,"marka":1,"tavsiye":1,"var-yok":"Var Yok","deger":"deger","desc":{ "var-yok":"En Üste","deger":"En Üste"},"asc":{ "var-yok":"En Altta","deger":"En Altta"}}'}

               {$siralamaparam['mevsim.yaz'] = "Mevsim (Yaz)"}
               {$siralamaparamdizi['mevsim.yaz'] = '{"siralama_tip":"var-yok","siraorder":"desc","listeleme":1,"tavsiye":1,"marka":1,"var-yok":"Mevsime Göre","deger":"Mevsime Göre","desc":{"var-yok":"Yaz","deger":"Yaz"},"asc":{"var-yok":"Yaz","deger":"Yaz"}}'}
               {$siralamaparam['mevsim.kis'] = "Mevsim (Kış)"}
               {$siralamaparamdizi['mevsim.kis'] = '{"siralama_tip":"var-yok","siraorder":"desc","listeleme":1,"tavsiye":1,"marka":1,"var-yok":"Mevsime Göre","deger":"Mevsime Göre","desc":{"var-yok":"Kış","deger":"Kış"},"asc":{"var-yok":"Kış","deger":"Kış"}}'}
               {$siralamaparam['mevsim.4m'] = "Mevsim (Dört Mevsim)"}
               {$siralamaparamdizi['mevsim.4m'] = '{"siralama_tip":"var-yok","siraorder":"desc","listeleme":1,"tavsiye":1,"marka":1,"var-yok":"Mevsime Göre","deger":"Mevsime Göre","desc":{"var-yok":"Dört Mevsim","deger":"Dört Mevsim"},"asc":{"var-yok":"Dört Mevsim","deger":"Dört Mevsim"}}'}

               {$siralamatip['var-yok'] = "Var Yok"}
               {$siralamatip['deger'] = "Değer"}



               {$siralamasira['desc'] = "Azalan Sıralama"}
               {$siralamasira['asc'] = "Artan Sıralama"}

               {foreach from=$this->products->siralamaparam() item=param}
               {if $param->silik_id == 'marka'}
               {$siralamaparam["m.sira"] = $param->adi}
               {$siralamaparamdizi["m.sira"] = $param->siralama_veri}
               {else}
               {$siralamaparam["u."|cat:$param->silik_id] = $param->adi}
               {$siralamaparamdizi["u."|cat:$param->silik_id] = $param->siralama_veri}
               {/if}

               {/foreach}
               <div class="portlet-body">
                <div class="tabbable-bordered"> 
                    <ul class="nav nav-tabs" id="tab_basliklari"> 

                        <li class="active"><a href="#listeleme" data-toggle="tab">Arama Sonuçları</a></li> 
                        <li><a href="#anasayfavitrini" data-toggle="tab">Ana Sayfa Vitrini</a></li> 
                        <li><a href="#markavitrini" data-toggle="tab">Marka Vitrini</a></li> 
                        <li ><a href="#tavsiye" data-toggle="tab">Tavsiye</a></li> 


                    </ul>
                    <div class="tab-content">                              
                     <div class="tab-pane active" id="listeleme">
                       <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th style="text-align:left;border-right:  0" align="left">

                                    <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#liste_parametre_ekle">
                                        <i class="fa fa-plus"></i>  
                                        Sıralama Parametresi Ekle
                                    </a>

                                </th> 
                                <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
                                    <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                                    <span id="islembilgisitext"> Sil </span>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th width="1%">Sıra </th>  
                                    <th>Parametre</th> 
                                    <th width="40%">Sıralama Verisi</th> 
                                    <th width="40%">Sıralama Şekli</th> 
                                    <th width="1%"></th>
                                </tr>
                            </thead>
                            <tbody>

                                {foreach from=$siralamalar item=siralama}
                                {if $siralama->tip == "listeleme"}
                                {$veriler = $siralamaparamdizi[{$siralama->silik_id}]|json_decode}
                                <tr>
                                    <td align="center"><a href="#" class="inlineedit" id="sira_{$siralama->id}" data-type="number"  data-emptytext="{$siralama->sira}" data-pk="sira" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                        {$siralama->sira}</a></td>
                                        <td>{$siralamaparam[{$siralama->silik_id}]}</td>
                                        <td>{if $veriler->{$siralama->siralama_tip}}{$veriler->{$siralama->siralama_tip}}{else}
                                            <a href="#" class="inlineedit" id="tip_{$siralama->id}" data-type="text"  data-emptytext="{$siralama->siralama_tip}" data-pk="siralama_tip" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                                {$siralama->siralama_tip}</a>

                                            {/if}</td>
                                            <td>{if $veriler->{$siralama->order}->{$siralama->siralama_tip}}{$veriler->{$siralama->order}->{$siralama->siralama_tip}}{else}id Sıralama{/if}</td>
                                            <td>  <a  href="#" onclick="psil({$siralama->id}); return false;" title="Sil" class="btn btn-sm btn-default sil">
                                                <i class="fa fa-times-circle"></i>
                                            </a>
                                        </td>   </tr> 
                                        {/if}
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>



                        </div>  

                        <div class="tab-pane" id="anasayfavitrini">
                            {$filtre['tip'] = "lastik"}
                            {$filtre['ust'] = "0"}
                            {$kategoriler =  $this->category->kategoriler($filtre)}
                            {$lastik = $this->products->anasayfasiralama()->lastik } 
                            <ul class="nav nav-tabs" id="tab_basliklari"> 
                                {foreach from=$kategoriler item=kategori name=lk}
                                {if count($lastik[$kategori->id]) > 0}
                                <li class="{if $smarty.foreach.lk.first} active{/if}"><a href="#kat_{$kategori->id}" data-toggle="tab">{$kategori->adi}</a></li>
                                {/if}
                                {/foreach}

                            </ul>
                            <div class="tab-content">  
                             {foreach from=$kategoriler item=kategori name=lk}
                             {if  count($lastik[$kategori->id]) > 0}
                             <div class="tab-pane{if $smarty.foreach.lk.first} active{/if}" id="kat_{$kategori->id}">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="1%">Sıra </th>  
                                                <th>Stok Kodu</th>  
                                                <th width="80%">Ürün Adı</th> 
                                                <th>Aşağı</th>
                                                <th>Yukarı</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="akat_{$kategori->id}">

                                           {foreach from=$lastik[$kategori->id] item=urun name=uk}

                                           <tr>
                                            <td align="center">
                                            {$smarty.foreach.uk.index +1}
                                        </td>
                                        <td>{$urun->stok_kodu}</td>
                                        <td>{$urun->urun_adi}</td>
                                        <td  style="text-align: center;">{if !$smarty.foreach.uk.last}
                                            <a  href="#" onclick="urunsirala('down',{$kategori->id},{$urun->id}); return false;" title="Aşağı"  class="btn btn-sm btn-default asagi">
                                                <i class="icon-arrow-down"></i>
                                            </a>{/if}
                                        </td>
                                        <td  style="text-align: center;">{if !$smarty.foreach.uk.first}
                                            <a  href="#" onclick="urunsirala('up',{$kategori->id},{$urun->id}); return false;" title="Yukarı"   class="btn btn-sm btn-default yukari">
                                                <i class="icon-arrow-up"></i>
                                            </a>{/if}
                                        </td>
                                   
                                    </tr> 

                                    {/foreach}  
                                </tbody>
                            </table>
                        </div> 
                    </div>
                    {/if}
                    {/foreach}
                </div>





            </div>  


            <div class="tab-pane" id="markavitrini">
               <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr style="width:50%;">   
                        <th style="text-align:left;border-right:  0" align="left">

                            <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#marka_parametre_ekle">
                                <i class="fa fa-plus"></i>  
                                Sıralama Parametresi Ekle
                            </a>

                        </th> 
                        <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
                            <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                            <span id="islembilgisitext"> Sil </span>
                        </th>
                    </tr>
                </thead>
            </table>
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr>
                            <th width="1%">Sıra </th>  
                            <th>Parametre</th> 
                            <th width="40%">Sıralama Verisi</th> 
                            <th width="40%">Sıralama Şekli</th> 
                            <th width="1%"></th>
                        </tr>
                    </thead>
                    <tbody>

                        {foreach from=$siralamalar item=siralama}
                        {if $siralama->tip == "marka"}
                        {$veriler = $siralamaparamdizi[{$siralama->silik_id}]|json_decode}
                        <tr>
                            <td align="center"><a href="#" class="inlineedit" id="sira_{$siralama->id}" data-type="number"  data-emptytext="{$siralama->sira}" data-pk="sira" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                {$siralama->sira}</a></td>
                                <td>{$siralamaparam[{$siralama->silik_id}]}</td>
                                <td>{if $veriler->{$siralama->siralama_tip}}{$veriler->{$siralama->siralama_tip}}{else}
                                    <a href="#" class="inlineedit" id="tip_{$siralama->id}" data-type="text"  data-emptytext="{$siralama->siralama_tip}" data-pk="siralama_tip" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                        {$siralama->siralama_tip}</a>

                                    {/if}</td>
                                    <td>{if $veriler->{$siralama->order}->{$siralama->siralama_tip}}{$veriler->{$siralama->order}->{$siralama->siralama_tip}}{else}id Sıralama{/if}</td>
                                    <td>  <a  href="#" onclick="psil({$siralama->id}); return false;" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times-circle"></i>
                                    </a>
                                </td>   </tr> 
                                {/if}
                                {/foreach}



                            </tbody>
                        </table>
                    </div>



                </div>  
                <div class="tab-pane" id="tavsiye">
                   <table class="table table-striped table-hover table-bordered">
                    <thead>
                        <tr style="width:50%;">   
                            <th style="text-align:left;border-right:  0" align="left">

                                <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#tavsiye_parametre_ekle">
                                    <i class="fa fa-plus"></i>  
                                    Sıralama Parametresi Ekle
                                </a>

                            </th> 
                            <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
                                <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                                <span id="islembilgisitext"> Sil </span>
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="1%">Sıra </th>  
                                <th>Parametre</th> 
                                <th width="40%">Sıralama Verisi</th> 
                                <th width="40%">Sıralama Şekli</th> 
                                <th width="1%"></th>
                            </tr>
                        </thead>
                        <tbody>

                            {foreach from=$siralamalar item=siralama}
                            {if $siralama->tip == "tavsiye"}
                            {$veriler = $siralamaparamdizi[{$siralama->silik_id}]|json_decode}

                            <tr>
                                <td align="center"><a href="#" class="inlineedit" id="sira_{$siralama->id}" data-type="number"  data-emptytext="{$siralama->sira}" data-pk="sira" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                    {$siralama->sira}</a></td>
                                    <td>{$siralamaparam[{$siralama->silik_id}]}</td>
                                    <td>{if $veriler->{$siralama->siralama_tip}}
                                       {$veriler->{$siralama->siralama_tip}}
                                       {else}
                                       <a href="#" class="inlineedit" id="tip_{$siralama->id}" data-type="text"  data-emptytext="{$siralama->siralama_tip}" data-pk="siralama_tip" data-url="http://admin.lastikcim.com.tr/ajax/siralamaedit/{$siralama->id}" data-title="Sıralaması">
                                        {$siralama->siralama_tip}</a>

                                    {/if}</td>
                                    <td>{if $veriler->{$siralama->order}->{$siralama->siralama_tip}}{$veriler->{$siralama->order}->{$siralama->siralama_tip}}{else}id Sıralama{/if}</td>
                                    <td>  <a  href="#" onclick="psil({$siralama->id}); return false;" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times-circle"></i>
                                    </a>
                                </td>   </tr> 
                                {/if}
                                {/foreach}



                            </tbody>
                        </table>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

</div>
</div> 




<div class="modal fade in" id="liste_parametre_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Sıralama Parametresi Ekle</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane" id="tab_kategori_ekle">
                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/siralama')}">
                    <input type="hidden" name="tip" value="listeleme">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Parametre:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <select name="silik_id" class="form-control" required="required" data-bolum="liste_parametre_ekle">
                               <option value="">Seçiniz</option>
                               {foreach from=$siralamaparam item=item key=key }      
                               {if strpos($siralamaparamdizi[{$key}], 'listeleme') != false}             
                               <option value="{$key}" data-dizi='{$siralamaparamdizi[{$key}]}'>{$item}</option>   
                               {/if}                    
                               {/foreach}                                                                             
                           </select>                                          
                       </div>
                   </div>
                   <div class="form-group siralama_tip"   style="display: none">
                    <label class="col-md-4 control-label">Siralama Verisi:
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                       <select name="siralama_tip" class="form-control"  data-bolum="liste_parametre_ekle">

                           <option value="">Seçiniz</option>
                           {foreach from=$siralamatip item=item key=key }                             
                           <option value="{$key}">{$item}</option>                       
                           {/foreach}                                                                             
                       </select>     
                       <input type="text" name="siralama_tip" class="form-control" placeholder="idlerin arasında virgül kullanın">                                            
                   </div>
               </div>
               <div class="form-group siraorder"   style="display: none">
                <label class="col-md-4 control-label">Siralama Şekli:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-8" >
                   <select name="order" class="form-control">
                       <option value="">Seçiniz</option>
                       {foreach from=$siralamasira item=item key=key }                             
                       <option value="{$key}">{$item}</option>                       
                       {/foreach}                                                                             
                   </select>                                               
               </div>
           </div>
           <div class="form-group">

            <label class="col-md-4 control-label">Sıralama:
                <span class="required"> * </span>
            </label>
            <div class="col-md-8" >
               <input type="number" class="form-control" name="sira" min="1"   required="required">                                             
           </div>
       </div>
       <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-8" id="kategorilerimiz">         
            <button type="submit" class="btn blue">Ekle</button> 
        </div>
    </div>
</form>
</div> 
</div> 
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<div class="modal fade in" id="marka_parametre_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Sıralama Parametresi Ekle</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane" id="tab_kategori_ekle">
                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/siralama')}">
                    <input type="hidden" name="tip" value="marka">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Parametre:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <select name="silik_id" class="form-control" required="required" data-bolum="marka_parametre_ekle">
                               <option value="">Seçiniz</option>
                               {foreach from=$siralamaparam item=item key=key } 
                               {if strpos($siralamaparamdizi[{$key}], 'marka') != false}                              
                               <option value="{$key}" data-dizi='{$siralamaparamdizi[{$key}]}'>{$item}</option>  
                               {/if}                     
                               {/foreach}                                                                             
                           </select>                                          
                       </div>
                   </div>
                  
                   <div class="form-group siralama_tip"   style="display: none">
                    <label class="col-md-4 control-label">Siralama Verisi:
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8">
                       <select name="siralama_tip" class="form-control"  data-bolum="marka_parametre_ekle">

                           <option value="">Seçiniz</option>
                           {foreach from=$siralamatip item=item key=key }                             
                           <option value="{$key}">{$item}</option>                       
                           {/foreach}                                                                             
                       </select>     
                       <input type="text" name="siralama_tip" class="form-control" placeholder="idlerin arasında virgül kullanın">                                            
                   </div>
               </div>
               <div class="form-group siraorder"   style="display: none">
                <label class="col-md-4 control-label">Siralama Şekli:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-8" >
                   <select name="order" class="form-control" >
                       <option value="">Seçiniz</option>
                       {foreach from=$siralamasira item=item key=key }                             
                       <option value="{$key}">{$item}</option>                       
                       {/foreach}                                                                             
                   </select>                                               
               </div>
           </div>
           <div class="form-group">

            <label class="col-md-4 control-label">Sıralama:
                <span class="required"> * </span>
            </label>
            <div class="col-md-8" >
               <input type="number" class="form-control" name="sira" min="1"   required="required">                                             
           </div>
       </div>
       <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-8" id="kategorilerimiz">         
            <button type="submit" class="btn blue">Ekle</button> 
        </div>
    </div>
</form>
</div> 
</div> 
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<div class="modal fade in" id="tavsiye_parametre_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Sıralama Parametresi Ekle</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane" id="tab_kategori_ekle">
                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('products/siralama')}">
                    <input type="hidden" name="tip" value="tavsiye">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Parametre:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <select name="silik_id" class="form-control" required="required" data-bolum="tavsiye_parametre_ekle">
                               <option value="">Seçiniz</option>
                               {foreach from=$siralamaparam item=item key=key }      
                               {if strpos($siralamaparamdizi[{$key}], 'tavsiye') != false}                         
                               <option value="{$key}" data-dizi='{$siralamaparamdizi[{$key}]}'>{$item}</option>
                               {/if}                       
                               {/foreach}                                                                             
                           </select>                                          
                       </div>
                   </div>
                   <div class="form-group siralama_tip"   style="display: none">
                    <label class="col-md-4 control-label">Siralama Verisi:
                        <span class="required"> * </span>
                    </label>
                    <div class="col-md-8" >
                       <select name="siralama_tip" class="form-control"  data-bolum="tavsiye_parametre_ekle" >
                           <option value="">Seçiniz</option>
                           {foreach from=$siralamatip item=item key=key }                             
                           <option value="{$key}">{$item}</option>                       
                           {/foreach}                                                                             
                       </select>     
                       <input type="text" name="siralama_tip" class="form-control" placeholder="idlerin arasında virgül kullanın">                                            
                   </div>
               </div>
               <div class="form-group siraorder"   style="display: none">
                <label class="col-md-4 control-label">Siralama Şekli:
                    <span class="required"> * </span>
                </label>
                <div class="col-md-8" >
                   <select name="order" class="form-control" >
                       <option value="">Seçiniz</option>
                       {foreach from=$siralamasira item=item key=key }                             
                       <option value="{$key}">{$item}</option>                       
                       {/foreach}                                                                             
                   </select>                                               
               </div>
           </div>
           <div class="form-group">

            <label class="col-md-4 control-label">Sıralama:
                <span class="required"> * </span>
            </label>
            <div class="col-md-8" >
               <input type="number" class="form-control" name="sira" min="1"   required="required">                                             
           </div>
       </div>
       <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-8" id="kategorilerimiz">         
            <button type="submit" class="btn blue">Ekle</button> 
        </div>
    </div>
</form>
</div> 
</div> 
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
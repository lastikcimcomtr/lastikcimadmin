{include file="base/header.tpl"}
<div class="container-fluid">
	<div class="page-content">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">

				<form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-shopping-cart"></i> Lastik Ekleme 
							</div>
						</div>
						<div class="portlet-body">
							{if $status && $message}
							<div class="note note-{$status}">
								<p> {$message} </p>
							</div>
							{/if}
							<div class="tabbable-bordered">
								<ul class="nav nav-tabs" id="tab_basliklari">
									{if $parametregruplari}
									{foreach from=$parametregruplari key=key item=item name=pgrup}
									<li {if $smarty.foreach.pgrup.first}class="active"{else}data-gcategories="{$item->categories}"  style="display:none"{/if}>
										<a href="#tab_parametre{$item->id}" data-toggle="tab"> {$item->grup_adi} </a>
									</li>
									{/foreach}
									{/if}
								</ul>
								<div class="tab-content">
									{if $parametregruplari}
									{foreach from=$parametregruplari key=key item=item name=pgrup}
									<div class="tab-pane {if $smarty.foreach.pgrup.first}active{/if}" id="tab_parametre{$item->id}">
										{if $item->parametreler}

										{foreach $item->parametreler as $ip}
										<div {if $ip->input_type <> "kategori"}data-categories="{$ip->categories}" style="display:none"{/if}  >
											
											{if $ip->input_type != ""}
											<div class="form-group" id="{if $ip->silik_id != ""}silinebilir_{$ip->silik_id}{/if}">
												{if $ip->input_type == "checkbox"}
												<label class="col-md-2 control-label" style="margin-top:-5px;">
													{else}
													<label class="col-md-2 control-label">
														{/if}
														{$ip->adi}:
														{if $ip->required == 1}<span class="required"> * </span>{/if}
													</label>
													{if $ip->input_type == "ckeditor" || $ip->input_type == "textbox"}
													{$boy = "6"}
													{elseif $ip->input_type == "resim"}
													{$boy = "10"}
													{elseif $ip->input_type == "url_duzenle"}
													{$boy = "4"}
													{elseif $ip->input_type == "seo_etiketle"}
													{$boy = "8"}
													{elseif $ip->input_type == "uyumlu_yazici"}
													{$boy = "8"}
													{elseif $ip->input_type == "kargo_bedeli"}
													{$boy = "3"}
													{elseif $ip->input_type == "text_dsb"}
													{$boy = "4"}
													{elseif $ip->input_type == "urun_adi"}
													{$boy = "4"}
													{elseif $ip->input_type == "fiyat"}
													{$boy = "3"}
													{else}
													{$boy = "2"}
													{/if}
													<div class="col-md-{$boy}">

														{$tipi = $ip->input_type}
														{if $tipi == "text"}
														<input type="text" id="{$ip->silik_id}" class="form-control{if $ip->required == 1} required_chk {/if}" name="parametreler[{$ip->silik_id}]" placeholder="" value="{$ip->varsayilan}" {if $ip->required == 1}required{/if} />
														{/if}

														{if $tipi == "text_dsb"}
														<input type="text" id="{$ip->silik_id}" readonly="readonly" class="form-control {if $ip->required == 1} required_chk {/if}" name="parametreler[{$ip->silik_id}]" placeholder="" value="{$ip->varsayilan}" {if $ip->required == 1}required{/if} />
														{/if}

														{if $tipi == "urun_adi"}																		
														<div class="row">
															<div class="col-md-9">
																<input type="text" id="{$ip->silik_id}" readonly="readonly" class="form-control {if $ip->required == 1} required_chk {/if}" name="parametreler[{$ip->silik_id}]" placeholder="" value="{$ip->varsayilan}" {if $ip->required == 1}required{/if} />
															</div>
															<div class="col-md-3">
																<input type="checkbox" onchange="urun_adi_disable();" class="form-control" id="urun_adi_otomatik" value="1" checked="">
																<label for="urun_adi_otomatik">Otomatik</label>
															</div>
														</div>
														{/if}

														{if $tipi == "kategori"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Kategoriler" onchange="kategoriler(1, '{$tip}', 'parametreler[{$ip->silik_id}]')" class="form-control form-filter input-sm select2me {if $ip->required == 1} required_chk {/if}" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $kategoriler as $kategori}
															<option value="{$kategori->id}" {if 1 == $kategori->id}selected{/if} >{$kategori->adi}</option>
															{/foreach}
														</select>
														<div id="alt_kategori"></div>
														{/if}

														{if $tipi == "marka"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Markalar" onchange="markalar(1, '{$tip}')" class="form-control select2-allow-clear form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $markalar as $marka}
															<option value="{$marka->id}">{$marka->adi}</option>
															{/foreach}
														</select>
														<div id="alt_marka"></div>
														{/if}

														{if $tipi == "mevsim"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" onchange="desenler('{$tip}');" data-placeholder="Mevsimler" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $mevsimler as $mevsim}
															<option value="{$mevsim->id}">{$mevsim->adi}</option>
															{/foreach}
														</select>
														{/if}

														{if $tipi == "desen"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Desenler" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
														</select>
														{/if}

														{if $tipi == "taban"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Tabanlar" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $tabanlar as $taban}
															<option value="{$taban->id}">{$taban->deger}</option>
															{/foreach}
														</select>
														{/if}

														{if $tipi == "yanak"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Yanaklar" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $yanaklar as $yanak}
															<option value="{$yanak->id}">{$yanak->deger}</option>
															{/foreach}
														</select>
														{/if}

														{if $tipi == "yapisi"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Yapısı" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															<option value="r">R</option>
															<option value="Zr">ZR</option>
															<option value="-">-</option>
														</select>
														{/if}

														{if $tipi == "tt_tl"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="TT TL" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															<option value="TT">TT</option>
															<option value="TL">TL</option>
														</select>
														{/if}

														{if $tipi == "on_arka"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Ön / Arka" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															<option value="on">Ön</option>
															<option value="arka">Arka</option>
															<option value="onarka">Ön + Arka</option>
														</select>
														{/if}

														{if $tipi == "motosiklet_turu"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Motosikler Türleri" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value="">Seçiniz</option>
															{foreach $motorsikleturleri as $motorsikleturu}
															<option value="{$motorsikleturu->id}">{$motorsikleturu->deger}</option>
															{/foreach}
														</select>
														{/if}

														{if $tipi == "jant_capi"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" data-placeholder="Jant Çapı" class="form-control form-filter input-sm select2me" {if $ip->required == 1}required{/if}>
															<option value=""> Seçiniz </option>
															{foreach $jantcaplari as $jantcapi}
															<option value="{$jantcapi->id}">{$jantcapi->deger}</option>
															{/foreach}
														</select>
														{/if}

														{if $tipi == "run_flat"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" {if $ip->required == 1}required{/if}>
															<option data-rft = "" value=""> Yok </option>
															<option value="RFT" {if $urun->{$ip->silik_id} == "RFT"}selected{/if}>RFT</option>
															<option value="ROF" {if $urun->{$ip->silik_id} == "ROF"}selected{/if}>ROF</option>
															<option value="SSR" {if $urun->{$ip->silik_id} == "SSR"}selected{/if}>SSR</option>
															<option value="ZP" {if $urun->{$ip->silik_id} == "ZP"}selected{/if}>ZP</option>
															<option value="HRS" {if $urun->{$ip->silik_id} == "HRS"}selected{/if}>HRS</option>
															<option value="EXT" {if $urun->{$ip->silik_id} == "EXT"}selected{/if}>EXT</option>
															<option value="RunFlat" {if $urun->{$ip->silik_id} == "RunFlat"}selected{/if}>RunFlat</option>
															<option value="P-RFT" {if $urun->{$ip->silik_id} == "P-RFT"}selected{/if}>P-RFT</option>
														</select>
														{/if}

														{if $tipi == "xl_rf"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" {if $ip->required == 1}required{/if}>
															<option value=""> Yok </option>
															<option value="XL">XL</option>
															<option value="RF">RF</option>
														</select>
														{/if}

														{if $tipi == "checkbox"}
														<input type="checkbox" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" value="1" checked {if $ip->required == 1}required{/if}/>
														{/if}

														{if $tipi == "durum"}
														<select style="height:34px;" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm" {if $ip->required == 1}required{/if}>
															<option value="1" selected="">Aktif</option>
															<option value="0">Pasif</option>
														</select>
														{/if}

														{if $tipi == "vitrin"}
														<select style="height:34px;" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm" {if $ip->required == 1}required{/if}>
															<option value="1">Evet</option>
															<option value="0" selected="selected">Hayır</option>
														</select>
														{/if}

														{if $tipi == "textbox"}
														<textarea class="form-control" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" rows="6" {if $ip->required == 1} required {/if}></textarea>
														{/if}

														{if $tipi == "ckeditor"}
														<textarea class="ckeditor form-control" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" rows="6" {if $ip->required == 1} required {/if}>{literal}{desen_aciklama}{/literal}</textarea>
														{/if}

														{if $tipi == "m_s"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" {if $ip->required == 1}required{/if}>
															<option value=""> Seçiniz </option>
															<option value="var"> Var </option>
															<option selected value="yok"> Yok </option>
														</select>
														{/if}

														{if $tipi == "var_yok"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" {if $ip->required == 1}required{/if}>
															<option value=""> Seçiniz </option>
															<option value="var"> Var </option>
															<option value="yok"> Yok </option>
														</select>
														{/if}

														{if $tipi == "seal"}
														<select id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" {if $ip->required == 1}required{/if}>
															<option value=""> Seçiniz </option>
															<option value="ContiSeal" {if $urun->{$ip->silik_id} == "ContiSeal"}selected{/if}>ContiSeal</option>
															<option value="Seal" {if $urun->{$ip->silik_id} == "Seal"}selected{/if}>Seal</option>
															<option value="s-i" {if $urun->{$ip->silik_id} == "s-i"}selected{/if}>S-I</option>
															<option value="SealGuard" {if $urun->{$ip->silik_id} == "SealGuard"}selected{/if}>Seal Guard</option>
															<option value="SelfSeal" {if $urun->{$ip->silik_id} == "SelfSeal"}selected{/if}>SelfSeal</option>
														</select>
														{/if}

														{if $tipi == "fiyat"}
														<div class="row">
															<div class="col-md-8">
																<input type="text" class="form-control {if $ip->required == 1} required_chk {/if}" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" placeholder="" value="{$ip->varsayilan}" {if $ip->required == 1}required{/if} />
															</div>
															<div class="col-md-4">
																<select name="parametreler[{$ip->silik_id}_birim]" class="form-control" {if $ip->required == 1}required{/if}>
																	<option value=""> Seçiniz </option>
																	{foreach $kurlar as $kur}
																	<option value="{$kur->kod}" {if $kur->kod == "TRY"}selected{/if}>{$kur->kod}</option>
																	{/foreach}
																</select>
															</div>
														</div>
														{/if}

														{*if $tipi == "kargo_bedeli"}
														<div class="row">
															<div class="col-md-3">
																<input type="checkbox" onchange="kargo_disable();" class="form-control" id="kargo_sistem" value="1" checked="">
																<label for="kargo_sistem">
																	Sistem
																</label>
															</div>
															<div class="col-md-5">
																<input type="text" class="form-control {if $ip->required == 1} required_chk {/if}" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" placeholder="" value="{$ip->varsayilan}" readonly="readonly" {if $ip->required == 1}required{/if} />
															</div>
															<div class="col-md-4">
																<select name="parametreler[{$ip->silik_id}_birim]" id="kargo_bedeli_birim" class="form-control" disabled="disabled" {if $ip->required == 1}required{/if}>
																	<option value=""> Seçiniz </option>
																	{foreach $kurlar as $kur}
																	<option value="{$kur->kod}" {if $kur->kod == "TRY"}selected{/if}>{$kur->kod}</option>
																	{/foreach}
																</select>
															</div>
														</div>
														{/if*}

														{if $tipi == "resim"}
														<input type="hidden" id="{$ip->silik_id}" name="parametreler[{$ip->silik_id}]" value="" />

														<div class="fileinput fileinput-new" data-provides="fileinput" style="float:left;">
															<div class="input-group input-large">
																<div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
																	<i class="fa fa-file fileinput-exists"></i>&nbsp;
																	<span class="fileinput-filename"> </span>
																</div>
																<span class="input-group-addon btn default btn-file">
																	<span class="fileinput-new"> Resim Seçin </span>
																	<span class="fileinput-exists"> Değiştir </span>
																	<input type="file" name="resim_{$ip->silik_id}" id="{$ip->silik_id}" {if $ip->required == 1}required{/if}>
																</span>
																<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
															</div>
														</div>
														<div style="float:left;margin-left:10px;">
															<input style="width: 342px; float:left;" type="text" class="form-control" name="parametreler[{$ip->silik_id}_baslik]" placeholder="Resim Başlığı" >
															<input style="float: left; margin-left: 10px; width: 342px;" type="text" class="form-control" name="parametreler[{$ip->silik_id}_aciklama]" placeholder="Resim Alt Etiket" >
														</div>
														{/if}

														{if $tipi == "url_duzenle"}
														<span style="width:auto;float:left;padding-top: 6px;">https://www.lastikcim.com.tr/</span>
														<span style="width:auto;float:left;line-height: 30px;padding: 0 10px 0 5px;" id="url_duzenle_inpt">{$ip->varsayilan}<input type="text" class="form-control" name="parametreler[url_duzenle]" placeholder="" /></span>
														<button type="button" style="float:left;display:none;" id="seo_url_kaydetme_button"  onclick="inputu_gizle('parametreler[{$ip->silik_id}]');" class="btn default">Kaydet</button>
														<button type="button" style="float:left;" id="seo_url_duzenleme_button" class="btn blue" onclick="inputyap('parametreler[{$ip->silik_id}]','{$ip->varsayilan}')">Düzenle</button>

														<script>
															function inputyap(name,value){
																$("#url_duzenle_inpt").html('<input type="text" class="form-control" name="'+name+'" placeholder="" value="'+value+'" />');
																$("#url_duzenle_inpt").attr('onclick','');
																$("#seo_url_kaydetme_button").show();
																$("#seo_url_duzenleme_button").hide();
															}

															function inputu_gizle(name){ 
																var deger = $("input[name='"+name+"']").val();
																if(deger != ""){
																	$("#url_duzenle_inpt").html('<input type="hidden" class="form-control" name="'+name+'" placeholder="" value="'+deger+'">'+deger);
																	$("#seo_url_duzenleme_button").attr('onclick','inputyap("'+name+'","'+deger+'")');
																	$("#seo_url_kaydetme_button").hide();
																	$("#seo_url_duzenleme_button").show();
																}else{
																	alert("Url Düzenleme Alanı Boş Geçilemez..");
																}
															}

														</script>
														{/if}
													</div>
												</div>
												{/if}
											</div>
											{/foreach}
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-2 control-label"></label>
													<div class="col-md-10">
														<button type="submit" class="btn blue">Kaydet</button>
														{if !$smarty.foreach.pgrup.last}
														<button type="button" onclick="sonraki_tab({$item->id});" class="btn default">Devam Et</button>
														{/if}
													</div>
												</div> 
											</div>
											{/if}
										</div>
										{/foreach}
										{/if}
									</div>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
			<!-- END PAGE BASE CONTENT -->
		</div>
		{include file="base/footer_txt.tpl"}
	</div>
	{include file="base/quicksidebar.tpl"}
	{include file="base/footer.tpl"}
{include file="base/header.tpl"}
<style type="text/css">
  .anketcevaplar .secenek{
  width: 40%;
  float: left;
  text-align: right;
  padding-right: 10px;
}


.anketcevaplar .oran{
  width: 60%;
  float: right;
}
.anketcevaplar .oran>p{
 height: 20px; 
 float: left;
  background: #0e76bc;
      margin-right: 10px;
}
.anketcevaplar>div{
    height: 30px;
    margin-top: 10px;
 }

</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th style="text-align:left;border-right:  0" align="left">

                                    <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#soru_ekle">
                                        <i class="fa fa-plus"></i>  
                                        Anket Ekle  
                                    </a>

                                </th> 
                                <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
      <span class="btn btn-circle btn-icon-only btn-default" style="width: 18px;height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#990066;" title="Durum Değiştir">
                                        <i class=""></i>
                                    </span>
                                    <span id="islembilgisitext"> Durumu </span>
                                  <span class="btn btn-sm btn-default hizliduzenle"><i class="fa fa-pencil"></i></span>      
                                  <span id="islembilgisitext"> İncele </span>
                                  <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                                  <span id="islembilgisitext"> Sil </span>
                              </th>
                          </tr>
                      </thead>
                  </table>

                  <div class="portlet-body">
                    <div class="tabbable-bordered">
                       <div class="tab-content">                               
                        <div class="tab-pane active" id="tab_kategoriler">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="1%">Sıra</th>
                                            <th width="72%">Soru</th> 
                                            <th width="10%">Tarhi</th>  
                                            <th width="10%">Cevaplar</th> 
                                            <th width="1%"></th>
                                            <th width="1%"></th>
                                            <th width="1%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if count($anketler) > 0}

                                        {foreach $anketler as $anket}
                                        <tr>
                                            <td style="text-align: center;">{$anket->sira}</td> 
                                            <td>{$anket->soru}</td> 
                                            <td>{date('d m Y H:s',$anket->tarih)}</td> 
                                            <td>{$cevaplar = $this->anket->cevaplar($anket->id)}
{$toplamcevap =  0}  
{foreach from=$cevaplar item=cevap}
{$toplamcevap =  $toplamcevap + $cevap->oy}    
{/foreach}
<a data-toggle="modal" href="#cevap_{$anket->id}" title="cevaplar" class="btn btn-sm btn-default " style="width: 80%;">
                                                   {$toplamcevap } 
                                                </a>  
  <div class="modal fade" id="cevap_{$anket->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">Cevaplar</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="tab-pane anketcevaplar">
                                                                    {foreach from=$cevaplar item=cevap}
                                                                        {$yuzde = ($cevap->oy * 100) / $toplamcevap }
                                                                              {$gosterimyuzde = ($cevap->oy * 80) / $cevaplar[0]->oy}
                                                                    <div><span class='secenek'>{$cevap->secenek} {$cevap->oy} Oy</span><span class='oran'><p style='width:{$gosterimyuzde}%'></p> %{$yuzde|ceil}</span></div>
                                                                    {/foreach}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                            </td> 
                                            <td>
                                                                <a onclick="durum_degis({$anket->id},'anketler', '#anketdurumu{$anket->id}');" id="anketdurumu{$anket->id}" class="btn btn-circle btn-icon-only btn-default {if $anket->durum == 1}durumu{else}beyaz{/if}" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                            <td>  
                                                <a data-toggle="modal" href="#soruduzenle_{$anket->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                                    <i class="fa fa-pencil"></i>
                                                </a>                                                        
                                                <div class="modal fade" id="soruduzenle_{$anket->id}" tabindex="-1" role="basic" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">Soru Düzenleme</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="tab-pane" id="tab_kategori_ekle">
                                                                    <form class="form-horizontal form-row-seperated" method="post" action="{site_url('anket/edit')}">
                                                                        <input type="hidden"  name="id" value="{$anket->id}">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Soru:
                                                                                <span class="required"> * </span>
                                                                            </label>
                                                                            <div class="col-md-9">
                                                                                <input type="text" class="form-control" name="soru" value="{$anket->soru}">
                                                                            </div>
                                                                        </div>                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Sıra:
                                                                                <span class="required"> * </span>
                                                                            </label>
                                                                            <div class="col-md-9">
                                                                                <input type="text" class="form-control" name="sira" value="{$anket->sira}">
                                                                            </div>
                                                                        </div>
                                                                        {$secenekler = $this->anket->secenekler($anket->id)}


                                                                        <div class="form-group">
                                                                            <label class="col-md-10 control-label">
                                                                            </label> 
                                                                            <label class="col-md-2 control-label" style="text-align: center;">Başlangıç
                                                                            </label>

                                                                        </div>
                                                                        {foreach from=$secenekler  item=secenek name=foo}

                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Cevap {$smarty.foreach.foo.index + 1}
                                                                            </label>
                                                                            <div class="col-md-7">
                                                                                <input type="text" class="form-control" name="secenek[{$secenek->id}]"  value="{$secenek->secenek}">                                            
                                                                            </div>

                                                                            <div class="col-md-2">
                                                                                <input type="text" class="form-control" name="baslangic[{$secenek->id}]"  value="{$secenek->baslangic}">                                            
                                                                            </div>
                                                                        </div>
                                                                        {/foreach}
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">cevap yeni:
                                                                            </label>
                                                                            <div class="col-md-7" >
                                                                                <input type="text" class="form-control" name="ekle[]">
                                                                            </div>
                                                                        </div>                    <div class="form-group">
                                                                            <label class="col-md-3 control-label">Cevap yeni2:
                                                                            </label>
                                                                            <div class="col-md-7" >
                                                                                <input type="text" class="form-control" name="ekle[]">
                                                                            </div>
                                                                        </div>


                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"></label>
                                                                            <div class="col-md-7">         
                                                                                <button type="submit" class="btn blue">Düzenle</button> 
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div> </td>
                                                <td>
                                                <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('anket/remove')}/{$anket->id}" title="Sil" class="btn btn-sm btn-default sil">
                                                    <i class="fa fa-times-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        {/foreach}
                                        {else}
                                        <tr>
                                            <td colspan='10' style='text-align:center;'> Anket Yok.</td>
                                        </tr>
                                        {/if}
                                    </tbody>
                                </table>
                            </div> 
                        </div>                             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<div class="modal fade in" id="soru_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Soru Ekle</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane" id="tab_kategori_ekle">
                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('anket/add')}">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Soru:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="soru" placeholder=""  required="required">                                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 1:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder=""  required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 2:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder=""  required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 3:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 4:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 5:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 6:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 7:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Cevap 8:
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="secenek[]" placeholder="">
                        </div>
                    </div>
                   
                    

                    <div class="form-group">
                        <label class="col-md-4 control-label"></label>
                        <div class="col-md-8" id="kategorilerimiz">         
                            <button type="submit" class="btn blue">Ekle</button> 
                        </div>
                    </div>
                </form>
            </div> 
        </div> 
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
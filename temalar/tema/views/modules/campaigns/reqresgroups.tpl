{include file="base/header.tpl"}
{$module = $this->router->fetch_class()}
{$method = $this->router->fetch_method()}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
				{if $method == 'reqgroups'}
					{$var_add = 'type=1'}
				{else}
					{$var_add = 'type=2'}
				{/if}
                    <a href="{site_url('campaigns/addresreqgroup?')}{$var_add}" class="btn red btn-outline sbold"> 
                    	<i class="fa fa-plus"></i> 
                    	Koşul/Sonuç Grubu Ekle  
                	</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Grup Adı </th>
                                <th> Koşul İlişkisi </th>
                                <th> Tip </th>
                                <th width="1%"> 
                                    <a href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th width="1%">
                                    <a href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {if count($reqgroups) > 0}
                                {foreach $reqgroups as $reqgroup}
                                    {if $reqgroup->req_res == 'req'}
                                        {$type = 'Requirement Group'}
                                    {else}
                                        {$type = 'Result Group'}
                                    {/if}
                                <tr> 
                                    <td> {$reqgroup->id} </td>
                                    <td> {$reqgroup->group_name}</td>
                                    <td> {$reqgroup->group_type} </td>
                                    <td> {$type} </td>
                                    <td> <a href="{site_url('campaigns/editreqresgroup')}/{$reqgroup->id}" title="Düzenle" class="btn btn-sm btn-default hizliduzenle"> <i class="fa fa-pencil-square-o"></i> </a> </td>
                                    <td> <a onclick="return confirm('Kullanıcıyı silmek istediğinize emin misiniz?');" href="{site_url('campaigns/removegiftvoucher')}/{$reqgroup->id}" title="Sil" class="btn btn-sm btn-default sil"> <i class="fa fa-times"></i> </a> </td>
                                </tr>
                                {/foreach}
                            {else}
                            <tr>
                                <td colspan="14" align="center">Kayıtlı Grup Bulunamadı</td>
                            </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
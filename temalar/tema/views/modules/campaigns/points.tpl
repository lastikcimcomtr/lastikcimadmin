{include file="base/header.tpl"}

{$puan_sistemi_array['name'] = 'puan_sistemi_kullanimi'}
{$puan_sistemi = $this->extraservices->ayarlar($puan_sistemi_array, TRUE)}

{$hediye_ceki_array['name'] = 'hediye_ceki_kullanimi'}
{$hediye_ceki = $this->extraservices->ayarlar($hediye_ceki_array, TRUE)}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
		    <div class="col-md-12">
		    	{if $puan_sistemi->value == 0 || $hediye_ceki->value == 0}
		    	<div class="note note-danger">
				    <p> Puan Sistemi Kullanımı Şuan Pasif Görünüyor. Bu işlemi yapmadan önce <a href="{site_url('settings/group/8')}">Ek Ayarlar</a> kısmından Puan Sistemi Kullanımını ve Hediye Çeki Kullanımını Aktif Etmeniz Gerekmektedir. </p>
				</div>
		    	{/if}

		    	{if $status && $message}
		    	<div class="note note-{$status}">
				    <p> {$message} </p>
				</div>
				{/if}
		    	<form class="form-horizontal form-row-seperated" method="post" action="">
		    		<div class="form-body">
				        
						{foreach $ayarlar as $ayar}
						<div class="form-group">
							<label class="col-md-3 control-label">{$ayar->title} : </label>
							{if $ayar->name == "puan_degeri"}
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="1 TL" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control text-center" disabled="disabled" placeholder="=" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
								</div>
							{elseif $ayar->name == "en_az_hediye_ceki_puani"}
								<div class="col-md-1">
									<input type="text" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="TL" value="" />
								</div>
								<div class="col-md-1" >
									<input type="text" class="form-control" disabled="disabled" placeholder="10000" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="Puan" value="" />
								</div>
							{elseif $ayar->name == "hediye_ceki_kullanim_siniri"}
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="KDV Hariç" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
								</div>

								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled"  placeholder="TL" value="" />
								</div>
							{elseif $ayar->name == "yorum_puani"}
								<div class="col-md-1" style="padding-right: 0px;">
									<div class="checkbox col-md-2">
										<input type="checkbox" style="" id="sc" onclick="document.getElementById('gz').disabled = !this.checked"/>
									</div>
									<div class="col-md-10">
										<input  type="text" class="form-control" disabled="disabled" placeholder="Evet" value="" />
									</div>
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="Yorumlarda Kullan" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="=" value="" />
								</div>
								<div class="col-md-1">
									<input id="gz" disabled type="text" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="Puan" value="" />
								</div>
								<div class="col-md-1">
									<input type="text" class="form-control" disabled="disabled" placeholder="0.02 TL" value="" />
								</div>
							{elseif $ayar->name == "hediye_cekini_kullanabilen_markalar"}
								<div class="col-md-4">
					                <input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
					                <span class="help-block"> Seçmiş olduğunuz Marka ID numaralarını aralarına virgül koyarak yazınız.. </span>
					            </div>
							{elseif $ayar->name == "hediye_cekini_kullanamayan_markalar"}
								<div class="col-md-4">
					                <input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
					                <span class="help-block"> Seçmiş olduğunuz Marka ID numaralarını aralarına virgül koyarak yazınız.. </span>
					            </div>
							{elseif $ayar->name == "hediye_cekini_kullanabilen_kategoriler"}
								<div class="col-md-4">
					                <input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
					                <span class="help-block"> Seçmiş olduğunuz Kategori ID numaralarını aralarına virgül koyarak yazınız.. </span>
					            </div>
							{elseif $ayar->name == "hediye_cekini_kullanamayan_kategoriler"}
								<div class="col-md-4">
					                <input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
					                <span class="help-block"> Seçmiş olduğunuz Kategori ID numaralarını aralarına virgül koyarak yazınız.. </span>
					            </div>
							{else}
								<div class="col-md-1">
					                <input type="text" autocomplete="off" class="form-control" name="{$ayar->name}" placeholder="{$ayar->value}" value="{$ayar->value}" />
					            </div>
							{/if}
							{if $ayar->description}
								<div class="modal fade bs-modal-md" id="{$ayar->name}" tabindex="-1" role="dialog" aria-hidden="true">
			                        <div class="modal-dialog modal-md">
			                            <div class="modal-content">
			                            	<div class="modal-header">
			                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                    <h4 class="modal-title">{$ayar->title}</h4>
			                                </div>
			                                <div class="modal-body" style="line-height: 25px;"> {$ayar->description} </div>
			                                <div class="modal-footer">
			                                    <button type="button" class="btn btn-sm blue" data-dismiss="modal">Kapat</button>
			                                </div>
			                            </div>
			                            <!-- /.modal-content -->
			                        </div>
			                        <!-- /.modal-dialog -->
			                    </div>
			                    <!-- /.modal -->
				                <span class="col-md-3"> 
									<a data-toggle="modal" href="#{$ayar->name}" class="btn btn-circle btn-sm blue"><i class="fa fa-question"></i></a>
				                </span>
							{/if}
						</div>
						{/foreach}
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-3 control-label"></label>
				            <div class="col-md-9">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>


		        </form>
		    </div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
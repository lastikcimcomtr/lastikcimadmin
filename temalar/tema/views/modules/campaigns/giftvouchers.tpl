{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a href="{site_url('campaigns/addgiftvoucher')}" class="btn red btn-outline sbold"> 
                    	<i class="fa fa-plus"></i> 
                    	Hediye Çeki Ekle  
                	</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">                         
                                <th class="islem_bilgilendirme" style="text-align:right;" align="right">
                                    
                                    <span class="btn btn-circle btn-icon-only btn-default durumu" title="Durum Değiştir">
                                        <i class=""></i>
                                    </span>
                                    <span id="islembilgisitext"> Durumu </span>

                                    <span href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Düzenle  </span>

                                    <span href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th> 
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Çek Kodu </th>
                                <th> Değer </th>
                                <th> Kargo Bedava  </th>
                                <th> Kullanım Alışveriş Sınırı </th>
                                <th> Max Kullanım Sayısı </th> 
                                <th> Kişi Başı Kullanım Sınırı </th>
								<th> Tanımlı Sipariş</th>
                                <th> Başlangıç Tarihi </th>
                                <th> Bitiş Tarihi </th>
                                <th> Oluşturulma Tarihi </th>
                                <th> Oluşturan </th>
                                <th width="1%"> 
                                    <a class="btn btn-circle btn-icon-only btn-default durumu" title="Aktif veya Pasif">
                                        <i class=""></i>
                                    </a>  
                                </th> 
                                <th width="1%"> 
                                    <a href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th width="1%">
                                    <a href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {if count($hediyecekleri) > 0}
                                {foreach $hediyecekleri as $hediyeceki}
                                    {if $hediyeceki->dtip == 0}
                                        {$deger = $hediyeceki->dgr|cat:" TRY"}
                                    {else}
                                        {$deger = "% "|cat:$hediyeceki->dgr}
                                    {/if}

                                    {if $hediyeceki->kbd == 1}
                                        {$kargo = "Evet"}
                                    {else}
                                        {$kargo = "Hayır"}
                                    {/if}

                                    {$kim_yapti = $this->extraservices->fullisim($hediyeceki->usr)}
                                <tr> 
                                    <td> {$hediyeceki->id} </td>
                                    <td> {$hediyeceki->cek_kodu}</td>
                                    <td> {$deger} </td>
                                    <td> {$kargo} </td>
                                    <td> {$hediyeceki->asi} TL </td>
                                    <td> {$hediyeceki->mks} </td>
                                    <td> {$hediyeceki->kbks} </td>
									<td> <a href="/orders/index/id/{$hediyeceki->siparis_id}/" target="_blank">{$hediyeceki->siparis_id}</a> </td>
                                    <td> {date('d.m.Y', $hediyeceki->bast)} </td>
                                    <td> {date('d.m.Y', $hediyeceki->bitt)} </td>
                                    <td> {date('d.m.Y', $hediyeceki->zmn)} </td>
                                    <td> {$kim_yapti}</td>
                                    <td> 
                                        <a onclick="durum_degis({$hediyeceki->id}, 'hediye_cekleri', '#hediyecekidurumu{$hediyeceki->id}');" id="hediyecekidurumu{$hediyeceki->id}" class="btn btn-circle btn-icon-only btn-default {if $hediyeceki->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif">
                                            <i class=""></i>
                                        </a> 
                                    </td>
                                    <td> <a href="{site_url('campaigns/editgiftvoucher')}/{$hediyeceki->id}" title="Düzenle" class="btn btn-sm btn-default hizliduzenle"> <i class="fa fa-pencil-square-o"></i> </a> </td>
                                    <td> <a onclick="return confirm('Kullanıcıyı silmek istediğinize emin misiniz?');" href="{site_url('campaigns/removegiftvoucher')}/{$hediyeceki->id}" title="Sil" class="btn btn-sm btn-default sil"> <i class="fa fa-times"></i> </a> </td>
                                </tr>
                                {/foreach}
                            {else}
                            <tr>
                                <td colspan="14" align="center">Kayıtlı Hediye Çeki Bulunamadı</td>
                            </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
{$method = $this->router->fetch_method()}

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">
				{if $status && $message}
				<div class="note note-{$status}">
				    <p> {$message} </p>
				</div>
				{/if}
		        <form class="form-horizontal form-row-seperated" method="post" action="" autocomplete="off">
				    <div class="form-body">
							<input type="text" hidden="hidden" name="id" value="{$mst->id}"/>
							<div class="form-group">
								<label class="col-md-2 control-label">Kampanya Adı : <span class="required"> * </span></label>
								<div class="col-md-2">
										<input type="text" name="company_name" class="form-control req_chk" placeholder="Kampanya İsmi" value="{$mst->name}" />
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-2 control-label">Başlangıç Tarihi : <span class="required"> * </span></label>
								<div class="col-md-2">
									<div class="input-group date req_chk"  id='datetimepicker1'>
										<input type='text' class="form-control" name="start_date" value="{$mst->start_date}"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Bitiş Tarihi : <span class="required"> * </span></label>
								<div class="col-md-2">
									<div class="input-group date req_chk"  id='datetimepicker2'>
										<input type='text' class="form-control" name="end_date" value="{$mst->end_date}"/>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Sadece Üyeler</label>
								<div class="col-md-2">
									<select name="only_user" class="form-control select2me req_chk">
										<option value="0" {if !$mst->only_user}selected{/if}>Hayır</option>
										<option value="1" {if $mst->only_user}selected{/if}>Evet</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Kampanya Açıklama : <span class="required"> * </span></label>
								<div class="col-md-2">
									<textarea rows="5" class="form-control req_chk" name="description">{$mst->description}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Gereklilik Grupları : <span class="required"> * </span></label>
								<div class="col-md-2">
									<select name="req_groups[]" class="form-control select2me req_chk" multiple="">
										{foreach $reqgroups as $req_group}
											<option value="{$req_group->id}" {if in_array($req_group->id,$detail)}selected{/if}>{$req_group->group_name}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label">Sonuç Grupları : <span class="required"> * </span></label>
								<div class="col-md-2">
									<select name="res_groups[]" class="form-control select2me req_chk" multiple="">
										{foreach $resgroups as $res_group}
											<option value="{$res_group->id}" {if in_array($res_group->id,$detail)}selected{/if}>{$res_group->group_name}</option>
										{/foreach}
									</select>
								</div>
							</div>
							<div class="divider" style=""></div>
					
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="button" class="btn blue submit">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
	<script>
		$(function() {
		{literal}
		$('#datetimepicker1').datetimepicker({format: 'Y-MM-DD HH:mm:ss'});
        $('#datetimepicker2').datetimepicker({
			format: 'Y-MM-DD HH:mm:ss',
            useCurrent: false
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
        });
			
		{/literal}
			$('.btn.submit').click(function(){
				var form = $(this).closest('form');
				var data = form.serializeArray();
				//if(form.find('[name=req_groups[]').val())
				var req_groups = form.find("[name^='req_groups']");
				var res_groups = form.find("[name^='res_groups']");
				var description = form.find("[name='description']");
				var company_name = form.find("[name='company_name']");
				var only_user = form.find("[name='only_user']");
				var start_date = form.find("[name='start_date']");
				var end_date = form.find("[name='end_date']");
				if(req_groups.val() == null){
					req_groups.focus();
					return false;
				}else if(res_groups.val() == null){
					res_groups.focus();
					return false;
				}else if(description.val().length < 5){
					description.focus();
					return false;
				}else if(company_name.val().length < 5){
					company_name.focus();
					return false;
				}else if(end_date.val().length < 5){
					end_date.focus();
					return false;
				}else if(start_date.val().length < 5){
					start_date.focus();
					return false;
				}
				{literal}
				$.ajax({
					type: "POST",
					url: form.attr('action'),
					data: form.serialize(),
					dataType: 'json',
					success: function(data) {
						if(data.status == 'success'){
							swal(data.message);
						}
					}
				});
				{/literal}
			});
		});
	</script>
	<style>
		.krsl > div.checker > span{
			background-color:#ff0000
		}
		.krsl > div.checker > span.checked{
			background-color:#3598dc
		}
		.form-group{
			margin-bottom:1em;
		}
		.divider{
			margin-bottom:2em;border-bottom: 1px solid #eee;
		}
	</style>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
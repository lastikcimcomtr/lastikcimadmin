{include file="base/header.tpl"}
{$method = $this->router->fetch_method()}

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">
				{if $status && $message}
				<div class="note note-{$status}">
				    <p> {$message} </p>
				</div>
				{/if}
		        <form class="form-horizontal form-row-seperated" method="post" action="" autocomplete="off">
				    <div class="form-body">
						
							<div class="form-group">
								<label class="col-md-2 control-label">Koşul Grubu Adı : </label>
								<div class="col-md-2">
										<input type="text" name="group_name" class="form-control" placeholder="Grup İsmi" value="{$group->group_name}" />
								</div>
							</div>
							<input hidden="hidden" name="id" value="{$group->id}"/>
							<div class="form-group">
								<label class="col-md-2 control-label">Koşul Grubu Tipi : </label>
								<div class="col-md-2">
									<select name="group_type" class="form-control">
											<option {if $group->group_type=='AND'}selected{/if} value="AND">AND</option>
											<option {if $group->group_type=='OR'}selected{/if} value="OR">OR</option>
									</select>
								</div>
							</div>
							{foreach $detail as $cmp_item}
								
								
							<div class="form-group item">

								<label class="col-md-2 control-label">{$cmp_item->kosul_ad} : </label>
								<div class="col-md-2">
									{if $cmp_item->var_1_type=='pd_int'}
										<input type="number" name="param_1" class="form-control" placeholder="xx Değer Girin" value="{$cmp_item->param_1}" />
									{elseif $cmp_item->var_1_type=='pd_float'}
										<input type="number" name="param_1" class="form-control" placeholder="xx.xx Değer Girin"  value="{$cmp_item->param_1}"  />
									{elseif $cmp_item->var_1_type=='pd_checkbox'}
										<input type="checkbox" name="param_1" class="form-control" placeholder="Değer Girin"  value="{$cmp_item->param_1}"  />
									{elseif $cmp_item->var_1_type=='pd_text'}
										<input type="text" name="param_1" class="form-control" placeholder="Değer Girin"  value="{$cmp_item->param_1}"  />
									{elseif $cmp_item->var_1_type=='pd_select'}
										{$par_1_types = $cmp_item->par_1_type|json_decode}
										<select name="param_1" class="form-control">
											{foreach $par_1_types as $par_1_option}
												<option value="{$par_1_option}" {if $par_1_option==$cmp_item->param_1}selected{/if}>{$par_1_option}</option>
											{/foreach}
										</select>
									{else}
										Not Available!
									{/if}
								</div>
								<div class="col-md-2">
									{if $cmp_item->var_2_type=='pd_int'}
										<input type="number" name="param_2" class="form-control" placeholder="xx Değer Girin" value="{$cmp_item->param_2}" />
									{elseif $cmp_item->var_2_type=='pd_float'}
										<input type="number" name="param_2" class="form-control" placeholder="xx.xx Değer Girin" value="{$cmp_item->param_2}" />
									{elseif $cmp_item->var_2_type=='pd_checkbox'}
										<input type="checkbox" name="param_2" class="form-control" placeholder="Değer Girin" value="{$cmp_item->param_2}" />
									{elseif $cmp_item->var_2_type=='pd_select'}
										{$par_2_types = $cmp_item->par_2_type|json_decode}
										<select name="param_2" class="form-control">
											{foreach $par_2_types as $par_2_option}
												<option value="{$par_2_option}" {if $par_2_option==$cmp_item->param_2}selected{/if}>{$par_2_option}</option>
											{/foreach}
										</select>
									{else}
										-
									{/if}
								</div>
								
								<div class="col-md-2">
									<button type="button" class="btn btn-sm red delete_row"><i class="fa fa-minus"></i></button>
								</div>
								<div class="col-md-1 krsl">
											Karşıla <input type="checkbox" name="exl" class="form-control"  {if $cmp_item->exl}checked{/if}  />
								</div>
								<div class="form-group hidden">
											<input type="text" name="cmp_item_id" class="form-control" value="{$cmp_item->item_id}"  />
								</div>
							</div>		
							{/foreach}
							
							<div class="form-group hidden">
								<label class="col-md-2 control-label">Grup Tipi : </label>
								<div class="col-md-2">
										<input type="text" name="req_res" class="form-control" value="{if $group->req_res}{$group->req_res}{else}req{/if}"  />
								</div>
							</div>
							<div class="divider" style=""></div>
						{foreach $campaign_items as $cmp_item}
							<div class="form-group item">

								<label class="col-md-2 control-label">{$cmp_item->kosul_ad} : </label>
								<div class="col-md-2">
									{if $cmp_item->var_1_type=='pd_int'}
										<input type="number" name="param_1" class="form-control" placeholder="xx Değer Girin"  />
									{elseif $cmp_item->var_1_type=='pd_float'}
										<input type="number" name="param_1" class="form-control" placeholder="xx.xx Değer Girin"  />
									{elseif $cmp_item->var_1_type=='pd_checkbox'}
										<input type="checkbox" name="param_1" class="form-control" placeholder="Değer Girin"  />
									{elseif $cmp_item->var_1_type=='pd_select'}
										{$par_1_types = $cmp_item->par_1_type|json_decode}
										<select name="param_1" class="form-control">
											{foreach $par_1_types as $par_1_option}
												<option value="{$par_1_option}">{$par_1_option}</option>
											{/foreach}
										</select>
									{else}
										Not Available!
									{/if}
								</div>
								<div class="col-md-2">
									{if $cmp_item->var_2_type=='pd_int'}
										<input type="number" name="param_2" class="form-control" placeholder="xx Değer Girin"  />
									{elseif $cmp_item->var_2_type=='pd_float'}
										<input type="number" name="param_2" class="form-control" placeholder="xx.xx Değer Girin"  />
									{elseif $cmp_item->var_2_type=='pd_checkbox'}
										<input type="checkbox" name="param_2" class="form-control" placeholder="Değer Girin"  />
									{elseif $cmp_item->var_2_type=='pd_select'}
										{$par_2_types = $cmp_item->par_2_type|json_decode}
										<select name="param_2" class="form-control">
											{foreach $par_2_types as $par_2_option}
												<option value="{$par_2_option}">{$par_2_option}</option>
											{/foreach}
										</select>
									{else}
										-
									{/if}
								</div>
								
								<div class="col-md-2">
									{if ($cmp_item->var_1_type=='pd_int')||($cmp_item->var_1_type=='pd_float')}
									<button type="button" class="btn btn-sm blue add_row"><i class="fa fa-plus"></i></button>
									{/if}
								</div>
								<div class="col-md-1 krsl">
											Karşıla <input type="checkbox" name="exl" class="form-control"  checked  />
								</div>
								<div class="form-group hidden">
											<input type="text" name="cmp_item_id" class="form-control" value="{$cmp_item->id}"  />
								</div>
							</div>
						{/foreach}
				    </div>
				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="button" class="btn blue submit">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
	<script>
		$(function() {
			$('.btn.add_row').click(function(){
				var form_group = $(this).closest('.form-group');
				var clone = $(this).closest('.form-group').clone();
				clone.find('.add_row').toggleClass('.add_row .delete_row blue red').html('<i class="fa fa-minus"></i>').click(function(){
					var form_group = $(this).closest('.form-group').remove();
				});
				clone.insertAfter(form_group);
			});
			$('.btn.delete_row').click(function(){
				var form_group = $(this).closest('.form-group').remove();
			});
			$('input,select').keypress(function(){
				$(this).css('border-bottom','')
			});
			$('div.checker').click(function(){
				$(this).closest('div').toggleClass('bg-green');
			});
			$('.btn.submit').click(function(){
				var group_name  = $('input[name=group_name]');
				if(group_name.val().length < 5){
					group_name.css('border-bottom','1px solid #FF0000').focus();
					return;
				}
				var form = $(this).closest('form');
				var serialResult = [];
				form.find('.form-group.item').each(function(){
					var input_1 = $(this).find('.form-control').first();
					if(!input_1.val()){
						$(this).remove();
					}else if(input_1.val() == 'Farketmez'){
						$(this).remove();
					}else{
						serialResult.push($(this).find('.form-control').serializeArray());
					}
				});
				serialResult.push(form.find($('input[name=group_name]')).serializeArray());
				serialResult.push(form.find($('input[name=id]')).serializeArray());
				serialResult.push(form.find($('input[name=req_res]')).serializeArray());
				serialResult.push(form.find($('select[name=group_type]')).serializeArray());
				console.log(serialResult);
				{literal}
				$.ajax({
					type: "POST",
					url: form.attr('action'),
					data: {all :JSON.stringify(serialResult)},
					dataType: 'json',
					success: function(data) {
						if(data.status == 'success'){
							swal(data.message);
							document.location.reload();
						}
					}
				});
				{/literal}
			});
		});
	</script>
	<style>
		.krsl > div.checker > span{
			background-color:#ff0000
		}
		.krsl > div.checker > span.checked{
			background-color:#3598dc
		}
		.form-group{
			margin-bottom:1em;
		}
		.divider{
			margin-bottom:2em;border-bottom: 1px solid #eee;
		}
	</style>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
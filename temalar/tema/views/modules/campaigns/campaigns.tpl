{include file="base/header.tpl"}
{$module = $this->router->fetch_class()}
{$method = $this->router->fetch_method()}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a href="{site_url('campaigns/addcampaign')}" class="btn red btn-outline sbold"> 
                    	<i class="fa fa-plus"></i> 
                    	Kampanya Ekle
                	</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Kampanya Adı </th>
                                <th> Başlangıç Tarihi </th>
                                <th> Sona Erme Tarihi </th>
                                <th> Açıklama </th>
                                <th> Durum </th>
                                <th width="1%"> 
                                    <a href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th width="1%">
                                    <a href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {if count($campaigns) > 0}
                                {foreach $campaigns as $campaign}
                                <tr> 
                                    <td> {$campaign->id} </td>
                                    <td> {$campaign->name}</td>
                                    <td> {$campaign->start_date} </td>
                                    <td> {$campaign->end_date} </td>
                                    <td> {$campaign->description} </td>
                                    <td> <a onclick="durum_degis({$campaign->id},'kampanya','#kampanyadurumu{$campaign->id}')" id="kampanyadurumu{$campaign->id}" class="btn btn-circle btn-icon-only btn-default {if $campaign->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif">
                                            <i class=""></i>
                                        </a></td>
                                    <td> <a href="{site_url('campaigns/editcampaign')}/{$campaign->id}" title="Düzenle" class="btn btn-sm btn-default hizliduzenle"> <i class="fa fa-pencil-square-o"></i> </a> </td>
                                    <td> <a onclick="return confirm('Kullanıcıyı silmek istediğinize emin misiniz?');" href="{site_url('campaigns/removecampaign')}/{$campaign->id}" title="Sil" class="btn btn-sm btn-default sil"> <i class="fa fa-times"></i> </a> </td>
                                </tr>
                                {/foreach}
                            {else}
                            <tr>
                                <td colspan="14" align="center">Kayıtlı Grup Bulunamadı</td>
                            </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
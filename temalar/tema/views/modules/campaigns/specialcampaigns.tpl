{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a data-toggle="modal" href="#ozel_kampanya_ekle" class="btn red btn-outline sbold"> 
                        <i class="fa fa-plus"></i> 
                        Özel Kampanya Ekle  
                    </a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ozel_kampanya_ekle" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Yeni Kullanıcı Ekle</h4>
                    </div>
                    <form class="form-horizontal" id="form_ozel_kampanya_ekle" method="post" onsubmit="ozel_kampanya_ekle();" action="javascript:void(0);">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12" style="display: none;" id="form_cevabi_ekle"></div>
                                        </div>                                
                                        
                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Üye Grubu : <span class="required"> * </span></label>
                                            <div class="col-md-8">
                                                 <select name="uye_grubu" class="form-control">
                                                    <option value=""> Seçiniz </option>
                                                    {foreach $gruplar as $grup}
                                                    <option value="{$grup->id}"> {$grup->grup} </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>   

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Mail Uzantısı: <span class="required"> * </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required_chk" name="mail_uzantisi" required="required" placeholder="" value="" />                                                
                                            </div>
                                        </div>                                          

                                    </div>
                                </div>
                            </div>                          
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" onclick="yenile();" data-dismiss="modal">Kapat</button>
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width: 50%;">                         
                                <th class="islem_bilgilendirme" style="text-align: right;" align="right">

                                    <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Detaylı Düzenle  </span>

                                    <span href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th> 
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Üye / Bayi Grubu Adı </th>
                                <th> Mail Uzantısı </th>
                                <th> Tarih </th>
                                <th> Ekleyen </th>
                                <th width="1%"> 
                                    <a class="btn btn-circle btn-icon-only btn-default durumu" title="Aktif veya Pasif">
                                        <i class=""></i>
                                    </a>  
                                </th> 
                                <th width="1%"> 
                                    <a href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th width="1%">
                                    <a href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {if count($ozelkampanyalar) > 0}
                                {foreach $ozelkampanyalar as $ozelkampanya}
                                <div class="modal fade" id="ozel_kampanya_{$ozelkampanya->id}" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Özel Kampanya Düzenle</h4>
                                            </div>
                                            <form class="form-horizontal" id="form_ozel_kampanya_{$ozelkampanya->id}" method="post" onsubmit="ozel_kampanya_duzenle('{$ozelkampanya->id}');" action="javascript:void(0);">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-body">
                                                                <div class="row">
                                                                    <div class="col-md-12" style="display: none;" id="form_cevabi_{$ozelkampanya->id}"></div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label class="col-md-4 control-label">Üye Grubu : <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <select name="uye_grubu" class="form-control">
                                                                            <option value=""> Seçiniz </option>
                                                                            {foreach $gruplar as $grup}
                                                                            <option {if $ozelkampanya->uye_grubu == $grup->id}selected{/if} value="{$grup->id}"> {$grup->grup} </option>
                                                                            {/foreach}
                                                                        </select>
                                                                    </div>
                                                                </div>   

                                                                <div class="form-group row">
                                                                    <label class="col-md-4 control-label">Mail Uzantısı: <span class="required"> * </span></label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control required_chk" name="mail_uzantisi" required="required" placeholder="" value="{$ozelkampanya->mail_uzantisi}" />                                                
                                                                    </div>
                                                                </div> 

                                                            </div>
                                                        </div>
                                                    </div>                          
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" onclick="yenile();" data-dismiss="modal">Kapat</button>
                                                    <button type="submit" class="btn blue">Kaydet</button>
                                                </div>
                                            </form> 
                                        </div>
                                    </div>
                                </div>
                                <tr> 
                                    <td> {$ozelkampanya->id} </td>
                                    <td> {$ozelkampanya->uye_grubu_adi} </td>
                                    <td> {$ozelkampanya->mail_uzantisi} </td>
                                    <td> {date('d.m.Y H:i:s', $ozelkampanya->zaman)} </td>
                                    <td> {$this->extraservices->fullisim($ozelkampanya->kullanici)} </td>
                                    <td> 
                                        <a onclick="durum_degis({$ozelkampanya->id}, 'ozel_kampanyalar', '#ozelkampanyadurumu{$ozelkampanya->id}');" id="ozelkampanyadurumu{$ozelkampanya->id}" class="btn btn-circle btn-icon-only btn-default {if $ozelkampanya->durum == 1}durumu{else}beyaz{/if}" title="Aktif / Pasif">
                                            <i class=""></i>
                                        </a> 
                                    </td>
                                    <td> 
                                        <a data-toggle="modal" href="#ozel_kampanya_{$ozelkampanya->id}" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>                                                
                                    </td>
                                    <td>  
                                        <a onclick="return confirm('Kullanıcıyı silmek istediğinize emin misiniz?');" href="{site_url('campaigns/removespecial')}/{$ozelkampanya->id}" title="Sil" class="btn btn-sm btn-default sil">
                                            <i class="fa fa-times"></i>
                                        </a>                             
                                    </td>
                                </tr>
                                {/foreach}
                            {else}
                                <tr>
                                    <td colspan="7" align="center">Kayıtlı Özel Kampanya Bulunamadı</td>
                                </tr>
                            {/if}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
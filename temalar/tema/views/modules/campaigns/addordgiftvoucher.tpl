{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">
				{if $status && $message}
				<div class="note note-{$status}">
				    <p> {$message} </p>
				</div>
				{/if}
		        <form class="form-horizontal form-row-seperated" method="post" action="" autocomplete="off">

				    <div class="form-body">

				    	<div class="form-group">
				            <label class="col-md-2 control-label">Çek Kodu : <span class="required"> * </span></label>
				            <div class="col-md-2">
				                <input type="text" class="form-control required_chk" id="cek_kodu" name="cek_kodu" placeholder="Çek Kodu" value="{$hediyeceki->cek_kodu}" required="required" />
				            </div>
				            <div class="col-md-1">
				                <button type="button" onclick="randomCode();" class="btn red">Otomatik Üret</button>
				            </div>
				            <div class="col-md-1">
								<button type="button" class="btn btn-circle btn-sm blue tooltips" data-container="body" data-placement="top" data-original-title="Çek Kodunuz maximum 50 minimum 6 karakter olmalıdır."><i class="fa fa-question"></i></button>
				            </div>
				        </div>
						
				    	<div class="form-group">
				            <label class="col-md-2 control-label">Sipariş ID : </label>
				            <div class="col-md-2">
				                <input type="text" class="form-control" name="siparis_id" placeholder="Sipariş Numarası" value="{$hediyeceki->siparis_id}" />
				            </div>
						</div>
						<div class="form-group">
				            <label class="col-md-2 control-label">Max Kullanım Sayısı : </label>
				            <div class="col-md-1">
				                <input type="text" class="form-control" name="mks" placeholder="1" value="{$hediyeceki->mks}" />
				            </div>
				        </div>

				        <div class="form-group">
				            <label class="col-md-2 control-label">Kişi Başı Kullanım Sınırı : </label>
				            <div class="col-md-1">
				                <input type="text" class="form-control" name="kbks" placeholder="1" value="{$hediyeceki->kbks}" />
				            </div>
				            <div class="col-md-1">
								<button type="button" class="btn btn-circle btn-sm blue tooltips" data-container="body" data-placement="top" data-original-title="Daha önce kullanılmış olan bir hediye çekinde kullanım sınırı kullanımının değiştirilmesi önerilmez."><i class="fa fa-question"></i></button>
				            </div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Üye/Bayi grubu : </label>
							<div class="col-md-2">
								 <select name="ubg" class="form-control">
				                	<option value="0"> Tümü </option>
				                	{foreach $gruplar as $grup}
				                	<option {if $hediyeceki->ubg == $grup->id}selected{/if} value="{$grup->id}"> {$grup->grup} </option>
				                	{/foreach}
				                </select>

							</div>
				        </div>

				        <div class="form-group">
				        	<label class="col-md-2 control-label">Başlangıç ve Bitiş Tarihi : <span class="required"> * </span></label>
				        	<div class="col-md-2">
				        		<div class="input-group input-large date-picker input-daterange" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
				                    <input type="text" class="form-control required_chk" name="bast" placeholder="Başlangıç" value="{date('d-m-Y', time())}" required="required" />
				                    	<span class="input-group-addon"> # </span>
				                    <input type="text" class="form-control required_chk" name="bitt" placeholder="Bitiş" value="{date('d-m-Y', time())}" required="required" /> 
			                    </div>
				        	</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Değer Tipi : </label>
							<div class="col-md-2">
								 <select name="dtip" id="dtip" onchange="degerdegistir();" class="form-control">
				                	<option value="0" {if $hediyeceki->dtip == 0}selected{/if}> Sabit </option>
				                	<option value="1" {if $hediyeceki->dtip == 1}selected{/if}> Yüzde </option>
				                </select>
							</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Değer : </label>
							<div class="col-md-2">
								<div class="input-group">
		                            <input type="text" class="form-control" name="dgr" placeholder="5" value="{$hediyeceki->dgr}" />
		                            <span class="input-group-addon" id="deger_tipimiz">
		                                {if $hediyeceki->dtip == 0}TL{else}%{/if}
		                            </span>
		                        </div>
							</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Kargo Bedava : </label>
							<div class="col-md-2">
								 <select name="kbd" class="form-control">
				                	<option value="0" {if $hediyeceki->kbd == 0}selected{/if}> Hayır </option>
				                	<option value="1" {if $hediyeceki->kbd == 1}selected{/if}> Evet </option>
				                </select>
							</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">İndirimli Ürünlerde Geçerli Olsun : </label>
							<div class="col-md-2">
								 <select name="iui" class="form-control">
				                	<option value="0" {if $hediyeceki->iui == 0}selected{/if}> Hayır </option>
				                	<option value="1" {if $hediyeceki->iui == 1}selected{/if}> Evet </option>
				                </select>
							</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Kullanım Alışveriş Sınırı : </label>
							<div class="col-md-5">
								<div class="input-group">
									<span class="input-group-addon">KDV Hariç </span>
		                            <input type="text" class="form-control required_chk" name="asi" placeholder="0" value="{$hediyeceki->asi}" required="required" />
		                            <span class="input-group-addon">
		                                TL üstü alışverişlerde kullanılabilsin.
		                            </span>
		                        </div>
							</div>
				        </div>

				        <div class="form-group">
							<label class="col-md-2 control-label">Özel Çek Tanımla : </label>
							<div class="col-md-2">
								 <select name="gcr_tp" id="ozel_cek_tanimla" onchange="ozelcekayarlari();" class="form-control">
				                	<option value="normal" {if $hediyeceki->gcr_tp == "normal"}selected{/if}> Tüm Kategori ve Markalar </option>
		                            <option value="product" {if $hediyeceki->gcr_tp == "product"}selected{/if}> Belirli Ürünlere Özel </option>
		                            <option value="category" {if $hediyeceki->gcr_tp == "category"}selected{/if}> Belirli Kategorilere Özel </option>
		                            <option value="brand" {if $hediyeceki->gcr_tp == "brand"}selected{/if}> Belirli Markalara Özel </option>
		                            <option value="member" {if $hediyeceki->gcr_tp == "member"}selected{/if}> Belirli Üyelere Özel </option>
				                </select>
							</div>
				        </div>

				        <div class="form-group ozelcekler" style="display: none;">
							
							<div class="col-md-offset-2 col-md-10">
								<div class="radio-list">
		                            <label class="radio-inline">
		                            	<input type="radio" name="gcr" value="1" {if $hediyeceki->gcr == 1}checked{/if}> <span id="gcr_msg_1"></span>
		                            </label>
		                            <label class="radio-inline">
		                            	<input type="radio" name="gcr" value="0" {if $hediyeceki->gcr == 0}checked{/if}> <span id="gcr_msg_2"></span>
		                            </label>
		                        </div>
							</div>
							<div class="col-md-offset-2 col-md-6">
				                <label class="col-md-2 control-label" id="gcr_label"></label>
								<div class="col-md-6">
				                    <input type="text" class="form-control tooltips" data-container="body" data-placement="top" data-original-title="" name="ngcr" value="{$hediyeceki->ngcr}" />
				                </div>
							</div>
				        </div>
				    </div>

				    <div class="form-body">
				        <div class="form-group">
				            <label class="col-md-2 control-label"></label>
				            <div class="col-md-10">
				                <button type="submit" class="btn blue">Kaydet</button>
				            </div>
				        </div>
				    </div>

				</form>
			</div>
		</div>
		<!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
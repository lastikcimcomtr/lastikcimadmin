{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">

        <div class="row">
         <div class="col-md-12"> 

            <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <form class="row" method="post" action="" autocomplete="off">
                            <div class="form-group col-md-6">
                                <label for="stok">Stok Kodu</label>
                                <input type="text" name="stok" class="form-control" id="stok" placeholder="Stok Kodu" value="{$sdata['stok']}" />
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Arama Yap</button>
                            </div>
                        </form>
                    </div>
                </div>

          <div class="table-responsive">






         <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr> 
                    <th>Stok</th>
                    <th>Miktar</th>
                    <th>Eski Miktar</th> 
                    <th>Fiyat</th> 
                    <th>Eski Fiyat</th> 
                    <th>Model</th> 
                    <th>Tatko</th>
                    <th>Lastikcim</th>
                </tr>
            </thead>
            <tbody>
                {foreach $sonurunler as $sonurun}
                    <tr>
                        <td>{$sonurun->stok}</td>
                        <td {if $sonurun->e_miktar && $sonurun->e_miktar > $sonurun->miktar}style="background-color: red; color: white;"{else}style="background-color: green; color: white;"{/if}>{$sonurun->miktar}</td>
                        <td>{$sonurun->e_miktar}</td>
                        <td {if $sonurun->e_fiyat && $sonurun->e_fiyat > $sonurun->fiyat}style="background-color: red; color: white;"{else}style="background-color: green; color: white;"{/if}>{$sonurun->fiyat}</td>
                        <td>{$sonurun->e_fiyat}</td>
                        <td>{$sonurun->model}</td>
                        <td>{date('d.m.Y H:i', $sonurun->zaman)}</td>
                        <td {if $sonurun->sistem}{else}style="background-color: red; color: white;"{/if}>{if $sonurun->sistem}{date('d.m.Y H:i', $sonurun->sistem)}{else}Bekliyor{/if}</td>
                    </tr>
                {/foreach}    
            </tbody>
        </table>       
    </div>
</div>
</div>
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
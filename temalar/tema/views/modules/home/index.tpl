{include file="base/header.tpl"}
<style type="text/css">	
#chartdiv {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
#ziyaretcilerimiz {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row" style="margin-bottom: 10px">
      <div class="col-md-6">
        <div class="row">
                <div class="col-md-6">
                    <a href="/orders/index" class="btn btn-warning font-dark" target="_blank">Siparişlere Git</a>
                    <a href="/home/arama_trend" class="btn btn-primary" target="_blank">Arama Trendleri</a>
                </div>
        </div>
        </div>
      </div>
      <div class="row">
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-green-haze"></i>
                          <span class="font-sm bold font-green-haze">Yeni Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$total_onay_bekleyen['adet']}) Adet</div><div class="col-md-4"><a href="{$total_onay_bekleyen['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-yellow-gold"></i>
                          <span class="font-sm bold font-yellow-gold">Kargoya Verilmeyenler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$kargoya_verilmeyen['adet']}) Adet</div><div class="col-md-4"><a href="{$kargoya_verilmeyen['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-red-mint"></i>
                          <span class="font-sm bold font-red-mint">İptal/İade Talebi</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$iptal_talebi['adet']}) Adet</div><div class="col-md-4"><a href="{$iptal_talebi['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-yellow-gold"></i>
                          <span class="font-sm bold font-yellow-gold">Kargoya Verildi Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$kargoya_verildi['adet']}) Adet</div><div class="col-md-4"><a href="{$kargoya_verildi['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-blue-steel"></i>
                          <span class="font-sm bold font-blue-steel">Geciken Kargo Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$kargoda_bekleyen['adet']}) Adet</div><div class="col-md-4"><a href="{$kargoda_bekleyen['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-blue-steel"></i>
                          <span class="font-sm bold font-blue-steel">Geciken Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">({$geciken_hazirlaniyor['adet']}) Adet Hazırlanıyor</div><div class="col-md-4"><a href="{$geciken_hazirlaniyor['link']}" target="_blank">Listele</a></div></div>
                    <div class="row"><div class="col-md-8">({$geciken_tedarik['adet']}) Adet Tedarik Sürecinde</div><div class="col-md-4"><a href="{$geciken_tedarik['link']}" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
      </div>
        {*}
        {include file="base/bakiyeler.tpl"}
        <div class="row">
			<div class="col-md-6">
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze"> Çok Arananlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            {foreach $most_searched_keywords as $s_search_word}
                                <div class="row">
                                    <div class="col-md-8">
                                        {$s_search_word->search}
                                    </div>
                                    <div class="col-md-4">
                                        {$s_search_word->adet} Adet
                                    </div>
                                </div>          
                            {/foreach} 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Aranıp Bulunamayanlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            {foreach $most_searched_empty as $s_search_word}
                                <div class="row">
                                    <div class="col-md-8">
                                        {$s_search_word->search}
                                    </div>
                                    <div class="col-md-4">
                                        {$s_search_word->adet} Adet
                                    </div>
                                </div>          
                            {/foreach} 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Son Arananlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            {foreach $last_searched as $s_search_word}
                                <div class="row">
                                    <div class="col-md-7">
                                        {$s_search_word->search}
                                    </div>
                                    <div class="col-md-5">
                                        {date('d/m H:i:s', $s_search_word->time)}
                                    </div>
                                </div>          
                            {/foreach} 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Trendler</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            {$count=0}
                            <div class="row">
                            {foreach $search_trends as $chunk_group}
                                <div class="col-md-6">
                                    {foreach $chunk_group as $s_search_word}
                                    {$count=$count+1}
                                    <div class="row">
                                        <div class="col-md-12">
                                       <b>{$count}.</b>{$s_search_word->searched}
                                        </div>
                                    </div>
                                    {/foreach} 
                                </div>          
                            {/foreach} 
                            </div>          
                    </div>
                </div>
            </div>
        </div>

        
        {*}
        <div id="arama_kaydet" class="modal fade" tabindex="-1" data-width="400" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Arama Kaydetme</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Arama İsmi</label>
                                        <input type="text" id="arama_ismi" value="" placeholder="Arama için isim giriniz" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="lastikkaydetmesonuc"></div>
                        <button type="button" data-dismiss="modal" class="btn dark btn-outline">Kapat</button>
                        <button type="button" class="btn red" onclick="arama_kaydet();">Kaydet</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div id="lastikler">
            
        </div>              
        <!-- END PAGE BASE CONTENT -->
    </div>
    {include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
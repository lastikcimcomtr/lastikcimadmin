<!DOCTYPE html> 
<html lang="tr">
<head>
    <meta charset="utf-8" />
    <title>Lastikcim</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{$theme_url}assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="{$theme_url}assets/pages/css/lock-2.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" /> 
</head>
<body class="">

    <div class="page-lock">
        {if $status && $message}
        <div class="alert alert-{$status} text-center">
            <button class="close" data-close="alert"></button>
            <span> {$message} </span>
        </div>
        {/if}
        <div class="page-body" style="background-color: #fff;"> 
            <img class="page-lock-img" src="{$theme_url}assets/logo.png" alt="">
            <div class="page-lock-info">
                <h1 style="color: #504d4d;">Giriş Yap</h1>
                <p class="text-muted" style="margin-bottom: 13px;color: #504d4d;">Yönetim Paneli Bilgilerini Giriniz</p>
                <form action="" method="post" autocomplete="off" style="margin:0px;">
                    <div class="input-group mb-1" style="margin-bottom: 10px;">
                        <span class="input-group-addon"><i class="icon-user"></i></span>
                        <input type="text" class="form-control" required="" placeholder="Kullanıcı Adı" name="kullanici" id="kullaniciadi">
                    </div>
                    <div class="input-group mb-2" style="margin-bottom: 10px;">
                        <span class="input-group-addon"><i class="icon-lock"></i>
                        </span>
                        <input type="password" required="" class="form-control" placeholder="Şifre" name="sifre" id="sifre">
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-primary px-2">Giriş Yap</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="page-footer-custom" style="color:#504d4d;">  Lastikcim.com.tr <a target="_blank" href="http://www.lastikcim.com.tr">Panel</a> </div>
        </div>

    <style type="text/css">
        .display-table{
            display: table;
            table-layout: fixed;
        }

        .display-cell{
            display: table-cell;
            vertical-align: middle;
            float: none;
        }
    </style>
    <script src="{$theme_url}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <script src="{$theme_url}assets/global/scripts/app.min.js" type="text/javascript"></script>
</body>
</html>
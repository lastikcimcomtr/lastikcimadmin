{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a data-toggle="modal" href="#kullanici_ekle" class="btn red btn-outline sbold"> 
                        <i class="fa fa-plus"></i> 
                        Kullanıcı Grubu Ekle
                    </a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="kullanici_ekle" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Kullanıcı Grubu Ekle</h4>
                    </div>
                    <form class="form-horizontal" id="form_grup_ekle" method="post" onsubmit="grup_ekle();" action="javascript:void(0);">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12" style="display: none;" id="form_cevabi_ekle"></div>
                                        </div>
                                        <div class="form-group row" style="margin-bottom: 10px !important;">
                                            <label class="col-md-4 control-label">Adı: </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="adi" placeholder="" value="" />                                                
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>                          
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">                         
                                <th class="islem_bilgilendirme" style="text-align:right;" align="right">

                                    <span style="height:20px;padding:0px 5px 0 5px; background-color:#ff5500; color:white;" href="#" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Yetkileri Düzenle </span>

                                    <span style="height:20px;padding:0px 5px 0 5px; background-color:#DD55C9; color:white;" href="#" title="Sipariş Detayları" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Düzenle  </span>

                                    <span style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th> 
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th style="width:2%;"> ID </th>
                                <th> Kullanıcı Grubu Adı </th>
                                <th> Kullanıcı Sayısı  </th>
                                <th style="width:2%;"> 
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#ff5500;color:white;" href="#" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th style="width:2%;"> 
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;" href="#" title="Sayfayı Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th style="min-width:2%;width:2%;">
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                          {foreach from=$kullanici_gruplari item=grup}

                          <div class="modal fade" id="grup_{$grup->id}" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Kullanıcı Grubu Düzenle</h4>
                                    </div>
                                    <form class="form-horizontal" id="form_grup_{$grup->id}" method="post" onsubmit="grup_duzenle('{$grup->id}');" action="javascript:void(0);">
                                        <input type="hidden" name="id" value="{$grup->id}" />
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12" style="display: none;" id="form_cevabi_{$grup->id}"></div>
                                                        </div>

                                                        <div class="form-group row" style="margin-bottom: 10px !important;">
                                                            <label class="col-md-4 control-label">Adı: </label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" name="adi" placeholder="" value="{$grup->adi}" />                                                
                                                            </div>
                                                        </div>                                                                                         

                                                    </div>
                                                </div>
                                            </div>                          
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
                                            <button type="submit" class="btn blue">Kaydet</button>
                                        </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                        <tr id="gt_{$grup->id}"> 
                            <td>{$grup->id}</td>
                            <td>{$grup->adi}</td>
                            <td>{$grup->kullaniciSayisi}</td>
                            <td> 
                                <a style="height:20px; padding:0px 5px 0 5px; background-color:#ff5500; color:white;"  href="{site_url('users/roles/index/grup/'|cat:{$grup->id})}" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>                                                
                            </td>
                            <td> 
                                <a style="height:20px; padding:0px 5px 0 5px; background-color:#DD55C9; color:white;" data-toggle="modal" href="#grup_{$grup->id}" title="Sayfayı Düzenle" class="btn btn-sm btn-default">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>                                                
                            </td>
                            <td>  {if $grup->kullaniciSayisi == 0}
                                <a style="height:20px; padding:0px 5px 0 5px; background-color:#E73434; color:white; border-radius:10px;" onclick="sil({$grup->id})" href="#" title="Sil" class="btn btn-sm btn-default">
                                    <i class="fa fa-times"></i>
                                </a>
                                {else}
                                Kullanıcı Var 
                                {/if}                            
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
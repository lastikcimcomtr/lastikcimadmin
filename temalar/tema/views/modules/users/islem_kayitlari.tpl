{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
 

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th> Tarih </th>
                                <th> Kullanıcı Adı </th>
                                <th> Adı </th>
                                <th> Soyadı  </th>
                                <th> İslem Yolu </th>
                                <th> Ip </th> 
                                <th> Açıkklama </th> 
                                
                            </tr>
                        </thead>
                        <tbody>
                          {foreach from=$islemler item=islem}

                        
                    <tr id="kt_{$islem->id}"> 
                        <td>{date('d.m.Y H:i:s',$islem->tarih)}</td>
                        <td>{$islem->kullanici}</td>
                        <td>{$islem->ad}</td>
                        <td>{$islem->soyad}</td>
                        <td>{$islem->modul_method}</td>
                        <td>{$islem->ip}</td>
                        <td>{$islem->aciklama}</td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
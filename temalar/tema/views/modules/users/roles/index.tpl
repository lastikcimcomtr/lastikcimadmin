{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="breadcrumbs">
            {if $type == "grup"}
            <h1>{$roller->grup->adi} Grup  Yetkilerini Düzenle</h1>
            {else}
            <h1>{$roller->grup->adi} Grup Üyesi {$roller->user->kullanici}  Yetkilerini Düzenle</h1>
            {/if}
        </div> 
        <div class="row" style="border-bottom: 1px solid #ededed; height: 40px; line-height: 40px;">
            <div class="col-md-4"></div>
            <div class="col-md-2 text-center"><strong>Görüntüleme Yetkisi</strong></div>
            <div class="col-md-2 text-center"><strong>Ekleme Yetkisi</strong></div>
            <div class="col-md-2 text-center"><strong>Düzenleme Yetkisi</strong></div>
            <div class="col-md-2 text-center"><strong>Silme Yetkisi</strong></div>
        </div>
        <style type="text/css">
        .rol_hover:hover {
            background-color: #ededed;
        }
    </style>
    {foreach from=$roller->roller item=rol}

    <div class="row rol_hover" style="border-bottom: 1px solid #ededed; height: 40px; line-height: 40px;">
        <div class="col-md-4">{$rol->rol_adi}</div>

        <div class="col-md-2 text-center">
            {if  $rol->grup->gor}{if $type == "grup"}{$durum = 'Aktif'}{else}{$durum = 'Kilitli'}{/if}{else}{if  $rol->user->gor}{$durum = 'Aktif'}{else}{$durum = 'Pasif'}{/if}{/if}
            <a href="#" id="gor_{$rol->id}" {if $durum <> 'Kilitli'} onclick="yetki_edit({$rol->id},'gor');" data-deger="{if $durum == 'Aktif'}1{else}0{/if}" data-bg = '#35366C' {/if}  class="btn btn-circle btn-icon-only btn-default" style="width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:{if $durum == 'Aktif'}#35366C{elseif $durum == 'Pasif'}#fff{else}#999{/if};" title="{$durum}">
                <i class=""></i>
            </a>
        </div>

        <div class="col-md-2 text-center">
           {if  $rol->grup->ekle}{if $type == "grup"}{$durum = 'Aktif'}{else}{$durum = 'Kilitli'}{/if}{else}{if  $rol->user->ekle}{$durum = 'Aktif'}{else}{$durum = 'Pasif'}{/if}{/if}
           <a href="#" id="ekle_{$rol->id}" {if $durum <> 'Kilitli'} onclick="yetki_edit({$rol->id},'ekle');" data-deger="{if $durum == 'Aktif'}1{else}0{/if}" data-bg = '#0D9E0D' {/if}   class="btn btn-circle btn-icon-only btn-default" style="width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:{if $durum == 'Aktif'}#0D9E0D{elseif $durum == 'Pasif'}#fff{else}#999{/if};" title="{$durum}">
             <i class=""></i>
         </a>
     </div>
     <div class="col-md-2 text-center">
        {if  $rol->grup->duzenle}{if $type == "grup"}{$durum = 'Aktif'}{else}{$durum = 'Kilitli'}{/if}{else}{if  $rol->user->duzenle}{$durum = 'Aktif'}{else}{$durum = 'Pasif'}{/if}{/if}       
        <a href="#" id="duzenle_{$rol->id}" {if $durum <> 'Kilitli'} onclick="yetki_edit({$rol->id},'duzenle');" data-deger="{if $durum == 'Aktif'}1{else}0{/if}" data-bg = '#FFCC00' {/if}   class="btn btn-circle btn-icon-only btn-default" style="width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:{if $durum == 'Aktif'}#FFCC00{elseif $durum == 'Pasif'}#fff{else}#999{/if};" title="{$durum}">
           <i class=""></i>
       </a>
   </div>
   <div class="col-md-2 text-center">
    {if  $rol->grup->sil}{if $type == "grup"}{$durum = 'Aktif'}{else}{$durum = 'Kilitli'}{/if}{else}{if  $rol->user->sil}{$durum = 'Aktif'}{else}{$durum = 'Pasif'}{/if}{/if}
    <a href="#" id="sil_{$rol->id}" {if $durum <> 'Kilitli'} onclick="yetki_edit({$rol->id},'sil');" data-deger="{if $durum == 'Aktif'}1{else}0{/if}" data-bg = '#CF0D0D' {/if}   class="btn btn-circle btn-icon-only btn-default" style="width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:{if $durum == 'Aktif'}#CF0D0D{elseif $durum == 'Pasif'}#fff{else}#999{/if};" title="{$durum}">
      <i class=""></i>
  </a>
</div>
</div>
{/foreach}
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
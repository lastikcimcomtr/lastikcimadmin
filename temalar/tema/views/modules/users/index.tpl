{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="portlet">
            <div class="portlet-title" style="margin-top: -25px;border-bottom:0px;">
                <div class="caption"></div>
                <div class="actions btn-set"> 
                    <a data-toggle="modal" href="#kullanici_ekle" class="btn red btn-outline sbold"> 
                        <i class="fa fa-plus"></i> 
                        Kullanıcı Ekle  
                    </a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="kullanici_ekle" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Yeni Kullanıcı Ekle</h4>
                    </div>
                    <form class="form-horizontal" id="form_kullanici_ekle" method="post" onsubmit="kullanici_ekle();" action="javascript:void(0);">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12" style="display: none;" id="form_cevabi_ekle"></div>
                                        </div>

                                        <div class="form-group row" style="margin-bottom: 10px !important;">
                                            <label class="col-md-4 control-label">Adı: </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="ad" placeholder="" value="" />                                                
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Soyadı: </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="soyad" placeholder="" value="" />                                                
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Email: <span class="required"> * </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required_chk" name="email" required="required" placeholder="" value="" />                                                
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Kullanıcı Adı: <span class="required"> * </span></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control required_chk" name="kullanici" required="required" placeholder="" value="" />                                                
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Kullanıcı Grubu: <span class="required"> * </span></label>
                                            <div class="col-md-8">
                                                <select style="height:34px;" name="grup" class="form-control form-filter input-sm required_chk" required="required">
                                                    <option value="">Seçiniz</option>
                                                    {foreach from=$kullanici_gruplari item=grup }
                                                    <option value="{$grup->id}">{$grup->adi}</option>
                                                    {/foreach}                                        
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 control-label">Şifre: </label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="sifre" placeholder="" value="" />                                                
                                            </div>
                                        </div>                                              

                                    </div>
                                </div>
                            </div>                          
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn dark btn-outline" onclick="yenile();" data-dismiss="modal">Kapat</button>
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">                         
                                <th class="islem_bilgilendirme" style="text-align:right;" align="right">

                                    <span class="btn btn-circle btn-icon-only btn-default" style="width: 18px;height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#990066;" title="Durum Değiştir">
                                        <i class=""></i>
                                    </span>
                                    <span id="islembilgisitext"> Kullanıcı Durumu </span>

                                    <span style="height:20px;padding:0px 5px 0 5px; background-color:#ff5500; color:white;" href="#" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Yetkileri Düzenle </span>

                                    <span style="height:20px;padding:0px 5px 0 5px; background-color:#DD55C9; color:white;" href="#" title="Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Düzenle  </span>

                                    <span style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th> 
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th style="width:2%;"> ID </th>
                                <th> Kullanıcı Adı </th>
                                <th> Adı </th>
                                <th> Soyadı  </th>
                                <th> Email </th>
                                <th> Son Giriş </th> 
                                <th style="width:2%;"> 
                                    <a class="btn btn-circle btn-icon-only btn-default" style="width: 18px;height: 18px;padding:1px 3px 3px 3px;border-radius: 0px !important;background-color:#990066;" title="durum">
                                        <i class=""></i>
                                    </a>  
                                </th> 
                                <th style="width:2%;"> 
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#ff5500;color:white;" href="#" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th style="width:2%;"> 
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#DD55C9;color:white;" href="#" title="Sayfayı Düzenle" class="btn btn-sm btn-default">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>                                                
                                </th>
                                <th style="min-width:2%;width:2%;">
                                    <a style="height:20px;padding:0px 5px 0 5px;background-color:#E73434;color:white;border-radius:10px;" href="#" title="Sil" class="btn btn-sm btn-default">
                                        <i class="fa fa-times"></i>
                                    </a> 
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                          {foreach from=$kullanicilar item=kullanici}

                          <div class="modal fade" id="kullanici_{$kullanici->id}" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Kullanıcı Düzenle</h4>
                                    </div>
                                    <form class="form-horizontal" id="form_kullanici_{$kullanici->id}" method="post" onsubmit="kullanici_duzenle('{$kullanici->id}');" action="javascript:void(0);">
                                        <input type="hidden" name="id" value="{$kullanici->id}" />
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-md-12" style="display: none;" id="form_cevabi_{$kullanici->id}"></div>
                                                        </div>

                                                        <div class="form-group row" style="margin-bottom: 10px !important;">
                                                            <label class="col-md-4 control-label">Adı: </label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" name="ad" placeholder="" value="{$kullanici->ad}" />                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-md-4 control-label">Soyadı: </label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" name="soyad" placeholder="" value="{$kullanici->soyad}" />                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-md-4 control-label">Email: <span class="required"> * </span></label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control required_chk" name="email" required="required" placeholder="" value="{$kullanici->email}" />                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-md-4 control-label">Kullanıcı Adı: <span class="required"> * </span></label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control required_chk" name="kullanici" required="required" placeholder="" value="{$kullanici->kullanici}" />                                                
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label class="col-md-4 control-label">Kullanıcı Grubu: <span class="required"> * </span></label>
                                                            <div class="col-md-8">
                                                                <select style="height:34px;" name="grup" class="form-control form-filter input-sm required_chk" required="required">
                                                                   {foreach from=$kullanici_gruplari item=grup }
                                                                   <option value="{$grup->id}" {if $kullanici->grup == $grup->id} selected {/if}>{$grup->adi}</option>
                                                                   {/foreach} 
                                                               </select>
                                                           </div>
                                                       </div>

                                                       <div class="form-group row">
                                                        <label class="col-md-4 control-label">Kayıt Tarihi: </label>
                                                        <div class="col-md-8">{date('d.m.Y H:i:s',$kullanici->kayit)}</div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-4 control-label">Son Giriş: </label>
                                                        <div class="col-md-8"> {date('d.m.Y H:i:s',$kullanici->songiris)}</div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-4 control-label">En Son IP Adresi: </label>
                                                        <div class="col-md-8"> {$kullanici->sonip}</div>
                                                    </div>
                                                    
                                                    <div class="form-group row" style="border-bottom: 1px solid #ededed;">
                                                        <div class="col-md4">&nbsp;</div>
                                                        <div class="col-md-8"> <h4 style="font-weight: bold !important;">Şifre Değişimi</h4></div>                                                      
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-4 control-label">Yeni Şifre: </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="sifre" placeholder="Yeni Şifre" value="" />                                                
                                                        </div>
                                                    </div>                                              

                                                </div>
                                            </div>
                                        </div>                          
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
                                        <button type="submit" class="btn blue">Kaydet</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                    <tr id="kt_{$kullanici->id}"> 
                        <td>{$kullanici->id}</td>
                        <td>{$kullanici->kullanici}</td>
                        <td>{$kullanici->ad}</td>
                        <td>{$kullanici->soyad}</td>
                        <td>{$kullanici->email}</td>
                        <td>{date('d.m.Y H:i:s',$kullanici->songiris)}</td>
                        <td id="durum_guncelle"> 
                            <a href="#" onclick="durum_degis({$kullanici->id},'kullanicilar', '#kullanicidurumu{$kullanici->id}');" id="kullanicidurumu{$kullanici->id}" class="btn btn-circle btn-icon-only btn-default {if $kullanici->durum == 1}durumu{else}beyaz{/if}"><i class=""></i></a>                                                 
                        </td>
                        <td> 
                            <a style="height:20px; padding:0px 5px 0 5px; background-color:#ff5500; color:white;"  href="{site_url('users/roles/index/user/'|cat:{$kullanici->id})}" title="Yetkileri Düzenle" class="btn btn-sm btn-default">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>                                                
                        </td>
                        <td> 
                            <a style="height:20px; padding:0px 5px 0 5px; background-color:#DD55C9; color:white;" data-toggle="modal" href="#kullanici_{$kullanici->id}" title="Sayfayı Düzenle" class="btn btn-sm btn-default">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>                                                
                        </td>
                        <td>  
                            <a style="height:20px; padding:0px 5px 0 5px; background-color:#E73434; color:white; border-radius:10px;" onclick="sil({$kullanici->id})" href="#" title="Sil" class="btn btn-sm btn-default">
                                <i class="fa fa-times"></i>
                            </a>                             
                        </td>
                    </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
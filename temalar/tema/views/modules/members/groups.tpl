{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row">
            <div class="col-md-12">
	            <div class="row" style="margin-bottom: 10px;">
	            	<div class="col-md-12">
	            		<a data-toggle="modal" href="#grup_ekle" class="btn red btn-outline sbold btn-sm pull-right"> 
			            	<i class="fa fa-plus"></i> 
			            	Üye / Bayi Grup Ekle  
			        	</a>
	            	</div>
	            </div>
				<div class="table-responsive">
					<table class="table table-striped table-hover table-bordered">
				        <thead>
				            <tr>                         
				                <th class="islem_bilgilendirme" style="text-align:right;" align="right"> 

				                    <span href="#" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
				                        <i class="fa fa-pencil-square-o"></i>
				                    </span> 
				                    <span id="islembilgisitext"> Düzenle  </span> 

				                    <span href="#" title="Sil" class="btn btn-sm btn-default sil">
				                        <i class="fa fa-times"></i>
				                    </span>
				                    <span id="islembilgisitext"> Sil </span>

				                </th> 
				            </tr>
				        </thead>
				    </table>
					
				    <table class="table table-striped table-hover table-bordered">
				        <thead>
				            <tr> 
				                <th width="1%"> ID </th>
				                <th> Adı </th> 
				                <th width="1%"></th>  
				                <th width="1%"></th>  
				            </tr>
				        </thead>
				        <tbody>
				        	{foreach $gruplar as $grup}
				            <tr> 
				                <td> {$grup->id} </td>
				                <td> {$grup->grup} </td> 
				                <td> 
				                	<a data-toggle="modal" data-target="#grup_duzenle_{$grup->id}" title="Düzenle" class="btn btn-sm btn-default hizliduzenle">
				                        <i class="fa fa-pencil-square-o"></i>
				            		</a>  
				            		<div class="modal fade" id="grup_duzenle_{$grup->id}" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
													<h4 class="modal-title">Üye / Bayi Grup Bilgisi Güncelleme</h4>
												</div>
												<form class="form-horizontal" id="form_grup_{$grup->id}" autocomplete="off" method="post" onsubmit="grup_duzenle('{$grup->id}');" action="javascript:void(0);"> 
													<div class="modal-body">
														
														<div class="row">
															<div class="col-md-12">
															    <div class="form-body">
															        <div class="row">
															        	<div class="col-md-12" id="form_cevabi_{$grup->id}"></div>
															        </div>

															        <div class="form-group row" style="margin-bottom: 10px !important;">
															            <label class="col-md-4 control-label text-right">Adı: </label>
															            <div class="col-md-8">
															                <input type="text" class="form-control" name="grup" placeholder="" value="{$grup->grup}" />                                                
															            </div>
															        </div>
																	{$data = json_decode($grup->data)}
															        <div class="form-group row" style="margin-bottom: 10px !important;">
															            <label class="col-md-4 control-label text-right">Fiyat Türü: </label>
															            <div class="col-md-8">
															                <select name="data[fiyat_turu]" class="form-control">
															                	<option value="fiyat" {if $data->fiyat_turu == "fiyat"}selected{/if}> Fiyat 1 </option>
															                	<option value="fiyat_2" {if $data->fiyat_turu == "fiyat_2"}selected{/if}> Fiyat 2 </option>
															                	<option value="fiyat_3" {if $data->fiyat_turu == "fiyat_3"}selected{/if}> Fiyat 3 </option>
															                </select>                                                
															            </div>
															        </div>

															        <div class="form-group row" style="margin-bottom: 10px !important;">
															            <label class="col-md-4 control-label text-right">Ödeme Sistemi: </label>
															            <div class="col-md-8">
																            <label for="odeme_hayir_{$grup->id}">
																               	<input type="radio" class="form-control" {if $data->odeme_sistemi == "0"}checked{/if} name="data[odeme_sistemi]" value="0"> Hayır                                               
																            </label>
																            <label for="odeme_evet_{$grup->id}">
																               	<input type="radio" class="form-control" {if $data->odeme_sistemi == "1"}checked{/if} name="data[odeme_sistemi]" value="1"> Evet                                               
																            </label>
															            </div>
															        </div>


															        <div class="form-group row" style="margin-bottom: 10px !important;">
															            <label class="col-md-4 control-label text-right">Ödeme Seçenekleri: </label>
															            <div class="col-md-8" style="padding-top:8px;">
															            	{foreach $odemetipleri as $odeme_tipi}
															            		{$chck = ""}
															            		{if count($data->odeme_tipi) > 0}
															            			{foreach $data->odeme_tipi as $odvarmi}
															            				{if $odvarmi == $odeme_tipi->id}
															            					{$chck = "checked"}
															            				{/if}
															            			{/foreach}
															            		{/if}
															            		<input type="checkbox" name="data[odeme_tipi][]" {$chck} value="{$odeme_tipi->id}"> {$odeme_tipi->odeme_tipi} <br/>
															            	{/foreach}
															            </div>
															        </div>								        
															    </div>
															</div>
														</div>							
													</div>

													<div class="modal-footer">
														<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
														<button type="submit" class="btn blue">Kaydet</button>
													</div>
												</form>	
											</div>
										</div>
									</div>
				        		</td>  
				                <td> 
				                	{if $grup->uyesayisi > 0}
				            			Üye / Bayi Var
				            		{else}
					                	<a href="{site_url('members/removegroup')}/{$grup->id}" onclick="return confirm('Silmek istediğinizden emin misiniz?');" title="Sil" class="btn btn-sm btn-default sil">
					                        <i class="fa fa-times"></i>
					            		</a>
				            		{/if}
				        		</td>  
				            </tr>
				        	{/foreach}
				        </tbody>
				    </table>
				</div>

			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="grup_ekle" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Grup ekleme</h4>
			</div>
			<form class="form-horizontal" id="form_grup_ekle" autocomplete="off" method="post" onsubmit="grup_ekle();" action="javascript:void(0);"> 
				<div class="modal-body">
					
					<div class="row">
						<div class="col-md-12">
						    <div class="form-body">
						        <div class="row">
						        	<div class="col-md-12" id="form_cevabi_ekle"></div>
						        </div>

						        <div class="form-group row" style="margin-bottom: 10px !important;">
						            <label class="col-md-4 control-label text-right">Adı: </label>
						            <div class="col-md-8">
						                <input type="text" class="form-control" name="grup" placeholder="" value="" />                                                
						            </div>
						        </div>

						        <div class="form-group row" style="margin-bottom: 10px !important;">
						            <label class="col-md-4 control-label text-right">Fiyat Türü: </label>
						            <div class="col-md-8">
						                <select name="data[fiyat_turu]" class="form-control">
						                	<option value="fiyat"> Fiyat 1 </option>
						                	<option value="fiyat_2"> Fiyat 2 </option>
						                	<option value="fiyat_3"> Fiyat 3 </option>
						                </select>                                                
						            </div>
						        </div>

						        <div class="form-group row" style="margin-bottom: 10px !important;">
						            <label class="col-md-4 control-label text-right">Ödeme Sistemi: </label>
						            <div class="col-md-8">
							            <label for="odeme_hayir_{$grup->id}">
							               	<input type="radio" class="form-control" name="data[odeme_sistemi]" value="0"> Hayır                                               
							            </label>
							            <label for="odeme_evet_{$grup->id}">
							               	<input type="radio" class="form-control" name="data[odeme_sistemi]" value="1"> Evet                                               
							            </label>
						            </div>
						        </div>


						        <div class="form-group row" style="margin-bottom: 10px !important;">
						            <label class="col-md-4 control-label text-right">Ödeme Seçenekleri: </label>
						            <div class="col-md-8" style="padding-top:8px;">
						            	{foreach $odemetipleri as $odeme_tipi}
						            		<input type="checkbox" name="data[odeme_tipi][]" value="{$odeme_tipi->id}"> {$odeme_tipi->odeme_tipi} <br/>
						            	{/foreach}
						            </div>
						        </div>								        
						    </div>
						</div>
					</div>							
				</div>

				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
					<button type="submit" class="btn blue">Kaydet</button>
				</div>
			</form>	
		</div>
	</div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
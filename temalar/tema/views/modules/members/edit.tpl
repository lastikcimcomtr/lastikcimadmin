{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row">
            <div class="col-md-12"> 
                
                <form class="form-horizontal form-row-seperated" autocomplete="off" method="post" action="">
                    <div class="portlet">
                        <div class="portlet-body">
                            <div class="tabbable-bordered"> 
                                <ul class="nav nav-tabs" id="tab_basliklari"> 
                                    <li class="active">
                                        <a href="#tab_parametre11" data-toggle="tab"> Detaylı Düzenleme </a>
                                    </li> 
                                </ul>
                                <div class="tab-content">                              
                                    <div class="tab-pane active" id="tab_parametre11">
                                        <div class="form-body">
                                            {if $status && $message}
                                            <div class="form-group">
                                                <div class="note note-{$status}"> <p> {$message} </p> </div>
                                            </div>
                                            {/if}
                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Adı :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="adi" placeholder="" value="{$uye->adi}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Soyadı :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="soyadi" placeholder="" value="{$uye->soyadi}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    E-Mail :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="email" placeholder="" value="{$uye->email}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Doğum Tarihi :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control"  name="dogumtarihi" placeholder="" value="{$uye->dogumtarihi}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Cinsiyet :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <select class="form-control" name="cinsiyet" id="">
                                                        <option value="">Seçiniz</option>
                                                        <option value="bay" {if $uye->cinsiyet == "bay"}selected{/if}>Bay</option>
                                                        <option value="bayan" {if $uye->cinsiyet == "bayan"}selected{/if}>Bayan</option>
                                                    </select> 
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Telefon :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="telefon" placeholder="" value="{$uye->telefon}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Cep Telefonu :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="ceptelefonu" placeholder="" value="{$uye->ceptelefonu}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Semt :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="semt" placeholder="" value="{$uye->semt}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Şehir :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="sehir" placeholder="" value="{$uye->sehir}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Diğer Şehir :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="diger_sehir" placeholder="" value="{$uye->diger_sehir}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    İlçe :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="ilce" placeholder="" value="{$uye->ilce}" />
                                                </div>
                                            </div>

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Adres :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <textarea class="form-control" name="adres">{$uye->adres}</textarea>
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Posta Kodu :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="posta_kodu" placeholder="" value="{$uye->posta_kodu}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Ticari Ünvan :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="ticari_unvan" placeholder="" value="{$uye->ticari_unvan}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Vergi No :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="vergi_no" placeholder="" value="{$uye->vergi_no}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Vergi Dairesi :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="vergi_dairesi" placeholder="" value="{$uye->vergi_dairesi}" />
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    T.C. Kimlik No :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="tc_kimlik_no" placeholder="" value="{$uye->tc_kimlik_no}" />
                                                </div>
                                            </div>

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Şifre :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <input type="text" class="form-control" name="sifre" placeholder="Şifre" value="" />
                                                    
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Üye Grubu :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <select style="height:34px;" name="grup" class="form-control form-filter input-sm required_chk" required="required">
                                                        <option value="">Seçiniz</option>
                                                        {foreach $gruplar as $grup}
                                                            <option {if $grup->id == $uye->grup}selected{/if} value="{$grup->id}">{$grup->grup}</option>
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Kayıt Tarihi :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz" style="padding-top: 8px;">
                                                    {date("d/m/Y h:i:s", $uye->kayit_tarihi)}
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Son Giriş :
                                                </label> 
                                                <div class="col-md-2" id="kategorilerimiz" style="padding-top: 8px;">
                                                    {if $uye->son_giris}{date("d/m/Y", $uye->son_giris)}{else}Yapılmamış{/if}
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> 
                                                    Mail Listesi :
                                                </label> 
                                                <div class="col-md-4" id="kategorilerimiz" style="padding-top: 8px;">
                                                    <input type="checkbox" class="form-control" name="mail_listesi" {if $uye->mail_listesi == 1}checked{/if} placeholder="" value="1" >
                                                    Kampanyalardan haberdar olmak istiyorum
                                                </div>
                                            </div> 

                                            <div class="form-group"> 
                                                <label class="col-md-2 control-label" style=""> </label> 
                                                <div class="col-md-2" id="kategorilerimiz">
                                                    <button type="submit" class="btn blue" onclick="kaydi_guncelle();">Kaydet</button>
                                                </div>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{include file="base/header.tpl"}
{$markalar = $this->brand->markalar()}
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Arama
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="Aç / Kapat"> </a> 
                        </div>
                    </div>
                    <div class="portlet-body tabs-below" >
                        <form class="form-horizontal form-row-seperated" method="post" action="{site_url('members/crm')}">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Personel</label>
                                    <div class="col-md-7" >
                                        {$personeller = $this->members->personeller()}
                                        <select id="" name="personel" class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            {foreach $personeller as $pers}
                                                <option value="{$pers->id}" {if $personel == $pers->id}selected='selected'{/if}>{$pers->kullanici} ({$pers->ad} {$pers->soyad})</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Tarih Aralığı</label>
                                    <div class="col-md-7" >                                                                       
                                        <input type="text" class="form-control" autocomplete = "off" name="tarih"  {if $tarih}value="{$tarih[0]}-{$tarih[1]}"{/if}>                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">İsim</label>
                                    <div class="col-md-7" >                                                                       
                                        <input type="text" class="form-control" autocomplete = "off" name="isim" placeholder="" value="{$isim}" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Email</label>
                                    <div class="col-md-7" >                                                                        
                                        <input type="text" class="form-control" autocomplete = "off" name="email" placeholder="" value="{$email}" >                     
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">Telefon</label>
                                    <div class="col-md-7" >                                                                         
                                        <input type="text" class="form-control" autocomplete = "off"  name="telefon" placeholder="" value="{$telefon}" >                                                         </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-5 control-label"></label>
                                        <div class="col-md-7" style="padding-right: 0px">         
                                            <button type="submit" class="btn" style="background-color: #74a921;border-color: #74a921;width: 100%;color: white;">Arama Yapın</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">

                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th  style="border-right: 0">

                                  <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#kart_ekle">
                                    <i class="fa fa-plus"></i>  
                                    Müşteri Kartı Ekle 
                                </a>

                            </th> 
                            <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
                               <span class="btn btn-sm btn-default detayliduzenle"><i class="fa fa-pencil-square-o"></i></span>      
                               <span id="islembilgisitext"> Müşteri Cevapları </span>

                               <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                               <span id="islembilgisitext"> Sil </span>
                           </th>
                       </tr>
                   </thead>
               </table>

               <div class="portlet-body">
                <div class="tabbable-bordered">

                    <div class="tab-content">                               
                        <div class="tab-pane active" id="tab_kategoriler">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="1%">ID </th> 
                                            <th>Adı - Soyadı </th> 
                                            <th>Telefon</th>
                                            <th>Eposta</th>
                                            <th>Tarih</th>
                                            <th>Personel</th>
                                            <th width="1%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {if count($kartlar) > 0}

                                        {foreach $kartlar as $kart}
                                        <tr>
                                            <td>{$kart->id}</td> 
                                            <td>{$kart->adi} {$kart->soyadi}</td> 
                                            <td>{$kart->telefon}</td>
                                            <td>{$kart->email}</td>
                                            <td>{date("d-m-Y H:m",$kart->tarih)}</td>

                                            <td>{$kart->personel_adi}</td>

                                            {$sorular = $this->members->crm_cevaplar($kart->id)}

                                            <td align="center">                                                 
                                                <a href="#crmsoru_{$kart->id}"  data-toggle="modal" class="btn btn-sm btn-default detayliduzenle" title="Cevaplar">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>
                                                <div class="modal fade in" id="crmsoru_{$kart->id}" tabindex="-1" role="soru_ekle" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                      <div class="modal-content" style="max-width: 500px">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Müşteri Kartı Ekle</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="tab-pane">
                                                                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('members/crm_kart_duzenle')}/{$kart->id}">
                                                                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                                                                        <label class="col-md-4 control-label" style="text-align: left;">Adı:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-8" >
                                                                            <input type="text" class="form-control" name="adi" placeholder=""  value="{$kart->adi}" required="required">                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                                                                        <label class="col-md-4 control-label"  style="text-align: left;">Soyadi:

                                                                        </label>
                                                                        <div class="col-md-8" >
                                                                            <input type="text" class="form-control" name="soyadi" value="{$kart->soyadi}" placeholder=""  >                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                                                                        <label class="col-md-4 control-label"  style="text-align: left;">EPosta:

                                                                        </label>
                                                                        <div class="col-md-8" >
                                                                            <input type="email" class="form-control" name="email" value="{$kart->email}" placeholder=""  >                                          
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                                                                        <label class="col-md-4 control-label"  style="text-align: left;">Telefon:
                                                                            <span class="required"> * </span>
                                                                        </label>
                                                                        <div class="col-md-8" >
                                                                            <input type="text" class="form-control"  data-mask="0(000) 000 00 00" value="{$kart->telefon}"   name="telefon" placeholder=""  required="required" >                                          
                                                                        </div>
                                                                    </div>
                                                                    <hr/>
                                                                    {foreach from=$sorular item=soru }
                                                                    <div class="form-group" style="margin:auto;">
                                                                        <label class="col-md-12 control-label" style="text-align: left;margin-bottom: 5px;">{$soru->soru}</label>
                                                                        <div class="col-md-12"    style="text-align: left;">
                                                                            {if $soru->soru_tip == "e-h"}
                                                                            <div class="input-group">
                                                                                <div class="icheck-inline">
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-green" value="1" {if $soru->cevap == 1} checked {/if}> Evet 
                                                                                    </label>
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]"  {if $soru->cevap == 0} checked {/if} class="icheck" data-radio="iradio_flat-red" value="0"> Hayır
                                                                                    </label>

                                                                                </div>
                                                                            </div>
                                                                            {elseif $soru->soru_tip == "puan"}

                                                                            <div class="input-group">
                                                                                <div class="icheck-inline">
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" {if $soru->cevap == 1} checked {/if} data-radio="iradio_flat-red" value="1"> 1 
                                                                                    </label>
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-grey" {if $soru->cevap == 2} checked {/if} value="2"> 2 
                                                                                    </label>
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-aero" {if $soru->cevap == 3} checked {/if} value="3"> 3 
                                                                                    </label>
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-blue" {if $soru->cevap == 4} checked {/if} value="4"> 4 
                                                                                    </label>
                                                                                    <label>
                                                                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-green"{if $soru->cevap == 5} checked {/if} value="5"> 5 
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                            {elseif $soru->soru_tip == "marka"}
                                                                            <select class="form-control" name="soru[{$soru->id}]">,
                                                                               <option value="">Seçiniz</option>
                                                                               {foreach from=$markalar item=marka}
                                                                               <option value="{$marka->adi}" {if $soru->cevap == $marka->adi} selected {/if}>{$marka->adi}</option>
                                                                               {/foreach}
                                                                           </select>
                                                                           {else}
                                                                           <input type="text" class="form-control" name="soru[{$soru->id}]"  value="{$soru->cevap}">
                                                                           {/if}
                                                                       </div>
                                                                   </div>
                                                                   {/foreach}


                                                                   <div class="form-group">
                                                                    <label class="col-md-4 control-label"></label>
                                                                    <div class="col-md-12" id="kategorilerimiz" style="text-align: right;padding-right: 30px;padding-top: 13px;">         
                                                                        <button type="submit" class="btn blue">Müşteri Kartı Düzenleme</button> 
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div> 

                                        <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="{site_url('members/crm_kart_remove')}/{$kart->id}" title="Sil" class="btn btn-sm btn-default sil">
                                            <i class="fa fa-times-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                                {/foreach}
                                {else}
                                <tr>
                                    <td colspan='10' style='text-align:center;'>Müşteri Kartı Yok.</td>
                                </tr>
                                {/if}
                            </tbody>
                        </table>
                    </div> 
                </div>                             
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- END PAGE BASE CONTENT -->

<div class="modal fade in" id="kart_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="max-width: 500px">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Müşteri Kartı Ekle</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane">
                <form class="form-horizontal form-row-seperated" method="post" action="{site_url('members/crm_kart_ekle')}">
                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                        <label class="col-md-4 control-label" style="text-align: left;">Adı:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="adi" placeholder=""  required="required">                                          
                        </div>
                    </div>
                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                        <label class="col-md-4 control-label"  style="text-align: left;">Soyadi:

                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control" name="soyadi" placeholder=""  >                                          
                        </div>
                    </div>
                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                        <label class="col-md-4 control-label"  style="text-align: left;">EPosta:

                        </label>
                        <div class="col-md-8" >
                            <input type="email" class="form-control" name="email" placeholder=""  >                                          
                        </div>
                    </div>
                    <div class="form-group" style="margin:auto;    padding-bottom: 10px;">
                        <label class="col-md-4 control-label"  style="text-align: left;">Telefon:
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-8" >
                            <input type="text" class="form-control"  data-mask="0(000) 000 00 00"  name="Telefon" placeholder=""  required="required" >                                          
                        </div>
                    </div>
                    <hr/>
                    {$sorular = $this->members->crm_sorular()}
                    {foreach from=$sorular item=soru }
                    <div class="form-group" style="margin:auto;">
                        <label class="col-md-12 control-label" style="text-align: left;margin-bottom: 5px;">{$soru->soru}</label>
                        <div class="col-md-12">
                            {if $soru->soru_tip == "e-h"}
                            <div class="input-group">
                                <div class="icheck-inline">
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-green" value="1"> Evet 
                                    </label>
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" checked class="icheck" data-radio="iradio_flat-red" value="0"> Hayır
                                    </label>

                                </div>
                            </div>
                            {elseif $soru->soru_tip == "puan"}

                            <div class="input-group">
                                <div class="icheck-inline">
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-red" value="1"> 1 
                                    </label>
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-grey" value="2"> 2 
                                    </label>
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-aero" value="3"> 3 
                                    </label>
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-blue" value="4"> 4 
                                    </label>
                                    <label>
                                        <input type="radio" name="soru[{$soru->id}]" class="icheck" data-radio="iradio_flat-green" value="5"> 5 
                                    </label>
                                </div>
                            </div>
                            {elseif $soru->soru_tip == "marka"}
                            <select class="form-control" name="soru[{$soru->id}]">,
                               <option value="">Seçiniz</option>
                               {foreach from=$markalar item=marka}
                               <option value="{$marka->adi}">{$marka->adi}</option>
                               {/foreach}
                           </select>
                           {else}
                           <input type="text" class="form-control" name="soru[{$soru->id}]"  >
                           {/if}
                       </div>
                   </div>
                   {/foreach}


                   <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-12" id="kategorilerimiz" style="text-align: right;padding-right: 30px;padding-top: 13px;">         
                        <button type="submit" class="btn blue">Müşteri Kartı Ekle</button> 
                    </div>
                </div>
            </form>
        </div> 
    </div> 
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
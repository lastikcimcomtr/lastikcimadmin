{include file="base/header.tpl"}
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row">
            <div class="col-md-12"> 
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th class="islem_bilgilendirme" style="text-align:right;" align="right">
                                    
                                    <span class="btn btn-circle btn-icon-only btn-default durumu" title="">
                                        <i class=""></i>
                                    </span>
                                    <span id="islembilgisitext"> Üye / Bayi Durumu </span>
                                    
                                    <span class="btn btn-circle btn-icon-only btn-default populerurun" title="">
                                        <i class=""></i>
                                    </span>
                                    <span id="islembilgisitext"> Üye / Bayi Aktivasyon </span>

                                    <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default hizliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Düzenle  </span>

                                    <span href="#" title="Sipariş Detayları" class="btn btn-sm btn-default detayliduzenle">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </span> 
                                    <span id="islembilgisitext"> Hızlı Düzenle  </span>

                                    <span href="#" title="Sil" class="btn btn-sm btn-default sil">
                                        <i class="fa fa-times"></i>
                                    </span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th> 
                            </tr>
                        </thead>
                    </table>
                    
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr> 
                                <th width="1%"> ID </th>
                                <th> Adı </th>
                                <th> Soyadı  </th>
                                <th> Email </th>
                                <th> Kayıt Tarihi </th> 
                                <th> Son Giriş </th> 
                                <th> Üye Grubu </th> 
                                <th> Puan </th> 
                                <th> Mail Listesi </th> 
                                <th width="1%"> </th> 
                               	<th width="1%"> </th> 
                               	<th width="1%"> </th> 
                               	<th width="1%"> </th> 
                               	<th width="1%"> </th>  
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $uyeler as $uye} 
                            <tr> 
                                <td> {$uye->id} </td>
                                <td> {$uye->adi} </td>
                                <td> {$uye->soyadi}  </td>
                                <td> {$uye->email} </td>
                                <td> {date("d/m/Y h:i:s", $uye->kayit_tarihi)} </td> 
                                <td> {if $uye->son_giris}{date("d/m/Y", $uye->son_giris)}{else}Yapılmamış{/if} </td>  
                                <td> {$uye->grup_adi} </td> 
                                <td> 0 </td> 
                                <td> {if $uye->mail_listesi == 1}Evet{else}Hayır{/if} </td> 
                                <td> <a class="btn btn-circle btn-icon-only btn-default {if $uye->hesap_durumu == 1}durumu{else}beyaz{/if}" onclick="birsifir({$uye->id}, 'musteri_hesaplari', 'hesap_durumu', '#uyebayidurumu_{$uye->id}', 'durumu');" id="uyebayidurumu_{$uye->id}" title="Üye / Bayi Durumu"></a> </td> 
                                <td> <a class="btn btn-circle btn-icon-only btn-default {if $uye->aktivasyon == 1}populerurun{else}beyaz{/if}" onclick="birsifir({$uye->id}, 'musteri_hesaplari', 'aktivasyon', '#uyebayiaktivasyon_{$uye->id}', 'populerurun');" id="uyebayiaktivasyon_{$uye->id}" title="Üye / Bayi Aktivasyon"></a> </td> 
                                <td> <a data-toggle="modal" data-target="#kullanici_{$uye->id}" title="Hızlı Düzenle" class="btn btn-sm btn-default hizliduzenle"> <i class="fa fa-pencil-square-o"></i> </a>    
                                    <div class="modal fade" id="kullanici_{$uye->id}" tabindex="-1" role="basic" aria-hidden="true" data-width="500">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Üye Bilgisi Güncelleme</h4>
                                                </div>
                                                <form class="form-horizontal" id="form_kullanici_{$uye->id}" method="post" autocomplete="off" onsubmit="kullanici_duzenle('{$uye->id}');" action="javascript:void(0);">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-body">
                                                                    <div class="row">
                                                                        <div class="col-md-12" id="form_cevabi_{$uye->id}"></div>
                                                                    </div>

                                                                    <div class="form-group row" style="margin-bottom: 10px !important;">
                                                                        <label class="col-md-4 control-label text-right">Adı: </label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="adi" placeholder="" value="{$uye->adi}" />                                                
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Soyadı: </label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="soyadi" placeholder="" value="{$uye->soyadi}" />                                                
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Email: <span class="required"> * </span></label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control required_chk" name="email" required="required" placeholder="" value="{$uye->email}" />                                                
                                                                        </div>
                                                                    </div> 
                                                                    
                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Kullanıcı Grubu: <span class="required"> * </span></label>
                                                                        <div class="col-md-8">
                                                                            <select style="height:34px;" name="grup" class="form-control form-filter input-sm required_chk" required="required">
                                                                                <option value="">Seçiniz</option>
                                                                                {foreach $gruplar as $grup}
                                                                            		<option {if $grup->id == $uye->grup}selected{/if} value="{$grup->id}">{$grup->grup}</option>
                                                                            	{/foreach}
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Kayıt Tarihi: </label>
                                                                        <div class="col-md-8" style="margin-top: 8px;"> {date("d/m/Y h:i:s", $uye->kayit_tarihi)} </div>
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Son Giriş: </label>
                                                                        <div class="col-md-8" style="margin-top: 8px;"> {if $uye->son_giris}{date("d/m/Y", $uye->son_giris)}{else}Yapılmamış{/if} </div>
                                                                    </div> 
                                                                    
                                                                    <div class="form-group row" style="border-bottom: 1px solid #ededed;">
                                                                        <div class="col-md-12 text-left"> <h4 style="font-weight: bold !important;">Şifre Değişimi</h4></div>                                                       
                                                                    </div>

                                                                    <div class="form-group row">
                                                                        <label class="col-md-4 control-label text-right">Yeni Şifre: </label>
                                                                        <div class="col-md-8">
                                                                            <input type="text" class="form-control" name="sifre" placeholder="Yeni Şifre" value="" />                                                
                                                                        </div>
                                                                    </div>                                              

                                                                </div>
                                                            </div>
                                                        </div>                          
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Kapat</button>
                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                    </div>
                                                </form> 
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td> <a href="{site_url('members/edit')}/{$uye->id}" title="Detaylı Düzenle" class="btn btn-sm btn-default detayliduzenle"> <i class="fa fa-pencil-square-o"></i> </a>                                                
                                </td>
                                <td> <a href="{site_url('members/remove')}/{$uye->id}" onclick="return confirm('Silmek istediğinizden emin misiniz?');" title="Sil" class="btn btn-sm btn-default sil"> <i class="fa fa-times"></i> </a> 
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
</div>
{include file="base/footer_txt.tpl"}
</div>
{include file="base/quicksidebar.tpl"}
{include file="base/footer.tpl"}
{$module = $this->router->fetch_class()}
{$method = $this->router->fetch_method()}

{if $module == "importers" && $method == "mikro"}
<script type="text/javascript">
    function coklurunformgonder() {
        $('div#cokluurunguncellemecevap').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        $('div#cokluurunguncellemecevap').show();

        var pdata = $('form#cokluurunguncelleme').serialize();

        $.ajax({
            type:'POST',
            url:'{site_url('importers/insert/mikro/'|cat:$secilikategori)}',
            data:pdata,
            dataType: 'json',
            success:function(cevap){
                // console.log(cevap);
                var mesaj = '<div class="note note-'+cevap.status+'"> <p> '+cevap.message+' </p> </div>';
                $('div#cokluurunguncellemecevap').html(mesaj);

                setTimeout(function() {
                    $('div#cokluurunguncellemecevap').hide('');
                    $('div#cokluurunguncellemecevap').html('');
                }, 5000);
            }
        });

    }
</script>
{/if}

{if $module == "news"}
<script type="text/javascript">
    $('[name="sef_url"]').focusout(function() {  sefurl('sef_url');  });
    function sefurl(veri){
        text = $('[name="'+veri+'"]').val();
        $.ajax({
            type:'POST',
            url:'{site_url('news/checkurl')}',
            data:"text=" +  text + "&id=" + {if $haber->id}{$haber->id}{else}0{/if},
            success:function(cevap){
                $('[name="sef_url"]').val(cevap);

            }
        });
    }
	$('.select2_altcat').select2({
		width: '100%'
	});

    this.resimonizleme = function(){
        /* Düzenleme */
        xOffset = 10;
        yOffset = 30;
        /* Düzenleme Sonu */
        $("a[rel*=popupacil]").hover(function(e){
            this.t = this.title;
            this.title = "";
            var c = (this.t != "") ? "<br />" + this.t : "";
            $("body").append("<p id='onizleme'><img src='"+ this.href +"' alt='Önizleme Resmi' style='max-width:300px; max-height:225px;'/>"+ c +"</p>");
            $("#onizleme")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px")
            .fadeIn();
        },
        function(){
            this.title = this.t;
            $("#onizleme").remove();
        });
        $("[rel*=popupacil]").mousemove(function(e){
            $("#onizleme")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px");
        });
    };

	 function init_select2(container) {
            container = $(container).closest('td').find('.modal');
            container.find('.etiket_select2').select2({
                width: '100%',
                ajax: {
                    url: '/ajax/geturunetiket',
                    dataType: 'json'
                }
            });
        }

        function etiket_ekle(element) {
            element = $(element);
            var id = element.closest('.form-group').find('select').val();
            var form = element.closest('form');
            var etiketarray = form.serializeArray();
            for (var i = 0; i < etiketarray.length; i++) {
                if (etiketarray[i].name === 'etiket_id[]')
                    if (parseInt(etiketarray[i].value) === parseInt(id)) {
                        return;
                    }
            }
            var text = element.closest('.form-group').find('select option:selected').text();
            var ul_main = form.find('ul');
            var li_new = '<li class="item"\n' +
                'style="border-bottom:1px dimgrey solid">\n' +
                text +
                '<button onclick="$(this).closest(\'li\').remove();" style="margin-top:3px"\n' +
                'class="btn btn-danger btn-xs pull-right">\n' +
                '<i class="fa fa-times"></i>\n' +
                '</button>\n' +
                '<input type="hidden" name="etiket_id[]" value="' + id + '">\n' +
                '</li>';
            $(li_new).appendTo(ul_main);
        }

        function save_haber_etiketler(element) {
            var form_value = $(element).closest('form').serialize();
            $.ajax({
                type: "POST",
                url: '/news/haber_etiket_kaydet',
                data: form_value,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#form_cevap_' + id).html('<div class="note note-success"> <p> Başarılı Bir Şekilde Güncellenmiştir. </p> </div>');
                    } else {
                        $('#form_cevap_' + id).html('<div class="note note-danger"> <p> İşlem sırasında hata meydana geldi. </p> </div>');
                    }
                }
            });
        }
    // Kodları başlangıçta çağıralım
    $(document).ready(function(){
        resimonizleme();
    });
</script>
{/if}

{if $module== "products" && $method == "updatemultiproducts"}

<script type="text/javascript">
    function coklurunformgonder() {
        $('div#cokluurunguncellemecevap').html('<center><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></center>');
        $('div#cokluurunguncellemecevap').show();

        var pdata = $('form#cokluurunguncelleme').serialize();

        $.ajax({
            type:'POST',
            url:'{site_url('products/updatemultiproductsdata')}',
            data:pdata,
            dataType: 'json',
            success:function(cevap){
                console.log(cevap);
                var mesaj = '<div class="note note-'+cevap.status+'"> <p> '+cevap.message+' </p> </div>';
                $('div#cokluurunguncellemecevap').html(mesaj);

                setTimeout(function() {
                    $('div#cokluurunguncellemecevap').hide('');
                    $('div#cokluurunguncellemecevap').html('');
                }, 5000);
            }
        });

    }
</script>

{/if}

{if $module == "products" && ($method == "add" || $method == "edit")}
<script type="text/javascript">

   function kategorimizdegisti() {
    console.log('kategorimizdegisti');
    {if $method == "edit"}
    {if $urun->desen}
    desenler('{$urun->tip}');
    {/if}
    {/if}

    kategori = $('select#kategori:last').val();
    if(kategori  > 0){
     $("[data-categories]").each(function(){
        catstring = $(this).data('categories');
        var n = catstring.indexOf('|'+kategori+'|')
        if (n == -1){
            $(this).hide();
            $(this).find('input, textarea, button, select').attr('disabled',true)
        }else{
            $(this).show();
            $(this).find('input, textarea, button, select').attr('disabled',false)
        }          

    });

     $("[data-gcategories]").each(function(){
        catstring = $(this).data('gcategories');
        var n = catstring.indexOf('|'+kategori+'|')
        if (n == -1){
            $(this).hide();
        }else{
            $(this).show();
        }                  

    }); 
 }

}

$(document).on('change', "select#kategori", function (){
   kategorimizdegisti();
});


{if $method == "edit"}
$(window).bind("load", function() { 
  kategorimizdegisti();
});
var ajaxbitti = 0; 
$(document).ajaxComplete(function () {
			// console.log('ajaxComplete');
            if (ajaxbitti < 10) {
                kategorimizdegisti();
                ajaxbitti = ajaxbitti+1;
            }
        });
{/if}
</script>
{/if}


{if ($module == "brand" && $method == "index" )}
<script type="text/javascript">
 $(document).on('change', '[name="fiyat_grup"]', function (){ 
     $.get( "{site_url('ajax/markagrup')}/"+  $(this).data('marka') + "/" +$(this).val() ).done(function( data ) {
        console.log(data);
        
    });

 });
</script>

{/if}

{if ($module == "products" && ($method == "index" || $method == "add" || $method == "edit" || $method == "updatemultiproducts")) || ($module == "dimension" && $method == "add" ) || ($module == "importers" && $method == "liste" ) || ($module == "brand" && $method == "patterns")}
<script type="text/javascript">


    this.resimonizleme = function(){
        /* Düzenleme */
        xOffset = 10;
        yOffset = 30;
        /* Düzenleme Sonu */
        $("a[rel*=popupacil]").hover(function(e){
            this.t = this.title;
            this.title = "";
            var c = (this.t != "") ? "<br />" + this.t : "";
            $("body").append("<p id='onizleme'><img src='"+ this.href +"' alt='Önizleme Resmi' style='max-width:300px; max-height:225px;'/>"+ c +"</p>");
            $("#onizleme")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px")
            .fadeIn();
        },
        function(){
            this.title = this.t;
            $("#onizleme").remove();
        });
        $("[rel*=popupacil]").mousemove(function(e){
            $("#onizleme")
            .css("top",(e.pageY - xOffset) + "px")
            .css("left",(e.pageX + yOffset) + "px");
        });
    };

    // Kodları başlangıçta çağıralım
    $(document).ready(function(){
        resimonizleme();
    });
    {if $method == "edit"}
    {$jqkategoriler = json_decode($urun->kategoriler)}
    var jqkategoriler = new Array({foreach $jqkategoriler as $jqkategori name=kategoriymis} {$jqkategori->id}{if !$smarty.foreach.kategoriymis.last} , {/if}{/foreach});
    {/if}

    function kategoriler(basamak, tip, name){
        $("div#basamak_"+(basamak+1)).remove();
        $("div#basamak_"+(basamak+2)).remove();
        $("div#basamak_"+(basamak+3)).remove();
        $("div#basamak_"+(basamak+4)).remove();
        var kategori = $('select#kategori:last').val();

        var alt_kat = $("div#alt_kategori").html();

        // $("#selected_kat").val(kategori); 
        basamak = basamak +1;

        if(alt_kat == "") {
            $("div#alt_kategori").append("<div id='basamak_"+(basamak)+"'></div>"); 
        } else {
            $("div#basamak_"+(basamak-1)).append("<div id='basamak_"+(basamak)+"'></div>"); 
        }
        $.ajaxSetup({ cache: false });
        if (kategori > 0){
            $.getJSON( "{site_url('json/kategoriler')}/"+kategori+"/"+tip, function( data ) {

                if(typeof data.kategoriler  !== 'undefined'){
                   if (data.kategoriler.length > 0) {
                    // var obj = jQuery.parseJSON(data);
                    var sonuc = "<br/>";
                    sonuc += '<select onchange="kategoriler('+(basamak)+', \'{$tip}\', \''+name+'\');" data-placeholder="Seçiniz" name="'+name+'" id="kategori" class="form-control form-filter input-sm select2me" required>';
                    sonuc += "<option value=''>Seçiniz</option>";
                    $.each(data.kategoriler, function(id, obj) { 
                        {if $method == "edit"}
                        if( $.inArray(parseInt(obj.id), jqkategoriler) !== -1 ) {
                          sonuc += "<option selected value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                      } else {
                          sonuc += "<option value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                      }

                      {else}
                      sonuc += "<option value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                      {/if}
                  });
                    sonuc += "</select>";
                    //$("div#alt_kategori").append(sonuc);
                    $("div#basamak_"+(basamak)+"").html(sonuc);
                    $('select#kategori').select2();
                    kategoriler(basamak, tip, name);
                    kategorimizdegisti();
                }
            }else{
                $("div#basamak_"+(basamak)+"").html("");

                kategoriler(basamak, tip, name);
            }
        });
        }

        
        {if $method == "edit"}
        	// kategorimizdegisti();
        	/*
        	if (basamak < 4) {
				$(document).ajaxComplete(function () {
		            kategoriler(basamak, '{$urun->tip}', 'parametreler[kategori]');
				});
        		
        	}
        	*/
            {/if}
        }

        function markalar(basamak, tip, name) {
            console.log(tip);
            $("div#basamakmarka_"+(basamak+1)).remove();

            var marka = $('select#marka:last').val();
            var alt_kat = $("div#alt_marka").html();

        // $("#selected_marka").val(kategori); 
        basamak = basamak +1;

        if(alt_kat == "") {
            $("div#alt_marka").append("<div id='basamakmarka_"+(basamak)+"'></div>"); 
        } else {
            $("div#basamakmarka_"+(basamak-1)).append("<div id='basamakmarka_"+(basamak)+"'></div>"); 
        }


        $.getJSON( "{site_url('json/markalar')}/"+marka+"/"+tip, function( data ) {
            console.log(data);
            if(typeof data.markalar !== "undefined"){
                if (data.markalar.length > 0) {
	                // var obj = jQuery.parseJSON(data);
	                var sonuc = "<br/>";
                   sonuc += '<select onchange="markalar('+(basamak)+', \'{$tip}\', \''+name+'\')" name="'+name+'" id="marka" class="form-control form-filter input-sm select2me" required>';
                   sonuc += "<option value=''>Seçiniz</option>";
                   $.each(data.markalar, function(id, obj) { 
                       sonuc += "<option value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                   });
                   sonuc += "</select>";
	                //$("div#alt_kategori").append(sonuc);
	                $("div#basamakmarka_"+(basamak)+"").html(sonuc);
	                $('select#marka').select2();
	            } else {
	            	desenler(tip);
	            	veriler('taban');
	            	veriler('yanak');
	            	veriler('jant_capi');
	            }
            }else{
                $("div#basamakmarka_"+(basamak)+"").html("");
            }
        });
    }

    function desenler(tip) {
    	var marka 		= $('select#marka:last').val();
    	var kategori 	= $('select#kategori:last').val();
    	var mevsim		= $('select#mevsim:last').val();

        {if $method == "edit"}
        {if $urun->desen}
        var deseni = {$urun->desen};
        {/if}
        {/if}

        $.getJSON( "{site_url('json/desenler')}/"+marka+"/"+kategori+"/"+tip+"/"+mevsim, function( data ) {
        	// console.log(data);
            if(typeof data.desenler !== "undefined"){
                if (data.desenler.length > 0) {
                   var sonuc = "";
                   sonuc += "<option value=''>Seçiniz</option>";
                   $.each(data.desenler, function(id, obj) { 

                    {if $method == "edit" && $urun->desen}
                    if (deseni == obj.id) {
                        sonuc += "<option selected value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                    } else {
                        sonuc += "<option value=\'"+obj.id+"\'>"+obj.adi+"</option>";
                    }
                    {else}                            
                    sonuc += "<option value=\'"+obj.id+"\'>"+obj.adi+"</option>";                        
                    {/if}

                });
                   $('select#desen').html(sonuc);
                   $('select#desen').select2();
               }else{
                 $('select#desen').html('');
                 $('select#desen').select2();
             }
         }
     });
    }

    function veriler(veritip){
        var kategori 	= $('select#kategori:last').val();
        $.getJSON( "{site_url('json/veriler')}/"+kategori+"/"+veritip, function( data ) {
        	// console.log(data);
            if(typeof data.veriler !== "undefined"){
                if (data.veriler.length > 0) {
                   var sonuc = "";
                   sonuc += "<option value=''>Seçiniz</option>";
                   $.each(data.veriler, function(id, obj) { 
                    sonuc += "<option value=\'"+obj.id+"\'>"+obj.deger+"</option>";
                });
                   $('select#'+veritip).html(sonuc);
                   $('select#'+veritip).select2();
               }
           }
       });
    }

    $( "body select:not(.etiket_select2)" ).change(function() { isim_olustur(); });
    $( "body input" ).change(function() { isim_olustur(); }); 

    function isim_olustur(){

        var send_data = {
            kategori: $('select#kategori:last').val()
        }

        send_data['marka']      = $('select#marka:last').val();
        send_data['desen']      = $('select#desen:last').val();
        send_data['run_flat']   = $('select#run_flat:last').val();
        send_data['seal']       = $('select#seal:last').val();
        send_data['taban']      = $('select#taban:last').val();
        send_data['yanak']      = $('select#yanak:last').val();
        send_data['yapisi']     = $('select#yapisi').val();
        send_data['jant_capi']  = $('select#jant_capi:last').val();
        send_data['xl_rf']       = $('select#xl_rf:last').val();
        send_data['m_s']         = $('select#m_s:last').val();
        send_data['mevsim']      = $('select#mevsim:last').val();
        send_data['yuk_endeksi'] = $('input#yuk_endeksi').val();
        send_data['hiz_endeksi'] = $('input#hiz_endeksi').val();
        send_data['oem']        = $('input#oem').val();
        send_data['grnx']       = $('input#grnx').val();
        send_data['ebat']       = $('input#ebat').val();
        send_data['et']       = $('input#et').val();
        send_data['model']       = $('input#model').val();
        send_data['pcd']       = $('input#pcd').val();
        send_data['renk']       = $('input#renk').val();

        $.get( "{site_url('ajax/urunisim')}", send_data ).done(function( data ) {
            var veri = JSON.parse(data);
            console.log(veri);
            if ($("#urun_adi_otomatik").val() == 1) {
                $("#urun_adi").val(veri['urun_adi']);
            }

             if ($('input[name="parametreler[url_duzenle]"]').val() == ''  {if $method == "add"}  || 1 == 1 {/if}){
                $('input[name="parametreler[url_duzenle]"]').val(veri['url_duzenle']);
             }
                 if ($('#seo_baslik').val() == ''){
               $('#seo_baslik').val(veri['seo_baslik']);
             }
        });


    }





    function kargo_disable(){
        var islem = $("#kargo_sistem");

        if(islem.val() == "0"){
            islem.val("1");
            $("#kargo_bedeli").attr("readonly", true);
            $("#kargo_bedeli").val("");

            $('select#kargo_bedeli_birim').attr('disabled', true);
        }else{
            islem.val("0");
            $("#kargo_bedeli").attr("readonly", false);
            $("#kargo_bedeli").val("");

            $('select#kargo_bedeli_birim').attr('disabled', false);
        }
    }

    function urun_adi_disable(){
        var islem = $("#urun_adi_otomatik");

        if(islem.val() == "0"){
            islem.val("1");
            $("input#urun_adi").attr("readonly", true);
            $("input#urun_adi").val("");
        }else{
            islem.val("0");
            $("input#urun_adi").attr("readonly", false);
            $("input#urun_adi").val("");
        }
    }

    {if $method == "edit"}
    kategoriler(1, '{$urun->tip}', 'parametreler[kategori]');
    {else if $method == "add"}
    kategoriler(1, 'lastik', 'parametreler[kategori]');
    {/if}
</script>
{/if}

{if $module == "members" && $method == "index"}
<script type="text/javascript">
    function kullanici_duzenle(id) {
        var pdata = $('form#form_kullanici_'+id).serialize();

        $.ajax({
            type:'POST',
            url:'{site_url('members/fastedit')}/'+id,
            data:pdata,
            dataType: 'json',
            success:function(cevap){

                var mesaj = '<div class="note note-'+cevap.status+'"> <p> '+cevap.message+' </p> </div>';
                $('div#form_cevabi_'+id).html(mesaj);
                $('div#form_cevabi_'+id).show();

                setTimeout(function() {
                    $('div#form_cevabi_'+id).hide('');
                    $('div#form_cevabi_'+id).html('');
                }, 5000);

            }
        });
    }
</script>
{/if}


{if ($module == "members" && $method == "crm") || $module == "sayac"}
<script type="text/javascript">
    $(function() {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        $('input[name="tarih"]').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
             'Bugün': [moment(), moment()],
             'Dün': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Son 7 Gün': [moment().subtract(6, 'days'), moment()],
             'Son 30 Gün': [moment().subtract(29, 'days'), moment()],
             'Bu Ay': [moment().startOf('month'), moment().endOf('month')],
             'Geçen Ay': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
         }
     }, cb);

        cb(start, end);
        
    });
</script>
{/if}

{if $module == "members" && $method == "groups"}
<script type="text/javascript">
    function grup_duzenle(id) {
        var pdata = $('form#form_grup_'+id).serialize();

        $.ajax({
            type:'POST',
            url:'{site_url('members/editgroup')}/'+id,
            data:pdata,
            dataType: 'json',
            success:function(cevap){

                var mesaj = '<div class="note note-'+cevap.status+'"> <p> '+cevap.message+' </p> </div>';
                $('div#form_cevabi_'+id).html(mesaj);
                $('div#form_cevabi_'+id).show();

                setTimeout(function() {
                    $('div#form_cevabi_'+id).hide('');
                    $('div#form_cevabi_'+id).html('');
                }, 5000);

            }
        });
    }

    function grup_ekle() {
        var pdata = $('form#form_grup_ekle').serialize();

        $.ajax({
            type:'POST',
            url:'{site_url('members/addgrup')}',
            data:pdata,
            dataType: 'json',
            success:function(cevap){

                var mesaj = '<div class="note note-'+cevap.status+'"> <p> '+cevap.message+' </p> </div>';
                $('div#form_cevabi_ekle').html(mesaj);
                $('div#form_cevabi_ekle').show();

                setTimeout(function() {
                    $('div#form_cevabi_ekle').hide('');
                    $('div#form_cevabi_ekle').html('');
                }, 5000);

            }
        });
    }
</script>
{/if}

{if $module == "users" && $method == "index"}
<script type="text/javascript">
    function kullanici_duzenle(formid) {
        var formdata = $("#form_kullanici_"+formid).serialize();
        if (formdata) {
            $.getJSON( "{site_url('users/edit')}?"+formdata, function( data ) { 
                // alert(data);
                if (data > 0) {
                 swal("Başarılı","Kullanıcı başarıyla düzenlendi","success");
                 $('.modal').modal('hide');

             } else {
                 swal("Hata","Kullanıcı düzenlenirken Hata Oldu","error");  
                 $('.modal').modal('hide'); 
             }
         });
        }
    }

    function kullanici_ekle() {
        var formdata = $('#form_kullanici_ekle').serialize();
        if (formdata) {
            $.getJSON( "{site_url('users/add')}?"+formdata, function( data ) { 
                // console.log(data);
                if (data > 0) {
                    swal("Başarılı","Kullanıcı başarıyla eklendi","success");
                    $('.modal').modal('hide');
                    
                } else {
                    swal("Hata","Kullanıcı Eklenirken Hata Oldu","error"); 
                    $('.modal').modal('hide'); 
                    // $('.modal').modal('hide');                 
                }
            });
        }
    }  


    function sil(id){
        swal({
          title: 'Dikkat!',
          text: "Kullanıcıyı silmek istediğinize emin misiniz?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Evet Sil!'
      }).then(function () {
          $.getJSON( "{site_url('users/delete')}/"+id, function( data ) { 
            if (data > 0) {
                swal("Başarılı","Kullanıcı başarıyla silindi","success"); 
                $('#kt_'+id).remove();                                 
            } else {
                swal("Hata","Kullanıcı Silinirken Hata Oldu","error");                                 
            }
        });
      })
  }
</script>
{/if}

{if $module == "groups" && $method == "index"}
<script type="text/javascript">
    function grup_duzenle(formid) {
        var formdata = $("#form_grup_"+formid).serialize();
        if (formdata) {
            $.getJSON( "{site_url('users/groups/edit')}?"+formdata, function( data ) { 
                // alert(data);
                if (data > 0) {
                 swal("Başarılı","Kullanıcı başarıyla düzenlendi","success");
                 $('.modal').modal('hide');

             } else {
                 swal("Hata","Kullanıcı düzenlenirken Hata Oldu","error");  
                 $('.modal').modal('hide'); 
             }
         });
        }
    }

    function grup_ekle() {
        var formdata = $('#form_grup_ekle').serialize();
        if (formdata) {
            $.getJSON( "{site_url('users/groups/add')}?"+formdata, function( data ) { 
                // console.log(data);
                if (data > 0) {
                    swal("Başarılı","Kullanıcı başarıyla eklendi","success");
                    $('.modal').modal('hide');
                    
                } else {
                    swal("Hata","Kullanıcı Eklenirken Hata Oldu","error"); 
                    $('.modal').modal('hide'); 
                    // $('.modal').modal('hide');                 
                }
            });
        }
    }  


    function sil(id){
        swal({
          title: 'Dikkat!',
          text: "Kullanıcıyı silmek istediğinize emin misiniz?",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Evet Sil!'
      }).then(function () {
          $.getJSON( "{site_url('users/groups/delete')}/"+id, function( data ) { 
            if (data > 0) {
                swal("Başarılı","Kullanıcı başarıyla silindi","success"); 
                $('#kt_'+id).remove();                                 
            } else {
                swal("Hata","Kullanıcı Silinirken Hata Oldu","error");                                 
            }
        });
      })
  }
</script>
{/if}

{if $module == "roles" && $method == "index"}
<script type="text/javascript">
    function yetki_edit(rolid,rol){

        deger = $('#' + rol + '_' + rolid).data('deger');
        deger = 1 - deger;
        $.getJSON( "{site_url('users/roles/edit/'|cat:$type|cat:'/'|cat:$sid)}/" + rolid + '/' + rol + '/'+ deger, function( data ) { 
            if (data > 0) {
                if(deger){
                    renk = $('#' + rol + '_' + rolid).data('bg');
                    $('#' + rol + '_' + rolid).css("background-color", renk);
                }else{
                    $('#' + rol + '_' + rolid).css("background-color", "#fff");
                }
                $('#' + rol + '_' + rolid).data('deger',deger);
            } else {
               swal("Hata"," yetkide hata oldu","error");  

           }
       });
    }
</script>
{/if}


{if $module == "category" && $method == "index"}
<script type="text/javascript">
    function parametreDuzenle(veri, cat){
        var sfrom = $("#" + veri );
        var url = "{site_url('products/category/editparams')}/" +  cat ; 
        $.ajax({
            type: "POST",
            url: url,
            data: sfrom.serialize(), 
            success: function(data)
            {
                if (data=='ok'){
                    swal ( "Başarılı" ,  "Kategori Parametreleri Güncellendi" ,  "success" );
                    $('.modal').modal('hide');
                }else{
                    swal ( "Hata" ,  "Kategori Parametreleri Güncellenirken Sorun Oluştu!" ,  "error" );
                }
            }
        });
    }
</script>
{/if}

{if $module == "importers" && $method == "liste" }
<script type="text/javascript">
    function veri_ekle(){
        ust = $('select#kategori:last').val();
        if(ust > 0){
            window.location.href = '{site_url("importers/liste/"|cat:$id)}/' +  ust ;
        }
    }

    $("#checkAll").click(function(){
        $('.icheck').not(this).prop('checked', this.checked);
        console.log('girdi');

    });

</script>
{/if}
{if $module == "importers" && $method == "excel" }
<script type="text/javascript">
    function urunekle(id){
        swal({
            title: "Emin misin?",
            text: "Bu Listedeki Ürünler Eklenecek / Güncellenecek?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                swal("İşlem Sürüyor","Ürünler Ekleniyor","info");
                $.ajax({ url:  "{site_url('importers/listekaydet')}/"+id, success: function(result){
                    swal("İşlem Tamamlandı ",result,"success");
                }});
            }
        });
    }
</script>
{/if}
{literal}
<script type="text/javascript">
function update_iletisim_talep(talep_id){
	var talep_body = $('#talep_'+talep_id);
	var note = talep_body.find('[name=istek_note]').val();
	var satis_fiyati = talep_body.find('[name=satis_fiyati]').val();
	var tedarikci = talep_body.find('[name=tedarikci]').val();
	var tedarik_fiyati = talep_body.find('[name=tedarik_fiyati]').val();
	$.ajax({
		type: "POST",
		{/literal}
		url: "{site_url('orders/iletisim_talep_update')}",
		{literal}
		data: {"id": talep_id , "note" : note , "tedarikci" : tedarikci, "tedarik_fiyati" : tedarik_fiyati, "satis_fiyati" : satis_fiyati  },
		dataType: 'json',
		success: function(data) {
			if (data.status) {
				$('#form_cevabi_ekle').html('<div class="note note-'+data.status+'"> <p> '+data.message+' </p> </div>').slideDown();
			}
		}
	});
}
function update_iletisim_tlb(talep_id){
	var talep_body = $('#talep_'+talep_id);
	var note = CKEDITOR.instances['editor_'+talep_id].getData();
	$.ajax({
		type: "POST",
		{/literal}
		url: "{site_url('home/iletisim_talep_gonder')}",
		{literal}
		data: {"id": talep_id , "note" : note },
		dataType: 'json',
		success: function(data) {
			if (data.status) {
				$('#form_cevabi_ekle').html('<div class="note note-'+data.status+'"> <p> '+data.message+' </p> </div>').slideDown();
			}
		}
	});
}
</script>
{/literal}
{if $module == "dimension" && $method == "add"}

<script type="text/javascript">

    $(document).on('change', "select", function (){
        getveri();
    });

    function getveri(){
        kategori = $('select#kategori:last').val();
        tip = $('#tip').val();
        kategoritext = $("select#kategori:last option:selected").text();
        tiptext = $("#tip option:selected").text();
        $('#veri').attr("placeholder", tiptext + " Değerini Giriniz");
        $.get( "{site_url('products/dimension/verilist')}/"+ kategori + "/" + tip, function( data ) {
            $("tbody#liste").html("<tr><td colspan='3' style='text-align: center;'> <img style='width: 50px;' border=\"0\" src = '/admin/load.gif'> </td></tr>");
            var sonuc = "";
            $("tbody#liste").html(sonuc);
            var obj = jQuery.parseJSON(data);
            $.each(obj, function(id, obj) { 
                sonuc += "<tr><td>"+obj.deger+"</td><td>"+kategoritext+"</td>"; 
                sonuc += "<td>"; 
                sonuc += "<a style=\"background-color:#D93D5E;color:white;height:20px;padding:0px 5px 0 5px;\" href='#' onclick='veri_sil("+ obj.id +")' class=\"btn btn-sm btn-default\"><i class=\"fa fa-times-circle\"></i></a>";
                sonuc += "</td>";
                sonuc += "</tr>";
            });   
            $("tbody#liste").html(sonuc);
            $("#tiptext").html(tiptext);

        });
    }

    function veri_ekle(){
        ust = $('select#kategori:last').val();
        tip = $('#tip').val();
        kategoritext = $("select#kategori:last option:selected").text();
        tiptext = $("#tip option:selected").text();
        
        deger = $('#veri').val();
        $.post( "{site_url('products/dimension/add')}", { ust: ust, tip: tip, deger: deger } ,function( data ) {
            if(data == "ok"){
                swal(tiptext +" Eklendi",deger + " değeri " + kategoritext + " kategorisine eklendi", "success");
            }else{
                swal("Hata!", "Parametre Eklenmedi", "error");
            }
            getveri();
        });
    }


    function veri_sil(id){
        swal({
            title: "Emin misin?",
            text: "Parametre silmeyi onaylıyor musunuz?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.post( "{site_url('products/dimension/delete')}", { id: id } ,function( data ) {
                    if(data == "ok"){
                        swal("Parametre Silindi"  ,"", "success");
                    }else{
                        swal("Hata!", "Parametre silinirken hata oldu", "error");
                    }
                    getveri();
                });
            }
        });
    }
</script>

{/if}


{* Her Yerde Kullanılanlar *}
<script type="text/javascript">
    function durum_degis(id, tip, nerede) {
        $.getJSON( "{site_url('json/durumdegistir')}/"+id+"/"+tip, function( data ) {
            if (data.status) {
                $(nerede).removeClass('durumu');
                $(nerede).removeClass('beyaz');

                if (data.durum == 1) {
                    $(nerede).addClass('durumu');
                } else {
                    $(nerede).addClass('beyaz');
                }
            }
        });
    }

    function birsifir(id, tip, neyi, nerede, neyle) {
        $.getJSON( "{site_url('json/birsifir')}/"+id+"/"+tip+"/"+neyi, function( data ) {
            if (data.status) {
                $(nerede).removeClass(neyle);
                $(nerede).removeClass('beyaz');

                if (data.durum == 1) {
                    $(nerede).addClass(neyle);
                } else {
                    $(nerede).addClass('beyaz');
                }
            }
        });
    }
    $('#haberform').submit(function() {
     if( $('#anasayfa').is(':checked')){
        $("#anasayfahidden").prop('disabled', true);
    }

});
</script>
{* Her Yerde Kullanılanlar *}

{if $module == "products" && $method == "index"}
<script type="text/javascript">
    function urunparametreguncelle(urun, parametre, value, tip, eklenecek){
        var pdata = 'urun='+urun+'&parametre='+parametre+'&value='+value+'&tip='+tip;
        var url = "{site_url('json/upg')}"; 
        $.ajax({
            type: "POST",
            url: url,
            data: pdata,
            dataType: 'json',
            success: function(data) {
                $('#'+parametre+'_'+urun).removeClass(eklenecek);
                $('#'+parametre+'_'+urun).removeClass('beyaz');
                if (data.status) {
                    // console.log(data);
                    if (value == 1) {
                        $('#'+parametre+'_'+urun).addClass('beyaz');

                        $('#'+parametre+'_'+urun).removeAttr("onclick");
                        $('#'+parametre+'_'+urun).attr("onclick", "urunparametreguncelle("+urun+", '"+parametre+"', 0, '"+tip+"', '"+eklenecek+"')");
                    } else {
                        $('#'+parametre+'_'+urun).addClass(eklenecek);

                        $('#'+parametre+'_'+urun).removeAttr("onclick");
                        $('#'+parametre+'_'+urun).attr("onclick", "urunparametreguncelle("+urun+", '"+parametre+"', 1, '"+tip+"', '"+eklenecek+"')");
                    }
                }
            }
        });
    }
       function init_select2(container) {
            container = $(container).closest('td').find('.modal');
            container.find('.etiket_select2').select2({
                width: '100%',
                ajax: {
                    url: '/ajax/geturunetiket',
                    dataType: 'json'
                }
            });
        }

        function etiket_ekle(element) {
            element = $(element);
            var id = element.closest('.form-group').find('select').val();
            var form = element.closest('form');
            var etiketarray = form.serializeArray();
            for (var i = 0; i < etiketarray.length; i++) {
                if (etiketarray[i].name === 'etiket_id[]')
                    if (parseInt(etiketarray[i].value) === parseInt(id)) {
                        return;
                    }
            }
            var text = element.closest('.form-group').find('select option:selected').text();
			var ul_main = form.find('ul');
            var li_new = '<li class="item"\n' +
                'style="border-bottom:1px dimgrey solid">\n' +
                text +
                '<button onclick="$(this).closest(\'li\').remove();" style="margin-top:3px"\n' +
                'class="btn btn-danger btn-xs pull-right">\n' +
                '<i class="fa fa-times"></i>\n' +
                '</button>\n' +
                '<input type="hidden" name="etiket_id[]" value="' + id + '">\n' +
                '</li>';
            $(li_new).appendTo(ul_main);
        }

        function save_etiketler(element) {
            var form_value = $(element).closest('form').serialize();
            $.ajax({
                type: "POST",
                url: '/products/etiket_kaydet',
                data: form_value,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#form_cevap_' + id).html('<div class="note note-success"> <p> Başarılı Bir Şekilde Güncellenmiştir. </p> </div>');
                    } else {
                        $('#form_cevap_' + id).html('<div class="note note-danger"> <p> İşlem sırasında hata meydana geldi. </p> </div>');
                    }
                }
            });
        }
    function hizliduzenle(id) {
        var pdata = $('form#hizli_duzenle_'+id).serialize();
        var url = "{site_url('json/upghizli')}/"+id; 
        $.ajax({
            type: "POST",
            url: url,
            data: pdata,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#form_cevap_'+id).html('<div class="note note-success"> <p> Başarılı Bir Şekilde Güncellenmiştir. </p> </div>');
                } else {
                    $('#form_cevap_'+id).html('<div class="note note-danger"> <p> İşlem sırasında hata meydana geldi. </p> </div>');
                }
            }
        });
    }
</script>
{/if}

{if $module == "campaigns" && ($method == "addgiftvoucher" || $method == "editgiftvoucher" || $method == "addordergiftvoucher")}

<script type="text/javascript" src="{$theme_url}assets/global/scripts/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
    function ozelcekayarlari() {
        var ozelcek = $('#ozel_cek_tanimla').val();
        if (ozelcek == "product") {

            $('.ozelcekler').css('display', 'block');
            $('span#gcr_msg_1').html('Aşağıdaki ürünlerde geçerli olsun');
            $('span#gcr_msg_2').html('Aşağıdaki ürünlerde geçerli olmasın');
            $('label#gcr_label').html('Ürün ID\'leri : ');
            $('input[name="ngcr"]').attr('data-original-title', 'Seçmiş olduğunuz Ürün ID numaralarını aralarına virgül koyarak yazınız.');
            // $('#urunlere_ozel').css('display', 'block');

        } else if (ozelcek == "category") {

            $('.ozelcekler').css('display', 'block');
            $('span#gcr_msg_1').html('Aşağıdaki kategorilerde geçerli olsun');
            $('span#gcr_msg_2').html('Aşağıdaki kategorilerde geçerli olmasın');
            $('label#gcr_label').html('Kategori ID\'leri : ');
            $('input[name="ngcr"]').attr('data-original-title', 'Seçmiş olduğunuz Kategori ID numaralarını aralarına virgül koyarak yazınız.Hediye çeki, eklediğiniz kategorilerin, alt kategorilerinde de geçerli olur.');
            // $('#kategorilere_ozel').css('display','block');

        } else if (ozelcek == "brand") {

            $('.ozelcekler').css('display', 'block');
            $('span#gcr_msg_1').html('Aşağıdaki markalarda geçerli olsun');
            $('span#gcr_msg_2').html('Aşağıdaki markalarda geçerli olmasın');
            $('label#gcr_label').html('Marka ID\'leri : ');
            $('input[name="ngcr"]').attr('data-original-title', 'Seçmiş olduğunuz Marka ID numaralarını aralarına virgül koyarak yazınız.Hediye çeki, eklediğiniz markaların, alt markalarında da geçerli olur.');
            // $('#markalara_ozel').css('display','block');

        } else if (ozelcek == "member") {

            $('.ozelcekler').css('display', 'block');
            $('span#gcr_msg_1').html('Aşağıdaki üyelerde geçerli olsun');
            $('span#gcr_msg_2').html('Aşağıdaki üyelerde geçerli olmasın');
            $('label#gcr_label').html('Üye ID\'leri : ');
            $('input[name="ngcr"]').attr('data-original-title', 'Seçmiş olduğunuz Üye ID numaralarını aralarına virgül koyarak yazınız.');
            // $('#uyelere_ozel').css('display','block');

        } else if (ozelcek == "normal") {

            $('.ozelcekler').css('display', 'none');
            $('input[name="ngcr"]').attr('data-original-title', '');
            $('input[name="ngcr"]').val('');
            // $('input[name="gcr"]').removeAttr('checked');
        }
    }

    {if $method == "editgiftvoucher"}
    ozelcekayarlari();
    {/if}

    function randomCode() {
        var length = 6;
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        pass="";
        for(x=0; x<length; x++)
        {
            i = Math.floor(Math.random() * 62);
            pass += chars.charAt(i);
        }
        $('#cek_kodu').val(pass);
        return true;
    }

    function degerdegistir() {
        var deger_tipi = $('#dtip').val();
        var degerimiz  = "TL";
        if (deger_tipi == 1)
            degerimiz = "%";

        $('#deger_tipimiz').html(degerimiz);
    }
	$(document).ready(function(){
		$("input[name='siparis_id']").inputmask({ "mask": "999999"});
		$("input[name='siparis_id']").change(function(){
			var siparis_id = $(this).val();
			if(siparis_id.length == 6){
				check_siparis_id($(this).val());
			}
		});
	});
	function check_siparis_id(siparis_id){
		console.log('SiparişID:'+siparis_id);
        $.get( "{site_url('ajax/siparis_total')}/"  + siparis_id).done(function( data ) {
		console.log(data);
		}
	}

</script>
{/if}

{if $module == "campaigns" && $method == "specialcampaigns"}
<script type="text/javascript">
    function ozel_kampanya_duzenle(formid) {
        var formdata = $("#form_ozel_kampanya_"+formid).serialize();

        var url = "{site_url('json/okduzenle')}/"+formid;
        $.ajax({
            type: "POST",
            url: url,
            data: formdata,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#form_cevabi_'+formid).html('<div class="note note-'+data.status+'"> <p> '+data.message+' </p> </div>').slideDown();
                }
            }
        });
    }
    
    function ozel_kampanya_ekle() {
        var formdata = $('#form_ozel_kampanya_ekle').serialize();
        
        var url = "{site_url('json/okekle')}";
        $.ajax({
            type: "POST",
            url: url,
            data: formdata,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#form_cevabi_ekle').html('<div class="note note-'+data.status+'"> <p> '+data.message+' </p> </div>').slideDown();
                }
            }
        });
    }
</script>
{/if}


<script type="text/javascript">
    function yenile() {
        setTimeout(function() {

            window.location.reload();
        }, 1000);
    }
</script>




{* montaj liste*}
{if $module == "montaj" && $method == "index"}
<script type="text/javascript">
    $(document).on('change', '[name="il"]', function (){
        $('[name="ilce"]').html("<option value='0'>Tümü</option>");
        $.get( "{site_url('ajax/location')}/"  + $( this ).val()).done(function( data ) {
            var veri = JSON.parse(data);
            var html = "<option value='0'>Tümü</option>";
            $.each( veri, function( key, value ) {
                html += '<option value="'+value.id+'" >'+value.ilce+'</option>';
            })
            $('[name="ilce"]').html(html);
            $('[name="ilce"]').trigger("chosen:updated");
        });
    });
    var markers = [];
    function initMap() {
        var geocoder = new google.maps.Geocoder;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 9,
            center: { lat: 41.048241, lng: 29.035338 }
        });

        $(document).on('change', 'select', function (){
            $.get( "{site_url('ajax/montajNoktalari')}/"  + $('[name="il"]').val() + "/"  + $('[name="ilce"]').val()).done(function( data ) {
                var firmalar = JSON.parse(data);
                var firmalist = "<table class='table table-striped table-hover table-bordered'><tr><th>Firma Adı</th><th>Firma Konumu</th><th>Firma Telefon</th><th>İşlem</th></tr>";
                var theme_url = "{$theme_url}";
                deleteMarkers();
                $.each( firmalar, function( key, firma ) {
                  firmalist += '<tr id="firma_'+firma.id+'"><td>' + firma.firma_adi + '</td><td>' + firma.il + "/" + firma.ilce + '</td><td>' + firma.firma_tel1 + '</td><td><a class="btn btn-sm btn-default edit" title="Sayfayı Düzenle" href="{site_url("montaj/edit")}/'+firma.id+'"><i class="fa fa-pencil-square-o"></i></a><a onclick="montajsil('+firma.id+')" href="#" title="Sil" class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></a></td></tr>';
                        //  console.log(firma);
                        var marker = new google.maps.Marker({
                            map: map,
                            icon: "{$theme_url}assets/img/marker.png",
                            title: firma.firma_adi,
                            position:  { lat: firma.lat * 1, lng: firma.lng  * 1}
                        });
                        markers.push(marker);
                    });

                firmalist += "</table>";
                $('#firmalist').html(firmalist);
                geocodeAddress(geocoder, map);
            })

        }); 
        $('[name="il"]').change();

    }

    function montajsil(id){
     swal({
        title: "Emin misin?",
        text: "Montaj noktasını silmeyi onaylıyor musunuz?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Hayır", "Evet"],
    })
     .then((willDelete) => {
        if (willDelete) {
            $.post( "{site_url('ajax/montajsil')}", { id: id } ,function( data ) {
                var data = JSON.parse(data);
                if(data == "Silindi"){
                    swal("Montaj noktası silindi"  ,"", "success");
                    $("#firma_"+id).remove();
                }else{
                    swal("Hata!", "Montaj noktası silinirken hata oldu", "error");
                }
            });
        }
    });
 }

 function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
  }
}

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
    }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
    }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    function geocodeAddress(geocoder, resultsMap) {
     il = $('[name="il"] option:selected').text();
     ilce = $('[name="ilce"] option:selected').text();
     if($('[name="ilce"]').val() == "0")
     {
        var address = il ;
        resultsMap.setZoom(9);
    }
    else{
        var address = il + " , " + ilce ;
        resultsMap.setZoom(11);
    }

    geocoder.geocode({ 'address': address}, function(results, status) {
        if (status === 'OK') {         
          resultsMap.setCenter(results[0].geometry.location);
      } 
  });
}
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT9zmOu4szuS-xKFSMsF-Ew8R4WpM00BM&callback=initMap"></script>
{/if}
{* montaj liste*}

{* montaj add edit*}
{if $module == "montaj" && ($method == "add" || $method == "edit")}

<script type="text/javascript" src="{$theme_url}assets/global/scripts/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
   $(document).on('change', '[name="firma_il"]', function (){
      $('[name="firma_ilce"]').html("<option value='0'>Tümü</option>");
      $.get( "{site_url('ajax/location')}/"  + $( this ).val()).done(function( data ) {
         var veri = JSON.parse(data);
         var html = "<option value='0'>Tümü</option>";
         $.each( veri, function( key, value ) {
            html += '<option value="'+value.id+'" >'+value.ilce+'</option>';
        })
         $('[name="firma_ilce"]').html(html);
     });
  });

   var markers = [];
   function initMap() {
      var geocoder = new google.maps.Geocoder;
      var map = new google.maps.Map(document.getElementById('map'), {
         zoom: 12,
         center: { lat: 41.048241, lng: 29.035338 }
     });   
      map.addListener('click', function(e) {
         deleteMarkers();
         placeMarker(e.latLng, map);
     });
      $(document).on('change', 'select', function (){
         geocodeAddress(geocoder, map);
     }); 
      {if $method == "add"}	
      $('[name="firma_il"]').change();
      {else}
      var mylatlng = { lat: $('[name="lat"]').val() * 1, lng: $('[name="lng"]').val() * 1};
      var marker = new google.maps.Marker({
         position: mylatlng,
         map: map
     });
      map.panTo(mylatlng);
      markers.push(marker);
      {/if}
  }

  function geocodeAddress(geocoder, resultsMap) {
      il = $('[name="firma_il"] option:selected').text();
      ilce = $('[name="firma_ilce"] option:selected').text();
      if($('[name="firma_ilce"]').val() == "0")
      {
         var address = il ;
         resultsMap.setZoom(9);
     }
     else{
         var address = il + " , " + ilce ;
         resultsMap.setZoom(11);
     }

     geocoder.geocode({ 'address': address}, function(results, status) {
         if (status === 'OK') {         
            resultsMap.setCenter(results[0].geometry.location);
        } 
    });
 }

 function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
  }
}

      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setMapOnAll(null);
    }

      // Shows any markers currently in the array.
      function showMarkers() {
        setMapOnAll(map);
    }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
    }




    function placeMarker(position, map) {
        $('[name="lat"]').val(position.lat());
        $('[name="lng"]').val(position.lng());
        var marker = new google.maps.Marker({
            position: position,
            map: map
        });
        markers.push(marker);
        /*map.panTo(position);*/
    }




    $(document).on('click', '[data-sfiyat]', function (){
     var input = $('[name="'+ $( this ).data('sfiyat') +'"]');
     if ($( this ).is(":checked")){
         input.attr("required", "true");
         input.show();
     }else{
        input.removeAttr("required");
        input.hide();
    }
});
    $(document).ready(function(){
      $(".phonemask").inputmask({ "mask": "0 (999) 999 99 99"});;
  });
</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT9zmOu4szuS-xKFSMsF-Ew8R4WpM00BM&callback=initMap"></script>
{/if}
{* montaj add edit*}


{if $module == "products" && ($method == "siralama")}
<script type="text/javascript">

 $(document).on('change', '[name="silik_id"]', function (){
    var bolum = $(this).data('bolum');
    var veri = $(this).find(':selected').data('dizi');

    if(veri.siraorder == 1){
        $('#' + bolum + ' .siraorder').show();
    }else{

      $('#' + bolum + ' [name="order"]').val(veri.siraorder);

      $('#' + bolum + ' .siraorder').hide();
  } 
  if(veri.siralama_tip == 1){
    $('#' + bolum + ' .siralama_tip').show();
 

}else{

  $('#' + bolum + ' select[name="siralama_tip"]').val(veri.siralama_tip);

  $('#' + bolum + ' .siralama_tip').hide();
} 
   if(veri.siralama_tip_text == 1 ){
       $('#' + bolum + ' select[name="siralama_tip"]').hide();
       $('#' + bolum + ' select[name="siralama_tip"]').prop( "disabled", true );
       $('#' + bolum + ' input[name="siralama_tip"]').show();
       $('#' + bolum + ' input[name="siralama_tip"]').prop( "disabled", false );

   }else{

       $('#' + bolum + ' select[name="siralama_tip"]').show();
       $('#' + bolum + ' select[name="siralama_tip"]').prop( "disabled", false );
       $('#' + bolum + ' input[name="siralama_tip"]').hide();
       $('#' + bolum + ' input[name="siralama_tip"]').prop( "disabled", true );
   }


$('#' + bolum + ' select[name="siralama_tip"] option[value="var-yok"]').text(veri['var-yok']);
$('#' + bolum + ' select[name="siralama_tip"] option[value="deger"]').text(veri['deger']);

});

 $(document).on('change', '[name="siralama_tip"]', function (){
    var bolum = $(this).data('bolum');
    var veri = $('#' + bolum + ' [name="silik_id"]').find(':selected').data('dizi');
    $('#' + bolum + ' [name="order"] option[value="asc"]').text(veri.asc[$(this).val()]);
    $('#' + bolum + ' [name="order"] option[value="desc"]').text(veri.desc[$(this).val()]);
});

 function psil(id){
     swal({
        title: "Emin misin?",
        text: "Sıralama parametresini silmek istiyor musunuz?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["Hayır", "Evet"],
    })
     .then((willDelete) => {
        if (willDelete) {
          location.href = "{site_url('products/spsil')}/" + id;
      }
  });
 }  


 $(document).ready(function() {
    $('.inlineedit').editable(); 
});


function urunsirala(tip,kategori,urun){
    $.get( "{site_url('products/anasayfasiralamadegis')}/" + tip + "/" + kategori + "/" + urun ).done(function( data ) {
        $("#akat_" + kategori).html(data);

    }); 
}



</script>
{/if}

{if $module == "settings" && ($method == "payments")}
<script type="text/javascript">
 $(document).on('change', '[name="pos_tip"]', function (){
    var bolum = $(this).data('bolum');
    var kayitli = $(this).data('sp');
    var veri = $(this).find(':selected').data('veri');
    var html = "";
    $.each( veri, function( key, value ) {
       html = html +   '<div class="form-group"><label class="col-md-4 control-label">';
       html = html + value.title;
       html = html +   ': </label><div class="col-md-8"><input type="text" class="form-control" name="data[';
       html = html + key;
       html = html +  ']"  value="';
       if (kayitli[key]){
           html = html + kayitli[key]; 
       }else{
           html = html + value.default  ;
       }

       html = html +  '"></div></div>';
   });
    $('.' +bolum).html(html);
});
</script>
{/if}
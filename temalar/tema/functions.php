<?php
	/*Theme Functions*/
	ini_set('display_errors', 0);
	ini_set('display_startup_errors', 0);
	error_reporting(E_ALL);

	function getref()
	{
		return $_SERVER['HTTP_REFERER'];
	}

	function fiyatver($fiyat = NULL, $birim = NULL, $indirim = NULL, $taksit = NULL ,$yuvarla = 2)
	{
		$fiyatimiz = sayiformat(fiyatvernumber($fiyat, $indirim, $taksit) , $yuvarla );
		if ($birim == "TRY") {
			$fiyatimiz = $fiyatimiz." ₺";
		} else if ($birim) { 
			$fiyatimiz = $fiyatimiz." ".$birim;
		}
		return $fiyatimiz;
	}

	function sayiformat($sayi,$yuvarla = 2){
		return number_format($sayi, $yuvarla , ",", ".");
	}


	function fiyatvernumber($fiyat = NULL,  $indirim = NULL, $taksit = NULL){

		$fiyatimiz = NULL;
		if ($indirim) {
			$fiyat = $fiyat-(($fiyat*$indirim)/100);
		}
		if ($taksit) {
			$fiyat = ($fiyat/$taksit);
		}
		if ($fiyat) {
			$fiyatimiz = round($fiyat, 2);
		} else {
			$fiyatimiz = 0;
		}
		return $fiyatimiz;
	}

function kdvhesapla($fiyat, $kdv)
	{
		$fiyatimiz = 0;
		if ($fiyat && $kdv) {
			$fiyatimiz = ($fiyat*$kdv)/100;
		}
		$fiyatimiz = round($fiyatimiz, 2);
		return $fiyatimiz;
	}

	function kdvcikar($fiyat, $kdv)
	{
		$fiyatimiz = 0;
		if ($fiyat && $kdv) {
			$fiyatimiz = $fiyat/(1 + ($kdv/100));
		}
		$fiyatimiz = round($fiyatimiz, 2);
		return $fiyatimiz;
	}
	function agaclar($json = NULL)
	{
		$agac_text = "";
		$json = json_decode($json);
		if (count($json) > 0) {
			foreach ($json as $key => $value) {
				$agac_text .= $value->name." - ";
			}
		}
		$agac_text = rtrim($agac_text, " - ");
		return $agac_text;
	}

	function posmiktar($detay = NULL)
	{
		$datasi = json_decode($detay->data);
		switch ($detay->pos_tip) {
			case 'Posnet':
				$fiyati = $datasi->ekstra->miktar;
				$fiyati = ($fiyati / 100);
			break;
			
			case 'NestPay':
				$fiyati = $datasi->ekstra->amount;
			break;

			case 'Gvp':
				$fiyati = $datasi->ekstra->txnamount;
				$fiyati = ($fiyati / 100);
			break;

			default:
				$fiyati = $datasi->ekstra->amount;
			break;
		}

		return $fiyati;
	}

	function caritelefon($telefon = NULL, $bolge = 0)
	{
		$telefon = filter_var($telefon, FILTER_SANITIZE_NUMBER_INT);
		$telefon = ltrim($telefon, "0");
		if (strlen($telefon) < 10) {
			$telefon = str_pad($telefon, 10, "0", STR_PAD_LEFT);
		}
		if (strlen($telefon) > 10) {
			$telefon = substr($telefon, 0, 10);
		}

		switch ($bolge) {
			case '0':
				$telefon = $telefon;	
			break;

			case '1':
				$telefon = substr($telefon, 0, 3);	
			break;

			case '2':
				$telefon = substr($telefon, 3);	
			break;
			
			default:
				$telefon = $telefon;
			break;
		}

		return $telefon;
	}

?>
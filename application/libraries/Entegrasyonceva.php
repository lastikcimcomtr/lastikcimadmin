<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');

// ini_set('mssql.charset', 'UTF-8');

class Entegrasyonceva
{
    protected $ci;
    public $client;
    public $session_id;
    public $base_url, $ups_login_url, $wsdl;
    public $CustomerNumber, $UserName, $Password, $Company, $ContractCode, $ContractRefNumber;

    function __construct()
    {
        $this->ci = get_instance();
        $this->base_url = "https://cevaapp02.cevakurumsal.com/CevaECommerceWebService/Service.asmx";
        $this->CustomerNumber = "01261336";
        $this->UserName = "Lastikcim";
        $this->Company = "Lastikcim";
        $this->Password = "@647Ak@23";
        $this->wsdl = "https://cevaapp02.cevakurumsal.com/CevaECommerceWebService/Service.asmx?WSDL";
        $this->ContractCode = 'C-013767';
        $this->ContractRefNumber = 'ETicaret';
    }

    public function soapLogin($query)
    {
        $this->client = new SoapClient($this->wsdl);
        $login_info = ['Language' => 'TR', 'Company' => $this->Company, 'UserName' => $this->UserName, 'Password' => $this->Password];
        $query['CevaLogin'] = $login_info;
        return $query;
    }

    public function processOrder($order)
    {
        $shipments = [];
        $order_products = json_decode($order->urunler);
        $depo_data = json_decode($order->depo_data);
        foreach ($depo_data as $key => $depo) {
            if (!isset($shipments[$depo->id])) {
                $shipments[$depo->id]['tedarikci'] = (array)$this->ci->db->where('id', $depo->tedarikci_id)->select('*')->from('tedarikciler')->get()->row();
                $shipments[$depo->id]['depo'] = (array)$this->ci->db->where('id', $depo->id)->select('*')->from('tedarikci_depolar')->get()->row();
            }
            $depo_products = (array)$depo->products;
            foreach ($order_products as $key => $product_line) {
                if (in_array($product_line->stok_kodu, $depo_products)) {
                    $shipments[$depo->id]['urunler'][] = $product_line;
                }
            }
        }

        $all_shipment_results = [];
        foreach ($shipments as $key => $shipment) {
            $all_shipment_results[$key] = $this->processItem($shipment, $order);
        }

        $this->processShipmentResult($all_shipment_results, $order);
    }

    public function processShipmentResult($all_shipment_results, $order)
    {
        $ups_image = [];
        $ups_barcode = [];
        $ups_link = [];
        $ups_result = [];

        foreach ($all_shipment_results as $key => $shipment_result) {
            $shipment_response = $shipment_result->Response;
            $shipment_barcodes = $shipment_result->Barcodes->Barcodes;
            $ups_link[$key] = $shipment_response->OrderNumber;
            $ups_result[$key] = $shipment_result;
            if ($shipment_response->Successful == 1) {
                if (is_iterable($shipment_barcodes)) {
                    foreach ($shipment_barcodes as $shipment_barcode) {
                        $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                        $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                    }
                } else {
                    $shipment_barcode = $shipment_barcodes;
                    $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                    $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                }
            }
        }

        $order_update['ups_image'] = json_encode($ups_image);
        $order_update['ups_barcode'] = json_encode($ups_barcode);
        $order_update['ups_link'] = json_encode($ups_link);
        $order_update['ups_result'] = json_encode($ups_result);
        $this->ci->db->where('id', $order->id)->update('siparisler', $order_update);
    }

    public function parseFromUpsResult($siparis_id, $ups_result)
    {
        $ups_result = json_decode($ups_result);
        $ups_image = [];
        $ups_barcode = [];
        $ups_link = [];
        foreach ($ups_result as $key => $shipment_result) {
            $shipment_response = $shipment_result->Response;
            if (!isset($shipment_result->Barcodes->Barcodes)) {
                $shipment_barcodes = $shipment_result->Barcodes->Barcode;
            } else {
                $shipment_barcodes = $shipment_result->Barcodes->Barcodes;
            }
            $ups_link[$key] = $shipment_response->OrderNumber;
            if ($shipment_response->Successful == 1) {
                if (is_iterable($shipment_barcodes)) {
                    foreach ($shipment_barcodes as $shipment_barcode) {
                        $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                        $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                    }
                } else {
                    $shipment_barcode = $shipment_barcodes;
                    $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                    $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                }
            }
        }
        $order_update['ups_image'] = json_encode($ups_image);
        $order_update['ups_barcode'] = json_encode($ups_barcode);
        $order_update['ups_link'] = json_encode($ups_link);

        $this->ci->db->where('id', $siparis_id)->update('siparisler', $order_update);
    }

    public function getMikroData($order)
    {
        $this->ci->load->library('lastikcim');
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adres = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adres->no;
        $cari_data = $this->ci->lastikcim->mevcutCariyiGetirId($cari_kod);
        $adres_data = $this->ci->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        return array_merge($cari_data, $adres_data);
    }

    public function processItem($shipment, $order)
    {
        $mikro_data = $this->getMikroData($order);

        if (in_array($order->durumu, [9, 11])) {
            $shipment_data = $this->createUpsReturnData($mikro_data, $shipment, $order);
        } else {
            $shipment_data = $this->createData($mikro_data, $shipment, $order);
        }
        $check_shipment_data = $this->checkShipmentData($shipment_data);
        if ($check_shipment_data != 'ok') {
            echo $check_shipment_data;
            return $check_shipment_data;
        }
        //$shipment_processed = $this->processShipment($shipment_data);
        $shipment_processed = $this->processShipmentAndDemand($shipment_data);
        return $shipment_processed->CreateShipmentResult;
    }

    public function processShipmentAndDemand($shipment_data)
    {
        $send_data = [];
        $send_data = $this->soapLogin($send_data);
        $request_info = $shipment_data;
        $send_data['createShipmentItem'] = $request_info;
        $send = ["CreateShipment" => $send_data];
        /*echo json_encode($send_data);
		die();*/
        $result = $this->client->__soapCall("CreateShipment", $send, array('location' => $this->base_url), NULL);
        return $result;
    }

    public function checkShipmentData($shipment_data)
    {
        foreach ($shipment_data as $key => $data_line) {
            if ($data_line == '' || $data_line == NULL) {
                return $key;
            }
        }
        return 'ok';
    }

    public function createData($mikro_data, $shipment, $order_data)
    {
        $conversion = [];
        /*Tedarikçi*/
        $shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
        $shipper_area_id = $this->findAreaCode($shipper_city, $shipment['depo']['depo_ilce']);

        $cari_il_ilce = explode('/', $mikro_data['il_ilce']);
        $cari_il = $this->findCityCode($cari_il_ilce[1]);
        $cari_area_id = $this->findAreaCode($cari_il, $cari_il_ilce[0]);


        $conversion['DeliveryType'] = 'Delivery';
        $conversion['OrderNumber'] = $order_data->id . '_' . time();
        /*        $conversion['AtfNumber'] = '';
                $conversion['CustomerRefNumber'] = '';
                $conversion['DeliveryNoteNumber'] = '';*/
        $conversion['MainCode'] = $this->CustomerNumber;
        $conversion['CevaContractCode'] = $this->ContractCode;;
        $conversion['ContractRefNumber'] = $this->ContractRefNumber;

        $conversion['Supplier'] = [];
        //$conversion['Supplier']['Code'] = '';
        $conversion['Supplier']['Name'] = 'LASTİKCİM PZ. İÇ VE DIŞ TİCARET A.Ş.';
        $conversion['Supplier']['City'] = $shipment['depo']['depo_il'];
        $conversion['Supplier']['Town'] = $shipment['depo']['depo_ilce'];
        //$conversion['Supplier']['District'] = '';
        $conversion['Supplier']['AddressTitle'] = $shipment['depo']['depo_adi'];
        $conversion['Supplier']['Address'] = $shipment['depo']['depo_adres'];
        //$conversion['Supplier']['PlateCode'] = '';
        //$conversion['Supplier']['PostalCode'] = '';
        $conversion['Supplier']['PhoneNumber'] = '08508111101';
        $conversion['Supplier']['Email'] = 'info@lastikcim.com.tr';
        //$conversion['Supplier']['TaxNumber'] = '';

        $conversion['Receiver'] = [];
        //$conversion['Receiver']['Code'] = '';
        $conversion['Receiver']['Name'] = $this->clearString(substr($mikro_data['cari_unvan1'], 0, 39));
        if ($cari_il_ilce[1] == $cari_il_ilce[0]) {
            $cari_il_ilce[0] = 'Merkez';
        }
        $conversion['Receiver']['City'] = $cari_il_ilce[1];
        $conversion['Receiver']['Town'] = $cari_il_ilce[0];
        //$conversion['Receiver']['District'] = '';
        //$conversion['Receiver']['AddressTitle'] = '';
        $conversion['Receiver']['Address'] = $this->clearString($mikro_data['tam_adres']);
        //$conversion['Receiver']['PlateCode'] = '';
        //$conversion['Receiver']['PostalCode'] = '';
        $phone_3_char = substr($mikro_data['telefon'], 0, 3);
        $phone_2_char = substr($mikro_data['telefon'], 0, 2);
        if ($phone_3_char == '905' || $phone_2_char == "05") {
            $conversion['Receiver']['PhoneNumber'] = $mikro_data['telefon'];
        } else {
            $conversion['Receiver']['PhoneNumber'] = '0850 811 11 01';
        }
        $conversion['Receiver']['Email'] = $mikro_data['email'];
        //$conversion['Receiver']['TaxNumber'] = '';
        $conversion['Products']['Product'] = [];
        foreach ($shipment['urunler'] as $key => $urun_line) {
            $product = [];
            $product['ProductType'] = 'Lastik';
            $product['OrderlineNumber'] = $key + 1;
            $product['ProductCode'] = $urun_line->stok_kodu;
            $product['ProductBarcode'] = $urun_line->stok_kodu;
            $product['ProductName'] = $urun_line->urun_adi;
            $product['PackageQuantity'] = 1;
            $product['Volume'] = '10';
            $product['Weight'] = '10';
            for ($i = 0; $i < $urun_line->miktar; $i++) {
                $conversion['Products']['Product'][] = $product;
            }
        }

        $conversion['WhoPays'] = 'Customer';
        $conversion['Shippinglabel'] = '1';
        $conversion['ShipingLabelType'] = 'Image';
        $conversion['HasBarcode'] = '0';

        return $conversion;
    }


    public function createUpsReturnData($mikro_data, $shipment, $order_data)
    {
        /*		$conversion = [];
                $shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
                $shipper_area_id = $this->findAreaCode($shipper_city,$shipment['depo']['depo_ilce']);

                $cari_il_ilce = explode('/',$mikro_data['il_ilce']);
                $cari_il = $this->findCityCode($cari_il_ilce[1]);
                $cari_area_id = $this->findAreaCode($cari_il,$cari_il_ilce[0]);




                $conversion['ShipperAccountNumber'] = $this->CustomerNumber;

                $conversion['ShipperName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
                $conversion['ShipperContactName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
                $conversion['ShipperAddress'] = $this->clearString($mikro_data['tam_adres']);
                $conversion['ShipperCityCode'] = $cari_il;
                $conversion['ShipperAreaCode'] = $cari_area_id;
                $conversion['ShipperPhoneNumber'] = $mikro_data['telefon'];
                $conversion['ShipperEMail'] = $mikro_data['email'];
                $conversion['ConsigneeName'] = 'LASTİKCİM PZ. İÇ VE DIŞ TİCARET A.Ş.';
                $conversion['ConsigneeAddress'] = $shipment['depo']['depo_adres'];
                $conversion['ConsigneeCityCode'] = $shipper_city;
                $conversion['ConsigneeAreaCode'] = $shipper_area_id;
                $conversion['ConsigneePhoneNumber'] = '08508111101';
                $conversion['ConsigneeMobilePhoneNumber'] = '0850 811 11 01';
                $conversion['ConsigneeEMail'] = 'info@lastikcim.com.tr';

                $conversion['ServiceLevel'] = '3';
                $conversion['PaymentType'] = '2';
                $conversion['PackageType'] = 'K';
                $conversion['NumberOfPackages'] = $this->getProductsTotalAmount($shipment['urunler']);
                $conversion['CustomerReferance'] = $order_data->id.' nolu sipariş';
                $conversion['DescriptionOfGoods'] = $this->getProductsName($shipment['urunler']);
                $conversion['DeliveryNotificationEmail'] = 'depo@lastikcim.com.tr';
                $conversion['SmsToConsignee'] = 1;

                return $conversion;*/
    }


    public function getProductsTotalAmount($products)
    {
        $toplam = 0;
        foreach ($products as $key => $product) {
            $toplam += $product->miktar;
        }
        return $toplam;
    }


    public function getProductsName($products)
    {
        $products_name = [];
        foreach ($products as $key => $product) {
            $products_name[] = $product->urun_adi;
        }
        return implode(',', $products_name);
    }

    public function findAreaCode($il_id, $ilce_adi)
    {
        $ilce = $this->ci->db->select('*')->from('ilceler')->where('ilce', $ilce_adi)->where('il_id', $il_id)->get()->row();
        return $ilce->area_id;
    }

    public function findCityCode($city)
    {
        $il = $this->ci->db->select('*')->from('iller')->where('il', $city)->get()->row();
        return $il->id;
    }

    public function clearString($string)
    {
        $string = str_replace("-", " ", $string);
        $string = str_replace("/", " ", $string);
        $string = str_replace("\\", " ", $string);
        $string = str_replace("*", " ", $string);
        $string = str_replace("_", " ", $string);
        return mb_convert_encoding($string, "UTF-8", "auto");
    }

    public function getReadyData()
    {
        $a = '{"CreateShipment":{
      "CevaLogin":{
         "Language":"TR",
         "Company":"Lastikcim",
         "UserName":"Lastikcim",
         "Password":"@647Ak@23"
      },
      "createShipmentItem":{
         "DeliveryType":"Delivery",
         "OrderNumber":"3432407A8676728s",
         "MainCode":"01261118",
         "CevaContractCode":"C-012837",
         "ContractRefNumber":"ETicaret",
         "Supplier":{
            "Name":"Lastikcim",
            "City":"Istanbul",
            "Town":"Maltepe",
            "Address":"Büyükbakkalköy Mah. Büyükbakkalköy Yolu No: 31/1  Maltepe /Istanbul",
            "PhoneNumber":"05301402417"
         },
         "Receiver":{
            "Name":"Ali Kaya",
            "City":"Izmir",
            "Town":"Bayrakli",
            "Address":"Sogukkuyu Mahallesi 2.Yesilova Sokak No 17, Giris   Kat.",
            "PhoneNumber":"05061138104"
         },
         "Products":{
            "Product":{
               "ProductType":"Lastik",
               "OrderlineNumber":"1",
			   "ProductBarcode":"755733335757",
               "ProductName":"Lastik",
			"PackageQuantity":"1",
               "Volume":"37",
				"Weight":"37"
            }
         },
         "WhoPays":"Customer",
         "Shippinglabel":"true",
         "ShipingLabelType":"Image",
         "HasBarcode":"false"
      }
   }
}';
        return json_decode($a, true);
    }
}

?>
<?php

class Entegrasyontatko
{
    public $ci, $db_host, $db_name, $db_user, $db_password, $conn, $limit;

    function __construct()
    {
        $this->ci = get_instance();
        $this->limit = 100;
        $this->db_host = '213.153.177.103:1433';
        $this->db_name = 'MikroDB_V15_TATKOLASTIK';
        $this->db_user = 'tatkoeltermainali';
        $this->db_password = '1q2w3e';
        ini_set('mssql.charset', 'UTF-8');
        //$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
        $serverName = '213.153.177.103,1433'; 
        //$connectionInfo = array( "Database"=>"MikroDB_V15_TATKOLASTIK", "UID"=>"tatkoeltermainali", "PWD"=>"1q2w3e", "CharacterSet" => "UTF-8");
        $connectionInfo = array( "Database"=>"MikroDB_V16_TATKOLASTIK", "UID"=>"lastikcim", "PWD"=>"3[s_&:#+vLuTYNC:", "CharacterSet" => "UTF-8");
        
        $this->conn  = sqlsrv_connect( $serverName, $connectionInfo);
        if ($this->conn === false) {  
            $this->failed_connection(json_encode(sqlsrv_errors()));
            die('Hata Oluştu, Mail Gönderildi');
        }  
        /*if (!$this->conn) {
        }*/
    }
    public function failed_connection($message){
            $this->ci->load->model('mail/Mail_model', 'mail');
            $html = '<html>Zaman: '.time().'<br>Lastikcim Tatko Entegrasyonunda bir hata oluştu.:'.$message.'</html>';
            $mail_status = $this->ci->mail->htmlmail(array('email'=>'berkaykilic@lastikcim.com.tr','title'=>'Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            $mail_status = $this->ci->mail->htmlmail(array('email'=>'harunozel@lastikcim.com.tr','title'=>'Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            $mail_status = $this->ci->mail->htmlmail(array('email'=>'mustafa.vurulkan@tatko1927.com','title'=>'Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            return false;
    }
    public function fiyathesapla($fiyat = null, $model = null, $marka = null, $stok = null)
    {
		$kar_marji_orani = $this->ci->db->select('*')->from('ayarlar')->where('name','kar_marji_orani')->get()->row()->value;
		$filters = (array)  json_decode($kar_marji_orani);
        $oran = 0;
		foreach($filters as $key => $value){
			$value = (array) $value;
			$oran = $filters['Default'];
			 if (mb_strpos($stok, $key)) {			
				 if(sizeOf($value) > 1){ 
					 $oran = $value['Default'];
					 foreach($value as $key_2 => $marj){
                        if (mb_strpos($marka, $key_2) !== false){
    						 $oran = $marj;
    						 break;
				        }
					 }
				 }else{
					 $oran = array_pop($value);
				 }
				 break;
			 }
		}
        $fiyat = $fiyat / ((100 - $oran) / 100);

        /*24.01.2018*/
        $kdvekle = (($fiyat * 18) / 100);
        $fiyat = $fiyat + $kdvekle;
        /*24.01.2018*/

        $fiyat = round($fiyat, 0, PHP_ROUND_HALF_DOWN);

        return $fiyat;
    }

    public function miktarhesapla($miktar = null)
    {
        if ($miktar < 0) {
            $miktar = 0;
        } else {
            $miktar = (int)$miktar;
        }
        return $miktar;
    }

    public function stoklar()
    {
        $data = array();
        $sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat, ay.Markaismi as marka, ay.[Model Kodu] as model,ay.[Stok İsmi] as urun_ismi, ay.dot from M5VW_STOKMIKTARLASTIKCIM ay where ay.Markaismi!='Lassa' and ay.Markaismi!='Bridgestone' and ay.Markaismi!='Dayton' group by ay.stok, ay.[Model Kodu],ay.[Stok İsmi], ay.Markaismi, ay.dot";

        $res = sqlsrv_query($this->conn,$sql);
        if(sqlsrv_errors()){
            $errors = json_encode(sqlsrv_errors());
           echo $errors;
           $this->failed_connection($errors);
        }
        while ($row = sqlsrv_fetch_array($res)) {
            $data[] = $this->convert($row);
        }
        return $data;
    }

    /**/
    private function convert($data = null)
    {
        $keys = (array_keys($data));
        foreach ($keys as $key) {
            $data[$key] = utf8_encode($data[$key]);
            $data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
            $data[$key] = str_replace("Ý", "İ", $data[$key]);
            $data[$key] = str_replace("ý", "ı", $data[$key]);
            $data[$key] = str_replace("þ", "ş", $data[$key]);
            $data[$key] = str_replace("Þ", "Ş", $data[$key]);
            $data[$key] = str_replace("ð", "ğ", $data[$key]);
            $data[$key] = str_replace("Ð", "Ğ", $data[$key]);
        }
        return $data;
    }
    /**/

}

?>
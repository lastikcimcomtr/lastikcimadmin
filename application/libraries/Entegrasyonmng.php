<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');

// ini_set('mssql.charset', 'UTF-8');

class Entegrasyonmng
{
    protected $ci;
    public $client;
    public $session_id;
    public $base_url, $ups_login_url, $wsdl;
    public $CustomerNumber, $UserName, $Password, $Company, $ContractCode, $ContractRefNumber;

    function __construct()
    {
        $this->ci = get_instance();
        $this->base_url = "http://service.mngkargo.com.tr/musterikargosiparis/musterisiparisnew.asmx";
        $this->CustomerNumber = "01261336";
        $this->UserName = "351438175";
        $this->Company = "LASTIKCIM";
        $this->Password = "IKOXQHHZ";
        $this->wsdl = "http://service.mngkargo.com.tr/musterikargosiparis/musterisiparisnew.asmx?wsdl";
        error_reporting(E_ERROR);
    }

    public function soapLogin($query)
    {
        $this->client = new SoapClient($this->wsdl);
    }

    public function processOrder($order)
    {
        $shipments = [];
        $order_products = json_decode($order->urunler);
        $depo_data = json_decode($order->depo_data);
        foreach ($depo_data as $key => $depo) {
            if (!isset($shipments[$depo->id])) {
                $shipments[$depo->id]['tedarikci'] = (array)$this->ci->db->where('id', $depo->tedarikci_id)->select('*')->from('tedarikciler')->get()->row();
                $shipments[$depo->id]['depo'] = (array)$this->ci->db->where('id', $depo->id)->select('*')->from('tedarikci_depolar')->get()->row();
            }
            $depo_products = (array)$depo->products;
            foreach ($order_products as $key => $product_line) {
                if (in_array($product_line->stok_kodu, $depo_products)) {
                    $shipments[$depo->id]['urunler'][] = $product_line;
                }
            }
        }

        $all_shipment_results = [];
        foreach ($shipments as $key => $shipment) {
            $all_shipment_results[$key] = $this->processItem($shipment, $order);
        }

        $this->processShipmentResult($all_shipment_results, $order);
    }

    public function processShipmentResult($all_shipment_results, $order)
    {
        $ups_image = [];
        $ups_barcode = [];
        $ups_link = [];
        $ups_result = [];

        foreach ($all_shipment_results as $key => $shipment_result) {
			//$shipment_response = $shipment_result->Response;
            //$shipment_barcodes = $shipment_result->Barcodes->Barcodes;
            //$ups_link[$key] = $shipment_response->OrderNumber;
            $ups_result[$key] = $shipment_result;
            /*if ($shipment_response->Successful == 1) {
                if (is_iterable($shipment_barcodes)) {
                    foreach ($shipment_barcodes as $shipment_barcode) {
                        $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                        $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                    }
                } else {
                    $shipment_barcode = $shipment_barcodes;
                    $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                    $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                }
            }*/
        }

        $order_update['ups_image'] = json_encode($ups_image);
        $order_update['ups_barcode'] = json_encode($ups_barcode);
        $order_update['ups_link'] = json_encode($ups_link);
        $order_update['ups_result'] = json_encode($ups_result);
        $this->ci->db->where('id', $order->id)->update('siparisler', $order_update);
    }

    public function parseFromUpsResult($siparis_id, $ups_result)
    {
        $ups_result = json_decode($ups_result);
        $ups_image = [];
        $ups_barcode = [];
        $ups_link = [];
        foreach ($ups_result as $key => $shipment_result) {
            $shipment_response = $shipment_result->Response;
            if (!isset($shipment_result->Barcodes->Barcodes)) {
                $shipment_barcodes = $shipment_result->Barcodes->Barcode;
            } else {
                $shipment_barcodes = $shipment_result->Barcodes->Barcodes;
            }
            $ups_link[$key] = $shipment_response->OrderNumber;
            if ($shipment_response->Successful == 1) {
                if (is_iterable($shipment_barcodes)) {
                    foreach ($shipment_barcodes as $shipment_barcode) {
                        $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                        $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                    }
                } else {
                    $shipment_barcode = $shipment_barcodes;
                    $ups_image[$key]['string'][] = $shipment_barcode->Barcode;
                    $ups_barcode[$key][] = $shipment_barcode->BarcodeNumber;
                }
            }
        }
        $order_update['ups_image'] = json_encode($ups_image);
        $order_update['ups_barcode'] = json_encode($ups_barcode);
        $order_update['ups_link'] = json_encode($ups_link);

        $this->ci->db->where('id', $siparis_id)->update('siparisler', $order_update);
    }

    public function getMikroData($order)
    {
        $this->ci->load->library('lastikcim');
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adres = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adres->no;
        $cari_data = $this->ci->lastikcim->mevcutCariyiGetirId($cari_kod);
        $adres_data = $this->ci->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        return array_merge($cari_data, $adres_data);
    }

    public function processItem($shipment, $order)
    {
        $mikro_data = $this->getMikroData($order);

        if (in_array($order->durumu, [9, 11])) {
            //$shipment_data = $this->createUpsReturnData($mikro_data, $shipment, $order);
        } else {
            $shipment_data = $this->createData($mikro_data, $shipment, $order);
        }
        //$check_shipment_data = $this->checkShipmentData($shipment_data);
        //if ($check_shipment_data != 'ok') {
        //    echo $check_shipment_data;
        //    return $check_shipment_data;
        //}
        //$shipment_processed = $this->processShipment($shipment_data);
        $shipment_processed = $this->processShipmentAndDemand($shipment_data);
		return $shipment_processed;
		//return $shipment_processed->CreateShipmentResult;
    }

    public function processShipmentAndDemand($shipment_data)
    {
        $send_data = [];

        $client = new SoapClient("http://service.mngkargo.com.tr/tservis/musterisiparisnew.asmx?wsdl");
        $result = $client->__soapCall("SiparisKayit_C2C", array($shipment_data));
		if(strlen($result)<5){
			return $shipment_data['pSiparisNo'];
		}
        return $result;
    }

    public function checkShipmentData($shipment_data)
    {
        foreach ($shipment_data as $key => $data_line) {
            if ($data_line == '' || $data_line == NULL) {
                return $key;
            }
        }
        return 'ok';
    }

    public function createData($mikro_data, $shipment, $order_data)
    {
        $conversion = [];
        /*Tedarikçi*/
        $shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
        //$shipper_area_id = $this->findAreaCode($shipper_city, $shipment['depo']['depo_ilce']);

        $cari_il_ilce = explode('/', $mikro_data['il_ilce']);
        $cari_il = $this->findCityCode($cari_il_ilce[1]);
        //$cari_area_id = $this->findAreaCode($cari_il, $cari_il_ilce[0]);

        //$conversion['Receiver']['Name'] =
        if ($cari_il_ilce[1] == $cari_il_ilce[0]) {
            $cari_il_ilce[0] = 'Merkez';
        }

        $login_info = ['pSifre' => $this->Password, 'pKullaniciAdi' => $this->UserName, 'pPlatformKisaAdi' => $this->Company];
        $products = [];
        foreach ($shipment['urunler'] as $key => $urun_line) {
            $product = [];
            $product['Kg'] = 10;
            $product['Desi'] = 10;
            $product['Adet'] = 1;
            $product['Icerik'] = $urun_line->urun_adi;
            $products['GonderiParca'] = $product;
        }

        $veriler = array(
            'pKullaniciAdi' =>$this->UserName,
            'pSifre' =>$this->Password,
            'pPlatformKisaAdi' => $this->Company,
            'pSiparisNo' => $order_data->id.'_'.uniqid(),
            'pBarkodText'=>'',
            'pIrsaliyeNo' =>'',
            'pUrunBedeli' =>'',
            'pGonderiParcaList' => $products,
            'pGonderenMusteri'=> array(
                'pGonMusteriMngNo' =>'',
                'pGonMusteriBayiNo' =>'',
                'pGonMusteriSiparisNo' => $order_data->id,
                'pGonMusteriAdi' =>'LASTIKCIM',
                'pGonMusAdresFarkli' =>"1",
                'pGonIlAdi' => mb_strtoupper($shipment['depo']['depo_il']),
                'pGonilceAdi' =>mb_strtoupper($shipment['depo']['depo_ilce']),
                'pGonAdresText' => $shipment['depo']['depo_adres'].' '.$shipment['depo']['depo_adi'],
                'pChSemt' =>'',
                'pChMahalle' =>"",
                'pChMeydanBulvar' =>"",
                'pChCadde' =>"",
                'pChSokak' =>"",
                'pGonTelEv' =>"8508111101",
                'pGonTelCep' =>'8508111101',
                'pGonFax' =>"",
                'pGonEmail' =>'info@lastikcim.com.tr',
                'pGonVergiDairesi' =>'Kağıthane V.D.',
                'pGonVergiNumarasi' =>'6080713776',
            ),
            'pAliciMusteri'=> array(
                'pAliciMusteriMngNo' =>"",
                'pAliciMusteriBayiNo' =>'',
                'pAliciMusteriAdi' => $this->clearString(substr($mikro_data['cari_unvan1'], 0, 39)),
                'pAliciMusAdresFarkli' =>"1",
                'pAliciIlAdi' =>$cari_il_ilce[1],
                'pAliciilceAdi' =>$cari_il_ilce[0],
                'pAliciAdresText' => $this->clearString($mikro_data['tam_adres']),
                'pAliciSemt' =>'',
                'pAliciMahalle' =>"",
                'pAliciMeydanBulvar' =>"",
                'pAliciCadde' =>"",
                'pAliciSokak' =>"",
                'pAliciTelIs' =>"",
                'pAliciTelEv' =>'',
                'pAliciTelCep' =>'',
                'pAliciFax' =>"",
                'pAliciEmail' =>'',
                'pAliciVergiDairesi' =>'',
                'pAliciVergiNumarasi' =>'',
            ),
            'pOdemeSekli' =>"Platform_Odeyecek",
            'pTeslimSekli' =>'Adrese_Teslim',
            'pKargoCinsi' =>'PAKET',
            'pGonSms' =>"SMSGonderilmesin",
            'pAliciSms' =>"SMSGonderilmesin",
            'pKapidaTahsilat' =>"Mal_Bedeli_Tahsil_Edilmesin",
            'pAciklama' =>"");

            return $veriler;
    }


    public function createUpsReturnData($mikro_data, $shipment, $order_data)
    {
        /*		$conversion = [];
                $shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
                $shipper_area_id = $this->findAreaCode($shipper_city,$shipment['depo']['depo_ilce']);

                $cari_il_ilce = explode('/',$mikro_data['il_ilce']);
                $cari_il = $this->findCityCode($cari_il_ilce[1]);
                $cari_area_id = $this->findAreaCode($cari_il,$cari_il_ilce[0]);




                $conversion['ShipperAccountNumber'] = $this->CustomerNumber;

                $conversion['ShipperName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
                $conversion['ShipperContactName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
                $conversion['ShipperAddress'] = $this->clearString($mikro_data['tam_adres']);
                $conversion['ShipperCityCode'] = $cari_il;
                $conversion['ShipperAreaCode'] = $cari_area_id;
                $conversion['ShipperPhoneNumber'] = $mikro_data['telefon'];
                $conversion['ShipperEMail'] = $mikro_data['email'];
                $conversion['ConsigneeName'] = 'LASTİKCİM PZ. İÇ VE DIŞ TİCARET A.Ş.';
                $conversion['ConsigneeAddress'] = $shipment['depo']['depo_adres'];
                $conversion['ConsigneeCityCode'] = $shipper_city;
                $conversion['ConsigneeAreaCode'] = $shipper_area_id;
                $conversion['ConsigneePhoneNumber'] = '08508111101';
                $conversion['ConsigneeMobilePhoneNumber'] = '0850 811 11 01';
                $conversion['ConsigneeEMail'] = 'info@lastikcim.com.tr';

                $conversion['ServiceLevel'] = '3';
                $conversion['PaymentType'] = '2';
                $conversion['PackageType'] = 'K';
                $conversion['NumberOfPackages'] = $this->getProductsTotalAmount($shipment['urunler']);
                $conversion['CustomerReferance'] = $order_data->id.' nolu sipariş';
                $conversion['DescriptionOfGoods'] = $this->getProductsName($shipment['urunler']);
                $conversion['DeliveryNotificationEmail'] = 'depo@lastikcim.com.tr';
                $conversion['SmsToConsignee'] = 1;

                return $conversion;*/
    }


    public function getProductsTotalAmount($products)
    {
        $toplam = 0;
        foreach ($products as $key => $product) {
            $toplam += $product->miktar;
        }
        return $toplam;
    }


    public function getProductsName($products)
    {
        $products_name = [];
        foreach ($products as $key => $product) {
            $products_name[] = $product->urun_adi;
        }
        return implode(',', $products_name);
    }

    public function findAreaCode($il_id, $ilce_adi)
    {
        $ilce = $this->ci->db->select('*')->from('ilceler')->where('ilce', $ilce_adi)->where('il_id', $il_id)->get()->row();
        return $ilce->area_id;
    }

    public function findCityCode($city)
    {
        $il = $this->ci->db->select('*')->from('iller')->where('il', $city)->get()->row();
        return $il->id;
    }

    public function clearString($string)
    {
        $string = str_replace("-", " ", $string);
        $string = str_replace("/", " ", $string);
        $string = str_replace("\\", " ", $string);
        $string = str_replace("*", " ", $string);
        $string = str_replace("_", " ", $string);
        return mb_convert_encoding($string, "UTF-8", "auto");
    }

    public function getReadyData()
    {
        echo '';
    }
	
}

?>
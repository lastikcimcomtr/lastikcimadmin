<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

// print_r(ini_get_all());

class Lastikcim
{
	public $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	public $path, $cache_time;
	function __construct() {
        ini_set('mssql.charset', 'UTF-8');
		$this->limit = 100;
		$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);
		$this->path = $_SERVER["DOCUMENT_ROOT"]."/";
		$this->cache_time = 1*24*60*60;
	}

	/*Tablolarda Ara*/
	public function tablolar()
	{
		$sql = "select * from sys.tables";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);

		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function columlar($tablo_id = NULL)
	{
		if ($tablo_id) {
			$sql = "select * from sys.columns where collation_name is not null and object_id = ".$tablo_id;
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function tablodara($tablo_adi = NULL, $colum_adi = NULL, $aranan = NULL)
	{
		if ($tablo_adi && $colum_adi && $aranan) {

			$sql = "select * from ".$tablo_adi." where ".$colum_adi." like '%".$aranan."%' collate Turkish_CS_AS";
			$res = @mssql_query($sql);
			$count = @mssql_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}
	private function convert($data = NULL)
	{
		$keys = (array_keys($data));

		foreach($keys as $key) if (!is_numeric($key)) {
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		} else {
			unset($data[$key]);
		}
		return $data;
	}
	
	private function eklerken($data)
	{
		
		$data = str_ireplace("Ç", "C", $data);

		$data = str_replace("İ", "Ý", $data);
		
		$data = str_replace("ı", "ý", $data);
		
		$data = str_replace("ş", "þ", $data);
		
		$data = str_replace("Ş", "Þ", $data);
		
		$data = str_replace("ğ", "ð", $data);
		
		$data = str_replace("Ğ", "Ð", $data);

		$data = utf8_decode($data);

		return $data;
	}
	/*Tablolarda Ara*/
	
	/*Cari İşlemler*/
	public function cariHesapBilgileri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAPLAR where cari_kod = '".$cari_kod."'";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function cariHesapAdresleri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAP_ADRESLERI where adr_cari_kod = '".$cari_kod."'";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	/**/
	public function cariIdAlalim()
	{
		$sql = "SELECT IDENT_CURRENT ('CARI_HESAPLAR') + 1 AS numara";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function cariKodAlalim()
	{
		/* $sql = "select max(cari_kod) as cari_kodumuz from CARI_HESAPLAR where cari_baglanti_tipi = 0 and cari_muh_kod = '120.01.001' and cari_kod like '120%'"; */
		$sql = "select max(cari_kod) as cari_kodumuz from CARI_HESAPLAR where cari_kod like '120%' and cari_baglanti_tipi = 0";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			$cari_kod = $res->cari_kodumuz;
			$cari_kod = explode(".", $cari_kod);

			$ilk 	= (int)$cari_kod[0];
			$orta 	= (int)$cari_kod[1];
			$son 	= (int)$cari_kod[2];

			if ($son == 999) {
				$ilk 	= $ilk; 
				$orta 	= $orta+1;
				$son 	= 1;
			} else {
				$ilk 	= $ilk;
				$orta 	= $orta;
				$son 	= $son+1;
			}
			$ilk 	= str_pad($ilk, 3, "0", STR_PAD_LEFT);
			$orta 	= str_pad($orta, 2, "0", STR_PAD_LEFT);
			$son 	= str_pad($son, 3, "0", STR_PAD_LEFT);
			return $ilk.".".$orta.".".$son;
		} else {
			return FALSE;
		}
	}

	public function cariHesapOlustur($data = NULL)
	{
		foreach ($data as $data_key => $data_value) {
			$data[$data_key] = $data_value;
		}

		$response = array();

		$boslar = '{"cari_special1":" ","cari_special2":" ","cari_special3":" ","cari_muh_kod1":" ","cari_muh_kod2":" ","cari_sicil_no":" ","cari_VergiKimlikNo":" ","cari_banka_tcmb_kod1":" ","cari_banka_tcmb_subekod1":" ","cari_banka_tcmb_ilkod1":" ","cari_banka_hesapno1":" ","cari_banka_swiftkodu1":" ","cari_banka_tcmb_kod2":" ","cari_banka_tcmb_subekod2":" ","cari_banka_tcmb_ilkod2":" ","cari_banka_hesapno2":" ","cari_banka_swiftkodu2":" ","cari_banka_tcmb_kod3":" ","cari_banka_tcmb_subekod3":" ","cari_banka_tcmb_ilkod3":" ","cari_banka_hesapno3":" ","cari_banka_swiftkodu3":" ","cari_banka_tcmb_kod4":" ","cari_banka_tcmb_subekod4":" ","cari_banka_tcmb_ilkod4":" ","cari_banka_hesapno4":" ","cari_banka_swiftkodu4":" ","cari_banka_tcmb_kod5":" ","cari_banka_tcmb_subekod5":" ","cari_banka_tcmb_ilkod5":" ","cari_banka_hesapno5":" ","cari_banka_swiftkodu5":" ","cari_banka_tcmb_kod6":" ","cari_banka_tcmb_subekod6":" ","cari_banka_tcmb_ilkod6":" ","cari_banka_hesapno6":" ","cari_banka_swiftkodu6":" ","cari_banka_tcmb_kod7":" ","cari_banka_tcmb_subekod7":" ","cari_banka_tcmb_ilkod7":" ","cari_banka_hesapno7":" ","cari_banka_swiftkodu7":" ","cari_banka_tcmb_kod8":" ","cari_banka_tcmb_subekod8":" ","cari_banka_tcmb_ilkod8":" ","cari_banka_hesapno8":" ","cari_banka_swiftkodu8":" ","cari_banka_tcmb_kod9":" ","cari_banka_tcmb_subekod9":" ","cari_banka_tcmb_ilkod9":" ","cari_banka_hesapno9":" ","cari_banka_swiftkodu9":" ","cari_banka_tcmb_kod10":" ","cari_banka_tcmb_subekod10":" ","cari_banka_tcmb_ilkod10":" ","cari_banka_hesapno10":" ","cari_banka_swiftkodu10":" ","cari_Ana_cari_kodu":" ","cari_satis_isk_kod":" ","cari_sektor_kodu":" ","cari_bolge_kodu":" ","cari_grup_kodu":" ","cari_temsilci_kodu":" ","cari_muhartikeli":" ","cari_wwwadresi":" ","cari_CepTel":" ","cari_Portal_PW":" ","cari_kampanyakodu":" ","cari_ufrs_fark_muh_kod":" ","cari_ufrs_fark_muh_kod1":" ","cari_ufrs_fark_muh_kod2":" ","cari_TeminatMekAlacakMuhKodu1":" ","cari_TeminatMekAlacakMuhKodu2":" ","cari_TeminatMekBorcMuhKodu1":" ","cari_TeminatMekBorcMuhKodu2":" ","cari_VerilenDepozitoTeminatMuhKodu":" ","cari_AlinanDepozitoTeminatMuhKodu":" ","cari_KEP_adresi":" ","cari_mutabakat_mail_adresi":" ","cari_mersis_no":" ","cari_istasyon_cari_kodu":" "}';
		$boslar = json_decode($boslar, TRUE);

		$sifirlar = '{"cari_SpecRECno":"0","cari_iptal":"0","cari_hidden":"0","cari_kilitli":"0","cari_degisti":"0","cari_checksum":"0","cari_hareket_tipi":"0","cari_baglanti_tipi":"0","cari_stok_alim_cinsi":"0","cari_stok_satim_cinsi":"0","cari_doviz_cinsi":"0","cari_vade_fark_yuz1":"0","cari_vade_fark_yuz2":"0","cari_odeme_cinsi":"0","cari_odeme_gunu":"0","cari_odemeplan_no":"0","cari_opsiyon_gun":"0","cari_cariodemetercihi":"0","cari_firma_acik_kapal":"0","cari_BUV_tabi_fl":"0","cari_cari_kilitli_flg":"0","cari_etiket_bas_fl":"0","cari_Detay_incele_flg":"0","cari_efatura_fl":"0","cari_POS_ongpesyuzde":"0","cari_POS_ongtaksayi":"0","cari_POS_ongIskOran":"0","cari_KabEdFCekTutar":"0","cari_hal_caritip":"0","cari_HalKomYuzdesi":"0","cari_TeslimSuresi":"0","cari_VarsayilanGirisDepo":"0","cari_VarsayilanCikisDepo":"0","cari_Portal_Enabled":"0","cari_BagliOrtaklisa_Firma":"0","cari_b_bakiye_degerlendirilmesin_fl":"0","cari_a_bakiye_degerlendirilmesin_fl":"0","cari_b_irsbakiye_degerlendirilmesin_fl":"0","cari_a_irsbakiye_degerlendirilmesin_fl":"0","cari_b_sipbakiye_degerlendirilmesin_fl":"0","cari_a_sipbakiye_degerlendirilmesin_fl":"0","cari_KrediRiskTakibiVar_flg":"0","cari_odeme_sekli":"0","cari_def_efatura_cinsi":"0","cari_otv_tevkifatina_tabii_fl":"0","cari_gonderionayi_sms":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array();
		$veriler = array_merge($boslar, $sifirlar);

		/**/
		$veriler['cari_fileid'] 					= "31";
		$veriler['cari_create_user'] 				= "246";
		$veriler['cari_create_date'] 				= date('Y-m-d H:i:s.'.rand(100, 999), time());
		$veriler['cari_lastup_user']				= "246";
		$veriler['cari_lastup_date']				= date('Y-m-d H:i:s.'.rand(100, 999), time());
		$veriler['cari_doviz_cinsi1']				= "255";
		$veriler['cari_doviz_cinsi2']				= "255";
		$veriler['cari_vade_fark_yuz']				= "25";
		$veriler['cari_KurHesapSekli']				= "1";
		$veriler['cari_satis_fk']					= "1";
		$veriler['cari_TeminatMekAlacakMuhKodu'] 	= "910";
		$veriler['cari_TeminatMekBorcMuhKodu'] 		= "912";
		$veriler['cari_efatura_baslangic_tarihi']	= "Dec 31 1899 12:00AM";
		$veriler['cari_gonderionayi_email']			= "1";
		$veriler['cari_muh_kod']					= "120.02.001";
		$veriler['cari_kaydagiristarihi']			= "Dec 30 1899 12:00AM";
		$veriler['cari_EftHesapNum']				= "1";
		/**/
		$cari_id_alinan 	= $this->cariIdAlalim();
		$cari_kod_alinan 	= $this->cariKodAlalim();
		$alinacaklar = array();
		/* $alinacaklar['cari_RECno'] 				= "".$this->cariIdAlalim().""; */
		//$alinacaklar['cari_RECid_RECno'] 		= "".$cari_id_alinan."";
		$alinacaklar['cari_kod']				= "".$cari_kod_alinan."";
		$alinacaklar['cari_unvan1']				= $data['cari_unvan_1'];
		if (strlen(trim($data['cari_unvan_2'])) == 0) {
			$data['cari_unvan_2'] = ".";
		}
		$alinacaklar['cari_unvan2']				= $data['cari_unvan_2'];
		$alinacaklar['cari_vdaire_adi']			= $data['cari_vergi_dairesi'];
		$alinacaklar['cari_vdaire_no']			= $data['cari_vergi_numarasi'];
		$alinacaklar['cari_sevk_adres_no']		= "1";
		if ($data['farkliadres'] === TRUE) {
			$alinacaklar['cari_fatura_adres_no']	= "2";
		} else {
			$alinacaklar['cari_fatura_adres_no']	= "1";
		}
		$alinacaklar['cari_EMail']				= $data['cari_email'];

		$veriler = array_merge($veriler, $alinacaklar);

		foreach ($veriler as $key => $value) {
			$veriler[$key] = "'".$value."'";
		}

        ini_set('mssql.charset', 'UTF-8');

		$sql = "insert into CARI_HESAPLAR (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = mssql_query($sql);

		if ($res) {
			$response['id'] 	= $cari_id_alinan;
			$response['kod']	= $cari_kod_alinan;
			$response['status'] = $res;
		} else {
			$response['status'] = $res;
			$response['error']	= mssql_get_last_message();
		}
		return $response;
	}

	public function cariAdresIdAlalim()
	{
		$sql = "SELECT IDENT_CURRENT ('CARI_HESAP_ADRESLERI') + 1 AS numara";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function cariAdresNumarasiAlalim($cari_kod = NULL)
	{
		$sql = "select isnull(max(adr_adres_no),0)+1 as numara from CARI_HESAP_ADRESLERI with(nolock) where adr_cari_kod = '".$cari_kod."'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return 1;
		}
	}

	public function cariAdresOlustur($data = NULL)
	{
		foreach ($data as $data_key => $data_value) {
			$data[$data_key] = $data_value;
		}

		$response = array();

		$boslar = '{"adr_special1":" ","adr_special2":" ","adr_special3":" ","adr_mahalle":" ","adr_Semt":" ","adr_Apt_No":" ","adr_Daire_No":" ","adr_Adres_kodu":" ","adr_tel_faxno":" ","adr_tel_modem":" ","adr_yon_kodu":" ","adr_temsilci_kodu":" ","adr_ozel_not":" ","adr_efatura_alias":" "}';
		$boslar = json_decode($boslar, TRUE);

		$sifirlar = '{"adr_SpecRECno":"0","adr_iptal":"0","adr_hidden":"0","adr_kilitli":"0","adr_degisti":"0","adr_checksum":"0","adr_aprint_fl":"0","adr_uzaklik_kodu":"0","adr_ziyaretperyodu":"0","adr_ziyaretgunu":"0","adr_gps_enlem":"0","adr_gps_boylam":"0","adr_ziyarethaftasi":"0","adr_ziygunu2_1":"0","adr_ziygunu2_2":"0","adr_ziygunu2_3":"0","adr_ziygunu2_4":"0","adr_ziygunu2_5":"0","adr_ziygunu2_6":"0","adr_ziygunu2_7":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array();
		$veriler = array_merge($boslar, $sifirlar);

		/**/
		$veriler['adr_fileid'] = "32";
		$veriler['adr_create_user']	= "246";
		$veriler['adr_create_date']	= date('M d Y h:iA', time());
		$veriler['adr_lastup_user']	= "246";
		$veriler['adr_lastup_date']	= date('M d Y h:iA', time());
		/**/

		$cari_adres_id_alinan = $this->cariAdresIdAlalim();
		$cari_adres_numarasi_alinan = $this->cariAdresNumarasiAlalim($data['cari_kod']);

		$alinacaklar = array();
		/* $alinacaklar['adr_RECno']			= "".$this->cariAdresIdAlalim().""; */
		//$alinacaklar['adr_RECid_RECno'] 	= "".$cari_adres_id_alinan."";
		$alinacaklar['adr_cari_kod']		= "".$data['cari_kod']."";
		$alinacaklar['adr_adres_no']		= "".$cari_adres_numarasi_alinan."";
		$alinacaklar['adr_cadde']			= "".$data['cadde']."";
		$alinacaklar['adr_sokak']			= "".$data['sokak']."";
		$alinacaklar['adr_posta_kodu']		= "".$data['posta_kodu']."";
		$alinacaklar['adr_ilce']			= "".$data['ilce']."";
		$alinacaklar['adr_il']				= "".$data['il']."";
		$alinacaklar['adr_ulke']			= "".$data['ulke']."";
		$alinacaklar['adr_tel_ulke_kodu']	= "".$data['ulke_kodu']."";
		$alinacaklar['adr_tel_bolge_kodu']	= "".$data['bolge_kodu']."";
		$alinacaklar['adr_tel_no1']			= "".$data['tel_1']."";
		$alinacaklar['adr_tel_no2']			= "".$data['tel_2']."";
		$veriler = array_merge($veriler, $alinacaklar);
		
		
		foreach ($veriler as $key => $value) if (is_string($value)){
			$veriler[$key] = "'".$value."'";
		}


		$sql = "insert into CARI_HESAP_ADRESLERI (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = mssql_query($sql);

		if ($res) {
			$response['id'] 	= $cari_adres_id_alinan;
			$response['no']		= $cari_adres_numarasi_alinan;
			$response['status'] = $res;
		} else {
			$response['status'] = $res;
		}
		return $response;
	}
	/**/
	/*Cari İşlemler*/


	/*Sipariş İşlemleri*/
	public function siparisBilgileri($cari_kod = NULL)
	{
		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from SIPARISLER where sip_musteri_kod = '".$cari_kod."' and sip_evrakno_seri = 'LC'";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function dovizKuru()
	{
		$sql = "select dbo.fn_KurBul('', 1, 1) as kur";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->kur;
		} else {
			return 1;
		}
	}

	public function siparisIdAlalim()
	{
		$sql = "SELECT max(sip_evrakno_sira)+1 as numara from SIPARISLER where sip_evrakno_seri = 'LC'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			if ((int)$res->numara == 0) {
				return 1;
			} else {
				return $res->numara;
			}
		} else {
			return 1;
		}
	}

	public function siparisRecnoAlalim()
	{
		$sql = "SELECT IDENT_CURRENT ('SIPARISLER') + 1 AS numara";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	private function siparisOzetOlustur($data = NULL)
	{
		$veriler = array();
		// $veriler['so_RECno'] 			= "";
		$veriler['so_firmano'] 			= "0";
		$veriler['so_subeno'] 			= "0";
		$veriler['so_Tipi'] 			= "0";
		$veriler['so_Kodu'] 			= "'".$data['stok_kod']."'";
		$veriler['so_SrmMerkezi'] 		= "''";
		$veriler['so_ProjeKodu'] 		= "''";
		$veriler['so_Depo'] 			= "1";
		$veriler['so_MaliYil'] 			= "".(int)date('Y', time())."";
		$veriler['so_Donem'] 			= "".(int)date('m', time())."";
		$veriler['so_HareketCins'] 		= "0";
		$veriler['so_TalepMiktar'] 		= "".$data['adet']."";
		$veriler['so_TalepKarsilanan'] 	= "0";
		$veriler['so_TalepKapanan'] 	= "0";
		$veriler['so_TeminMiktar'] 		= "0";
		$veriler['so_TeminKarsilanan'] 	= "0";
		$veriler['so_TeminKapanan'] 	= "0";

		$sql = "insert into SIPARISLER_OZET (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = mssql_query($sql);
		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function siparisOlustur($data = NULL)
	{
		$response 	= array();
		$veriler 	= array();

		$boslar = '{"sip_special1":" ","sip_special2":" ","sip_special3":" ","sip_belgeno":" ","sip_satici_kod":" ","sip_aciklama":" ","sip_aciklama2":" ","sip_cari_sormerk":" ","sip_stok_sormerk":" ","sip_teslimturu":" ","sip_Exp_Imp_Kodu":" ","sip_parti_kodu":" ","sip_projekodu":" ","sip_paket_kod":" ","sip_kapatmanedenkod":" ","sip_onodeme_evrak_seri":" "}';
		$boslar = json_decode($boslar, TRUE);

		$veriler = array_merge($veriler, $boslar);

		$sifirlar = '{"sip_SpecRECno":"0","sip_iptal":"0","sip_hidden":"0","sip_kilitli":"0","sip_degisti":"0","sip_checksum":"0","sip_firmano":"0","sip_subeno":"0","sip_tip":"0","sip_cins":"0","sip_teslim_miktar":"0","sip_iskonto_1":"0","sip_iskonto_2":"0","sip_iskonto_3":"0","sip_iskonto_4":"0","sip_iskonto_5":"0","sip_iskonto_6":"0","sip_masraf_1":"0","sip_masraf_2":"0","sip_masraf_3":"0","sip_masraf_4":"0","sip_masvergi_pntr":"0","sip_masvergi":"0","sip_OnaylayanKulNo":"0","sip_vergisiz_fl":"0","sip_kapat_fl":"0","sip_promosyon_fl":"0","sip_cari_grupno":"0","sip_doviz_cinsi":"0","sip_cagrilabilir_fl":"0","sip_iskonto1":"0","sip_isk1":"0","sip_isk2":"0","sip_isk3":"0","sip_isk4":"0","sip_isk5":"0","sip_isk6":"0","sip_mas1":"0","sip_mas2":"0","sip_mas3":"0","sip_mas4":"0","sip_kar_orani":"0","sip_durumu":"0","sip_planlananmiktar":"0","sip_lot_no":"0","sip_fiyat_liste_no":"0","sip_Otv_Pntr":"0","sip_Otv_Vergi":"0","sip_otvtutari":"0","sip_OtvVergisiz_Fl":"0","sip_onodeme_evrak_tip":"0","sip_onodeme_evrak_sira":"0","sip_rezervasyon_miktari":"0","sip_rezerveden_teslim_edilen":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array_merge($veriler, $sifirlar);

		/**/
		// $veriler['sip_RECno'] = "";

		$veriler['sip_fileid'] = "21";
		$veriler['sip_create_user'] = "246";
		$veriler['sip_create_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_lastup_user'] = "246";
		$veriler['sip_lastup_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_tarih'] 			= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_teslim_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_evrakno_seri'] 	= "LC";
		$veriler['sip_vergi_pntr'] 		= "4";
		$veriler['sip_belge_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_depono'] 			= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_iskonto2'] 		= "1";
		$veriler['sip_iskonto3'] 		= "1";
		$veriler['sip_iskonto4']		= "1";
		$veriler['sip_iskonto5'] 		= "1";
		$veriler['sip_iskonto6'] 		= "1";
		$veriler['sip_masraf1'] 		= "1";
		$veriler['sip_masraf2'] 		= "1";
		$veriler['sip_masraf3'] 		= "1";
		$veriler['sip_masraf4'] 		= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_gecerlilik_tarihi'] = "1899-12-30 00:00:00.000";
		/**/
		if ($data['ozel_siparis_id'] && is_numeric($data['ozel_siparis_id'])) {
			$siparis_id_alinan = $data['ozel_siparis_id'];
		} else {
			$siparis_id_alinan = $this->siparisIdAlalim();
		}

		$alinacaklar = array();
		$alinacaklar['sip_evrakno_sira'] 	= "".$siparis_id_alinan."";
		$alinacaklar['sip_musteri_kod'] 	= "".$data['cari_kod']."";
		$alinacaklar['sip_opno'] 			= "0"; // Ödeme Planı 0 - Peşin -- 1 - Kredi Kartı
		$alinacaklar['sip_alt_doviz_kuru'] 	= "".$this->dovizKuru()."";
		$alinacaklar['sip_adresno'] 		= "".$data['cari_adres']."";

		$veriler = array_merge($veriler, $alinacaklar);

		if (count($data['urunler']) > 0) {
			$satir_no = 0;
			foreach ($data['urunler'] as $urun_key => $urun_value) {
				$tekseferlik = $veriler;
				$urunler = array();
				$rec_no_alinan = $this->siparisRecnoAlalim();

				//$urunler['sip_RECid_RECno'] 	= "".$rec_no_alinan."";
				$urunler['sip_stok_kod'] 		= "".$urun_value['stok_kod']."";
				$urunler['sip_b_fiyat'] 		= "".$urun_value['stok_fiyat']."";
				$urunler['sip_miktar'] 			= "".$urun_value['adet']."";
				$urunler['sip_tutar'] 			= "".$urun_value['stok_toplam']."";
				$urunler['sip_vergi'] 			= "".$urun_value['stok_vergi']."";
				$urunler['sip_satirno'] 		= "".$satir_no."";
				$urunler['sip_birim_pntr'] 		= "".$urun_value['stok_birim']."";
				$urunler['sip_harekettipi']		= "".$urun_value['stok_tip']."";

				/*Özet Oluştur*/
				// $ozet_res = $this->siparisOzetOlustur($urun_value);				
				/*Özet Oluştur*/

				$tekseferlik = array_merge($tekseferlik, $urunler);

				foreach ($tekseferlik as $key => $value) if (is_string($value)){
					$tekseferlik[$key] = "'".$value."'";
				}

				$sql = "insert into SIPARISLER (".implode(", ", array_keys($tekseferlik)).") VALUES(".implode(", ", array_values($tekseferlik)).")";
				$res = mssql_query($sql);

				$response['urunler'][$urun_key] 				= $urun_value;
				$response['urunler'][$urun_key]['status'] 		= $res;
				$response['urunler'][$urun_key]['rec_no'] 		= $rec_no_alinan;
				// $response['urunler'][$urun_key]['oz_status'] 	= $ozet_res;
				
				$satir_no++;
			}
		}
		$response['siparis_id_alinan'] 	= $siparis_id_alinan;
		$response['cari_kod']			= $data['cari_kod'];

		return $response;
	}
	/*Sipariş İşlemleri*/

}

?>
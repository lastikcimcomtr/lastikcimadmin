<?php

/**/
ini_set('memory_limit', '1024M');
ini_set('mssql.charset', 'UTF-8');
date_default_timezone_set('Europe/Istanbul');
/**/

class Tatkomiktar
{
	public $ci, $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	function __construct() {
		$this->ci = get_instance();
		$this->limit = 100;
		/*
		$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);*/
        $serverName = '185.87.252.36,1433'; 
        $connectionInfo = array( "Database"=>"MikroDB_V16_LASTIKCIM", "UID"=>"lastikcim_web", "PWD"=>"1q2w3e");
        $this->conn  = sqlsrv_connect( $serverName, $connectionInfo);
	}

	public function fiyathesapla($jant = NULL, $marka = NULL, $fiyat = NULL)
	{
		switch ($marka) {
			default:
				$oran = 5;	
			break;
		}

		$fiyat = $fiyat / ((100-$oran)/100);
		$fiyat = $fiyat;

		/*24.01.2018*/
		$kdvekle = (($fiyat * 18)/100);
		$fiyat = $fiyat+$kdvekle;
		/*24.01.2018*/

		$fiyat = round($fiyat, 0, PHP_ROUND_HALF_DOWN);

		return $fiyat;
	}

	public function miktarhesapla($miktar = NULL)
	{
		if ($miktar < 0) {
			$miktar = 0;
		} else {
			$miktar = (int) $miktar;
		}
		return $miktar;
	}

	public function bizdekikodlar()
	{
		$this->ci->db->select('DISTINCT(xml_stok_kodu) as xml_stok_kodu');
		$this->ci->db->from('urunler');
		$this->ci->db->order_by('apizaman', 'asc');
		$this->ci->db->limit(500);
		$res = $this->ci->db->get()->result();
		return $res;
	}

	public function aktarimyeni()
	{
		$data = array();
		$sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat from M5VW_STOKMIKTARLASTIKCIM as ay group by ay.stok";
		$res = sqlsrv_query($conn,$sql);
		$count = sqlsrv_num_rows($res);
		if ($count > 0) {
			while ($row = sqlsrv_fetch_array($res)) {
	        	$data[] = $this->convert($row);
		    }
		}
		return $data;
	}

	public function guncelle($xml_kodu = NULL, $data = NULL)
	{
		$data['apizaman'] = time();
		$this->ci->db->where('xml_stok_kodu', $xml_kodu);
		$res = $this->ci->db->update('urunler', $data);
		return $res;
	}

	/**/
	private function convert($data = NULL)
	{
		$keys = (array_keys($data));
		foreach($keys as $key){
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		}
		return $data;
	}

	private function eklerken($data)
	{
		$data = str_replace("İ", "Ý", $data);
		$data = str_replace("ı", "ý", $data);
		$data = str_replace("ş", "þ", $data);
		$data = str_replace("Ş", "Þ", $data);
		$data = str_replace("ğ", "ð", $data);
		$data = str_replace("Ğ", "Ð", $data);
		$data = utf8_decode($data);

		return $data;
	}
	/**/

}
?>
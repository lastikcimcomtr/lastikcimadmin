<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');

// ini_set('mssql.charset', 'UTF-8');

class Entegrasyoncevastatus
{
    protected $ci;
    public $client;
    public $session_id;
    public $base_url, $ups_login_url, $wsdl;
    public $CustomerNumber, $UserName, $Password;

    function __construct()
    {
        $this->ci = get_instance();
        $this->base_url = "https://cevaapp02.cevakurumsal.com/Ceva.ETOrderStatus.WebService/ETOrderStatus.asmx";
        $this->CustomerNumber = "01261336";
        $this->UserName = "Lastikcim";
        $this->Company = "Lastikcim";
        $this->Password = "@647Ak@23";
        $this->wsdl = "https://cevaapp02.cevakurumsal.com/Ceva.ETOrderStatus.WebService/ETOrderStatus.asmx?WSDL";
        $this->ContractCode = 'C-013767';
        $this->ContractRefNumber = 'ETicaret';

    }

    public function soapLogin()
    {
        $this->client = new SoapClient($this->wsdl);
        $login_info = ['Language' => 'TR', 'Company' => $this->Company, 'UserName' => $this->UserName, 'Password' => $this->Password];
        $query['LoginInfo'] = $login_info;
        return $query;
    }

    public function checkBarcodeStatus($barcode)
    {
        $this->soapLogin();
        $result = $this->getShipmentInfo($barcode);
        return $result;
    }

    public function checkOrderStatus($orderNumber)
    {
        $this->soapLogin();
        $result = $this->getShipmentInfo($orderNumber);
        return $result;
    }

    public function getShipmentInfo($orderNumber)
    {
        $send_data = [];
        $send_data = $this->soapLogin($send_data);
        $request_info = [];
        $request_info['CustomerCode'] = $this->CustomerNumber;
        $request_info['OrderNumber'] = $orderNumber;
        $request_info['DeliveryType'] = 'Delivery';
        $request_info['ETHistory'] = true;
        $request_info['TMSHistory'] = true;
        $request_info['TmsOrderInformation'] = false;
        $send_data['Request'] = $request_info;

        $send = ["GetETOrderStatusInfo" => $send_data];
        $result = 'Barkod Oluşturuldu.';
        $result = $this->client->__soapCall("GetETOrderStatusInfo", $send, array('location' => $this->base_url), NULL);
        if (isset($result->GetETOrderStatusInfoResult->OrderStatus->StatusHistory)) {
            $status_history = $result->GetETOrderStatusInfoResult->OrderStatus->StatusHistory->StatusHistory;

//            echo json_encode($status_history);
//            echo '<hr>';
            $histories = [];
            foreach ($status_history as $status_history_line) {
//                echo json_encode($status_history_line);
//                echo '<hr>';
                $histories[] = $status_history_line->NewStatusCode;
            }
        }
        $state = $this->descriptionFromLine($histories);
        if ($state['COMPLETE']) {
            return 'COMPLETE';
        } elseif ($state['CANCELED']) {
            return 'CANCELED';
        }elseif ($state['HAREKET']) {
            return 'HAREKET';
        }else{
            return 'NOTHING';
        }
    }

    public function descriptionFromLine($status)
    {
        if (in_array('TES', $status)) {
            return ['STATE' => 'TESLIM EDILDI', 'HAREKET' => 1, 'COMPLETE' => 1, 'CANCELED' => 0];
        } elseif (in_array('TP', $status)) {
            return ['STATE' => 'SUBE HAREKETI', 'HAREKET' => 1, 'COMPLETE' => 0, 'CANCELED' => 0];
        }
    }

    public function returnShipmentInfo($orderNumber){
        $result = $this->parseShipmentInfo($orderNumber);
        return $result;
    }

    public function parseShipmentInfo($orderNumber){
        $send_data = [];
        $send_data = $this->soapLogin($send_data);
        $request_info = [];
        $request_info['CustomerCode'] = $this->CustomerNumber;
        $request_info['OrderNumber'] = $orderNumber;
        $request_info['DeliveryType'] = 'Delivery';
        $request_info['ETHistory'] = true;
        $request_info['TMSHistory'] = true;
        $request_info['TmsOrderInformation'] = true;
        $send_data['Request'] = $request_info;

        $send = ["GetETOrderStatusInfo" => $send_data];
        $result = 'Barkod Oluşturuldu.';
        $result = $this->client->__soapCall("GetETOrderStatusInfo", $send, array('location' => $this->base_url), NULL);
        echo json_encode($result);
        die();
        if (isset($result->GetETOrderStatusInfoResult->OrderStatus->StatusHistory)) {
            $status_history = $result->GetETOrderStatusInfoResult->OrderStatus->StatusHistory->StatusHistory;
            return $status_history;
        }
    }
}

?>
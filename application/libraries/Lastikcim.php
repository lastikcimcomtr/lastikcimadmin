<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

// print_r(ini_get_all());

class Lastikcim
{
	public $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	public $path, $cache_time;
	function __construct() {
		$this->limit = 100;
		/*$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
        ini_set('mssql.charset', 'UTF-8');
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);*/
        $serverName = '185.87.252.36,1433'; 
        $connectionInfo = array( "Database"=>"MikroDB_V16_LASTIKCIM", "UID"=>"lastikcim_web", "PWD"=>"1q2w3e", "CharacterSet" => "UTF-8");
        $this->conn  = 	sqlsrv_connect( $serverName, $connectionInfo);
        if ($this->conn === false) {
		    echo "Could not connect.\n";  
		    die(print_r(sqlsrv_errors(), true));  
		}  
		$this->path = $_SERVER["DOCUMENT_ROOT"]."/";
		$this->cache_time = 1*24*60*60;
	}

	/*Tablolarda Ara*/
	public function tablolar()
	{
		$sql = "select * from sys.tables";
		$res = sqlsrv_query($this->conn,$sql);
		$count = sqlsrv_num_rows($res);

		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = sqlsrv_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function columlar($tablo_id = NULL)
	{
		if ($tablo_id) {
			$sql = "select * from sys.columns where collation_name is not null and object_id = ".$tablo_id;
			$res = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function tablodara($tablo_adi = NULL, $colum_adi = NULL, $aranan = NULL)
	{
		if ($tablo_adi && $colum_adi && $aranan) {

			$sql = "select * from ".$tablo_adi." where ".$colum_adi." like '%".$aranan."%' collate Turkish_CS_AS";
			$res = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}
	private function convert($data = NULL)
	{
		$keys = (array_keys($data));

		foreach($keys as $key) if (!is_numeric($key)) {
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		} else {
			unset($data[$key]);
		}
		return $data;
	}
	
	private function eklerken($data)
	{
		
		$data = str_ireplace("Ç", "C", $data);

		$data = str_replace("İ", "Ý", $data);
		
		$data = str_replace("ı", "ý", $data);
		
		$data = str_replace("ş", "þ", $data);
		
		$data = str_replace("Ş", "Þ", $data);
		
		$data = str_replace("ğ", "ð", $data);
		
		$data = str_replace("Ğ", "Ð", $data);

		$data = utf8_decode($data);

		return $data;
	}
	/*Tablolarda Ara*/
	
	/*Cari İşlemler*/
	public function cariHesapBilgileri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAPLAR where cari_kod = '".$cari_kod."'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function cariHesapAdresleri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAP_ADRESLERI where adr_cari_kod = '".$cari_kod."'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	/**/
	public function cariIdAlalim()
	{
		$sql = "select MAX(cari_fileid) numara from CARI_HESAPLAR";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->numara+1;
		} else {
			return FALSE;
		}
	}

	public function cariKodAlalim()
	{
		/* $sql = "select max(cari_kod) as cari_kodumuz from CARI_HESAPLAR where cari_baglanti_tipi = 0 and cari_muh_kod = '120.01.001' and cari_kod like '120%'"; */
		$sql = "select max(cari_kod) as cari_kodumuz from CARI_HESAPLAR where cari_kod like '120%' and cari_baglanti_tipi = 0";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$cari_kod = $res->cari_kodumuz;
			$cari_kod = explode(".", $cari_kod);

			$ilk 	= (int)$cari_kod[0];
			$orta 	= (int)$cari_kod[1];
			$son 	= (int)$cari_kod[2];

			if ($son == 999) {
				$ilk 	= $ilk; 
				$orta 	= $orta+1;
				$son 	= 1;
			} else {
				$ilk 	= $ilk;
				$orta 	= $orta;
				$son 	= $son+1;
			}
			$ilk 	= str_pad($ilk, 3, "0", STR_PAD_LEFT);
			$orta 	= str_pad($orta, 2, "0", STR_PAD_LEFT);
			$son 	= str_pad($son, 3, "0", STR_PAD_LEFT);
			return $ilk.".".$orta.".".$son;
		} else {
			return FALSE;
		}
	}

	public function cariHesapOlustur($data = NULL)
	{
		foreach ($data as $data_key => $data_value) {
			$data[$data_key] = $data_value;
		}

		$response = array();

		$boslar = '{"cari_special1":" ","cari_special2":" ","cari_special3":" ","cari_muh_kod1":" ","cari_muh_kod2":" ","cari_sicil_no":" ","cari_VergiKimlikNo":" ","cari_banka_tcmb_kod1":" ","cari_banka_tcmb_subekod1":" ","cari_banka_tcmb_ilkod1":" ","cari_banka_hesapno1":" ","cari_banka_swiftkodu1":" ","cari_banka_tcmb_kod2":" ","cari_banka_tcmb_subekod2":" ","cari_banka_tcmb_ilkod2":" ","cari_banka_hesapno2":" ","cari_banka_swiftkodu2":" ","cari_banka_tcmb_kod3":" ","cari_banka_tcmb_subekod3":" ","cari_banka_tcmb_ilkod3":" ","cari_banka_hesapno3":" ","cari_banka_swiftkodu3":" ","cari_banka_tcmb_kod4":" ","cari_banka_tcmb_subekod4":" ","cari_banka_tcmb_ilkod4":" ","cari_banka_hesapno4":" ","cari_banka_swiftkodu4":" ","cari_banka_tcmb_kod5":" ","cari_banka_tcmb_subekod5":" ","cari_banka_tcmb_ilkod5":" ","cari_banka_hesapno5":" ","cari_banka_swiftkodu5":" ","cari_banka_tcmb_kod6":" ","cari_banka_tcmb_subekod6":" ","cari_banka_tcmb_ilkod6":" ","cari_banka_hesapno6":" ","cari_banka_swiftkodu6":" ","cari_banka_tcmb_kod7":" ","cari_banka_tcmb_subekod7":" ","cari_banka_tcmb_ilkod7":" ","cari_banka_hesapno7":" ","cari_banka_swiftkodu7":" ","cari_banka_tcmb_kod8":" ","cari_banka_tcmb_subekod8":" ","cari_banka_tcmb_ilkod8":" ","cari_banka_hesapno8":" ","cari_banka_swiftkodu8":" ","cari_banka_tcmb_kod9":" ","cari_banka_tcmb_subekod9":" ","cari_banka_tcmb_ilkod9":" ","cari_banka_hesapno9":" ","cari_banka_swiftkodu9":" ","cari_banka_tcmb_kod10":" ","cari_banka_tcmb_subekod10":" ","cari_banka_tcmb_ilkod10":" ","cari_banka_hesapno10":" ","cari_banka_swiftkodu10":" ","cari_Ana_cari_kodu":" ","cari_satis_isk_kod":" ","cari_sektor_kodu":" ","cari_bolge_kodu":" ","cari_grup_kodu":" ","cari_temsilci_kodu":" ","cari_muhartikeli":" ","cari_wwwadresi":" ","cari_CepTel":" ","cari_Portal_PW":" ","cari_kampanyakodu":" ","cari_ufrs_fark_muh_kod":" ","cari_ufrs_fark_muh_kod1":" ","cari_ufrs_fark_muh_kod2":" ","cari_TeminatMekAlacakMuhKodu1":" ","cari_TeminatMekAlacakMuhKodu2":" ","cari_TeminatMekBorcMuhKodu1":" ","cari_TeminatMekBorcMuhKodu2":" ","cari_VerilenDepozitoTeminatMuhKodu":" ","cari_AlinanDepozitoTeminatMuhKodu":" ","cari_KEP_adresi":" ","cari_mutabakat_mail_adresi":" ","cari_mersis_no":" ","cari_istasyon_cari_kodu":" "}';
		$boslar = json_decode($boslar, TRUE);

		$sifirlar = '{"cari_SpecRECno":"0","cari_iptal":"0","cari_hidden":"0","cari_kilitli":"0","cari_degisti":"0","cari_checksum":"0","cari_hareket_tipi":"0","cari_baglanti_tipi":"0","cari_stok_alim_cinsi":"0","cari_stok_satim_cinsi":"0","cari_doviz_cinsi":"0","cari_vade_fark_yuz1":"0","cari_vade_fark_yuz2":"0","cari_odeme_cinsi":"0","cari_odeme_gunu":"0","cari_odemeplan_no":"0","cari_opsiyon_gun":"0","cari_cariodemetercihi":"0","cari_firma_acik_kapal":"0","cari_BUV_tabi_fl":"0","cari_cari_kilitli_flg":"0","cari_etiket_bas_fl":"0","cari_Detay_incele_flg":"0","cari_efatura_fl":"0","cari_POS_ongpesyuzde":"0","cari_POS_ongtaksayi":"0","cari_POS_ongIskOran":"0","cari_KabEdFCekTutar":"0","cari_hal_caritip":"0","cari_HalKomYuzdesi":"0","cari_TeslimSuresi":"0","cari_VarsayilanGirisDepo":"0","cari_VarsayilanCikisDepo":"0","cari_Portal_Enabled":"0","cari_BagliOrtaklisa_Firma":"0","cari_b_bakiye_degerlendirilmesin_fl":"0","cari_a_bakiye_degerlendirilmesin_fl":"0","cari_b_irsbakiye_degerlendirilmesin_fl":"0","cari_a_irsbakiye_degerlendirilmesin_fl":"0","cari_b_sipbakiye_degerlendirilmesin_fl":"0","cari_a_sipbakiye_degerlendirilmesin_fl":"0","cari_KrediRiskTakibiVar_flg":"0","cari_odeme_sekli":"0","cari_def_efatura_cinsi":"0","cari_otv_tevkifatina_tabii_fl":"0","cari_gonderionayi_sms":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array();
		$veriler = array_merge($boslar, $sifirlar);

		/**/
		$veriler['cari_fileid'] 					= "31";
		$veriler['cari_create_user'] 				= "246";
		$veriler['cari_create_date'] 				= date('Y-m-d H:i:s.'.rand(100, 999), time());
		$veriler['cari_lastup_user']				= "246";
		$veriler['cari_lastup_date']				= date('Y-m-d H:i:s.'.rand(100, 999), time());
		$veriler['cari_doviz_cinsi1']				= "255";
		$veriler['cari_doviz_cinsi2']				= "255";
		$veriler['cari_vade_fark_yuz']				= "25";
		$veriler['cari_KurHesapSekli']				= "1";
		$veriler['cari_satis_fk']					= "1";
		$veriler['cari_TeminatMekAlacakMuhKodu'] 	= "910";
		$veriler['cari_TeminatMekBorcMuhKodu'] 		= "912";
		$veriler['cari_efatura_baslangic_tarihi']	= "Dec 31 1899 12:00AM";
		$veriler['cari_gonderionayi_email']			= "1";
		$veriler['cari_muh_kod']					= "120.02.001";
		$veriler['cari_kaydagiristarihi']			= "Dec 30 1899 12:00AM";
		$veriler['cari_EftHesapNum']				= "1";
		/**/
		$cari_id_alinan 	= $this->cariIdAlalim();
		$cari_kod_alinan 	= $this->cariKodAlalim();
		$alinacaklar = array();
		/* $alinacaklar['cari_RECno'] 				= "".$this->cariIdAlalim().""; */
		//$alinacaklar['cari_RECid_RECno'] 		= "".$cari_id_alinan."";
		$alinacaklar['cari_kod']				= "".$cari_kod_alinan."";
		$alinacaklar['cari_unvan1']				= $data['cari_unvan_1'];
		if (strlen(trim($data['cari_unvan_2'])) == 0) {
			$data['cari_unvan_2'] = ".";
		}
		$alinacaklar['cari_unvan2']				= $data['cari_unvan_2'];
		$alinacaklar['cari_vdaire_adi']			= $data['cari_vergi_dairesi'];
		$alinacaklar['cari_vdaire_no']			= $data['cari_vergi_numarasi'];
		$alinacaklar['cari_sevk_adres_no']		= "1";
		if ($data['farkliadres'] === TRUE) {
			$alinacaklar['cari_fatura_adres_no']	= "2";
		} else {
			$alinacaklar['cari_fatura_adres_no']	= "1";
		}
		$alinacaklar['cari_EMail']				= $data['cari_email'];

		$veriler = array_merge($veriler, $alinacaklar);

		foreach ($veriler as $key => $value) {
			$veriler[$key] = "'".$value."'";
		}

		$sql = "insert into CARI_HESAPLAR (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = sqlsrv_query($this->conn,$sql);
		//echo $sql;
		if ($res) {
			$response['id'] 	= $cari_id_alinan;
			$response['kod']	= $cari_kod_alinan;
			$response['status'] = true;
		} else {
			$response['status'] = $res;
			$response['error']	= sqlsrv_errors();
		}
		return $response;
	}

	public function cariAdresIdAlalim()
	{
		$sql = "SELECT IDENT_CURRENT ('CARI_HESAP_ADRESLERI') + 1 AS numara";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function cariAdresNumarasiAlalim($cari_kod = NULL)
	{
		$sql = "select isnull(max(adr_adres_no),0)+1 as numara from CARI_HESAP_ADRESLERI with(nolock) where adr_cari_kod = '".$cari_kod."'";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return 1;
		}
	}

	public function cariAdresOlustur($data = NULL)
	{
		foreach ($data as $data_key => $data_value) {
			$data[$data_key] = $data_value;
		}

		$response = array();

		$boslar = '{"adr_special1":" ","adr_special2":" ","adr_special3":" ","adr_mahalle":" ","adr_Semt":" ","adr_Apt_No":" ","adr_Daire_No":" ","adr_Adres_kodu":" ","adr_tel_faxno":" ","adr_tel_modem":" ","adr_yon_kodu":" ","adr_temsilci_kodu":" ","adr_ozel_not":" ","adr_efatura_alias":" "}';
		$boslar = json_decode($boslar, TRUE);

		$sifirlar = '{"adr_SpecRECno":"0","adr_iptal":"0","adr_hidden":"0","adr_kilitli":"0","adr_degisti":"0","adr_checksum":"0","adr_aprint_fl":"0","adr_uzaklik_kodu":"0","adr_ziyaretperyodu":"0","adr_ziyaretgunu":"0","adr_gps_enlem":"0","adr_gps_boylam":"0","adr_ziyarethaftasi":"0","adr_ziygunu2_1":"0","adr_ziygunu2_2":"0","adr_ziygunu2_3":"0","adr_ziygunu2_4":"0","adr_ziygunu2_5":"0","adr_ziygunu2_6":"0","adr_ziygunu2_7":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array();
		$veriler = array_merge($boslar, $sifirlar);

		/**/
		$veriler['adr_fileid'] = "32";
		$veriler['adr_create_user']	= "246";
		$veriler['adr_create_date']	= date('M d Y h:iA', time());
		$veriler['adr_lastup_user']	= "246";
		$veriler['adr_lastup_date']	= date('M d Y h:iA', time());
		/**/

		$cari_adres_id_alinan = $this->cariAdresNumarasiAlalim();
		$cari_adres_numarasi_alinan = $this->cariAdresNumarasiAlalim($data['cari_kod']);

		$alinacaklar = array();
		/* $alinacaklar['adr_RECno']			= "".$this->cariAdresIdAlalim().""; */
		//$alinacaklar['adr_RECid_RECno'] 	= "".$cari_adres_id_alinan."";
		$alinacaklar['adr_cari_kod']		= "".$data['cari_kod']."";
		$alinacaklar['adr_adres_no']		= "".$cari_adres_numarasi_alinan."";
		$alinacaklar['adr_cadde']			= "".$data['cadde']."";
		$alinacaklar['adr_sokak']			= "".$data['sokak']."";
		$alinacaklar['adr_posta_kodu']		= "".$data['posta_kodu']."";
		$alinacaklar['adr_ilce']			= "".$data['ilce']."";
		$alinacaklar['adr_il']				= "".$data['il']."";
		$alinacaklar['adr_ulke']			= "".$data['ulke']."";
		$alinacaklar['adr_tel_ulke_kodu']	= "".$data['ulke_kodu']."";
		$alinacaklar['adr_tel_bolge_kodu']	= "".$data['bolge_kodu']."";
		$alinacaklar['adr_tel_no1']			= "".$data['tel_1']."";
		$alinacaklar['adr_tel_no2']			= "".$data['tel_2']."";
		$veriler = array_merge($veriler, $alinacaklar);
		
		
		foreach ($veriler as $key => $value) if (is_string($value)){
			$veriler[$key] = "'".$value."'";
		}


		$sql = "insert into CARI_HESAP_ADRESLERI (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = sqlsrv_query($this->conn,$sql);

		if ($res) {
			$response['id'] 	= $cari_adres_id_alinan;
			$response['no']		= $cari_adres_numarasi_alinan;
			$response['status'] = true;
		} else {
			$response['status'] = $res;
		}
		return $response;
	}
	/**/
	public function mevcutAdresiGetir($cari_kod){
		$sql = "select * from CARI_HESAP_ADRESLERI where adr_cari_kod like '%".$cari_kod."%'";
		$res = sqlsrv_query($this->conn,$sql);
		$adresler = [];
        while ($row = sqlsrv_fetch_array($res)) {
        	$adres = [];
        	$adres['il_ilce'] = $row['adr_ilce'].'/'.$row['adr_il'];
        	$adres['telefon'] = $row['adr_tel_ulke_kodu'].$row['adr_tel_bolge_kodu'].$row['adr_tel_no1'];
        	$adres['tam_adres'] = $row['adr_cadde'].' '.$row['adr_sokak'];
        	$adres['posta_kodu'] = $row['adr_posta_kodu'];
        	$adresler[] = $adres;
        }
		return $adresler;
	}

	public function mevcutAdresiGetirId($cari_kod,$adres_id){
		$sql = "select * from CARI_HESAP_ADRESLERI where adr_cari_kod like '%".$cari_kod."%' and adr_adres_no=".$adres_id;
		$res = sqlsrv_query($this->conn,$sql);
		$adresler = [];
        while ($row = sqlsrv_fetch_array($res)) {
        	$adres = [];
        	$adres['il_ilce'] = $row['adr_ilce'].'/'.$row['adr_il'];
        	$adres['telefon'] = $row['adr_tel_ulke_kodu'].$row['adr_tel_bolge_kodu'].$row['adr_tel_no1'];
        	$adres['tam_adres'] = $row['adr_cadde'].' '.$row['adr_sokak'];
        	$adres['posta_kodu'] = $row['adr_posta_kodu'];
        	$adresler[] = $adres;
        }
		return $adresler[0];
	}

	public function mevcutCariyiGetirId($cari_kod){
		$sql = "select * from CARI_HESAPLAR where cari_kod like '%".$cari_kod."%'";
		$res = sqlsrv_query($this->conn,$sql);
		$adresler = [];
        while ($row = sqlsrv_fetch_array($res)) {
        	$adres = [];
        	$adres['cari_unvan1'] = $row['cari_unvan1'];
        	$adres['vdaire_adi'] = $row['cari_vdaire_adi'];
        	$adres['vdaire_no'] = $row['cari_vdaire_no'];
        	$adres['email'] = $row['cari_EMail'];
        	$adresler[] = $adres;
        }
		return $adresler[0];
	}

	
	/*Cari İşlemler*/


	/*Sipariş İşlemleri*/
	public function siparisBilgileri($cari_kod = NULL)
	{
		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from SIPARISLER where sip_musteri_kod = '".$cari_kod."' and sip_evrakno_seri = 'LC'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function dovizKuru()
	{
		$sql = "select dbo.fn_KurBul('', 1, 1) as kur";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->kur;
		} else {
			return 1;
		}
	}

	public function siparisIdAlalim()
	{
		$sql = "SELECT max(sip_evrakno_sira)+1 as numara from SIPARISLER where sip_evrakno_seri = 'LC'";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			if ((int)$res->numara == 0) {
				return 1;
			} else {
				return $res->numara;
			}
		} else {
			return 1;
		}
	}

	public function siparisIptalEt($siparis_no){
        $sql = "UPDATE SIPARISLER set sip_kapat_fl=1 where sip_evrakno_seri='LC' and sip_evrakno_sira ='".$siparis_no."'";
        $res = sqlsrv_query($this->conn,$sql);
    }

	public function siparisRecnoAlalim()
	{
		$sql = "SELECT * from  SIPARISLER";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	private function siparisOzetOlustur($data = NULL)
	{
		$veriler = array();
		// $veriler['so_RECno'] 			= "";
		$veriler['so_firmano'] 			= "0";
		$veriler['so_subeno'] 			= "0";
		$veriler['so_Tipi'] 			= "0";
		$veriler['so_Kodu'] 			= "'".$data['stok_kod']."'";
		$veriler['so_SrmMerkezi'] 		= "''";
		$veriler['so_ProjeKodu'] 		= "''";
		$veriler['so_Depo'] 			= "1";
		$veriler['so_MaliYil'] 			= "".(int)date('Y', time())."";
		$veriler['so_Donem'] 			= "".(int)date('m', time())."";
		$veriler['so_HareketCins'] 		= "0";
		$veriler['so_TalepMiktar'] 		= "".$data['adet']."";
		$veriler['so_TalepKarsilanan'] 	= "0";
		$veriler['so_TalepKapanan'] 	= "0";
		$veriler['so_TeminMiktar'] 		= "0";
		$veriler['so_TeminKarsilanan'] 	= "0";
		$veriler['so_TeminKapanan'] 	= "0";

		$sql = "insert into SIPARISLER_OZET (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = sqlsrv_query($this->conn,$sql);
		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function siparisOlustur($data = NULL)
	{
		$response 	= array();
		$veriler 	= array();

		$boslar = '{"sip_special1":" ","sip_special2":" ","sip_special3":" ","sip_belgeno":" ","sip_satici_kod":" ","sip_aciklama":" ","sip_aciklama2":" ","sip_cari_sormerk":" ","sip_stok_sormerk":" ","sip_teslimturu":" ","sip_Exp_Imp_Kodu":" ","sip_parti_kodu":" ","sip_projekodu":" ","sip_paket_kod":" ","sip_kapatmanedenkod":" ","sip_onodeme_evrak_seri":" "}';
		$boslar = json_decode($boslar, TRUE);

		$veriler = array_merge($veriler, $boslar);

		$sifirlar = '{"sip_SpecRECno":"0","sip_iptal":"0","sip_hidden":"0","sip_kilitli":"0","sip_degisti":"0","sip_checksum":"0","sip_firmano":"0","sip_subeno":"0","sip_tip":"0","sip_cins":"0","sip_teslim_miktar":"0","sip_iskonto_1":"0","sip_iskonto_2":"0","sip_iskonto_3":"0","sip_iskonto_4":"0","sip_iskonto_5":"0","sip_iskonto_6":"0","sip_masraf_1":"0","sip_masraf_2":"0","sip_masraf_3":"0","sip_masraf_4":"0","sip_masvergi_pntr":"0","sip_masvergi":"0","sip_OnaylayanKulNo":"0","sip_vergisiz_fl":"0","sip_kapat_fl":"0","sip_promosyon_fl":"0","sip_cari_grupno":"0","sip_doviz_cinsi":"0","sip_cagrilabilir_fl":"0","sip_iskonto1":"0","sip_isk1":"0","sip_isk2":"0","sip_isk3":"0","sip_isk4":"0","sip_isk5":"0","sip_isk6":"0","sip_mas1":"0","sip_mas2":"0","sip_mas3":"0","sip_mas4":"0","sip_kar_orani":"0","sip_durumu":"0","sip_planlananmiktar":"0","sip_lot_no":"0","sip_fiyat_liste_no":"0","sip_Otv_Pntr":"0","sip_Otv_Vergi":"0","sip_otvtutari":"0","sip_OtvVergisiz_Fl":"0","sip_onodeme_evrak_tip":"0","sip_onodeme_evrak_sira":"0","sip_rezervasyon_miktari":"0","sip_rezerveden_teslim_edilen":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array_merge($veriler, $sifirlar);

		/**/
		// $veriler['sip_RECno'] = "";

		$veriler['sip_fileid'] = "21";
		$veriler['sip_create_user'] = "246";
		$veriler['sip_create_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_lastup_user'] = "246";
		$veriler['sip_lastup_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_tarih'] 			= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_teslim_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_evrakno_seri'] 	= "LC";
		$veriler['sip_vergi_pntr'] 		= "4";
		$veriler['sip_belge_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_depono'] 			= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_iskonto2'] 		= "1";
		$veriler['sip_iskonto3'] 		= "1";
		$veriler['sip_iskonto4']		= "1";
		$veriler['sip_iskonto5'] 		= "1";
		$veriler['sip_iskonto6'] 		= "1";
		$veriler['sip_masraf1'] 		= "1";
		$veriler['sip_masraf2'] 		= "1";
		$veriler['sip_masraf3'] 		= "1";
		$veriler['sip_masraf4'] 		= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_gecerlilik_tarihi'] = "1899-12-30 00:00:00.000";
		/**/
		if ($data['ozel_siparis_id'] && is_numeric($data['ozel_siparis_id'])) {
			$siparis_id_alinan = $data['ozel_siparis_id'];
		} else {
			$siparis_id_alinan = $this->siparisIdAlalim();
		}

		$alinacaklar = array();
		$alinacaklar['sip_evrakno_sira'] 	= "".$siparis_id_alinan."";
		$alinacaklar['sip_musteri_kod'] 	= "".$data['cari_kod']."";
		$alinacaklar['sip_opno'] 			= "0"; // Ödeme Planı 0 - Peşin -- 1 - Kredi Kartı
		$alinacaklar['sip_alt_doviz_kuru'] 	= "".$this->dovizKuru()."";
		$alinacaklar['sip_adresno'] 		= "".$data['cari_adres']."";

		$veriler = array_merge($veriler, $alinacaklar);

		if (count($data['urunler']) > 0) {
			$satir_no = 0;
			foreach ($data['urunler'] as $urun_key => $urun_value) {
				$tekseferlik = $veriler;
				$urunler = array();
				$rec_no_alinan = $this->siparisRecnoAlalim();

				//$urunler['sip_RECid_RECno'] 	= "".$rec_no_alinan."";
				$urunler['sip_stok_kod'] 		= "".$urun_value['stok_kod']."";
				$urunler['sip_b_fiyat'] 		= "".$urun_value['stok_fiyat']."";
				$urunler['sip_miktar'] 			= "".$urun_value['adet']."";
				$urunler['sip_tutar'] 			= "".$urun_value['stok_toplam']."";
				$urunler['sip_vergi'] 			= "".$urun_value['stok_vergi']."";
				$urunler['sip_satirno'] 		= "".$satir_no."";
				$urunler['sip_birim_pntr'] 		= "".$urun_value['stok_birim']."";
				$urunler['sip_harekettipi']		= "".$urun_value['stok_tip']."";

				/*Özet Oluştur*/
				// $ozet_res = $this->siparisOzetOlustur($urun_value);				
				/*Özet Oluştur*/

				$tekseferlik = array_merge($tekseferlik, $urunler);

				foreach ($tekseferlik as $key => $value) if (is_string($value)){
					$tekseferlik[$key] = "'".$value."'";
				}

				$sql = "insert into SIPARISLER (".implode(", ", array_keys($tekseferlik)).") VALUES(".implode(", ", array_values($tekseferlik)).")";
				$res = sqlsrv_query($this->conn,$sql);

				$response['urunler'][$urun_key] 				= $urun_value;
				$response['urunler'][$urun_key]['status'] 		= true;
				$response['urunler'][$urun_key]['rec_no'] 		= $rec_no_alinan;
				// $response['urunler'][$urun_key]['oz_status'] 	= $ozet_res;
				
				$satir_no++;
			}
		}
		$response['siparis_id_alinan'] 	= $siparis_id_alinan;
		$response['cari_kod']			= $data['cari_kod'];

		return $response;
	}
	/*Sipariş İşlemleri*/
    public function existControl($aciklama){
        $sql = "SELECT TOP 1 * FROM MikroDB_V16_LASTIKCIM.dbo.CARI_HESAP_HAREKETLERI where cha_aciklama like '%".$aciklama."%';";
        $res = sqlsrv_query($this->conn,$sql);
        $res = sqlsrv_fetch_object($res);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
	/*Taksit Bölüm İşlemleri*/
	public function odemeVerisiGir($siparis,$odeme_fisi_no,$odeme_bilgisi=''){
		if(strlen($siparis->odemedata) < 1){
			return 'error:Ödeme verisi tanımlı değil';
		}
		$json_data = '{"cha_DBCno":"0","cha_SpecRecNo":"0","cha_iptal":"0","cha_fileid":"51","cha_hidden":"0","cha_kilitli":"0","cha_degisti":"0","cha_CheckSum":"0","cha_create_user":"17","cha_lastup_user":"17","cha_special1":"","cha_special2":"","cha_special3":"","cha_firmano":"0","cha_subeno":"0","cha_tip":"1","cha_normal_Iade":"0","cha_tpoz":"0","cha_ticaret_turu":"0","cha_belge_no":"","cha_satici_kodu":"","cha_EXIMkodu":"","cha_projekodu":"","cha_yat_tes_kodu":"","cha_cari_cins":"0","cha_ciro_cari_kodu":"","cha_d_cins":"0","cha_d_kur":"1","cha_altd_kur":"6.5498","cha_grupno":"0","cha_srmrkkodu":"","cha_kasa_hizmet":"2","cha_karsidcinsi":"0","cha_karsid_kur":"1","cha_karsisrmrkkodu":"","cha_miktari":"0","cha_Vade_Farki_Yuz":"0","cha_ft_iskonto1":"0","cha_ft_iskonto2":"0","cha_ft_iskonto3":"0","cha_ft_iskonto4":"0","cha_ft_iskonto5":"0","cha_ft_iskonto6":"0","cha_ft_masraf1":"0","cha_ft_masraf2":"0","cha_ft_masraf3":"0","cha_ft_masraf4":"0","cha_isk_mas1":"0","cha_isk_mas2":"0","cha_isk_mas3":"0","cha_isk_mas4":"0","cha_isk_mas5":"0","cha_isk_mas6":"0","cha_isk_mas7":"0","cha_isk_mas8":"0","cha_isk_mas9":"0","cha_isk_mas10":"0","cha_sat_iskmas1":"0","cha_sat_iskmas2":"0","cha_sat_iskmas3":"0","cha_sat_iskmas4":"0","cha_sat_iskmas5":"0","cha_sat_iskmas6":"0","cha_sat_iskmas7":"0","cha_sat_iskmas8":"0","cha_sat_iskmas9":"0","cha_sat_iskmas10":"0","cha_yuvarlama":"0","cha_StFonPntr":"0","cha_stopaj":"0","cha_savsandesfonu":"0","cha_avansmak_damgapul":"0","cha_vergipntr":"0","cha_vergi1":"0","cha_vergi2":"0","cha_vergi3":"0","cha_vergi4":"0","cha_vergi5":"0","cha_vergi6":"0","cha_vergi7":"0","cha_vergi8":"0","cha_vergi9":"0","cha_vergi10":"0","cha_vergisiz_fl":"0","cha_otvtutari":"0","cha_otvvergisiz_fl":"0","cha_oiv_pntr":"0","cha_oivtutari":"0","cha_oiv_vergi":"0","cha_oivergisiz_fl":"0","cha_reftarihi":"1899-12-30 00:00:00","cha_istisnakodu":"0","cha_pos_hareketi":"0","cha_meblag_ana_doviz_icin_gecersiz_fl":"0","cha_meblag_alt_doviz_icin_gecersiz_fl":"0","cha_meblag_orj_doviz_icin_gecersiz_fl":"0","cha_sip_uid":"00000000-0000-0000-0000-000000000000","cha_kirahar_uid":"00000000-0000-0000-0000-000000000000","cha_vardiya_tarihi":"1899-12-30 00:00:00","cha_vardiya_no":"0","cha_vardiya_evrak_ti":"0","cha_ebelge_turu":"0","cha_tevkifat_toplam":"0","cha_ilave_edilecek_kdv1":"0","cha_ilave_edilecek_kdv2":"0","cha_ilave_edilecek_kdv3":"0","cha_ilave_edilecek_kdv4":"0","cha_ilave_edilecek_kdv5":"0","cha_ilave_edilecek_kdv6":"0","cha_ilave_edilecek_kdv7":"0","cha_ilave_edilecek_kdv8":"0","cha_ilave_edilecek_kdv9":"0","cha_ilave_edilecek_kdv10":"0","cha_e_islem_turu":"0","cha_fatura_belge_turu":"0","cha_diger_belge_adi":"","cha_uuid":"","cha_adres_no":"0","cha_vergifon_toplam":"0","cha_ilk_belge_tarihi":"1899-12-30 00:00:00","cha_ilk_belge_doviz_kuru":"0","cha_HareketGrupKodu1":"","cha_HareketGrupKodu2":"","cha_HareketGrupKodu3":""}';
		$defaults = json_decode($json_data);

		$dateOne = new \DateTime();
		$odeme_data = json_decode($siparis->odemedata);
		$nonDefaults = [];
		$nonDefaults['cha_lastup_date'] = $dateOne->format('Y-m-d H:i:s');
		$nonDefaults['cha_create_date'] = $dateOne->format('Y-m-d H:i:s');

		$date = new \DateTime();
		$date->setTimestamp($siparis->tarih);
		$sip_tarih = $date->format('Y-m-d H:i:s');
		$siptarih_2 = $date->format('Ymd');

		$nonDefaults['cha_tarihi'] = $siptarih_2; 
		$nonDefaults['cha_fis_tarih'] = $sip_tarih; 
		$nonDefaults['cha_belge_tarih'] = $sip_tarih; 

		$nonDefaults['cha_evrak_tip'] =  $this->calculateOdemeTipi($siparis,$odeme_data);
		$nonDefaults['cha_evrakno_seri'] = 'OTM'; 

		$nonDefaults['cha_cinsi'] = ($nonDefaults['cha_evrak_tip'] == 1 ? 19 : 0); // KK 19 - Havale 0
		$nonDefaults['cha_karsidgrupno'] = ($nonDefaults['cha_evrak_tip'] == 1 ? 7 : 1); // KK 7 - Havale 1

		$nonDefaults['cha_aciklama'] = $this->createAcklm($siparis,$odeme_data,$nonDefaults['cha_evrak_tip']); // Açıklama KK No + Sip No ya da Sip No

        $exist_control =  $this->existControl($nonDefaults['cha_aciklama']);
        if($exist_control){
        	echo 'zaten mevcut<hr>';
        	echo $nonDefaults['cha_aciklama'];
            die();
        }
		$nonDefaults['cha_kod'] = $this->getCariKod($siparis); // Cari Kodu
		$nonDefaults['cha_kasa_hizkod'] = $odeme_bilgisi['hizmet_kodu']; // Banka Kodu

		$nonDefaults['cha_meblag'] = $this->meblagHesapla($odeme_bilgisi);
		$nonDefaults['cha_evrakno_sira'] = (int)number_format($odeme_fisi_no,0,'','');
		$nonDefaults['cha_aratoplam'] = $this->meblagHesapla($odeme_bilgisi);

		$nonDefaults['cha_sntck_poz'] = ($nonDefaults['cha_evrak_tip'] == 1 ? 2 : 0);; //  KK 2 Havale 0
		$text_baslangic = '+'.$odeme_bilgisi['baslangic'].' days';
		$vade = $date->modify($text_baslangic);
		$insert_result = [];
		$insert_muh_fis = [];
		$satir_no = 0;
		//$nonDefaults['cha_Guid'] = $this->getLastTahNo();
		$fis_no = $this->getFisNo($dateOne->format('Ymd')) + 1;
		if($odeme_bilgisi['aralik'] == 0){
			$nonDefaults['cha_fis_sirano'] = $fis_no;
			$nonDefaults['cha_trefno'] = $this->getRefNo($dateOne->format('Y'),$nonDefaults['cha_evrak_tip']);
			$nonDefaults['cha_satir_no'] = $satir_no;
			$nonDefaults['cha_vade'] = $vade->format('Ymd'); // Vade Tarihi (20200214)
			//$nonDefaults['cha_Guid'] = $this->increaseTahNo($nonDefaults['cha_Guid']);
			$insert_result[] = array_merge((array)$defaults,$nonDefaults);
		}else{
			$refNo = $this->getRefNo($dateOne->format('Y'),$nonDefaults['cha_evrak_tip']);
			$text_aralik = '+'.$odeme_bilgisi['aralik'].' days';
			$taksit = $odeme_bilgisi['taksit'];
			for($i = 0; $i<$taksit; $i++) {
				$nonDefaults['cha_fis_sirano'] = $fis_no;
				$nonDefaults['cha_trefno'] = $refNo;
				$nonDefaults['cha_satir_no'] = $satir_no;
				$nonDefaults['cha_vade'] = $vade->format('Ymd'); // Vade Tarihi (20200214)
				//$nonDefaults['cha_Guid'] = $this->increaseTahNo($nonDefaults['cha_Guid']);
				//if($i == 0){
					//$insert_muh_fis[] = $this->createMuhasebeFis($nonDefaults,$odeme_bilgisi);
				//}
				$insert_result[] = array_merge((array)$defaults,$nonDefaults);
				$vade = $vade->modify($text_aralik);
				$satir_no = $satir_no + 1;
				$refNo = $this->increaseRefNo($refNo);
			}
		}
		if($nonDefaults['cha_evrak_tip'] == 1){
			$cari_kod = json_decode($siparis->caridata);
			$cari_data = $this->mevcutCariyiGetirId($cari_kod->kod);
			$cari_data['kod'] = $cari_kod;
			$evrak_sira_no = $this->emirEvrakSiraNo();
			$emir_no = $this->getLastEmrNo();
			foreach ($insert_result as $key => $result_row) {
			    //print_r($result_row);die();
				$emir_no = $this->increaseTahNo($emir_no);
				$this->odemeEmriOlustur($result_row,$odeme_bilgisi,$cari_data,$evrak_sira_no,$emir_no);
			}
		}
		foreach ($insert_result as $key => $result_row) {
			$this->insertHesapHareketi($result_row);
		}
	}



	public function odemeEmriOlustur($hesapHareketi,$odeme_bilgisi,$cari_data,$evrak_sira_no,$emir_no){
		$defaults = [];
		$default_json = '{"sck_DBCno":"0","sck_SpecRECno":"0","sck_iptal":"0","sck_fileid":"54","sck_hidden":"0","sck_kilitli":"0","sck_degisti":"0","sck_checksum":"0","sck_create_user":"17","sck_lastup_user":"17","sck_special1":"","sck_special2":"","sck_special3":"","sck_firmano":"0","sck_subeno":"0","sck_bankano":"","sck_doviz":"0","sck_odenen":"0","sck_degerleme_islendi":"0","sck_banka_adres1":"","sck_sube_adres2":"","sck_borclu_tel":"","sck_hesapno_sehir":"","sck_no":"","sck_doviz_kur":"1","sck_imza":"0","sck_srmmrk":"","sck_kesideyeri":"","Sck_TCMB_Banka_kodu":"","Sck_TCMB_Sube_kodu":"","Sck_TCMB_il_kodu":"","SckTasra_fl":"0","sck_projekodu":"","sck_masraf1":"0","sck_masraf1_isleme":"0","sck_masraf2":"0","sck_masraf2_isleme":"0","sck_odul_katkisi_tutari":"0","sck_servis_komisyon_tutari":"0","sck_erken_odeme_faiz_tutari":"0","sck_odul_katkisi_tutari_islendi_fl":"0","sck_servis_komisyon_tutari_islendi_fl":"0","sck_erken_odeme_faiz_tutari_islendi_fl":"0","sck_kredi_karti_tipi":"0","sck_uye_isyeri_no":"","sck_kredi_karti_no":"","sck_provizyon_kodu":"","sck_ilk_evrak_seri":"OTM","sck_nerede_cari_grupno":"7","sck_nerede_cari_cins":"2","sck_sahip_cari_cins":"0","sck_duzen_tarih":"1899-12-30 00:00:00","sck_tip":"6","sck_sahip_cari_grupno":"0","sck_sonpoz":"2"}';
		$defaults = (array) json_decode($default_json);
		//$defaults['sck_Guid'] = $emir_no;
		$defaults['sck_create_date'] = $hesapHareketi['cha_create_date'];
		$defaults['sck_lastup_date'] = $hesapHareketi['cha_lastup_date'];
		$defaults['sck_refno'] = $hesapHareketi['cha_trefno'];
		$defaults['sck_tutar'] = $hesapHareketi['cha_aratoplam'];
		$defaults['sck_ilk_evrak_satir_no'] = $hesapHareketi['cha_satir_no'];
		$defaults['sck_ilk_hareket_tarihi'] = $hesapHareketi['cha_tarihi'];
		$defaults['sck_son_hareket_tarihi'] = $hesapHareketi['cha_tarihi'];
		$defaults['sck_taksit_sayisi'] = $odeme_bilgisi['taksit'];
		$defaults['sck_kacinci_taksit'] = $hesapHareketi['cha_satir_no'] + 1;
		$defaults['sck_nerede_cari_kodu'] = $hesapHareketi['cha_kasa_hizkod'];
		$defaults['sck_borclu'] = substr($cari_data['cari_unvan1'],0,30);
		$defaults['sck_vdaire_no'] = $cari_data['vdaire_no'];
		$defaults['sck_sahip_cari_kodu'] = $cari_data['kod']->kod;
		$dateOne = \DateTime::createFromFormat('Ymd H:i:s',$hesapHareketi['cha_vade'].' 00:00:00');	
		$defaults['sck_vade'] = $dateOne->format('Y-m-d H:i:s');
		$defaults['sck_ilk_evrak_sira_no'] = $evrak_sira_no;
		$this->insertOdemeEmri($defaults);
	}

	public function increaseRefNo($refNo){
		$sub_str = str_split($refNo,16);
		$sub_str[1] =sprintf("%08d", ($sub_str[1] + 1));
		$result = implode('',$sub_str);
		return $result;
	}

	public function insertOdemeEmri($insert_array){
		foreach ($insert_array as $key => $value) 
			if (is_string($value)){
				$insert_array[$key] = "'".$value."'";
			}
		$sql = "insert into ODEME_EMIRLERI (".implode(", ", array_keys($insert_array)).") VALUES(".implode(", ", array_values($insert_array)).")";	
		$res = sqlsrv_query($this->conn,$sql);
		if( ($errors = sqlsrv_errors() ) != null) {
	        foreach( $errors as $error ) {
	            echo "SQLSTATE: ".$error[ 'SQLSTATE']."<br />";
	            echo "code: ".$error[ 'code']."<br />";
	            echo "message: ".$error[ 'message']."<br />";
	        }
    	}

	}

	public function insertHesapHareketi($insert_array){
		foreach ($insert_array as $key => $value) 
			if (is_string($value)){
				$insert_array[$key] = "'".$value."'";
			}
		$sql = "insert into CARI_HESAP_HAREKETLERI (".implode(", ", array_keys($insert_array)).") VALUES(".implode(", ", array_values($insert_array)).")";
			$res = sqlsrv_query($this->conn,$sql);
		if( ($errors = sqlsrv_errors() ) != null) {
	        foreach( $errors as $error ) {
	            echo "SQLSTATE: ".$error[ 'SQLSTATE']."<br />";
	            echo "code: ".$error[ 'code']."<br />";
	            echo "message: ".$error[ 'message']."<br />";
	        }
    	}

	}

	public function emirEvrakSiraNo(){
		$sql = "SELECT TOP 1 sck_ilk_evrak_sira_no FROM MikroDB_V16_LASTIKCIM.dbo.ODEME_EMIRLERI order by sck_ilk_evrak_sira_no desc;";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$result = (int) str_replace(',', '',$res->sck_ilk_evrak_sira_no );
			return $result + 1;
		} else {
			echo 'error:1211';
			die();
		}	
	}


	/*Taksit Bölüm İşlemleri*/
	public function getRefNo($year,$evrak_tip){
		$result = '';
		if($evrak_tip == 1){
			$sql = "SELECT TOP 1 cha_Guid, cha_trefno FROM MikroDB_V16_LASTIKCIM.dbo.CARI_HESAP_HAREKETLERI where cha_belge_tarih like '%".$year."%' and cha_evrak_tip=1 order by cha_trefno desc,cha_create_date desc;";
			$res = sqlsrv_query($this->conn,$sql);
			$res = sqlsrv_fetch_object($res);
			if ($res) {
				$result = $res->cha_trefno;
			} else {
				$result = 'MK-000-000-'.$year.'-00000001';
			}	
			$result = $this->increaseRefNo($result);
		}
		return $result;
	}
	public function getFisNo($date){
		$result = '';
		$sql = "SELECT TOP 1 cha_tarihi, cha_fis_sirano FROM MikroDB_V16_LASTIKCIM.dbo.CARI_HESAP_HAREKETLERI where cha_evrakno_seri='OTM' and cha_tarihi='".$date."' order by cha_fis_sirano desc;";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$result = $res->cha_fis_sirano;
		} else {
			$result = '1';
		}	
		return $result;
	}
	public function meblagHesapla($odeme_bilgisi){
		$fiyat = $odeme_bilgisi['tutar'];
		if(isset($odeme_bilgisi['taksit'])){
			return ($fiyat/((int) $odeme_bilgisi['taksit']));
		}else{
			return $fiyat;
		}
	}
	public function createAcklm($siparis,$odeme_data,$type = 1){
		$aciklama = '';
		$aciklama .= 'Siparis No: '.$siparis->id;
		if($type == 1){
			$aciklama .= ' #KK:'.$odeme_data->card_number;
		}
		return $aciklama;
	}
	public function getLastTahNo(){
		$sql = "SELECT TOP 1 * from  CARI_HESAP_HAREKETLERI ORDER BY cha_create_date desc,cha_satir_no desc";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$ch_res = $res->cha_Guid;
			return $ch_res;
		} else {
			return FALSE;
		}
	}

	public function getLastEmrNo(){
		$sql = "SELECT TOP 1 * from  ODEME_EMIRLERI ORDER BY sck_create_date desc,sck_refno desc";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$ch_res = $res->sck_Guid;
			return $ch_res;
		} else {
			return FALSE;
		}
	}
	public function increaseTahNo($tahno){
		$sub_str = explode('-',$tahno);
		$sub_str[4] = strtoupper(dechex(hexdec($sub_str[4]) + 1));
		$ch_res = implode('-',$sub_str);
		return $ch_res;
	}

	public function getCariKod($siparis){
		$cari_data = json_decode($siparis->caridata);
		return $cari_data->kod;
	}

	public function calculateOdemeTipi($siparis,$odeme_data){
		if($siparis->odeme_tipi == 1){
			return 34;
		}elseif($siparis->odeme_tipi == 2){
			return 1;
		}else{
			if(isset($odeme_data->sanalpos)){
				return 1;
			}else{
				return 34;
			}
		}
	}

	public function createMuhasebeFis($nonDefaults,$odeme_bilgisi){
		$dateOne = new \DateTime();
		$data = [];
		$data['fis_Guid'] = '';
		$data['fis_DBCno'] = '0';
		$data['fis_SpecRECno'] = '0';
		$data['fis_iptal'] = '0';
		$data['fis_fileid'] = '2';
		$data['fis_hidden'] = '0';
		$data['fis_kilitli'] = '0';
		$data['fis_degisti'] = '0';
		$data['fis_checksum'] = '0';
		$data['fis_create_user'] = '17';
		$data['fis_lastup_user'] = '17';
		$data['fis_kurfarkifl'] = '0';
		$data['fis_ticari_evraktip'] = '1';
		$data['fis_tic_evrak_seri'] = 'OTM';
		$data['fis_tur'] = '1';
		$data['fis_special1'] = '';
		$data['fis_special2'] = '';
		$data['fis_special3'] = '';
		$data['fis_firmano'] = '0';
		$data['fis_subeno'] = '0';
		$data['fis_meblag3'] = '0';
		$data['fis_meblag4'] = '0';
		$data['fis_meblag5'] = '0';
		$data['fis_meblag6'] = '0';
		$data['fis_katagori'] = '0';
		$data['fis_evrak_DBCno'] = '0';
		$data['fis_fmahsup_tipi'] = '0';
		$data['fis_fozelmahkod'] = '';
		$data['fis_grupkodu'] = '';
		$data['fis_aktif_pasif'] = '0';
		$data['fis_proje_kodu'] = '';
		$data['fis_HareketGrupKodu1'] = '';
		$data['fis_HareketGrupKodu2'] = '';
		$data['fis_HareketGrupKodu3'] = '';


		$data['fis_maliyil'] = explode('-',$nonDefaults['cha_tarihi'])[0];
		$data['fis_tarih'] = $nonDefaults['cha_tarihi'];
		$data['fis_create_date'] = $nonDefaults['cha_lastup_date'];
		$data['fis_lastup_date'] = $nonDefaults['cha_lastup_date'];
		$data['fis_sira_no'] = $this->getFisSiraNo($dateOne->format('Y-m-d'));

		$data['fis_aciklama1'] = 'Tah.mak. : KK-12966/21.01.2020/5370 58** **** 1651/120.07.461/. MEHMET ÖZASLAN';

		$data['fis_meblag0'] = ($odeme_bilgisi['tutar']);
		$data['fis_meblag1'] = $this->dovizHesapla($odeme_bilgisi['tutar']);
		$data['fis_meblag2'] = ($odeme_bilgisi['tutar']);

		$data['fis_sorumluluk_kodu'] = '';
		$data['fis_ticari_tip'] = '';
		$data['fis_ticari_uid'] = $nonDefaults['cha_Guid'];

		$data['fis_tic_evrak_sira'] = $nonDefaults['cha_fis_sirano'];

		$data['fis_tic_belgeno'] = '';
		$data['fis_tic_belgetarihi'] = $nonDefaults['cha_tarihi'];
		$data['fis_yevmiye_no'] = $this->maxYevmiyeNo($dateOne->format('Y'));

		echo json_encode($data);
		die();
	}

	public function dovizHesapla($tutar){
		$ratio = 6.5498;
		return (((float) $tutar)/$ratio);
	}

	public function maxYevmiyeNo($yil){
		$result = '';
		$sql = "SELECT TOP 1 fis_yevmiye_no,fis_maliyil FROM MikroDB_V16_LASTIKCIM.dbo.MUHASEBE_FISLERI where fis_maliyil='".$yil."' order by fis_yevmiye_no desc;";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$result = $res->fis_yevmiye_no;
		} else {
			$result = '1';
		}	
		return $result;
	}

	public function getFisSiraNo($date){
		$result = '';
		$sql = "SELECT TOP 1 fis_create_date, fis_sira_no FROM MikroDB_V16_LASTIKCIM.dbo.MUHASEBE_FISLERI where CONVERT(DATE, fis_create_date)='".$date."' order by fis_sira_no desc;";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			$result = $res->fis_sira_no;
		} else {
			$result = '1';
		}	
		return $result;
	}
}

?>
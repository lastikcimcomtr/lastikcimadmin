<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

// print_r(ini_get_all());

class Lastikcrm
{
	public $conn,$limit;

	function __construct() {
		$this->limit = 100;
		/*$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
        ini_set('mssql.charset', 'UTF-8');
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);*/
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		try {
		  $mysqli = new mysqli("68.183.67.121", "whowwn", "Morsamorsi_3", "crm_tatko");
		  $mysqli->set_charset("utf8mb4");
		  $this->conn = $mysqli;
		} catch(Exception $e) {
		  error_log($e->getMessage());
		  exit('Error connecting to database'); 
		}
	}

	public function defineOrder($order,$fatura,$sevk,$cari){
		$customerData = json_decode($order->data);
		$mainOrder = $this->createMainOrder($order,$customerData,$fatura,$sevk,$cari);
		$products = json_decode($order->urunler);
		$parsedProducts = [];

		foreach ($products as $key => $product) {
			$parsedProducts[] = $this->createOrderLine($product,$customerData,$mainOrder);
		}
		print_r($mainOrder);
		echo '<hr>';
		print_r($order);
		echo '<hr>';
		print_r($fatura);
		echo '<hr>';
		print_r($sevk);
		echo '<hr>';
		print_r($cari);
		die();
	}

	public function createOrderLine($product,$customerData,$mainOrder){
		$orderLine = [];
		$orderLine['order_id'] = $mainOrder['id'];
		$orderLine['merchant_id'] = '1';
		$orderLine['product_type'] = $product->tip;
		$orderLine['product_name'] = $product->urun_adi;
		$orderLine['status_id'] = $this->mainOrder['status_id'];

		$orderLine['shipment_number'] = '';
		$orderLine['shipment_firm'] = '';
		$orderLine['note'] = '';
		$orderLine['code'] = $product->stok_kodu;
		$orderLine['amount'] = '';
		$orderLine['list_price'] = '';
		$orderLine['net_price'] = '';
		$orderLine['price'] = '';
		$orderLine['tax'] = '';
		$orderLine['commission'] = '';
		$orderLine['total_price'] = '';
		$orderLine['cancel_date'] = '';
		$orderLine['created_at'] = '';
		$orderLine['updated_at'] = '';
		print_r($product);
		
	}

	public function createMainOrder($order,$customer,$fatura,$sevk,$cari){
		$mainOrder = [];
		$mainOrder['id'] = '10'.$order->id;
		$mainOrder['status_id'] = $this->convertOrderStatus($order->durumu);
		$mainOrder['payment_type'] = ($customer->tur_sec == "kurumsal" ? 'corporate' : 'individual');
		$mainOrder['orderHash'] = $order->hash_value;
		$mainOrder['mail'] = $cari['email'];
		$mainOrder['full_name'] = $cari['cari_unvan1'];
		$mainOrder['user_details'] = '';
		$mainOrder['note'] = 'Lastikcim Siparişi: #'.$order->id;
		$mainOrder['ip_address'] = $order->ip;
		$mainOrder['cancel_date'] = null;
		$mainOrder['created_at'] = $this->unixToCd($order->tarih);
		$mainOrder['updated_at'] = $this->unixToCd($order->tarih);
		$mainOrder = array_filter($mainOrder);
		return $mainOrder;
	}

	public function unixToCd($unix_time){
		$date = new \DateTime();
		$date->setTimestamp($unix_time);
		return $date->format('Y-m-d H:i:s');
	}

	public function customerMail($customerData, $type){
		return ($type ? $customerData->kur_email : $customerData->email);
	}

	public function convertOrderStatus($status_id){
		$arrayConversion = [];
		$arrayConversion[1] = 2;
		$arrayConversion[2] = 4;
		$arrayConversion[3] = 7;
		$arrayConversion[4] = 10;
		$arrayConversion[5] = 8;
		$arrayConversion[6] = 5;
		$arrayConversion[7] = 3;
		$arrayConversion[8] = 6;
		$arrayConversion[9] = 9;
		$arrayConversion[10] = 1;
		$arrayConversion[11] = 11;
		if (isset($arrayConversion[$status_id])){
			return $arrayConversion[$status_id];
		}else{
			return 1;
		}
	}
}
?>
<?php

class Entegrasyontatko
{
    public $ci, $db_host, $db_name, $db_user, $db_password, $conn, $limit;

    function __construct()
    {
        $this->ci = get_instance();
        $this->limit = 100;

        $this->db_host = '212.174.163.3:1433';
        $this->db_name = 'MikroDB_V15_TATKOLASTIK';
        $this->db_user = 'tatkoeltermainali';
        $this->db_password = '1q2w3e';
        ini_set('mssql.charset', 'UTF-8');
        $this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
        mssql_select_db($this->db_name, $this->conn);
    }

    public function fiyathesapla($fiyat = null, $model = null, $marka = null, $stok = null)
    {
        $oran = 0;

        if (mb_strpos($stok, '-BN-')) {
            switch ($marka) {
                case 'Goodyear':
                    $oran = 5;
                    break;
                case 'Michelin':
                    $oran = 5;
                    break;
                case 'Pirelli':
                    $oran = 5;
                    break;
                case 'Continental':
                    $oran = 5;
                    break;
                case 'Lassa':
                    $oran = 5;
                    break;
                case 'Bridgestone':
                    $oran = 5;
                    break;
                case 'Petlas':
                    $oran = 5;
                    break;
                case 'Hankook':
                    $oran = 5;
                    break;
                case 'Nokian':
                    $oran = 5;
                    break;
                case 'BFGoodrich':
                    $oran = 10;
                    break;
                case 'Kleber':
                    $oran = 5;
                    break;
                case 'Sava':
                    $oran = 5;
                    break;
                case 'Debica':
                    $oran = 5;
                    break;
                case 'Marshal':
                    $oran = 10;
                    break;
                case 'Strial':
                    $oran = 8;
                    break;
                case 'Vitour':
                    $oran = 8;
                    break;
                case 'Goodride':
                    $oran = 8;
                    break;
                case 'Taurus':
                    $oran = 8;
                    break;
                case 'Tatko':
                    $oran = 15;
                    break;
                default:
                    $oran = 10;
                    break;
            }
        } else if (mb_strpos($stok, '-KM-')) {
            $oran = 12;
        } else if(mb_strpos($stok, '-YD-')) {
            $oran = 12;
        } else {
            $oran = 12;
        }

        $fiyat = $fiyat / ((100 - $oran) / 100);

        /*24.01.2018*/
        $kdvekle = (($fiyat * 18) / 100);
        $fiyat = $fiyat + $kdvekle;
        /*24.01.2018*/

        $fiyat = round($fiyat, 0, PHP_ROUND_HALF_DOWN);

        return $fiyat;
    }

    public function miktarhesapla($miktar = null)
    {
        if ($miktar < 0) {
            $miktar = 0;
        } else {
            $miktar = (int)$miktar;
        }
        return $miktar;
    }

    public function stoklar()
    {
        $data = array();
        // $sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat, ay.['Model Kodu'] as mdl from M5VW_STOKMIKTARLASTIKCIM as ay group by ay.stok";
        $sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat, ay.Markaismi as marka, ay.[Model Kodu] as model, ay.dot from M5VW_STOKMIKTARLASTIKCIM as ay group by ay.stok, ay.[Model Kodu], ay.Markaismi, ay.dot";
        $res = mssql_query($sql);
        $count = mssql_num_rows($res);
        if ($count > 0) {
            while ($row = mssql_fetch_array($res)) {
                $data[] = $this->convert($row);
            }
        }
        return $data;
    }

    /**/
    private function convert($data = null)
    {
        $keys = (array_keys($data));
        foreach ($keys as $key) {
            $data[$key] = utf8_encode($data[$key]);
            $data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
            $data[$key] = str_replace("Ý", "İ", $data[$key]);
            $data[$key] = str_replace("ý", "ı", $data[$key]);
            $data[$key] = str_replace("þ", "ş", $data[$key]);
            $data[$key] = str_replace("Þ", "Ş", $data[$key]);
            $data[$key] = str_replace("ð", "ğ", $data[$key]);
            $data[$key] = str_replace("Ð", "Ğ", $data[$key]);
        }
        return $data;
    }
    /**/

}

?>
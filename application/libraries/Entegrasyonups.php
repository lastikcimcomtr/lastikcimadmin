<?php

 ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
 //ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

class Entegrasyonups 
{	
	protected $ci;
	public $client;
	public $session_id;
	public $base_url,$ups_login_url,$wsdl;
	public $CustomerNumber, $UserName, $Password;
	function __construct(){
		$this->ci = get_instance();
		$this->base_url = "http://ws.ups.com.tr/wsCreateShipment/wsCreateShipment.asmx";
		$this->ups_login_url = "http://ws.ups.com.tr/wsCreateShipment/wsCreateShipment.asmx";//"http://ws.ups.com.tr/wsCreateShipment/Login_Type1";
		$this->CustomerNumber = "72154F";
		$this->UserName = "4M96nEhhtKHHNrD5hL9k";
		$this->Password = "3uLfMHsR85T9FcTpflhP";
		$this->wsdl = "http://ws.ups.com.tr/wsCreateShipment/wsCreateShipment.asmx?WSDL";
	}

	public function soapLogin(){
		$login_info = ['CustomerNumber' => $this->CustomerNumber, 'UserName' => $this->UserName, 'Password' => $this->Password];
		$this->client = new SoapClient($this->wsdl);
		$result = $this->client->__soapCall("Login_Type1", ["Login_Type1" => $login_info], array('location' => $this->base_url ), NULL);
		$session_id = $result->Login_Type1Result->SessionID;
		$this->session_id = $session_id;
	}

	public function processOrder($order){
		$this->soapLogin();
		$shipments = [];
		$order_products = json_decode($order->urunler);
		$depo_data = json_decode($order->depo_data);
		foreach ($depo_data as $key => $depo) {
			if(!isset($shipments[$depo->id])){
				$shipments[$depo->id]['tedarikci'] = (array) $this->ci->db->where('id',$depo->tedarikci_id)->select('*')->from('tedarikciler')->get()->row(); 
				$shipments[$depo->id]['depo'] = (array) $this->ci->db->where('id',$depo->id)->select('*')->from('tedarikci_depolar')->get()->row(); 
			}
			$depo_products = (array) $depo->products;
			foreach ($order_products as $key => $product_line) {
				if(in_array($product_line->stok_kodu, $depo_products)){
					$shipments[$depo->id]['urunler'][] = $product_line;
				}
			}
		}

		/*
		foreach ($order_products as $key => $product_line) {
			if(!isset($shipments[$product_line->tedarikci])){
				$shipments['tedarikci'] = $this->ci->db->where('kod',$product_line->tedarikci)->select('*')->from('tedarikciler')->get()->row(); 
			}
			$shipments[$product_line->tedarikci]['urunler'][] = $product_line;
		}
		*/
		$all_shipment_results = [];
		foreach ($shipments as $key => $shipment) {
				$all_shipment_results[$key] = $this->processItem($shipment,$order);
		}
		$this->processShipmentResult($all_shipment_results,$order);
	}

	public function processShipmentResult($all_shipment_results,$order){
		$ups_image = [];
		$ups_barcode = [];
		$ups_link = [];
		$ups_result = [];
		foreach ($all_shipment_results as $key => $shipment_result) {
			$ups_image[$key] = $shipment_result->BarkodArrayPng;
			$ups_barcode[$key] = $shipment_result->ShipmentNo;
			$ups_link[$key] = $this->session_id;
			$ups_result[$key] = $shipment_result;
		}

		$order_update['ups_image'] = json_encode($ups_image);
		$order_update['ups_barcode'] = json_encode($ups_barcode);
		$order_update['ups_link'] = json_encode($ups_link);
		$order_update['ups_result'] = json_encode($ups_result);
		$this->ci->db->where('id',$order->id)->update('siparisler',$order_update);
	}

	public function getMikroData($order){
		$this->ci->load->library('lastikcim');
		$cari_data = json_decode($order->caridata);
		$cari_kod = $cari_data->kod;
		$sevk_adres = json_decode($order->cariadresdata_sevk);
		$sevk_adres_no = $sevk_adres->no;
		$cari_data = $this->ci->lastikcim->mevcutCariyiGetirId($cari_kod);
		$adres_data = $this->ci->lastikcim->mevcutAdresiGetirId($cari_kod,$sevk_adres_no);
		return array_merge($cari_data,$adres_data);  
	}
	
	public function processItem($shipment,$order){
		$mikro_data = $this->getMikroData($order);

		if(in_array($order->durumu,[9,11])){
			$shipment_data = $this->createUpsReturnData($mikro_data,$shipment,$order);
		}else{
			$shipment_data = $this->createUpsData($mikro_data,$shipment,$order);
		}
		$check_shipment_data = $this->checkShipmentData($shipment_data);
		if($check_shipment_data != 'ok'){
			echo $check_shipment_data;
			return $check_shipment_data;
		}
		//$shipment_processed = $this->processShipment($shipment_data);
		$shipment_processed = $this->processShipmentAndDemand($shipment_data);
		return $shipment_processed->OnDemandPickupRequest_Type1Result;
	}

	public function processShipmentAndDemand($shipment_data){
		$send_data = [];
		$send_data['SessionID'] = $this->session_id;
		$request_info = [];
		$request_info['LabelSource'] = 2;

	  	$timezone = new DateTimeZone('Europe/Istanbul');
	  	$date = new DateTime('');
    	$date->setTimezone($timezone);
	  	$current_hour = $date->format('H');
	  	if($current_hour > 15){
	  		$date = $date->modify('+1 day');
	  		$date = $date->setTime('09','00');
	  	}
        $week_day = $date->format('w');
        if($week_day == 0){
            $date = $date->modify('+1 day');
        }else if($week_day == 6){
            $date = $date->modify('+2 day');
        }
		$dateTime = $date->format('c');
		$request_info['PickupRequestDay'] = $dateTime;
		$request_info['RequestedBoxList'] = [];
		$shipment_data['IdControlFlag'] = '0';
		$shipment_data['PhonePrealertFlag'] = '0';
		$shipment_data['SmsToShipper'] = '0';
		$shipment_data['InsuranceValue'] = '0';
		$shipment_data['ValueOfGoods'] = '0';
		$shipment_data['ConsigneeAccountNumber'] = $shipment_data['ShipperAccountNumber'];
		unset($shipment_data['ShipperAccountNumber']);
		$shipment_data['ValueOfGoodsPaymentType'] = '0';


		$shipment_data['PackageDimensions'] = [];

		$request_info['ShipmentInfo'] = $shipment_data;
		$send_data['OnDemandPickupRequestInfo'] = $request_info;
		$result = $this->client->__soapCall("OnDemandPickupRequest_Type1", ["OnDemandPickupRequest_Type1" => $send_data], array('location' => $this->base_url ), NULL);
		return $result;
	}

	public function processShipment($shipment_data){
		$send_data = [];
		$send_data['ReturnLabelLink'] = 1;
		$send_data['ReturnLabelImage'] = 1;
		$send_data['ShipmentInfo'] = $shipment_data;
		$result = $this->client->__soapCall("CreateShipment_Type3", ["CreateShipment_Type3" => $send_data], array('location' => $this->base_url ), NULL);
		return $result;
	}

	public function checkShipmentData($shipment_data){
		foreach ($shipment_data as $key => $data_line) {
			if($data_line == '' ||  $data_line == NULL){
				return $key;
			}
		}
		return 'ok';
	}

	public function createUpsData($mikro_data,$shipment,$order_data){
		$conversion = [];
		/*Tedarikçi*/
		$shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
		$shipper_area_id = $this->findAreaCode($shipper_city,$shipment['depo']['depo_ilce']);

		$cari_il_ilce = explode('/',$mikro_data['il_ilce']);
		$cari_il = $this->findCityCode($cari_il_ilce[1]);
		$cari_area_id = $this->findAreaCode($cari_il,$cari_il_ilce[0]);


		$conversion['ShipperAccountNumber'] = $this->CustomerNumber;
		$conversion['ShipperName'] = 'LASTİKCİM PZ. İÇ VE DIŞ TİCARET A.Ş.';
		$conversion['ShipperContactName'] = 'LASTİKCİM';
		$conversion['ShipperAddress'] = $shipment['depo']['depo_adres'];
		$conversion['ShipperCityCode'] = $shipper_city;
		$conversion['ShipperAreaCode'] = $shipper_area_id;
		$conversion['ShipperPhoneNumber'] = '08508111101';
		$conversion['ShipperEMail'] = 'info@lastikcim.com.tr';
		/*Alıcı*/
		$conversion['ConsigneeName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
		$conversion['ConsigneeAddress'] = $this->clearString($mikro_data['tam_adres']);
		$conversion['ConsigneeCityCode'] = $cari_il;
		$conversion['ConsigneeAreaCode'] = $cari_area_id;
		$conversion['ConsigneePhoneNumber'] = $mikro_data['telefon'];
		$phone_3_char = substr($mikro_data['telefon'],0,3);
		$phone_2_char = substr($mikro_data['telefon'],0,2);
		if($phone_3_char == '905' || $phone_2_char == "05"){
			$conversion['ConsigneeMobilePhoneNumber'] = $mikro_data['telefon'];
		}else{
			$conversion['ConsigneeMobilePhoneNumber'] = '0850 811 11 01';
		}
		$conversion['ConsigneeEMail'] = $mikro_data['email'];
		/*Ürün ve genel*/
		$conversion['ServiceLevel'] = '3';
		$conversion['PaymentType'] = '2';
		$conversion['PackageType'] = 'K';
		$conversion['NumberOfPackages'] = $this->getProductsTotalAmount($shipment['urunler']);
		$conversion['CustomerReferance'] = $order_data->id.' nolu sipariş';
		$conversion['DescriptionOfGoods'] = $this->getProductsName($shipment['urunler']);
		$conversion['DeliveryNotificationEmail'] = 'depo@lastikcim.com.tr';
		$conversion['SmsToConsignee'] = 1;

		return $conversion;
	}


	public function createUpsReturnData($mikro_data,$shipment,$order_data){
		$conversion = [];
		/*Tedarikçi*/
		$shipper_city = $this->findCityCode($shipment['depo']['depo_il']);
		$shipper_area_id = $this->findAreaCode($shipper_city,$shipment['depo']['depo_ilce']);

		$cari_il_ilce = explode('/',$mikro_data['il_ilce']);
		$cari_il = $this->findCityCode($cari_il_ilce[1]);
		$cari_area_id = $this->findAreaCode($cari_il,$cari_il_ilce[0]);


		$conversion['ShipperAccountNumber'] = $this->CustomerNumber;

		$conversion['ShipperName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
		$conversion['ShipperContactName'] = $this->clearString(substr($mikro_data['cari_unvan1'] , 0, 39));
		$conversion['ShipperAddress'] = $this->clearString($mikro_data['tam_adres']);
		$conversion['ShipperCityCode'] = $cari_il;
		$conversion['ShipperAreaCode'] = $cari_area_id;
		$conversion['ShipperPhoneNumber'] = $mikro_data['telefon'];
		$conversion['ShipperEMail'] = $mikro_data['email'];
		/*Alıcı*/
		$conversion['ConsigneeName'] = 'LASTİKCİM PZ. İÇ VE DIŞ TİCARET A.Ş.';
		$conversion['ConsigneeAddress'] = $shipment['depo']['depo_adres'];
		$conversion['ConsigneeCityCode'] = $shipper_city;
		$conversion['ConsigneeAreaCode'] = $shipper_area_id;
		$conversion['ConsigneePhoneNumber'] = '08508111101';
		$conversion['ConsigneeMobilePhoneNumber'] = '0850 811 11 01';
		$conversion['ConsigneeEMail'] = 'info@lastikcim.com.tr';
		
		/*Ürün ve genel*/
		$conversion['ServiceLevel'] = '3';
		$conversion['PaymentType'] = '2';
		$conversion['PackageType'] = 'K';
		$conversion['NumberOfPackages'] = $this->getProductsTotalAmount($shipment['urunler']);
		$conversion['CustomerReferance'] = $order_data->id.' nolu sipariş';
		$conversion['DescriptionOfGoods'] = $this->getProductsName($shipment['urunler']);
		$conversion['DeliveryNotificationEmail'] = 'depo@lastikcim.com.tr';
		$conversion['SmsToConsignee'] = 1;

		return $conversion;
	}

	


	public function getProductsTotalAmount($products){
		$toplam = 0;
		foreach ($products as $key => $product) {
			$toplam += $product->miktar;
		}
		return $toplam;
	}


	public function getProductsName($products){
		$products_name = [];
		foreach ($products as $key => $product) {
			$products_name[] = $product->urun_adi;
		}
		return implode(',',$products_name);
	}

	public function findAreaCode($il_id,$ilce_adi){
		$ilce = $this->ci->db->select('*')->from('ilceler')->where('ilce',$ilce_adi)->where('il_id',$il_id)->get()->row();
		return $ilce->area_id;
	}

	public function findCityCode($city){
		$il = $this->ci->db->select('*')->from('iller')->where('il',$city)->get()->row();
		return $il->id;
	}

	public function clearString($string){
		$string = str_replace("-", " ", $string);
		$string = str_replace("/", " ", $string);
		$string = str_replace("\\", " ", $string);
		$string = str_replace("*", " ", $string);
		$string = str_replace("_", " ", $string);
		return mb_convert_encoding($string,"UTF-8","auto");
	}
}

?>
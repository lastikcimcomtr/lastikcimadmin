<?php

 ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
 //ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

class Entegrasyonupsstatus
{	
	protected $ci;
	public $client;
	public $session_id;
	public $base_url,$ups_login_url,$wsdl;
	public $CustomerNumber, $UserName, $Password;
	function __construct(){
		$this->ci = get_instance();
		$this->base_url = "http://ws.ups.com.tr/QueryPackageInfo/wsQueryPackagesInfo.asmx";
		$this->ups_login_url = "http://ws.ups.com.tr/QueryPackageInfo/wsQueryPackagesInfo.asmx";//"http://ws.ups.com.tr/wsCreateShipment/Login_Type1";
		$this->CustomerNumber = "72154F";
		$this->UserName = "4M96nEhhtKHHNrD5hL9k";
		$this->Password = "3uLfMHsR85T9FcTpflhP";
		$this->wsdl = "http://ws.ups.com.tr/QueryPackageInfo/wsQueryPackagesInfo.asmx?WSDL";

	}

	public function soapLogin(){
		$login_info = ['CustomerNumber' => $this->CustomerNumber, 'UserName' => $this->UserName, 'Password' => $this->Password];
		$this->client = new SoapClient($this->wsdl);
		$result = $this->client->__soapCall("Login_V1", ["Login_V1" => $login_info], array('location' => $this->base_url ), NULL);
		$session_id = $result->Login_V1Result->SessionID;
		$this->session_id = $session_id;
	}

	public function checkBarcodeStatus($barcode){
		$this->soapLogin();
		$result = $this->getShipmentInfo($barcode);
		return $result;
	}

	public function getShipmentInfo($tracking_number){
		$send_data = [];
		$send_data['TrackingNumber'] = $tracking_number;
		$send_data['SessionID'] = $this->session_id;
		$send_data['InformationLevel'] = 1;
		$result = 'Barkod Oluşturuldu.';
 		$result = $this->client->__soapCall("GetTransactionsByTrackingNumber_V1", ["GetTransactionsByTrackingNumber_V1" => $send_data], array('location' => $this->base_url ), NULL);
 		$result = $result->GetTransactionsByTrackingNumber_V1Result->PackageTransaction;

 		if(!is_array($result)){
 			$result = [0 => $result];
 		}
 		foreach ($result as $key => $line_info) {
 			$statusCode = $line_info->StatusCode;
 			$state = $this->descriptionFromLine($statusCode);
 			if($state['COMPLETE']){
 				return $state;
 			}elseif($state['CANCELED']){
 				return $state;
 			}
 		}
 		return $state;
	}
	public function descriptionFromLine($status){
		$array_descriptions = [];
		$array_descriptions[0] = ['STATE' => 'BARKOD OLUŞTU', 'HAREKET' => 0, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[1] = ['STATE' => 'BARKOD TARAMASI -1', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[2] = ['STATE' => 'TESLIM EDILDI', 'HAREKET' => TRUE, 'COMPLETE' => 1, 'CANCELED' => 0];
		$array_descriptions[3] = ['STATE' => 'OZEL DURUM', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[4] = ['STATE' => 'DAGITIMDA', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[5] = ['STATE' => 'DAGITIMDAN DONDU', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[6] = ['STATE' => 'SUBE HAREKETI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[7] = ['STATE' => 'SUBEDEN GELDI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[12] = ['STATE' => 'KONTEYNERDA', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[15] = ['STATE' => 'MANIFESTO FAZLASI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[16] = ['STATE' => 'KONTEYNERDAN INDI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[17] = ['STATE' => 'GONDERENE IADE', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[18] = ['STATE' => 'MUSTERI TOPLU GIRIS', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[19] = ['STATE' => 'SUBEDE BEKLIYOR', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[30] = ['STATE' => 'KONSOLOSLUKTA', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[31] = ['STATE' => 'CAGRI TESLIM ALINDI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[32] = ['STATE' => 'DEPOYA GIRDI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[33] = ['STATE' => 'DEPODAN CIKTI', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[34] = ['STATE' => 'EDI BILGI TRANSFER', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[35] = ['STATE' => 'MUSTERI DEPODA OKUNDU', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[36] = ['STATE' => 'TOPLU DAGITIMDA', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[37] = ['STATE' => 'TRANSIT KARSILAMA', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		$array_descriptions[38] = ['STATE' => 'TRANSIT CIKIS', 'HAREKET' => TRUE, 'COMPLETE' => 0, 'CANCELED' => 0];
		return $array_descriptions[$status];
	}


}

?>
<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('memory_limit', '1024M');
//ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');
// ini_set('mssql.charset', 'UTF-8');

// print_r(ini_get_all());

class Tatkomikro
{
	public $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	public $path, $cache_time;
	function __construct() {
        $this->ci = get_instance();
		$this->limit = 100;
		/*$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
        ini_set('mssql.charset', 'UTF-8');
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);*/
        ini_set('mssql.charset', 'UTF-8');
        $serverName = '213.153.177.103,1433'; 
        $connectionInfo = array( "Database"=>"MikroDB_V16_TATKOLASTIK", "UID"=>"lastikcim", "PWD"=>"3[s_&:#+vLuTYNC:", "CharacterSet" => "UTF-8");
        $this->conn  = 	sqlsrv_connect( $serverName, $connectionInfo);
		$this->conn  = sqlsrv_connect( $serverName, $connectionInfo);
        if ($this->conn === false) {  
            $this->failed_connection(json_encode(sqlsrv_errors()));
            die('Hata Oluştu, Mail Gönderildi');
        }  
		$this->path = $_SERVER["DOCUMENT_ROOT"]."/";
		$this->cache_time = 1*24*60*60;
	}


    public function failed_connection($message){
            $this->ci->load->model('mail/Mail_model', 'mail');
            $html = '<html>Zaman: '.time().'<br>Lastikcim Tatko Entegrasyonunda bir hata oluştu.:'.$message.'</html>';
            $mail_status = $this->ci->mail->htmlmail(array('email'=>'berkaykilic@lastikcim.com.tr','title'=>'Mikro : Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            //$mail_status = $this->ci->mail->htmlmail(array('email'=>'harunozel@lastikcim.com.tr','title'=>'Mikro : Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            //$mail_status = $this->ci->mail->htmlmail(array('email'=>'mustafa.vurulkan@tatko1927.com','title'=>'Mikro : Entegrasyonda Bir Hata Oluştu!!', 'html'=> $html)); 
            return false;
    }
	/*Tablolarda Ara*/
	public function tablolar()
	{
		$sql = "select * from sys.tables";
		$res = sqlsrv_query($this->conn,$sql);
		$count = sqlsrv_num_rows($res);

		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = sqlsrv_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function columlar($tablo_id = NULL)
	{
		if ($tablo_id) {
			$sql = "select * from sys.columns where collation_name is not null and object_id = ".$tablo_id;
			$res = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function tablodara($tablo_adi = NULL, $colum_adi = NULL, $aranan = NULL)
	{
		if ($tablo_adi && $colum_adi && $aranan) {

			$sql = "select * from ".$tablo_adi." where ".$colum_adi." like '%".$aranan."%' collate Turkish_CS_AS";
			$res = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}
	private function convert($data = NULL)
	{
		$keys = (array_keys($data));

		foreach($keys as $key) if (!is_numeric($key)) {
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		} else {
			unset($data[$key]);
		}
		return $data;
	}
	
	private function eklerken($data)
	{
		
		$data = str_ireplace("Ç", "C", $data);

		$data = str_replace("İ", "Ý", $data);
		
		$data = str_replace("ı", "ý", $data);
		
		$data = str_replace("ş", "þ", $data);
		
		$data = str_replace("Ş", "Þ", $data);
		
		$data = str_replace("ğ", "ð", $data);
		
		$data = str_replace("Ğ", "Ð", $data);

		$data = utf8_decode($data);

		return $data;
	}
	/*Tablolarda Ara*/
	
	/*Cari İşlemler*/
	public function cariHesapBilgileri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAPLAR where cari_kod = '".$cari_kod."'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function cariHesapAdresleri($cari_kod = NULL)
	{

		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from CARI_HESAP_ADRESLERI where adr_cari_kod = '".$cari_kod."'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	/*Sipariş İşlemleri*/
	public function siparisBilgileri($cari_kod = NULL)
	{
		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from SIPARISLER where sip_musteri_kod = '".$cari_kod."' and sip_evrakno_seri = 'LC'";
			$res   = sqlsrv_query($this->conn,$sql);
			$count = sqlsrv_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = sqlsrv_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function dovizKuru()
	{
		$sql = "select dbo.fn_KurBul('', 1, 1) as kur";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->kur;
		} else {
			return 1;
		}
	}

	public function siparisIdAlalim()
	{
		$sql = "SELECT max(sip_evrakno_sira)+1 as numara from SIPARISLER where sip_evrakno_seri = 'LC'";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			if ((int)$res->numara == 0) {
				return 1;
			} else {
				return $res->numara;
			}
		} else {
			return 1;
		}
	}

	public function siparisRecnoAlalim()
	{
		$sql = "SELECT * from  SIPARISLER";
		$res = sqlsrv_query($this->conn,$sql);
		$res = sqlsrv_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	private function siparisOzetOlustur($data = NULL)
	{
		$veriler = array();
		// $veriler['so_RECno'] 			= "";
		$veriler['so_firmano'] 			= "0";
		$veriler['so_subeno'] 			= "0";
		$veriler['so_Tipi'] 			= "0";
		$veriler['so_Kodu'] 			= "'".$data['stok_kod']."'";
		$veriler['so_SrmMerkezi'] 		= "''";
		$veriler['so_ProjeKodu'] 		= "''";
		$veriler['so_Depo'] 			= "1";
		$veriler['so_MaliYil'] 			= "".(int)date('Y', time())."";
		$veriler['so_Donem'] 			= "".(int)date('m', time())."";
		$veriler['so_HareketCins'] 		= "0";
		$veriler['so_TalepMiktar'] 		= "".$data['adet']."";
		$veriler['so_TalepKarsilanan'] 	= "0";
		$veriler['so_TalepKapanan'] 	= "0";
		$veriler['so_TeminMiktar'] 		= "0";
		$veriler['so_TeminKarsilanan'] 	= "0";
		$veriler['so_TeminKapanan'] 	= "0";
		print_r($veriler);
		die();	
		$sql = "insert into SIPARISLER_OZET (".implode(", ", array_keys($veriler)).") VALUES(".implode(", ", array_values($veriler)).")";
		$res = sqlsrv_query($this->conn,$sql);
		if ($res) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function siparisOlustur($data = NULL)
	{
		$response 	= array();
		$veriler 	= array();

		$boslar = '{"sip_special1":"5","sip_special2":" ","sip_special3":" ","sip_belgeno":" ","sip_satici_kod":" ","sip_aciklama2":" ","sip_cari_sormerk":"CARİ","sip_stok_sormerk":"K.01.002","sip_teslimturu":" ","sip_Exp_Imp_Kodu":" ","sip_parti_kodu":" ","sip_projekodu":" ","sip_paket_kod":" ","sip_kapatmanedenkod":" ","sip_onodeme_evrak_seri":" "}';
		$boslar = json_decode($boslar, TRUE);

		$veriler = array_merge($veriler, $boslar);

		$sifirlar = '{"sip_SpecRECno":"0","sip_iptal":"0","sip_hidden":"1","sip_kilitli":"1","sip_degisti":"0","sip_checksum":"0","sip_firmano":"0","sip_subeno":"0","sip_tip":"0","sip_cins":"0","sip_teslim_miktar":"0","sip_iskonto_1":"0","sip_iskonto_2":"0","sip_iskonto_3":"0","sip_iskonto_4":"0","sip_iskonto_5":"0","sip_iskonto_6":"0","sip_masraf_1":"0","sip_masraf_2":"0","sip_masraf_3":"0","sip_masraf_4":"0","sip_masvergi_pntr":"0","sip_masvergi":"0","sip_OnaylayanKulNo":"298","sip_vergisiz_fl":"0","sip_kapat_fl":"0","sip_promosyon_fl":"0","sip_cari_grupno":"0","sip_doviz_cinsi":"0","sip_cagrilabilir_fl":"0","sip_iskonto1":"0","sip_isk1":"0","sip_isk2":"0","sip_isk3":"0","sip_isk4":"0","sip_isk5":"0","sip_isk6":"0","sip_mas1":"0","sip_mas2":"0","sip_mas3":"0","sip_mas4":"0","sip_kar_orani":"0","sip_durumu":"0","sip_planlananmiktar":"0","sip_lot_no":"0","sip_fiyat_liste_no":"0","sip_Otv_Pntr":"0","sip_Otv_Vergi":"0","sip_otvtutari":"0","sip_OtvVergisiz_Fl":"0","sip_onodeme_evrak_tip":"0","sip_onodeme_evrak_sira":"0","sip_rezervasyon_miktari":"0","sip_rezerveden_teslim_edilen":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array_merge($veriler, $sifirlar);

		/**/
		// $veriler['sip_RECno'] = "";

		if ($data['ozel_siparis_id'] && is_numeric($data['ozel_siparis_id'])) {
			$siparis_id_alinan = $data['ozel_siparis_id'];
		} else {
			$siparis_id_alinan = $this->siparisIdAlalim();
		}

		$veriler['sip_fileid'] = "21";
		$veriler['sip_create_user'] = "489";
		$veriler['sip_create_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_lastup_user'] = "489";
		$veriler['sip_lastup_date'] = date('Y-m-d 00:00:00.000', time());
		$veriler['sip_tarih'] 			= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_teslim_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_evrakno_seri'] 	= "LC";
		$veriler['sip_vergi_pntr'] 		= "4";
		$veriler['sip_belge_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_depono'] 			= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_iskonto2'] 		= "0";
		$veriler['sip_iskonto3'] 		= "0";
		$veriler['sip_iskonto4']		= "0";
		$veriler['sip_iskonto5'] 		= "0";
		$veriler['sip_iskonto6'] 		= "0";
		$veriler['sip_masraf1'] 		= "0";
		$veriler['sip_masraf2'] 		= "0";
		$veriler['sip_masraf3'] 		= "0";
		$veriler['sip_masraf4'] 		= "0";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_gecerlilik_tarihi'] = date('Y-m-d 00:00:00.000', (time() + 5000000));
		$veriler['sip_aciklama'] = "AE:".$siparis_id_alinan." ";
		/**/

		$alinacaklar = array();
		$alinacaklar['sip_evrakno_sira'] 	= "".$siparis_id_alinan."";
		$alinacaklar['sip_musteri_kod'] 	= "120.34.00302";
		$alinacaklar['sip_opno'] 			= "-120"; // Ödeme Planı 0 - Peşin -- 1 - Kredi Kartı
		$alinacaklar['sip_alt_doviz_kuru'] 	= "".$this->dovizKuru()."";
		$alinacaklar['sip_adresno'] 		= "1";

		$veriler = array_merge($veriler, $alinacaklar);

		if (count($data['urunler']) > 0) {
			$satir_no = 0;
			foreach ($data['urunler'] as $urun_key => $urun_value) {
				$tekseferlik = $veriler;
				$urunler = array();
				$rec_no_alinan = $this->siparisRecnoAlalim();
				$urun_detay = $urun_value['urun_detay'];
				$urun_marka = json_decode($urun_detay->markalar);
				$urun_marka = $urun_marka[0]->name;
				$urun_xml_stok_kodu = $urun_detay->xml_stok_kodu;
				//$xml_stok_kodu = 

				//$urunler['sip_RECid_RECno'] 	= "".$rec_no_alinan."";
				$urunler['sip_stok_kod'] 		= "".$urun_value['stok_kod']."";
				$urunler['sip_b_fiyat'] 		= "".$this->fiyathesapla($urun_value['stok_fiyat'],null,$urun_marka,$urun_xml_stok_kodu)."";
				$urunler['sip_miktar'] 			= "".$urun_value['adet']."";
				$urunler['sip_tutar'] 			= "".$this->fiyathesapla($urun_value['stok_toplam'],null,$urun_marka,$urun_xml_stok_kodu)."";
				$urunler['sip_vergi'] 			= "".$this->fiyathesapla($urun_value['stok_vergi'],null,$urun_marka,$urun_xml_stok_kodu)."";
				$urunler['sip_satirno'] 		= "".$satir_no."";
				$urunler['sip_birim_pntr'] 		= "".$urun_value['stok_birim']."";
				$urunler['sip_harekettipi']		= "".$urun_value['stok_tip']."";

				/*Özet Oluştur*/
				// $ozet_res = $this->siparisOzetOlustur($urun_value);				
				/*Özet Oluştur*/

				$tekseferlik = array_merge($tekseferlik, $urunler);

				foreach ($tekseferlik as $key => $value) if (is_string($value)){
					$tekseferlik[$key] = "'".$value."'";
				}
				echo json_encode($tekseferlik);
				die();
				$sql = "insert into SIPARISLER (".implode(", ", array_keys($tekseferlik)).") VALUES(".implode(", ", array_values($tekseferlik)).")";
				$res = sqlsrv_query($this->conn,$sql);

				$response['urunler'][$urun_key] 				= $urun_value;
				$response['urunler'][$urun_key]['status'] 		= true;
				$response['urunler'][$urun_key]['rec_no'] 		= $rec_no_alinan;
				// $response['urunler'][$urun_key]['oz_status'] 	= $ozet_res;
				
				$satir_no++;
			}
		}
		$response['siparis_id_alinan'] 	= $siparis_id_alinan;
		$response['cari_kod']			= $data['cari_kod'];

		return $response;
	}
	/*Sipariş İşlemleri*/
   public function fiyathesapla($fiyat = null, $model = null, $marka = null, $stok = null)
    {	
		$iskonto_orani = $this->ci->db->select('*')->from('ayarlar')->where('name','iskonto_orani')->get()->row()->value;
		$filters = (array)  json_decode($iskonto_orani);
        $oran = 0;
		foreach($filters as $key => $value){
			$value = (array) $value;
			$oran = $filters['Default'];
			 if (mb_strpos($stok, $key)) {			
				 if(sizeOf($value) > 1){ 
					 $oran = $value['Default'];
					 foreach($value as $key_2 => $marj){
                        if (mb_strpos($marka, $key_2) !== false){
    						 $oran = $marj;
    						 break;
				        }
					 }
				 }else{
					 $oran = array_pop($value);
				 }
				 break;
			 }
		}
        $fiyat = $fiyat * ((100 - $oran) / 100);

        /*24.01.2018*/
        //$kdvekle = (($fiyat) / 100);
        $fiyat = $fiyat;
        /*24.01.2018*/

        $fiyat = round($fiyat, 4, PHP_ROUND_HALF_DOWN);

        return $fiyat;
    }

}

?>
<?php

class Entegrasyontatko
{
	public $ci, $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	function __construct() {
		$this->ci = get_instance();
		$this->limit = 100;

		$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);
	}

	public function fiyathesapla($fiyat = NULL, $model = NULL, $marka = NULL)
	{
		switch ($model) {
			case 'BİNEK':

				switch ($marka) {
					case 'Kleber':
						$oran = 5;
					break;

					case 'Michelin':
					    $oran = 5;
					    break;
					case 'Goodyear':
					    $oran = 5;
					    break;
					case 'Pirelli':
					    $oran = 5;
					    break;
					case 'Continental':
                        $oran = 5;
                        break;
					case 'Lassa':
                        $oran = 5;
                        break;
					case 'Bridgestone':
                        $oran = 5;
                        break;
					case 'Petlas':
                        $oran = 5;
                        break;
					case 'Hankook':
                        $oran = 5;
                        break;
					case 'Nokian':
                        $oran = 5;
                        break;
					case 'BFGoodrich':
                        $oran = 5;
                        break;
					case 'Sava':
                        $oran = 5;
                        break;
					case 'Debica':
						$oran = 5;
					break;

					case 'Marshal':
                        $oran = 8;
                        break;
					case 'Strial':
                        $oran = 8;
                        break;
					case 'Vitour':
                        $oran = 8;
                        break;
					case 'Goodride':
						$oran = 8;
					break;

					default:
						$oran = 8;
					break;
				}

			break;
			
			default:
				$oran = 15;
			break;
		}

		$fiyat = $fiyat / ((100-$oran)/100);
		$fiyat = $fiyat;

		/*24.01.2018*/
		$kdvekle = (($fiyat * 18)/100);
		$fiyat = $fiyat+$kdvekle;
		/*24.01.2018*/

		$fiyat = round($fiyat, 0, PHP_ROUND_HALF_DOWN);

		return $fiyat;
	}

	public function miktarhesapla($miktar = NULL)
	{
		if ($miktar < 0) {
			$miktar = 0;
		} else {
			$miktar = (int) $miktar;
		}
		return $miktar;
	}

	public function stoklar()
	{
		$data = array();
		// $sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat, ay.['Model Kodu'] as mdl from M5VW_STOKMIKTARLASTIKCIM as ay group by ay.stok";
		$sql = "select sum(ay.miktar) as miktar, ay.stok, max(ay.fiyat) as fiyat, ay.Markaismi as marka, ay.[Model Kodu] as model, ay.dot from M5VW_STOKMIKTARLASTIKCIM as ay group by ay.stok, ay.[Model Kodu], ay.Markaismi, ay.dot";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			while ($row = mssql_fetch_array($res)) {
	        	$data[] = $this->convert($row);
		    }
		}
		return $data;
	}

	/**/
	private function convert($data = NULL)
	{
		$keys = (array_keys($data));
		foreach($keys as $key){
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		}
		return $data;
	}
	/**/

}
?>
<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
ini_set('memory_limit', '1024M');
ini_set('mssql.charset', 'UTF-8');
date_default_timezone_set('Europe/Istanbul');
//ini_set('memory_limit', '-1')
// ini_set('mssql.charset', 'Turkish_CS_AS');
class Tatko
{
	public $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	public $path, $cache_time;
	function __construct() {
		$this->limit = 100;
		$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		// $this->db_name = 'MikroDB_V15_BUDUR';
		// $this->db_name = "LASTIKCIM";
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
		//$connection_string = "DRIVER={SQL Server};SERVER=$this->db_host;DATABASE=$this->db_name;CharacterSet=UTF-8;"; 
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);
		//mssql_init(sp_name)
		$this->path = $_SERVER["DOCUMENT_ROOT"]."/";
		$this->cache_time = 1*24*60*60;
	}

	/*Sipariş Filtreleri*/
	public function siparisveremezler($cari_kod = NULL)
	{
		if ($cari_kod) {
			$sql = "select distinct sto_kod from mv4_carininsiparisveremeyecegistoklar('".$cari_kod."')";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function siparisveremez($cari_kod = NULL ,$stok_kodu = NULL)
	{
		if ($cari_kod && $stok_kodu) {
			$sql = "select distinct sto_kod from mv4_carininsiparisveremeyecegistoklar('".$cari_kod."') where sto_kod = '".$stok_kodu."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}
	/*Sipariş Filtreleri*/

	/*Tablolarda Ara*/
	public function tablolar()
	{
		$sql = "select * from sys.tables";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);

		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function columlar($tablo_id = NULL)
	{
		if ($tablo_id) {
			$sql = "select * from sys.columns where collation_name is not null and object_id = ".$tablo_id;
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			// var_dump($count);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function tablodara($tablo_adi = NULL, $colum_adi = NULL, $aranan = NULL)
	{
		if ($tablo_adi && $colum_adi && $aranan) {

			$sql = "select * from ".$tablo_adi." where ".$colum_adi." like '%".$aranan."%' collate Turkish_CS_AS";
			$res = @mssql_query($sql);
			$count = @mssql_num_rows($res);
			// var_dump($count);
			if ($count > 0) {

				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
					// var_dump($row);
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	private function convertim($data = NULL)
	{
		// var_dump($data);
		$keys = (array_keys($data));
		foreach($keys as $key) {
			if (!is_numeric($key)) {
				unset($data[$key]);
			} else {
				
				$data[$key] = utf8_encode($data[$key]);
			    // $data[$key] = iconv(@mb_convert_encoding($data[$key]),'UTF-8', $data[$key]);
			    // $data[$key] = mb_convert_encoding($data[$key], 'UTF-8', "auto");
				$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
				$data[$key] = str_replace("Ý", "İ", $data[$key]);
				$data[$key] = str_replace("ý", "ı", $data[$key]);
				$data[$key] = str_replace("þ", "ş", $data[$key]);
				$data[$key] = str_replace("Þ", "Ş", $data[$key]);
				$data[$key] = str_replace("ð", "ğ", $data[$key]);
				$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
			}
		}
		//var_dump($data);
		
		$keylerimiz = array('msg_S_0088', 'bayi_kodu', 'bayi_adi', 'msg_S_1034', 'Cari_F10da_detay', 'msg_S_1530', 'msg_S_3171', 'msg_S_0888', 'kredi', 'aciktaninankredi', 'garanti', 'halkbank', 'ziraat', 'yapikredi', 'finans', 'trfinans', 'ingbank', 'akbank', 'isbankasi', 'albaraka');
		// echo count($keylerimiz);
		$data       = array_combine($keylerimiz, array_values($data));

		return $data;
	}
	/*
		Bu fonksiyon ile tanımlanmış olan bütün kalan limitleri alabiliyorz.
	*/
	public function testt()
	{
		$sql = "select * from CARI_HESAPLAR_CHOOSE_2AX where msg_S_1032 = '120.34.100246'";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		// var_dump($count);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	// var_dump($row);
	        	$data['results'][] = $this->convertim($row);
	        	// $data['results'][] = ($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		// var_dump(json_last_error());
		return $data;
	}
	/*Tablolarda Ara*/

	public function kargoStok()
	{
		$data = array();

		$sql = "select * from STOKLAR where sto_kod like '%KARGO%'";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	/**/
	public function stokIskontolar($page = 1, $limit = 100)
	{
		if (!is_numeric($page))
			$page = 1;

		if (!is_numeric($limit))
			$limit = 100;
		
		$sql = "select sto_maxiskonto_orani, sto_komisyon_orani, sto_kod from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno order by sto_maxiskonto_orani DESC OFFSET ".(($page-1)*$limit)." ROWS FETCH NEXT ".$limit." ROWS ONLY ";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data['results']);
	}
	public function sonStoklar($lastid = NULL)
	{
		$sql = "select * from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where sto_RECno > ".$lastid." order by RecID_RECno ASC ";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data['results']);
	}

	public function lazimolan($stokkodu = NULL)
	{
		$sql = "select * from STOKLAR where sto_kod = '".$stokkodu."'";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data['results']);
	}
	/**/

	/*STOK GUNCELLE*/
	public function updateStoklar($page = 1, $limit = 100)
	{
		$sql = "select * from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno order by RecID_RECno ASC OFFSET ".(($page-1)*$limit)." ROWS FETCH NEXT ".$limit." ROWS ONLY ";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data['results']);
	}

	public function updateStok($stokkodu = NULL)
	{
		$data = array();
		if ($stokkodu) {

			$sql = "select * from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where sto_kod = '".$stokkodu."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function depoDurumlari($stokkodu = NULL)
	{
		$depolar = array();
		$sql_depolar = "select [Depo No] as depo_no, [Depo Adi] as depo_adi, Miktar as miktar, Fiyat as fiyat from dbo.MV4_stokdepodurumlari('".$stokkodu."')";
		$res_depolar = mssql_query($sql_depolar);
		$count_depo  = mssql_num_rows($res_depolar);
		if ($count_depo > 0) {
			while ($depo_row = mssql_fetch_array($res_depolar)) {
				$depolar[] = $this->convert($depo_row);
			}
		}
		return json_encode($depolar);
	}

	public function markaAdi($markakodu = NULL)
	{
		$marka = array();
		$sql = "select mrk_ismi, mrk_kod from STOK_MARKALARI where mrk_kod = '".$this->eklerken($markakodu)."'";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			while ($marka_row = mssql_fetch_array($res)) {
				$marka = $this->convert($marka_row);
			}
		}
		return json_encode($marka);
	}

	public function kategoriAdi($kategorikodu = NULL)
	{
		$kategori = array();
		$sql = "select dbo.fn_AnaGrupIsmi('".$kategorikodu."') as sto_anagrup_adi";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			while ($kategori_row = mssql_fetch_array($res)) {
				$kategori = $this->convert($kategori_row);
			}
		}
		return json_encode($kategori);
	}

	public function altKategoriAdi($kategorikodu = NULL, $altkategorikodu = NULL)
	{
		$kategori = array();
		$sql = "select dbo.fn_AltGrupIsmi('".$kategorikodu."', '".$altkategorikodu."') as sto_altgrup_adi";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			while ($kategori_row = mssql_fetch_array($res)) {
				$kategori = $this->convert($kategori_row);
			}
		}
		return json_encode($kategori);
	}
	/*STOK GUNCELLE*/

	public function sepetidAl($stokkodu = NULL, $cari = NULL, $fiyat = NULL, $miktar = NULL)
	{
		$data = array();
		if ($stokkodu && $cari && $fiyat && $miktar) {
			$sql = "select recno from MV4_ANDROIDSEPET where stokkodu = '".$stokkodu."' and carikod = '".$cari."' and miktar = '".$miktar."' and birimfiyat = '".$fiyat."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function sepetGuncelle($sepet_id = NULL, $miktar = 1)
	{
		$data = array();
		if ($sepet_id) {
			$sepetim = array();
			$sql = "select * from MV4_ANDROIDSEPET where recno = ".$sepet_id;
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$sepetim = $this->convert($row);
			    }
			    if ($sepetim) {
			    	$sepetim = (object)$sepetim;
			    	$iskonto = $sepetim->isk2y;
			    	$birimfiyat = $sepetim->birimfiyat;
			    	$toplam_iskonto = $miktar*(($birimfiyat*$iskonto)/100);
			    	$toplam_tutar   = $miktar*$birimfiyat;
			    	$sql_update = "update MV4_ANDROIDSEPET set miktar = '".$miktar."', isk2t = '".$toplam_iskonto."', tutar = '".$toplam_tutar."' where recno = ".$sepet_id;
			    	$res_update = mssql_query($sql_update);
			    	if ($res_update) {
			    		$data['sepet_id'] = $sepet_id;
			    		$data['miktar']   = $miktar;
			    		$data['tutar']    = $toplam_tutar;
			    		$data['iskonto']  = $toplam_iskonto;
			    		$data['status']   = TRUE;
			    	} else {
			    		$data['status'] = FALSE;
			    	}
			    } else {
			    	$data['status'] = FALSE;
			    }
			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function tabanlari($anakategori = NULL, $altkategori = NULL, $enaltkategori = NULL, $marka = NULL, $desen = NULL)
	{
		$data = $this->get_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen."tabanlari", "data");
		if (!$data) {
			$where = NULL;
			$data = array();
			$data['params'] = array('anakategori' => $anakategori, 'altkategori' => $altkategori, 'enaltkategori' => $enaltkategori, 'marka' => $marka, 'desen' => $desen);
			if ($anakategori || $altkategori || $enaltkategori || $marka || $desen) {

				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				if ($enaltkategori) {
					$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
				}

				if ($marka) {
					$where .= " sto_marka_kodu = '".$marka."' and";
				}

				if ($desen) {
					$where .= " lastikdeseni = '".$desen."' and";
				}

				$where = rtrim($where, " and");

				$sql   = "select distinct tabani from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where".$where;
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen."tabanlari", "data", $data);
		}
		return $data;
	}

	public function yanaklari($anakategori = NULL, $altkategori = NULL, $enaltkategori = NULL, $marka = NULL, $desen = NULL, $taban = NULL)
	{
		$data = $this->get_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen.$taban."yanaklari", "data");
		if (!$data) {
			$where = NULL;
			$data = array();
			$data['params'] = array('anakategori' => $anakategori, 'altkategori' => $altkategori, 'enaltkategori' => $enaltkategori, 'marka' => $marka, 'desen' => $desen, 'taban' => $taban);
			if ($anakategori || $altkategori || $enaltkategori || $marka || $desen || $taban) {

				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				if ($enaltkategori) {
					$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
				}

				if ($marka) {
					$where .= " sto_marka_kodu = '".$marka."' and";
				}

				if ($desen) {
					$where .= " lastikdeseni = '".$desen."' and";
				}

				if ($taban) {
					$where .= " tabani = '".$taban."' and";
				}

				$where = rtrim($where, " and");

				$sql   = "select distinct yanagi from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where".$where;
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen.$taban."yanaklari", "data", $data);
		}
		return $data;
	}

	public function jantlari($anakategori = NULL, $altkategori = NULL, $enaltkategori = NULL, $marka = NULL, $desen = NULL, $taban = NULL, $yanak = NULL)
	{
		$data = $this->get_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen.$taban.$yanak."jantlari", "data");
		if (!$data) {
			$where = NULL;
			$data = array();
			$data['params'] = array(
								'anakategori' => $anakategori, 
								'altkategori' => $altkategori, 
								'enaltkategori' => $enaltkategori, 
								'marka' => $marka, 
								'desen' => $desen,
								'taban' => $taban,
								'yanak' => $yanak
							);
			if ($anakategori || $altkategori || $enaltkategori || $marka || $desen || $taban || $yanak) {

				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				if ($enaltkategori) {
					$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
				}

				if ($marka) {
					$where .= " sto_marka_kodu = '".$marka."' and";
				}

				if ($desen) {
					$where .= " lastikdeseni = '".$desen."' and";
				}

				if ($taban) {
					$where .= " tabani = '".$taban."' and";
				}

				if ($yanak) {
					$where .= " yanagi = '".$yanak."' and";
				}

				$where = rtrim($where, " and");

				$sql   = "select distinct jantcapi from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where".$where;
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($anakategori.$altkategori.$enaltkategori.$marka.$desen.$taban.$yanak."jantlari", "data", $data);
		}
		return $data;
	}

	/*
	private function get_cache($name, $type, $md5 = TRUE)
	{
		$cache_time = $this->cache_time;	
		$cache_dir  = $this->path."cache_".$type."/";
		if (!is_dir($cache_dir))
			mkdir($cache_dir , 0755);

		$cache_file = $name;
		if ($md5 === TRUE)
			$cache_file = md5($cache_file);



		$cache_file = $cache_dir.$cache_file.".html";	
		$cache_file_created = (@file_exists($cache_file)) ? @filemtime($cache_file) : 0;
		
		if (time() - $cache_time < $cache_file_created) {
			$data = @file_get_contents($cache_file);
		}
		
		return @$data;		
	}
	
	private function put_cache($name, $type, $data, $md5 = TRUE)
	{
		$cache_dir  = $this->path."cache_".$type."/";
		if (!is_dir($cache_dir))
			mkdir($cache_dir , 0755);

		$cache_file = $name;
		if ($md5 === TRUE)
			$cache_file = md5($cache_file);

		$cache_file = $cache_dir.$cache_file.".html";

		if (strlen($data) > 0) {
			$fp = @fopen($cache_file, 'w'); 
			@fwrite($fp, $data);
			@fclose($fp); 
		}
		
		return TRUE;		
	}
	*/

	public function get_cache()
	{
		return NULL;
	}

	public function put_cache()
	{
		return FALSE;
	}

	private function convert($data = NULL)
	{
		// var_dump($data);
		$keys = (array_keys($data));
		foreach($keys as $key){
			$data[$key] = utf8_encode($data[$key]);
		    // $data[$key] = iconv(@mb_convert_encoding($data[$key]),'UTF-8', $data[$key]);
		    // $data[$key] = mb_convert_encoding($data[$key], 'UTF-8', "auto");
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		}
		return $data;
	}

	public function eklerken($data)
	{
		$data = str_replace("İ", "Ý", $data);
		$data = str_replace("ı", "ý", $data);
		$data = str_replace("ş", "þ", $data);
		$data = str_replace("Ş", "Þ", $data);
		$data = str_replace("ğ", "ð", $data);
		$data = str_replace("Ğ", "Ð", $data);
		$data = utf8_decode($data);

		return $data;
	}

	public function depolar()
	{
		$data = $this->get_cache("depolar", "data");
		if (!$data) {
			$data = array();
			$data['params'] = array();
			$sql = "select dep_adi as depoAdi, dep_RECno as depoId from DEPOLAR where dep_RECno in (2, 4, 17, 19) order by dep_RECno asc";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;
				
				// for ($i=1; $i<=$count; $i++) {
				//     $data['results'][] = $this->convert(mssql_fetch_array($res, $i));
				// }
				
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }

			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("depolar", "data", $data);
		}		
		
		return $data;	
	}

	public function stoklar($page = 1, $limit = NULL, $tumu = TRUE, $min = NULL)
	{

		if (is_null($limit))
			$limit = $this->limit;

		if (!is_numeric($page))
			$page = 1;

		$data  = array();
		$data['params'] = array('page' => $page, 'limit' => $limit, 'tumu' => $tumu, 'min' => $min);
		$where = NULL;
		
		if ($tumu) {
			$where = "where dbo.fn_EldekiMiktar(sto_kod) > 0";
		}

		if (is_numeric($min)) {
			$where = "where dbo.fn_EldekiMiktar(sto_kod) >= ".$min;
		}

		if (!is_null($where)) {
			$where .= " and dbo.fn_StokSatisFiyati ( sto_kod, 1, 0 ) > 0 and sto_isim like '%205/55%'";
		} else {
			$where = " where dbo.fn_StokSatisFiyati ( sto_kod, 1, 0 ) > 0";
		}

		$sql   = "SELECT sto_isim, sto_kod, tabani, yanagi, jantcapi, yuk, hiz, lastikdeseni, resim1, resim2, resim3, xlrf, rftzprofssrzps, kati, yakit, islak, gurultu, oem, dot, altkategoriadi2, Ebat, dbo.fn_EldekiMiktar ( sto_kod ) AS miktar, dbo.fn_StokSatisFiyati ( sto_kod, 1, 0 ) AS fiyat, dbo.fn_SektorIsmi ( sto_sektor_kodu ) AS sktr_ismi, dbo.fn_AmbalajIsmi ( sto_ambalaj_kodu ) AS amb_ismi, dbo.fn_KategoriIsmi ( sto_kategori_kodu ) AS ktg_isim, dbo.fn_ModelIsmi ( sto_model_kodu ) AS mdl_ismi, dbo.fn_ReyonIsmi ( sto_reyon_kodu ) AS ryn_ismi, dbo.fn_UreticiIsmi ( sto_uretici_kodu ) AS urt_ismi, sto_kalkon_kodu as mevsim, dbo.fn_AnaGrupIsmi ( sto_anagrup_kod ) AS kategori, dbo.fn_AltGrupIsmi ( sto_anagrup_kod, sto_altgrup_kod ) AS altkategori, dbo.fn_MarkaIsmi ( sto_marka_kodu ) AS mrk_ismi FROM STOKLAR WITH ( NOLOCK ) LEFT JOIN STOKLAR_USER ON RecID_RECno = sto_RECno ".$where." order by dbo.fn_EldekiMiktar(sto_kod) ASC OFFSET ".(($page-1)*$limit)." ROWS FETCH NEXT ".$limit." ROWS ONLY ";
		$res   = mssql_query($sql);
		$count = mssql_num_rows($res); 
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function kategoriler()
	{
		$data  = $this->get_cache("kategoriler", "data");
		if (!$data) {
			$data = array();
			$data['params'] = array();
			$sql   = "select distinct sto_altgrup_kod, sto_anagrup_kod, dbo.fn_AnaGrupIsmi(sto_anagrup_kod) as ana_grup, dbo.fn_AltGrupIsmi(sto_anagrup_kod, sto_altgrup_kod) as alt_grup, altkategoriadi2 as altr_grup_alt from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("kategoriler", "data", $data);
		}
		return $data;
	}

	public function kategorileri($anakategori = NULL)
	{
		$data  = $this->get_cache($anakategori."kategorileri", "data");
		if (!$data) {
			$data = array();
			$data['params'] = array('anakategori' => $anakategori);
			if ($anakategori) {
				$sql   = "select distinct sto_altgrup_kod, dbo.fn_AltGrupIsmi(sto_anagrup_kod, sto_altgrup_kod) as alt_grup from STOKLAR where sto_anagrup_kod = '".$anakategori."' order by alt_grup asc";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($anakategori."kategorileri", "data", $data);
		}		
		return $data;
	}

	public function altkategorileri($altkategori = NULL, $anakategori = NULL)
	{
		$data  = $this->get_cache($altkategori.$anakategori."altkategorileri", "data");
		if (!$data) {
			$where = NULL;
			$data = array();
			$data['params'] = array('altkategori' => $altkategori, 'anakategori' => $anakategori);
			if ($altkategori || $anakategori) {

				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				$where = rtrim($where, " and");

				$sql   = "select distinct altkategoriadi2 as alt_grup_alt from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where".$where." order by altkategoriadi2 asc";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($altkategori.$anakategori."altkategorileri", "data", $data);
		}		
		return $data;
	}

	public function ebatlari($jantcapi = NULL, $anakategori = NULL, $altkategori = NULL, $enaltkategori = NULl)
	{
		$data  = $this->get_cache($jantcapi.$anakategori.$altkategori.$enaltkategori."ebatlari", "data");
		if (!$data) {
			$where = NULL;
			$data = array();
			$data['params'] = array('altkategori' => $altkategori, 'anakategori' => $anakategori, 'jantcapi' => $jantcapi, 'enaltkategori' => $enaltkategori);
			if ($altkategori || $anakategori || $jantcapi || $enaltkategori) {

				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				if ($jantcapi) {
					$where .= " jantcapi = '".$jantcapi."' and";
				}

				if ($enaltkategori) {
					$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
				}

				$where = rtrim($where, " and");

				$sql   = "select distinct Ebat as ebati from STOKLAR inner join STOKLAR_USER on RecID_RECno = sto_RECno where".$where." order by Ebat asc";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($jantcapi.$anakategori.$altkategori.$enaltkategori."ebatlari", "data", $data);
		}		
		return $data;
	}

	public function desenleri($anakategori = NULL, $altkategori = NULL, $marka = NULL, $enaltkategori = NULL)
	{
		$data  = $this->get_cache($anakategori.$altkategori.$marka.$enaltkategori."desenleri", "data");
		if (!$data) {
			$where = NULL;
			$data  = array();
			$data['params'] = array('anakategori' => $anakategori, 'altkategori' => $altkategori, 'marka' => $marka, 'enaltkategori' => $enaltkategori);
			if ($anakategori || $altkategori || $marka) {
				if ($anakategori) {
					$where .= " sto_anagrup_kod = '".$anakategori."' and";
				}

				if ($altkategori) {
					$where .= " sto_altgrup_kod = '".$altkategori."' and";
				}

				if ($marka) {
					$where .= " sto_marka_kodu = '".$marka."' and";
				}

				if ($enaltkategori) {
					$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
				}

				$where = rtrim($where, " and");

				$sql = "SELECT DISTINCT lastikdeseni FROM STOKLAR INNER JOIN STOKLAR_USER ON RecID_RECno = sto_RECno where ".$where;
				$res = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache($anakategori.$altkategori.$marka.$enaltkategori."desenleri", "data", $data);
		}
		return $data;
	}

	public function odemeplanlari()
	{
		$data  = $this->get_cache("odemeplanlari", "data");
		if (!$data) {
			$data = array();
			$data['params'] = array();
			$sql   = "select odp_no, odp_adi, odp_aratop from ODEME_PLANLARI where odp_no in (4,16,17,31,55,57)";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("odemeplanlari", "data", $data);
		}
		return $data;		
	}

	public function stokAra($stokkodu = NULL, $taban = NULL, $yanak = NULL, $jantcapi = NULL, $yuk = NULL, $hiz = NULL, $xlrf = FALSE, $rftzprofssrzps = FALSE, $markalar = NULL, $kategori = NULL, $altkategori = NULL, $enaltkategori = NULL, $desen = NULL, $mevsim = NULL, $min = NULL, $page = 1, $limit = NULL)
	{

		if (!is_numeric($page))
			$page = 1;

		if (is_null($limit))
			$limit = $this->limit;
		$where = NULL;
		if (is_null($min))
			$min = 0;

		$data  = array();
		// $mevsim = $this->eklerken($mevsim);

		// $enaltkategori = utf8_encode($enaltkategori);

		$mevsim = rtrim($mevsim, "Ş");
		$data['params'] = array(
			'stokkodu' => @$stokkodu,
			'taban' => @$taban,
			'yanak' => @$yanak,
			'jantcapi' => @$jantcapi,
			'yuk' => @$yuk,
			'hiz' => @$hiz,
			'xlrf' => @$xlrf,
			'rftzprofssrzps' => @$rftzprofssrzps,
			'markalar' => @$markalar,
			'kategori' => @$kategori,
			'altkategori' => @$altkategori,
			'enaltkategori' => utf8_encode(@$enaltkategori),
			'desen' => @$desen,
			'mevsim' => @$mevsim,
			'min' => @$min,
			'page' => @$page,
			'limit' => @$limit
		);

		if (is_numeric($min)) {
			$where .= " dbo.fn_EldekiMiktar(sto_kod) >= ".$min;
		}

		if ($stokkodu) {
			$where .= " AND sto_kod = '".$stokkodu."'";
		}

		if ($taban) {
			$where .= " AND tabani = '".$taban."'";
		}

		if ($taban) {
			$where .= " AND yanagi = '".$yanak."'";
		}

		if ($jantcapi) {
			$where .= " AND jantcapi like '%".$jantcapi."%'";
		}

		if ($yuk) {
			$where .= " AND yuk = '".$yuk."'";
		}

		if ($hiz) {
			$where .= " AND hiz = '".$hiz."'";
		}

		if ($xlrf === TRUE) {
			$where .= " AND xlrf <> ''";
		}

		if ($kategori) {
			$where .= " AND sto_anagrup_kod = '".$kategori."'";
		}

		if ($altkategori) {
			$where .= " AND sto_altgrup_kod = '".$altkategori."'";
		}

		if ($desen) {
			$where .= " AND lastikdeseni = '".$desen."'";
		}

		if ($enaltkategori) {
			$where .= " AND altkategoriadi2 = '".$this->eklerken($enaltkategori)."'";
		}

		if ($mevsim) {
			$where .= " AND sto_kalkon_kodu like '%".$mevsim."%'";
		}

		if ($rftzprofssrzps === TRUE) {
			$where .= " AND rftzprofssrzps <> ''";
		}

		
		$markalar = @split(",", $markalar);
		if (count($markalar) > 0) {
			$markalar_query = NULL;
			foreach ($markalar as $key => $marka) {
				$marka = trim($marka);
				$markalar_query .= "mrk_ismi like '%".$marka."%' OR ";
			}
			$markalar_query = trim($markalar_query);
			$markalar_query = rtrim($markalar_query, "OR");
			$markalar_query = trim($markalar_query);
			$where .= " AND (".$markalar_query.")";
		}

		$where = ltrim($where, " AND");

		$where = "where ".$where;

		$sql   = "select sto_isim, sto_kod, dbo.fn_EldekiSonMiktar(sto_kod) as miktar, dbo.fn_StokSatisFiyati(sto_kod, 1, 0) as fiyat, tabani, yanagi, jantcapi, yuk, hiz, lastikdeseni, resim1, resim2, resim3, xlrf, rftzprofssrzps, mrk_ismi, sto_kalkon_kodu as mevsim, dbo.fn_AnaGrupIsmi(sto_anagrup_kod) as kategori, dbo.fn_AltGrupIsmi(sto_anagrup_kod, sto_altgrup_kod) as altkategori, altkategoriadi2 as enaltkategori from STOKLAR WITH (NOLOCK) LEFT JOIN STOKLAR_USER on RecID_RECno = sto_RECno LEFT JOIN STOK_MARKALARI on sto_marka_kodu = mrk_kod ".$where." order by dbo.fn_EldekiMiktar(sto_kod) ASC OFFSET ".(($page-1)*$limit)." ROWS FETCH NEXT ".$limit." ROWS ONLY ";
		// echo $sql;
		$res   = mssql_query($sql);
		$count = mssql_num_rows($res); 
		if ($count > 0) {
			$data['status'] = TRUE;
			while ($row = mssql_fetch_array($res)) {
				$stok_array = array();
				$stok_array = $this->convert($row);
				$sql_depolar = "select * from dbo.MV4_stokdepodurumlari('".$stok_array['sto_kod']."')";
				$res_depolar = mssql_query($sql_depolar);
				$count_depo  = mssql_num_rows($res_depolar);
				if ($count_depo > 0) {
					while ($depo_row = mssql_fetch_array($res_depolar)) {
						$stok_array['depolar'][] = $this->convert($depo_row);
					}
				}
	        	$data['results'][] = $stok_array;
		    }

		} else {
			$data['status'] = FALSE;
		}
		$data['error'] = json_last_error();
		$data = json_encode($data);
		return $data;
	}

	public function markalar()
	{
		$data  = $this->get_cache("markalar", "data");
		if (!$data) {
			$data  = array();
			$data['params'] = array();
			$sql = "select distinct mrk_ismi, mrk_special3, mrk_RECno, mrk_kod from STOKLAR with(nolock) inner join STOK_MARKALARI on mrk_kod = sto_marka_kodu where mrk_special3 <>'M' order by mrk_ismi asc";
			$res = mssql_query($sql);
			//var_dump($res);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("markalar", "data", $data);
		}
		
		return $data;
	}

	public function markalari($anakategori = NULL, $altkategori = NULL, $enaltkategori = NULL)
	{
		$data = $this->get_cache($anakategori.$altkategori.$enaltkategori."markalari", "data");
		// $data = NULL;
		if (!$data) {
			$where = NULL;
			$join  = " left JOIN STOKLAR_USER on RecID_RECno = sto_RECno LEFT JOIN STOK_MARKALARI on mrk_kod = sto_marka_kodu";
			
			if ($anakategori) {
				$where .= " sto_anagrup_kod = '".$anakategori."' and";
			}

			if ($altkategori) {
				$where .= " sto_altgrup_kod = '".$altkategori."' and";
			}

			if ($enaltkategori) {
				$where .= " altkategoriadi2 = '".$this->eklerken($enaltkategori)."' and";
			}
			// echo $this->eklerken($enaltkategori);
			$where = rtrim($where, " and");

			$data  			= array();
			$data['params'] = array('anakategori' => $anakategori, 'altkategori' => $altkategori, 'enaltkategori' => utf8_encode($enaltkategori));
			if ($anakategori) {
				$sql = "select DISTINCT mrk_ismi, sto_marka_kodu as mrk_kod from STOKLAR".$join." where".$where." order by sto_marka_kodu asc";
				// $sql2 = "select DISTINCT mrk_ismi, sto_marka_kodu from STOKLAR where".$where." order by sto_marka_kodu asc";
				// $data['sql'] = $sql;
				$res = mssql_query($sql);
				// var_dump($res);
				$count = mssql_num_rows($res);
				//var_dump($count);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			// var_dump($data);
			$data = json_encode($data);
			// var_dump(json_last_error());
			$this->put_cache($anakategori.$altkategori.$enaltkategori."markalari", "data", $data);
		}
			
		

		return $data;
	}

	public function srmler($chkodu = NULL)
	{
		$data  = $this->get_cache("srmler".$chkodu, "data");
		if (!$data) {
			$data  = array();
			$data['params'] = array('chkodu' => $chkodu);
			if ($chkodu) {

				$sql = "select DISTINCT cha_srmrkkodu as srmkodu, som_isim as srmisim from CARI_HESAP_HAREKETLERI INNER JOIN SORUMLULUK_MERKEZLERI on som_kod = cha_srmrkkodu where cha_kod = '".$chkodu."'";
				$res = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}

			} else {
				$data['status'] = FALSE;
			}

			$data = json_encode($data);
			$this->put_cache("srmler".$chkodu, "data", $data);
		}
		return $data;		
	}

	public function onaylanmislarimiz($cari_kod = NULL)
	{
		$data  = array();
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from dbo.fn_CariSiparisFoyu('".$cari_kod."', '19990101', '".date('Ymd')."') left join dbo.SIPARISLER on sip_RECno = [msg_S_0088]";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function onaylanmislar($cari_kod = NULL)
	{
		$data  = array();
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select msg_S_0243 as evrakno_seri, msg_S_0157 as evrakno_sira, sip_tarih, msg_S_0078 as stok_kodu, msg_S_0070 as stok_isim, sip_miktar, msg_S_0248 as birim_fiyat, msg_S_0249 as toplam_fiyat, sip_cari_sormerk as odemetipii, #msg_S_1042 as nasil from dbo.fn_CariSiparisFoyu('".$cari_kod."', '19990101', '".date('Ymd')."') left join dbo.SIPARISLER on sip_RECno = [msg_S_0088] where sip_tip = 0 and sip_kapat_fl= 0 and sip_musteri_kod = '".$cari_kod."' and sip_OnaylayanKulNo <> 0";
			// echo $sql;
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}



	public function onaylanacaklar($cari_kod = NULL)
	{
		$data  = array();
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select msg_S_0243 as evrakno_seri, msg_S_0157 as evrakno_sira, sip_tarih, msg_S_0078 as stok_kodu, msg_S_0070 as stok_isim, sip_miktar, msg_S_0248 as birim_fiyat, msg_S_0249 as toplam_fiyat, sip_cari_sormerk as odemetipii, #msg_S_1042 as nasil from dbo.fn_CariSiparisFoyu('".$cari_kod."', '19990101', '".date('Ymd')."') left join dbo.SIPARISLER on sip_RECno = [msg_S_0088] where sip_tip = 0 and sip_kapat_fl= 0 and sip_musteri_kod = '".$cari_kod."' and sip_OnaylayanKulNo = 0";
			// echo $sql;
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;	
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function siparishareketleri($cari_kod = NULL, $ilk = NULL, $son = NULL)
	{
		$data  = array();
		if (is_null($ilk)) {
			$ilk = date("Y.m.01", time());
		}

		if (is_null($son)) {
			$son = date("Y.m.d", time());
		}

		$data['params'] = array('cari_kod' => $cari_kod, 'ilk' => $ilk, 'son' => $son);

		if ($cari_kod && $ilk && $son) {
			$sql = "select msg_S_0243 as evrakno_seri, msg_S_0157 as evrakno_sira, sip_tarih, msg_S_0078 as stok_kodu, msg_S_0070 as stok_isim, sip_miktar, msg_S_0248 as birim_fiyat, msg_S_0249 as toplam_fiyat, sip_cari_sormerk as odemetipii, #msg_S_1042 as nasil from dbo.fn_CariSiparisFoyu('".$cari_kod."', '".$ilk."', '".$son."') left join SIPARISLER on sip_RECno=msg_S_0088 where sip_cagrilabilir_fl = 1 and msg_S_0256 <> '' and msg_S_0078 <> '610.01.099'";
			// echo $sql;
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE; 
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function irsaliyelendirilmisler($cari_kod = NULL, $ilk = NULL, $son = NULL)
	{
		$data  = array();
		if (is_null($ilk)) {
			$ilk = date("Y.m.01", time());
		}

		if (is_null($son)) {
			$son = date("Y.m.d", time());
		}

		$data['params'] = array('cari_kod' => $cari_kod, 'ilk' => $ilk, 'son' => $son);

		if ($cari_kod && $ilk && $son) {
			$sql   = "select sth_tarih, sth_stok_kod, sth_evrakno_seri, sth_evrakno_sira, sth_miktar, sth_tutar, (sth_tutar / sth_miktar) as sth_birim, dbo.fn_StokIsmi(sth_stok_kod) as sth_isim from STOK_HAREKETLERI where sth_cari_kodu = '".$cari_kod."' and sth_tarih BETWEEN '".$ilk."' AND '".$son."'";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE; 
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function devreden($cari = NULL, $tarih = NULL)
	{
		$data = array();		
		$data['params'] = array('cari' => $cari, 'tarih' => $tarih);

		if ($cari && $tarih) {
			
			$tarih2 = $tarih;			
			$tarih 	= date('Ymd', strtotime($tarih2));
			
			$sql = "select [#msg_S_0111\T] as devreden, '".$tarih2."' as tarih from dbo.fn_CariFoy('0', 0, '".$cari."', NULL, '', '".$tarih."', '', 0, '') where msg_S_0088 = 0";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		
		return json_encode($data);
	}

	public function extre($cari = NULL, $ilk = NULL, $son = NULL, $srm = NULL)
	{
		$srm = rtrim($srm, "İ");
		$data = array();		
		$data['params'] = array('cari' => $cari, 'ilk' => $ilk, 'son' => $son, 'srm' => $srm);

		if ($cari && $ilk && $son) {
			$where = NULL;
			$ilk = date('Ymd', strtotime($ilk));
			$son = date('Ymd', strtotime($son));
			if ($srm) {
				$where = " where msg_S_0118 like '%".$srm."%'";
			}

			$sql = "select #msg_S_0092 as evrak_tarih, 
					msg_S_0090 as evrak_seri, 
					msg_S_0091 as evrak_sira, 
					msg_S_0094 as evrak_tipi, 
					#msg_S_0085 as evrak_aciklama, 
					msg_S_0098 as vade_tarihi, 
					[msg_S_0102\T] as alacak, 
					[msg_S_0101\T] as borc,
					msg_S_0118 as srm,
					[#msg_S_0103\T] as abdurum
					from dbo.fn_CariFoy('0', 0, '".$cari."', NULL, '', '".$ilk."', '".$son."', 0, '')".$where." order by evrak_tarih ASC";

			// $data['sql'] = $sql;
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data);
	}

	public function cariAdresler($cari_kod = NULL, $adres_no = NULL)
	{
		$data = $this->get_cache("cariadresler".$cari_kod, "data");
		if (!$data) {
			$data = array();

			$data['params'] = array('cari_kod' => $cari_kod);

			if ($cari_kod) {
				if ($adres_no) {

					$sql = "select adr_adres_no, adr_cadde, adr_mahalle, adr_sokak, adr_Semt, adr_Apt_No, adr_Daire_No, adr_posta_kodu, adr_il, adr_ilce, adr_tel_ulke_kodu, adr_tel_bolge_kodu, adr_tel_no1, adr_tel_no2, adr_tel_faxno, adr_ozel_not, cari_sevk_adres_no, cari_fatura_adres_no, IIF (cari_sevk_adres_no = adr_adres_no, 1, 0) as sevkadresi, IIF (cari_fatura_adres_no = adr_adres_no, 1, 0) as faturaadresi from CARI_HESAP_ADRESLERI LEFT JOIN CARI_HESAPLAR on cari_kod = '".$cari_kod."' where adr_cari_kod = '".$cari_kod."' and adr_adres_no = '".$adres_no."'";

				} else {

					$sql = "select adr_adres_no, adr_cadde, adr_mahalle, adr_sokak, adr_Semt, adr_Apt_No, adr_Daire_No, adr_posta_kodu, adr_il, adr_ilce, adr_tel_ulke_kodu, adr_tel_bolge_kodu, adr_tel_no1, adr_tel_no2, adr_tel_faxno, adr_ozel_not, cari_sevk_adres_no, cari_fatura_adres_no, IIF (cari_sevk_adres_no = adr_adres_no, 1, 0) as sevkadresi, IIF (cari_fatura_adres_no = adr_adres_no, 1, 0) as faturaadresi from CARI_HESAP_ADRESLERI LEFT JOIN CARI_HESAPLAR on cari_kod = '".$cari_kod."' where adr_cari_kod = '".$cari_kod."'";
				}
				$res = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("cariadresler".$cari_kod, "data", $data);
		}
		return $data;		
	}

	public function cariTelefonlar($cari_kod = NULL)
	{
		$data = $this->get_cache("caritelefonlar".$cari_kod, "data");
		if (!$data) {
			$data = array();		
			$data['params'] = array('cari_kod' => $cari_kod);

			if ($cari_kod) {
				$sql = "select DISTINCT adr_cari_kod, adr_tel_ulke_kodu, adr_tel_bolge_kodu, adr_tel_no1, adr_tel_no2, adr_tel_faxno, adr_tel_modem from CARI_HESAP_ADRESLERI where adr_cari_kod = '".$cari_kod."'";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("caritelefonlar".$cari_kod, "data", $data);
		}
		return $data;		
	}

	public function cari($cari_kod = NULL)
	{
		$data = $this->get_cache("cari".$cari_kod, "data");
		if (!$data) {
			$data = array();		
			$data['params'] = array('cari_kod' => $cari_kod);

			if ($cari_kod) {
				$sql   = "select cari_kod, cari_unvan1, cari_unvan2, cari_EMail, cari_wwwadresi, cari_CepTel, cari_bolge_kodu, cari_sektor_kodu, cari_grup_kodu, cari_vdaire_adi, cari_vdaire_no from CARI_HESAPLAR where cari_kod = '".$cari_kod."'";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("cari".$cari_kod, "data", $data);
		}
		return $data;
	}

	public function cariHesapBilgileri($cari_kod = NULL)
	{
		$data = $this->get_cache("carihesapbilgileri".$cari_kod, "data");
		if (!$data) {
			$data = array();		
			$data['params'] = array('cari_kod' => $cari_kod);

			if ($cari_kod) {
				$sql   = "select * from CARI_HESAPLAR where cari_kod = '".$cari_kod."'";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("carihesapbilgileri".$cari_kod, "data", $data);
		}
		return $data;		
	}


	public function yapilacakTahsilatlar($cari_kod = NULL, $srm = NULL)
	{
		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod, 'srm' => $srm);

		if ($cari_kod && $srm) {
			$srm = str_replace("İ", "i", $srm);
			$sql = "select dbo.fn_CariHesapAnaDovizBakiye('0',0, '".$cari_kod."', '".$srm."','',0,null,null,0) as ooo,cari_kod,cari_unvan1,dbo.fn_CariHarEvrTipUzun(evraktip) as [EvrakTip] ,dbo.fn_EvrNoForm(evrakseri,evraksira) as [EvrakNo] ,faturatarihi,vadetarihi,meblag,kalan,datediff(day,faturatarihi,vadetarihi) vadegun,datediff(day,getdate(),vadetarihi) kacgunkaldi,cast(0 as float) bakiye,datediff(day,'20100101',vadetarihi) * meblag as ortalamaVadeToplami from CARI_HESAPLAR with(nolock) outer apply dbo.mv4_bakiyefaturalari(cari_kod,dbo.fn_CariHesapAnaDovizBakiye('0',0, '".$cari_kod."', '".$srm."','',0,null,null,0), '".$srm."') where cari_kod='".$cari_kod."' and kalan > 0 order by vadetarihi asc";
			// echo $sql;
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			// var_dump(mssql_fetch_array($res));
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
			        $data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}

		return json_encode($data);
	}

	public function arizalilastikler($cari_kod = NULL)
	{
		$data = $this->get_cache("arizalilastikler".$cari_kod, "data");
		if (!$data) {
			$data = array();		
			$data['params'] = array('cari_kod' => $cari_kod);

			if ($cari_kod) {
				$sql   = "select * from SIPARISLER_USER LEFT JOIN SIPARISLER on sip_RECno=RecID_RECno where Cari_Kodu = '".$cari_kod."'";
				$res   = mssql_query($sql);
				$count = mssql_num_rows($res);
				if ($count > 0) {
					$data['status'] = TRUE;
					while ($row = mssql_fetch_array($res)) {
			        	$data['results'][] = $this->convert($row);
				    }
				} else {
					$data['status'] = FALSE;
				}
			} else {
				$data['status'] = FALSE;
			}
			$data = json_encode($data);
			$this->put_cache("arizalilastikler".$cari_kod, "data", $data);
		}
		return $data;
	}

	/*Burdan Sonrası Sipariş ve Arızalı Lastik Formları İçin.*/

	private function getUserId($cari_kod = NULL)
	{
		if ($cari_kod) {
			$sql = "select recno as user_id from MV4.dbo.USERS with(nolock) where kod='".$cari_kod."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				for($i=1; $i<= $count; $i++) {
					$data['results'][] = $this->convert(mssql_fetch_array($res, $i));
				}
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = $data['results'][0]['user_id'];
		if ($data) {
			return $data;
		} else {
			return FALSE;
		}
	}

	public function kredilimitkontrolu($cari_kod = NULL)
	{
		if ($cari_kod) {
			$user_id = $this->getUserId($cari_kod);
			// var_dump($user_id);
			$sql = "select intvalue from MV4_PARAMETRELER with(nolock) where paramclass=15 and paramno=14 and userId=".$user_id;
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				for($i=1; $i<= $count; $i++) {
					$data['results'][] = $this->convert(mssql_fetch_array($res, $i));
				}
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function getSiparisUserRECno()
	{
		$sql = "select isnull(max(RecID_RECno),0)+1 as numara from SIPARISLER_USER with(nolock)";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function arizalandi()
	{
		$sql = "insert into SIPARISLER (sip_RECid_DBCno, sip_RECid_RECno, sip_evrakno_seri, sip_evrakno_sira, sip_tip, sip_teslim_tarih, sip_tarih, sip_stok_kod, sip_musteri_kod, sip_cins, sip_satirno, sip_harekettipi, sip_depono, sip_cari_sormerk, sip_stok_sormerk, sip_projekodu, sip_firmano, sip_subeno, sip_miktar, sip_aciklama) values (0, (SELECT IDENT_CURRENT ('SIPARISLER') AS Current_Identity), '".$this->getEvrakno_Seri()."', ".$this->getEvrakno_Sira($this->getEvrakno_Seri()).", 1, GETDATE(), GETDATE(), '610.01.099', '120.81.00022', 0, 1, 2, 2, 'S02', 'S02', 'WEB', 0, 0, 1, 'EBAT - MARKA')";
		$res = mssql_query($sql);
		$arizalanan = $this->arizalanan();
		$this->arizalandidi($arizalanan);
	}

	private function arizalanan()
	{
		$sql = "SELECT @@IDENTITY AS 'Identity'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		return $res->Identity;
	}

	public function ConvertData($data = NULL)
	{
		$data = str_replace("Ş", "S", $data);
		$data = str_replace("Ğ", "G", $data);
		$data = str_replace("Ö", "O", $data);
		$data = str_replace("Ç", "C", $data);
		$data = str_replace("Ü", "U", $data);
		$data = str_replace("İ", "I", $data);


		$data = str_replace("ş", "s", $data);
		$data = str_replace("ğ", "g", $data);
		$data = str_replace("ö", "o", $data);
		$data = str_replace("ç", "c", $data);
		$data = str_replace("ü", "u", $data);
		$data = str_replace("ı", "i", $data);

		return $data;
	}

	public function arizalandidi($RecID_RECno = NULL)
	{
		$RecID_DBCno = 0;
		$Fatura_Tarihi = date('Y-m-d', time());
		$Fatura_No = "123765";
		$Mail = "osmangoveli@gmail.com";
		$Tel_No = "02163989898";
		$Musteri_Adi = "ÖÇŞİĞÜüğişçö";
		//$Musteri_Adi = mb_convert_encoding($Musteri_Adi, "UTF-8", "UTF-8");
		$Musteri_Adi = $this->ConvertData($Musteri_Adi);
		$Musteri_Adresi = "İstanbul merkez";
		$Musteri_Adresi = $this->ConvertData($Musteri_Adresi);
		$Dis_Derinligi = "11";
		$Pozisyon = "Sağ ön";
		$Pozisyon = $this->ConvertData($Pozisyon);
		$Hava_Basinci = "32";
		$Marka_Model = "Bmw 3 serisi";
		$Aciklama = "hava kaçırıyor.";
		$Aciklama = $this->ConvertData($Aciklama);
		$Marka = "Michelin";
		$Ebat = "225/45R17";
		$Seri_No = "as1276";
		$Plaka = "06FD600";
		$Cari_Kodu = "120.81.00022";

		$sql2 = "insert into SIPARISLER_USER (RecID_RECno, RecID_DBCno,Fatura_Tarihi,Fatura_No,Mail,Tel_No,Musteri_Adi,Musteri_Adresi,Dis_Derinligi,Pozisyon,Hava_Basinci,Marka_Model,Aciklama,Marka,Ebat,Seri_No,Plaka, Cari_Kodu) values ($RecID_RECno, $RecID_DBCno,'$Fatura_Tarihi',$Fatura_No,'$Mail','$Tel_No', N'$Musteri_Adi',N'$Musteri_Adresi','$Dis_Derinligi',N'$Pozisyon','$Hava_Basinci','$Marka_Model',N'$Aciklama','$Marka','$Ebat','$Seri_No','$Plaka','$Cari_Kodu')"; // COLLATE Turkish_CS_AS
		$res2 = mssql_query($sql2);
		var_dump($res2);
	}

	public function arizabildir()
	{

		/*Önce Siparişler Sonra Siparişler User*/
		$carikod = "120.81.00022";
		$stokkodu = "610.01.099";
		$stokAdi = $this->ConvertData("Arızalı Lastik");
		$miktar = 1;
		$birimfiyat = 0;
		$isk1t = 0;
		$isk2t = 0;
		$isk3t = 0;
		$isk4t = 0;
		$isk5t = 0;
		$isk6t = 0;
		$isk1y = 0;
		$isk2y = 0;
		$isk3y = 0;
		$isk4y = 0;
		$isk5y = 0;
		$isk6y = 0;
		$tutar = 0;
		$doviz = 0;
		$depokodu = 0;
		$user = "120.81.00022";
		$birimpntr = 1;
		$adresno = 1;
		$evraksira = 0;
		$evrakseri = '';
		$evracik1 = $this->ConvertData("Evrak Açıklama Test");
		$fiyatno = 0;
		$il = "Ankara";
		$aciklama = '';
		$nakliyebrmfiyat = 0;
		$iscilikbrmfiyat = 0;
		$kdv = 0;
		$opno = 55;
		//$teslimtarihi = '1899-12-30 00:00:00.000';

		$sql  = "insert into MV4_ANDROIDSEPET"; 
		$sql .= " (carikod, stokkodu, stokadi, iscilikbrmfiyat, opno, teslimtarihi, kdv, miktar, birimfiyat, isk1t, isk2t, isk3t, isk4t, isk5t, isk6t, isk1y, isk2y, isk3y, isk4y, isk5y, isk6y, tutar, createdate, userkod, birimpntr, adresno, aciklama, doviz, depokodu, nakliyebrmfiyat, evraksira, evrakseri, evracik1, fiyatno, il)";
		$sql .= " values ('$carikod','$stokkodu','$stokAdi', $iscilikbrmfiyat, $opno, getDate(), $kdv,$miktar,$birimfiyat,$isk1t,$isk2t,$isk3t,$isk4t,$isk5t,$isk6t,$isk1y,$isk2y, $isk3y, $isk4y, $isk5y, $isk6y, $tutar, getdate(), '$user', $birimpntr, $adresno, '$aciklama', $doviz, $depokodu, $nakliyebrmfiyat, $evraksira, '$evrakseri','$evracik1',$fiyatno,'$il')";
		
		$res = mssql_query($sql);


		$RecID_RECno = $this->getSiparisUserRECno();
		$RecID_DBCno = 0;
		$Fatura_Tarihi = '2015-11-10';
		$Fatura_No = "123765";
		$Mail = "osmangoveli@gmail.com";
		$Tel_No = "02163989898";
		$Musteri_Adi = "Osman Göveli";
		$Musteri_Adresi = "Istanbul merkez";
		$Dis_Derinligi = "11";
		$Pozisyon = "Sağ ön";
		$Hava_Basinci = "32";
		$Marka_Model = "Bmw 3 serisi";
		$Aciklama = $this->ConvertData("hava kaçırıyor.");
		$Marka = "Michelin";
		$Ebat = "225/45R17";
		$Seri_No = "as1276";
		$Plaka = "06FD600";
		$Cari_Kodu = "120.81.00022";


		$sql2 = "insert into SIPARISLER_USER (RecID_RECno, RecID_DBCno,Fatura_Tarihi,Fatura_No,Mail,Tel_No,Musteri_Adi,Musteri_Adresi,Dis_Derinligi,Pozisyon,Hava_Basinci,Marka_Model,Aciklama,Marka,Ebat,Seri_No,Plaka, Cari_Kodu)values
		($RecID_RECno, $RecID_DBCno,$Fatura_Tarihi,$Fatura_No,'$Mail','$Tel_No','$Musteri_Adi','$Musteri_Adresi','$Dis_Derinligi','$Pozisyon','$Hava_Basinci','$Marka_Model','$Aciklama','$Marka','$Ebat','$Seri_No','$Plaka','$Cari_Kodu')";
		if ($res) {
			$res1 = mssql_query($sql2);
		} else {
			$res1 = FALSE;
		}

		/*
		$sip_tip = 1;
		$sip_fileid = 21;
		$sip_evrakno_seri = "select stringvalue from MV4_PARAMETRELER with(nolock) where paramclass=15 and paramno=1 and userId=(select recno from MV4.dbo.USERS with(nolock) where kod=@kod)"; 
		if ($sip_evrakno_seri)
			$sip_evrakno_seri = "AND";
		$sip_evrakno_sira = "select isnull(max(sip_evrakno_sira),0)+1 from SIPARISLER with(nolock) where sip_evrakno_seri=@seri and sip_tip=1";
		$sip_lastup_user  = "select mikroUserId from MV4.dbo.USERS with(nolock) where kod='120.81.00022'";
		$sip_adresno  = 1;
		$sip_belge_tarih = "2015-11-03 15:11:03.900";
		$sip_tarih = "2015-11-03 15:11:03.900";
		*/
		var_dump($res1);
	}

	public function parametresonucString($cari_kodu, $class, $no)
	{
		$sql = "select stringvalue from MV4_PARAMETRELER with(nolock) where paramclass=".$class." and paramno=".$no." and userId=(select recno from MV4.dbo.USERS with(nolock) where kod='".$cari_kodu."')";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->stringvalue;
		} else {
			return FALSE;
		}
	}

	public function parametresonucInt($cari_kodu, $class, $no)
	{
		$sql = "select intvalue from MV4_PARAMETRELER with(nolock) where paramclass=".$class." and paramno=".$no." and userId=(select recno from MV4.dbo.USERS with(nolock) where kod='".$cari_kodu."')";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->intvalue;
		} else {
			return FALSE;
		}
	}

	/*Sipariş Sepet*/
	public function updateSepetKargo($recno = NULL, $fiyat = NULL)
	{

		if (is_null($fiyat) || empty($fiyat))
			$fiyat = "NULL";

		if (!$recno) {
			return FALSE;
		} else {
			// $fiyat = 0;
			$sql = "update MV4_ANDROIDSEPET set nakliyebrmfiyat = ".$fiyat." where recno = ".$recno;
			$res = mssql_query($sql);

			if ($res) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
		
	}


	public function sepeteAt($carikod = NULL, $stokkodu = NULL, $miktar = NULL)
	{
		$data = array();		
		$data['params'] = array('carikod' => $carikod, 'stokkodu' => $stokkodu, 'miktar' => $miktar);

		if ($carikod && $stokkodu && $miktar) {
			$depolar    = array();
			$carikod    = $carikod;
			$stokkodu   = $stokkodu;
			// $stokadi    = "225/45R17 91H Primacy Alpin PA3 ZP*"; // Sql'den direk alıyoruz.
			$userkod    = $carikod;
			$miktar     = $miktar;
			$birimpntr  = 1;
			$createdate = "getDate()";
			$adresno    = 1;
			$opno       = 4;


			/*Depo Durumunu Alıyoruz*/
			$sql_depolar = "select * from dbo.MV4_stokdepodurumlari('".$stokkodu."')";
			$res_depolar = mssql_query($sql_depolar);
			$count_depo  = mssql_num_rows($res_depolar);
			if ($count_depo > 0) {
				while ($depo_row = mssql_fetch_array($res_depolar)) {
					$depolar[] = $this->convert($depo_row);
				}
			}
			/*Depo Durumunu Alıyoruz*/

			if (count($depolar) > 0) {
				$depo = $depolar[0];
				$birimfiyat = $depo['Fiyat'];
				$tutar      = (($miktar)*($birimfiyat));
				$il 		= $depo['Depo Adi'];
				$il 		= $this->eklerken($il);

				$sql = "insert into MV4_ANDROIDSEPET (carikod, stokkodu, stokadi, userkod, miktar, birimpntr, birimfiyat, tutar, createdate, adresno, opno, il) values ('".$carikod."', '".$stokkodu."', (select sto_isim from STOKLAR where sto_kod = '".$stokkodu."'), '".$userkod."', ".$miktar.", ".$birimpntr.", '".$birimfiyat."', '".$tutar."', ".$createdate.", ".$adresno.", ".$opno.", (SELECT dep_Il from DEPOLAR where dep_RECno = ".$depo['Depo No']."))";
				$res = mssql_query($sql);

				if ($res) {
					$data['sepet_id'] = $this->sepetlenen();
					$data['status']   = TRUE;
				} else {
					$data['status'] = FALSE;
				}

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	private function sepetlenen()
	{
		$sql = "SELECT @@IDENTITY AS 'Identity'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		return $res->Identity;
	}

	public function sepetOp($carikod = NULL)
	{
		$data = array();		
		$data['params'] = array('carikod' => $carikod);
		if ($carikod) {
			$sql = "select DISTINCT opno from MV4_ANDROIDSEPET where carikod = '".$carikod."'";
			$res = mssql_query($sql);
			$res = mssql_fetch_object($res);
			if ($res) {
				$data['status'] = TRUE;
				$data['opno']   = $res->opno;
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function sepettekilerOp($carikod = NULL , $opno = NULL)
	{
		$data = array();		
		$data['params'] = array('carikod' => $carikod, 'opno' => $opno);
		if ($carikod && $opno) {
			$sql = "select * from MV4_ANDROIDSEPET where carikod = '".$carikod."' and opno <> '".$opno."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = FALSE;
				$data['error']  = "Farklı ödeme tipinde kayıtlar var.";
				/*
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			    */
			} else {
				$data['status'] = TRUE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}

	public function sepettekiler($carikod = NULL)
	{
		$data = array();		
		$data['params'] = array('carikod' => $carikod);
		if ($carikod) {
			$sql = "select * from MV4_ANDROIDSEPET where carikod = '".$carikod."'";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);
	}
	/*Sipariş Sepet*/

	public function arizaSepeteAt()
	{
		$carikod = "120.81.00022";
		$stokkodu = "610.01.099";
		$stokAdi = "Arızalı Lastik";
		$miktar = 1;
		$birimfiyat = 0;
		$isk1t = 0;
		$isk2t = 0;
		$isk3t = 0;
		$isk4t = 0;
		$isk5t = 0;
		$isk6t = 0;
		$isk1y = 0;
		$isk2y = 0;
		$isk3y = 0;
		$isk4y = 0;
		$isk5y = 0;
		$isk6y = 0;
		$tutar = 0;
		$user = "120.81.00022";
		$birimpntr = 1;
		$adresno = 1;
		$evraksira = 0;
		$evrakseri = '';
		$evracik1 = "Evrak Açıklama Test";
		$fiyatno = 0;
		$il = "Ankara";

		$sql = "insert into MV4_ANDROIDSEPET (carikod,stokkodu,stokadi,miktar,birimfiyat,isk1t,isk2t,isk3t,isk4t,isk5t,isk6t,isk1y,isk2y,isk3y,isk4y,isk5y,isk6y,tutar,createdate,userkod,birimpntr,adresno,evraksira,evrakseri,evracik1,fiyatno,il) values ('$carikod','$stokkodu','$stokAdi',$miktar,$birimfiyat,$isk1t,$isk2t,$isk3t,$isk4t,$isk5t,$isk6t,$isk1y,$isk2y,$isk3y,$isk4y,$isk5y,$isk6y,$tutar,getdate(),'$user',$birimpntr,$adresno, $evraksira, '$evrakseri','$evracik1',$fiyatno,'$il')";
		echo $sql;
	}

	public function getEvrakno_Seri()
	{
		return $this->parametresonucString('B2B', 15, 1);
	}

	public function getEvrakno_Sira($seri = NULL)
	{
		$sql = "select isnull(max(sip_evrakno_sira),0)+1 as numara from SIPARISLER with(nolock) where sip_evrakno_seri='".$seri."' and sip_harekettipi = 2";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function getSiparsiRECno()
	{
		$sql = "select isnull(max(sip_RECno),0)+1 as numara from SIPARISLER with(nolock)";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function getMikroUserId()
	{
		$sql = "select mikroUserId from MV4.dbo.USERS with(nolock) where kod='b2b'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->mikroUserId;
		} else {
			return FALSE;
		}
	}

	public function getSaticiKodu()
	{
		$sql = "select mikroPerKod from MV4.dbo.USERS with(nolock) where kod='B2B'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->mikroPerKod;
		} else {
			return FALSE;
		}
	}

	public function arizalisiparis()
	{
		$sip_evrakno_seri = $this->getEvrakno_Seri();
		$sip_evrakno_sira = $this->getEvrakno_Sira($sip_evrakno_seri);
		$sip_create_date = date('Y-m-d', time());
		$sip_lastup_date = $sip_create_date;
		$sip_create_user = $this->getMikroUserId();
		$sip_lastup_user = $sip_create_user;
		$sip_tip = 1;
		$sip_fileid = 21;
		$sip_adresno = 1;
		$sip_belge_tarih = $sip_create_date;
		$sip_tarih = $sip_belge_tarih;
		$sip_belgeno = NULL;
		$sip_cagrilabilir_fl = TRUE;
		$sip_OnaylayanKulNo  = $sip_create_user;
		$sip_depono = 2;
		$sip_firmano = 0;
		$sip_subeno = 0;
		$sip_aciklama = "Android";
		$sip_musteri_kod = "120.81.00022";
		$sip_satici_kod = $this->getSaticiKodu(); // select mikroPerKod from MV4.dbo.USERS with(nolock) where kod='B2B'
		$sip_teslim_tarih = $sip_create_date;
		$sip_stok_sormerk = $this->parametresonucString('B2B', 15, 25);
		$sip_cari_sormerk = $sip_stok_sormerk;
		$sip_isk1 = $sip_isk2 = $sip_isk3 = $sip_isk4 = $sip_isk5 = $sip_isk6 = TRUE;
		$sip_iskonto1 = $sip_iskonto2 = $sip_iskonto3 = $sip_iskonto4 = $sip_iskonto5 = $sip_iskonto6 = 0;

		// for içinde yapılan alan

		$sip_opno = 0;
		$sip_harekettipi = 2;
		$sip_satirno = 1;
		$sip_stok_kod = "610.01.099";
		$sip_birim_pntr = 1;
		$sip_miktar = 1;
		$sip_b_fiyat = 0;
		$sip_tutar = 0;
		$sip_vergi_pntr = 0;
		$sip_vergi = 0;
		//$this->siparislerUser();
		$sip_RECid_DBCno = 0;
		$sip_RECid_RECno = $this->getSiparsiRECno();
		$sip_SpecRECno = 0;



		$sip_iptal = $sip_hidden = $sip_kilitli = $sip_degisti = $sip_checksum = $sip_cins = $sip_teslim_miktar = 0;
		$sip_iskonto_1 = $sip_iskonto_2 = $sip_iskonto_3 = $sip_iskonto_4 = $sip_iskonto_5 = $sip_iskonto_6 = 0;
		$sip_masraf_1 = $sip_masraf_2 = $sip_masraf_3 = $sip_masraf_4 = 0;
		$sip_masvergi_pntr = $sip_masvergi = 0;

		$sip_special1 = $sip_special2 = $sip_special3 = $sip_aciklama2 = $sip_teslim_turu = '';
		$sip_vergisiz_fl = $sip_kapat_fl = $sip_promosyon_fl = $sip_cari_grupno = $sip_doviz_cinsi = 0;

		$sip_doviz_kuru = $sip_alt_doviz_kuru = $sip_masraf1 = $sip_masraf2 = $sip_masraf3 = $sip_masraf4 = 1;
		$sip_mas1 = $sip_mas2 = $sip_mas3 = $sip_mas4 = 0;


		$sip_kar_orani = $sip_durumu = $sip_stalRecId_DBCno = $sip_stalRecId_RECno = $sip_planlananmiktar = $sip_teklifRecId_DBCno = $sip_teklifRecId_RECno = 0;
		$sip_parti_kodu = $sip_lot_no = 0;
		$sip_Exp_Imp_Kodu = $sip_paket_kod = '';
		$sip_prosiprecDbId = $sip_prosiprecrecI = $sip_fiyat_liste_no = 0;


		$sip_Otv_Pntr = $sip_Otv_Vergi = $sip_otvtutari = $sip_OtvVergisiz_Fl = 0;
		$sip_RezRecId_DBCno = $sip_RezRecId_RECno = $sip_yetkili_recid_dbcno = $sip_yetkili_recid_recno = 0;
		$sip_kapatmanedenkod = '';
		$sip_gecerlilik_tarihi = "1899-12-30 00:00:00.000";
		$sip_projekodu = $this->parametresonucString('B2B', 15, 27);
		$sip_onodeme_evrak_tip = $sip_onodeme_evrak_sira = $sip_rezervasyon_miktari	= $sip_rezerveden_teslim_edilen = 0;

		$sql = "insert into SIPARISLER(sip_RECid_DBCno, sip_RECid_RECno, sip_SpecRECno, sip_evrakno_seri, sip_evrakno_sira, sip_create_date, sip_lastup_date, sip_create_user, sip_lastup_user, sip_tip, sip_fileid, sip_adresno, sip_belge_tarih, sip_tarih, sip_belgeno, sip_cagrilabilir_fl,sip_OnaylayanKulNo, sip_depono, sip_firmano, sip_subeno, sip_aciklama,sip_musteri_kod, sip_satici_kod,sip_teslim_tarih, sip_stok_sormerk, sip_cari_sormerk, sip_isk1, sip_isk2, sip_isk3, sip_isk4, sip_isk5, sip_isk6, sip_iskonto1, sip_iskonto2, sip_iskonto3, sip_iskonto4, sip_iskonto5, sip_iskonto6, sip_opno, sip_harekettipi, sip_satirno, sip_stok_kod, sip_birim_pntr, sip_miktar, sip_b_fiyat, sip_tutar, sip_vergi_pntr, sip_vergi) values ($sip_RECid_DBCno, $sip_RECid_RECno, $sip_SpecRECno, '$sip_evrakno_seri', $sip_evrakno_sira, '$sip_create_date', '$sip_lastup_date', $sip_create_user, $sip_lastup_user, $sip_tip, $sip_fileid, $sip_adresno, '$sip_belge_tarih', '$sip_tarih', $sip_belgeno, $sip_cagrilabilir_fl, $sip_OnaylayanKulNo, $sip_depono, $sip_firmano, $sip_subeno, '$sip_aciklama', '$sip_musteri_kod', '$sip_satici_kod', '$sip_teslim_tarih', '$sip_stok_sormerk', '$sip_cari_sormerk', $sip_isk1, $sip_isk2, $sip_isk3, $sip_isk4, $sip_isk5, $sip_isk6, $sip_iskonto1, $sip_iskonto2, $sip_iskonto3, $sip_iskonto4, $sip_iskonto5, $sip_iskonto6, $sip_opno, $sip_harekettipi, $sip_satirno, '$sip_stok_kod', $sip_birim_pntr, $sip_miktar, $sip_b_fiyat, $sip_tutar, $sip_vergi_pntr, $sip_vergi)";
		//mssql_query($sql);

		$lastinsert = $this->lastinsert();
		$sql_update = "update SIPARISLER set sip_iptal = $sip_iptal, sip_hidden = $sip_hidden, sip_kilitli = $sip_kilitli, sip_degisti = $sip_degisti, sip_checksum = $sip_checksum, sip_cins = $sip_cins, sip_teslim_miktar = $sip_teslim_miktar, ";
		$sql_update .= "sip_iskonto_1 = $sip_iskonto_1, sip_iskonto_2 = $sip_iskonto_2, sip_iskonto_3 = $sip_iskonto_3, sip_iskonto_4 = $sip_iskonto_4, sip_iskonto_5 = $sip_iskonto_5, sip_iskonto_6 = $sip_iskonto_6, ";
		$sql_update .= "sip_masraf_1 = $sip_masraf_1, sip_masraf_2 = $sip_masraf_2, sip_masraf_3 = $sip_masraf_3, sip_masraf_4 = $sip_masraf_4, ";
		$sql_update .= "sip_masvergi_pntr = $sip_masvergi_pntr, sip_masvergi = $sip_masvergi, ";
		$sql_update .= "sip_special1 = '$sip_special1', sip_special2 = '$sip_special2', sip_special3 = '$sip_special3', ";
		$sql_update .= "sip_vergisiz_fl = $sip_vergisiz_fl, sip_kapat_fl = $sip_kapat_fl, sip_promosyon_fl = $sip_promosyon_fl, sip_cari_grupno = $sip_cari_grupno, sip_doviz_cinsi = $sip_doviz_cinsi, ";
		$sql_update .= "sip_doviz_kuru = $sip_doviz_kuru, sip_alt_doviz_kuru = $sip_alt_doviz_kuru, sip_masraf1 = $sip_masraf1, sip_masraf2 = $sip_masraf2, sip_masraf3 = $sip_masraf3, sip_masraf4 = $sip_masraf4, ";
		$sql_update .= "sip_mas1 = $sip_mas1, sip_mas2 = $sip_mas2, sip_mas3 = $sip_mas3, sip_mas4 = $sip_mas4, ";
		$sql_update .= "sip_kar_orani = $sip_kar_orani, sip_durumu = $sip_durumu, sip_stalRecId_DBCno = $sip_stalRecId_DBCno, sip_stalRecId_RECno = $sip_stalRecId_RECno, sip_planlananmiktar = $sip_planlananmiktar, sip_teklifRecId_DBCno = $sip_teklifRecId_DBCno, sip_teklifRecId_RECno = $sip_teklifRecId_RECno, ";
		$sql_update .= "sip_parti_kodu = $sip_parti_kodu, sip_lot_no = $sip_lot_no, sip_Exp_Imp_Kodu = '$sip_Exp_Imp_Kodu', sip_paket_kod = '$sip_paket_kod', ";
		$sql_update .= "sip_prosiprecDbId = $sip_prosiprecDbId, sip_prosiprecrecI = $sip_prosiprecrecI, sip_fiyat_liste_no = $sip_fiyat_liste_no, ";
		$sql_update .= "sip_Otv_Pntr = $sip_Otv_Pntr, sip_Otv_Vergi = $sip_Otv_Vergi, sip_otvtutari = $sip_otvtutari, sip_OtvVergisiz_Fl = $sip_OtvVergisiz_Fl, ";
		$sql_update .= "sip_RezRecId_DBCno = $sip_RezRecId_DBCno, sip_RezRecId_RECno = $sip_RezRecId_RECno, sip_yetkili_recid_dbcno = $sip_yetkili_recid_dbcno, sip_yetkili_recid_recno = $sip_yetkili_recid_recno, sip_onodeme_evrak_tip = $sip_onodeme_evrak_tip, ";
		$sql_update .= "sip_kapatmanedenkod = '$sip_kapatmanedenkod', sip_gecerlilik_tarihi = '$sip_gecerlilik_tarihi', sip_projekodu = '$sip_projekodu', ";
		$sql_update .= "sip_onodeme_evrak_sira = $sip_onodeme_evrak_sira, sip_rezervasyon_miktari = $sip_rezervasyon_miktari, sip_rezerveden_teslim_edilen = $sip_rezerveden_teslim_edilen, ";

		$sql_update = rtrim($sql_update, ", ");
		$sql_update .= " where sip_RECno = ".$lastinsert;
		echo $sql_update;
		//odbc_exec($this->conn, $sql_update);
	}

	public function siparislerUser()
	{
		$RecID_RECno = "334885";
		$RecID_DBCno = 0;
		$Fatura_Tarihi = '2015-11-10';
		$Fatura_No = "123765";
		$Mail = "osmangoveli@gmail.com";
		$Tel_No = "02163989898";
		$Musteri_Adi = "Osman Göveli";
		$Musteri_Adresi = "Istanbul merkez";
		$Dis_Derinligi = "11";
		$Pozisyon = "Sağ ön";
		$Hava_Basinci = "32";
		$Marka_Model = "Bmw 3 serisi";
		$Aciklama = "hava kaçırıyor.";
		$Marka = "Michelin";
		$Ebat = "225/45R17";
		$Seri_No = "as1276";
		$Plaka = "06FD600";
		$Cari_Kodu = "120.81.00022";


		$sql2 = "insert into SIPARISLER_USER (RecID_RECno, RecID_DBCno, Fatura_Tarihi, Fatura_No,Mail,Tel_No,Musteri_Adi,Musteri_Adresi,Dis_Derinligi,Pozisyon,Hava_Basinci,Marka_Model,Aciklama,Marka,Ebat,Seri_No,Plaka, Cari_Kodu)values
		($RecID_RECno, $RecID_DBCno,$Fatura_Tarihi,$Fatura_No,'$Mail','$Tel_No','$Musteri_Adi','$Musteri_Adresi','$Dis_Derinligi','$Pozisyon','$Hava_Basinci','$Marka_Model','$Aciklama','$Marka','$Ebat','$Seri_No','$Plaka','$Cari_Kodu')";

		echo $sql2;
	}

	public function lastinsert()
	{
		$result = mssql_query("SELECT MAX(sip_RECno) AS LastID FROM SIPARISLER");
		$result = mssql_fetch_object($result);
		return $result->LastID;
	}

	public function arizaliLastik()
	{
		$user = "B2B";
		$cari_kod = "120.81.00022";
		$adres_no = 1;
		$sirket_kodu = "0014"; // SELECT * from MV4.dbo.SIRKET
		$fatura_tarihi = date('Y-m-d', time());
		$fatura_no     = "00255734";
		$musteri_mail_adresi = "osmangoveli57@gmail.com";
		$musteri_tel_no = "05382637785";
		$musteri_adi = "Osman Göveli";
		$musteri_adresi = "Mehmet Akif Ersoy Mah. Gül Cad. No:3 Daire:6";
		$dis_derinligi = "10";
		$pozisyon = "Sol ön";
		$hava_basinci = 12;
		$marka_model = "Bmw x5";
		$aciklama = "Test Arızalı Lastik";
		$marka = "Michelin";
		$lastik_ebati = "225/45R17";
		$lastik_seri_no = "12313123";
		$plaka = "06FD600";

		$this->arizaSepeteAt();
	}


	/*Yeniler*/
	public function odemeplanlariiskontolu($odp_no = NULL)
	{
		$data = array();
		if ($odp_no) {
			$where = "where odp_no=".$odp_no;
		} else {
			$where = "where odp_special2 = 'B' or odp_special2 = 'K' or odp_no=4  or odp_no=31  or odp_no=17 or odp_no=16";
		}
		$sql = "select odp_no, odp_kodu, odp_adi, isnull((select * from MikroDB_V16_LASTIKCIM.dbo.m5fn_iskontolar(odp_no)),0) as oran, odp_special2 from ODEME_PLANLARI with(nolock) ".$where." order by odp_kodu";
		// echo $sql;
		$res = mssql_query($sql);
		// $res = mssql_fetch_object($res);
		$count = mssql_num_rows($res);

		if ($count > 0) {
			$data['status'] = TRUE;

			while ($row = mssql_fetch_array($res)) {
				$op_array = array();
				$op_array = $this->convert($row);
				
	        	$data['results'][] = $op_array;
		    }

		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		return $data;
	}

	public function babs($cari = NULL, $yil = NULL, $ay = NULL)
	{
		/*
			0 => Alış Faturası
			63 => Satış Faturası
		*/
		if ($cari && $ay && $yil) {

			$sql = "EXEC m5sp_babsfaturalistesi @ns = '', @ctrl = '', @userid = '', @aramatipi = '', @ilkkod = '".$cari."', @sonkod = '".$cari."', @kodyapisi = '', @yil = '".$yil."', @ay = '".$ay."'";
			$res = mssql_query($sql);
			
			if (!$res) {
			    die('MSSQL error: ' . mssql_get_last_message());
			}

			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;

				while ($row = mssql_fetch_array($res)) {
					$op_array = array();
					$op_array = $this->convert($row);
					
		        	$data['results'][] = $op_array;
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		return $data;
	}

	public function cariextrakredilimit($cari = NULL)
	{
		if ($cari) {	

			$sql = "select * from dbo.mv4_cariextrakredilimit('".$cari."')";
			$res = mssql_query($sql);
			$res = mssql_fetch_object($res);
			// var_dump($res);
			if ($res) {
				return json_encode($res);
			} else {
				return FALSE;
			}

		} else {
			return FALSE;
		}
	}

	public function CariBazliBankaDbsLimitleri($cari = NULL)
	{
		if ($cari) {	

			$sql = "EXEC dp_CariBazliBankaDbsLimitleri '".$cari."'";
			$res = mssql_query($sql);
			$res = mssql_fetch_array($res);
			$res = $this->convert($res);
			
			$yres = array();
			foreach ($res as $key => $value) {
				// $key = (mb_convert_encoding($key, 'UTF-8', mb_detect_encoding($key, mb_detect_order(), true)));
				switch ($key) {
					case 'akbank':
						$key = "Akbank";	
					break;

					case 'finansbank':
						$key = "Finans Bank";	
					break;
					
					case 'garantibankasi':
						$key = "Garanti Bankası";
					break;

					case 'halkbank':
						$key = "Halk Bankası";
					break;

					case 'ingbank':
						$key = "İng Bankası";	
					break;

					case 'isbankasi':
						$key = "İş Bankası";	
					break;

					case 'turkiyefinans':
						$key = "Türkiye Finans";	
					break;

					case 'denizbank':
						$key = "Deniz Bank";
					break;

					case 'yapikredi':
						$key = "Yapı Kredi";	
					break;

					case 'ziraatbankasi':
						$key = "Ziraat Bankası";
					break;
					
					case 'albaraka':
						$key = "Albaraka";	
					break;
				}
				$yres[$key] = $value;
			}

			if ($yres) {
				return json_encode($yres);
			} else {
				return FALSE;
			}

		} else {
			return FALSE;
		}
	}

	public function Odenecek_Fatura_Listesi($cari = NULL)
	{
		$data = array();
		if ($cari) {	

			$sql = "EXEC m5_spOdenecek_Fatura_Listesi '".$cari."'";
			$res = mssql_query($sql);
			// $res = mssql_fetch_array($res);
			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;

				while ($row = mssql_fetch_array($res)) {
					$op_array = array();
					$op_array = $this->convert($row);
					
		        	$data['results'][] = $op_array;
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}

		// $data = json_encode($data, JSON_UNESCAPED_UNICODE);
		return $data;
	}

	public function Yapilacak_Tahsilat_Raporu($cari = NULL, $srm = NULL)
	{
		$data = array();
		if ($cari) {	
			$srm = $this->eklerken($srm);
			// var_dump($srm);
			// $srm = str_replace('İ', 'i', $srm);
			$sql = "EXEC dp_Yapilacak_Tahsilat_Raporu '".$cari."','".$srm."'";
			$res = mssql_query($sql);
			
			if (!$res) {
			    die('MSSQL error: ' . mssql_get_last_message());
			}
			
			// $res = mssql_fetch_array($res);
			$count = mssql_num_rows($res);

			if ($count > 0) {
				$data['status'] = TRUE;

				while ($row = mssql_fetch_array($res)) {
					$op_array = array();
					$op_array = $this->convert($row);
					
		        	$data['results'][] = $op_array;
			    }

			} else {
				$data['status'] = FALSE;
			}

		} else {
			$data['status'] = FALSE;
		}

		// $data = json_encode($data, JSON_UNESCAPED_UNICODE);
		return $data;
	}

	private function stokismitemizle($isim = NULL)
	{
		$isim = str_replace(",", ".", $isim);
		$isim = str_ireplace("x", "/", $isim);
		$isim = str_ireplace(" ", "/", $isim);

		$isim = trim($isim);
		return $isim;
	}


	public function stokAraYeni($sparam = NULL, $row = FALSE)
	{
		$data  = array();
		$data['params'] = $sparam;
		$iskonto = NULL;
		/*Sabitler*/
		if (isset($sparam['odeme'])) {
			$odeme = $sparam['odeme'];
			unset($sparam['odeme']);
			if ($odeme > 0)
				$iskonto = ", isnull((select * from MikroDB_V16_LASTIKCIM.dbo.m5fn_iskontolar(".$odeme.")),0) as odp_oran";
		}
		

		if (isset($sparam['select'])) {
			$select = $sparam['select'];
			unset($sparam['select']);
		}

		if (isset($sparam['page'])) {
			$page = $sparam['page'];
			unset($sparam['page']);
		}

		if (isset($sparam['limit'])) {
			$limit = $sparam['limit'];
			unset($sparam['limit']);
		}

		if (isset($sparam['min'])) {
			$min = $sparam['min'];
			unset($sparam['min']);
		}		

		if (!is_numeric($page))
			$page = 1;

		if (is_null($limit))
			$limit = $this->limit;

		if (is_null($min))
			$min = 0;

		if (isset($sparam['sto_kalkon_kodu']))
			$sparam['sto_kalkon_kodu'] = rtrim($sparam['sto_kalkon_kodu'], "Ş");

		if (isset($sparam['depo']) && $sparam['depo'] > 0) {
			$miktar = "dbo.fn_DepodakiMiktarSonDurum(sto_kod, ".$sparam['depo'].")";
			unset($sparam['depo']);
		} else {
			// $miktar = "dbo.fn_EldekiMiktar(sto_kod)";
			// $miktar = "( isnull((select sum(tkl_teslim_miktar) - sum(tkl_miktar) from VERILEN_TEKLIFLER where tkl_stok_kod = sto_kod), 0) + dbo.fn_EldekiMiktar(sto_kod) )";
			// $miktar = "dbo.mv4_SertasBayiDepoMiktarAnaDepo(stok_kod)";
			// $miktar = "( (dbo.fn_DepodakiMiktar(sto_kod,4,getdate())) + (dbo.fn_DepodakiMiktar(sto_kod,26,getdate())) + (dbo.fn_DepodakiMiktar(sto_kod,27,getdate())) + (dbo.fn_DepodakiMiktar(sto_kod,19,getdate())) + (dbo.fn_DepodakiMiktar(sto_kod,28,getdate())) - (select ISNULL(((select (sum(tkl_miktar) - sum(tkl_teslim_miktar) ) from VERILEN_TEKLIFLER where tkl_stok_kod = sto_kod and TKL_KAPAT_FL=0)),0)) - (select ISNULL(((select sum(sip_miktar-sip_teslim_miktar) from SIPARISLER with(nolock)  where sip_kapat_fl = 0 and sip_miktar > sip_teslim_miktar and sip_tip = 0  and sip_stok_kod = sto_kod )),0)) )";
			$miktar = "( isnull(	( SELECT sum( tkl_teslim_miktar ) - sum( tkl_miktar ) FROM VERILEN_TEKLIFLER WHERE tkl_stok_kod = sto_kod AND TKL_KAPAT_FL = 0 AND tkl_miktar > tkl_teslim_miktar ), 0) + dbo.fn_EldekiMiktar ( sto_kod ) )";
		}
		/*Sabitler*/


		$where = NULL;

		if (is_numeric($min)) {
			if ($min == 0) {

			} else {
				$where .= " AND ".$miktar." >= ".$min;
			}
		}

		foreach ($sparam as $key => $value) {

			if ((is_string($value) && strlen($value) > 0) || (is_numeric($value) && $value > 0)) {
				
				if (is_string($value))
					$value = $this->eklerken($value);

				switch ($key) {
					case 'jantcapi':
						$where .= " AND ".$key." like '%".$value."%'";
					break;

					case 'xlrf':
						$where .= " AND xlrf <> ''";
					break;
					
					case 'sto_kalkon_kodu':
						$where .= " AND ".$key." like '%".$value."%'";
					break;

					case 'rftzprofssrzps':
						$where .= " AND rftzprofssrzps <> ''";
					break;

					case 'sto_isim':
						$value = $this->stokismitemizle($value);
						$where .= " AND sto_isim like '%".$value."%'";	
					break;

					default:
						$where .= " AND ".$key." = '".$value."'";
					break;
				}
			}

		}

		$where .= " AND dbo.fn_StokSatisFiyati(sto_kod, 1, 0) > 0";

		$where = ltrim($where, " AND");
		$where = "where ".$where;

		// var_dump($where);

		if (!isset($select)) {
			$select = "sto_isim, sto_kod, ".$miktar." as miktar, dbo.fn_StokSatisFiyati(sto_kod, 1, 0) as fiyat, dbo.fn_DovizSembolu(dbo.fn_StokFiyatDovizCinsi(sto_kod, 1, 0)) as doviz, tabani, yanagi, jantcapi, yuk, hiz, lastikdeseni, resim1, resim2, resim3, xlrf, rftzprofssrzps, kati, yakit, islak, gurultu, oem, dot, dbo.fn_MarkaIsmi(sto_marka_kodu) as mrk_ismi, sto_marka_kodu, sto_kalkon_kodu as mevsim, dbo.fn_AnaGrupIsmi(sto_anagrup_kod) as kategori, sto_anagrup_kod, dbo.fn_AltGrupIsmi(sto_anagrup_kod, sto_altgrup_kod) as altkategori, sto_altgrup_kod, altkategoriadi2 as enaltkategori, Ebat, dbo.fn_SektorIsmi ( sto_sektor_kodu ) AS sktr_ismi, dbo.fn_AmbalajIsmi ( sto_ambalaj_kodu ) AS amb_ismi, dbo.fn_KategoriIsmi ( sto_kategori_kodu ) AS ktg_isim, dbo.fn_ModelIsmi ( sto_model_kodu ) AS mdl_ismi, dbo.fn_ReyonIsmi ( sto_reyon_kodu ) AS ryn_ismi, dbo.fn_UreticiIsmi ( sto_uretici_kodu ) AS urt_ismi".$iskonto;
			$sql   = "select ".$select." from STOKLAR WITH (NOLOCK) LEFT JOIN STOKLAR_USER on RecID_RECno = sto_RECno ".$where." order by dbo.fn_EldekiMiktar(sto_kod) ASC OFFSET ".(($page-1)*$limit)." ROWS FETCH NEXT ".$limit." ROWS ONLY ";
		} else {
			$sql   = "select ".$select." from STOKLAR WITH (NOLOCK) LEFT JOIN STOKLAR_USER on RecID_RECno = sto_RECno ".$where;
		}
		
		// echo $sql;
		$res   = mssql_query($sql);
		$count = mssql_num_rows($res); 
		if ($count > 0) {
			$data['status'] = TRUE;

			while ($row = mssql_fetch_array($res)) {
				$stok_array = array();
				$stok_array = $this->convert($row);
				if (!isset($select)) {
					$sql_depolar = "select * from dbo.MV4_stokdepodurumlari('".$stok_array['sto_kod']."')";
					$res_depolar = mssql_query($sql_depolar);
					$count_depo  = mssql_num_rows($res_depolar);
					if ($count_depo > 0) {
						while ($depo_row = mssql_fetch_array($res_depolar)) {
							$stok_array['depolar'][] = $this->convert($depo_row);
						}
					}
				}
	        	$data['results'][] = $stok_array;
		    }

		} else {
			$data['status'] = FALSE;
		}
		$data['error'] = json_last_error();
		$data = json_encode($data);
		return $data;
	}

	public function siparisver()
	{
		/*
			sip_RECid_DBCno, 
			sip_RECid_RECno, 
			sip_SpecRECno, 
			sip_evrakno_seri, 
			sip_evrakno_sira, 
			sip_create_date, 
			sip_lastup_date, 
			sip_create_user, 
			sip_lastup_user, 
			sip_tip, 
			sip_fileid, 
			sip_adresno, 
			sip_belge_tarih, 
			sip_tarih, 
			sip_belgeno, 
			sip_cagrilabilir_fl,
			sip_OnaylayanKulNo, 
			sip_depono, 
			sip_firmano, 
			sip_subeno, 
			sip_aciklama,
			sip_musteri_kod, 
			sip_satici_kod,
			sip_teslim_tarih, 
			sip_stok_sormerk, 
			sip_cari_sormerk, 
			sip_isk1, 
			sip_isk2, 
			sip_isk3, 
			sip_isk4, 
			sip_isk5, 
			sip_isk6, 
			sip_iskonto1, 
			sip_iskonto2, 
			sip_iskonto3, 
			sip_iskonto4, 
			sip_iskonto5, 
			sip_iskonto6, 
			sip_opno, 
			sip_harekettipi, 
			sip_satirno, 
			sip_stok_kod, 
			sip_birim_pntr, 
			sip_miktar, 
			sip_b_fiyat, 
			sip_tutar, 
			sip_vergi_pntr, 
			sip_vergi



			sip_RECno
			sip_RECid_DBCno
			sip_RECid_RECno
			sip_SpecRECno
			sip_iptal
			sip_fileid
			sip_hidden
			sip_kilitli
			sip_degisti
			sip_checksum
			sip_create_user
			sip_create_date
			sip_lastup_user
			sip_lastup_date
			sip_special1
			sip_special2
			sip_special3
			sip_firmano
			sip_subeno
			sip_tarih
			sip_teslim_tarih
			sip_tip
			sip_cins
			sip_evrakno_seri
			sip_evrakno_sira
			sip_satirno
			sip_belgeno
			sip_belge_tarih
			sip_satici_kod
			sip_musteri_kod
			sip_stok_kod
			sip_b_fiyat
			sip_miktar
			sip_birim_pntr
			sip_teslim_miktar
			sip_tutar
			sip_iskonto_1
			sip_iskonto_2
			sip_iskonto_3
			sip_iskonto_4
			sip_iskonto_5
			sip_iskonto_6
			sip_masraf_1
			sip_masraf_2
			sip_masraf_3
			sip_masraf_4
			sip_vergi_pntr
			sip_vergi
			sip_masvergi_pntr
			sip_masvergi
			sip_opno
			sip_aciklama
			sip_aciklama2
			sip_depono
			sip_OnaylayanKulNo
			sip_vergisiz_fl
			sip_kapat_fl
			sip_promosyon_fl
			sip_cari_sormerk
			sip_stok_sormerk
			sip_cari_grupno
			sip_doviz_cinsi
			sip_doviz_kuru
			sip_alt_doviz_kuru
			sip_adresno
			sip_teslimturu
			sip_cagrilabilir_fl
			sip_prosiprecDbId
			sip_prosiprecrecI
			sip_iskonto1
			sip_iskonto2
			sip_iskonto3
			sip_iskonto4
			sip_iskonto5
			sip_iskonto6
			sip_masraf1
			sip_masraf2
			sip_masraf3
			sip_masraf4
			sip_isk1
			sip_isk2
			sip_isk3
			sip_isk4
			sip_isk5
			sip_isk6
			sip_mas1
			sip_mas2
			sip_mas3
			sip_mas4
			sip_Exp_Imp_Kodu
			sip_kar_orani
			sip_durumu
			sip_stalRecId_DBCno
			sip_stalRecId_RECno
			sip_planlananmiktar
			sip_teklifRecId_DBCno
			sip_teklifRecId_RECno
			sip_parti_kodu
			sip_lot_no
			sip_projekodu
			sip_fiyat_liste_no
			sip_Otv_Pntr
			sip_Otv_Vergi
			sip_otvtutari
			sip_OtvVergisiz_Fl
			sip_paket_kod
			sip_RezRecId_DBCno
			sip_RezRecId_RECno
			sip_harekettipi
			sip_yetkili_recid_dbcno
			sip_yetkili_recid_recno
			sip_kapatmanedenkod
			sip_gecerlilik_tarihi
			sip_onodeme_evrak_tip
			sip_onodeme_evrak_seri
			sip_onodeme_evrak_sira
			sip_rezervasyon_miktari
			sip_rezerveden_teslim_edilen

		*/
	}

	/*
		aktarimrecno,
		tablename,
		gorevno,
		tipi,
		durumu,
		keydata,
		Silindi,
		dpartner
	*/
	public function aktarimlar($sdata = NULL, $row = FALSE)
	{
		$where = "";
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$where .= " and $key = '$value'";
			}
		}
		$where .= " and (dpartner = 0)";
		$where = trim($where);
		$where = ltrim($where, "and");
		$where = trim($where);

		if (strlen($where) > 0) {
			$where = " where ".$where;
		}
		
		$data = array();		
		$data['params'] = $sdata;
		if ($where) {
			$sql = "select TOP 100 * from M5_ENTEGRASYONAKTARIM ".$where." order by aktarimrecno desc";
			$res = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		return json_encode($data);

	}

	public function updateaktarim($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$sql = "update M5_ENTEGRASYONAKTARIM set dpartner = 1 where aktarimrecno = ".$id;
			$res = mssql_query($sql);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function aktarimyeni()
	{
		$data = array();
		$sql = "select * from M5VW_STOKMIKTARLASTIKCIM";
		$res = mssql_query($sql);
		$count = mssql_num_rows($res);
		if ($count > 0) {
			while ($row = mssql_fetch_array($res)) {
	        	$data['results'][] = $this->convert($row);
		    }
		}
		var_dump($data);
	}
	/*Yeniler*/
}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Csf
{
	protected $ci;
	function __construct()
	{
		$this->ci = get_instance();
	}

	public function parametreler()
	{
		$columns = array();						
		$this->ci->db->select('*');
		$this->ci->db->from('parametreler');
		$this->ci->db->group_by('silik_id');
		$res = $this->ci->db->get()->result();
		
		if ($res && count($res) > 0) {
			foreach ($res as $key => $value) {
				$columns[$value->silik_id] = $value->veritip;
				if ($value->extralar) {
					$extralar = explode(",", $value->extralar);
					foreach ($extralar as $extra) {
						$extra = str_replace("param", $value->silik_id, $extra);
						$columns[$extra] = "varchar(255)";
					}
				}
			}
		}

		if (count($columns) > 0)
			return $columns;
		else
			return FALSE;
	}

	public function tabloeksikmi($data = NULL, $tablename = NULL)
	{
		if (count($data) > 0 && $tablename) {

			$fields_list 	= $this->ci->db->list_fields($tablename);
	        $keyler 		= array_keys($data);

	        $this->ci->load->dbforge();
	        foreach ($keyler as $key) {

	            $varmi = in_array($key, $fields_list);
	            if ($varmi === FALSE) {
	                $fields = array( $key => array('type' => $data[$key], 'null' => TRUE) );
	                $res = $this->ci->dbforge->add_column($tablename, $fields);
	            } else {
	            	$fields = array( $key => array('type' => $data[$key], 'null' => TRUE) );
	                $res = $this->ci->dbforge->modify_column($tablename, $fields);
	            }

	        }
	        return TRUE;
		} else {
			return FALSE;
		}
	}

}
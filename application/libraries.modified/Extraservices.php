<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Extraservices
{
	
	protected $ci;
	function __construct()
	{
		$this->ci = get_instance();
	}

	public function parametregruplari($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('parametre_gruplari');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}
		$this->ci->db->order_by('sira', 'asc');
		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}



	public function iller(){
		$this->ci->db->select('*');
		$this->ci->db->from('iller');
		return $this->ci->db->get()->result();
	}


	public function ilceler($il_id = 34){
		$this->ci->db->select('*');
		$this->ci->db->from('ilceler');
		$this->ci->db->where('il_id', $il_id);
		return $this->ci->db->get()->result();
	}


	public function parametreselectvalue($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('parametre_select_value');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function parametredegerleri($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('parametre_degerleri');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}
		
		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function parametreler($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('parametreler');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}
		$this->ci->db->order_by('sira', 'asc');
		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function sahipler($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('sahipler');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}
		// $this->ci->db->order_by('sira', 'asc');
		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function gruplar($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('ayarlar_gruplari');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}
		$this->ci->db->order_by('sira', 'asc');
		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function sayfaturleri($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('sayfa_turleri');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}
	
	public function ayarlar($sdata = NULL, $row = FALSE)
	{
		$this->ci->db->select('*');
		$this->ci->db->from('ayarlar');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->ci->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->ci->db->get()->row();
		} else {
			$res = $this->ci->db->get()->result();
		}

		return $res;
	}

	public function ayarla($meta = NULL, $data = NULL)
	{
		$this->ci->db->where('name', $meta);
		$res = $this->ci->db->update('ayarlar', array('value' => $data));

		return $res;
	}

	public function fullresimurl($ayar = NULL, $resim = NULL)
	{
		$dizin = $this->ayarlar(array('name' => $ayar), TRUE)->value;
		if ($dizin) {
			return $dizin.$resim;
		} else {
			return NULL;
		}
	}

	public function fullisim($kullanici = NULL)
	{
		if ($kullanici && is_numeric($kullanici)) {
			$this->ci->db->select('ad, soyad');
			$this->ci->db->from('kullanicilar');
			$this->ci->db->where('id', $kullanici);
			$kullanici = $this->ci->db->get()->row();
			if ($kullanici) {
				return $kullanici->ad." ".$kullanici->soyad;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}


	/**/
	public function single_file_upload($path = "./uploads/", $name = NULL)
    {
        $config = array(
            'upload_path'   	=> $path,
            'allowed_types' 	=> 'jpeg|gif|png|jpg',
            'overwrite'     	=> 1,
            'max_size' 			=> 10000,
       		'file_ext_tolower' 	=> TRUE,
        	'encrypt_name' 		=> TRUE                     
        );

        $this->ci->load->library('upload', $config);

        $this->ci->upload->initialize($config);
        
        $image = NULL;
        $error = NULL;

        if ($this->ci->upload->do_upload($name)) {
            $updata = $this->ci->upload->data();
            $image = $updata['file_name'];
        } else {
        	$error = $this->ci->upload->display_errors();
        }

        return array('image' => $image, 'error' => $error);
    }
	
	public function multiple_files_upload($path = "./uploads/", $files = NULL)
    {
        $config = array(
            'upload_path'   	=> $path,
            'allowed_types' 	=> 'jpeg|gif|png|jpg',
            'overwrite'     	=> 1,
            'max_size' 			=> 10000,
       		'file_ext_tolower' 	=> TRUE,
        	'encrypt_name' 		=> TRUE                     
        );

        $this->ci->load->library('upload', $config);

        $images = array();
        $errors = array();
        // var_dump($files);

        foreach ($files['name'] as $key => $image) {
        	if (strlen($files['name'][$key]) > 4) {
	            $_FILES['resim']['name']		= $files['name'][$key];
	            $_FILES['resim']['type']		= $files['type'][$key];
	            $_FILES['resim']['tmp_name']	= $files['tmp_name'][$key];
	            $_FILES['resim']['error']		= $files['error'][$key];
	            $_FILES['resim']['size']		= $files['size'][$key];


	            $this->ci->upload->initialize($config);

	            if ($this->ci->upload->do_upload('resim')) {
	                $updata = $this->ci->upload->data();
	                $images[] = $updata['file_name'];
	            } else {
	            	$errors[] = $this->ci->upload->display_errors();
	            }
        	}
        }

        return array('images' => $images, 'errors' => $errors);
    }
	/**/



	public function paytype($data){
		if($data->sanalpos){
			$this->ci->db->select('*');
			$this->ci->db->from('sanalposlar');
			$this->ci->db->where('id', $data->sanalpos);
			return $this->ci->db->get()->row()->banka;
		}elseif($data->banka){
			$this->ci->db->select('*');
			$this->ci->db->from('banka_bilgileri');
			$this->ci->db->where('id', $data->banka);
			return $this->ci->db->get()->row()->banka_ismi;
		}else{
			return FALSE;
		}
	}


	public function il($id){
		$this->ci->db->select('*');
		$this->ci->db->from('iller');
		$this->ci->db->where('id', $id);
		return $this->ci->db->get()->row()->il;
	}

	public function ilce($id){
		$this->ci->db->select('*');
		$this->ci->db->from('ilceler');
		$this->ci->db->where('id', $id);
		return $this->ci->db->get()->row()->ilce;
	}
	public function montaj_noktalari($firma_id){
		$this->ci->db->select('mn.*');
		$this->ci->db->from('montaj_noktalari as mn');
		$this->ci->db->select('iller.il,ilceler.ilce');
		$this->ci->db->join('iller', ' mn.firma_il = iller.id ', 'left');
		$this->ci->db->join('ilceler', ' mn.firma_ilce = ilceler.id ', 'left');
		$this->ci->db->where('mn.id', $firma_id);
		return  $this->ci->db->get()->row();
	}

	/**/
	public function toplamtutar($order_id = NULL)
	{
		if ($order_id && is_numeric($order_id)) {

			$this->ci->load->model('orders/orders_model', 'orders');
			$order = $this->ci->orders->orders(array('id' => $order_id), TRUE);
			
			if ($order) {

				$urunler = json_decode($order->urunler);

				if ($urunler && count($urunler) > 0) {

					$kargokdvoran 	= 18;
                    $hizmetkdvoran 	= 18;
                    $urunlertoplam 	= 0;
                    $montajtoplam 	= 0;
                    $urunlerkdv 	= 0;
                    $urunlerkargo 	= 0;
                    $toplam 		= 0;

					foreach ($urunler as $urun) {
						if ($urun->kdv_dahil == 1) {
							$kdahil = $this->fiyatvernumber($urun->fiyat * $urun->miktar, $urun->indirim_orani);
							$kharic = $this->kdvcikar($kdahil, $urun->kdv);
                            $kdv 	= $kdahil - $kharic;
						} else {
							$kharic = $this->fiyatvernumber($urun->fiyat * $urun->miktar,  $urun->indirim_orani);
							$kdahil = $this->kdvhesapla($kharic,$urun->kdv);
							$kdv 	= $kdahil -$kharic;
						}

						$urunlertoplam 	= $urunlertoplam + $kharic;
                        $urunlerkdv 	= $urunlerkdv + $kdv;
						
						if ($urun->kargo_bedeli) {
							if ($urun->kargo > 0) {
								$kargokdvdahil 	= $urun->kargo_bedeli*$urun->miktar;
		                        $kargokdvharic 	= $this->kdvcikar($kargokdvdahil ,$kargokdvoran);
		                        $kargokdv 		= $kargokdvdahil - $kargokdvharic;
		                        $urunlerkdv 	= $urunlerkdv + $kargokdv;
		                        $urunlerkargo 	= $urunlerkargo + $kargokdvharic;
							}
						}

						if (count($urun->hizmetler) > 0) {
							foreach ($urun->hizmetler as $hizmet) {
								if ($hizmet->firma_id) {
									$firma_id = $hizmet->firma_id;
								}
								$hizmetkdvdahil = $hizmet->fiyat * $hizmet->adet ;
		                        $hizmetkdvharic = $this->kdvcikar($hizmetkdvdahil ,$hizmetkdvoran);
		                        $hizmetkdv 		= $hizmetkdvdahil - $hizmetkdvharic;
		                        $urunlerkdv 	= $urunlerkdv + $hizmetkdv;
		                        $montajtoplam 	= $montajtoplam + $hizmetkdvharic;
							}
						}
					}

					$toplam = $urunlertoplam + $urunlerkdv + $urunlerkargo + $montajtoplam;

					return $toplam;

				}

			} else {
				return FALSE;
			}

		} else {
			return FALSE;
		}
	}

	private function fiyatvernumber($fiyat = NULL,  $indirim = NULL, $taksit = NULL) {

		$fiyatimiz = NULL;
		if ($indirim) {
			$fiyat = $fiyat-(($fiyat*$indirim)/100);
		}
		if ($taksit) {
			$fiyat = ($fiyat/$taksit);
		}
		if ($fiyat) {
			$fiyatimiz = round($fiyat, 2);
		} else {
			$fiyatimiz = 0;
		}
		return $fiyatimiz;
	}

	private function kdvcikar($fiyat, $kdv) {
		$fiyatimiz = 0;
		if ($fiyat && $kdv) {
			$fiyatimiz = $fiyat/(1 + ($kdv/100));
		}
		$fiyatimiz = round($fiyatimiz, 2);
		return $fiyatimiz;
	}

	private function kdvhesapla($fiyat, $kdv) {
		$fiyatimiz = 0;
		if ($fiyat && $kdv) {
			$fiyatimiz = ($fiyat*$kdv)/100;
		}
		$fiyatimiz = round($fiyatimiz, 2);
		return $fiyatimiz;
	}
	/**/

}
<?php
ini_set('mssql.charset', 'ISO-8859-1');
date_default_timezone_set('Europe/Istanbul');

class Tatkosiparis
{
	public $db_host, $db_name, $db_user, $db_password, $conn, $limit;
	function __construct() {
		$this->limit = 100;
		$this->db_host = '185.87.252.36:1433';
		$this->db_name = 'MikroDB_V16_LASTIKCIM';
		$this->db_user = 'lastikcim_web';
		$this->db_password = '1q2w3e';
		$this->conn = mssql_connect($this->db_host, $this->db_user, $this->db_password);
		mssql_select_db($this->db_name, $this->conn);
	}

	private function convert($data = NULL)
	{
		$keys = (array_keys($data));

		foreach($keys as $key) if (!is_numeric($key)) {
			$data[$key] = utf8_encode($data[$key]);
			$data[$key] = iconv(mb_detect_encoding($data[$key]), mb_detect_encoding($data[$key]), $data[$key]);
			$data[$key] = str_replace("Ý", "İ", $data[$key]);
			$data[$key] = str_replace("ý", "ı", $data[$key]);
			$data[$key] = str_replace("þ", "ş", $data[$key]);
			$data[$key] = str_replace("Þ", "Ş", $data[$key]);
			$data[$key] = str_replace("ð", "ğ", $data[$key]);
			$data[$key] = str_replace("Ð", "Ğ", $data[$key]);
		} else {
			unset($data[$key]);
		}
		return $data;
	}
	
	private function eklerken($data)
	{
		$data = str_replace("İ", "Ý", $data);	
		$data = str_replace("ı", "ý", $data);
		$data = str_replace("ş", "þ", $data);
		$data = str_replace("Ş", "Þ", $data);
		$data = str_replace("ğ", "ð", $data);
		$data = str_replace("Ğ", "Ð", $data);
		$data = utf8_decode($data);
		return $data;
	}


	/*Sipariş İşlemleri*/
	public function siparisBilgileri($cari_kod = NULL)
	{
		$data = array();		
		$data['params'] = array('cari_kod' => $cari_kod);

		if ($cari_kod) {
			$sql   = "select * from SIPARISLER where sip_musteri_kod = '".$cari_kod."' and sip_evrakno_seri = 'LC'";
			$res   = mssql_query($sql);
			$count = mssql_num_rows($res);
			if ($count > 0) {
				$data['status'] = TRUE;
				while ($row = mssql_fetch_array($res)) {
		        	$data['results'][] = $this->convert($row);
			    }
			} else {
				$data['status'] = FALSE;
			}
		} else {
			$data['status'] = FALSE;
		}
		$data = json_encode($data);
		
		return $data;		
	}

	public function dovizKuru()
	{
		$sql = "select dbo.fn_KurBul('', 1, 1) as kur";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->kur;
		} else {
			return 1;
		}
	}

	public function dovizKuruAlt()
	{
		$sql = "select dbo.fn_FirmaAlternatifDovizKuru() as kur";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->kur;
		} else {
			return 1;
		}
	}

	public function siparisIdAlalim()
	{
		$sql = "SELECT max(sip_evrakno_sira)+1 as numara from SIPARISLER where sip_evrakno_seri = 'LC'";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			if ((int)$res->numara == 0) {
				return 1;
			} else {
				return $res->numara;
			}
		} else {
			return 1;
		}
	}

	public function siparisRecnoAlalim()
	{
		$sql = "SELECT IDENT_CURRENT ('SIPARISLER') + 1 AS numara";
		$res = mssql_query($sql);
		$res = mssql_fetch_object($res);
		if ($res) {
			return $res->numara;
		} else {
			return FALSE;
		}
	}

	public function siparisOlustur($data = NULL)
	{
		$data['cari_kod'] 	= "120.34.00302";
		$data['cari_adres'] = "1";

		$response 	= array();
		$veriler 	= array();

		$boslar = '{"sip_special1":" ","sip_special2":" ","sip_special3":" ","sip_belgeno":" ","sip_satici_kod":"151","sip_aciklama":" ","sip_aciklama2":" ","sip_stok_sormerk":"51.01.001","sip_teslimturu":"SEVK","sip_Exp_Imp_Kodu":" ","sip_parti_kodu":" ","sip_projekodu":"WEB","sip_paket_kod":" ","sip_kapatmanedenkod":" ","sip_onodeme_evrak_seri":" "}';
		$boslar = json_decode($boslar, TRUE);

		$veriler = array_merge($veriler, $boslar);

		$sifirlar = '{"sip_RECid_DBCno":"0","sip_SpecRECno":"0","sip_iptal":"0","sip_hidden":"0","sip_kilitli":"0","sip_degisti":"0","sip_checksum":"0","sip_firmano":"0","sip_subeno":"0","sip_tip":"0","sip_cins":"0","sip_teslim_miktar":"0","sip_iskonto_1":"0","sip_iskonto_2":"0","sip_iskonto_3":"0","sip_iskonto_4":"0","sip_iskonto_5":"0","sip_iskonto_6":"0","sip_masraf_1":"0","sip_masraf_2":"0","sip_masraf_3":"0","sip_masraf_4":"0","sip_masvergi_pntr":"0","sip_masvergi":"0","sip_OnaylayanKulNo":"0","sip_vergisiz_fl":"0","sip_kapat_fl":"0","sip_promosyon_fl":"0","sip_cari_grupno":"0","sip_doviz_cinsi":"0","sip_cagrilabilir_fl":"0","sip_prosiprecDbId":"0","sip_prosiprecrecI":"0","sip_iskonto1":"0","sip_isk1":"0","sip_isk2":"0","sip_isk3":"0","sip_isk4":"0","sip_isk5":"0","sip_isk6":"0","sip_mas1":"0","sip_mas2":"0","sip_mas3":"0","sip_mas4":"0","sip_kar_orani":"0","sip_durumu":"0","sip_stalRecId_DBCno":"0","sip_stalRecId_RECno":"0","sip_planlananmiktar":"0","sip_teklifRecId_DBCno":"0","sip_teklifRecId_RECno":"0","sip_lot_no":"0","sip_fiyat_liste_no":"1","sip_Otv_Pntr":"0","sip_Otv_Vergi":"0","sip_otvtutari":"0","sip_OtvVergisiz_Fl":"0","sip_RezRecId_DBCno":"0","sip_RezRecId_RECno":"0","sip_yetkili_recid_dbcno":"0","sip_yetkili_recid_recno":"0","sip_onodeme_evrak_tip":"0","sip_onodeme_evrak_sira":"0","sip_rezervasyon_miktari":"0","sip_rezerveden_teslim_edilen":"0"}';
		$sifirlar = json_decode($sifirlar, TRUE);

		$veriler = array_merge($veriler, $sifirlar);

		/**/
		$veriler['sip_cari_sormerk']	= "".$this->eklerken("CARİ")."";
		$veriler['sip_fileid'] 			= "21";
		$veriler['sip_create_user'] 	= "246";
		$veriler['sip_create_date'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_lastup_user'] 	= "246";
		$veriler['sip_lastup_date'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_tarih'] 			= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_teslim_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_evrakno_seri'] 	= "LC";
		$veriler['sip_vergi_pntr'] 		= "4";
		$veriler['sip_belge_tarih'] 	= date('Y-m-d 00:00:00.000', time());
		$veriler['sip_depono'] 			= "26";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_iskonto2'] 		= "1";
		$veriler['sip_iskonto3'] 		= "1";
		$veriler['sip_iskonto4']		= "1";
		$veriler['sip_iskonto5'] 		= "1";
		$veriler['sip_iskonto6'] 		= "1";
		$veriler['sip_masraf1'] 		= "1";
		$veriler['sip_masraf2'] 		= "1";
		$veriler['sip_masraf3'] 		= "1";
		$veriler['sip_masraf4'] 		= "1";
		$veriler['sip_doviz_kuru'] 		= "1";
		$veriler['sip_gecerlilik_tarihi'] = "1899-12-30 00:00:00.000";
		/**/

		if ($data['ozel_siparis_id'] && is_numeric($data['ozel_siparis_id'])) {
			$siparis_id_alinan = $data['ozel_siparis_id'];
		} else {
			$siparis_id_alinan = $this->siparisIdAlalim();
		}

		$alinacaklar = array();
		$alinacaklar['sip_evrakno_sira'] 	= "".$siparis_id_alinan."";
		$alinacaklar['sip_musteri_kod'] 	= "".$data['cari_kod']."";
		$alinacaklar['sip_opno'] 			= "76"; // Ödeme Planı 0 - Peşin -- 1 - Kredi Kartı
		$alinacaklar['sip_alt_doviz_kuru'] 	= "".$this->dovizKuruAlt()."";
		$alinacaklar['sip_adresno'] 		= "".$data['cari_adres']."";

		$veriler = array_merge($veriler, $alinacaklar);

		if (count($data['urunler']) > 0) {
			$satir_no = 0;
			foreach ($data['urunler'] as $urun_key => $urun_value) {
				$tekseferlik = $veriler;
				$urunler = array();
				$rec_no_alinan = $this->siparisRecnoAlalim();

				$urunler['sip_RECid_RECno'] 	= "".$rec_no_alinan."";
				$urunler['sip_stok_kod'] 		= "".$urun_value['stok_kod']."";
				$urunler['sip_b_fiyat'] 		= "".$urun_value['stok_fiyat']."";
				$urunler['sip_miktar'] 			= "".$urun_value['adet']."";
				$urunler['sip_tutar'] 			= "".$urun_value['stok_toplam']."";
				$urunler['sip_vergi'] 			= "".$urun_value['stok_vergi']."";
				$urunler['sip_satirno'] 		= "".$satir_no."";
				$urunler['sip_birim_pntr'] 		= "".$urun_value['stok_birim']."";
				$urunler['sip_harekettipi']		= "".$urun_value['stok_tip']."";


				$tekseferlik = array_merge($tekseferlik, $urunler);

				foreach ($tekseferlik as $key => $value) if (is_string($value)){
					$tekseferlik[$key] = "'".$value."'";
				}

				$sql = "insert into SIPARISLER (".implode(", ", array_keys($tekseferlik)).") VALUES(".implode(", ", array_values($tekseferlik)).")";
				$res = mssql_query($sql);

				$response['urunler'][$urun_key] 				= $urun_value;
				$response['urunler'][$urun_key]['status'] 		= $res;
				$response['urunler'][$urun_key]['rec_no'] 		= $rec_no_alinan;
				
				$satir_no++;
			}
		}
		$response['siparis_id_alinan'] 	= $siparis_id_alinan;
		$response['cari_kod']			= $data['cari_kod'];

		return $response;
	}
	/*Sipariş İşlemleri*/

}

?>
<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 10:41:43
         compiled from "temalar/tema/views/modules/products/comment/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11554077075f5a02e7d72cb3-22762135%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e4c627108626579eabd424a068b82c4fb2bf84b' => 
    array (
      0 => 'temalar/tema/views/modules/products/comment/index.tpl',
      1 => 1599655221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11554077075f5a02e7d72cb3-22762135',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'yorumlar' => 0,
    'yorum' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f5a02e7da46c7_97363273',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f5a02e7da46c7_97363273')) {function content_5f5a02e7da46c7_97363273($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Yorumlar </a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
							                <thead>
							                    <tr>                         
							                        <th class="islem_bilgilendirme" style="text-align:right;" align="right">
							                            
							                            <span class="btn btn-circle btn-icon-only btn-default durumu" title="Yorumu Göster veya Gösterme">
							                        		<i class=""></i>
							                    		</span>
							                            <span id="islembilgisitext"> Göster / Gösterme </span>

							                            <span href="#" title="Yorum Düzenle" class="btn btn-sm btn-default hizliduzenle">
							                                <i class="fa fa-pencil-square-o"></i>
							                            </span> 
							                            <span id="islembilgisitext"> Düzenle  </span>

							                            <span href="#" title="Sil" class="btn btn-sm btn-default sil">
							                                <i class="fa fa-times"></i>
							                            </span>
							                            <span id="islembilgisitext"> Sil </span>

							                        </th> 
							                    </tr>
							                </thead>
							            </table>

                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr> 
							                        <th width="1%"> ID </th>
							                        <th> Başlık </th> 
							                        <th> Ürün </th> 
							                        <th> Üye </th> 
							                        <th> Şehir </th>
							                        <th> Mail </th>
							                        <th> Evet </th>
							                        <th> Hayır </th>
							                        <th width="1%"> Tarih </th> 
							                        <th width="1%"> 
							                        	<a class="btn btn-circle btn-icon-only btn-default durumu" title="Yorumu Göster veya Gösterme">
							                        		<i class=""></i>
							                    		</a>  
							                		</th> 
													<th width="1%"> 
														<a href="#" title="Yorumu Düzenle" class="btn btn-sm btn-default hizliduzenle">
							                                <i class="fa fa-pencil-square-o"></i>
							                            </a>                                              	
													</th>
													<th width="1%"> 
														<a href="#" title="Cevap Ver" class="btn btn-sm btn-default detayliduzenle">
							                                <i class="fa fa-comment"></i>
							                            </a>                                              	
													</th>
							                        <th width="1%">
							                        	<a href="#" title="Sil" class="btn btn-sm btn-default sil">
							                                <i class="fa fa-times"></i>
							                            </a> 
							                        </th> 
							                    </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($_smarty_tpl->tpl_vars['yorumlar']->value)>0) {?>

                                                    <?php  $_smarty_tpl->tpl_vars['yorum'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['yorum']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['yorumlar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['yorum']->key => $_smarty_tpl->tpl_vars['yorum']->value) {
$_smarty_tpl->tpl_vars['yorum']->_loop = true;
?>
                                                        <tr>
                                                        	<td> <?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
 </td>
                                                        	<td title="<?php echo $_smarty_tpl->tpl_vars['yorum']->value->baslik;?>
"> <?php echo truncate_str($_smarty_tpl->tpl_vars['yorum']->value->baslik,50);?>
 </td>
                                                        	<td> <?php echo $_smarty_tpl->tpl_vars['yorum']->value->urun_adi;?>
 </td>
                                                        	<td title="<?php if ($_smarty_tpl->tpl_vars['yorum']->value->musteri) {
echo truncate_str((($_smarty_tpl->tpl_vars['yorum']->value->musteri_isim).(" ")).($_smarty_tpl->tpl_vars['yorum']->value->musteri_soyisim));
} else {
echo truncate_str((($_smarty_tpl->tpl_vars['yorum']->value->isim).(" ")).($_smarty_tpl->tpl_vars['yorum']->value->soyisim));
}?>"
															> <?php if ($_smarty_tpl->tpl_vars['yorum']->value->musteri) {
echo truncate_str((($_smarty_tpl->tpl_vars['yorum']->value->musteri_isim).(" ")).($_smarty_tpl->tpl_vars['yorum']->value->musteri_soyisim));
} else {
echo truncate_str((($_smarty_tpl->tpl_vars['yorum']->value->isim).(" ")).($_smarty_tpl->tpl_vars['yorum']->value->soyisim));
}?> </td>
                                                        	<td> <?php if ($_smarty_tpl->tpl_vars['yorum']->value->musteri) {
echo $_smarty_tpl->tpl_vars['yorum']->value->musteri_sehir;
} else {
echo $_smarty_tpl->tpl_vars['yorum']->value->sehir;
}?> </td>
                                                        	<td> <?php if ($_smarty_tpl->tpl_vars['yorum']->value->email) {
echo $_smarty_tpl->tpl_vars['yorum']->value->email;
} else { ?>-<?php }?></td>
                                                        	<td> <?php echo $_smarty_tpl->tpl_vars['yorum']->value->evet;?>
 </td>
                                                        	<td> <?php echo $_smarty_tpl->tpl_vars['yorum']->value->hayir;?>
 </td>
                                                        	<td> <?php echo date('d.m.Y',$_smarty_tpl->tpl_vars['yorum']->value->tarih);?>
 </td>
                                                        	<td>
                                                        		<a onclick="durum_degis(<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
, 'yorumlar', '#yorumdurumu<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
');" id="yorumdurumu<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['yorum']->value->durum==1) {?>durumu<?php } else { ?>beyaz<?php }?>" title="Göster / Gösterme">
                                                        			<i class=""></i>
                                                        		</a> 
                                                        	</td>
                                                        	<td>
                                                        		<a data-toggle="modal" href="#yorumduzenle_<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
			                                                        <i class="fa fa-pencil-square-o"></i>
			                                                    </a>                                                        
			                                                    <div class="modal fade" id="yorumduzenle_<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
			                                                        <div class="modal-dialog">
			                                                            <div class="modal-content">
			                                                                <div class="modal-header">
			                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                                                    <h4 class="modal-title">Yorum Düzenleme</h4>
			                                                                </div>
			                                                                <div class="modal-body">
			                                                                    <div class="tab-pane" id="tab_kategori_ekle">
			                                                                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/comment/edit');?>
/<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" autocomplete="off">
			                                                                            
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label">Başlık :
			                                                                                    <span class="required"> * </span>
			                                                                                </label>
			                                                                                <div class="col-md-8" id="kategori_adi">
			                                                                                    <input type="text" class="form-control" name="baslik" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['yorum']->value->baslik;?>
">                                            
			                                                                                </div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4 control-label">Açıklama : 
			                                                                            		<span class="required"> * </span>
			                                                                            	</label>
			                                                                            	<div class="col-md-8">
			                                                                            		<textarea class="form-control" name="aciklama" rows="5"><?php echo $_smarty_tpl->tpl_vars['yorum']->value->aciklama;?>
</textarea>
			                                                                            	</div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4 control-label">Puan : 
			                                                                            		<span class="required"> * </span>
			                                                                            	</label>
			                                                                            	<div class="col-md-2">
			                                                                            		<select name="puan" class="form-control">
			                                                                            			<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 5+1 - (1) : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
			                                                                            			<option <?php if ($_smarty_tpl->tpl_vars['yorum']->value->puan==$_smarty_tpl->tpl_vars['i']->value) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
			                                                                            			<?php }} ?>
			                                                                            		</select>
			                                                                            	</div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label"></label>
			                                                                                <div class="col-md-8" id="kategorilerimiz">         
			                                                                                    <button type="submit" class="btn blue">Düzenle</button> 
			                                                                                </div>
			                                                                            </div>

			                                                                        </form>
			                                                                    </div> 
			                                                                </div> 
			                                                            </div>
			                                                            <!-- /.modal-content -->
			                                                        </div>
			                                                        <!-- /.modal-dialog -->
			                                                    </div> 
                                                        	</td>
															<td>
																<a  data-toggle="modal" href="#yorumcevap_<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" title="Cevap Ver" class="btn btn-sm btn-default detayliduzenle">
																	<i class="fa fa-comment"></i>
																</a>           
																
																<div class="modal fade" id="yorumcevap_<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
			                                                        <div class="modal-dialog">
			                                                            <div class="modal-content">
			                                                                <div class="modal-header">
			                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                                                    <h4 class="modal-title">Yorum Cevap</h4>
			                                                                </div>
			                                                                <div class="modal-body">
			                                                                    <div class="tab-pane" id="tab_kategori_ekle">
			                                                                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/comment/answer');?>
/<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" autocomplete="off">
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4">Başlık :
			                                                                                </label>
			                                                                                <div class="col-md-8" id="kategori_adi">
																									<?php echo $_smarty_tpl->tpl_vars['yorum']->value->baslik;?>
																							
			                                                                                </div>
			                                                                            </div>

			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Açıklama : 
			                                                                            	</label>
			                                                                            	<div class="col-md-8" style="white-space: normal; ">
			                                                                            		<?php echo $_smarty_tpl->tpl_vars['yorum']->value->aciklama;?>

			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Puan : 
			                                                                            	</label>
			                                                                            	<div class="col-md-2">
																								<?php echo $_smarty_tpl->tpl_vars['yorum']->value->puan;?>

			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                            	<label class="col-md-4">Cevap : 
			                                                                            	</label>
			                                                                            	<div class="col-md-8">
			                                                                            		<textarea class="form-control" name="cevap" rows="5"></textarea>
			                                                                            	</div>
			                                                                            </div>
			                                                                            <div class="form-group">
			                                                                                <label class="col-md-4 control-label"></label>
			                                                                                <div class="col-md-8" id="kategorilerimiz">         
			                                                                                    <button type="submit" class="btn blue">Gönder</button> 
			                                                                                </div>
			                                                                            </div>

			                                                                        </form>
			                                                                    </div> 
			                                                                </div> 
			                                                            </div>
			                                                            <!-- /.modal-content -->
			                                                        </div>
			                                                        <!-- /.modal-dialog -->
			                                                    </div> 
															</td>
                                                        	<td>
                                                        		<a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="<?php echo site_url('products/comment/remove');?>
/<?php echo $_smarty_tpl->tpl_vars['yorum']->value->id;?>
" title="Sil" class="btn btn-sm btn-default sil">
			                                                        <i class="fa fa-times-circle"></i>
			                                                    </a>
                                                        	</td>
                                                        </tr>
                                                    <?php } ?>

                                                <?php } else { ?>
                                                <tr>
                                                    <td colspan='11' style='text-align:center;'> Ürünlere yapılmış bir yorum bulunamadı. </td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 10:50:47
         compiled from "temalar/tema/views/modules/products/marker/modernmarkeredit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13924945925f5a0459738457-37103789%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a1312e6fe910b264bf94960a551cd022a8132fa5' => 
    array (
      0 => 'temalar/tema/views/modules/products/marker/modernmarkeredit.tpl',
      1 => 1599735046,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13924945925f5a0459738457-37103789',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f5a0459743ca7_76807146',
  'variables' => 
  array (
    'saved' => 0,
    'etiket' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f5a0459743ca7_76807146')) {function content_5f5a0459743ca7_76807146($_smarty_tpl) {?><!DOCTYPE html> 
<html lang="tr">
<head>
    <meta charset="utf-8" />
    <title>Lastikcim</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/tinymce@5.1.5/tinymce.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="/temalar/tema/assets/apps/plugins/tinymce/plugin/plugin.js"><?php echo '</script'; ?>
>
    <link rel="shortcut icon" href="favicon.ico" /> 
</head>
<body class="">
<div class="container-fluid">
    <div class="page-content">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                        <?php if (isset($_smarty_tpl->tpl_vars['saved']->value)) {?>
                        <div class="alert alert-primary">
                            <?php echo $_smarty_tpl->tpl_vars['saved']->value;?>

                        </div>
                        <?php }?>
                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/marker/modernedit');?>
/<?php echo $_smarty_tpl->tpl_vars['etiket']->value->id;?>
">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $_smarty_tpl->tpl_vars['etiket']->value->adi;?>

                                    <span class="required"> * </span>
                                </label>
                            </div>       
                            <div class="form-group">
                                <label class="col-md-3 control-label">Etiket Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="tinymce form-control" name="etiket_aciklama"><?php echo $_smarty_tpl->tpl_vars['etiket']->value->etiket_aciklama;?>
</textarea>
                                </div>
                            </div>   

                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn btn-primary blue">Düzenle</button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
     <!-- JS, Popper.js, and jQuery -->
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://kit.fontawesome.com/dc840779d0.js" crossorigin="anonymous"><?php echo '</script'; ?>
>
    
    <?php echo '<script'; ?>
 type="text/javascript">
        var tbpKey = 'O/2t0LY/m1CDEzh3C9heyCxWD05rhPmQi8mB/0revfYU98KfzI+Wespvcgixm6oG1+MJzX46YgvYd8zs0xJlCe8CHAKDni4nQopeRSkJw7g=';
        var base_url = location.protocol + '//' + location.host + '/';
        tinymce.init({
            selector: 'textarea.tinymce',
            plugins: 'advlist autolink bootstrap link image lists charmap print preview help table code',
            toolbar: [
            'undo redo | bootstrap',
            'cut copy paste | styleselect | alignleft aligncenter alignright alignjustify | bold italic | link image | preview | tools code | help'],
            contextmenu: "link image imagetools table spellchecker | bootstrap",
            file_picker_types: 'file image media',
            bootstrapConfig: {
                url: base_url + '/temalar/tema/assets/apps/plugins/tinymce/plugin/',
                iconFont: 'fontawesome5',
                imagesPath: '/uploads/images/',
                key: tbpKey
            },
            formats: {
            alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-left'},
            aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-center'},
            alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-right'},
            alignjustify: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'text-justify'},
            bold: {inline : 'strong'},
            italic: {inline : 'em'},
            underline: {inline : 'u'},
            sup: {inline : 'sup'},
            sub: {inline : 'sub'},
            strikethrough: {inline : 'del'}
            },
            style_formats_autohide: true,
            height : "380",
            images_upload_url : '/home/uploadImage',
            images_upload_base_path: '/uploads/images',
            automatic_uploads : false,
        });
    <?php echo '</script'; ?>
>
    
</body>
</html><?php }} ?>

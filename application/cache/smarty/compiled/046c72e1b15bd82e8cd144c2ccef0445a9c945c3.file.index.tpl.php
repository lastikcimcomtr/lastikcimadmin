<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:45:16
         compiled from "temalar/tema/views/modules/products/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6524862035f58dc6c2ea626-28058380%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '046c72e1b15bd82e8cd144c2ccef0445a9c945c3' => 
    array (
      0 => 'temalar/tema/views/modules/products/index.tpl',
      1 => 1599655043,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6524862035f58dc6c2ea626-28058380',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tip' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
    'urunler' => 0,
    'urun' => 0,
    'this' => 0,
    'etiket' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dc6c31f6f6_76058101',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dc6c31f6f6_76058101')) {function content_5f58dc6c31f6f6_76058101($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-search"></i>Ürün Ara
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="expand" data-original-title="Aç / Kapat"> </a>
                        </div>
                    </div>
                    <div class="portlet-body tabs-below">
                        <form class="form-horizontal form-row-seperated" method="post"
                              action="<?php echo site_url('products/index');?>
">
                            <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
"/>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Kategori</label>
                                    <div class="col-md-6" id="kategorilerimiz">
                                        <select id="kategori" name="kategori" data-placeholder="Kategoriler"
                                                onchange="kategoriler(1, '<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
', 'kategori')"
                                                class="form-control form-filter input-sm select2me">
                                            <option value="">Seçiniz</option>
                                            <?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</option>
                                            <?php } ?>
                                        </select>
                                        <div id="alt_kategori"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Ürün Adı</label>
                                    <div class="col-md-6" id="kategorilerimiz">
                                        <input type="text" class="form-control" autocomplete="off" name="urun_adi"
                                               placeholder="" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Stok Kodu</label>
                                    <div class="col-md-6" id="kategorilerimiz">
                                        <input type="text" class="form-control" autocomplete="off" name="stok_kodu"
                                               placeholder="" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-1 control-label"></label>
                                        <div class="col-md-10" id="kategorilerimiz">
                                            <button type="submit" class="btn blue">Ürün Ara</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="islem_bilgilendirme">
                                    <span class="btn btn-circle btn-icon-only btn-default durumu"></span>
                                    <span id="islembilgisitext"> Durumu </span>

                                    <span class="btn btn-circle btn-icon-only btn-default anasayfavitrini"></span>
                                    <span id="islembilgisitext"> Anasayfa Vitrini </span>

                                    <span class="btn btn-circle btn-icon-only btn-default kampanyaliurun"></span>
                                    <span id="islembilgisitext"> Kampanyalı Ürün </span>

                                    <span class="btn btn-circle btn-icon-only btn-default kategorivitrini"></span>
                                    <span id="islembilgisitext"> Kategori Vitrini </span>

                                    <span class="btn btn-circle btn-icon-only btn-default markavitrini"></span>
                                    <span id="islembilgisitext"> Marka Vitrini </span>

                                    <span  class="btn btn-circle btn-icon-only btn-default gununurunu"></span>
                                    <span id="islembilgisitext"> Günün Ürünü </span>

                                    

                                    <span class="btn btn-sm btn-default hizliduzenle"><i
                                                class="fa fa-pencil"></i></span>
                                    <span id="islembilgisitext"> Hızlı Düzenle </span>

                                    <span class="btn btn-sm btn-default detayliduzenle"><i
                                                class="fa fa-pencil-square-o"></i></span>
                                    <span id="islembilgisitext"> Detaylı Düzenle </span>

                                    <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>
                                    <span id="islembilgisitext"> Sil </span>

                                </th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th width="1%"><input type="checkbox"></th>
                                    <th width="1%"> Resim</th>
                                    <th width="1%"> ID</th>
                                    <th> Ürün adı</th>
                                    <th> Stok Kodu</th>
                                    <th> Fiyat</th>
                                    <th> Güncelleme</th>
                                    <th> Kategori</th>
                                    <th width="1%">
                                        <span class="btn btn-circle btn-icon-only btn-default durumu"
                                              title="Durumu"></span>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default anasayfavitrini"
                                           title="Anasayfa Vitrini"></a>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default kampanyaliurun"
                                           title="Kampanyalı Ürün"></a>
                                    </th>
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default kategorivitrini"
                                           title="Kategori Vitrini"></a>
                                    </th>

                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default markavitrini"
                                           title="Marka Vitrini"></a>
                                    </th> 
                                    <th width="1%">
                                        <a class="btn btn-circle btn-icon-only btn-default gununurunu" title="Günü Ürünü"></a> 
                                    </th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                    <th width="1%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  $_smarty_tpl->tpl_vars['urun'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['urun']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['urunler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['urun']->key => $_smarty_tpl->tpl_vars['urun']->value) {
$_smarty_tpl->tpl_vars['urun']->_loop = true;
?>
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>
                                            <?php if ($_smarty_tpl->tpl_vars['urun']->value->resim_1) {?>
                                                <a href="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['urun']->value->resim_1;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('urun_resim',$_tmp1);?>
"
                                                   rel="popupacil" target="_blank">
                                                    <img rel="popupacil" border="0" width="30" height="20"
                                                         src="<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['urun']->value->resim_1;?>
<?php $_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('urun_resim',$_tmp2);?>
">
                                                </a>
                                            <?php } else { ?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('urun_resim','resim_yok.jpg');?>
"
                                                   rel="popupacil" target="_blank">
                                                    <img rel="popupacil" border="0" width="30" height="20"
                                                         src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('urun_resim','resim_yok.jpg');?>
">
                                                </a>
                                            <?php }?>
                                        </td>
                                        <td> <?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
 </td>
                                        <td> <?php echo $_smarty_tpl->tpl_vars['urun']->value->urun_adi;?>
  </td>
                                        <td> <?php echo $_smarty_tpl->tpl_vars['urun']->value->stok_kodu;?>
 </td>
                                        <td> <?php echo fiyatver($_smarty_tpl->tpl_vars['urun']->value->fiyat,$_smarty_tpl->tpl_vars['urun']->value->fiyat_birim);?>
 </td>
                                        <td> <?php echo date('d.m.Y H:i:s',$_smarty_tpl->tpl_vars['urun']->value->apizaman);?>
 </td>
                                        <td> <?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
 </td>
                                        <td id="durum_guncelle">
                                            <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'durumu', <?php echo $_smarty_tpl->tpl_vars['urun']->value->durumu;?>
,'<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'durumu');"
                                               id="durumu_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->durumu==1) {?>durumu<?php } else { ?>beyaz<?php }?>"
                                               title="Durumu"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'anasayfa_vitrini', <?php echo $_smarty_tpl->tpl_vars['urun']->value->anasayfa_vitrini;?>
, '<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'anasayfavitrini');"
                                               id="anasayfa_vitrini_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->anasayfa_vitrini==1) {?>anasayfavitrini<?php } else { ?>beyaz<?php }?>"
                                               title="Anasayfa Vitrini"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'kapmanyali_urunler', <?php echo $_smarty_tpl->tpl_vars['urun']->value->kapmanyali_urunler;?>
,'<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'kampanyaliurun');"
                                               id="kapmanyali_urunler_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->kapmanyali_urunler==1) {?>kampanyaliurun<?php } else { ?>beyaz<?php }?>"
                                               title="Kampanyalı Ürün"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'kategori_vitrini', <?php echo $_smarty_tpl->tpl_vars['urun']->value->kategori_vitrini;?>
,'<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'kategorivitrini');"
                                               id="kategori_vitrini_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->kategori_vitrini==1) {?>kategorivitrini<?php } else { ?>beyaz<?php }?>"
                                               title="Kategori Vitrini"></a>
                                        </td>
                                        <td>
                                            <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'marka_vitrini', <?php echo $_smarty_tpl->tpl_vars['urun']->value->marka_vitrini;?>
,'<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'markavitrini');"
                                               id="marka_vitrini_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->marka_vitrini==1) {?>markavitrini<?php } else { ?>beyaz<?php }?>"
                                               title="Marka Vitrini"></a>
                                        </td>

                                         <td>
                                             <a onclick="urunparametreguncelle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
, 'gunun_urunu', <?php echo $_smarty_tpl->tpl_vars['urun']->value->gunun_urunu;?>
,'<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
', 'gununurunu');" id="gunun_urunu_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['urun']->value->gunun_urunu==1) {?>gununurunu<?php } else { ?>beyaz<?php }?>" title="Günün Ürünü"></a>
                                         </td>
                                        
                                        <td>
                                            <a class="btn btn-sm btn-default benzerurun" title="Benzer Ürün Ekle"
                                               onclick="return confirm('Belirttiğiniz ürün için bir kopya ürün oluşturulacaktır. Oluşturulacak sayfada yapacağınız değişiklikler ile sisteme yeni bir ürün kaydedilecektir.');"
                                               href="">
                                                <i class="fa fa-plus-square"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-default hizliduzenle" title="Hızlı Düzenle"
                                               data-toggle="modal" href="#hizli_duzenle">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <div class="modal fade" id="hizli_duzenle" tabindex="-1" role="basic"
                                                 aria-hidden="true">
                                                <form method="post" action="javascript:void(0);"
                                                      id="hizli_duzenle_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                                      onsubmit="hizliduzenle(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
);" autocomplete="off">
                                                    <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['urun']->value->tip;?>
"/>
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Hızlı Düzenle</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form>
                                                                    <div class="form-body">
                                                                        <div id="form_cevap_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"></div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-4 control-label">
                                                                                Ürün Adı
                                                                            </label>
                                                                            <div class="col-md-8" id="kategorilerimiz"
                                                                                 style="margin-top:5px;">
                                                                                <input type="text" class="form-control"
                                                                                       name="urun_adi"
                                                                                       value="<?php echo $_smarty_tpl->tpl_vars['urun']->value->urun_adi;?>
"/>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </form>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="modal-footer"
                                                                 style="border-top: 0px;padding-top: 0px;">
                                                                <button type="button" class="btn dark btn-outline"
                                                                        data-dismiss="modal">Kapat
                                                                </button>
                                                                <button type="submit" class="btn green"
                                                                        style="background-color: #f5861d;border-color: #f5861d;">
                                                                    Kaydet
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                </form>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-default hizliduzenle" title="Etiketler" id="etiketler_btn_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"
                                               data-toggle="modal" onclick="init_select2(this);" href="#etiketler_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
">
                                                <i class="fa fa-tags"></i>
                                            </a>
                                            <div class="modal fade" id="etiketler_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" role="basic" aria-hidden="true">
                                                <form method="post" action="javascript:void(0);" onsubmit="save_etiketler(<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
,this)"
                                                      id="etiketler_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" autocomplete="off">
                                                    <input type="hidden" name="urun_id" value="<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Etiketler</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                    <div class="form-body">
                                                                        <div id="form_cevap_<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
"></div>
                                                                        <div class="form-group"
                                                                             style="display: inline-block;width: 100%">
                                                                            <label class="col-md-4 control-label font-lg"
                                                                                   style="margin-top:5px">
                                                                                Etiket Adı
                                                                            </label>
                                                                            <div id="kategorilerimiz" class="col-md-7">
                                                                                <select name="etiket_adi"
                                                                                        class="form-control etiket_select2">
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-1"
                                                                                 style="padding-left: 0">
                                                                                <button type="button" onclick="etiket_ekle(this)" class="btn btn-md btn-primary">
                                                                                    Ekle
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="list list-unstyled font-lg">
                                                                            <?php  $_smarty_tpl->tpl_vars['etiket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['etiket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->products->urun_etiketler($_smarty_tpl->tpl_vars['urun']->value->stok_kodu); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['etiket']->key => $_smarty_tpl->tpl_vars['etiket']->value) {
$_smarty_tpl->tpl_vars['etiket']->_loop = true;
?>
                                                                                <li class="item"
                                                                                    style="border-bottom:1px dimgrey solid">
                                                                                    <?php echo $_smarty_tpl->tpl_vars['etiket']->value->adi;?>

                                                                                    <button onclick="$(this).closest('li').remove();" style="margin-top:3px"
                                                                                            class="btn btn-danger btn-xs pull-right">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </button>
                                                                                    <input type="hidden"  name="etiket_id[]" value="<?php echo $_smarty_tpl->tpl_vars['etiket']->value->id;?>
">
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                        
                                                                    </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="modal-footer"
                                                                 style="border-top: 0px;padding-top: 0px;">
                                                                <button type="button" class="btn dark btn-outline"
                                                                        data-dismiss="modal">Kapat
                                                                </button>
                                                                <button type="button" onclick="save_etiketler(this)" class="btn blue"
                                                                        style="background-color: #f5861d;border-color: #f5861d;"
                                                                        data-dismiss="modal">Kaydet
                                                                </button>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                </form>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('products/edit');?>
/<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" title="Detaylı Düzenle"
                                               class="btn btn-sm btn-default detayliduzenle">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <a target="_blank" href="<?php echo site_url('products/copyproduct');?>
/<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" title="Ürün Kopyala"
                                               class="btn btn-sm btn-default detayliduzenle">
                                                <i class="fa fa-copy"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a onclick="return confirm('Silmek istediğinizden emin misiniz?');"
                                               href="<?php echo site_url('products/remove');?>
/<?php echo $_smarty_tpl->tpl_vars['urun']->value->id;?>
" title="Sil"
                                               class="btn btn-sm btn-default sil">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php echo $_smarty_tpl->tpl_vars['this']->value->pagination->create_links();?>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <?php echo '<script'; ?>
>

        <?php echo '</script'; ?>
>
        <!-- END PAGE BASE CONTENT -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 10:42:41
         compiled from "temalar/tema/views/modules/products/marker/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18545781115f5a0321bbb552-98297596%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9522635e08382e20e27fd7fff57d3bb2ce5fb350' => 
    array (
      0 => 'temalar/tema/views/modules/products/marker/index.tpl',
      1 => 1599732791,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18545781115f5a0321bbb552-98297596',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'ara' => 0,
    'etiketler' => 0,
    'etiket' => 0,
    'this' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f5a0321bd7746_88998868',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f5a0321bd7746_88998868')) {function content_5f5a0321bd7746_88998868($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <div class="row">

            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">

                      <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th  style="border-right: 0">

                                  <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="<?php echo site_url('products/marker/add');?>
">
                                    <i class="fa fa-plus"></i>  
                                    Etiket Ekle 
                                </a>

                            </th> 
                            <th><form method="get" style="display: inline-flex"><input style="width: 140px" class="form-control" name="etiket-ara" value="<?php echo $_smarty_tpl->tpl_vars['ara']->value;?>
" />
                                <button class="btn btn-primary">Ara</button></form></th>
                            <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
<span class="btn btn-circle btn-icon-only btn-default durumu"></span>
                                 <span id="islembilgisitext">Acıklaması Var</span>   
                                 <span class="btn btn-circle btn-icon-only btn-default beyaz"></span>
                                 <span id="islembilgisitext">Acıklaması Yok</span>   
                             <span class="btn btn-sm btn-default detayliduzenle"><i class="fa fa-pencil-square-o"></i></span>      
                             <span id="islembilgisitext">Düzenle</span>

                         </th>
                     </tr>
                 </thead>
             </table>

             <div class="portlet-body">
                <div class="tabbable-bordered">

                    <div class="tab-content">                               
                        <div class="tab-pane active" id="tab_kategoriler">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="1%">id</th> 
                                            <th width="48%">Etiket</th> 
                                            <th width="48%">Url </th> 
                                            <th width="1%"></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($_smarty_tpl->tpl_vars['etiketler']->value)>0) {?>

                                        <?php  $_smarty_tpl->tpl_vars['etiket'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['etiket']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['etiketler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['etiket']->key => $_smarty_tpl->tpl_vars['etiket']->value) {
$_smarty_tpl->tpl_vars['etiket']->_loop = true;
?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $_smarty_tpl->tpl_vars['etiket']->value->id;?>
</td> 
                                            <td ><?php echo $_smarty_tpl->tpl_vars['etiket']->value->adi;?>
</td> 
                                            <td ><?php echo $_smarty_tpl->tpl_vars['etiket']->value->sef_url;?>
</td> 
                                            <td>
                                                <span class="btn btn-circle btn-icon-only btn-default <?php if (preg_match_all('/[^\s]/u',$_smarty_tpl->tpl_vars['etiket']->value->etiket_aciklama, $tmp)>20) {?>durumu<?php } else { ?>beyaz<?php }?>"></span>
                                             <a href="<?php echo site_url(('products/marker/modernedit/').($_smarty_tpl->tpl_vars['etiket']->value->id));?>
"  style="background:green;" class="btn btn-sm btn-default detayliduzenle" title="Düzenle" target="_blank" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>

                                               <a href="<?php echo site_url(('products/marker/edit/').($_smarty_tpl->tpl_vars['etiket']->value->id));?>
"   class="btn btn-sm btn-default detayliduzenle" title="Düzenle" target="_blank" >
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>

                                        </tr>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <tr>
                                            <td colspan='10' style='text-align:center;'>Veri Bulunamadı </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                                <?php echo $_smarty_tpl->tpl_vars['this']->value->pagination->create_links();?>

                            </div> 
                        </div>                             
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->



</div>
</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

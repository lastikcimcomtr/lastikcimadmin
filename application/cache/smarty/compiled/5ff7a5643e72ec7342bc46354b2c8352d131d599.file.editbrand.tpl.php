<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 14:34:22
         compiled from "temalar/tema/views/modules/products/brand/editbrand.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10088468675f58e7ee64a863-03206454%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ff7a5643e72ec7342bc46354b2c8352d131d599' => 
    array (
      0 => 'temalar/tema/views/modules/products/brand/editbrand.tpl',
      1 => 1599655222,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10088468675f58e7ee64a863-03206454',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'marka' => 0,
    'tipler' => 0,
    'tip' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
    'active_categories' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58e7ee66f7e9_91363777',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58e7ee66f7e9_91363777')) {function content_5f58e7ee66f7e9_91363777($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-body">
                                            <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/brand/editbrand');?>
/<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Marka Adı:
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3" id="kategori_adi">
                                                        <input type="text" class="form-control" name="adi" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['marka']->value->adi;?>
">                                            
                                                    </div>
                                                </div>
                                                <?php if ($_smarty_tpl->tpl_vars['marka']->value->tip) {?>
                                                <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['marka']->value->tip;?>
" />
                                                <?php } else { ?>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Marka Tipi
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select name="tip" class="form-control" required>
                                                            <option value="">Seçiniz</option>
                                                            <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                                            <option <?php if ($_smarty_tpl->tpl_vars['marka']->value->tip==$_smarty_tpl->tpl_vars['tip']->value->adi) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?php }?>          
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Sıra No:
                                                        <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-3" id="">
                                                        <input type="number" class="form-control" name="sira" min="1" value="<?php echo $_smarty_tpl->tpl_vars['marka']->value->sira;?>
">                                            
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Marka Açıklama:
                                                    </label>
                                                    <div class="col-md-9" style="max-width: 850px;">
                                                        <textarea  class="ckeditor form-control" name="marka_aciklama" row="6"><?php echo $_smarty_tpl->tpl_vars['marka']->value->marka_aciklama;?>
</textarea>
                                                    </div>
                                                </div>                                                                                  

												<div class="form-group">
													<label class="col-md-3 control-label">Marka Etiket:
														<span> </span>
													</label>
													<div class="col-md-3">
														<input type="text" class="form-control" name="keywords" placeholder="Marka Etiketi" value="<?php echo $_smarty_tpl->tpl_vars['marka']->value->keywords;?>
">                                            
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        Aktif Kategoriler
                                                    </label>
                                                    <div class="col-md-3">
                                                        <select name="kategoriler[]" class="form-control multiple" multiple="multiple" required>
                                                            <option value="">Seçiniz</option>
                                                            <?php $_smarty_tpl->tpl_vars['active_categories'] = new Smarty_variable(explode(',',$_smarty_tpl->tpl_vars['marka']->value->kategoriler), null, 0);?>
                                                            <?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
                                                                <option value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
" <?php if (in_array($_smarty_tpl->tpl_vars['kategori']->value->adi,$_smarty_tpl->tpl_vars['active_categories']->value)) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label"></label>
                                                    <div class="col-md-9" id="kategorilerimiz">         
                                                        <button type="submit" class="btn blue">Düzenle</button> 
                                                    </div>
                                                </div>
                                            </form>
                                      

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

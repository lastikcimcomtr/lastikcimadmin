<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 09:28:18
         compiled from "temalar/tema/views/modules/products/category/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18729101785f59f1b29c55e3-83259386%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '94d44906dc416652ea891d9e6c32eb478f021f92' => 
    array (
      0 => 'temalar/tema/views/modules/products/category/index.tpl',
      1 => 1599655221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18729101785f59f1b29c55e3-83259386',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'kategorisi' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
    'this' => 0,
    'fiyat' => 0,
    'parametreler' => 0,
    'i' => 0,
    'grupAdi' => 0,
    'parametreGrup' => 0,
    'parametre' => 0,
    'tipler' => 0,
    'tip' => 0,
    'ust' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f59f1b2a35d23_75534145',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f59f1b2a35d23_75534145')) {function content_5f59f1b2a35d23_75534145($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>



<style type="text/css">
.editable-input input {
    height: 34px;
    width: 100px!important;
}
</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Kategoriler</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('/');?>
">Anasayfa</a>
                </li>
                <li>
                    <a href="<?php echo site_url('products/index');?>
">Ürünler</a>
                </li>
                <li class="active">Kategoriler</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut kategorileri görebilir, Yeni kategori ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#kategori_ekle">
                            <i class="fa fa-plus"></i>  
                            Kategori Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Kategoriler <?php if ($_smarty_tpl->tpl_vars['kategorisi']->value) {?> - <?php echo $_smarty_tpl->tpl_vars['kategorisi']->value->adi;
}?></a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="60%"> Kategori Adı </th>
                                                    <th width="1%"> Alt </th>
                                                    <th width="10%"> Kargo Ücretleri </th>
                                                    <th width="8%"> <center>Alt Kategoriler</center> </th>                                                    
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Kategori Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="8%"> <center>Ürünler</center> </th>
                                                    <th width="8%"> <center>Parametreler</center> </th>
                                                    <th width="8%"> <center>Vitrin Düzenle</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($_smarty_tpl->tpl_vars['kategoriler']->value)>0) {?>

                                                <?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
                                                <tr>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
</td> 
                                                    <td align="center"><?php echo $_smarty_tpl->tpl_vars['kategori']->value->sira;?>
</td> 
                                                    <td><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['kategori']->value->altkatsayisi;?>
</td>
                                                    <td>                                                       
                                                        <a data-toggle="modal" href="#kategori_kargo_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
"  class="btn btn-sm red btn-outline sbold bizimbutonlar">Kargo Fiyatı Ayarla</a> 
                                                                <div class="modal fade" id="kategori_kargo_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title"><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
 Kategorisi Kargo Fiyatları</h4>
                                                                                </div>
                                                                            <table class="table table-striped table-hover table-bordered">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="width:30%;"><?php if ($_smarty_tpl->tpl_vars['kategori']->value->tip=="lastik") {?>Jant Çapı<?php } elseif ($_smarty_tpl->tpl_vars['kategori']->value->tip=="jant") {?>Ebat<?php } else { ?>Ürün Tipi<?php }?></th> 
                                                                                        <th style="width:30%;"> Fiyatı </th> 
                                                                                        <th style="width:1%;"> </th> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody> 

                                                                                    <?php  $_smarty_tpl->tpl_vars['fiyat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['fiyat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['this']->value->category->kargofiyatlari($_smarty_tpl->tpl_vars['kategori']->value->id); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['fiyat']->key => $_smarty_tpl->tpl_vars['fiyat']->value) {
$_smarty_tpl->tpl_vars['fiyat']->_loop = true;
?>
                                                                                        <tr class="parametre-1">
                                                                                            <td style="vertical-align: middle;">
                                                                                                <?php if ($_smarty_tpl->tpl_vars['kategori']->value->tip=="mini-stepne") {?>
                                                                                                <?php if ($_smarty_tpl->tpl_vars['fiyat']->value->deger==1) {?>Yol Seti<?php } else { ?>Mini Stepne<?php }?>
                                                                                                <?php } else { ?>
                                                                                                <?php echo $_smarty_tpl->tpl_vars['fiyat']->value->deger;?>

                                                                                                <?php }?>                                                                                  
                                                                                            </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <a href="#" class="inlineedit" id="<?php echo $_smarty_tpl->tpl_vars['fiyat']->value->deger;?>
" data-type="number"  data-emptytext="0" data-pk="<?php echo $_smarty_tpl->tpl_vars['fiyat']->value->veriler;?>
" data-pk="<?php echo $_smarty_tpl->tpl_vars['fiyat']->value->deger;?>
" data-url="<?php echo site_url(('ajax/kargofiyati/').($_smarty_tpl->tpl_vars['kategori']->value->id));?>
" data-title="Fiyatı">
                                                                                             <?php echo $_smarty_tpl->tpl_vars['fiyat']->value->fiyat;?>
</a>
                                                                                            <i class="fa fa-try"></i> 
                                                                                        </td> 

                                                                                        <td style="vertical-align: middle;text-align: center;">
                                                                                           
                                                                                        </td>  
                                                                                    </tr>
                                                                                    <?php } ?>
                                                                                    
                                                                                </tbody>
                                                                        </table>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                                
                                                                <a href="<?php echo site_url('products/category/index');?>
/<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" class="btn btn-sm red btn-outline sbold bizimbutonlar">Alt Kategoriler</a>
                                                            </td>                                                            
                                                            <td>
                                                                <a onclick="durum_degis(<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
,'kategoriler', '#kategoridurumu<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
');" id="kategoridurumu<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['kategori']->value->durum==1) {?>durumu<?php } else { ?>beyaz<?php }?>" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#kategor_resmi_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="kategor_resmi_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Kategori Resmi Ekleme ( 416 x 122)</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                             <form method="post" enctype="multipart/form-data" action="<?php echo site_url('products/category/editcategoryimage');?>
/<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
">
                                                                              <div class="form-group">						                                                            
                                                                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 416px; height: 122px;">
                                                                                       <?php if ($_smarty_tpl->tpl_vars['kategori']->value->resim) {?>
                                                                                       <img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('kategori_resim',$_smarty_tpl->tpl_vars['kategori']->value->resim);?>
" alt="" />
                                                                                       <?php } else { ?>
                                                                                       <img src="http://www.placehold.it/416x122/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                       <?php }?>
                                                                                   </div>
                                                                                   <div>
                                                                                      <span class="btn red btn-outline btn-file">
                                                                                          <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                          <span class="fileinput-exists"> Değiştir </span>
                                                                                          <input type="file" name="resim"> </span>
                                                                                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                      </div>
                                                                                  </div>
                                                                              </div>
                                                                              <div class="form-group">
                                                                                <button type="submit" class="btn blue">Kaydet</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    </td>
                                                    <td align="center">                                                 
                                                        <a href="#urunler" class="btn red btn-outline sbold bizimbutonlar" title="Ürünler">
                                                            Ürünler
                                                        </a>
                                                    </td>     
                                                    <td align="center">   
                                                        <?php if ($_smarty_tpl->tpl_vars['kategori']->value->altkatsayisi==0) {?>                                              
                                                        <a data-toggle="modal" href="#parametreler_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
"  class="btn red btn-outline sbold bizimbutonlar" title="Parametreler">
                                                            Parametreler
                                                        </a>
                                                        <div class="modal" id="parametreler_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog  modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title">Parametre Düzenleme</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="tab-pane" id="tab_kategori_ekle">
                                                                            <form method="post" action="javascript:void(-1);" id="kpd_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
">
                                                                                <div class="tabbable-bordered">
                                                                                    <ul class="nav nav-tabs" id="tab_basliklari">
                                                                                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                                                                        <?php  $_smarty_tpl->tpl_vars['parametreGrup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['parametreGrup']->_loop = false;
 $_smarty_tpl->tpl_vars['grupAdi'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['parametreler']->value[$_smarty_tpl->tpl_vars['kategori']->value->tip]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['parametreGrup']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['parametreGrup']->key => $_smarty_tpl->tpl_vars['parametreGrup']->value) {
$_smarty_tpl->tpl_vars['parametreGrup']->_loop = true;
 $_smarty_tpl->tpl_vars['grupAdi']->value = $_smarty_tpl->tpl_vars['parametreGrup']->key;
 $_smarty_tpl->tpl_vars['parametreGrup']->index++;
 $_smarty_tpl->tpl_vars['parametreGrup']->first = $_smarty_tpl->tpl_vars['parametreGrup']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['first'] = $_smarty_tpl->tpl_vars['parametreGrup']->first;
?>
                                                                                        <li class="<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['pgrup']['first']) {?>active<?php }?>">
                                                                                            <a href="#tab_parametre_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" data-toggle="tab"> <?php echo $_smarty_tpl->tpl_vars['grupAdi']->value;?>
 </a>
                                                                                        </li>
                                                                                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                                                                        <?php } ?>
                                                                                    </ul>      
                                                                                    <div class="tab-content">
                                                                                       <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                                                                       <?php  $_smarty_tpl->tpl_vars['parametreGrup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['parametreGrup']->_loop = false;
 $_smarty_tpl->tpl_vars['grupAdi'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['parametreler']->value[$_smarty_tpl->tpl_vars['kategori']->value->tip]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['parametreGrup']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['parametreGrup']->key => $_smarty_tpl->tpl_vars['parametreGrup']->value) {
$_smarty_tpl->tpl_vars['parametreGrup']->_loop = true;
 $_smarty_tpl->tpl_vars['grupAdi']->value = $_smarty_tpl->tpl_vars['parametreGrup']->key;
 $_smarty_tpl->tpl_vars['parametreGrup']->index++;
 $_smarty_tpl->tpl_vars['parametreGrup']->first = $_smarty_tpl->tpl_vars['parametreGrup']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['first'] = $_smarty_tpl->tpl_vars['parametreGrup']->first;
?>
                                                                                       <div class="tab-pane <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['pgrup']['first']) {?>active<?php }?> row" id="tab_parametre_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
_<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"> 
                                                                                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                                                                        <?php  $_smarty_tpl->tpl_vars['parametre'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['parametre']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['parametreGrup']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['parametre']->key => $_smarty_tpl->tpl_vars['parametre']->value) {
$_smarty_tpl->tpl_vars['parametre']->_loop = true;
?>
                                                                                        <label class="col-md-4 control-label" style="text-align: left;"><input type="checkbox" <?php if (strpos($_smarty_tpl->tpl_vars['parametre']->value->categories,(('|').($_smarty_tpl->tpl_vars['kategori']->value->id)).('|'))!==false) {?> checked="true" <?php }?> name="parametre[]" value="<?php echo $_smarty_tpl->tpl_vars['parametre']->value->id;?>
"> <?php echo $_smarty_tpl->tpl_vars['parametre']->value->adi;?>
</label>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                    <?php } ?>                                                                                         
                                                                                </div>

                                                                            </div> 
                                                                            <div class="row">
                                                                            	<div class="col-md-12" style="text-align: right;margin-top: 10px;">
                                                                            		<button onclick="parametreDuzenle('kpd_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
' ,<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
)" class="btn blue">Düzenle</button>
                                                                            	</div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div> 
                                                    </div> 

                                                    <?php } else { ?>
                                                    -
                                                    <?php }?>
                                                </td>
                                                <td align="center">                                                 
                                                    <a href="#Vitrin" class="btn red btn-outline sbold bizimbutonlar" title="Vitrin Düzenle">
                                                        Vitrin Düzenle
                                                    </a>
                                                </td>
                                                <td>  
                                                    <a data-toggle="modal" href="#katduzenle_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>                                                        
                                                    <div class="modal fade" id="katduzenle_<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title">Kategori Düzenleme</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="tab-pane" id="tab_kategori_ekle">
                                                                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/category/editcategory');?>
/<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Kategori Adı:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8" id="kategori_adi">
                                                                                    <input type="text" class="form-control" name="adi" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
">                                            
                                                                                </div>
                                                                            </div>
                                                                            <?php if ($_smarty_tpl->tpl_vars['kategori']->value->tip) {?>
                                                                            <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->tip;?>
" />
                                                                            <?php } else { ?>
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">
                                                                                    Kategori Tipi
                                                                                </label>
                                                                                <div class="col-md-8">
                                                                                    <select name="tip" class="form-control" required>
                                                                                        <option value="">Seçiniz</option>
                                                                                        <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                                                                        <option <?php if ($_smarty_tpl->tpl_vars['kategori']->value->tip==$_smarty_tpl->tpl_vars['tip']->value->adi) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <?php }?>                                                                                        
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Sıra No:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8" id="">
                                                                                    <input type="number" class="form-control" name="sira" min="1" value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->sira;?>
">                                            
                                                                                </div>
                                                                            </div>
                                                                            <?php if ($_smarty_tpl->tpl_vars['kategori']->value->altkatsayisi==0) {?>     
                                                                           <div class="form-group">
                                                                                <label class="col-md-4 control-label">Merchants Id
                                                                                </label>
                                                                                <div class="col-md-8" id="">
                                                                                    <input type="text" class="form-control" name="merchants_id"  value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->merchants_id;?>
">                                            
                                                                                </div>
                                                                            </div>
                                                                            <?php }?>

                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label"></label>
                                                                                <div class="col-md-8" id="kategorilerimiz">         
                                                                                    <button type="submit" class="btn blue">Düzenle</button> 
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div> 
                                                    <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="<?php echo site_url('products/category/remove');?>
/<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" title="Sil" class="btn btn-sm btn-default sil">
                                                        <i class="fa fa-times-circle"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php } else { ?>
                                            <tr>
                                                <td colspan='10' style='text-align:center;'> Bu Kategoriye Ait Alt Kategori Yok.</td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <!-- Kategori Ekle -->
    <div class="modal fade in" id="kategori_ekle" tabindex="-1" role="kategori_ekle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Kategori Ekle</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/category/addcategory');?>
">
                        <input type="hidden" name="ust" value="<?php echo $_smarty_tpl->tpl_vars['ust']->value;?>
" />
                        <?php if ($_smarty_tpl->tpl_vars['kategorisi']->value->tip) {?>
                        <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['kategorisi']->value->tip;?>
" />
                        <?php } else { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label">
                                Kategori Tipi
                            </label>
                            <div class="col-md-8">
                                <select name="tip" class="form-control" required>
                                    <option value="">Seçiniz</option>
                                    <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php }?>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Kategori Adı:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="adi" placeholder="">                                            
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="col-md-4 control-label">Sıra No:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8" id="">
                                <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8" id="kategorilerimiz">         
                                <button type="submit" class="btn blue">Ekle</button> 
                            </div>
                        </div>
                    </form>
                </div> 
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Kategori Ekle -->
</div>
<?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function() {
        $('.inlineedit').editable(); 
    });
<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

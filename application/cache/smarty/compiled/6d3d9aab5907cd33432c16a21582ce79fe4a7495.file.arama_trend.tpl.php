<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:45:03
         compiled from "temalar/tema/views/modules/home/arama_trend.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15065977815f58dc5ff03a27-84109780%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d3d9aab5907cd33432c16a21582ce79fe4a7495' => 
    array (
      0 => 'temalar/tema/views/modules/home/arama_trend.tpl',
      1 => 1599655045,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15065977815f58dc5ff03a27-84109780',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'most_searched_keywords' => 0,
    's_search_word' => 0,
    'most_searched_empty' => 0,
    'last_searched' => 0,
    'search_trends' => 0,
    'chunk_group' => 0,
    'count' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dc5ff141c0_90963635',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dc5ff141c0_90963635')) {function content_5f58dc5ff141c0_90963635($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<style type="text/css">	
#chartdiv {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
#ziyaretcilerimiz {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        
        <div class="row">
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze"> Çok Arananlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            <?php  $_smarty_tpl->tpl_vars['s_search_word'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s_search_word']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['most_searched_keywords']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s_search_word']->key => $_smarty_tpl->tpl_vars['s_search_word']->value) {
$_smarty_tpl->tpl_vars['s_search_word']->_loop = true;
?>
                                <div class="row">
                                    <div class="col-md-8">
                                        <?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->search;?>

                                    </div>
                                    <div class="col-md-4">
                                        <?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->adet;?>
 Adet
                                    </div>
                                </div>          
                            <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Aranıp Bulunamayanlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            <?php  $_smarty_tpl->tpl_vars['s_search_word'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s_search_word']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['most_searched_empty']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s_search_word']->key => $_smarty_tpl->tpl_vars['s_search_word']->value) {
$_smarty_tpl->tpl_vars['s_search_word']->_loop = true;
?>
                                <div class="row">
                                    <div class="col-md-8">
                                        <?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->search;?>

                                    </div>
                                    <div class="col-md-4">
                                        <?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->adet;?>
 Adet
                                    </div>
                                </div>          
                            <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Son Arananlar</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            <?php  $_smarty_tpl->tpl_vars['s_search_word'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s_search_word']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['last_searched']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s_search_word']->key => $_smarty_tpl->tpl_vars['s_search_word']->value) {
$_smarty_tpl->tpl_vars['s_search_word']->_loop = true;
?>
                                <div class="row">
                                    <div class="col-md-7">
                                        <?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->search;?>

                                    </div>
                                    <div class="col-md-5">
                                        <?php echo date('d/m H:i:s',$_smarty_tpl->tpl_vars['s_search_word']->value->time);?>

                                    </div>
                                </div>          
                            <?php } ?> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-green-haze"></i>
                            <span class="caption-subject bold font-green-haze">Trendler</span>
                        </div>
                    </div>
                    <div class="portlet-body">  
                            <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable(0, null, 0);?>
                            <div class="row">
                            <?php  $_smarty_tpl->tpl_vars['chunk_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['chunk_group']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['search_trends']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['chunk_group']->key => $_smarty_tpl->tpl_vars['chunk_group']->value) {
$_smarty_tpl->tpl_vars['chunk_group']->_loop = true;
?>
                                <div class="col-md-6">
                                    <?php  $_smarty_tpl->tpl_vars['s_search_word'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['s_search_word']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['chunk_group']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['s_search_word']->key => $_smarty_tpl->tpl_vars['s_search_word']->value) {
$_smarty_tpl->tpl_vars['s_search_word']->_loop = true;
?>
                                    <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_smarty_tpl->tpl_vars['count']->value+1, null, 0);?>
                                    <div class="row">
                                        <div class="col-md-12">
                                       <b><?php echo $_smarty_tpl->tpl_vars['count']->value;?>
.</b><?php echo $_smarty_tpl->tpl_vars['s_search_word']->value->searched;?>

                                        </div>
                                    </div>
                                    <?php } ?> 
                                </div>          
                            <?php } ?> 
                            </div>          
                    </div>
                </div>
            </div>
        </div>

        
        <div id="arama_kaydet" class="modal fade" tabindex="-1" data-width="400" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Arama Kaydetme</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Arama İsmi</label>
                                        <input type="text" id="arama_ismi" value="" placeholder="Arama için isim giriniz" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="lastikkaydetmesonuc"></div>
                        <button type="button" data-dismiss="modal" class="btn dark btn-outline">Kapat</button>
                        <button type="button" class="btn red" onclick="arama_kaydet();">Kaydet</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div id="lastikler">
            
        </div>              
        <!-- END PAGE BASE CONTENT -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:58:47
         compiled from "temalar/tema/views/base/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16012445685f58d866b9e599-55919230%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64b867b0d310f97cfac5631a006d314e035b5532' => 
    array (
      0 => 'temalar/tema/views/base/header.tpl',
      1 => 1599659925,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16012445685f58d866b9e599-55919230',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58d866c629c4_90532991',
  'variables' => 
  array (
    'this' => 0,
    'theme_url' => 0,
    'sepettekiler' => 0,
    'sepetteki' => 0,
    'module' => 0,
    'method' => 0,
    'uyemail' => 0,
    'okunmayan_yorumlar' => 0,
    'segment' => 0,
    'iadetalepadet' => 0,
    'sayfatleri' => 0,
    'sayfat' => 0,
    'gruplarim' => 0,
    'gruplar' => 0,
    'grup' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58d866c629c4_90532991')) {function content_5f58d866c629c4_90532991($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['module'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->router->fetch_class(), null, 0);?>
<?php $_smarty_tpl->tpl_vars['method'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->router->fetch_method(), null, 0);?>
<?php $_smarty_tpl->tpl_vars['segment'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->uri->segment(3), null, 0);?>
<?php $_smarty_tpl->tpl_vars['uyemail'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->getMail(), null, 0);?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
    <title>Lastikcim Portal</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <!-- BEGIN LAYOUT FIRST STYLES  -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
    <!-- END LAYOUT FIRST STYLES -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <!-- <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/pages/css/pricing.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/select2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/select2/css/select2-bootstrap.min.css" />

    <!-- Emre Eklenen -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
    <!-- Emre Eklenen -->

    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->

    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/layouts/layout5/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/layouts/layout5/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
favicon.ico" /> 

    <!-- Emre Taşınanlar -->
    <!-- BEGIN CORE PLUGINS -->
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery.blockui.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <!-- END CORE PLUGINS -->
    <!-- Emre Taşınanlar -->

    <!-- Sweet Alert -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/apps/plugins/sweet/sweetalert.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/apps/plugins/sweet/sweetalert.min.js"><?php echo '</script'; ?>
>
    <!-- Sweet Alert -->
    <style type="text/css">
    .dashboard-stat .visual {
        height: 50px;
    }
</style>

<style type="text/css">
.page-order-navi ul li {
    width: 20%;
}
</style>
<style type="text/css">
.page-header .navbar .navbar-nav li.open:hover > a, .page-header .navbar .navbar-nav li.open>a {
  color: #0095DA !important;
}
.page-header .navbar .navbar-nav .dropdown-menu li.active>a, .page-header .navbar .navbar-nav .dropdown-menu>li>a:focus, .page-header .navbar .navbar-nav .dropdown-menu>li>a:hover {
  color: #0095DA !important;
}
.page-header .navbar .navbar-nav> li:hover>a {
  background-color: #0095DA !important;
}
.page-header .navbar .navbar-nav> li.open:hover>a {
  background-color: #f9f9f9 !important;
}
</style>

<style type="text/css">

#ortala {
    width:960px; margin:0 auto; padding:0 auto; display:block;
}
#onizleme { 
    position:absolute; border:3px solid #696767; background:#fff; padding:0px; display:none; color:#333; -moz-border-radius:5px;
}
.altlink{
    clear:both;margin-top:250px;
}
.islem_bilgilendirme span#islembilgisitext{
    font-size:11px; margin-right: 5px !important; margin-left:0px !important;
} 
.islem_bilgilendirme span{
    margin-left:0px !important;margin-right:0px !important;text-align:right;
}
.beyaz {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#FFF !important;
}
.durumu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#990066 !important;
}
.anasayfavitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#237BB1 !important;
}
.kampanyaliurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#FFCC00 !important;
}
.kategorivitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#EE8701 !important;
}
.markavitrini {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#000000 !important;
}
.yeniurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#009900 !important;
}
.sponsorurunu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#CC0000 !important;
}
.populerurun {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#e15656 !important;
}
.gununurunu {
    width: 18px; height: 18px; padding:1px 3px 3px 3px; border-radius: 0px !important; background-color:#c5b96b !important;
}
.benzerurun {
    background-color:#4D84D4; color:white; height:20px; padding:2px 4px 0px 4px;
}
.hizliduzenle {
    background-color:#4D84D4; color:white; height:20px; padding:0px 4px 0px 4px;
}
.detayliduzenle {
    background-color:#DB53C8; color:white; height:20px; padding:2px 4px 0px 4px;
}
.sil {
    height:20px; padding:0px 5px; background-color:#E73434; color:white;border-radius:10px;
}
.table .btn {
    margin-right: 0px;
}
.bizimbutonlar {
    height: 20px; padding: 0px 5px 0 5px; font-size: 12px;
}

.page-header .navbar .navbar-nav>li>a {
    padding: 20px 25px;
    font-size: 18px;
}
.dropdown-menu {
    min-width: 135px!important;
}
.dropdown-menu .dropdown-menu>li {
    height: 50px;
}
.dropdown-menu .dropdown-menu>li>a {
    padding: 2px;
}
.dropdown-menu .dropdown-menu>li>a {
    padding: 25px!important;
}
.modal-body .col-md-12 {
    white-space: normal;
    line-height: 24px;
}

</style>
</head>
<!-- END HEAD -->	
<body class="page-header-fixed page-sidebar-closed-hide-logo">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGB68Q8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- BEGIN CONTAINER -->
    <div class="wrapper">

        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar mega-menu" role="navigation">
                <div class="container-fluid">
                    <div class="clearfix navbar-fixed-top bg-header relative-mobile" style="background-color: #f9f9f9 !important; border-bottom: 2px solid #0095DA !important;">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Menuu</span>
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <!-- End Toggle Button -->
                        <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a id="index" href="/" class="p-logo">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/logo.png" alt="Logo">
                            </a>
                        </div>
                        <!-- END LOGO -->
                        <!-- END SEARCH -->
                        <!-- BEGIN TOPBAR ACTIONS -->
                        <div class="topbar-actions">
                             <div class="btn-group-img btn-group">
                                <a href="<?php echo site_url('pages/cacheclear');?>
" class="btn btn-sm dropdown-toggle"  style="background-color: #0095DA !important;width: 35px; margin-right: 7px;padding-top: 9px;" title="Geçici Verileri Sil">
                                   <i class="icon-trash" style="color: #fff !important;    font-size: 15px;"></i>
                                </a>
                            </div>
                            <!-- Begin Basket -->
                            <div class="btn-group-notification noti-clss btn-group" id="header_notification_bar">
                                <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: #0095DA !important;" data-hover="dropdown" data-close-others="true" onclick="window.location.assign('/sepet');">
                                    <i class="icon-basket" style="margin:0 0 0 -3px; color: #fff !important;"></i>
                                    <span class="badge basket-count" style="border-color: #0095DA !important;" >0</span>
                                </button>
                                <ul class="dropdown-menu-v2 t-basket">
                                    <li class="external">
                                        <h3>
                                            Sepetinizde <span class="bold basket-count"><?php echo count($_smarty_tpl->tpl_vars['sepettekiler']->value);?>
</span> Ürün Var
                                        </h3>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller basket-product" style="height: 5px; padding: 0;" data-handle-color="#637283">
                                            <?php  $_smarty_tpl->tpl_vars['sepetteki'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sepetteki']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sepettekiler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sepetteki']->key => $_smarty_tpl->tpl_vars['sepetteki']->value) {
$_smarty_tpl->tpl_vars['sepetteki']->_loop = true;
?>
                                            <li class="urun">
                                                <a href="javascript:;">
                                                    <span class="details">
                                                        <span class="label label-sm label-icon">
                                                            <img style="width:27px;" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/layouts/layout5/img/img-2.png">
                                                        </span> 
                                                        <?php echo $_smarty_tpl->tpl_vars['sepetteki']->value->sto_kod;?>
 - <?php echo $_smarty_tpl->tpl_vars['sepetteki']->value->odp_no;?>

                                                    </span>
                                                    <span class="time"><?php echo $_smarty_tpl->tpl_vars['sepetteki']->value->miktar;?>
 Adet</span>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Basket -->
                            <!-- BEGIN USER PROFILE -->
                            <div class="btn-group-img btn-group">
                                <button type="button" class="btn btn-sm dropdown-toggle" style="background-color: #0095DA !important;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span style="color: #fff !important;">Hoşgeldin, CARI ADI</span>
                                </button>
                                <ul class="dropdown-menu-v2 user-btn" role="menu">
                                    <li>
                                        <a href="<?php echo site_url('home/profil');?>
">
                                            <i class="icon-user"></i> BAYI PROFİLİ
                                            <span class="badge badge-danger">1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" style="back">
                                            <i class="icon-envelope-open"></i> Görüş & ÖNERİLER
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('home/profil');?>
#sifre">
                                            <i class="icon-lock"></i> ŞİFRE DEĞİŞTİR </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo site_url('home/logout');?>
">
                                                <i class="icon-key"></i> GÜVENLİ ÇIKIŞ </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END USER PROFILE -->
                                </div>
                                <!-- END TOPBAR ACTIONS -->
                            </div>
                            <!-- BEGIN HEADER MENU -->
                            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown dropdown-fw <?php if (($_smarty_tpl->tpl_vars['module']->value=='home'||$_smarty_tpl->tpl_vars['module']->value=='sepet'||$_smarty_tpl->tpl_vars['module']->value=='importers'||$_smarty_tpl->tpl_vars['module']->value=='montaj')&&$_smarty_tpl->tpl_vars['method']->value!='iletisim') {?>active open<?php }?>">
                                        <a href="javascript:;" class="text-uppercase">
                                            <i class="icon-home" style="display:none;"></i> Ana Sayfa
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-fw">
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='home'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>">
                                                <a href="<?php echo site_url('home/index');?>
">
                                                    <i class="fa fa-car" style="display:none;"></i> Ana Sayfa
                                                </a>
                                            </li>
                                            <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','urunyonetimi@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','esrakadiroglu@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='montaj'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='add')) {?>active<?php }?>">
                                                <a href="<?php echo site_url('montaj/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Montaj Noktaları 
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='importers'&&$_smarty_tpl->tpl_vars['method']->value=='excel') {?>active<?php }?>">
                                                <a href="<?php echo site_url('importers/excel');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Ürün Ekle (Excel)
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='importers'&&$_smarty_tpl->tpl_vars['method']->value=='xml') {?>active<?php }?>">
                                                <a href="<?php echo site_url('importers/xml');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Ürün Ekle (XML)
                                                </a> 
                                            </li>
                                            <?php }?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='home'&&$_smarty_tpl->tpl_vars['method']->value=='entegrasyondurum') {?>active<?php }?>">
                                                <a href="<?php echo site_url('home/entegrasyondurum');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Entegrasyon
                                                </a> 
                                            </li>
                                        </ul>
                                    </li>
                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','urunyonetimi@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','esrakadiroglu@lastikcim.com.tr','berkinbekret@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw <?php if ($_smarty_tpl->tpl_vars['module']->value=='products'||$_smarty_tpl->tpl_vars['module']->value=='marker'||$_smarty_tpl->tpl_vars['module']->value=='category'||$_smarty_tpl->tpl_vars['module']->value=='dimension'||$_smarty_tpl->tpl_vars['module']->value=='brand'||$_smarty_tpl->tpl_vars['module']->value=='comment') {?>active open<?php }?>">
                                        <a href="javascript:;" >
                                            <i class="icon-home" style="display:none;"></i> Ürünler
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
											<?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','urunyonetimi@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','esrakadiroglu@lastikcim.com.tr'))) {?>
                                            <li class="dropdown<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='siralama')) {?> active<?php }?>">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"> Ürünler
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="<?php echo site_url('products/index/tip/lastik');?>
">Lastik</a> 
                                                </li>

                                                <li>
                                                    <a href="<?php echo site_url('products/index/tip/jant');?>
">Jant</a> 
                                                </li>

                                                <li>
                                                    <a href="<?php echo site_url('products/index/tip/mini-stepne');?>
">Mini Stepne</a> 
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url('products/siralama');?>
">Ürün Sıralamaları</a> 
                                                </li>
                                            </ul>
											<?php }?>
                                        </li>



                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='comment'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/comment/index');?>
">
                                                    <?php $_smarty_tpl->tpl_vars['okunmayan_yorumlar'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->okunmayan_yorumlar(), null, 0);?>
                                                    <i style="display:none;" class="icon-basket"></i> Yorumlar <?php if ($_smarty_tpl->tpl_vars['okunmayan_yorumlar']->value>0) {?><span class="text-danger">(<?php echo $_smarty_tpl->tpl_vars['okunmayan_yorumlar']->value;?>
)</span><?php }?>
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='category'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/category/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Kategoriler 
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='brand'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='patterns')) {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/brand/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Markalar 
                                                </a> 
                                            </li>


                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='marker'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='add'||$_smarty_tpl->tpl_vars['method']->value=='edit')) {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/marker/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Etiketler 
                                                </a> 
                                            </li>

                                            <li class="dropdown<?php if (($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='add')||($_smarty_tpl->tpl_vars['module']->value=='dimension'&&$_smarty_tpl->tpl_vars['method']->value=='add')) {?> active<?php }?>"">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Ürün Ekle
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                 <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='add'&&$_smarty_tpl->tpl_vars['segment']->value=='lastik') {?>active<?php }?>">
                                                    <a href="<?php echo site_url('products/add/lastik');?>
">Lastik</a>
                                                </li>

                                                <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='add'&&$_smarty_tpl->tpl_vars['segment']->value=='jant') {?>active<?php }?>">
                                                    <a href="<?php echo site_url('products/add/jant');?>
">Jant</a>
                                                </li>

                                                <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='add'&&$_smarty_tpl->tpl_vars['segment']->value=='mini-stepne') {?>active<?php }?>">
                                                    <a href="<?php echo site_url('products/add/mini-stepne');?>
">Mini Stepne</a>
                                                </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='dimension'&&$_smarty_tpl->tpl_vars['method']->value=='add') {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/dimension/add');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Lastik Ebatı Ekle
                                                </a>
                                            </li>

                                        </ul>
										
											 <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='debug') {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/debug');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Hatalı Ürünler 
                                                </a> 
                                            </li>
											 <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='products'&&$_smarty_tpl->tpl_vars['method']->value=='tatko_eksik_urunler') {?>active<?php }?>">
                                                <a href="<?php echo site_url('products/tatko_eksik_urunler');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Eşleşmeyen Ürünler
                                                </a> 
                                            </li>
                                    </li>


                                        </ul>
                                    </li>
                                        <?php }?>

                                    <li class="dropdown dropdown-fw<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value!='uruntalepleri') {?> active open<?php }?>" >
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Siparişler
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Siparişler 
                                                </a> 
                                            </li> 
                                             <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='odemebildirim') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/odemebildirim');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Ödeme Bildirimleri
                                                </a> 
                                            </li>                                             
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='iptaliade') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/iptaliade');?>
">
                                                    <?php $_smarty_tpl->tpl_vars['iadetalepadet'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->iadetalepadet(), null, 0);?>
                                                    <i style="display:none;" class="icon-basket"></i> İptal & İade Talepleri <span class="font-red-mint">(<?php echo $_smarty_tpl->tpl_vars['iadetalepadet']->value;?>
)</span>
                                                </a> 
                                            </li> 
                             				<li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='posdetay') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/posdetay');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Pos Detay
                                                </a> 
                                            </li> 
                                             
                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','gokhannazli@lastikcim.com.tr','nurcanozdemir@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='siparis_cari_excel') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/siparis_cari_excel');?>
" target="_blank">
                                                    <i style="display:none;" class="icon-basket"></i> Sipariş Carileri İndir
                                                </a> 
                                            </li> 
                                    <?php }?>
                                           	
                                        </ul>
                                    </li>

                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw<?php if ($_smarty_tpl->tpl_vars['module']->value=='users'||$_smarty_tpl->tpl_vars['module']->value=='groups'||$_smarty_tpl->tpl_vars['module']->value=='roles') {?> active open<?php }?>">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Kullanıcılar
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">


                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='users'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>">
                                                <a href="<?php echo site_url('users/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Kullanıcılar
                                                </a>
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='groups'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>">
                                                <a href="<?php echo site_url('users/groups/index');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Kullanıcı Grupları 
                                                </a> 
                                            </li>       

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='users'&&$_smarty_tpl->tpl_vars['method']->value=='islem_kayitlari') {?>active<?php }?>">
                                                <a href="<?php echo site_url('users/islem_kayitlari');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> İşlem Kayıtları 
                                                </a> 
                                            </li>   
                             
                                        </ul>
                                    </li>
                                    <?php }?>
                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','urunyonetimi@lastikcim.com.tr','ulasaltunsoy@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','seldakazak@lastikcim.com.tr','berkinbekret@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw <?php if ($_smarty_tpl->tpl_vars['module']->value=='members'||($_smarty_tpl->tpl_vars['module']->value=='home'&&$_smarty_tpl->tpl_vars['method']->value=='iletisim')||($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='uruntalepleri')||($_smarty_tpl->tpl_vars['module']->value=='sayac'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='banner_sayac'))) {?> active open<?php }?>">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Müşteri Yönetimi
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                      
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='members'&&$_smarty_tpl->tpl_vars['method']->value=='add') {?>active<?php }?>">
                                                <a href="<?php echo site_url('members/add');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Yeni Müşteri 
                                                </a> 
                                            </li> 
                                            <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','ulasaltunsoy@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='members'&&$_smarty_tpl->tpl_vars['method']->value=='crm') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('members/crm');?>
">
                                                    <i style="display:none;" class="icon-basket"></i>Crm
                                                </a> 
                                            </li>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='members'&&$_smarty_tpl->tpl_vars['method']->value=='crmq') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('members/crmq');?>
">
                                                    <i style="display:none;" class="icon-basket"></i>Crm Soruları
                                                </a> 
                                            </li>
                                            <?php }?>
                                             <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='home'&&$_smarty_tpl->tpl_vars['method']->value=='iletisim') {?>active<?php }?>">
                                                <a href="<?php echo site_url('home/iletisim');?>
">
                                                    <i class="fa fa-car" style="display:none;"></i> iletişim İstekleri
                                                </a>
                                            </li>
                                                        <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='orders'&&$_smarty_tpl->tpl_vars['method']->value=='uruntalepleri') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url('orders/uruntalepleri');?>
">
                                                    <i style="display:none;" class="icon-basket"></i>Ürün Talepleri
                                                </a> 
                                            </li> 
                                            <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','ulasaltunsoy@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='sayac'&&$_smarty_tpl->tpl_vars['method']->value=='index') {?>active<?php }?>" style="display: none !important;"> 
                                                <a href="<?php echo site_url(('sayac/index/tarih/').(date('d.m.Y-d.m.Y',time()-86400)));?>
">
                                                    <i style="display:none;" class="icon-basket"></i>Hareketler
                                                </a> 
                                            </li> 
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='sayac'&&$_smarty_tpl->tpl_vars['method']->value=='banner_sayac') {?>active<?php }?>"> 
                                                <a href="<?php echo site_url(('sayac/banner_sayac/tarih/').(date('d.m.Y-d.m.Y',time()-86400)));?>
">
                                                    <i style="display:none;" class="icon-basket"></i>Banner Hareketleri
                                                </a> 
                                            </li> 
                                            <?php }?>
                                        </ul>
                                    </li>
                                    <?php }?>

                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','ulasaltunsoy@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw <?php if ($_smarty_tpl->tpl_vars['module']->value=='pages'||$_smarty_tpl->tpl_vars['module']->value=='faqs'||$_smarty_tpl->tpl_vars['module']->value=='news'||$_smarty_tpl->tpl_vars['module']->value=='anket') {?> active open<?php }?>">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> İçerik Yönetimi
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">

                                            <li class="dropdown <?php if ($_smarty_tpl->tpl_vars['module']->value=='pages'&&($_smarty_tpl->tpl_vars['method']->value=='add'||$_smarty_tpl->tpl_vars['method']->value=='index')) {?>active<?php }?>">
                                             <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Sabit Sayfalar
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                                <?php $_smarty_tpl->tpl_vars['sayfatleri'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->sayfaturleri(), null, 0);?>
                                                <?php  $_smarty_tpl->tpl_vars['sayfat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sayfat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sayfatleri']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sayfat']->key => $_smarty_tpl->tpl_vars['sayfat']->value) {
$_smarty_tpl->tpl_vars['sayfat']->_loop = true;
?>
                                                <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='pages'&&$_smarty_tpl->tpl_vars['method']->value=='index'&&$_smarty_tpl->tpl_vars['segment']->value==$_smarty_tpl->tpl_vars['sayfat']->value->id) {?>active<?php }?>">
                                                    <a href="<?php echo site_url('pages/index');?>
/<?php echo $_smarty_tpl->tpl_vars['sayfat']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['sayfat']->value->tip;?>
</a> 
                                                </li>
                                                <?php } ?>

                                                <li>
                                                    <a href="<?php echo site_url('faqs/index');?>
">Sık Sorulan Sorular</a> 
                                                </li>
                                                <li>
                                                 <a href="<?php echo site_url('pages/add');?>
">Yeni Sayfa Ekle</a> 
                                             </li>
                                         </ul>


                                     </li>
                                     <li class=" <?php if ($_smarty_tpl->tpl_vars['module']->value=='anket') {?>active<?php }?>">
                                        <a href="<?php echo site_url('anket/index');?>
">Anket</a>
                                   </li>

                                      
										 

                                            <li class="dropdown <?php if ($_smarty_tpl->tpl_vars['module']->value=='news'&&($_smarty_tpl->tpl_vars['method']->value=='index'||$_smarty_tpl->tpl_vars['method']->value=='add'||$_smarty_tpl->tpl_vars['method']->value=='edit')) {?>active<?php }?>">
                                             <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Haberler ve Kampanyalar
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu" role="menu">
                                             
                                                      <li>
													<a href="<?php echo site_url('news/index');?>
">
														<i style="display:none;" class="icon-plus"></i> Haber ve Kampanya Yönetimi
													</a> 
                                                </li>
                                                <li>
													<a href="<?php echo site_url('news/category');?>
">
														<i style="display:none;" class="icon-plus"></i> Haber Kategorileri
													</a> 
                                                </li>
                                         </ul>


                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='news'&&($_smarty_tpl->tpl_vars['method']->value=='opportunities'||$_smarty_tpl->tpl_vars['method']->value=='addopportunity'||$_smarty_tpl->tpl_vars['method']->value=='editopportunity')) {?>active<?php }?>">
                                                <a href="<?php echo site_url('news/opportunities');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Fırsat Ürünleri
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='news'&&$_smarty_tpl->tpl_vars['method']->value=='pageskin') {?>active<?php }?>">
                                                <a href="<?php echo site_url('news/pageskin');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Page Skin
                                                </a> 
                                            </li>
											
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='news'&&$_smarty_tpl->tpl_vars['method']->value=='banner_management') {?>active<?php }?>">
                                                <a href="<?php echo site_url('news/banner_management');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Banner Yönetimi
                                                </a> 
                                            </li>

                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='news'&&$_smarty_tpl->tpl_vars['method']->value=='testimonials') {?>active<?php }?>">
                                                <a href="<?php echo site_url('news/testimonials');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Testimonials
                                                </a> 
                                            </li>

                                        </ul>
                                    </li>
                                    <?php }?>
                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','seldakazak@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','gokhannazli@lastikcim.com.tr','nurcanozdemir@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw <?php if ($_smarty_tpl->tpl_vars['module']->value=='settings') {?> active open<?php }?>">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Ayarlar
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
                                            <?php $_smarty_tpl->createLocalArrayVariable('gruplarim', null, 0);
$_smarty_tpl->tpl_vars['gruplarim']->value['menu'] = 1;?>
                                            <?php $_smarty_tpl->tpl_vars['gruplar'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->gruplar($_smarty_tpl->tpl_vars['gruplarim']->value), null, 0);?>
                                        <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                            <li class="dropdown<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings') {?> active<?php }?>">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Seo Ayarları
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <?php  $_smarty_tpl->tpl_vars['grup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['gruplar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grup']->key => $_smarty_tpl->tpl_vars['grup']->value) {
$_smarty_tpl->tpl_vars['grup']->_loop = true;
?>    
                                                    <?php if ($_smarty_tpl->tpl_vars['grup']->value->seo_ayarlari==1) {?>
                                                    <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='group'&&$_smarty_tpl->tpl_vars['segment']->value==$_smarty_tpl->tpl_vars['grup']->value->id) {?>active<?php }?>">
                                                        <a href="<?php echo site_url('settings/group');?>
/<?php echo $_smarty_tpl->tpl_vars['grup']->value->id;?>
">
                                                            <i style="display:none;" class="icon-plus"></i> <?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>

                                                        </a> 
                                                    </li>
                                                    <?php }?>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php }?>
                                        <?php  $_smarty_tpl->tpl_vars['grup'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['grup']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['gruplar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['grup']->key => $_smarty_tpl->tpl_vars['grup']->value) {
$_smarty_tpl->tpl_vars['grup']->_loop = true;
?>
                                            <?php if ($_smarty_tpl->tpl_vars['grup']->value->seo_ayarlari==0) {?>
                                                <?php if ($_smarty_tpl->tpl_vars['grup']->value->id==14) {?>
                                                        <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                                            <li data-name="<?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>
" class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='group'&&$_smarty_tpl->tpl_vars['segment']->value==$_smarty_tpl->tpl_vars['grup']->value->id) {?>active<?php }?>">
                                                                <a href="<?php echo site_url('settings/group');?>
/<?php echo $_smarty_tpl->tpl_vars['grup']->value->id;?>
">
                                                                    <i style="display:none;" class="icon-plus"></i> <?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>

                                                                </a> 
                                                            </li>
                                                        <?php }?>
                                                <?php } elseif ($_smarty_tpl->tpl_vars['grup']->value->id==15) {?>
                                                        <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('seldakazak@lastikcim.com.tr','berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','yilmazustun@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                                            <li data-name="<?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>
" class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='group'&&$_smarty_tpl->tpl_vars['segment']->value==$_smarty_tpl->tpl_vars['grup']->value->id) {?>active<?php }?>">
                                                                <a href="<?php echo site_url('settings/group');?>
/<?php echo $_smarty_tpl->tpl_vars['grup']->value->id;?>
">
                                                                    <i style="display:none;" class="icon-plus"></i> <?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>

                                                                </a> 
                                                            </li>
                                                        <?php }?>
                                                <?php } else { ?>
                                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                                    <li data-name="<?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>
" class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='group'&&$_smarty_tpl->tpl_vars['segment']->value==$_smarty_tpl->tpl_vars['grup']->value->id) {?>active<?php }?>">
                                                        <a href="<?php echo site_url('settings/group');?>
/<?php echo $_smarty_tpl->tpl_vars['grup']->value->id;?>
">
                                                            <i style="display:none;" class="icon-plus"></i> <?php echo $_smarty_tpl->tpl_vars['grup']->value->group_name;?>

                                                        </a> 
                                                    </li>
                                                    <?php }?>
                                                <?php }?>
                                            <?php }?>
                                        <?php } ?>
                                        <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr','gokhannazli@lastikcim.com.tr','nurcanozdemir@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='payments') {?>active<?php }?>">
                                                <a href="<?php echo site_url('settings/payments');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Ödeme Ayarları
                                                </a> 
                                            </li>
                                        <?php }?>
                                        <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='settings'&&$_smarty_tpl->tpl_vars['method']->value=='mailtext') {?>active<?php }?>">
                                                <a href="<?php echo site_url('settings/mailtext');?>
">
                                                    <i style="display:none;" class="icon-plus"></i> Mail Metin Ayarları
                                                </a> 
                                            </li>
                                        <?php }?>
                                        </ul>
                                    </li>
                                    <?php }?>
                                    <?php if (in_array($_smarty_tpl->tpl_vars['uyemail']->value,array('berkaykilic@lastikcim.com.tr','harunozel@lastikcim.com.tr','ahmeterkan@lastikcim.com.tr'))) {?>
                                    <li class="dropdown dropdown-fw <?php if ($_smarty_tpl->tpl_vars['module']->value=='campaigns') {?> active open<?php }?>">
                                        <a href="javascript:;" class="">
                                            <i style="display:none;" class="icon-layers"></i> Kampanya Yönetimi 
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-fw">
											<li class="dropdown <?php if ($_smarty_tpl->tpl_vars['module']->value=='campaigns'&&($_smarty_tpl->tpl_vars['method']->value=='manage'||$_smarty_tpl->tpl_vars['method']->value=='reqgroups'||$_smarty_tpl->tpl_vars['method']->value=='resgroups')) {?>active<?php }?>">
												<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Kampanyalar
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
												<ul class="dropdown-menu" role="menu">
													
													<li>
														<a href="<?php echo site_url('campaigns/manage');?>
">Kampanyalar</a> 
													</li>
													<li>
														<a href="<?php echo site_url('campaigns/reqgroups');?>
">Kampanya Koşul Grupları</a> 
													</li>
													<li>
														<a href="<?php echo site_url('campaigns/resgroups');?>
">Kampanya Sonuç Grupları</a> 
													</li>
												</ul>
											</li>
                                            <li class="<?php if ($_smarty_tpl->tpl_vars['module']->value=='campaigns'&&($_smarty_tpl->tpl_vars['method']->value=='points')) {?>active<?php }?>">
                                                <a href="<?php echo site_url('campaigns/points');?>
">
                                                    <i style="display:none;" class="icon-basket"></i> Puan Sistemi 
                                                </a> 
                                            </li>

                                            <li class="dropdown <?php if ($_smarty_tpl->tpl_vars['module']->value=='campaigns'&&($_smarty_tpl->tpl_vars['method']->value=='addgiftvoucher'||$_smarty_tpl->tpl_vars['method']->value=='giftvouchers'||$_smarty_tpl->tpl_vars['method']->value=='editgiftvoucher')) {?>active<?php }?>">
												<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Hediye Çekleri
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
												<ul class="dropdown-menu" role="menu">
													<li>
														<a href="<?php echo site_url('campaigns/giftvouchers');?>
"> Hediye Çeki Yönetimi</a> 
													</li>
													<li>
														<a href="<?php echo site_url('campaigns/addordergiftvoucher');?>
">Sipariş Hediye Çeki Tanımı</a> 
													</li>
												</ul>
                                            </li>
											
                                        </ul>
                                    </li>
                                    <?php }?>
                                </ul>
                            </div>
                            <!-- END HEADER MENU -->
                        </div>
                        <!--/container-->
            </nav>
        </header>
        <!-- END HEADER --><?php }} ?>

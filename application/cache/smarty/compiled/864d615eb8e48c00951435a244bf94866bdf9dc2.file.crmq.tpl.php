<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 12:21:16
         compiled from "temalar/tema/views/modules/members/crmq.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6992828625f5a1a3c561fd6-59731901%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '864d615eb8e48c00951435a244bf94866bdf9dc2' => 
    array (
      0 => 'temalar/tema/views/modules/members/crmq.tpl',
      1 => 1599655045,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6992828625f5a1a3c561fd6-59731901',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'sorular' => 0,
    'soru' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f5a1a3c574a28_78022209',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f5a1a3c574a28_78022209')) {function content_5f5a1a3c574a28_78022209($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr style="width:50%;">   
                                <th style="text-align:left;border-right:  0" align="left">

                                    <a class="btn btn-sm red btn-outline sbold " data-toggle="modal" href="#soru_ekle">
                                        <i class="fa fa-plus"></i>  
                                        Soru Ekle  
                                    </a>

                                </th> 
                               <th class="islem_bilgilendirme"  style="text-align: right;border-left: 0">
                            
  <span class="btn btn-sm btn-default hizliduzenle"><i class="fa fa-pencil"></i></span>      
                                        <span id="islembilgisitext"> Hızlı Düzenle </span>
                               <span class="btn btn-sm btn-default sil"><i class="fa fa-times"></i></span>      
                               <span id="islembilgisitext"> Sil </span>
                           </th>
                            </tr>
                        </thead>
                    </table>

                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                         <div class="tab-content">                               
                            <div class="tab-pane active" id="tab_kategoriler">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="1%"> ID </th> 
                                                <th width="1%">Sıra </th>  
                                                <th width="80%">Soru </th> 
                                                <th width="10%">Tip </th>
                                                <th width="1%"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (count($_smarty_tpl->tpl_vars['sorular']->value)>0) {?>

                                            <?php  $_smarty_tpl->tpl_vars['soru'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['soru']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sorular']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['soru']->key => $_smarty_tpl->tpl_vars['soru']->value) {
$_smarty_tpl->tpl_vars['soru']->_loop = true;
?>
                                            <tr>
                                                <td><?php echo $_smarty_tpl->tpl_vars['soru']->value->id;?>
</td> 
                                                <td align="center"><?php echo $_smarty_tpl->tpl_vars['soru']->value->sira;?>
</td> 
                                                <td><?php echo $_smarty_tpl->tpl_vars['soru']->value->soru;?>
</td> 
                                                <td><?php if ($_smarty_tpl->tpl_vars['soru']->value->soru_tip=='e-h') {?> 
                                                    Evet - Hayır
                                                    <?php } elseif ($_smarty_tpl->tpl_vars['soru']->value->soru_tip=='puan') {?> 
                                                    Puan
                                                    <?php } elseif ($_smarty_tpl->tpl_vars['soru']->value->soru_tip=='marka') {?> 
                                                    Marka 
                                                    <?php } else { ?> 
                                                    Yazı
                                                <?php }?></td>
                                                <td>  
                                                    <a data-toggle="modal" href="#soruduzenle_<?php echo $_smarty_tpl->tpl_vars['soru']->value->id;?>
" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>                                                        
                                                    <div class="modal fade" id="soruduzenle_<?php echo $_smarty_tpl->tpl_vars['soru']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title">Soru Düzenleme</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="tab-pane" id="tab_kategori_ekle">
                                                                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('members/crm_soru_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['soru']->value->id;?>
">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Soru:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8">
                                                                                    <input type="text" class="form-control" name="soru" placeholder=""value="<?php echo $_smarty_tpl->tpl_vars['soru']->value->soru;?>
">                                            
                                                                                </div>
                                                                            </div>

                                                                            <input type="hidden" name="soru_tip" value="<?php echo $_smarty_tpl->tpl_vars['soru']->value->soru_tip;?>
" />

                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Sıra No:
                                                                                    <span class="required"> * </span>
                                                                                </label>
                                                                                <div class="col-md-8" id="">
                                                                                    <input type="number" class="form-control" name="sira" min="1" value="<?php echo $_smarty_tpl->tpl_vars['soru']->value->sira;?>
">                                            
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label"></label>
                                                                                <div class="col-md-8" id="kategorilerimiz">         
                                                                                    <button type="submit" class="btn blue">Düzenle</button> 
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div> 
                                                    <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="<?php echo site_url('members/crm_soru_remove');?>
/<?php echo $_smarty_tpl->tpl_vars['soru']->value->id;?>
" title="Sil" class="btn btn-sm btn-default sil">
                                                        <i class="fa fa-times-circle"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <?php } else { ?>
                                            <tr>
                                                <td colspan='10' style='text-align:center;'> Crm Sorusu Yok.</td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <div class="modal fade in" id="soru_ekle" tabindex="-1" role="soru_ekle" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Soru Ekle</h4>
            </div>
            <div class="modal-body">
                <div class="tab-pane" id="tab_kategori_ekle">
                    <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('members/crm_soru_ekle');?>
">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Soru:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8" >
                                <input type="text" class="form-control" name="soru" placeholder=""  required="required">                                           
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Soru Tipi:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8" >

                                <select name="soru_tip" class="form-control" required="required">
                                    <option value="">Seçiniz</option>
                                    <option value="marka">Marka</option>
                                    <option value="e-h">Evet - Hayır</option>
                                    <option value="puan">Puan</option>
                                    <option value="text">Yazı</option>                                                                
                                </select>                                           
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Sıra No:
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-8" id="">
                                <input type="number" class="form-control" name="sira" min="1"   required="required">                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8" id="kategorilerimiz">         
                                <button type="submit" class="btn blue">Ekle</button> 
                            </div>
                        </div>
                    </form>
                </div> 
            </div> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

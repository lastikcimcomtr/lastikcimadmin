<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:26:13
         compiled from "temalar/tema/views/modules/home/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1018067365f58d7f591e3f0-08000291%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd330b1f6a62be71308080dc24fdfc41d5ec5b51b' => 
    array (
      0 => 'temalar/tema/views/modules/home/login.tpl',
      1 => 1599657767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1018067365f58d7f591e3f0-08000291',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'theme_url' => 0,
    'status' => 0,
    'message' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58d7f5928b79_74149654',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58d7f5928b79_74149654')) {function content_5f58d7f5928b79_74149654($_smarty_tpl) {?><!DOCTYPE html> 
<html lang="tr">
<head>
    <meta charset="utf-8" />
    <title>Lastikcim</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/pages/css/lock-2.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico" /> 
</head>
<body class="">

    <div class="page-lock">
        <?php if ($_smarty_tpl->tpl_vars['status']->value&&$_smarty_tpl->tpl_vars['message']->value) {?>
        <div class="alert alert-<?php echo $_smarty_tpl->tpl_vars['status']->value;?>
 text-center">
            <button class="close" data-close="alert"></button>
            <span> <?php echo $_smarty_tpl->tpl_vars['message']->value;?>
 </span>
        </div>
        <?php }?>
        <div class="page-body" style="background-color: #fff;"> 
            <img class="page-lock-img" src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/logo.png" alt="">
            <div class="page-lock-info">
                <h1 style="color: #504d4d;">Giriş Yap</h1>
                <p class="text-muted" style="margin-bottom: 13px;color: #504d4d;">Yönetim Paneli Bilgilerini Giriniz</p>
                <form action="" method="post" autocomplete="off" style="margin:0px;">
                    <div class="input-group mb-1" style="margin-bottom: 10px;">
                        <span class="input-group-addon"><i class="icon-user"></i></span>
                        <input type="text" class="form-control" required="" placeholder="Kullanıcı Adı" name="kullanici" id="kullaniciadi">
                    </div>
                    <div class="input-group mb-2" style="margin-bottom: 10px;">
                        <span class="input-group-addon"><i class="icon-lock"></i>
                        </span>
                        <input type="password" required="" class="form-control" placeholder="Şifre" name="sifre" id="sifre">
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-primary px-2">Giriş Yap</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="page-footer-custom" style="color:#504d4d;">  Lastikcim.com.tr <a target="_blank" href="http://www.lastikcim.com.tr">Panel</a> </div>
        </div>

    <style type="text/css">
        .display-table{
            display: table;
            table-layout: fixed;
        }

        .display-cell{
            display: table-cell;
            vertical-align: middle;
            float: none;
        }
    </style>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/jquery.blockui.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['theme_url']->value;?>
assets/global/scripts/app.min.js" type="text/javascript"><?php echo '</script'; ?>
>
</body>
</html><?php }} ?>

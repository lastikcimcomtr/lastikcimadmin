<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 14:03:55
         compiled from "temalar/tema/views/modules/products/brand/patterns.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12082171555f58dfa9c88356-72729586%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12c69fbb2d728b12c094cc413b68671a845e81c1' => 
    array (
      0 => 'temalar/tema/views/modules/products/brand/patterns.tpl',
      1 => 1599660234,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12082171555f58dfa9c88356-72729586',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dfa9cce0f3_03684221',
  'variables' => 
  array (
    'markasi' => 0,
    'desenler' => 0,
    'desen' => 0,
    'this' => 0,
    'tipler' => 0,
    'tip' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
    'mevsimler' => 0,
    'mevsim' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dfa9cce0f3_03684221')) {function content_5f58dfa9cce0f3_03684221($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Desenler</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('/');?>
">Anasayfa</a>
                </li>
                <li>
                    <a href="<?php echo site_url('products/index');?>
">Ürünler</a>
                </li>
                <li>
                    <a href="<?php echo site_url('products/brand/index');?>
">Markalar</a>
                </li>
                <li class="active">Desenler</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut desenleri görebilir, Yeni desen ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#desen_ekle">
                            <i class="fa fa-plus"></i>  
                            Desen Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Desenler <?php if ($_smarty_tpl->tpl_vars['markasi']->value) {?> - <?php echo $_smarty_tpl->tpl_vars['markasi']->value->adi;
}?></a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="30%"> Desen Adı </th> 
                                                    <th width="15%"> Kategori </th>
                                                    <th width="15%"> Marka </th>
                                                    <th width="10%"> Tip </th>
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                                    <th width="8%"> <center>Ürünler</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($_smarty_tpl->tpl_vars['desenler']->value)>0) {?>

                                                    <?php  $_smarty_tpl->tpl_vars['desen'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['desen']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['desenler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['desen']->key => $_smarty_tpl->tpl_vars['desen']->value) {
$_smarty_tpl->tpl_vars['desen']->_loop = true;
?>

                                                        <tr>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
</td> 
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['desen']->value->sira;?>
</td> 
                                                            <td><?php echo $_smarty_tpl->tpl_vars['desen']->value->adi;?>
</td> 
                                                            <td><?php echo agaclar($_smarty_tpl->tpl_vars['desen']->value->kategori_adlari);?>
</td>
                                                            <td><?php echo agaclar($_smarty_tpl->tpl_vars['desen']->value->marka_adlari);?>
</td>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['desen']->value->tip;?>
</td>                                                           
                                                            <td>
                                                                <a onclick="durum_degis(<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
,'desenler', '#desendurum<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
');" id="desendurum<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['desen']->value->durum==1) {?>durumu<?php } else { ?>beyaz<?php }?>" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                           														<form method="post" enctype="multipart/form-data" action="<?php echo site_url('products/brand/editpatternimage');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
">
	                                                                            	<div class="form-group">						                                                            
						                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
						                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
						                                                                    	<?php if ($_smarty_tpl->tpl_vars['desen']->value->resim) {?>
						                                                                    	<img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('desen_resim',$_smarty_tpl->tpl_vars['desen']->value->resim);?>
" alt="" />
						                                                                    	<?php } else { ?>
						                                                                    	<img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
						                                                                    	<?php }?>
						                                                                    </div>
						                                                                    <div>
						                                                                        <span class="btn red btn-outline btn-file">
						                                                                            <span class="fileinput-new"> Resim Seçiniz </span>
						                                                                            <span class="fileinput-exists"> Değiştir </span>
						                                                                            <input type="file" name="resim"> </span>
						                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
						                                                                    </div>
						                                                                </div>
							                                                        </div>
							                                                        <div class="form-group">
							                                                        	<button type="submit" class="btn blue">Kaydet</button>
							                                                        </div>
							                                                    </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
_2"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
_2" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">2. Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('products/brand/editpatternimage');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
/resim_2">
                                                                                    <div class="form-group">                                                                                    
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                                                                                                <?php if ($_smarty_tpl->tpl_vars['desen']->value->resim) {?>
                                                                                                <img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('desen_resim',$_smarty_tpl->tpl_vars['desen']->value->resim_2);?>
" alt="" />
                                                                                                <?php } else { ?>
                                                                                                <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                                <?php }?>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span class="btn red btn-outline btn-file">
                                                                                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                                    <span class="fileinput-exists"> Değiştir </span>
                                                                                                    <input type="file" name="resim"> </span>
                                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <a data-toggle="modal" href="#desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
_3"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="desen_resmi_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
_3" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">3. Desen Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                                                                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('products/brand/editpatternimage');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
/resim_3">
                                                                                    <div class="form-group">                                                                                    
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
                                                                                                <?php if ($_smarty_tpl->tpl_vars['desen']->value->resim) {?>
                                                                                                <img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('desen_resim',$_smarty_tpl->tpl_vars['desen']->value->resim_3);?>
" alt="" />
                                                                                                <?php } else { ?>
                                                                                                <img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
                                                                                                <?php }?>
                                                                                            </div>
                                                                                            <div>
                                                                                                <span class="btn red btn-outline btn-file">
                                                                                                    <span class="fileinput-new"> Resim Seçiniz </span>
                                                                                                    <span class="fileinput-exists"> Değiştir </span>
                                                                                                    <input type="file" name="resim"> </span>
                                                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <button type="submit" class="btn blue">Kaydet</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>

                                                            <td align="center">                                                 
                                                                <a href="#urunler" class="btn red btn-outline sbold bizimbutonlar" title="Ürünler">
                                                                    Ürünler
                                                                </a>
                                                            </td>
                                                            <td>  
                                                                <a data-toggle="modal" href="#desenduzenle_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" title="Ürün Açıklaması Ekle" class="btn btn-sm btn-default hizliduzenle">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>   
                                                                <a href="<?php echo site_url('products/brand/editpattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" title="Desen Düzenle" class="btn btn-sm btn-default detayliduzenle">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>                                                
                                                                <a href="<?php echo site_url('products/brand/modernedit');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" style="background-color: #229922 !important" title="Desen Açıklama Düzenle" class="btn btn-sm durumu btn-primary ">
                                                                    <i class="fa fa-pencil-square"></i>
                                                                </a>                          
                                                                <a href="<?php echo site_url('products/brand/renderpattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" title="Desen Açıklama Düzenle" class="btn btn-sm btn-default detayliduzenle durumu">
                                                                    <i class="fa fa-pencil-square"></i>
                                                                </a>                                               
                                                                <div class="modal fade" id="desenduzenle_<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Desen Düzenleme</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="tab-pane" id="tab_kategori_ekle">
                                                                                    <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/brand/editpattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
">
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">Desen Adı:
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-8" id="kategori_adi">
                                                                                                <input type="text" class="form-control" name="adi" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->adi;?>
">                                            
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php if ($_smarty_tpl->tpl_vars['desen']->value->tip) {?>
                                                                                        <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->tip;?>
" />
                                                                                        <?php } else { ?>
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">
                                                                                                Desen Tipi
                                                                                            </label>
                                                                                            <div class="col-md-8">
                                                                                                <select name="tip" class="form-control" required>
                                                                                                    <option value="">Seçiniz</option>
                                                                                                    <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                                                                                        <option <?php if ($_smarty_tpl->tpl_vars['markasi']->value->tip==$_smarty_tpl->tpl_vars['tip']->value->adi) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                                                                                    <?php } ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php }?>                                                          
                                                                                        
 

                                                                                        <div class="form-group">
                                                                                            <label class="col-md-12 control-label">Desen Ürün Açıklaması:
                                                                                            </label>
                                                                                            <div class="col-md-12">
                                                                                                <textarea  class="ckeditor form-control" name="urun_aciklama" row="6"><?php echo $_smarty_tpl->tpl_vars['desen']->value->urun_aciklama;?>
</textarea>
                                                                                            </div>
                                                                                        </div>                																					
                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label">Sıra No:
                                                                                                <span class="required"> * </span>
                                                                                            </label>
                                                                                            <div class="col-md-8" id="">
                                                                                                <input type="number" class="form-control" name="sira" min="1" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->sira;?>
">                                            
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-md-4 control-label"></label>
                                                                                            <div class="col-md-8" id="kategorilerimiz">         
                                                                                                <button type="submit" class="btn blue">Düzenle</button> 
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div> 
                                                                            </div> 
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div> 
                                                                <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="<?php echo site_url('products/brand/removepattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
" title="Sil" class="btn btn-sm btn-default sil">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                <tr>
                                                    <td colspan='10' style='text-align:center;'> Bu Markaya Ait Desen Yok.</td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                        <?php echo $_smarty_tpl->tpl_vars['this']->value->pagination->create_links();?>

                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

        <!-- Desen Ekle -->
        <div class="modal fade in" id="desen_ekle" tabindex="-1" role="desen_ekle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Desen Ekle</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/brand/addpattern');?>
">
                            <input type="hidden" name="marka" value="<?php echo $_smarty_tpl->tpl_vars['markasi']->value->id;?>
" />
                            <input type="hidden" name="ust" value="0" />
                            <?php if ($_smarty_tpl->tpl_vars['markasi']->value->tip) {?>
                            <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['markasi']->value->tip;?>
" />
                            <?php } else { ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Desen Tipi
                                </label>
                                <div class="col-md-8">
                                    <select name="tip" class="form-control form-filter input-sm select2me" required>
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <div class="form-group">                                             
                                <label class="col-md-4 control-label">  
                                    Kategori 
                                </label> 
                                <div class="col-md-8" id="kategorilerimiz">
                                    <select id="kategori" name="kategori" data-placeholder="Kategoriler" onchange="kategoriler(1, 'lastik', 'kategori')" class="form-control form-filter input-sm select2me">
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                    <div id="alt_kategori"></div>                                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Mevsim
                                </label>
                                <div class="col-md-8">
                                    <select name="mevsim" data-placeholder="Mevsimler" class="form-control form-filter input-sm select2me" required>
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['mevsim'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['mevsim']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mevsimler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['mevsim']->key => $_smarty_tpl->tpl_vars['mevsim']->value) {
$_smarty_tpl->tpl_vars['mevsim']->_loop = true;
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['mevsim']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['mevsim']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="adi" placeholder="">                                            
                                </div>
                            </div>
                        

                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="form-control" name="desen_aciklama"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label">Desen Ürün Açıklaması:
                           </label>
                                <div class="col-md-8">
                                    <textarea  class="form-control" name="urun_aciklama"></textarea>
                                </div>
                            </div>


                            <div class="form-group" >
                                <label class="col-md-4 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" id="">
                                    <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Ekle</button> 
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Kategori Ekle -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:59:31
         compiled from "temalar/tema/views/modules/products/brand/editpattern.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20131982055f58dfc349dd61-01468611%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '186f96354c4ec10a3e822be45e9ac2bf291be451' => 
    array (
      0 => 'temalar/tema/views/modules/products/brand/editpattern.tpl',
      1 => 1599655221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20131982055f58dfc349dd61-01468611',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'desen' => 0,
    'tipler' => 0,
    'markasi' => 0,
    'tip' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dfc34c19f2_57899110',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dfc34c19f2_57899110')) {function content_5f58dfc34c19f2_57899110($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->

        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">

                    <div class="portlet-body">



                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/brand/editpattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="adi" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->adi;?>
">                                            
                                </div>
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['desen']->value->tip) {?>
                            <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->tip;?>
" />
                            <?php } else { ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">
                                    Desen Tipi
                                </label>
                                <div class="col-md-3">
                                    <select name="tip" class="form-control" required>
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['markasi']->value->tip==$_smarty_tpl->tpl_vars['tip']->value->adi) {?>selected<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php }?>                    
                            <div class="form-group">
                                <label class="col-md-3 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-3" id="">
                                    <input type="number" class="form-control" name="sira" min="1" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->sira;?>
">                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Kategorisi Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="ckeditor form-control" name="desen_aciklama"><?php echo $_smarty_tpl->tpl_vars['desen']->value->desen_aciklama;?>
</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Desen Ürün Açıklaması:
                                </label>
                                <div class="col-md-8">
                                    <textarea  class="ckeditor form-control" name="urun_aciklama" row="6"><?php echo $_smarty_tpl->tpl_vars['desen']->value->urun_aciklama;?>
</textarea>
                                </div>
                            </div>                                             

							<div class="form-group">
                                <label class="col-md-3 control-label">Desen Keywords:
                                    <span> </span>
                                </label>
                                <div class="col-md-3" id="kategori_adi">
                                    <input type="text" class="form-control" name="keywords" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->keywords;?>
">                                            
                                </div>
                            </div>

							<div class="form-group">
								<label class="col-md-3 control-label">Mevsim:
								</label>
								<div class="col-md-3 id="">
									<select name="mevsim" class="form-control">
										<option value="">Yok</option>
										<option value="4m" <?php if ($_smarty_tpl->tpl_vars['desen']->value->mevsim=="4m") {?>selected<?php }?>>4m</option>
										<option value="kis" <?php if ($_smarty_tpl->tpl_vars['desen']->value->mevsim=="kis") {?>selected<?php }?>>kis</option>
										<option value="yaz" <?php if ($_smarty_tpl->tpl_vars['desen']->value->mevsim=="yaz") {?>selected<?php }?>>yaz</option>
									</select>                                        
								</div>
							</div>	

                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Düzenle</button> 
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

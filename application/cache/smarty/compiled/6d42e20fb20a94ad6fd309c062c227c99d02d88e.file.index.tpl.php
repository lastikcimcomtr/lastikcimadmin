<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:59:01
         compiled from "temalar/tema/views/modules/products/brand/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13246101955f58dfa58d5830-13941815%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d42e20fb20a94ad6fd309c062c227c99d02d88e' => 
    array (
      0 => 'temalar/tema/views/modules/products/brand/index.tpl',
      1 => 1599655221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13246101955f58dfa58d5830-13941815',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'markasi' => 0,
    'markalar' => 0,
    'marka' => 0,
    'this' => 0,
    'ust' => 0,
    'tipler' => 0,
    'tip' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dfa58f6720_11886499',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dfa58f6720_11886499')) {function content_5f58dfa58f6720_11886499($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Markalar</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo site_url('/');?>
">Anasayfa</a>
                </li>
                <li>
                    <a href="<?php echo site_url('products/index');?>
">Ürünler</a>
                </li>
                <li class="active">Markalar</li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-shopping-cart"></i> Mevcut markaları görebilir, Yeni marka ekleyebilirsiniz.
                        </div>
                        <a class="btn btn-sm red btn-outline sbold pull-right" data-toggle="modal" href="#marka_ekle">
                            <i class="fa fa-plus"></i>  
                            Marka Ekle  
                        </a>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-bordered">
                            <ul class="nav nav-tabs"> 
                                <li class="active"><a href="#tab_kategoriler" data-toggle="tab"> Markalar <?php if ($_smarty_tpl->tpl_vars['markasi']->value) {?> - <?php echo $_smarty_tpl->tpl_vars['markasi']->value->adi;
}?></a></li>   
                            </ul>
                            <div class="tab-content">                               
                                <div class="tab-pane active" id="tab_kategoriler">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="1%"> ID </th> 
                                                    <th width="1%"> Sıra </th>  
                                                    <th width="60%"> Marka Adı </th> 
                                                    <th width="10%"> Tip </th>
                                                    <th width="8%"> <center>Marka Gruplaması</center> </th>                                               
                                                    <th width="1%"> <span  class="btn btn-circle btn-icon-only btn-default durumu" title="Durumu"></span> </th> 
                                                    <th width="1%"> <span class="btn btn-sm btn-default detayliduzenle" title="Marka Logosu"><i class="icon-picture"></i></span> </th> 
                                               
                                                    <th width="8%"> <center>Desenler</center> </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (count($_smarty_tpl->tpl_vars['markalar']->value)>0) {?>

                                                    <?php  $_smarty_tpl->tpl_vars['marka'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['marka']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['markalar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['marka']->key => $_smarty_tpl->tpl_vars['marka']->value) {
$_smarty_tpl->tpl_vars['marka']->_loop = true;
?>
                                                        <tr>
                                                            <td><?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
</td> 
                                                            <td align="center"><?php echo $_smarty_tpl->tpl_vars['marka']->value->sira;?>
</td> 
                                                            <td><?php echo $_smarty_tpl->tpl_vars['marka']->value->adi;?>
</td> 
                                                            <td><?php echo $_smarty_tpl->tpl_vars['marka']->value->tip;?>
</td>
                                                            <td>
                                                                <select name="fiyat_grup" class="form-control" data-marka="<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
">
                                                                <option value="">Seçiniz</option>
                                                                <option <?php if ($_smarty_tpl->tpl_vars['marka']->value->fiyat_grup=="T1") {?>selected<?php }?> value="T1">T1</option>
                                                                <option <?php if ($_smarty_tpl->tpl_vars['marka']->value->fiyat_grup=="T2") {?>selected<?php }?> value="T2">T2</option>
                                                                <option <?php if ($_smarty_tpl->tpl_vars['marka']->value->fiyat_grup=="T3") {?>selected<?php }?> value="T3">T3</option>
                                                                
                                                            </select>
                                                            </td>
                                                                                                                   
                                                            <td>
                                                                <a onclick="durum_degis(<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
,'markalar', '#markadurumu<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
');" id="markadurumu<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
" class="btn btn-circle btn-icon-only btn-default <?php if ($_smarty_tpl->tpl_vars['marka']->value->durum==1) {?>durumu<?php } else { ?>beyaz<?php }?>" title="Pasif Yap"><i class=""></i></a> 
                                                            </td>
                                                            <td>
                                                                <a data-toggle="modal" href="#marka_resmi_<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
"  class="btn btn-sm btn-default detayliduzenle" title="Yeni Resim Yükle">
                                                                    <span aria-hidden="true" class="icon-picture"></span>
                                                                </a> 
                                                                <div class="modal fade" id="marka_resmi_<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Marka Resmi Ekleme</h4>
                                                                            </div>
                                                                            <div class="modal-body"> 
                           														<form method="post" enctype="multipart/form-data" action="<?php echo site_url('products/brand/editbrandimage');?>
/<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
">
	                                                                            	<div class="form-group">						                                                            
						                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
						                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 200px;">
						                                                                    	<?php if ($_smarty_tpl->tpl_vars['marka']->value->resim) {?>
						                                                                    	<img src="<?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->fullresimurl('marka_resim',$_smarty_tpl->tpl_vars['marka']->value->resim);?>
" alt="" />
						                                                                    	<?php } else { ?>
						                                                                    	<img src="http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=resim+yok" alt="" />
						                                                                    	<?php }?>
						                                                                    </div>
						                                                                    <div>
						                                                                        <span class="btn red btn-outline btn-file">
						                                                                            <span class="fileinput-new"> Resim Seçiniz </span>
						                                                                            <span class="fileinput-exists"> Değiştir </span>
						                                                                            <input type="file" name="resim"> </span>
						                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
						                                                                    </div>
						                                                                </div>
							                                                        </div>
							                                                        <div class="form-group">
							                                                        	<button type="submit" class="btn blue">Kaydet</button>
							                                                        </div>
							                                                    </form>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </td>
                                 
                                                            <td align="center">                                                 
                                                                <a href="<?php echo site_url('products/brand/patterns');?>
/<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
" class="btn btn-sm red btn-outline sbold bizimbutonlar">Desenler</a>
                                                            </td>
                                                            <td>  
                                                                <a  href="<?php echo site_url('products/brand/editbrand');?>
/<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
" title="Detaylı Düzenle" class="btn btn-sm btn-default hizliduzenle" target="_blank">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>                                                        
                                                             
                                                                <a onclick="return confirm('Silmek istediğinizden emin misiniz?');" href="<?php echo site_url('products/brand/remove');?>
/<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
" title="Sil" class="btn btn-sm btn-default sil">
                                                                    <i class="fa fa-times-circle"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                <tr>
                                                    <td colspan='10' style='text-align:center;'> Bu Markaya Ait Alt Marka Yok.</td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    </div> 
                                </div>                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
        <!-- Marka Ekle -->
        <div class="modal fade in" id="marka_ekle" tabindex="-1" role="marka_ekle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Marka Ekle</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-row-seperated" method="post" action="<?php echo site_url('products/brand/addbrand');?>
">
                            <input type="hidden" name="ust" value="<?php echo $_smarty_tpl->tpl_vars['ust']->value;?>
" />
                            <?php if ($_smarty_tpl->tpl_vars['markasi']->value->tip) {?>
                            <input type="hidden" name="tip" value="<?php echo $_smarty_tpl->tpl_vars['markasi']->value->tip;?>
" />
                            <?php } else { ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Marka Tipi
                                </label>
                                <div class="col-md-8">
                                    <select name="tip" class="form-control" required>
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['tip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tip']->key => $_smarty_tpl->tpl_vars['tip']->value) {
$_smarty_tpl->tpl_vars['tip']->_loop = true;
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['tip']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php }?>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Marka Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="adi" placeholder="">                                            
                                </div>
                            </div>
                        
                            <div class="form-group" >
                                <label class="col-md-4 control-label">Sıra No:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8" id="">
                                    <input type="number" class="form-control" min="1" name="sira" placeholder="">                                            
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="col-md-4 control-label">
                                    Aktif Kategoriler
                                </label>
                                <div class="col-md-8">
                                    <select name="kategoriler[]" class="form-control multiple" multiple="multiple" required>
                                        <option value="">Seçiniz</option>
                                        <?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
"><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Ekle</button> 
                                </div>
                            </div>
                        </form>
                    </div> 
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- Kategori Ekle -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

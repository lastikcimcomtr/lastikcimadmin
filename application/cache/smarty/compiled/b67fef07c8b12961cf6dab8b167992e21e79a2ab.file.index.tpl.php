<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:41:44
         compiled from "temalar/tema/views/modules/orders/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8728426775f58db9899a830-73752919%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b67fef07c8b12961cf6dab8b167992e21e79a2ab' => 
    array (
      0 => 'temalar/tema/views/modules/orders/index.tpl',
      1 => 1599655040,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8728426775f58db9899a830-73752919',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
    'sdata' => 0,
    'method' => 0,
    'durumlar' => 0,
    'durum' => 0,
    'orders' => 0,
    'order' => 0,
    'urunler' => 0,
    'odemedata' => 0,
    'data' => 0,
    'carikod' => 0,
    'siparis_id' => 0,
    'tatko_siparis_data' => 0,
    'hizmetler' => 0,
    'links' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58db989fa981_17816619',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58db989fa981_17816619')) {function content_5f58db989fa981_17816619($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php $_smarty_tpl->tpl_vars['method'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->router->fetch_method(), null, 0);?>
<style type="text/css">
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
        vertical-align: middle;
    }
</style>
<div class="container-fluid">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <form class="row" method="post" action="" autocomplete="off">
                            <div class="form-group col-md-2">
                                <label for="siparisid">Sipariş ID</label>
                                <input type="text" name="id" class="form-control" id="siparisid" placeholder="Sipariş ID" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['id'];?>
" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Müşteri İsmi</label>
                                <input type="text" name="isim" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['isim'];?>
" placeholder="Müşteri İsmi" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Cari Kodu</label>
                                <input type="text" name="cari" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['cari'];?>
" placeholder="Müşteri Cari Kodu" />
                            </div>
                            <div class="form-group col-md-2">
                                <label>Mail Adresi</label>
                                <input type="text" name="email" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['email'];?>
" placeholder="Müşteri Mail Adresi" />
                            </div>
                            <?php if ($_smarty_tpl->tpl_vars['method']->value!="hatalilar") {?>
                            <div class="form-group col-md-2">
                                <label for="siparisdurumu">Sipariş Durumu</label>
                                <select class="form-control" name="durumu">
                                    <option value="">Seçim Yapınız</option>
                                    <?php  $_smarty_tpl->tpl_vars['durum'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['durum']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['durumlar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['durum']->key => $_smarty_tpl->tpl_vars['durum']->value) {
$_smarty_tpl->tpl_vars['durum']->_loop = true;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['durum']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['durum']->value->id==$_smarty_tpl->tpl_vars['sdata']->value['durumu']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['durum']->value->durum;?>
</option>
                                    <?php } ?> 
                                </select>  
                            </div> 
                            <div class="form-group col-md-2">
                                <label for="durumguvenlik">Ödeme Tipi</label>
                                <select name="odeme_tipi" class="form-control" id="durumguvenlik">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['odeme_tipi']=="1") {?>selected<?php }?>>Havale</option>
                                    <option value="2" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['odeme_tipi']=="2") {?>selected<?php }?>>Kredi Kartı</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="durumodeme">Ödeme</label>
                                <select name="durum_odeme" class="form-control" id="durumodeme">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="1" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['durum_odeme']=="1") {?>selected<?php }?>>Başarılı</option>
                                    <option value="0" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['durum_odeme']=="0") {?>selected<?php }?>>Başarısız</option>
                                </select>
                            </div>
                            <?php }?>
                            <div class="form-group col-md-2">
                                <label for="postip">Pos</label>
                                <select name="pos_tip" class="form-control" id="postip">
                                    <option value="">Seçim Yapınız</option>
                                    <option value="NestPay" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['pos_tip']=="NestPay") {?>selected<?php }?>>NestPay</option>
                                    <option value="Gvp" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['pos_tip']=="Gvp") {?>selected<?php }?>>Gvp</option>
                                    <option value="Posnet" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['pos_tip']=="Posnet") {?>selected<?php }?>>Posnet</option>
                                    <option value="ziraatmvp" <?php if ($_smarty_tpl->tpl_vars['sdata']->value['pos_tip']=="ziraatmvp") {?>selected<?php }?>>ziraatmvp</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Tarih Aralığı</label>
                                <div class="input-group input-large date-picker input-daterange" data-date="<?php echo date('d.m.Y');?>
" data-date-format="dd.mm.yyyy">
                                    <input type="text" class="form-control" name="tarih_min" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['tarih_min'];?>
" />
                                    <span class="input-group-addon"> - </span>
                                    <input type="text" class="form-control" name="tarih_max" value="<?php echo $_smarty_tpl->tpl_vars['sdata']->value['tarih_max'];?>
" />
                                    <span style="border-width :1px;" class="input-group-addon text-danger" onclick="return $(this).closest('.form-group').find('input').val('')"> <i class="fa fa-times"></i> </span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">Arama Yap</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="table-responsive">                   
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
		                    <tr> 
		                        <th> Ref No  </th>
		                        <th> Üye Adı Soyadı </th>
		                        <th width="10%"> Sipariş Durumu </th>
		                        <th width="8%"> Sipariş Tarihi </th> 
		                        <th width="5%"> T.Tutar </th> 
		                        <th width="5%"> Ö.Tutar </th>
                                <th width="1%"> Ödeme </th>
                                <th width="1%"> Detay </th>
                                <th width="1%"> T.Durumu </th>
		                        <th width="1%"> Cari </th>
		                        <th width="1%"> Lstkc </th>
		                        <th width="1%"> Tedarikci </th>
		                        <th> İşlemler </th> 
		                    </tr>
		                </thead>
                        <tbody>
                            <tbody>
                                <?php  $_smarty_tpl->tpl_vars['order'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['order']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['order']->key => $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
?>

                                <?php $_smarty_tpl->tpl_vars['data'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->data), null, 0);?>
			                    <?php $_smarty_tpl->tpl_vars['urunler'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->urunler), null, 0);?>
			                    <?php $_smarty_tpl->tpl_vars['hizmetler'] = new Smarty_variable($_smarty_tpl->tpl_vars['urunler']->value[0]->hizmetler, null, 0);?>
			                    <?php $_smarty_tpl->tpl_vars['odemedata'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->odemedata), null, 0);?>
			                    <?php $_smarty_tpl->tpl_vars['odemebanka'] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->extraservices->paytype($_smarty_tpl->tpl_vars['odemedata']->value), null, 0);?>

			                    <?php if ($_smarty_tpl->tpl_vars['odemedata']->value->artitaksit) {?>
			                    <?php $_smarty_tpl->tpl_vars['installment'] = new Smarty_variable(($_smarty_tpl->tpl_vars['odemedata']->value->installment*1)+($_smarty_tpl->tpl_vars['odemedata']->value->artitaksit*1), null, 0);?>
			                    <?php } else { ?>
			                    <?php $_smarty_tpl->tpl_vars['installment'] = new Smarty_variable($_smarty_tpl->tpl_vars['odemedata']->value->installment, null, 0);?> 
			                    <?php }?>

                                <tr <?php if ($_smarty_tpl->tpl_vars['order']->value->durumu==2) {?> style="background-color:palegreen"<?php }
if ($_smarty_tpl->tpl_vars['order']->value->durumu==11) {?> style="background-color:#f9fe3e"<?php }
if ($_smarty_tpl->tpl_vars['order']->value->durumu==4||$_smarty_tpl->tpl_vars['order']->value->durumu==9) {?> style="background-color:#ff6f6f"<?php }?>>
                                    <td><?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
</td>
                                    <td title="<?php if ($_smarty_tpl->tpl_vars['data']->value->ticari_unvani) {?> <?php echo $_smarty_tpl->tpl_vars['data']->value->ticari_unvani;?>
 <?php } else {
echo ((string)$_smarty_tpl->tpl_vars['data']->value->adi)." ".((string)$_smarty_tpl->tpl_vars['data']->value->soyadi);?>
 <?php }?>">
									<?php if ($_smarty_tpl->tpl_vars['data']->value->ticari_unvani) {?> <?php echo truncate_str($_smarty_tpl->tpl_vars['data']->value->ticari_unvani,30);?>
 <?php } else {
echo truncate_str(((string)$_smarty_tpl->tpl_vars['data']->value->adi)." ".((string)$_smarty_tpl->tpl_vars['data']->value->soyadi),30);?>
 <?php }?></td>
                                    <td>
                                    	<select class="form-control" onchange="siparis_durum_degis(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);" id="siparis_durumu_<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
">
											<?php  $_smarty_tpl->tpl_vars['durum'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['durum']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['durumlar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['durum']->key => $_smarty_tpl->tpl_vars['durum']->value) {
$_smarty_tpl->tpl_vars['durum']->_loop = true;
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['durum']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['durum']->value->id==$_smarty_tpl->tpl_vars['order']->value->durumu) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['durum']->value->durum;?>
</option>
											<?php } ?> 
										</select>                                    	
                                    </td>
                                    <td><?php echo date('d.m.Y H:i',$_smarty_tpl->tpl_vars['order']->value->tarih);?>
</td>
                                    <td><?php echo $_smarty_tpl->tpl_vars['this']->value->extraservices->toplamtutar($_smarty_tpl->tpl_vars['order']->value->id);?>
</td>
                                    <td><?php if ($_smarty_tpl->tpl_vars['odemedata']->value->son_fiyat) {?> <?php echo $_smarty_tpl->tpl_vars['odemedata']->value->sepet_tutar;?>
 <?php } else { ?> - <?php }?></td>
                                    <?php if ($_smarty_tpl->tpl_vars['order']->value->posdetayid&&$_smarty_tpl->tpl_vars['order']->value->odeme_tipi==2) {?>
                                    <td align="center"><?php if ($_smarty_tpl->tpl_vars['order']->value->durum_odeme==1) {?><i class="fa fa-check" style="color: green;"></i><?php } else { ?><i class="fa fa-times" style="color: red;"></i><?php }?></td>
                                    <td>
                                        <a href="<?php echo site_url(('orders/posdetay/').($_smarty_tpl->tpl_vars['order']->value->posdetayid));?>
" data-target="#detay" data-toggle="modal" class="btn btn-sm btn-warning">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                    <?php } elseif ($_smarty_tpl->tpl_vars['order']->value->odeme_tipi==2) {?>
                                    <td colspan="2" style="background-color: #fc8383;"><center><b>Hatalı</b></center></td>
                                    <?php } else { ?>
                                    <td colspan="2" style="background-color: #ededed;"><center><b>Havale</b></center></td>
                                    <?php }?>
                                    <td align="center">
                                        <?php if ($_smarty_tpl->tpl_vars['order']->value->kargo_tipi) {
echo date('d.m.Y H:i',$_smarty_tpl->tpl_vars['order']->value->kargo_tipi);
} else { ?>-<?php }?>
                                    </td>
                                    <td align="center">
                                    	<?php if ($_smarty_tpl->tpl_vars['order']->value->caridata) {?>
											<?php $_smarty_tpl->tpl_vars['carikod'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->caridata), null, 0);?>
											<?php $_smarty_tpl->tpl_vars['carikod'] = new Smarty_variable($_smarty_tpl->tpl_vars['carikod']->value->kod, null, 0);?>
											<a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', '<?php echo $_smarty_tpl->tpl_vars['carikod']->value;?>
 Kodu ile Bir Cari Hesap Zaten Açılmış', 'error');" title="cari hesap açılmış"><i class="fa fa-user"></i></a>
										<?php } else { ?>
											<a class="btn btn-icon-only btn-success" href="javascript:carihesapac(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);"><i class="fa fa-user"></i></a>
										<?php }?>
                                    </td>
                                    <td align="center">
                                    	<?php if ($_smarty_tpl->tpl_vars['order']->value->siparis_data) {?>
											<?php $_smarty_tpl->tpl_vars['siparis_id'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->siparis_data), null, 0);?>
											<?php $_smarty_tpl->tpl_vars['siparis_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['siparis_id']->value->siparis_id_alinan, null, 0);?>
											<a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', 'LC-<?php echo $_smarty_tpl->tpl_vars['siparis_id']->value;?>
 Numarası ile Bir Sipariş Zaten Girilmiş', 'error');" title="sipariş işlenmiş"><i class="fa fa-gears"></i></a>
										<?php } else { ?>
											<a class="btn btn-icon-only btn-success" href="javascript:carisiparisac(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);"><i class="fa fa-gears"></i></a>
										<?php }?>
                                    </td>
                                    <td align="center">
                                    	<?php if ($_smarty_tpl->tpl_vars['order']->value->tatko_siparis_data) {?>
                                            <?php $_smarty_tpl->tpl_vars['tatko_siparis_data'] = new Smarty_variable(json_decode($_smarty_tpl->tpl_vars['order']->value->tatko_siparis_data), null, 0);?>
					                        <a class="btn btn-icon-only btn-danger" href="javascript:swal('Hata', '<?php echo $_smarty_tpl->tpl_vars['tatko_siparis_data']->value->urun_maliyet;?>
 ₺ maliyet ile <?php echo $_smarty_tpl->tpl_vars['tatko_siparis_data']->value->tedarikci;?>
 tedarikcisine gönderilmiş.', 'error');" title="sipariş işlenmiş"><i class="fa fa-envelope"></i></a>
				                        <?php } else { ?>
				                        	<a class="btn btn-icon-only btn-success" href="javascript:siparistedarikci(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);"><i class="fa fa-envelope"></i></a>
				                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['order']->value->ups_barcode) {?>
                                            <a class="btn btn-icon-only btn-danger" href="javascript:barkodexist(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);" title="sipariş işlenmiş"><i class="fa fa-barcode"></i></a>
                                        <?php } else { ?>
                                            <a class="btn btn-icon-only btn-success" href="javascript:siparisbarcode(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);"><i class="fa fa-barcode"></i></a>
                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['order']->value->irsaliye_data) {?>
                                            <a class="btn btn-icon-only btn-danger" href="javascript:irsaliyeexist(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);" title="irsaliye işlenmiş"><i class="fa fa-file-text-o"></i></a>
                                        <?php } else { ?>
                                            <a class="btn btn-icon-only btn-success" href="javascript:siparisirsaliye(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
);"><i class="fa fa-file-text-o"></i></a>
                                        <?php }?>
                                    </td>
                                    <td>   
                                    	<a href="/orders/duzenle/<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
" target="_blank" style="background-color: #71b100; border-color: #71b100; color: white;" title="Sipariş Düzenle" class="btn btn-sm btn-default">
					                        <i class="fa fa-pencil"></i>
					                    </a>                                 	
                                    	<a onclick="siparisYazdir( <?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
 );" style="background-color: #703688; border-color: #703688; color: white;" title="Sipariş Detaylarını Yazdır" class="btn btn-sm btn-default" >
					                        <i class="fa fa-print"></i>
					                    </a>
                                        
                                        <a style="background-color: <?php if ($_smarty_tpl->tpl_vars['hizmetler']->value) {?>#f5861d<?php } else { ?>#E73434<?php }?>; border-color: <?php if ($_smarty_tpl->tpl_vars['hizmetler']->value) {?>#f5861d<?php } else { ?>#E73434<?php }?>; color: white;" data-toggle="modal" href="<?php echo site_url(('orders/montaj_detay/').($_smarty_tpl->tpl_vars['order']->value->id));?>
" data-target="#montaj_detay" title="Montaj Detayları" class="btn btn-sm btn-default">
                                            <i class="fa fa-cog"></i>
                                        </a>
                                        
                                        <a style="background-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->kargo_takip) {?>#1BBC9B<?php } else { ?>#E73434<?php }?>; border-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->kargo_takip) {?>#1BBC9B<?php } else { ?>#E73434<?php }?>; color: white;" data-toggle="modal" href="<?php echo site_url(('orders/kargo_detaylari/').($_smarty_tpl->tpl_vars['order']->value->id));?>
" data-target="#kargo_detay" title="Kargo Detayları" class="btn btn-sm btn-default">
                                            <i class="fa fa-truck"></i>
                                        </a>

                                        <a style="background-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->siparis_notu) {?>#703688<?php } else { ?>#E73434<?php }?>; border-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->siparis_notu) {?>#703688<?php } else { ?>#E73434<?php }?>; color: white;" data-toggle="modal" href="<?php echo site_url(('orders/siparis_notu/').($_smarty_tpl->tpl_vars['order']->value->id));?>
" data-target="#siparis_notu" title="Sipariş Notu" class="btn btn-sm btn-default">
                                            <i class="fa fa-list-ol"></i>
                                        </a>
										
                                        <a style="background-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->tedarik_not) {?>#703688<?php } else { ?>#E73434<?php }?>; border-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->siparis_notu) {?>#703688<?php } else { ?>#E73434<?php }?>; color: white;" data-toggle="modal" href="<?php echo site_url(('orders/tedarik_notu/').($_smarty_tpl->tpl_vars['order']->value->id));?>
" data-target="#tedarik_notu" title="Tedarik Notu" class="btn btn-sm btn-default">
                                            <i class="fa fa-list-alt"></i>
                                        </a>

                                        <a style="background-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->tedarikci_odeme) {?>#1BBC9B<?php } else { ?>#E73434<?php }?>; border-color: <?php if ($_smarty_tpl->tpl_vars['order']->value->tedarikci_odeme) {?>#1BBC9B<?php } else { ?>#E73434<?php }?>; color: white;" onclick="toggleTedarikciOdeme(<?php echo $_smarty_tpl->tpl_vars['order']->value->id;?>
,this);" title="Tedarikçi Ödeme" class="btn btn-sm btn-default">
                                            <i class="fa fa-money"></i>
                                        </a>                							
                                        
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </tbody>
                    </table>
                    <?php echo $_smarty_tpl->tpl_vars['links']->value;?>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="montaj_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="kargo_detay" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="siparis_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tedarik_notu" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="fatura_bilgileri" role="basic" aria-hidden="true">
    <div class="modal-dialog" style="width: 75%;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carihesap" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="carisiparis" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="siparisbarcode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="siparisirsaliye" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>
                    <center>
                        <i class="fa fa-spinner fa-2x fa-spin"></i>
                        <br />
                        <p>Lütfen Bekleyiniz...</p>
                    </center>
                </p>
            </div>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
    $.ajaxSetup ({
        cache: false
    });
    function siparisirsaliye(id){
        var siparisurl = "<?php echo site_url('orders/siparisirsaliye');?>
/"+id;
        swal({
            title: 'Lastikcim İrsaliye',
            text: 'Sipariş İrsaliyesi Göndermek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {
            if (isConfirm) {
                $('#siparisirsaliye').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }
    function carihesapac(id) {
        var hesapurl = "<?php echo site_url('orders/carihesapac');?>
/"+id;
        swal({
            title: 'Cari Hesap',
            text: 'Mikroya Cari Hesap Açmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carihesap').modal('show').find('.modal-body').load(hesapurl);
            }

        });
    }

    function carisiparisac(id) {
        var siparisurl = "<?php echo site_url('orders/carisiparisac');?>
/"+id;
        swal({
            title: 'Lastikcim Sipariş',
            text: 'Lastikcim-e Cari Sipariş Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }



    function siparisbarcode(id) {
        var siparisurl = "<?php echo site_url('orders/siparisbarcode');?>
/"+id;
        swal({
            title: 'Lastikcim Barkod',
            text: 'Sipariş Barkodu Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#siparisbarcode').modal('show').find('.modal-body').load(siparisurl);
            }

        });
    }

    function caritatkosiparisac(id) {
        var tatkosiparisurl = "<?php echo site_url('orders/carisiparisac');?>
/"+id+'/tatko';
        swal({
            title: 'Tatko Sipariş',
            text: 'Tatko-ya Cari Sipariş Girmek İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {

            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(tatkosiparisurl);
            }

        });
    }

    function siparistedarikci(id){
        var tedarikcisiparisurl = "<?php echo site_url('orders/tedarikciyeiletformu');?>
/"+id;
        swal({
            title: 'Tedarikçiye Sipariş Aktar',
            text: 'Siparişi Tedarikçiye Aktarmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            confirmButtonText: 'Evet',
            cancelButtonText: 'Hayır'
        }).then((isConfirm) => {
            if (isConfirm) {
                $('#carisiparis').modal('show').find('.modal-body').load(tedarikcisiparisurl);
            }
        });
    }

    function barkodexist(siparis_id){
        swal({
            title: 'Barkod Zaten Mevcut',
            text: 'Barkodu Tekrar Oluşturmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            icon: 'warning',
            buttons: ["Hayır", "Yeni Barkod"]
        }).then((isConfirm) => {
            if (isConfirm) {
                siparisbarcode(siparis_id);
            }
        });
    }

    function irsaliyeexist(siparis_id){
        swal({
            title: 'İrsaliye Zaten Mevcut',
            text: 'İrsaliyeyi Tekrar Oluşturmak İstediğinize Emin misiniz?',
            buttons: true,
            showCancelButton: true,
            icon: 'warning',
            buttons: ["Hayır", "Yeni İrsaliye"],
        }).then((isConfirm) => {
            if (isConfirm) {
                siparisirsaliye(siparis_id);
            }
        });
    }
<?php echo '</script'; ?>
>

<iframe id="frame_yazdir" name="frame_yazdir"  src="" style="visibility: hidden; width: 0px; height: 0px;"></iframe>

<?php echo '<script'; ?>
 type="text/javascript">
	function siparisYazdir(id){
	    $('#frame_yazdir').attr("src","<?php echo site_url('orders/siparisdetay');?>
/" + id);
	    return false;
	} 

    function toggleTedarikciOdeme(order_id,element) {
        element = $(element);
        $.ajax({
            type:'POST',
            url:'/orders/toggle_tedarikci_odeme/',
            dataType: 'json',
            data: { order_id : order_id },
            success:function(cevap){
                if (cevap) {
                    if(cevap.status == 'error'){
                        element.css('background-color','#E73434').css('border-color','#E73434');
                    }else{
                        element.css('background-color','#1BBC9B').css('border-color','#1BBC9B');
                    }
                    swal({
                        title: 'Başarılı',
                        text: cevap.message,
                        icon: cevap.status,
                    });
                }
            }
        });
    }
<?php echo '</script'; ?>
>


<?php echo '<script'; ?>
 type="text/javascript">
    function siparis_durum_degis(sipid) {
		var select = $("#siparis_durumu_"+sipid);
        var durum = $("#siparis_durumu_"+sipid+" option:selected").val();
        var send_data = {
            order_id:sipid,
            durum:durum
        };
        $.get( "<?php echo site_url('ajax/order_durum');?>
", send_data ).done(function( data ) {
			data = JSON.parse(data);
			if(data.status == 'error'){
                 select.val(data.durumu);
				 swal("Başarısız",data.msg,"warning");
                 
                
			}else{
				 swal({
				  title: "Başarılı",
				  text: "Sipariş Durumu Güncellendi.",
				  timer: 1200
				});
				 if(durum == 4 || durum==9){
				 	 dataLayer.push({
                  'ecommerce': {
                    'refund': {                       
                      'actionField': {'id': sipid.toString()}         // Transaction ID. Required for purchases and refunds.
	                    }
	                  }
	                });
				 }
				if(durum == 9 || durum==4){
					select.closest('tr').css('background-color','#ff6f6f');
				}
				if(durum == 2){
					select.closest('tr').css('background-color','palegreen');
				}
			}
            // console.log(data);
        });
    }
<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

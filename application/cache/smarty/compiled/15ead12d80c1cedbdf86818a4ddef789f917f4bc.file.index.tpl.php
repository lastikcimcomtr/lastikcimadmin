<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:28:06
         compiled from "temalar/tema/views/modules/home/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8765312195f58d866b85205-37596851%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15ead12d80c1cedbdf86818a4ddef789f917f4bc' => 
    array (
      0 => 'temalar/tema/views/modules/home/index.tpl',
      1 => 1599657456,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8765312195f58d866b85205-37596851',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'total_onay_bekleyen' => 0,
    'kargoya_verilmeyen' => 0,
    'iptal_talebi' => 0,
    'kargoya_verildi' => 0,
    'kargoda_bekleyen' => 0,
    'geciken_hazirlaniyor' => 0,
    'geciken_tedarik' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58d866b9c028_57491309',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58d866b9c028_57491309')) {function content_5f58d866b9c028_57491309($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<style type="text/css">	
#chartdiv {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
#ziyaretcilerimiz {
    width: 100%;
    height: 500px;
    font-size: 11px;
}
</style>
<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN PAGE BASE CONTENT -->

        <div class="row" style="margin-bottom: 10px">
      <div class="col-md-6">
        <div class="row">
                <div class="col-md-6">
                    <a href="/orders/index" class="btn btn-warning font-dark" target="_blank">Siparişlere Git</a>
                    <a href="/home/arama_trend" class="btn btn-primary" target="_blank">Arama Trendleri</a>
                </div>
        </div>
        </div>
      </div>
      <div class="row">
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-green-haze"></i>
                          <span class="font-sm bold font-green-haze">Yeni Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['total_onay_bekleyen']->value['adet'];?>
) Adet</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['total_onay_bekleyen']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-yellow-gold"></i>
                          <span class="font-sm bold font-yellow-gold">Kargoya Verilmeyenler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['kargoya_verilmeyen']->value['adet'];?>
) Adet</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['kargoya_verilmeyen']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-red-mint"></i>
                          <span class="font-sm bold font-red-mint">İptal/İade Talebi</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['iptal_talebi']->value['adet'];?>
) Adet</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['iptal_talebi']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-yellow-gold"></i>
                          <span class="font-sm bold font-yellow-gold">Kargoya Verildi Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['kargoya_verildi']->value['adet'];?>
) Adet</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['kargoya_verildi']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-blue-steel"></i>
                          <span class="font-sm bold font-blue-steel">Geciken Kargo Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['kargoda_bekleyen']->value['adet'];?>
) Adet</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['kargoda_bekleyen']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 p-0">
              <div class="portlet light bordered">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bar-chart font-blue-steel"></i>
                          <span class="font-sm bold font-blue-steel">Geciken Siparişler</span>
                      </div>
                  </div>
                  <div class="portlet-body">  
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['geciken_hazirlaniyor']->value['adet'];?>
) Adet Hazırlanıyor</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['geciken_hazirlaniyor']->value['link'];?>
" target="_blank">Listele</a></div></div>
                    <div class="row"><div class="col-md-8">(<?php echo $_smarty_tpl->tpl_vars['geciken_tedarik']->value['adet'];?>
) Adet Tedarik Sürecinde</div><div class="col-md-4"><a href="<?php echo $_smarty_tpl->tpl_vars['geciken_tedarik']->value['link'];?>
" target="_blank">Listele</a></div></div>
                  </div>
              </div>
          </div>
      </div>
        
        <div id="arama_kaydet" class="modal fade" tabindex="-1" data-width="400" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Arama Kaydetme</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">Arama İsmi</label>
                                        <input type="text" id="arama_ismi" value="" placeholder="Arama için isim giriniz" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="lastikkaydetmesonuc"></div>
                        <button type="button" data-dismiss="modal" class="btn dark btn-outline">Kapat</button>
                        <button type="button" class="btn red" onclick="arama_kaydet();">Kaydet</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div id="lastikler">
            
        </div>              
        <!-- END PAGE BASE CONTENT -->
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

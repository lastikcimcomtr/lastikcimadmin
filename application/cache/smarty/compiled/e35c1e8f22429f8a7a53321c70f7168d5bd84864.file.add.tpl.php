<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-10 12:21:10
         compiled from "temalar/tema/views/modules/products/add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:165505105f5a1a36360a42-76840228%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e35c1e8f22429f8a7a53321c70f7168d5bd84864' => 
    array (
      0 => 'temalar/tema/views/modules/products/add.tpl',
      1 => 1599655043,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '165505105f5a1a36360a42-76840228',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'status' => 0,
    'message' => 0,
    'parametregruplari' => 0,
    'item' => 0,
    'ip' => 0,
    'boy' => 0,
    'tipi' => 0,
    'tip' => 0,
    'kategoriler' => 0,
    'kategori' => 0,
    'markalar' => 0,
    'marka' => 0,
    'mevsimler' => 0,
    'mevsim' => 0,
    'tabanlar' => 0,
    'taban' => 0,
    'yanaklar' => 0,
    'yanak' => 0,
    'motorsikleturleri' => 0,
    'motorsikleturu' => 0,
    'jantcaplari' => 0,
    'jantcapi' => 0,
    'urun' => 0,
    'kurlar' => 0,
    'kur' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f5a1a3640c962_50778864',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f5a1a3640c962_50778864')) {function content_5f5a1a3640c962_50778864($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
	<div class="page-content">
		<!-- BEGIN PAGE BASE CONTENT -->
		<div class="row">
			<div class="col-md-12">

				<form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-shopping-cart"></i> Lastik Ekleme 
							</div>
						</div>
						<div class="portlet-body">
							<?php if ($_smarty_tpl->tpl_vars['status']->value&&$_smarty_tpl->tpl_vars['message']->value) {?>
							<div class="note note-<?php echo $_smarty_tpl->tpl_vars['status']->value;?>
">
								<p> <?php echo $_smarty_tpl->tpl_vars['message']->value;?>
 </p>
							</div>
							<?php }?>
							<div class="tabbable-bordered">
								<ul class="nav nav-tabs" id="tab_basliklari">
									<?php if ($_smarty_tpl->tpl_vars['parametregruplari']->value) {?>
									<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['parametregruplari']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['item']->iteration=0;
 $_smarty_tpl->tpl_vars['item']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['item']->iteration++;
 $_smarty_tpl->tpl_vars['item']->index++;
 $_smarty_tpl->tpl_vars['item']->first = $_smarty_tpl->tpl_vars['item']->index === 0;
 $_smarty_tpl->tpl_vars['item']->last = $_smarty_tpl->tpl_vars['item']->iteration === $_smarty_tpl->tpl_vars['item']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['first'] = $_smarty_tpl->tpl_vars['item']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['last'] = $_smarty_tpl->tpl_vars['item']->last;
?>
									<li <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['pgrup']['first']) {?>class="active"<?php } else { ?>data-gcategories="<?php echo $_smarty_tpl->tpl_vars['item']->value->categories;?>
"  style="display:none"<?php }?>>
										<a href="#tab_parametre<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" data-toggle="tab"> <?php echo $_smarty_tpl->tpl_vars['item']->value->grup_adi;?>
 </a>
									</li>
									<?php } ?>
									<?php }?>
								</ul>
								<div class="tab-content">
									<?php if ($_smarty_tpl->tpl_vars['parametregruplari']->value) {?>
									<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['parametregruplari']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['item']->iteration=0;
 $_smarty_tpl->tpl_vars['item']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
 $_smarty_tpl->tpl_vars['item']->iteration++;
 $_smarty_tpl->tpl_vars['item']->index++;
 $_smarty_tpl->tpl_vars['item']->first = $_smarty_tpl->tpl_vars['item']->index === 0;
 $_smarty_tpl->tpl_vars['item']->last = $_smarty_tpl->tpl_vars['item']->iteration === $_smarty_tpl->tpl_vars['item']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['first'] = $_smarty_tpl->tpl_vars['item']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pgrup']['last'] = $_smarty_tpl->tpl_vars['item']->last;
?>
									<div class="tab-pane <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['pgrup']['first']) {?>active<?php }?>" id="tab_parametre<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
">
										<?php if ($_smarty_tpl->tpl_vars['item']->value->parametreler) {?>

										<?php  $_smarty_tpl->tpl_vars['ip'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ip']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item']->value->parametreler; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ip']->key => $_smarty_tpl->tpl_vars['ip']->value) {
$_smarty_tpl->tpl_vars['ip']->_loop = true;
?>
										<div <?php if ($_smarty_tpl->tpl_vars['ip']->value->input_type!="kategori") {?>data-categories="<?php echo $_smarty_tpl->tpl_vars['ip']->value->categories;?>
" style="display:none"<?php }?>  >
											
											<?php if ($_smarty_tpl->tpl_vars['ip']->value->input_type!='') {?>
											<div class="form-group" id="<?php if ($_smarty_tpl->tpl_vars['ip']->value->silik_id!='') {?>silinebilir_<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;
}?>">
												<?php if ($_smarty_tpl->tpl_vars['ip']->value->input_type=="checkbox") {?>
												<label class="col-md-2 control-label" style="margin-top:-5px;">
													<?php } else { ?>
													<label class="col-md-2 control-label">
														<?php }?>
														<?php echo $_smarty_tpl->tpl_vars['ip']->value->adi;?>
:
														<?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?><span class="required"> * </span><?php }?>
													</label>
													<?php if ($_smarty_tpl->tpl_vars['ip']->value->input_type=="ckeditor"||$_smarty_tpl->tpl_vars['ip']->value->input_type=="textbox") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("6", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="resim") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("10", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="url_duzenle") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("4", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="seo_etiketle") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("8", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="uyumlu_yazici") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("8", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="kargo_bedeli") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("3", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="text_dsb") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("4", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="urun_adi") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("4", null, 0);?>
													<?php } elseif ($_smarty_tpl->tpl_vars['ip']->value->input_type=="fiyat") {?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("3", null, 0);?>
													<?php } else { ?>
													<?php $_smarty_tpl->tpl_vars['boy'] = new Smarty_variable("2", null, 0);?>
													<?php }?>
													<div class="col-md-<?php echo $_smarty_tpl->tpl_vars['boy']->value;?>
">

														<?php $_smarty_tpl->tpl_vars['tipi'] = new Smarty_variable($_smarty_tpl->tpl_vars['ip']->value->input_type, null, 0);?>
														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="text") {?>
														<input type="text" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" class="form-control<?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required_chk <?php }?>" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?> />
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="text_dsb") {?>
														<input type="text" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" readonly="readonly" class="form-control <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required_chk <?php }?>" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?> />
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="urun_adi") {?>																		
														<div class="row">
															<div class="col-md-9">
																<input type="text" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" readonly="readonly" class="form-control <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required_chk <?php }?>" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?> />
															</div>
															<div class="col-md-3">
																<input type="checkbox" onchange="urun_adi_disable();" class="form-control" id="urun_adi_otomatik" value="1" checked="">
																<label for="urun_adi_otomatik">Otomatik</label>
															</div>
														</div>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="kategori") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Kategoriler" onchange="kategoriler(1, '<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
', 'parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]')" class="form-control form-filter input-sm select2me <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required_chk <?php }?>" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['kategori'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kategori']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kategoriler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kategori']->key => $_smarty_tpl->tpl_vars['kategori']->value) {
$_smarty_tpl->tpl_vars['kategori']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['kategori']->value->id;?>
" <?php if (1==$_smarty_tpl->tpl_vars['kategori']->value->id) {?>selected<?php }?> ><?php echo $_smarty_tpl->tpl_vars['kategori']->value->adi;?>
</option>
															<?php } ?>
														</select>
														<div id="alt_kategori"></div>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="marka") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Markalar" onchange="markalar(1, '<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
')" class="form-control select2-allow-clear form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['marka'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['marka']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['markalar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['marka']->key => $_smarty_tpl->tpl_vars['marka']->value) {
$_smarty_tpl->tpl_vars['marka']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['marka']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['marka']->value->adi;?>
</option>
															<?php } ?>
														</select>
														<div id="alt_marka"></div>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="mevsim") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" onchange="desenler('<?php echo $_smarty_tpl->tpl_vars['tip']->value;?>
');" data-placeholder="Mevsimler" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['mevsim'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['mevsim']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mevsimler']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['mevsim']->key => $_smarty_tpl->tpl_vars['mevsim']->value) {
$_smarty_tpl->tpl_vars['mevsim']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['mevsim']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['mevsim']->value->adi;?>
</option>
															<?php } ?>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="desen") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Desenler" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="taban") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Tabanlar" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['taban'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['taban']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tabanlar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['taban']->key => $_smarty_tpl->tpl_vars['taban']->value) {
$_smarty_tpl->tpl_vars['taban']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['taban']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['taban']->value->deger;?>
</option>
															<?php } ?>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="yanak") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Yanaklar" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['yanak'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['yanak']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['yanaklar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['yanak']->key => $_smarty_tpl->tpl_vars['yanak']->value) {
$_smarty_tpl->tpl_vars['yanak']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['yanak']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['yanak']->value->deger;?>
</option>
															<?php } ?>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="yapisi") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Yapısı" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<option value="r">R</option>
															<option value="Zr">ZR</option>
															<option value="-">-</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="tt_tl") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="TT TL" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<option value="TT">TT</option>
															<option value="TL">TL</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="on_arka") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Ön / Arka" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<option value="on">Ön</option>
															<option value="arka">Arka</option>
															<option value="onarka">Ön + Arka</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="motosiklet_turu") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Motosikler Türleri" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="">Seçiniz</option>
															<?php  $_smarty_tpl->tpl_vars['motorsikleturu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['motorsikleturu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['motorsikleturleri']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['motorsikleturu']->key => $_smarty_tpl->tpl_vars['motorsikleturu']->value) {
$_smarty_tpl->tpl_vars['motorsikleturu']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['motorsikleturu']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['motorsikleturu']->value->deger;?>
</option>
															<?php } ?>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="jant_capi") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" data-placeholder="Jant Çapı" class="form-control form-filter input-sm select2me" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value=""> Seçiniz </option>
															<?php  $_smarty_tpl->tpl_vars['jantcapi'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['jantcapi']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['jantcaplari']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['jantcapi']->key => $_smarty_tpl->tpl_vars['jantcapi']->value) {
$_smarty_tpl->tpl_vars['jantcapi']->_loop = true;
?>
															<option value="<?php echo $_smarty_tpl->tpl_vars['jantcapi']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['jantcapi']->value->deger;?>
</option>
															<?php } ?>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="run_flat") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option data-rft = "" value=""> Yok </option>
															<option value="RFT" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="RFT") {?>selected<?php }?>>RFT</option>
															<option value="ROF" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="ROF") {?>selected<?php }?>>ROF</option>
															<option value="SSR" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="SSR") {?>selected<?php }?>>SSR</option>
															<option value="ZP" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="ZP") {?>selected<?php }?>>ZP</option>
															<option value="HRS" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="HRS") {?>selected<?php }?>>HRS</option>
															<option value="EXT" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="EXT") {?>selected<?php }?>>EXT</option>
															<option value="RunFlat" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="RunFlat") {?>selected<?php }?>>RunFlat</option>
															<option value="P-RFT" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="P-RFT") {?>selected<?php }?>>P-RFT</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="xl_rf") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value=""> Yok </option>
															<option value="XL">XL</option>
															<option value="RF">RF</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="checkbox") {?>
														<input type="checkbox" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" value="1" checked <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>/>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="durum") {?>
														<select style="height:34px;" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="1" selected="">Aktif</option>
															<option value="0">Pasif</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="vitrin") {?>
														<select style="height:34px;" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value="1">Evet</option>
															<option value="0" selected="selected">Hayır</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="textbox") {?>
														<textarea class="form-control" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" rows="6" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required <?php }?>></textarea>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="ckeditor") {?>
														<textarea class="ckeditor form-control" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" rows="6" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required <?php }?>>{desen_aciklama}</textarea>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="m_s") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value=""> Seçiniz </option>
															<option value="var"> Var </option>
															<option selected value="yok"> Yok </option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="var_yok") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value=""> Seçiniz </option>
															<option value="var"> Var </option>
															<option value="yok"> Yok </option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="seal") {?>
														<select id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" class="form-control form-filter input-sm select2me" data-placeholder="Seçiniz" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
															<option value=""> Seçiniz </option>
															<option value="ContiSeal" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="ContiSeal") {?>selected<?php }?>>ContiSeal</option>
															<option value="Seal" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="Seal") {?>selected<?php }?>>Seal</option>
															<option value="s-i" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="s-i") {?>selected<?php }?>>S-I</option>
															<option value="SealGuard" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="SealGuard") {?>selected<?php }?>>Seal Guard</option>
															<option value="SelfSeal" <?php if ($_smarty_tpl->tpl_vars['urun']->value->{$_smarty_tpl->tpl_vars['ip']->value->silik_id}=="SelfSeal") {?>selected<?php }?>>SelfSeal</option>
														</select>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="fiyat") {?>
														<div class="row">
															<div class="col-md-8">
																<input type="text" class="form-control <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?> required_chk <?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?> />
															</div>
															<div class="col-md-4">
																<select name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
_birim]" class="form-control" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
																	<option value=""> Seçiniz </option>
																	<?php  $_smarty_tpl->tpl_vars['kur'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['kur']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['kurlar']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['kur']->key => $_smarty_tpl->tpl_vars['kur']->value) {
$_smarty_tpl->tpl_vars['kur']->_loop = true;
?>
																	<option value="<?php echo $_smarty_tpl->tpl_vars['kur']->value->kod;?>
" <?php if ($_smarty_tpl->tpl_vars['kur']->value->kod=="TRY") {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['kur']->value->kod;?>
</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<?php }?>

														

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="resim") {?>
														<input type="hidden" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]" value="" />

														<div class="fileinput fileinput-new" data-provides="fileinput" style="float:left;">
															<div class="input-group input-large">
																<div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
																	<i class="fa fa-file fileinput-exists"></i>&nbsp;
																	<span class="fileinput-filename"> </span>
																</div>
																<span class="input-group-addon btn default btn-file">
																	<span class="fileinput-new"> Resim Seçin </span>
																	<span class="fileinput-exists"> Değiştir </span>
																	<input type="file" name="resim_<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" id="<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
" <?php if ($_smarty_tpl->tpl_vars['ip']->value->required==1) {?>required<?php }?>>
																</span>
																<a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Sil </a>
															</div>
														</div>
														<div style="float:left;margin-left:10px;">
															<input style="width: 342px; float:left;" type="text" class="form-control" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
_baslik]" placeholder="Resim Başlığı" >
															<input style="float: left; margin-left: 10px; width: 342px;" type="text" class="form-control" name="parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
_aciklama]" placeholder="Resim Alt Etiket" >
														</div>
														<?php }?>

														<?php if ($_smarty_tpl->tpl_vars['tipi']->value=="url_duzenle") {?>
														<span style="width:auto;float:left;padding-top: 6px;">https://www.lastikcim.com.tr/</span>
														<span style="width:auto;float:left;line-height: 30px;padding: 0 10px 0 5px;" id="url_duzenle_inpt"><?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
<input type="text" class="form-control" name="parametreler[url_duzenle]" placeholder="" /></span>
														<button type="button" style="float:left;display:none;" id="seo_url_kaydetme_button"  onclick="inputu_gizle('parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]');" class="btn default">Kaydet</button>
														<button type="button" style="float:left;" id="seo_url_duzenleme_button" class="btn blue" onclick="inputyap('parametreler[<?php echo $_smarty_tpl->tpl_vars['ip']->value->silik_id;?>
]','<?php echo $_smarty_tpl->tpl_vars['ip']->value->varsayilan;?>
')">Düzenle</button>

														<?php echo '<script'; ?>
>
															function inputyap(name,value){
																$("#url_duzenle_inpt").html('<input type="text" class="form-control" name="'+name+'" placeholder="" value="'+value+'" />');
																$("#url_duzenle_inpt").attr('onclick','');
																$("#seo_url_kaydetme_button").show();
																$("#seo_url_duzenleme_button").hide();
															}

															function inputu_gizle(name){ 
																var deger = $("input[name='"+name+"']").val();
																if(deger != ""){
																	$("#url_duzenle_inpt").html('<input type="hidden" class="form-control" name="'+name+'" placeholder="" value="'+deger+'">'+deger);
																	$("#seo_url_duzenleme_button").attr('onclick','inputyap("'+name+'","'+deger+'")');
																	$("#seo_url_kaydetme_button").hide();
																	$("#seo_url_duzenleme_button").show();
																}else{
																	alert("Url Düzenleme Alanı Boş Geçilemez..");
																}
															}

														<?php echo '</script'; ?>
>
														<?php }?>
													</div>
												</div>
												<?php }?>
											</div>
											<?php } ?>
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-2 control-label"></label>
													<div class="col-md-10">
														<button type="submit" class="btn blue">Kaydet</button>
														<?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['pgrup']['last']) {?>
														<button type="button" onclick="sonraki_tab(<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
);" class="btn default">Devam Et</button>
														<?php }?>
													</div>
												</div> 
											</div>
											<?php }?>
										</div>
										<?php } ?>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
			<!-- END PAGE BASE CONTENT -->
		</div>
		<?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</div>
	<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

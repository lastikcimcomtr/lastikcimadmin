<?php /* Smarty version Smarty-3.1.21-dev, created on 2020-09-09 13:59:10
         compiled from "temalar/tema/views/modules/products/brand/renderpattern.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4109855315f58dfae2cb6d8-54977163%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d3ea98a9bc984e52680c34e37f517fa76fb791a' => 
    array (
      0 => 'temalar/tema/views/modules/products/brand/renderpattern.tpl',
      1 => 1599655221,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4109855315f58dfae2cb6d8-54977163',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'desen' => 0,
    'input_parse' => 0,
    'input_row' => 0,
    'input_group' => 0,
    'k' => 0,
    'k_2' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5f58dfae2f5bf8_32209681',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f58dfae2f5bf8_32209681')) {function content_5f58dfae2f5bf8_32209681($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("base/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div class="container-fluid">
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->

        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-md-3 p-0" style="font-size: 9px">
                <img src="http://admin.lastikcim.com.tr/temalar/tema/assets/img/page_design.png?v=0.02" class="img-fluid" style="width: 100%">
            </div>
            <div class="col-md-9">
                <div class="portlet">
                    <div class="portlet-body">
                        <form class="form-horizontal form-row-seperated" enctype="multipart/form-data" method="post" action="<?php echo site_url('products/brand/renderpattern');?>
/<?php echo $_smarty_tpl->tpl_vars['desen']->value->id;?>
">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Desen Adı:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-10" id="kategori_adi">
                                    <input type="text" class="form-control" name="adi" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['desen']->value->adi;?>
" disabled>                                            
                                </div>
                            </div>
                            <?php  $_smarty_tpl->tpl_vars['input_row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['input_row']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['input_parse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['input_row']->key => $_smarty_tpl->tpl_vars['input_row']->value) {
$_smarty_tpl->tpl_vars['input_row']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['input_row']->key;
?>
                                <?php  $_smarty_tpl->tpl_vars['input_group'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['input_group']->_loop = false;
 $_smarty_tpl->tpl_vars['k_2'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['input_row']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['input_group']->key => $_smarty_tpl->tpl_vars['input_group']->value) {
$_smarty_tpl->tpl_vars['input_group']->_loop = true;
 $_smarty_tpl->tpl_vars['k_2']->value = $_smarty_tpl->tpl_vars['input_group']->key;
?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"><?php echo $_smarty_tpl->tpl_vars['input_group']->value['label'];?>
:
                                        </label>
                                        <div class="col-md-10">
                                            <?php if ($_smarty_tpl->tpl_vars['input_group']->value['type']=='input') {?>
                                            <input type="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['input-type'];?>
"  class="form-control" name="row[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['k_2']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['input_group']->value['name'];?>
]" placeholder="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['placeholder'];?>
" />
                                            <?php } else { ?>
                                            <textarea class="form-control ckeditor" name="row[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['k_2']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['input_group']->value['name'];?>
]" placeholder="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['placeholder'];?>
"></textarea>
                                            <?php }?>
                                        </div>
                                    </div>    
                                    <?php if (sizeOf($_smarty_tpl->tpl_vars['input_group']->value['sub_input'])>0) {?>
                                        <?php $_smarty_tpl->tpl_vars['input_group'] = new Smarty_variable($_smarty_tpl->tpl_vars['input_group']->value['sub_input'], null, 0);?>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"><?php echo $_smarty_tpl->tpl_vars['input_group']->value['label'];?>
:
                                            </label>
                                            <div class="col-md-10">
                                                <?php if ($_smarty_tpl->tpl_vars['input_group']->value['type']=='input') {?>
                                                <input type="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['input-type'];?>
"  class="form-control" name="row[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['k_2']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['input_group']->value['name'];?>
]" placeholder="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['placeholder'];?>
" />
                                                <?php } else { ?>
                                                <textarea class="form-control ckeditor" name="row[<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['k_2']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['input_group']->value['name'];?>
]" placeholder="<?php echo $_smarty_tpl->tpl_vars['input_group']->value['placeholder'];?>
"></textarea>
                                                <?php }?>
                                            </div>
                                        </div>   
                                    <?php }?>
                                <?php } ?>
                            <?php } ?>


                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="col-md-8" id="kategorilerimiz">         
                                    <button type="submit" class="btn blue">Oluştur</button> 
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <?php echo $_smarty_tpl->getSubTemplate ("base/footer_txt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php echo $_smarty_tpl->getSubTemplate ("base/quicksidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate ("base/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

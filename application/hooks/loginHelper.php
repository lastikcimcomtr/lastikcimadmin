<?php
class SessionData{
    var $CI;
    var $method;

    function __construct(){
        $this->CI =& get_instance();
        $this->module = $this->CI->router->fetch_module();
        $this->method = $this->CI->router->fetch_method();
    }

    function initializeData() {
		if(!$this->CI->session->userdata('login') && strpos(uri_string(), "login") === FALSE){    //this is line 13
            if (($this->module == "cars" && $this->method == "update") || ($this->module == "entegrasyon") || ($this->module == "members" && ($this->method == "sendreplymails")) ||($this->module == "importers" && ($this->method == "mikroupdate" || $this->method == "ferhat_deger" || $this->method == "yeniaktarim" || $this->method == "nokian_enteg" || $this->method == "freze_all" || $this->method == "senturk_all")) || ($this->method == "ferhat_close_removed") || ($this->method == "cevaunarrivedpackages" || $this->method == "unarrivedpackages") ||($this->module == "rims")) {
                
            } else {
                redirect('home/login');
            }
		}
    }
}
?>


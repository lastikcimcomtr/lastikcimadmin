<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Members_model', 'members');
    }

    public function index()
    {
        $data = array();
        $data['theme_url'] 	= $this->theme_url;
        $data['gruplar']    = $this->members->gruplar();
        $data['uyeler']		= $this->members->uyeler();
        $this->parser->parse("index", $data);
    }


    public function crm(){
        if ($this->input->method() == "post") {
            $sdata =$this->input->post();
            foreach ($sdata as $key => $value) {
                //$value = urlencode($value);
                $sdata[$key] = str_replace("@", "1at1", $value);
                if((is_numeric($value) && $value == 0) || $value == 'null' || $value == null ){
                    unset($sdata[$key]);
                }
                if(is_array($value)){     
                    $sdata[$key] =  implode ("-",$value);     
                }
            }
            $url = $this->uri->assoc_to_uri($sdata);
            redirect(site_url("members/crm/" . $url));
        }else{
            $sdata  = $this->uri->uri_to_assoc();
            foreach ($sdata as $key => $value) {
                $value = urldecode($value);
                $sdata[$key] = str_replace("1at1","@", $value);
                if( $key == 'tarih' ){
                    $sdata[$key] =  explode("-",$value);
                }
            }

        }
        $this->load->model('products/Brand_model', 'brand');
        $data = $sdata;
        $data['theme_url'] = $this->theme_url;   
        $data['kartlar'] = $this->members->crm($sdata); 
        $this->parser->parse("crm", $data);
    }

    public function crmq(){
        $data = array();
        $data['theme_url'] = $this->theme_url;   
        $data['sorular'] = $this->members->crm_sorular(); 
        $this->parser->parse("crmq", $data);
    }

    public function crm_soru_ekle(){

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->members->crm_sorular_add($pdata);
        } 
        redirect('members/crmq');
    }
    public function crm_soru_edit($id){

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->members->crm_sorular_edit($id,$pdata);
        } 
        redirect('members/crmq');
    }

    public function crm_soru_remove($id){

        $this->members->crm_soru_remove($id);

        redirect('members/crmq');
    }



    public function crm_kart_ekle(){

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->members->crm_kart_add($pdata);
        } 
        redirect('members/crm');
    }
    public function crm_kart_duzenle($id){

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->members->crm_kart_edit($id,$pdata);
        } 
        redirect('members/crm');
    }

    public function crm_kart_remove($id){

        $this->members->crm_kart_remove($id);

        redirect('members/crm');
    }

    public function add()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['gruplar']    = $this->members->gruplar();
        if ($this->input->method() == "post") {
            $pdata  = $this->input->post();
            $res    = $this->members->ekle($pdata);
            if ($res) {
                $data['status']     = "success";
                $data['message']    = "Üye / Bayi başarılı bir şekilde eklenmiştir.";
            } else {
                $data['uye']        = (object)$pdata;
                $data['status']     = "danger";
                $data['message']    = "Üye / Bayi ekleme işlemi sırasında hata meydana geldi.";
            }
        }

        $this->parser->parse("add", $data);
    }

    public function edit($id = NULL)
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $uye                = $this->members->uyeler(array('id' => $id), TRUE);

        if ($this->input->method() == "post") {
            $pdata  = $this->input->post();
            $res    = $this->members->duzenle($uye->id, $pdata);
            if ($res) {
                $data['status']     = "success";
                $data['message']    = "Üye / Bayi başarılı bir şekilde düzenlenmiştir.";
            } else {
                $data['status']     = "danger";
                $data['message']    = "Üye / Bayi düzenleme işlemi sırasında hata meydana geldi.";
            }
        }

        $data['gruplar']    = $this->members->gruplar();
        $data['uye']        = $uye;
        $this->parser->parse("edit", $data);
    }

    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $uye = $this->members->uyeler(array('id' => $id), TRUE);
            if ($uye) {
                $this->members->sil($uye->id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function fastedit($id = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            if ($id && is_numeric($id)) {
                $pdata = $this->input->post();
                $res = $this->members->duzenle($id, $pdata);
                if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Üye başarılı bir şekilde düzenlenmiştir.";
                } else {
                    $data['status']     = "danger";
                    $data['message']    = "Üye düzenleme işlemi sırasında hata meydana geldi.";
                }
            } else {
                $data['status']     = "danger";
                $data['message']    = "Üye seçimi yapmadan düzenleme yapılamaz.";
            }
        } else {
            $data['status']     = "danger";
            $data['message']    = "Bu şekilde hızlı düzenleme yapılmaz.";
        }
        echo json_encode($data);
    }

    public function groups()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['gruplar']    = $this->members->gruplar();
        $data['odemetipleri'] = $this->members->odemetipleri();
        $this->parser->parse("groups", $data);
    }

    public function editgroup($id = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {

            $pdata  = $this->input->post();
            $res    = $this->members->grupduzenle($id, $pdata);
            
            if ($res) {
                $data['status']     = "success";
                $data['message']    = "Grup başarılı bir şekilde düzenlenmiştir.";
            } else {
                $data['status']     = "danger";
                $data['message']    = "Grup düzenleme işlemi sırasında hata meydana geldi.";
            }

        } else {

            $data['status']     = "danger";
            $data['message']    = "Grup düzenleme işlemi sırasında hata meydana geldi.";

        }
        echo json_encode($data);
    }

    public function addgrup()
    {
        $data = array();
        if ($this->input->method() == "post") {

            $pdata  = $this->input->post();
            $res    = $this->members->grupekle($pdata);
            
            if ($res) {
                $data['status']     = "success";
                $data['message']    = "Grup başarılı bir şekilde eklenmiştir.";
            } else {
                $data['status']     = "danger";
                $data['message']    = "Grup ekleme işlemi sırasında hata meydana geldi.";
            }

        } else {

            $data['status']     = "danger";
            $data['message']    = "Grup ekleme işlemi sırasında hata meydana geldi.";

        }
        echo json_encode($data);
    }

    public function removegroup($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $grup = $this->members->gruplar(array('id' => $id), TRUE);
            if ($grup) {
                $this->members->grupsil($grup->id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function sendreplymails(){
        $this->members->send_reply_mails();
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	

		$this->loginUser =	$this->session->userdata('login');
	}
	
	public function uyeparametreleri()
	{
		$this->db->select('DISTINCT user_meta', FALSE);
		$this->db->from('musteri_datalar');
		$res = $this->db->get()->result();
		return $res;
	}

	public function grupsil($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('musteri_gruplari');
		return $res;
	}

	public function grupduzenle($id = NULL, $data = NULL)
	{
		if ($id && $data) {
			$data['data'] = json_encode($data['data']);
			
			$this->db->where('id', $id);
			$res = $this->db->update('musteri_gruplari', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function grupekle($data = NULL)
	{
		if ($data) {
			$data['data'] = json_encode($data['data']);
			$res = $this->db->insert('musteri_gruplari', $data);
			return $res;
		} else {
			return FALSE;
		}
	}


	public function personeller(){
		$this->db->select("*");
		$this->db->from("kullanicilar");
		return $this->db->get()->result();
	}

	public function crm_sorular(){
		$this->db->select("*");
		$this->db->from("crm_sorular");
		$this->db->where("sil",0);
		$this->db->order_by('sira');
		return $this->db->get()->result();
	}


	public function crm($sdata = NULL, $row = FALSE){
		$this->db->select("c.* , concat(k.ad ,' ' ,k.soyad) as personel_adi");
		$this->db->from("crm c");
		$this->db->join('kullanicilar k','k.id = c.personel','left');
		$this->db->where("c.sil",0);
		$this->db->order_by('c.tarih desc');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				
				if(is_numeric($value)){
					$this->db->where('c.'.$key, $value);
				}elseif(is_array($value)){
					$startdate = strtotime($value[0]);
					$enddate = strtotime($value[1]);
					if($startdate == $enddate)
					{
						$enddate = $enddate+ 86400;
					} 
					$this->db->where('c.'.$key . ' >=', $startdate );				
					$this->db->where('c.'.$key . ' <=', $enddate );
				}else{
					if($key == 'isim'){
						$this->db->where("concat(c.adi,soyadi) like", "%". str_replace(" ", "%", $value)."%" );
					}
					else{
						$this->db->where('c.'.$key . ' like', "%". $value."%" );
					}
				}
				
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}



	public function crm_cevaplar($crm_id){
		$this->db->select("s.soru,s.id,s.soru_tip,s.soru_tip,c.cevap");
		$this->db->from("crm_sorular s");
		$this->db->join("crm_cevaplar c",'c.soru_id = s.id and c.crm_id = "' .$crm_id .'"','left');
		$this->db->where("s.sil",0);
		$this->db->order_by('s.sira');
		return $this->db->get()->result();
	}

	public function crm_sorular_add($data){
		$this->db->insert("crm_sorular",$data);
	}
	
	public function crm_kart_add($data){
		$sorular = $data['soru'];
		unset($data['soru']);
		$data['tarih'] = time();
		$data['personel'] = $this->loginUser->id;
		$this->db->insert("crm",$data);
		$crm_id = $this->db->insert_id();
		foreach ($sorular as $key => $value) {
			$this->db->insert("crm_cevaplar",array('crm_id'=>	$crm_id ,'soru_id'=>$key ,'cevap'=>$value));
		}
	}
	public function crm_kart_edit($crm_id,$data){
		$sorular = $data['soru'];
		unset($data['soru']);
		$this->db->where("id",$crm_id);
		$this->db->update("crm",$data);
		$this->db->where("crm_id",$crm_id);
		$this->db->delete("crm_cevaplar");
		foreach ($sorular as $key => $value) {
			$this->db->insert("crm_cevaplar",array('crm_id'=>	$crm_id ,'soru_id'=>$key ,'cevap'=>$value));
		}
	}

	public function crm_sorular_edit($id,$data){
		$this->db->where("id",$id);
		$this->db->update("crm_sorular",$data);
	}

	public function crm_soru_remove($id){
		$this->db->where("id",$id);
		$this->db->update("crm_sorular",array('sil'=>1));
	}

	public function crm_kart_remove($id){
		$this->db->where("id",$id);
		$this->db->update("crm",array('sil'=>1));
	}
	public function odemetipleri($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('odeme_tipleri');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	public function gruplar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('mg.*, count(distinct mh.id) as uyesayisi');
		$this->db->from('musteri_gruplari as mg');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where('mg.'.$key, $value);
			}
		}
		$this->db->join('musteri_hesaplari as mh', 'mh.grup = mg.id', 'LEFT');
		$this->db->group_by('mg.id');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}
	
	public function uyeler($sdata = NULL, $row = FALSE) 
	{
		$parametreler = $this->uyeparametreleri();

		$this->db->select('m.*, mg.grup as grup_adi');

		foreach ($parametreler as $parametre) {
			$this->db->select('MAX(CASE WHEN md.user_meta = \''.$parametre->user_meta.'\' THEN md.user_metavalue END) as '.$parametre->user_meta);
		}

		$this->db->from('musteri_hesaplari as m');
		$this->db->join('musteri_datalar as md', 'md.user_id = m.id', 'LEFT');
		$this->db->join('musteri_gruplari as mg', 'mg.id = m.grup', 'LEFT');
		$this->db->group_by('m.id');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				if (strlen($value) > 0) {
					if (is_string($value))
						$this->db->having($key." like '%".$value."%'");
					else
						$this->db->having($key, $value);
				} 
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	public function sil($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('musteri_hesaplari');
		if ($res) {
			$this->db->where('user_id', $id);
			$res = $this->db->delete('musteri_datalar');
		}
		return $res;
	}

	public function duzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$uye = $this->uyeler(array('id' => $id), TRUE);
			if ($uye) {
				$upindata 	= array();
				$upoutdata 	= array();

				if (count($data) > 0) {
					
					foreach ($data as $key => $value) {

						if ($key <> "email" && $key <> "sifre" && $key <> "grup" && $key <> "aktivasyon" && $key <> "hesap_durumu") {
							if (strlen($value) > 0)
								$upoutdata[$key] = $value;
						} else {
							if (strlen($value) > 0)
								$upindata[$key] = $value;
						}

					}

					if (count($upindata) > 0) {
						$this->db->where('id', $uye->id);
						$res = $this->db->update('musteri_hesaplari', $upindata);
					}

					if (count($upoutdata) > 0) {
						foreach ($upoutdata as $uokey => $uovalue) {
							$this->db->where('user_id', $uye->id);
							$this->db->where('user_meta', $uokey);
							$res = $this->db->update('musteri_datalar', array('user_metavalue' => $uovalue));
						}
					}
					return $res;

				} else {
					return TRUE;
				}

			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function ekle($data = NULL)
	{
		$upindata 	= array();
		$upoutdata 	= array();

		if (count($data) > 0) {
			
			foreach ($data as $key => $value) {

				if ($key <> "email" && $key <> "sifre" && $key <> "grup" && $key <> "aktivasyon" && $key <> "hesap_durumu") {
					if (strlen($value) > 0)
						$upoutdata[$key] = $value;
				} else {
					if (strlen($value) > 0)
						$upindata[$key] = $value;
				}

			}

			if (count($upindata) > 0) {
				$varmi = $this->uyeler(array('email' => $upindata['email']), TRUE);
				if ($varmi) {
					return FALSE;
				} else {					
					$upindata['kayit_tarihi'] = time();
					$res = $this->db->insert('musteri_hesaplari', $upindata);
					if ($res) {
						$user_id = $this->db->insert_id();
						if ($user_id && is_numeric($user_id)) {

							if (count($upoutdata) > 0) {
								foreach ($upoutdata as $uokey => $uovalue) {
									$res = $this->db->insert('musteri_datalar', array('user_metavalue' => $uovalue, 'user_meta' => $uokey, 'user_id' => $user_id));
								}
							}
							return $res;

						} else {
							return FALSE;
						}
					} else {
						return FALSE;
					}
				}
			}

		} else {
			return TRUE;
		}
	}

	public function send_reply_mails(){
		$tmago = mktime(0, 0, 0, date("m")-3, date("d"), date("Y"));
		$fiyatdhb = $this->db->where('tarih >',$tmago)->where('kategori','Fiyat Düşünce Haber')->where('okundu',0)->select('*')->from('iletisim_istekleri');
		$fiyatdhb = $fiyatdhb->get()->result();
		$this->load->model('products/Products_model', 'products');
		foreach ($fiyatdhb as $key => $fiyat_item) {
			$urun = $this->products->urunler(array('id' => $fiyat_item->urun_id),true);
			$fiyat_parsed = explode(",", $fiyat_item->urun_fiyat)[0];
			if($urun->fiyat >= $fiyat_parsed){
				unset($fiyatdhb[$key]);
			}else if($urun->stok_miktari <= 0){
				unset($fiyatdhb[$key]);
			}else if($urun->durumu == 0){
				unset($fiyatdhb[$key]);
			}
			$fiyatdhb[$key]->urun = $urun;
		}
		$this->sendFiyatEmail($fiyatdhb);
		$stokhdb = $this->db->where('tarih >',$tmago)->where('kategori','Ürün Talep')->where('okundu',0)->select('*')->from('iletisim_istekleri');
		$stokhdb = $stokhdb->get()->result();
		foreach ($stokhdb as $key => $stok_item) {
			$urun = $this->products->urunler(array('id' => $stok_item->urun_id),true);
			if($urun->stok_miktari <= 0){
				unset($stokhdb[$key]);
			}else if($urun->durumu == 0){
				unset($stokhdb[$key]);
			}
			$stokhdb[$key]->urun = $urun;
		}
		$this->sendStokEmail($stokhdb);
	}
	public function sendFiyatEmail($fiyatdhb){
		$this->load->model('mail/Mail_model', 'mail');
		foreach ($fiyatdhb as $key => $item) {
			$sending = [];
			$sending['iletisim_talep'] = $item;
			$sending['satis_fiyati'] = $item->urun->fiyat;
			$html = $this->parser->parse('modules/mail/fiyathaber',$sending,true);
			$this->db->where('id',$item->id)->update('iletisim_istekleri',array('okundu' => 1));
			$mail_status = $this->mail->htmlmail(array('email'=> $item->email,'title'=>'Beklediğin Ürünün Fiyatı Düştü', 'html'=> $html));	
		}
	}
	public function sendStokEmail($stokhdb){
		$this->load->model('mail/Mail_model', 'mail');
		foreach ($stokhdb as $key => $item) {
			$sending = [];
			$sending['iletisim_talep'] = $item;
			$sending['satis_fiyati'] = $item->urun->fiyat;
			$html = $this->parser->parse('modules/mail/fiyathaber',$sending,true);
			$this->db->where('id',$item->id)->update('iletisim_istekleri',array('okundu' => 1));
			$mail_status = $this->mail->htmlmail(array('email'=> $item->email ,'title'=>'Beklediğin Ürün Stoklarımıza Gelmiştir', 'html'=> $html));	
		}
	}
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cars_model extends CI_Model {
	/*
	summer
	winter
	*/
	function __construct() {
		parent::__construct();	
	}

	public function ekle($data = NULL)
	{
		if (count($data) > 0) {
			$res = $this->arabalar($data, TRUE);
			if ($res) {
				return "ALREADY";
			} else {
				$res = $this->db->insert('arabalar', $data);
				if ($res) {
					return "YES";
				} else {
					return "NO";
				}
			}

		} else {
			return "NO";
		}
	}

	public function arabalar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('arabalar');
		if (count($sdata) > 0 ) {
			if (isset($sdata['limit'])) {
				$this->db->limit($sdata['limit']);
				unset($sdata['limit']);
			}
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		$this->db->order_by('last', 'asc');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}


	public function ebatlar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('arabalar_data');
		if (count($sdata) > 0 ) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	public function ebatekle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$ebat = $this->ebatlar(array('araba' => $id), TRUE);
			if ($ebat) {
				return "ALREADY";
			} else {
				$res = $this->db->insert('arabalar_data', $data);
				if ($res) {
					return "YES";
				} else {
					return "NO";
				}
			}
		} else {
			return "NO";
		}
	}

	public function ebatcek($id){
		$res = $this->arabalar($data, TRUE);
		$post_fields = ['maker' => $res->k,  'segment' => $res->s,'energy' => $res->n ,'type' => $res->t,  'motor' => $res->p, 'season' => 'summer'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
		curl_setopt($ch, CURLOPT_URL, 'http://www.rezulteo-tyres.ie/tyre-selector-dimensions-for-vehicle');
		$response = curl_exec($ch);
		if (curl_error($ch)) {
   			 $error_msg = curl_error($ch);
   			 echo $error_msg;
		}
				echo $response;
				die();
		curl_close($ch);
		$dimensions = json_decode($response);
		$dimension = [];

		$dimension['araba'] = $id;
		$dimension['message'] = json_encode($dimensions->message);
		$dimension['dimensions'] = json_encode($dimensions->dimensions);
		$this->ebatekle($id,$dimension);
	}

	public function lastup($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->update('arabalar', array('last' => time()));
		return $res;
	}
	
}

?>
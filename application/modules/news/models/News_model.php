<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_Model {

	
	function __construct() {
		parent::__construct();	
	}
	
	public function haberler($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('haberler');

		if ($sdata && count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		
		$this->db->order_by('sira', 'asc');

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		
		return $res;
	} 
	
	public function categories($sdata = NULL, $row = FALSE){
		$this->db->select('*');
		$this->db->from('haber_kategoriler');
		if ($sdata && count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}
	
	public function kategori_ekle($data = NULL){
		if ($data && count($data) > 0) {
			$res = $this->db->insert('haber_kategoriler', $data);
			return $res;
		} else {
			return FALSE;
		}
	}
	
	public function haber_kategoriler($id = NULL){
		if ($id && is_numeric($id)) {
			$this->db->select('kategori_id');
			$this->db->from('haber_kategori');
			$this->db->where('haber_id =', (int)$id);
			$res = $this->db->get()->result_array();
			$res = array_column($res,"kategori_id");
			return $res;
		} else {
			return FALSE;
		}
	}
	
	public function kategori_sil($id = NULL){
		if ($id && is_numeric($id)) {
			$this->db->where('kategori_id', $id);
			$this->db->delete('haber_kategori');
			$this->db->where('id', $id);
			$res = $this->db->delete('haber_kategoriler');
			return $res;
		} else {
			return FALSE;
		}
	}
	
		
	public function haber_etiketler($id = NULL){
		$this->db->select('he.id as he_id,et.id as et_id,et.adi as adi');
		$this->db->from('haber_etiketler he');
		$this->db->from('etiketler et');
		$this->db->where('he.id = et.id');
		$this->db->where('he.haber_id',$id);
		$res = $this->db->get()->result();
		return $res;
	}
		
	
	public function kategori_duzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			$this->db->where('id', $id);
			$res = $this->db->update('haber_kategoriler', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function checksefurl($text,$id = 0){
		$text = $this->texttourl($text);
		$this->db->select("sef_url");
		$this->db->from('haberler');
		if($id){
			$this->db->where("id <>", $id);
		}
		$this->db->group_start();
		$this->db->or_where("sef_url REGEXP ", $text . "_[0-9]");
		$this->db->or_where("sef_url", $text);
		$this->db->group_end();
		$res = $this->db->get()->result();
		//print_r($res);
		if($res){
			$olanlar = array();
			foreach ($res as $key => $value) {
				$olanlar[] = $value->sef_url;
			}
			$i = 0;
			do {
				if($i)
					$cikti = $text ."_$i";
				else				
					$cikti = $text;
				++$i;
			} while (in_array($cikti, $olanlar));
			return $cikti;
		}else{
			return $text;
		}

	}

	public function texttourl($text){
		$search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü');
		$replace = array('c','c','g','g','i','i','o','o','s','s','u','u'); 
		$text = str_replace($search, $replace, $text);
		$text = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9]+/', ' ', $text)));
		$text =preg_replace('/\s\s+/', ' ', $text);   
		$text = trim($text);
		$text = str_replace(" ","-", $text);
		return $text;
	}

	public function alt_kategori_guncelle($haber_id,$kategoriler){
			$this->db->where('haber_id', $haber_id);
			$res = $this->db->delete('haber_kategori');
			foreach($kategoriler as $kategori){
				$data = array();
				$data['haber_id'] = $haber_id;
				$data['kategori_id'] = $kategori;
				$this->db->insert('haber_kategori', $data);
			}
	}
	public function ekle($data = NULL)
	{
		if ($data && count($data) > 0) {
			if($data['alt_cats']){
				$alt_cats = $data['alt_cats'];
				unset($data['alt_cats']);
			}
			if (strlen($_FILES['haber_resim']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'haber_resim'));
				$data['resim']	= $upload_res->image;
			} else {
				unset($data['resim']);
			}

			if (strlen($_FILES['haber_resim_ic']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'haber_resim_ic'));
				$data['resim_ic']	= $upload_res->image;
			} else {
				unset($data['resim_ic']);
			}

			$data['eklenme'] = time();
			$res = $this->db->insert('haberler', $data);
			
			if(isset($alt_cats) && $res){
				$this->alt_kategori_guncelle($this->db->insert_id(),$alt_cats);
			}
			return $res;
		} else {
			return FALSE;
		}
	}
	

	public function duzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			if($data['alt_cats']){
				$alt_cats = $data['alt_cats'];
				unset($data['alt_cats']);
			}
			$data['guncelleme'] = time();

			if (strlen($_FILES['haber_resim']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'haber_resim'));
				$data['resim']	= $upload_res->image;
			} else {
				unset($data['resim']);
			}
			
			if (strlen($_FILES['haber_resim_ic']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'haber_resim_ic'));
				$data['resim_ic']	= $upload_res->image;
			} else {
				unset($data['resim_ic']);
			}
			if(!$_FILES['haber_resim_ic']['name']){
				if($data['haber_ic_resim_sil'] === 'true'){
					$data['resim_ic'] = null;
				}else{
					unset($data['resim_ic']);
				}
			}
			unset($data['haber_ic_resim_sil'] );
			$this->db->where('id', $id);
			$res = $this->db->update('haberler', $data);
			
			if(isset($alt_cats) && $id){
				$this->alt_kategori_guncelle($id,$alt_cats);
			}
			return $res;
		} else {
			return FALSE;
		}
	}

	public function sil($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$this->db->where('id', $id);
			$res = $this->db->delete('haberler');
			return $res;
		} else {
			return FALSE;
		}
	}

	public function kategoriler(){
		$this->db->select('*');
		$this->db->from('haber_kategoriler');
		return $this->db->get()->result();
	}

	public function urunler($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('firsaturunleri');

		if ($sdata && count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		
		$this->db->order_by('sira', 'asc');

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		
		return $res;
	} 

	public function urunsil($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$this->db->where('id', $id);
			$res = $this->db->delete('firsaturunleri');
			return $res;
		} else {
			return FALSE;
		}
	}

	public function pageskinUpd($data, $upd = FALSE){
		if($upd == TRUE){
			if (strlen($_FILES['urun_resim']['name']) > 4) {
				print_r($_FILES['urun_resim']);
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'urun_resim'));
				$data['img']	= "/uploads/news/".$upload_res->image;
			} 
			$upd = $this->db->query("update pageSkins set url = '".$data["url"]."', img = '".$data["img"]."' where id = '1'");
			if($upd){ 
	            $returnData['status']     = "success";
	            $returnData['message']    = "Page Skin Başarılı Bir Şekilde Güncellendi.";
			}else{
                $returnData['status']     = "danger";
                $returnData['message']    = "Page Skin Güncellenemedi.";
			}
            return $returnData;
		}else{
			$ps = $this->db->query("select * from pageSkins where sil = 0 limit 1")->row();
			return $ps;
		}
	}

	public function urunekle($data = NULL)
	{
		if ($data && count($data) > 0) {

			if (strlen($_FILES['urun_resim']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'urun_resim'));
				$data['resim']	= $upload_res->image;
			} else {
				unset($data['resim']);
			}

			$res = $this->db->insert('firsaturunleri', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function urunduzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {

			if (strlen($_FILES['urun_resim']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'haber_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'urun_resim'));
				$data['resim']	= $upload_res->image;
			} else {
				unset($data['resim']);
			}

			$this->db->where('id', $id);
			$res = $this->db->update('firsaturunleri', $data);
			return $res;
		} else {
			return FALSE;
		}
	}
	
	public function bannerbase($code = NULL, $data = NULL){
		 $bases = $this->db->select('*')->from('banner_base');
		if($code){
			$bases = $bases->where('code',$code);
		}
		if($data){
			foreach($data as $key=>$value){
					$bases = $bases->where($key ,$value);
			}
			return $bases->get()->row();
		}
		return $bases->get()->result();
	}
	
	public function bannerdeploy($id = NULL, $data = NULL){
		 $deploys = $this->db->select('bd.*, bb.banner_name banner_name, bb.height height, bb.width width,bb.code code')->from('banner_deploy bd');
		 $deploys = $deploys->from('banner_base bb')->where('bb.id = bd.base_id');
		if($id){
			$deploys = $deploys->where('bd.id',$id);
		}
		if($data){
			foreach($data as $key=>$value){
					$deploys = $deploys->where('bd.'.$key ,$value);
			}
		}
		if($id){
			return $deploys->get()->row();
		}
		return $deploys->get()->result();
	}
	
	public function add_banner_deploy($data = NULL){
		if($data){
			if (strlen($_FILES['src']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'banner_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'src'));
				$data['src']	=  $upload_res->image;
				$res = $this->db->insert('banner_deploy', $data);
				return $res;
			} else {
				unset($data['src']);
			}
		}
		return false;
	}
	
	public function edit_banner_deploy($id, $data = NULL){
		if($data && $id){
			if (strlen($_FILES['src']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'banner_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'src'));
				$data['src']	=  $upload_res->image;
			}else{
				unset($data['src']);
			}
			$this->db->where('id', $id);
			$res = $this->db->update('banner_deploy', $data);
			return $res;
		}
		return false;
	}
 
	public function remove_banner_deploy($id){
		if ($id && is_numeric($id)) {
			$this->db->where('id', $id);
			$res = $this->db->delete('banner_deploy');
			return $res;
		} else {
			return FALSE;
		}
	}
}

?>
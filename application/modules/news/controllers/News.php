<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('News_model', 'news');
    }

    public function index()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['haberler']   = $this->news->haberler();
        $this->parser->parse("index", $data);
    }
	
	public function category()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['categories']   = $this->news->categories();
        $this->parser->parse("categories", $data);
    }
	
	public function add_category(){
		$data = array();
        $data['theme_url']  = $this->theme_url;
        if ($this->input->method() == "post") {
        	$pdata 	= $this->input->post();
        	$addres = $this->news->kategori_ekle($pdata);
        	if ($addres) {
        		$data['status'] 	= "success";
        		$data['message']	= "Haber Kategorisi başarılı bir şekilde eklenmiştir.";
        	} else {
        		$data['status'] 	= "danger";
        		$data['message']	= "Haber Kategorisi eklenirken hata meydana geldi.";
        	}
        }
        $this->parser->parse("category_add", $data);
	}
	
	public function category_remove($id=NULL){
        if ($id && is_numeric($id)) {
                $this->news->kategori_sil($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function cat_edit($id=NULL){
       if ($id && is_numeric($id)) {
            $kategori = $this->news->categories(array('id' => $id), TRUE);
            if ($kategori) {
                $data = array();
                $data['theme_url'] 	= $this->theme_url;
                $data['kategori'] 		= $kategori;
                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $addres = $this->news->kategori_duzenle($id, $pdata);
                    if ($addres) {
                        $data['message']    = "Kategori başarılı bir şekilde düzenlenmiştir.";
                        $data['status']     = "success";
                    } else {
                        $data['message']    = "Kategori düzenleme işlemi sırasında bir hata oldu.";
                        $data['status']     = "danger";
                    }
                }
                $this->parser->parse("category_edit", $data);
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
	}

    public function opportunities()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['urunler']    = $this->news->urunler();
        $this->parser->parse("opportunities", $data);
    }

    public function addopportunity()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        
        if ($this->input->method() == "post") {
            $pdata  = $this->input->post();
            $addres = $this->news->urunekle($pdata);
            if ($addres) {
                $data['status']     = "success";
                $data['message']    = "Fırsat Ürünü başarılı bir şekilde eklenmiştir.";
            } else {
                $data['status']     = "danger";
                $data['message']    = "Fırsat Ürünü eklenirken hata meydana geldi.";
            }
        }

        $this->parser->parse("addopportunity", $data);
    }

    public function pageskin()
    {
        $data = array();
        if ($this->input->method() == "post") {
            $pdata  = $this->input->post();
            $data = $this->news->pageSkinUpd($pdata,1);
        }
        $data['theme_url']  = $this->theme_url;
        $data["pageSkins"] = $this->news->pageskinUpd();
        $this->parser->parse("pageskin", $data);
    }

    public function editopportunity($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $urun = $this->news->urunler(array('id' => $id), TRUE);
            if ($urun) {
                $data = array();
                $data['theme_url']  = $this->theme_url;
                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $addres = $this->news->urunduzenle($id, $pdata);

                    if ($addres) {
                        $data['message']    = "Fırsat Ürünü başarılı bir şekilde düzenlenmiştir.";
                        $data['status']     = "success";
                        $urun = $this->news->urunler(array('id' => $id), TRUE);
                    } else {
                        $data['message']    = "Fırsat Ürünü düzenleme işlemi sırasında bir hata oldu.";
                        $data['status']     = "danger";
                    }
                }
                $data['urun']      = $urun;
                $this->parser->parse("editopportunity", $data);
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function opportunity($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $urun = $this->news->urunler(array('id' => $id), TRUE);
            if ($urun) {
                $this->news->urunsil($id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function add()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        
        if ($this->input->method() == "post") {
        	$pdata 	= $this->input->post();
        	$addres = $this->news->ekle($pdata);
        	if ($addres) {
        		$data['status'] 	= "success";
        		$data['message']	= "Haber / Kampanya başarılı bir şekilde eklenmiştir.";
        	} else {
        		$data['status'] 	= "danger";
        		$data['message']	= "Haber / Kampanya eklenirken hata meydana geldi.";
        	}
        }

        $this->parser->parse("add", $data);
    }

    public function edit($id = NULL)
    {
    	if ($id && is_numeric($id)) {
            $haber = $this->news->haberler(array('id' => $id), TRUE);
            if ($haber) {
                $data = array();
                $data['theme_url'] 	= $this->theme_url;
                $data['haber'] 		= $haber;
                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $addres = $this->news->duzenle($id, $pdata);

                    if ($addres) {
                        $data['message']    = "Haber / Kampanya başarılı bir şekilde düzenlenmiştir.";
                        $data['status']     = "success";
                    } else {
                        $data['message']    = "Haber / Kampanya düzenleme işlemi sırasında bir hata oldu.";
                        $data['status']     = "danger";
                    }
                }
                $this->parser->parse("edit", $data);
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function checkurl(){
       if ($this->input->method() == "post") {
         $pdata = $this->input->post();
         echo $this->news->checksefurl($pdata['text'],$pdata['id']);
         die;
     }
 }

    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $haber = $this->news->haberler(array('id' => $id), TRUE);
            if ($haber) {
                $this->news->sil($id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }




    public function removeopportunity($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $urun = $this->news->urunler(array('id' => $id), TRUE);
            if ($urun) {
                $this->news->urunsil($id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function banner_management(){
		$data = [];
        $data['theme_url']  = $this->theme_url;
        $data["bannerDeploy"] = $this->news->bannerdeploy();
        $this->parser->parse("banners", $data);
	}
	
	public function banner_edit($id = NULL){
		$data = [];
		if ($this->input->method() == "post") {
			$pdata = $this->input->post();
			$res= $this->news->edit_banner_deploy($id,$pdata);
			if ($res) {
        		$data['status'] 	= "success";
        		$data['message'] = "Banner başarılı bir şekilde düzenlendi.";
        	} else {
        		$data['status'] 	= "danger";
        		$data['message'] = "Banner düzenlendirken hata meydana geldi.";
        	}
		}
        $data["theme_url"]  = $this->theme_url;
        $data["banner"] = $this->news->bannerdeploy($id);
		$data["bannerBase"] = $this->news->bannerbase();
        $this->parser->parse("banner_edit", $data);
	}
	
	public function banner_add(){
		$data = [];
		if ($this->input->method() == "post") {
			$pdata = $this->input->post();
			$res = $this->news->add_banner_deploy($pdata);
			if ($res) {
        		$data['status'] 	= "success";
        		$data['message'] = "Banner başarılı bir şekilde eklenmiştir.";
        	} else {
        		$data['status'] 	= "danger";
        		$data['message'] = "Banner eklenirken hata meydana geldi.";
        	}
		}
        $data["theme_url"]  = $this->theme_url;
		$data["bannerBase"] = $this->news->bannerbase();
        $this->parser->parse("banner_add", $data);
	}
	
	public function banner_remove($id = NULL){
        if ($id && is_numeric($id)) {
			$this->news->remove_banner_deploy($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function haber_etiket_kaydet()
    {
        $pdata = $this->input->post();
		
        if (isset($pdata['haber_id'])) {
			$this->db->where('haber_id', $pdata['haber_id']);
			$res = $this->db->delete('haber_etiketler');
			if (isset($pdata['etiket_id'])) {
				$data = [];
				foreach($pdata['etiket_id'] as $etiket_id){
					$data['etiket_id'] = $etiket_id;
					$data['haber_id'] = $pdata['haber_id'];
					$res = $this->db->insert('haber_etiketler', $data);
				}
			}
		}
        echo json_encode($res);
    }

    public function testimonials(){
        $testimonials = $this->db->select('*')->from('musteri_yorumlari')->get()->result();
        $data["theme_url"]  = $this->theme_url;
        $data["testimonials"] = $testimonials;
        $this->parser->parse("testimonials", $data);
    }
    public function toggle_testimonial($testimonial_id){
        $testimonial = $this->db->select('id,active')->from('musteri_yorumlari')->where('id',$testimonial_id)->get()->row();
        if($testimonial){
            if($testimonial->active){
                $this->db->where('id',$testimonial_id)->update('musteri_yorumlari',['active' => 0]);
                echo 'true';
            }else{
                $this->db->where('id',$testimonial_id)->update('musteri_yorumlari',['active' => 1]);
                echo 'false';
            }
            return;
        }
        echo 'error';
    }
    public function testimonial_edit($id){
        $testimonial = $this->db->select('*')->from('musteri_yorumlari')->where('id',$id)->get()->row();
        if($testimonial){
            $data = [];
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $this->db->where('id', $id);
                $this->db->update('musteri_yorumlari',$pdata);
                $testimonial = $this->db->select('*')->from('musteri_yorumlari')->where('id',$id)->get()->row();
                $data['message'] = 'Başarı ile güncellendi';
                $data['status'] = 'success';
            }
            $data["theme_url"]  = $this->theme_url;
            $data["testimonial"] = $testimonial;
            $this->parser->parse("testimonial_edit", $data);
        }
        else echo 'Not Found';
    }
    public function testimonial_remove($id){
        $this->db->where('id', $id);
        $this->db->delete('musteri_yorumlari');
        redirect($_SERVER['HTTP_REFERER']);
    }
}

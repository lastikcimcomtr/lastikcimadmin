<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Command_model extends CI_Model
{
    public function getDesenUrl()
    {
        $this->db->select('urunler.stok_kodu, urunler.desen, urunler.url_duzenle, desenler.resim, desenler.resim_2, , desenler.resim_3')
            ->from('urunler')
            ->join('desenler', 'desenler.id = urunler.desen', 'left');

        return $this->db->get();
    }

    public function urlDuzenleTemizle($string)
    {
        if ($pos = mb_strpos($string, '/')) {
            return mb_substr($string, 0, $pos);
        }
        return $string;
    }
}
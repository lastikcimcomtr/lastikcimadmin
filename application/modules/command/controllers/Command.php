<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Command extends CI_Controller
{
    private $site_resim_dir = '/home/lastikcim/domains/lastikcim.com.tr/public_html/resimler/';
    private $pattern_dir = '/home/lastikcim/domains/lastikcim.com.tr/public_html/admin/uploads/pattern/';

    public function __construct()
    {
        parent::__construct();
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        ini_set('max_execution_time', 600);
        $this->load->model('Command_model', 'command');
    }

    public function index()
    {

    }

    public function fix_resimsiz_urunler()
    {

        $desenUrlRecord = $this->command->getDesenUrl();

        foreach ($desenUrlRecord->result() as $desenUrl) {
            // urunler.desen, urunler.url_duzenle, desenler.resim, desenler.resim_2, , desenler.resim_3
            $resim1 = $this->command->urlDuzenleTemizle($desenUrl->url_duzenle) . '.jpg';
            $resim2 = $this->command->urlDuzenleTemizle($desenUrl->url_duzenle) . '-lastik.jpg';
            $resim3 = $this->command->urlDuzenleTemizle($desenUrl->url_duzenle) . '-lastik-fiyati.jpg';

            $resim1_path = $this->site_resim_dir . $resim1;
            $resim2_path = $this->site_resim_dir . $resim2;
            $resim3_path = $this->site_resim_dir . $resim3;

            echo "<strong>{$desenUrl->stok_kodu} </strong><br>";

            if (!file_exists($resim1_path)) {
                echo '<span style="color: #ff0000">' . $resim1 . '</span>';
                if (is_file($this->pattern_dir . $desenUrl->resim)) {
                    copy($this->pattern_dir . $desenUrl->resim, $resim1_path);
                }
            } else {
                echo '<span style="color: #00ff00">' . $resim1 . '</span>';
            }
            echo '<br>';

            if (!file_exists($resim2_path)) {
                echo '<span style="color: #ff0000">' . $resim2 . '</span>';
                if (is_file($this->pattern_dir . $desenUrl->resim_2)) {
                    copy($this->pattern_dir . $desenUrl->resim_2, $resim2_path);
                }
            } else {
                echo '<span style="color: #00ff00">' . $resim2 . '</span>';
            }
            echo '<br>';

            if (!file_exists($resim3_path)) {
                echo '<span style="color: #ff0000">' . $resim3 . '</span>';
                if (is_file($this->pattern_dir . $desenUrl->resim_3)) {
                    copy($this->pattern_dir . $desenUrl->resim_3, $resim3_path);
                }
            } else {
                echo '<span style="color: #00ff00">' . $resim3 . '</span>';
            }
            echo '<br>';
            echo '<br>';

            //echo $resim1_path . PHP_EOL;
            //echo $resim2_path . PHP_EOL;
            //echo $resim3_path . PHP_EOL;

        }

    }
}
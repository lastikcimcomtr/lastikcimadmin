<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Pages_model', 'pages');
    }

    public function index($tur = NULL)
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['sayfalar']   = $this->pages->sayfalar(array('sayfa_turu' => $tur));
        $this->parser->parse("index", $data);
    }

    public function add()
    {
    	$data = array();
        $data['theme_url'] = $this->theme_url;
        $data['sayfaturleri'] = $this->pages->sayfaturleri();
        $data['sayfayerleri'] = $this->pages->sayfayerleri();
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $addres = $this->pages->ekle($pdata);

            if ($addres) {
                $data['message']    = "Sayfa başarılı bir şekilde eklenmiştir.";
                $data['status']     = "success";
            } else {
                $data['message']    = "Sayfa ekleme işlemi sırasında bir hata oldu.";
                $data['status']     = "danger";
            }
        }
        $this->parser->parse("add", $data);
    }

    public function edit($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $sayfa = $this->pages->sayfalar(array('id' => $id), TRUE);
            if ($sayfa) {
                $data = array();
                $data['theme_url'] = $this->theme_url;
                $data['sayfaturleri'] = $this->pages->sayfaturleri();
                $data['sayfayerleri'] = $this->pages->sayfayerleri();
                $data['sayfa']        = $sayfa;
                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $addres = $this->pages->duzenle($id, $pdata);

                    if ($addres) {
                        $data['message']    = "Sayfa başarılı bir şekilde düzenlenmiştir.";
                        $data['status']     = "success";
                    } else {
                        $data['message']    = "Sayfa düzenleme işlemi sırasında bir hata oldu.";
                        $data['status']     = "danger";
                    }
                }
                $this->parser->parse("edit", $data);
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $sayfa = $this->pages->sayfalar(array('id' => $id), TRUE);
            if ($sayfa) {
                $this->pages->sil($id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function cacheclear(){

        $sonuc = $this->getSslPage('https://www.lastikcim.com.tr/pages/cachesil') ;
       if ($sonuc == "ok"){
          redirect($_SERVER['HTTP_REFERER']);
        }else{ 
        echo "hataaa ";                     
        echo $sonuc;
        }
    }


   private function getSslPage($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function sayfalar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('s.*');
		$this->db->from('sayfalar as s');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where("s.".$key, $value);
			}
		}
		$this->db->join('sayfa_yerleri as sy', 'sy.id = s.sayfa_yeri', 'LEFT');
		$this->db->select('sy.yer as sayfa_yeri_adi');

		$this->db->join('sayfa_turleri as st', 'st.id = s.sayfa_turu', 'LEFT');
		$this->db->select('st.tip as sayfa_turu_adi');

		$this->db->order_by('s.sayfa_sira', 'asc');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function ekle($data = NULL)
	{
		if ($data && count($data)) {
			$data['eklenme_tarihi'] = time();
			$res = $this->db->insert('sayfalar', $data);
		} else {
			$res = FALSE;
		}
		return $res;
	}

	public function duzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			$data['guncellenme_tarihi'] = time();
			$this->db->where('id', $id);
			$res = $this->db->update('sayfalar', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function sil($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$this->db->where('id', $id);
			$res = $this->db->delete('sayfalar');
			return $res;
		} else {
			return FALSE;
		}
	}

	public function sayfayerleri($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('sayfa_yerleri');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function sayfaturleri($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('sayfa_turleri');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}
	
}

?>
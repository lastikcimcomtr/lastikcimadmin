<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Json_model', 'json');
    }

    public function kategoriler($kategori = NULL, $tip = NULL)
    {
        $data = array();
        if ($kategori && is_numeric($kategori)) {
            $this->load->model('products/Category_model', 'category');
            if($tip)
                $kategoriler = $this->category->kategoriler(array('ust' => $kategori, 'tip' => $tip));
            else
                $kategoriler = $this->category->kategoriler(array('ust' => $kategori));
            
            $data['kategoriler'] = $kategoriler;
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function markalar($marka = NULL, $tip = NULL)
    {
        $data = array();
        if ($marka && is_numeric($marka)) {
            $this->load->model('products/Brand_model', 'brand');
            $markalar = $this->brand->markalar(array('ust' => $marka, 'tip' => $tip));
            $data['markalar'] = $markalar;
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function desenler($marka = NULL, $kategori = NULL, $tip = NULL, $mevsim = NULL)
    {
        $data = array();
        if ($marka && is_numeric($marka) && $kategori && is_numeric($kategori)) {
            $this->load->model('products/Brand_model', 'brand');
          //  if($mevsim == "" || $mevsim == null){
                $desenler = $this->brand->desenler(array('marka' => $marka, 'kategori' => $kategori, 'tip' => $tip));
           // }else{
             //   $desenler = $this->brand->desenler(array('marka' => $marka, 'kategori' => $kategori, 'tip' => $tip, 'mevsim' => $mevsim));
           // }
            $data['desenler'] = $desenler;
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function veriler($kategori = NULL, $tip = NULL)
    {
        $data = array();
        if ($kategori && is_numeric($kategori)) {
            $this->load->model('products/Products_model', 'products');
            $veriler = $this->products->veriler(array('ust' => $kategori, 'usttip' => 'kategoriler', 'tip' => $tip));
            $data['veriler'] = $veriler;
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function durumdegistir($id = NULL, $tip = NULL)
    {
        $data = array();
        $this->db->select('*');
        $this->db->from($tip);
        $this->db->where('id', $id);
        $res = $this->db->get()->row();

        if ($res->durum == 1) {
            $this->db->where('id', $res->id);
            $upres = $this->db->update($tip, array('durum' => 0));
            if($tip == 'markalar'){
                $this->db->where('marka', $id);
                $this->db->update("urunler", array('durumu' => 0));
            }

            $data['status'] = $upres;
            if ($upres) {
                $data['durum'] = 0;
            } else {
                $data['durum'] = $res->durumu;
            }
        } else {
            $this->db->where('id', $res->id);
            $upres = $this->db->update($tip, array('durum' => 1));
            if($tip == 'markalar'){
                $this->db->where('marka', $id);
                $this->db->update("urunler", array('durumu' => 1));
            }

            $data['status'] = $upres;
            if ($upres) {
                $data['durum'] = 1;
            } else {
                $data['durum'] = $res->durumu;
            }
        }
        echo json_encode($data);
    }

    public function birsifir($id = NULL, $tip = NULL, $neyi = NULL)
    {
        $data = array();
        $this->db->select('*');
        $this->db->from($tip);
        $this->db->where('id', $id);
        $res = $this->db->get()->row();

        if ($res->$neyi == 1) {
            $this->db->where('id', $res->id);
            $upres = $this->db->update($tip, array($neyi => 0));
            $data['status'] = $upres;
            if ($upres) {
                $data['durum'] = 0;
            } else {
                $data['durum'] = $res->$neyi;
            }
        } else {
            $this->db->where('id', $res->id);
            $upres = $this->db->update($tip, array($neyi => 1));
            $data['status'] = $upres;
            if ($upres) {
                $data['durum'] = 1;
            } else {
                $data['durum'] = $res->$neyi;
            }
        }
        echo json_encode($data);
    }

    /*Ürün Parametre Güncelle*/
    public function upg()
    {
        $data = array();
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if ($pdata['urun'] && $pdata['parametre'] && $pdata['tip']) {
                $this->load->model('products/Products_model', 'products');
                
                if ($pdata['value'] == 1) {
                    $pdata['value'] = 0;
                } elseif ($pdata['value'] == 0) {
                    $pdata['value'] = 1;
                }

                $data['pdata']  = $pdata['value'];
                $data['status'] = $this->products->urunparametreguncelle($pdata['urun'], $pdata['parametre'], $pdata['value'], $pdata['tip']);
            } else {
                $data['status'] = FALSE;
            }
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }

    public function upghizli($urun = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if ($pdata['tip']) {
                $this->load->model('products/Products_model', 'products');
                $data['status'] = $this->products->urunparametreguncelle($urun, "urun_adi", $pdata['urun_adi'], $pdata['tip']);
            } else {
                $data['status'] = FALSE;
            }
        } else {
            $data['status'] = FALSE;
        }
        echo json_encode($data);
    }
    /*Ürün Parametre Güncelle*/

    /*Kampanyalar*/
    public function okduzenle($id = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->load->model('campaigns/Campaigns_model', 'campaigns');
            $res = $this->campaigns->okduzenle($id, $pdata);
            if ($res) {
                $data['status'] = "success";
                $data['message'] = "Özel kampanya başarılı bir şekilde düzenlenmiştir.";
            } else {
                $data['status'] = "danger";
                $data['message'] = "Özel kampanya düzenlenemedi.";
            }
        } else {
            $data['status'] = "danger";
            $data['message'] = "Bu işlemi bu şekilde yapamazsınız.";
        }
        echo json_encode($data);
    }

    public function okekle($data = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->load->model('campaigns/Campaigns_model', 'campaigns');
            $res = $this->campaigns->okekle($pdata);
            if ($res) {
                $data['status'] = "success";
                $data['message'] = "Özel kampanya başarılı bir şekilde eklenmiştir.";
            } else {
                $data['status'] = "danger";
                $data['message'] = "Özel kampanya eklenemedi.";
            }
        } else {
            $data['status']     = "danger";
            $data['message']    = "Bu işlemi bu şekilde yapamazsınız.";
        }
        echo json_encode($data);
    }
    /*Kampanyalar*/

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller  {


	public function __construct(){
		parent::__construct();
		$this->load->model('products/Products_model', 'products');
		$this->load->model('basket/Basket_model', 'basket'); 
		$this->load->model('montaj/montaj_model', 'montaj'); 
		$this->theme_url = $this->parser->theme_url();
	}

	public function parametredegerler(){
		//pr($this->products->parametre_degerler_on($this->input->get()));
		$this->parser->parse("default", array('yaz'=>json_encode($this->products->parametre_degerler_on($this->input->get()))));
		$this->output->cache(1000); 
	}
	public function arabasecim(){
		$this->parser->parse("default", array('yaz'=>json_encode($this->products->arabasecim($this->input->get()))));
		$this->output->cache(300); 
	}
	public function arabasecimjant(){
		$this->parser->parse("default", array('yaz'=>json_encode($this->products->arabasecimjant($this->input->get()))));
		$this->output->cache(300); 
	}

	public function ccnumbertobank($bin_number){
		$this->parser->parse("default", array('yaz'=>json_encode($this->basket->ccnumbertobank($bin_number))));
		$this->output->cache(3000); 
	}


	public function location($id=NULL){
		if($id){
			$this->parser->parse("default", array('yaz'=> json_encode($this->extraservices->ilceler($id))));
		}else{
			$this->parser->parse("default", array('yaz'=> json_encode( $this->extraservices->iller())));
		}	
		$this->output->cache(10080); 	
	}

	public function montajNoktalari($il = 34 ,$ilce =NULL , $product_id = NULL){
		
		if($this->input->get("s")){
			if($product_id){
				$urun_detay = $this->products->urunler(array('id' => $product_id), true);
				$kategori_idler = [0];
				$kategoriler = json_decode($urun_detay->kategoriler);
				foreach($kategoriler as $kategori){
					if ($kategori && is_numeric($kategori->id)) {
						array_push($kategori_idler,$kategori->id);
					}
				}
				$data_urun = [];
				$data_urun['kategoriler'] = $kategori_idler;
				$data_urun['run_flat'] = json_decode($urun_detay->run_flat);
				$json = json_encode($this->montaj->montajNoktalari($il,$ilce,$data_urun));
			}else{
				$json = json_encode($this->montaj->montajNoktalari($il,$ilce));
			}
			echo "<pre>";
			print_r(json_decode($json));
			echo "</pre>";
		}else{
			if($product_id){
				$urun_detay = $this->products->urunler(array('id' => $product_id), true);
				$kategori_idler = [0];
				$kategoriler = json_decode($urun_detay->kategoriler);
				foreach($kategoriler as $kategori){
					if ($kategori && is_numeric($kategori->id)) {
						array_push($kategori_idler,$kategori->id);
					}
				}
				$data_urun = [];
				$data_urun['kategoriler'] = $kategori_idler;
				$data_urun['run_flat'] = json_decode($urun_detay->run_flat);
				$this->parser->parse("default", array('yaz'=> json_encode($this->montaj->montajNoktalari($il,$ilce,$data_urun))));
				//$json = json_encode($this->montaj->montajNoktalari($il,$ilce,$kategori_idler));
			}else{
				$this->parser->parse("default", array('yaz'=> json_encode($this->montaj->montajNoktalari($il,$ilce))));
				//$json = json_encode($this->montaj->montajNoktalari($il,$ilce));
			}
		}
		//$this->parser->parse("default", array('yaz'=> json_encode($this->montaj->montajNoktalari($il,$ilce))));
	}


	public function ebatgetir($araba){
		$ebatlar = $this->products->ebatgetir($araba);
		$data["norm"] = array();
		$data["on_arka"] = array();
		$data["norm_count"] =  0;
		$data["on_arka_count"] =  0;
		foreach ($ebatlar as $ebat) {
			if(is_array($ebat)){
				foreach ($ebat as $key => $value) {
					if($key) {
						$value->on_arka = "arka";
					}else{
						$value->on_arka = "on";
					}
					$data["on_arka"][$value->diameter][] = $value;	
				}
				++$data["on_arka_count"] ;
			}else{
				$data["norm"][$ebat->diameter][] = $ebat;	
				++$data["norm_count"] ;
			}

		}
		$this->parser->parse("ebatgetir", $data);
		$this->output->cache(300); 
	}

	public function addtofavorite()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$member = $this->session->userdata('member');
			if ($member) {
				$urun = $this->input->post('urun');
				$data['status'] = $this->products->favoriekle($urun, $member->id);
			} else {
				$data['status'] = "user";
			}
		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}

		public function deltofavorite()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$member = $this->session->userdata('member');
			if ($member) {
				$urun = $this->input->post('urun');
				$data['status'] = $this->products->favorisil($urun, $member->id);
			} else {
				$data['status'] = "user";
			}
		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}


	public function addtofdhv()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$member = $this->session->userdata('member');
			if ($member) {
				$urun = $this->input->post('urun');
				$data['status'] = $this->products->haberverekle($urun, $member->id);
			} else {
				$data['status'] = "user";
			}
		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}


	public function siltofdhv()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$member = $this->session->userdata('member');
			if ($member) {
				$urun = $this->input->post('urun');
				$data['status'] = $this->products->haberversil($urun, $member->id);
			} else {
				$data['status'] = "user";
			}
		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}

	public function sepetekle()
	{		
		$sepetdata = array();
		$data = array();
		if ($this->input->method() == "post") {

			$urun 	= $this->input->post('urun');
			$miktar = $this->input->post('miktar');

			if ($urun && is_numeric($urun) && $miktar && is_numeric($miktar)) {
				$member = $this->session->userdata('member');
				if ($member) {
					$sepetdata['musteri'] = $member->id;
					$sepetdata['session'] = NULL;
				} else {
					$sepetdata['musteri'] = NULL;
					$sepetdata['session'] = session_id();
				}
				$sepetdata['zaman'] 	= time();
				$sepetdata['urun'] 		= $urun;
				$sepetdata['miktar'] 	= $miktar;

				$data['montaj'] = $this->products->urunler(array('id' => $urun), true)->montaj;
				$data['status'] = $this->basket->ekle($sepetdata);
			} else {
				$data['status'] = "no";
			}

		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}



	public function sepetsil()
	{
		$sepetdata 	= array();
		$data 		= array();

		if($this->input->method() == "post") {
			$urun 	= $this->input->post('urun');
			if ($urun && is_numeric($urun))
				$sepetdata['urun'] = $urun;
			$member = $this->session->userdata('member');
			if ($member) {
				$sepetdata['musteri'] = $member->id;
				$sepetdata['session'] = NULL;
			} else {
				$sepetdata['musteri'] = NULL;
				$sepetdata['session'] = session_id();
			}

			$data['status'] = $this->basket->sil($sepetdata);
			

		} else {
			$data['status'] = "no";
		}
		echo json_encode($data);
	}

	public function sepet()
	{
		$member = $this->session->userdata('member');
		if ($member) {
			$sepetdata['musteri'] = $member->id;
			$sepetdata['session'] = NULL;
		} else {
			$sepetdata['musteri'] = NULL;
			$sepetdata['session'] = session_id();
		}

		$sepettekiler = $this->basket->sepettekiler($sepetdata);
		$data = array();
		$data['urunler'] = $sepettekiler;
		$data['toplam']	 = count($sepettekiler);

		$toplamadet = 0;
		foreach ($sepettekiler as $key => $value) {
			if ($value->tip == "jant"){
				$toplamadet = $toplamadet + ($value->miktar * 4);
			}else{
				$toplamadet = $toplamadet + ($value->miktar);
			}
		}
		$data['toplamadet'] = $toplamadet;

		echo json_encode($data);
	}

	public function sepettekiler($type = null, $segment = NULL)
	{
		$member = $this->session->userdata('member');
		if ($member) {
			$sepetdata['musteri'] = $member->id;
			$sepetdata['session'] = NULL;
		} else {
			$sepetdata['musteri'] = NULL;
			$sepetdata['session'] = session_id();
		}
		
		$sepettekiler = $this->basket->sepettekiler($sepetdata);

		$data = array();
		$data['theme_url'] 	  = $this->theme_url;
		$data['sepettekiler'] = $sepettekiler;
		$data['segment'] 	  = $segment;
		if($type == 'ozet'){
			$this->load->library('campaign');
			$data['kampanyakodu'] = $kampanyakodu 	= $this->campaign->bekleyenkod();
			$data['indirim']	  = $indirim 		= $this->campaign->indirimhesapla($kampanyakodu, $sepettekiler);
			if($sepettekiler){
				if($sepettekiler[0]->temp_on_order){
						$this->load->model('basket/Basket_model', 'basket');
						$this->load->model('pages/Pages_model', 'pages');
						$order = $this->basket->getorder($sepettekiler[0]->temp_on_order);
						$data['order'] = $order;
				}
			}
			$this->parser->parse("modules/basket/ozet.tpl", $data);
		}else{
			$this->parser->parse("sepettekiler", $data);
		}
		
	}

	public function active_campaigns(){
		$now = date("Y-m-d H:i:s");
		$active_campaigns = $this->db->select('kmp.id')->from('kampanya kmp')->where('kmp.start_date <=',$now)->where('kmp.end_date >=',$now)->get()->result_array();
		foreach($active_campaigns as $key => $a_campaign){
			$active_campaigns[$key]['req_groups'] = $this->db->select('krg.id as id, krg.group_type group_type')->from('kampanya_req_group krg')->from('kampanya_req kr')->where('kr.kampanya_id',$a_campaign['id'])->where('krg.id = kr.req_id')->where('krg.req_res','req')->get()->result_array();	
			$active_campaigns[$key]['res_groups'] = $this->db->select('krg.id')->from('kampanya_req_group krg')->from('kampanya_req kr')->where('kr.kampanya_id',$a_campaign['id'])->where('krg.id = kr.req_id')->where('krg.req_res','res')->get()->result_array();
		}
		
		foreach($active_campaigns as $key_1 => $a_campaign){
			foreach($active_campaigns[$key_1]['req_groups'] as $key_2 => $group){
				$active_campaigns[$key_1]['req_groups'][$key_2]['items'] = $this->get_req_group_items($group['id']);
			}
			foreach($active_campaigns[$key_1]['res_groups'] as $key_2 => $group){
				$active_campaigns[$key_1]['res_groups'][$key_2]['items'] = $this->get_req_group_items($group['id']);
			}
		}
		/*foreach($active_campaigns as $a_campaign){
			echo '<hr><hr>';
			echo $a_campaign['id'];
			foreach($a_campaign['req_groups'] as $rq){
				echo '<hr>';	
				print_r($rq['items']);		
			}
			foreach($a_campaign['res_groups'] as $rq){
				echo '<hr>';	
				print_r($rq['items']);		
			}
		}*/
		return $active_campaigns;
	}
	
	public function get_req_group_items($req_group_id){
		$req_group_items = $this->db->select('kir.*')->from('kampanya_req_group krg')->from('kampanya_item_req kir')->where('kir.req_id = krg.id')->where('krg.id',$req_group_id)->get()->result_array();	
		return $req_group_items;
	}
	public function sepet_clear_disc($sepet){
		$update_zero = [];
		$update_zero['kargo_disc'] = 0;
		$update_zero['montaj_disc'] = 0;
		$update_zero['urun_disc'] = 0;
		foreach($sepet as $key=>$sepetteki){
			$this->db->where('id',$sepetteki->id)->update('sepettekiler',$update_zero);
		}
	}
	
	public function bcr_test(){
				$member = $this->session->userdata('member');
		if ($member) {
			$sepetdata['musteri'] = $member->id;
			$sepetdata['session'] = NULL;
		} else {
			$sepetdata['musteri'] = NULL;
			$sepetdata['session'] = session_id();
		}

		$sepet = $this->basket->sepettekiler($sepetdata);
		$this->sepet_clear_disc($sepet);
		$active_campaigns = $this->active_campaigns();
		foreach($active_campaigns as  $active_campaign){
			$rem_sepet = $this->check_campaign($active_campaign['req_groups'],$sepet);
			if($rem_sepet){
				$this->apply_campaign($active_campaign['res_groups'],$rem_sepet,$sepet);
			}
		}
		$sepet = $this->basket->sepettekiler($sepetdata);
			echo '<table border="1" cellpadding="3"><thead><tr><td>URUN_ADI</td><td>URUN_ID</td><td>FIYAT</td><td>INDIRIM1</td><td>INDIRIM2</td><td>KARGOINDIRIMYUZDE</td><td>MONTAJINDIRIMYUZDE</td></tr></thead><tbody>';
			
			foreach($sepet as $sepet_urun){
				echo '<tr>';
				echo '<td>'.$sepet_urun->urun.'</td>'.'<td>'.$sepet_urun->urun_adi.'</td>'.'<td>'.$sepet_urun->fiyat.'</td>'.'<td>'.$sepet_urun->indirim_orani.'</td>'.'<td>'.$sepet_urun->urun_disc.'</td>'.'<td>'.$sepet_urun->kargo_disc.'</td>'.'<td>'.$sepet_urun->montaj_disc.'</td>';
				echo '</tr>';
			}
			echo '</tbody></table>';
	}
	public function check_campaign($req_groups,$sepet){
		foreach($req_groups as $req_group){
			if($req_group['group_type'] == 'AND'){
				foreach($req_group['items'] as $req_item){
					$condition_item_control = $this->check_item_cnt($req_item,$sepet);
					if($condition_item_control == false){
						$sepet = null;
					}else{
						$sepet = $condition_item_control;
					}
				}
			}else{
				$cont = false;
				foreach($sepet as $key => $sepetteki){
					$keep = false;
					foreach($req_group['items'] as $req_item){
						$condition_item_control = $this->check_item_cnt($req_item,[$sepetteki]);
						if($condition_item_control){
							//echo '<p>kep</p>';
							$keep=true; break;
						}
					}
					if(!$keep){
						unset($sepet[$key]);
					}
				}
			}
			if(!$sepet || sizeOf($sepet) == 0){ return false;}
		}
		return $sepet;
	}
	public function check_item_cnt($req_item,$sepet){
		if($req_item['item_id'] == 1){
					foreach($sepet as $key => $sepetteki){
						if($sepetteki->kategori != $req_item['param_1'] ){
							unset($sepet[$key]);
						}
					}
		}else if($req_item['item_id'] == 2){
				$this->load->model('products/Brand_model', 'brand');
				foreach($sepet as $key => $sepetteki){
					$marka_adi = $this->brand->markalar(array( 'id' => $req_item['param_1']),true)->adi;
					if($sepetteki->markaadi != $marka_adi){
						unset($sepet[$key]);
					}
				}		
		}else if($req_item['item_id'] == 3){
				echo $req_item['param_1'].'ile'.$req_item['param_2'].' arası taban';
				foreach($sepet as $key => $sepetteki){
					$urun_taban = $this->products->urunler(array('id'=>$sepetteki->urun),true)->taban_deger;
					if($req_item['param_1'] ){
						if($urun_taban >= $req_item['param_1'] ){
							if($req_item['param_2'] ){
								if($urun_taban <= $req_item['param_2'] ){
									continue;
								}
							}else{
								continue;
							}
						}
					}
					unset($sepet[$key]);
				}
		}else if($req_item['item_id'] == 4){
				echo $req_item['param_1'].'ile'.$req_item['param_2'].' yanak';
				foreach($sepet as $key => $sepetteki){
					$urun_yanak = $this->products->urunler(array('id'=>$sepetteki->urun),true)->yanak_deger;
					if($req_item['param_1'] ){
						if($urun_yanak >= $req_item['param_1'] ){
							if($req_item['param_2'] ){
								if($urun_yanak <= $req_item['param_2'] ){
									continue;
								}
							}else{
								continue;
							}
						}
					}
					unset($sepet[$key]);
				}
		}else if($req_item['item_id'] == 5){
				echo $req_item['param_1'].'ile'.$req_item['param_2'].' jant_capi';
				foreach($sepet as $key => $sepetteki){
					$urun_jcapi = $this->products->urunler(array('id'=>$sepetteki->urun),true)->jant_capi_deger;
					if($req_item['param_1'] ){
						if($urun_jcapi >= $req_item['param_1'] ){
							if($req_item['param_2'] ){
								if($urun_jcapi <= $req_item['param_2'] ){
									continue;
								}
							}else{
								continue;
							}
						}
					}
					unset($sepet[$key]);
				}
		}else if($req_item['item_id'] == 6){
				foreach($sepet as $key => $sepetteki){
					$urun_runflat = $this->products->urunler(array('id'=>$sepetteki->urun),true)->run_flat;
					echo $urun_runflat;echo '<hr>';
					$param_1 = $req_item['param_1'] ;
					if($param_1){
						if($param_1 == 'Var'){
							if($urun_runflat != '' && ($urun_runflat != null)){
								continue;
							}
						}else if($param_1 == 'Yok'){
							if($urun_runflat == '' && ($urun_runflat == null)){
								continue;
							}							
						}
					}else{continue;}
					unset($sepet[$key]);
				}
		}else if($req_item['item_id'] == 7){
				foreach($sepet as $key => $sepetteki){
					$urun_seal = $this->products->urunler(array('id'=>$sepetteki->urun),true)->seal;
					echo $urun_seal;echo '<hr>';
					$param_1 = $req_item['param_1'] ;
					if($param_1){
						if($param_1 == 'Var'){
							if($urun_seal != '' && ($urun_seal != null)){
								continue;
							}
						}else if($param_1 == 'Yok'){
							if($urun_seal == '' && ($urun_seal == null)){
								continue;
							}							
						}else{continue;}
					}
					unset($sepet[$key]);
				}
		}else if($req_item['item_id'] == 8){
				$sepet_toplam = 0;
				foreach($sepet as $key => $sepetteki){
					$fiyat =((float)$sepetteki->fiyat);
					$indirim_carpani = (100 - ((float)$sepetteki->indirim_orani))/100;
					$sepet_toplam += ($fiyat * $indirim_carpani);
				}
				$sepet_toplam;
				if($sepet_toplam >= $req_item['param_1']  && $sepet_toplam<= $req_item['param_2']){
					return $sepet;
				}else{
					return false;
				}
		}else if($req_item['item_id'] == 9){
				foreach($sepet as $key => $sepetteki){
					$urun_mevsim = $this->products->urunler(array('id'=>$sepetteki->urun),true)->mevsim;
					if($urun_mevsim == $req_item['param_1']){
						continue;
					}else{
						unset($sepet[$key]);
					}
				}
		}else if($req_item['item_id'] == 10){
				foreach($sepet as $key => $sepetteki){
					if($sepetteki->montaj_islemler){
						$montaj_islemler = (array) json_decode($sepetteki->montaj_islemler);
						$montaj_islemler = get_object_vars(array_pop($montaj_islemler));
						$montaj_sayisi = array_pop($montaj_islemler);
						if($montaj_sayisi >= $req_item['param_1']  && $montaj_sayisi<= $req_item['param_2']){
							continue;
						}else{
							unset($sepet[$key]);
						}
					}else{
							unset($sepet[$key]);
					}
				}
		}else if($req_item['item_id'] == 11){
				foreach($sepet as $key => $sepetteki){
					if($sepetteki->urun == $req_item['param_1']){
						continue;
					}
					else{
							unset($sepet[$key]);
					}
				}
		}else if($req_item['item_id'] == 12){
				foreach($sepet as $key => $sepetteki){
					$urun_uretim_yili = $this->products->urunler(array('id'=>$sepetteki->urun),true)->uretim_tarihi;
					if($urun_uretim_yili >= $req_item['param_1']  && $urun_uretim_yili<= $req_item['param_2']){
						continue;
					}else{
						unset($sepet[$key]);
					}
				}
		}else if($req_item['item_id'] == 18){
				$toplam_miktar = 0;
				foreach($sepet as $key => $sepetteki){
					$toplam_miktar += $sepetteki->miktar;
				}
				if($toplam_miktar >= $req_item['param_1']  && $toplam_miktar<= $req_item['param_2']){
					continue;
				}else{
					return false;
				}
		}
		if(!$sepet){
			return false;
		}else{
			return $sepet;
		}
	}
	public function apply_campaign($res_groups,$sepet,$tum_sepet){
		foreach($res_groups as $res_group){
			foreach($res_group['items'] as $res_item){
				if($res_item['item_id'] == 15){
					$indirim_orani = ((float)$res_item['param_1']);
					if(0<$indirim_orani && $indirim_orani<=100){
						foreach($sepet as $key => $sepetteki){
							$res = $this->db->where('id',$sepetteki->id)->update('sepettekiler',array('montaj_disc' =>$indirim_orani));
						}
					}
				}else if($res_item['item_id'] == 16){
					$indirim_orani = ((float)$res_item['param_1']);
					if(0<$indirim_orani && $indirim_orani<=100){
						foreach($sepet as $key => $sepetteki){
							$res = $this->db->where('id',$sepetteki->id)->update('sepettekiler',array('kargo_disc' =>$indirim_orani));
						}
					}
				}else if($res_item['item_id'] == 17){
					$indirim_orani = ((float)$res_item['param_1']);
					if(0<$indirim_orani && $indirim_orani<=100){
						foreach($sepet as $key => $sepetteki){
							$ind_disc =((float) $sepetteki->urun_disc)+$indirim_orani;
							$res = $this->db->where('id',$sepetteki->id)->update('sepettekiler',array('urun_disc' =>$ind_disc));
						}
					}
				}else if($res_item['item_id'] == 13){
					$indirim_net= ((float)$res_item['param_1']);
						foreach($sepet as $key => $sepetteki){
							$fiyat =((float)$sepetteki->fiyat);
							$indirim_carpani = (100 - ((float)$sepetteki->indirim_orani))/100;
							$gercek_fiyat = ($fiyat * $indirim_carpani);
							$indirim_orani = number_format($indirim_net/$gercek_fiyat*100,4,'.','');
							$ind_disc =((float) $sepetteki->urun_disc)+$indirim_orani;
							if(0<$indirim_net && $indirim_orani>0){
								$res = $this->db->where('id',$sepetteki->id)->update('sepettekiler',array('urun_disc' =>$ind_disc));
							}
						}
				}else if($res_item['item_id'] == 14){
						$hediye_urun = ((int)$res_item['param_1']);
						foreach($tum_sepet as $key => $sepetteki){
							if($sepetteki->urun == $hediye_urun){
									$ind_disc =((float) $sepetteki->urun_disc)+(100/$sepetteki->miktar);
									$res = $this->db->where('id',$sepetteki->id)->update('sepettekiler',array('urun_disc' =>$ind_disc));
							}
						}
				}
				
			}
		}
	}


	public function compare($main_id)
	{
		$anaurun = $this->products->urunler(array('id'=>$main_id),true);
		$kategoriler = json_decode($anaurun->kategoriler);
		$istenilen = $kategoriler[0];
		$comparelist = $this->products->comparelist();
		$cikti = array();
		foreach ($comparelist as  $value) {
			$kategoriler = json_decode($value->kategoriler);
			$rkategori = $kategoriler[0];
			if($istenilen == $rkategori)
				$cikti[]=$value;
		}
		$data = array();
		$data['theme_url'] 	  = $this->theme_url;
		$data['urunler'] = $cikti;
		$this->parser->parse("compare", $data);
	}

	public function seccode()
	{
		$this->seccode->create();
	}

	public function likeComment(){

		$this->load->model('products/Comment_model', 'comment');
		echo $this->comment->likeComment($this->input->post());die;

	}


	/**/
	public function montajhizmetleri($firma_id = NULL, $urun_id = NULL)
	{
		$data = array();
		$data['theme_url'] = $this->theme_url;

		if ($firma_id && is_numeric($firma_id)) {
			$firma = $this->montaj->detay($firma_id);
			if ($firma) {
				$data['firma'] = $firma;
			}
		}
		$data['urun_id'] = $urun_id;
		$data['urun_detay'] = $this->products->urunler(array('id' => $urun_id), true);

		/**/
		$member = $this->session->userdata('member');
		if ($member) {
			$sepetdata['musteri'] = $member->id;
			$sepetdata['session'] = NULL;
		} else {
			$sepetdata['musteri'] = NULL;
			$sepetdata['session'] = session_id();
		}
		$sepetdata['urun'] = $urun_id;

		$sepetteki = $this->basket->sepettekiler($sepetdata, true);

		if (!is_null($sepetteki->montaj_islemler)) {
			$montajlarim = json_decode($sepetteki->montaj_islemler, true);
			reset($montajlarim);
			$montajlarim = current($montajlarim);
			$data['montajlarim'] = $montajlarim;
		}

		$data['sepetteki'] = $sepetteki;
		/**/

		$this->parser->parse('montajhizmetleri', $data);
	}

	public function montajhizmetiekle()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$data['status'] = true;
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}

	public function sepetemontajhizmetiekle($urunid = NULL)
	{
		$data = array();
		if ($this->input->method() == "post") {
			$pdata = $this->input->post('montajhizmetleri');
			if (is_array($pdata)) {
				$pdata = json_encode($pdata);
			} else {
				$pdata = NULL;
			}
			$data['status'] = $this->basket->mhekle($pdata, $urunid);
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}

	public function sepetkargoguncelle($sepetteki = NULL)
	{
		$data = array();
		if ($this->input->method() == "post") {
			$kargo = $this->input->post('kargo');
			$data['status'] = $this->basket->sepetkargoguncelle($sepetteki, $kargo);
		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}

	public function sepettekilerbasket()
	{
		$member = $this->session->userdata('member');
		if ($member) {
			$sepetdata['musteri'] = $member->id;
			$sepetdata['session'] = NULL;
		} else {
			$sepetdata['musteri'] = NULL;
			$sepetdata['session'] = session_id();
		}


		$sepettekiler = $this->basket->sepettekiler($sepetdata);

		$data = array();
		$data['theme_url'] 	  = $this->theme_url;
		$data['sepettekiler'] = $sepettekiler;

		$this->parser->parse("modules/basket/sepettekileric", $data);
	}

	public function montajsil()
	{
		$data = array();
		if ($this->input->method() == "post") {
			$id 		= $this->input->post('id');
			$firma_id 	= $this->input->post('firma_id');
			$id_str 	= $this->input->post('id_str');
			
			if ($id && $firma_id && $id_str) {
				$data['status'] = $this->basket->montajsil($id, $firma_id, $id_str);
			} else {
				$data['status'] = false;
			}

		} else {
			$data['status'] = false;
		}
		echo json_encode($data);
	}




	public function adressil(){

		$this->load->model('members/Members_model', 'members');
		echo $this->members->adressil($this->input->get());
	}


public function anketal(){
		$data = array();
		if ($this->input->method() == "post") {
			echo $this->extraservices->anketal($this->input->post('anket_id'),$this->input->post('secenek_id'));
		}else{
			echo "Bilinmeyen Bir Hata Oldu";
		}
		
}

public function anketcevaplar($anket_id){
	$cevaplar = $this->extraservices->anketcevaplar($anket_id);
	echo json_encode($cevaplar);
}
public function sayac(){
	$pdata = $this->input->post();
	$kayit = array();
	foreach ($pdata as $key => $value) {
		$kayit[$key] = base64_decode($value); 
	}
	 $this->extraservices->sayac($kayit);
}
public function testmail(){
		$this->load->model('mail/mail_model', 'mail');
		$email = "berkay.klc.en@gmail.com";
		$this->load->model('members/Members_model', 'members');
		$siparis_id = $this->input->get()['siparis_id'];
		$data = array();
		$data['order'] = $this->db->select('*')->from('siparisler')->where('id =',$siparis_id)->get()->row();
		$mail_template = json_decode($this->extraservices->ayarlar('siparisMailText-2',true)->value);
		$mail_template->mail_aciklamasi = str_replace('#siparisid#','<a class="id">'.$siparis_id.'</a>',$mail_template->mail_aciklamasi);
		$data['mail_template'] = $mail_template;
		$html = $this->parser->parse('test_mail',$data,true);
		$this->mail->htmlmail(array('email'=> $email ,'title'=>$mail_template->mail_basligi1, 'html'=> $html));
		echo $html;
}
public function testmail2(){
		$this->load->model('members/Members_model', 'members');
		$mail_texts = $this->extraservices->ayarlar('siparisMailText-7',true);
		$siparis_id = $this->input->get()['siparis_id'];
		$data = array();
		$data['order'] = $this->db->select('*')->from('siparisler')->where('id =',$siparis_id)->get()->row();
		$data['mailtext'] = $mail_texts;
		$this->parser->parse('odemebekliyor', $data);
}
public function mesafeli_satis_sozlesmesi($hash){
		$this->load->model('pages/Pages_model', 'pages');
		$data = array();
		$order = $this->db->select('*')->from('siparisler')->where('hash_value =',$hash)->get()->row();
		if(!$hash || !$order){
			echo 'invalid access';
			die();
		}
		$data['order'] = $order;
		$sayfa = $this->pages->sayfalar(array('id' => 12), TRUE);
		$sayfa->sayfa_icerik = $this->revise_sayfa_icerik($sayfa->sayfa_icerik,$order);
		$data['page'] = $sayfa;
		$data['theme_url'] = $this->theme_url;  
		print_r( $data['page']->sayfa_icerik);
		$this->parser->parse("pages/index", $data);
		
}
public function revise_sayfa_icerik($icerik,$order){
		$data = json_decode($order->data);
		$odeme_data = json_decode($order->odemedata);
		$result = [];
		$result['musteri_adi'] = ($data->ticari_unvani ? $data->ticari_unvani : $data->adi.' '.$data->soyadi);
		$result['musteri_mail'] = ($data->kur_email ? $data->kur_email : $data->email);
		$result['musteri_telefon'] = ($data->kur_cep_telefonu ? $data->kur_cep_telefonu : $data->cep_telefonu);
		$result['musteri_adresi'] = ($data->kur_adres ? $data->kur_adres : $data->adres);
		$result['siparis_tarihi'] = $order->tarih;
		
		$urunler = json_decode($order->urunler);
		$urun_html = '<table style="width:100%;text-align: left;"  border="1" cellpadding="10" class="table"><thead><tr><th>Ürün Kodu</th><th>Ürün Kodu</th><th>Adet</th><th>Birim Fiyatı</th><th>Birim İndirimi</th><th>Kupon</th><th>Puan</th>'.
		'<th>Toplam Satış Tutarı</th><th>Vade Farkı</th><th>KDV Dahil Toplam Tutar</th><tr></thead><tbody>';
		$urunler_length = ' rowspan="'.sizeOf($urunler).'"';
		$vade_farki = 0;
		if($order->odeme_tipi == 2){
			if($odeme_data->komisyon){
				$vade_farki = str_replace(',','',$odeme_data->komisyon);
			}
		}
		$son_fiyat = $data->sepet_tutar.' TL';
		if($order->odeme_tipi == 2){
			if($odeme_data->son_fiyat){
				$son_fiyat = number_format($odeme_data->son_fiyat,2).' TL';
			}
		}
		$total_price_order = 0;
		foreach($urunler as $urun){
			$urun_vade_farki = '-';
			$toplam_satis_tutari = round($urun->liste_fiyati*(100- $urun->indirim_orani)/100*$urun->miktar);
			$KDV_total_price = $toplam_satis_tutari;
			if($vade_farki>0){
				$urun_vade_farki = $vade_farki/str_replace(',','',$data->sepet_tutar)*$toplam_satis_tutari;
				if($urun_vade_farki !== $toplam_satis_tutari){
					$KDV_total_price = $toplam_satis_tutari+$vade_farki;
				}
				$urun_vade_farki = number_format($urun_vade_farki,2); 
			}else{
				$urun_vade_farki = '-';
			}
			$total_price_order += $KDV_total_price;
			$toplam_satis_tutari = number_format($toplam_satis_tutari,2);
			$KDV_total_price = number_format($KDV_total_price,2);
			if($urun_vade_farki != '-'){
				if($urun_vade_farki == $toplam_satis_tutari){
					$urun_vade_farki = '-';
				}else{
					$urun_vade_farki .= ' TL';
				}
			}
			
			$urun_html .= '<tr><td>'.$urun->stok_kodu.'</td><td>'.$urun->urun_adi.'</td><td>'.$urun->miktar.'</td><td>'.$urun->liste_fiyati.' TL</td><td>'.number_format($urun->indirim_orani,2).'%'.'</td><td>'
			.($data->kampanyakodu ? $data->kampanyakodu : '-').'</td><td>'.'-'.'</td>';
			
				$urun_html .='<td>'.$toplam_satis_tutari.' TL</td><td>'.$urun_vade_farki.'</td><td>'.$KDV_total_price.' TL</td>';
			
			$urun_html .= '</tr>';
		}
		$urun_html .= '</tbody><tfoot><tr><td colspan="10" style="text-align:right">Toplam Sipariş Bedeli: '.number_format($total_price_order,2).' TL</td></tr></tfoot></table><style></style>';
		$result['urunler'] = $urun_html;
		foreach($result as $key => $r_item){
			$hash = '#'.$key.'#';
			$icerik = str_replace($hash,$r_item,$icerik);
		}
		return $icerik;
}

	/**/
}

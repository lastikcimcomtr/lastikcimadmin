<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }


    public function location($id = NULL)
    {
        if ($id) {
            echo json_encode($this->extraservices->ilceler($id));
        } else {
            echo json_encode($this->extraservices->iller());
        }
    }

    public function montajNoktalari($il = 34, $ilce = NULL)
    {
        $this->load->model('montaj/montaj_model', 'montaj');
        echo json_encode($this->montaj->montajNoktalari($il, $ilce));
    }

    public function montajsil()
    {
        $this->load->model('montaj/montaj_model', 'montaj');
        echo json_encode($this->montaj->montajsil($this->input->post()));
    }

    public function urunisim()
    {
        $this->load->model('products/Products_model', 'products');
        echo json_encode($this->products->lastiktitle($this->input->get()));
    }

    public function markagrup($marka, $grup)
    {
        $this->load->model('products/Brand_model', 'brand');
        echo json_encode($this->brand->markagrupset($marka, $grup));
    }


    /*public function order_durum()
    {
        $this->load->model('orders/Orders_model', 'orders');
        echo json_encode($this->orders->durum_degis($this->input->get()));
    }*/

    public function siparisnotu()
    {
        $this->load->model('orders/Orders_model', 'orders');
        echo json_encode($this->orders->siparisnotu($this->input->get()));
    }


    public function postdurum()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->posdurumset($this->input->get()));
    }

    public function posoran()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->posoranset($this->input->get()));
    }

    public function posekle()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->posadd($this->input->get()));
    }

    public function posguncelle()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->posedit($this->input->get()));
    }

    public function bankahesapekle()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->bankaadd($this->input->get()));

    }

    public function bankasil()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->bankadelete($this->input->get()));

    }

    public function bankahesapguncelle()
    {
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->bankaedit($this->input->get()));

    }

    public function kargofiyati($id)
    {
        $data = $this->input->post();
        $data['id'] = $id;
        $this->load->model('products/Category_model', 'category');
        echo json_encode($this->category->kargofiyatlariset($data));
    }

    public function siralamaedit($id)
    {
        $data = $this->input->post();
        $data['id'] = $id;
        $this->load->model('products/Products_model', 'products');
        echo json_encode($this->products->siralamaedit($data));
    }
	
	public function posguncelleaciklama(){
        $this->load->model('settings/Settings_model', 'settings');
        echo json_encode($this->settings->poseditaciklama($this->input->get()));
	}
	
    public function geturunetiket()
    {
        $data = $this->input->get();
        $this->load->model('products/Products_model', 'products');
        $result = $this->products->etiket_ad($data,10);
        $response = array();
        $response['pagination'] = new stdClass;
        $response['pagination']->more = false;
        $response['results'] = array();
        foreach ($result as $result){
            $item = new stdClass();
            $item->id = $result->id;
            $item->text = $result->adi;
            array_push($response['results'], $item);
        }
        echo json_encode($response);
    }
	public function order_durum(){
        $this->load->model('orders/Orders_model', 'orders');
        $data = $this->input->get();
		echo json_encode($this->orders->durum_degis($data));
	}
	public function siparis_total($siparis_id){
        $this->load->model('orders/Orders_model', 'orders');
		$siparis = [];
		$siparis['id'] = $siparis_id;
		$siparis_last = $this->orders->orders($siparis,true);
		if($siparis_last){
			if($siparis_last->odeme_tip == 1){
				$siparis_last = json_decode($siparis_last->odemedata)->son_fiyat;
			}else if($siparis_last->odeme_tip == 2){
				$siparis_last = json_decode($siparis_last->odemedata)->son_fiyat;
			}else{
				$siparis_last = json_decode($siparis_last->odemedata)->son_fiyat;
			}
			echo json_encode($siparis_last);
		}else{
			echo false;
		}
	}
	
	public function firma_hizmetler($firma_id){
		$montaj_noktasi_hizmetleri = $this->db->select('mnh.id , mh.id_str ,mh.baslik')->from('montaj_noktasi_hizmetleri mnh')->from('montaj_hizmetleri mh')->where('mnh.id_str = mh.id_str')->where('mnh.firma_id',$firma_id)->get()->result();
		echo json_encode($montaj_noktasi_hizmetleri);
	}
	/*
	Array ( [urun_id] => 29763 [order_id] => 447440 [firma_id] => 32 [id_str] => 2447 [adet] => 1 )
	*/
	public function urn_hizmet_ekle(){
		if ($this->input->method() == "post") {
			$pdata = $this->input->post();
			$yeni = $pdata['yeni'];
			$mnh = $this->db->select('mnh.id , mnh.firma_id ,  mh.id_str , mnh.fiyat ,mn.firma_adi, mh.baslik montaj_hizmeti_adi')->from('montaj_noktalari mn')->from('montaj_noktasi_hizmetleri mnh')->from('montaj_hizmetleri mh')->where('mnh.id_str = mh.id_str')->where('mnh.id', $yeni['id_str'])->where('mnh.firma_id = mn.id')->get()->row();
			$mnh->adet = $yeni['adet'];
			$urunler = $this->db->select('urunler')->where('id', $yeni['order_id'])->from('siparisler')->get()->row()->urunler;
			$urunler = json_decode($urunler);
			foreach($urunler as $key_1=> $urn){
				if($urn->id == $yeni['urun_id']){
					if( $urunler[$key_1]->hizmetler){
						$new_array = $urunler[$key_1]->hizmetler;
					}else{
						$new_array = [];
					}
					array_push($new_array,$mnh);
					$urunler[$key_1]->hizmetler = $new_array;
				}
			}
			$urunler = json_encode($urunler);
			//echo $urunler;
			$this->db->where('id', $yeni['order_id'] )->update('siparisler',  array('urunler' => $urunler));
			echo true;
		}
	}
	public function urn_hizmet_sil(){
		if ($this->input->method() == "post") {
			$pdata = $this->input->post();
			$urunler = $this->db->select('urunler')->where('id', $pdata['siparis_id'])->from('siparisler')->get()->row()->urunler;
			$urunler = json_decode($urunler);
			foreach($urunler as $key_1=> $urn){
				if($urn->id == $pdata['urun_id']){
					if( $urunler[$key_1]->hizmetler){
						foreach($urunler[$key_1]->hizmetler as $key_2=> $hizmet){
							if($hizmet->id == $pdata['hizmet_id']){
								unset($urunler[$key_1]->hizmetler[$key_2]);
								$urunler[$key_1]->hizmetler = array_values($urunler[$key_1]->hizmetler);
							}
						}
					}
				}
			}
			$urunler = json_encode($urunler);
			$this->db->where('id', $pdata['siparis_id'] )->update('siparisler',  array('urunler' => $urunler));
			echo true;
		}
	}
	public function sozlesmeleri_ilet(){
		
		if ($this->input->method() == "post") {
			$pdata = $this->input->post();
			$order = $this->db->select('*')->where('id', $pdata['siparis_id'])->from('siparisler')->get()->row();
			$data = [];
			$data['order'] = $order;
			$order_data = json_decode($order->data);
			if(isset($order_data->email)){
				$email = $order_data->email;
			}elseif (isset($order_data->kur_email)){
				$email = $order_data->kur_email;
			}else{
				$email = "siparis@lastikcim.com.tr";
			}
			$this->load->model('mail/Mail_model', 'mail');
			$this->load->model('orders/Orders_model', 'orders');
			$html = $this->parser->parse('modules/mail/uyesiparismail',$data,true);
			$mail_status = $this->mail->htmlmail(array('email'=> $email ,'title'=>'Güncel Sipariş Bilgilerin', 'html'=> $html));	
            $mail_status = $this->mail->htmlmail(array('email'=> 'siparis@lastikcim.com.tr' ,'title'=>'Güncel Sipariş Bilgilerin', 'html'=> $html));    
			
		}
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller  {


    public $theme_url;
    public function __construct()
    { 
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Roles_model', 'roles');
    }

    public function index($type = 'grup',$id )
    {
        $data['theme_url'] 	= $this->theme_url;
        $data['type']     = $type ;
        $data['sid']     = $id ;
        $data['roller'] = $this->roles->roles($type ,$id );
        $this->parser->parse("roles/index", $data);
    }


    public function edit($type,$id,$rolid,$rol,$deger){
        echo $this->roles->edit($type,$id,$rolid,$rol,$deger);
    }


}

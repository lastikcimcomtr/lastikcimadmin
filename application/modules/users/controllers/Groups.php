<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends CI_Controller  {


    public $theme_url;
    public function __construct()
    { 
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Groups_model', 'groups');
    }

    public function index($page = 1)
    {
        $data['theme_url'] 	= $this->theme_url;
        $data['kullanici_gruplari'] = $this->groups->gruplar();
        $this->load->model('Users_model', 'users');
        foreach ($data['kullanici_gruplari']  as $key => $value) {
            $value->kullaniciSayisi = $this->users->kullanicilar(array('count'=>1,'grup'=>$value->id))->toplam ;
            $data['kullanici_gruplari'][$key] =$value;
        }
        $this->parser->parse("groups/index", $data);
    }
  
    public function add()
    {
        echo $this->groups->save($this->input->get());
    } 
    public function edit()
    {
        echo $this->groups->edit($this->input->get());
    }
    public function delete($id)
    {
        echo $this->groups->delete($id);
    }
   
}

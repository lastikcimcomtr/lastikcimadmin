<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller  {


    public $theme_url;
    public function __construct()
    { 
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Users_model', 'users');
    }

    public function index($page = 1)
    {
        $data['theme_url'] 	= $this->theme_url;

        $this->load->model('Groups_model', 'groups');
        $data['kullanici_gruplari'] = $this->groups->gruplar();
        $data['kullanicilar'] = $this->users->kullanicilar(null,$page,50);
        $this->parser->parse("index", $data);
    }

    public function add()
    {
        echo $this->users->save($this->input->get());
    } 
    public function edit()
    {
        echo $this->users->edit($this->input->get());
    }
    public function delete($id)
    {
        echo $this->users->delete($id);
    }

    public function islem_kayitlari($user = null){
         $sayfa = $this->input->get('sayfa');
        if ($sayfa < 1)
            $sayfa = 1;
        $data['theme_url']    = $this->theme_url;

       $data['islemler'] = $this->users->islem_kayitlari($user, $sayfa);
        $this->parser->parse("islem_kayitlari", $data);
    }

}

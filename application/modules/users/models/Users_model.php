<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function kullanicilar($filtreler,$page = 1 ,$limit = 50){
		if(isset($filtreler['count'])) {	
			$this->db->select("count(*) toplam");
		}else {	
			$this->db->select("*");
		}
		$this->db->from("kullanicilar");
		foreach ($filtreler as $filtre => $deger) {
			if( $filtre <> 'count'){
				$this->db->where($filtre, $deger);
			}
		}
		$this->db->where('sil',0);
		$this->db->limit($limit,($page-1)*$limit);
		if(isset($filtreler['count']) || isset($filtreler['id'])){
			return $this->db->get()->row();
		}else{
			return $this->db->get()->result();
		}
	}

	public function save($data){
		$data['durum'] = 1;
		$data['kayit'] = time();
		//print_r($data);
		$this->db->db_debug = false;
		if( $this->db->insert("kullanicilar",$data))
		{
			return 1;
		}else
		{
			return 0;
		}
	}	
	public function edit($data){
		$this->db->db_debug = false;
		$this->db->where('id', $data['id']);
		if($data['sifre'] == ""){
			unset($data['sifre']);
		}
		return $this->db->update("kullanicilar",$data);
	}	
	public function delete($id){
		$this->db->where('id', $id);
		$data['sil'] = 1;
		return $this->db->update("kullanicilar",$data);
	}

	public function islem_kayitlari($user, $page = 1,$limit=50){
		$this->db->select('kik.id,
			kik.ip,
			kik.tarih,
			kik.modul_method,
			kik.aciklama,
			kik.data,
			k.kullanici,
			k.ad,
			k.soyad');
		$this->db->from('kullanici_islem_kayitlari kik');
		$this->db->join('kullanicilar k','kik.kullanici = k.id');
		if($user)
			$this->db->where('kik.kullanici',$user);
		$this->db->limit($limit,($page-1)*$limit);
		$this->db->order_by('kik.tarih desc');
		return $this->db->get()->result();

	}


}

?>
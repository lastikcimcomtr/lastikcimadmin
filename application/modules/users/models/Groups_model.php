<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function gruplar($filtreler=NULL,$page = 1 ,$limit = 50){
		if(isset($filtreler['count'])) {	
			$this->db->select("count(*) toplam");
		}else {	
			$this->db->select("*");
		}
		$this->db->from("kullanici_gruplar");
		foreach ($filtre as $filtre => $deger) {
			if( $filtre <> 'count'){
				$this->db->where($filtre, $deger);
			}
		}
		$this->db->limit($limit,($page-1)*$limit);
		if(isset($filtreler['count']) || isset($filtreler['id'])){
			return $this->db->get()->row();
		}else{
			return $this->db->get()->result();
		}
	}


	public function save($data){
		$this->db->db_debug = false;
		return $this->db->insert("kullanici_gruplar",$data);
	}	
	public function edit($data){
		$this->db->db_debug = false;
		$this->db->where('id', $data['id']);
		return $this->db->update("kullanici_gruplar",$data);
	}	
	public function delete($id){
		$this->db->where('id', $id);
		return $this->db->delete("kullanici_gruplar");
	}

	

}

?>
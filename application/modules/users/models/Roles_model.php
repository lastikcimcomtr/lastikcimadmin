<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function roles($type,$id){
		$this->db->select('*');
		$this->db->from('kullanici_roller');
		$roller = $this->db->get()->result();
		if($type == 'grup'){
			$gid = $id;
		}else{
			$this->db->select('*');
			$this->db->from('kullanicilar');
			$this->db->where('id',$id);
			$user = $this->db->get()->row();
			@$data->user =$user ;
			$gid = $user->grup;
			$this->db->select('*');
			$this->db->from('kullanici_verilen_roller');
			$this->db->where('kullanici_id',$id);
			$user_rol = $this->db->get()->result();
			$user_rol_duzenli= array();
			foreach ($user_rol as $key => $value) {
				$user_rol_duzenli[$value->verilen_rol_id] = $value;
			}
		}
		$this->db->select('*');
		$this->db->from('kullanici_gruplar');
		$this->db->where('id',$gid);
		$grup = $this->db->get()->row();
		@$data->grup =$grup ;
		$this->db->select('*');
		$this->db->from('kullanici_grup_verilen_roller');
		$this->db->where('grup_id',$gid);
		$grup_rol = $this->db->get()->result();
		$grup_rol_duzenli = array();
		foreach ($grup_rol as $key => $value) {
			$grup_rol_duzenli[$value->verilen_rol_id] = $value;
		}
		$sroller = array();
		foreach ($roller as $key => $value) {
			$sroller[$value->id] = $value;
			if(isset($grup_rol_duzenli[$value->id]))
			{
				@$sroller[$value->id]->grup = $grup_rol_duzenli[$value->id];
			}else{
				@$sroller[$value->id]->grup = (object)array('gor'=>0,'ekle'=>0,'duzenle'=>0,'sil'=>0);
			}
			if(isset($user_rol_duzenli[$value->id]))
			{
				@$sroller[$value->id]->user = $user_rol_duzenli[$value->id];
			}else{
				@$sroller[$value->id]->user = (object)array('gor'=>0,'ekle'=>0,'duzenle'=>0,'sil'=>0);
			}
			@$sroller[$value->id]->sonuc->gor =  $sroller[$value->id]->user->gor + $sroller[$value->id]->grup->gor ;
			@$sroller[$value->id]->sonuc->ekle =  $sroller[$value->id]->user->ekle + $sroller[$value->id]->grup->ekle ;
			@$sroller[$value->id]->sonuc->duzenle =  $sroller[$value->id]->user->duzenle + $sroller[$value->id]->grup->duzenle ;
			@$sroller[$value->id]->sonuc->sil =  $sroller[$value->id]->user->sil + $sroller[$value->id]->grup->sil ;
		}
		@$data->roller =$sroller ;
		return $data;

	}


	public function rolkontrol($user,$path,$savelog = true)
	{  
		$path = "%$path%";
		$this->db->select("id,rol_adi, concat(if(gor like '$path','gor','') ,if(ekle like '$path',',ekle','') ,if(duzenle like '$path',',duzenle','') ,if(sil like '$path',',sil',''))  as izinler",false);
		$this->db->from('kullanici_roller');
		$this->db->or_where('gor like',$path);
		$this->db->or_where('ekle like',$path);
		$this->db->or_where('duzenle like',$path);
		$this->db->or_where('sil like',$path); 
		$roller = $this->db->get()->result();
		$sroller = $this->roles('user',$user)->roller;

		if(count($roller) > 0){
			foreach ($roller as  $rol) {
				$izinler = explode(',', trim( $rol->izinler,','));
				foreach ($izinler as $izin) { 
					$izin = trim($izin);
					if($sroller[$rol->id]->sonuc->{$izin} > 0){
						if($savelog){	
							$data['kullanici'] =$user;
							$data['ip'] =$_SERVER['REMOTE_ADDR'];
							$data['tarih'] =time();
							$data['modul_method'] =trim($this->uri->uri_string(),"%");
							$data['data'] = json_encode($this->input->post()) .json_encode($this->input->get()) ;
							$data['aciklama'] = $rol->rol_adi . " " . $izin;
							$this->db->db_debug = false;
							$this->db->insert("kullanici_islem_kayitlari",$data);
						}
						return TRUE;
					}
				}
			}
			if($savelog){
				$data['kullanici'] =$user;
				$data['ip'] =$_SERVER['REMOTE_ADDR'];
				$data['tarih'] =time();
				$data['modul_method'] =$path;
				$data['aciklama'] = "Yetkisiz Giriş" . $rol->rol_adi . " " . $izin;
				$this->db->db_debug = false;
				$this->db->insert("kullanici_islem_kayitlari",$data);
			}

			return FALSE;
		}else{
         	return TRUE;//işaretli rol yoksa izin ver
         }    


     }	

     public function edit($type,$id,$rolid,$rol,$deger){

     	if($type == 'grup')
     	{
     		$this->db->select('id');
     		$this->db->from('kullanici_grup_verilen_roller');
     		$this->db->where('grup_id',$id);
     		$this->db->where('verilen_rol_id',$rolid);
     		$grup_rol = $this->db->get()->row();
     		if(isset($grup_rol->id)){
     			$this->db->where('id', $grup_rol->id);
     			$data[$rol]= $deger;
     			$this->db->db_debug = false;
     			return $this->db->update("kullanici_grup_verilen_roller",$data);
     		}
     		else{
     			$data[$rol]= $deger;
     			$data['grup_id']= $id;
     			$data['verilen_rol_id']= $rolid;
     			$this->db->db_debug = false;
     			return $this->db->insert("kullanici_grup_verilen_roller",$data);
     		}

     	}else{
     		$this->db->select('id');
     		$this->db->from('kullanici_verilen_roller');
     		$this->db->where('kullanici_id',$id);
     		$this->db->where('verilen_rol_id',$rolid);
     		$grup_rol = $this->db->get()->row();
     		if(isset($grup_rol->id)){
     			$this->db->where('id', $grup_rol->id);
     			$data[$rol]= $deger;
     			$this->db->db_debug = false;
     			return $this->db->update("kullanici_verilen_roller",$data);
     		}
     		else{
     			$data[$rol]= $deger;
     			$data['kullanici_id']= $id;
     			$data['verilen_rol_id']= $rolid;
     			$this->db->db_debug = false;
     			return $this->db->insert("kullanici_verilen_roller",$data);
     		}

     	}

     }




 }

 ?>
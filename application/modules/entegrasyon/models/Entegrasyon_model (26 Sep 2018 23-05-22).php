<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entegrasyon_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
		
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		
	}

	public function kontrol($data = NULL)
	{
		if ($data && isset($data['stok'])) {
			$this->db->select('*');
			$this->db->from('_entegrasyon');
			$this->db->where('stok', $data['stok']);
			$res = $this->db->get()->row();
			if ($res) {
				if ( ($res->miktar <> $data['miktar']) || ($res->fiyat <> $data['fiyat']) ) {
					echo $data['stok']." - miktar - fiyat güncellendi.<br />";
					if($data['fiyat'] == 0) {
                        $data['e_fiyat'] = 0;
                        $data['fiyat'] = 0;
                        $data['e_miktar'] = 0;
                        $data['miktar'] = 0;
                    } else {
                        $data['e_fiyat'] 	= $res->fiyat;
                        $data['e_miktar'] 	= $res->miktar;
                    }
					return $this->guncelle($data);
				} elseif( ($res->marka == "" and $data["marka"] != "") or ($res->dot != $data["dot"] && $data["dot"] != "0" ) ) {
					echo $data['stok']." - güncelleme yok.<br />";
					$this->guncelle($data, FALSE);
				} else {
					echo $data['stok']." - güncelleme yok.<br />";
					// return $this->guncelle(array('stok' => $data['stok']));
				}

			} else {
				echo $data['stok']." - eklendi.<br />";
				return $this->ekle($data);
			}
		} else {
			return FALSE;
		}
	}

	public function guncellenecekler()
	{
		$this->db->select('*');
		$this->db->from('_entegrasyon');
		$this->db->limit('100');
		$this->db->order_by('sistem', 'asc');
		$this->db->order_by('zaman', 'desc');
		// $this->db->where('stok', 'BKT-YD-FG431642488001022');
		$res = $this->db->get()->result();
		return $res;
	}

	public function guncellenenler($sdata = NULL)
	{
		$this->db->select('*');
		$this->db->from('_entegrasyon');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->like($key, $value);
			}
		} else {
			$this->db->limit('100');
		}
		$this->db->order_by('sistem', 'desc');
		$res = $this->db->get()->result();
		return $res;
	}

	public function urunguncelle($stok = NULL, $data = NULL)
	{
		$updateData = array('sistem' => time());

		/*
		if($data["fiyat"] == "0"){
			$listefiyati = $this->db->query("select liste_fiyati from urunler where xml_stok_kodu = '".$stok."'")->row();
			$data["fiyat"] = $listefiyati->liste_fiyati;
		}
		*/

		if($data["fiyat"] == "0"){
			$updateData['fiyat'] = 0;
			$updateData['e_fiyat'] = 0;
			$updateData['miktar'] = 0;
			$updateData['e_miktar'] = 0;
		}

		/*Entegrasyon Güncelle*/
		$this->db->where('stok', $stok);
		$this->db->update('_entegrasyon', $updateData);
		/*Entegrasyon Güncelle*/

		/*Sistem Güncelle*/
		$data['apizaman'] = time();
		$this->db->where('xml_stok_kodu', $stok);
		$this->db->update('urunler', $data);
		/*Sistem Güncelle*/
	}

	/**/
	private function guncelle($data = NULL, $zaman = TRUE)
	{
		if($zaman == TRUE){
			$data['zaman'] = time();
		}

		$this->db->where('stok', $data['stok']);
		$res = $this->db->update('_entegrasyon', $data);
		return $res;
	}

	private function ekle($data = NULL)
	{
		$data['zaman'] = time();
		$res = $this->db->insert('_entegrasyon', $data);
		return $res;
	}
	/**/
}

?>

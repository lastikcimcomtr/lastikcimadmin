<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entegrasyon_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
		
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		
	}

	public function kontrol($data = NULL)
	{
		if ($data && isset($data['stok'])) {
			$this->db->select('*');
			$this->db->from('_entegrasyon');
			$this->db->where('stok', $data['stok']);
			$res = $this->db->get()->row();
			if ($res) {
				if ( ($res->miktar <> $data['miktar']) || ($res->fiyat <> $data['fiyat']) ) {
					echo $data['stok']." - miktar - fiyat güncellendi.<br />";

					if($data['fiyat'] == 0 || $data['miktar'] == 0) {
                        $data['e_fiyat'] = 0;
                        $data['fiyat'] = 0;
                        $data['e_miktar'] = 0;
                        $data['miktar'] = 0;
                    } else {
                        $data['e_fiyat'] 	= $res->fiyat;
                        $data['e_miktar'] 	= $res->miktar;
                    }

					return $this->guncelle($data);
				} elseif( ($res->marka == "" and $data["marka"] != "") or ($res->dot != $data["dot"] && $data["dot"] != "0" ) ) {
					echo $data['stok']." - güncelleme yok.<br />";
					$this->guncelle($data, FALSE);
				} else {
					echo $data['stok']." - güncelleme yok.<br />";
					// return $this->guncelle(array('stok' => $data['stok']));
				}

			} else {
				echo $data['stok']." - eklendi.<br />";
				return $this->ekle($data);
			}
		} else {
			return FALSE;
		}
	}
	public function guncellenecekler()
	{
		$this->db->select('*');
		$this->db->from('_entegrasyon');
		$this->db->limit('150');
		$this->db->order_by('sistem', 'asc');
		$this->db->order_by('zaman', 'desc');
		// $this->db->where('stok', 'BKT-YD-FG431642488001022');
		$res = $this->db->get()->result();
		return $res;
	}

	public function guncellenenler($sdata = NULL)
	{
		$this->db->select('*');
		$this->db->from('_entegrasyon');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->like($key, $value);
			}
		} else {
			$this->db->limit('100');
		}
		$this->db->order_by('sistem', 'desc');
		$res = $this->db->get()->result();
		return $res;
	}

	public function urunguncelle($stok = NULL, $data = NULL, $marka = NULL)
	{
		if($data["stok_miktari"] > 0 && $data["fiyat"] == 0) {
            $data["stok_miktari"] = 0;
            $data["fiyat"] = 0;
		}
		if($data['uretim_tarihi'] == 0){
			unset($data['uretim_tarihi']);
		}
		$tedarikci = 'TATKO';
		if($marka){
			if(in_array($marka, ['Bridgestone','Lassa','Dayton'])){
				$tedarikci = 'YUZYIL';
				unset($data['fiyat']);
				unset($data['urun_alis_fiyati']);
			}
			if(!in_array($marka, ['BFGoodrich','Debica','Kleber'])){
				if(!isset($data['uretim_tarihi'])){
					$data['uretim_tarihi'] = '2019/2020';
				}
			}
		}
		$data['tedarikci'] = $tedarikci;
		/*Entegrasyon Güncelle*/
		/*$this->db->where('stok', $stok);
		$this->db->update('_entegrasyon', array('sistem' => time()));*/
		/*Entegrasyon Güncelle*/
		
		/*FD KAPA */
		if(isset($data["fiyat"]) && $data["stok_miktari"] >= 4){
			$fd_kapat = $this->db->where('xml_stok_kodu',$stok)->where('fd_onoff = true')->where('fiyat >=',$data["fiyat"])->update('urunler',array('fd_onoff' => false));
		}
		/*FD KAPA*/
		/*Fiyat Güncellemeyecekler*/

		/*Sistem Güncelle*/
		$data['apizaman'] = time();
		$this->db->where('xml_stok_kodu', $stok);
		$this->db->where('fd_onoff = false');
		$this->db->update('urunler', $data);
		/*Sistem Güncelle*/
	}

	public function guncellenenleri_ayarla($stok_kodlar){
		$this->db->where_in('stok', $stok_kodlar);
		$this->db->update('_entegrasyon', array('sistem' => time()));
		$fd_kapat_2 = $this->db->where_in('xml_stok_kodu',$stok_kodlar)->where('fd_onoff = true')->where('stok_miktari < 4')->update('urunler',array('fd_onoff' => false));
	}


	/**/
	private function guncelle($data = NULL, $zaman = TRUE)
	{
		if($zaman == TRUE){
			$data['zaman'] = time();
		}
		$this->db->where('stok', $data['stok']);
		$res = $this->db->update('_entegrasyon', $data);
		return $res;
	}

	private function ekle($data = NULL)
	{
		$data['zaman'] = time();
		$res = $this->db->insert('_entegrasyon', $data);
		return $res;
	}
	/**/
}

?>
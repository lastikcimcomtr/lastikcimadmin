<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entegrasyon extends CI_Controller  {


	public $theme_url;
	public function __construct()
	{
		ini_set('memory_limit', '2G');

		ini_set('max_execution_time', 600);

		parent::__construct();
		$this->theme_url = $this->parser->theme_url();
		$this->load->model('Entegrasyon_model', 'entegrasyon');
	}

	public function index()
	{
		$this->load->library('entegrasyontatko');
		$stoklar = $this->entegrasyontatko->stoklar();
		foreach ($stoklar as $stok) {
			$stokDatasiArray = array(
				'stok' => $stok['stok'],
				'miktar' => $stok['miktar'],
				'fiyat' => $stok['fiyat'],
				'model' => $stok['model'],
				'marka' => $stok['marka'],
				'dot' => $stok['dot']
			);
			if($stok["model"] == "BİNEK" && ($stok["marka"] == "Taurus" or $stok["marka"] == "Riken")){
				echo $stok['stok']." - <b>Güncelleme Almıyor</b.<br />";
			}else{
				$this->entegrasyon->kontrol($stokDatasiArray);
			}
		}
	}

	public function stokguncelle()
	{
		$this->load->library('entegrasyontatko');
		$urunler = $this->entegrasyon->guncellenecekler();
		foreach ($urunler as $urun) {
			if($urun->dot == "0" or $urun->dot == ""){ $urun->dot = NULL; }
			$this->entegrasyon->urunguncelle(
				$urun->stok,
				array(
					'stok_miktari' => $this->entegrasyontatko->miktarhesapla($urun->miktar),
					'urun_alis_fiyati' => $urun->fiyat,
					'fiyat' => $this->entegrasyontatko->fiyathesapla($urun->fiyat, $urun->model, $urun->marka),
					'uretim_tarihi' => $urun->dot
				)
			);
		}
	}

	/*public function test()
	{
		$this->load->library('entegrasyontatko');
		$stoklar = $this->entegrasyontatko->stoklar();
		echo "<pre>";
		foreach ($stoklar as $stok) {
			if(strpos($stok["stok"], "-W-")){$stok["miktar"] = "0"; }
			$stokDatasiArray = array(
				'stok' => $stok['stok'],
				'miktar' => $stok['miktar'],
				'fiyat' => $stok['fiyat'],
				'model' => $stok['model'],
				'marka' => $stok['marka'],
				'dot' => $stok['dot']
			);
			print_r($stokDatasiArray);
			// if(strpos($stok["model"], "BİNEK") && (strpos($stok["marka"], "Taurus") or strpos($stok["marka"], "Riken"))){
			// 	echo $stok['stok']." - <b>Güncelleme Almıyor</b.<br />";
			// }else{
			// 	$this->entegrasyon->kontrol($stokDatasiArray);
			// }
		}
		echo "</pre>";
	}*/

}

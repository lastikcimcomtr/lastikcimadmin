<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entegrasyon extends CI_Controller  {


	public $theme_url;
	public function __construct()
	{
		ini_set('memory_limit', '4G');

		ini_set('max_execution_time', 600);

		parent::__construct();
		$this->theme_url = $this->parser->theme_url();
		$this->load->model('Entegrasyon_model', 'entegrasyon');
	}

	public function index()
	{
		$this->load->library('entegrasyontatko');
		$stoklar = $this->entegrasyontatko->stoklar();
		foreach ($stoklar as $stok) {
			$stokDatasiArray = array(
				'stok' => $stok['stok'],
				'miktar' => $stok['miktar'],
				'fiyat' => $stok['fiyat'],
				'model' => $stok['model'],
				'marka' => $stok['marka'],
				'dot' => $stok['dot'],
				'urun_ismi' => $stok['urun_ismi']
			);
			if(mb_strpos($stok["marka"], 'RKN-BN-')){
				echo $stok['stok']." - <b>Güncelleme Almıyor</b.<br />";
			}else{
				$this->entegrasyon->kontrol($stokDatasiArray);
			}
		}
		$this->index2();
	}


	public function index2()
	{
		$this->load->library('entegrasyonyuzyil');
		$stoklar = $this->entegrasyonyuzyil->stoklar();
		foreach ($stoklar as $stok) {
			$stokDatasiArray = array(
				'stok' => $stok['stok'],
				'miktar' => $stok['miktar'],
				'fiyat' => $stok['fiyat'],
				'model' => $stok['model'],
				'marka' => $stok['marka'],
				'dot' => $stok['dot']
			);
			if(mb_strpos($stok["marka"], 'RKN-BN-')){
				echo $stok['stok']." - <b>Güncelleme Almıyor</b.<br />";
			}else{
				$this->entegrasyon->kontrol($stokDatasiArray);
			}
		}
	}

	public function stokguncelle()
	{
		$this->load->library('entegrasyontatko');
		$urunler = $this->entegrasyon->guncellenecekler();
		$guncellenen_kodlar = [];
		foreach ($urunler as $urun) {
			if($urun->dot == "0" or $urun->dot == ""){ $urun->dot = NULL; }
			$guncellenen_kodlar[]=$urun->stok;
			$this->entegrasyon->urunguncelle(
				$urun->stok,
				array(
					'stok_miktari' => $this->entegrasyontatko->miktarhesapla($urun->miktar),
					'urun_alis_fiyati' => $urun->fiyat,
					'fiyat' => $this->entegrasyontatko->fiyathesapla($urun->fiyat, $urun->model, $urun->marka, $urun->stok),
					'uretim_tarihi' => $urun->dot
				),
				$urun->marka
			);
		}
		$this->entegrasyon->guncellenenleri_ayarla($guncellenen_kodlar);
	}

	/*public function test()
	{
		$this->load->library('entegrasyontatko');
		$stoklar = $this->entegrasyontatko->stoklar();
		echo "<pre>";
		foreach ($stoklar as $stok) {
			if(strpos($stok["stok"], "-W-")){$stok["miktar"] = "0"; }
			$stokDatasiArray = array(
				'stok' => $stok['stok'],
				'miktar' => $stok['miktar'],
				'fiyat' => $stok['fiyat'],
				'model' => $stok['model'],
				'marka' => $stok['marka'],
				'dot' => $stok['dot']
			);
			print_r($stokDatasiArray);
			// if(strpos($stok["model"], "BİNEK") && (strpos($stok["marka"], "Taurus") or strpos($stok["marka"], "Riken"))){
			// 	echo $stok['stok']." - <b>Güncelleme Almıyor</b.<br />";
			// }else{
			// 	$this->entegrasyon->kontrol($stokDatasiArray);
			// }
		}
		echo "</pre>";
	}*/

	public function toKvkk(){
		$tmax = 0;
		$user_last_consent_date = $this->db->select('max(consentDate) max_date')->from('kvkk_iletisim')->where('sub_source','USER')->get()->row();
		if($user_last_consent_date->max_date){
			$tmax = $user_last_consent_date->max_date;
		}

		$user_base = $this->db->select("mh.id,mh.email,md2.user_metavalue cep_telefonu,kayit_tarihi consentDate")->from('musteri_hesaplari mh')->from('musteri_datalar md')->where('md.user_id=mh.id')->where('md.user_meta','mail_listesi')->where('md.user_metavalue','1');
		$user_base = $user_base->from('musteri_datalar md2')->where('md2.user_id=mh.id')->where('mh.hesap_durumu','1')->where('md2.user_meta','cep_telefonu')->where('mh.kayit_tarihi >',$tmax);
		$user_base = $user_base->limit(1000000)->get()->result();
		$data = [];
		//DATE_FORMAT(FROM_UNIXTIME(kayit_tarihi), '%Y-%m-%d %H:%i:%s')
		$current_time = time();
		foreach ($user_base as $key => $user_single) {
			$user_unique = uniqid();
			$data[] = array(
	           'sub_source' => 'USER', 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'EPOSTA', 
	           'source' => 'HS_WEB', 
	           'recipient' => $user_single->email, 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
			$data[] = array(
	           'sub_source' => 'USER', 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'MESAJ', 
	           'source' => 'HS_WEB', 
	           'recipient' => $this->clearNumber($user_single->cep_telefonu), 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
			$data[] = array(
	           'sub_source' => 'USER', 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'ARAMA', 
	           'source' => 'HS_WEB', 
	           'recipient' => $this->clearNumber($user_single->cep_telefonu), 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
		}
		if(sizeof($data) > 0){
			$this->db->insert_batch('kvkk_iletisim', $data);
		}
		$tmax = 0;
		$iletisim_last_consent_date = $this->db->select('max(consentDate) max_date')->from('kvkk_iletisim')->where('sub_source','URUN_TALEP')->get()->row();
		if($iletisim_last_consent_date->max_date){
			$tmax = $iletisim_last_consent_date->max_date;
		}
		$user_base = $this->db->select("ii.id,ii.email,ii.cep_telefonu,ii.tarih consentDate,ii.kategori")->from('iletisim_istekleri ii')->where('ii.tarih >',$tmax);
		$user_base = $user_base->limit(1000000)->get()->result();
		$data = [];
		//DATE_FORMAT(FROM_UNIXTIME(kayit_tarihi), '%Y-%m-%d %H:%i:%s')

		$current_time = time();
		foreach ($user_base as $key => $user_single) {
			$user_unique = uniqid();
			if(strlen($user_single->kategori) > 0){
				$sub_source = $this->slugify($user_single->kategori);
				$sub_source = mb_strtoupper($sub_source);
			}else{
				$sub_source = 'ILETISIM';
			}
			$data[] = array(
	           'sub_source' => $sub_source, 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'EPOSTA', 
	           'source' => 'HS_WEB', 
	           'recipient' => $user_single->email, 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
			$data[] = array(
	           'sub_source' => $sub_source, 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'MESAJ', 
	           'source' => 'HS_WEB', 
	           'recipient' => $this->clearNumber($user_single->cep_telefonu), 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
			$data[] = array(
	           'sub_source' => $sub_source, 
	           'consentDate' => $user_single->consentDate, 
	           'type' => 'ARAMA', 
	           'source' => 'HS_WEB', 
	           'recipient' => $this->clearNumber($user_single->cep_telefonu), 
	           'status' => 'ONAY', 
	           'created_at' => $current_time,
	           'user_uniq' => $user_unique
           );
		}
		if(sizeof($data) > 0){
			$this->db->insert_batch('kvkk_iletisim', $data);
		}
	}

	public function syncIYS(){
		ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
		$user_base = $this->db->select("id,type,source,recipient,status,LEFT(FROM_UNIXTIME(consentDate),19) consentDate,'BIREYSEL' recipientType")->from('kvkk_iletisim ki')->where('ki.valid','1')->where('ki.send_date','0');
		$user_base = $user_base->limit(5)->get()->result();
		$ids = [];
		foreach($user_base as $object){
			$ids[] = $object->id;
			unset($object->{'id'});
		}
		$users = json_encode($user_base);
		//echo $users;die();
		$url = 'https://api.iys.org.tr/oauth/token';
		$data = array("username" => "yazilim@lastikcim.com.tr","password" => "387653" );

		$postdata = json_encode($data);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		$result = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);
		echo $result;die();
		$result = json_decode($result);
		
		$access_token = $result->result->accessToken;
		
		$url = 'https://api.iys.org.tr/sps/687304/brands/687304/consents/request';

		$postdata = $users;

		$token_parsed = "Authorization: Bearer ".$access_token;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($token_parsed, "Content-Type: application/json" ));
		$result = curl_exec($ch);
		echo curl_error($ch);
		curl_close($ch);
		$result = json_decode($result);
		if(isset($result->requestId)){
			$this->db->where_in('id',$ids)->update('kvkk_iletisim',['send_date' => time()]);
		}elseif(isset($result->errors)){
			foreach ($result->errors as $key => $error) {
				if($error->code == 'H194'){
					$this->db->where_in('id',$ids)->update('kvkk_iletisim',['send_date' => time()]);
				}
			}
		}
		die();
	}

	public function clearNumber($number)
    {
        $number = preg_replace("/[^0-9]/", "", $number);
        if(substr($number, 0,1) == '9' || substr($number, 0,1) == '0'){
        	return $this->clearNumber(substr($number,1,999));	
        }
        return '+90'.$number;
    }

	public static function slugify ($text) {

	    $replace = [
	        '&lt;' => '', '&gt;' => '', '&#039;' => '', '&amp;' => '',
	        '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä'=> 'Ae',
	        '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae',
	        'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
	        'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
	        'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
	        'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
	        'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
	        'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
	        'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
	        'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
	        'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
	        'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
	        'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
	        'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
	        '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
	        'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
	        'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
	        'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
	        'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
	        'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
	        'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
	        'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
	        'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
	        'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
	        'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
	        'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
	        'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
	        '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
	        'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
	        'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
	        'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
	        'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
	        'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
	        'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
	        'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
	        'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
	        'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
	        'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
	        'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
	        'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
	        'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
	        'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
	        'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
	        'ю' => 'yu', 'я' => 'ya'
	    ];

	    $text = strtr($text, $replace);

	    // replace non letter or digits by -
	    $text = preg_replace('~[^\\pL\d.]+~u', '-', $text);

	    // trim
	    $text = trim($text, '-');

	    // remove unwanted characters
	    $text = preg_replace('~[^-\w.]+~', '', $text);

	    $text = strtolower($text);

	    return $text;
	}
}

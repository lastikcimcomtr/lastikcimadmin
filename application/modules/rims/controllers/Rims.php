<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rims extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
    	ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		ini_set("memory_limit", -1);
		error_reporting(E_ALL);

        set_time_limit(0);
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Rims_model', 'rims');
    }

    public function markalar()
    {
    	$markalar = $this->rims->markalar();
    	if (count($markalar)) {
    		foreach ($markalar as $mkey => $mvalue) {
    			$this->rims->markaekle($mkey, $mvalue);
    		}
    	}
    }

    public function marka()
    {
    	$marka = $this->rims->markaal();
    	if ($marka) {

    		try {
    			
    			$yillar 	= $this->rims->yillar($marka->marka_kod);
			
				foreach ($yillar as $ykey => $yvalue) {			
					
					$modeller 		= $this->rims->modeller($marka->marka_kod, $ykey);

					foreach ($modeller as $mkey => $mvalue) {
						
						$olculer 		= $this->rims->olculer($marka->marka_kod, $ykey, $mkey);
						
						if (count($olculer) > 0) {
							foreach ($olculer as $olcu) {
									
								$res = $this->rims->olcuekle($olcu);
								var_dump($res);
								
							}
						}

					}

				}

    		} catch (Exception $e) {
				echo $e->getMessage();
			}

    	}
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rims_model extends CI_Model {

	function __construct() {
		parent::__construct();	
	}



	public function markaekle($kod = NULL, $adi = NULL)
	{
		if ($kod && $adi) {

			$this->db->select('*');
			$this->db->from('_jant_markalari');
			$this->db->where('marka_kod', $kod);
			$res = $this->db->get()->row();

			if ($res) {
				
				$this->db->where('id', $res->id);
				return $this->db->update('_jant_markalari', array('marka_kod' => $kod, 'marka_adi' => $adi));

			} else {

				return $this->db->insert('_jant_markalari', array('marka_kod' => $kod, 'marka_adi' => $adi));

			}

		} else {
			return FALSE;
		}
	}

	public function olculerden($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('_jant_olculeri');

		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	public function olcuekle($olcu = NULL)
	{
		if ($olcu) {

			$ovarmi = $this->olculerden($olcu, TRUE);
			if ($ovarmi) {
				return TRUE;
			} else {
				return $this->db->insert('_jant_olculeri', $olcu);
			}

		} else {
			return FALSE;
		}
	}

	public function markaal()
	{
		$this->db->select('*');
		$this->db->from('_jant_markalari');
		$this->db->order_by('marka_lastup', 'asc');
		$res = $this->db->get()->row();

		/**/
		$this->db->where('id', $res->id);
		$this->db->update('_jant_markalari', array('marka_lastup' => time()));
		/**/

		return $res;
	}
	
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function markatipleri()
	{
		$this->db->select('tip as adi');
		$this->db->from('markalar');
		$this->db->group_by('tip');
		$res = $this->db->get()->result();
		return $res;
	}

	public function markalar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('markalar');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		$this->db->order_by('sira', 'asc');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function desenler($sdata = NULL, $row = FALSE)
	{
		$this->db->select('d.*, kategori_agaci(d.kategori) as kategori_adlari, marka_agaci(d.marka) as marka_adlari');
		$this->db->from('desenler as d');

		if (isset($sdata['limit']) && is_numeric($sdata['limit'])) {
			$limit = $sdata['limit'];
			if(isset($sdata['sayfa']))
				$sayfa = $sdata['sayfa'];
			else
				$sayfa = 1;
			unset($sdata['limit']);
			unset($sdata['sayfa']);
			
			$this->db->limit($limit,($sayfa - 1) * $limit);
		}
		if (isset($sdata['order'])){
			$order = str_replace('-',' ', $sdata['order']);
			unset($sdata['order']);
			$this->db->order_by($order);
		}else{
			$this->db->order_by('sira');
		}



		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where("d.".$key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}


public function markagrupset($marka,$grup){
	if(is_numeric($marka)){
		$this->db->where('id', $marka);
		$this->db->update('markalar', array('fiyat_grup'=>$grup));
		return 1;
	}else{
		return 0;
	}
}


	public function editbrand($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$marka = $this->markalar(array('id' => $id), TRUE);
			if ($marka) {
				$this->db->where('id', $marka->id);
				$res = $this->db->update('markalar', $data);
				return $res;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function editpattern($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$desen = $this->desenler(array('id' => $id), TRUE);
			if ($desen) {
				$this->db->where('adi', $desen->adi);
				$this->db->where('marka', $desen->marka);
				$res = $this->db->update('desenler', $data);
				return $res;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function editpatternimage($id = NULL,$resim = "resim")
	{
		if ($id && is_numeric($id)) {
			$desen = $this->desenler(array('id' => $id), TRUE);
			if ($desen) {
				$data 						= array();
				if (strlen($_FILES['resim']['name']) > 4) {
					$resim_dizin 				= $this->extraservices->ayarlar(array('name' => 'desen_resim'), TRUE)->value;
					$upload_res 				= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'resim'));
					$data[$resim] 				= $upload_res->image;

					$this->db->where('adi', $desen->adi);
					$this->db->where('marka', $desen->marka);
					$res = $this->db->update('desenler', $data);
					return $res;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function editbrandimage($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$marka = $this->markalar(array('id' => $id), TRUE);
			if ($marka) {
				$data 						= array();
				if (strlen($_FILES['resim']['name']) > 4) {
					$resim_dizin 				= $this->extraservices->ayarlar(array('name' => 'marka_resim'), TRUE)->value;
					$upload_res 				= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'resim'));
					$data['resim'] 				= $upload_res->image;
					$this->db->where('id', $marka->id);
					$res = $this->db->update('markalar', $data);
					return $res;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function addbrand($data = NULL)
	{
		if (count($data) > 0) {
			$data['durum'] = 1;
			$res = $this->db->insert('markalar', $data);
			return $res; 
		} else {
			return FALSE;
		}
	}

	public function addpattern($data = NULL)
	{
		if (count($data) > 0) {
			$data['durum'] = 1;
			$res = $this->db->insert('desenler', $data);
			return $res; 
		} else {
			return FALSE;
		}
	}

	public function remove($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('markalar');
		return $res;
	}

	public function removepattern($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('desenler');
		return $res;
	}

}
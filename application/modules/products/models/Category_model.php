<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}
	

	public function kategoritipleri()
	{
		$this->db->select('tip as adi');
		$this->db->from('kategoriler');
		$this->db->group_by('tip');
		$res = $this->db->get()->result();
		return $res;
	}

	public function kategoriler($sdata = NULL, $row = FALSE)
	{
		$this->db->select('k1.*, count(distinct k.id) as altkatsayisi');
		$this->db->from('kategoriler as k1');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where('k1.'.$key, $value);
			}
		}
		$this->db->join('kategoriler as k', 'k.ust = k1.id', 'LEFT');
		$this->db->order_by('k1.sira', 'asc');
		$this->db->group_by('k1.id');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function editcategory($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$kategori = $this->kategoriler(array('id' => $id), TRUE);
			if ($kategori) {
				$this->db->where('id', $kategori->id);
				$res = $this->db->update('kategoriler', $data);
				return $res;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function kargofiyatlari($id){
		$this->db->select('* , kategori_agaci_ters(id) altlar');
		$this->db->from('kategoriler');
		$this->db->where('id', $id);		
		$res = $this->db->get()->row();
		switch ($res->tip) {
			case 'lastik':	
			foreach (json_decode($res->altlar) as $kategori) {
				$altkategoriler[] = $kategori->id;
			}
			$this->db->select('GROUP_CONCAT(v.id) as veriler,v.deger , kf.fiyat');
			$this->db->from('veriler v');
			$this->db->join('kargo_fiyatlari kf ', 'v.deger = kf.deger and kf.kategori = ' .$id, 'LEFT');	
			$this->db->where('tip', "jant_capi");	
			$this->db->where_in('ust', $altkategoriler);
			$this->db->group_by('v.deger');
			return $this->db->get()->result();
			break;
			case 'jant':

			$this->db->select('u.ebat as veriler,u.ebat as deger, kf.fiyat');	
			$this->db->from('urunler u');
			$this->db->join('kargo_fiyatlari kf ', 'u.ebat = kf.deger and kf.kategori = ' .$id, 'LEFT');
			$this->db->where('u.tip', "jant");	
			$this->db->group_by('u.ebat');
			return $this->db->get()->result();

			break;
			case 'mini-stepne':

			$this->db->select('ifnull(u.set,0) as veriler,ifnull(u.set,0) as deger, kf.fiyat');	
			$this->db->from('urunler u');
			$this->db->join('kargo_fiyatlari kf ', 'ifnull(u.set,0) = kf.deger and kf.kategori = ' .$id, 'LEFT' ,FALSE);
			$this->db->where('u.tip', "mini-stepne");	
			$this->db->group_by('ifnull(u.set,0)');
			return $this->db->get()->result();

			break;
			default:
			return FALSE;
			break;
		}
	}
	public function kargofiyatlariset($data){
		if(is_numeric($data['value'])){
			$this->db->select('*');
			$this->db->from('kategoriler');
			$this->db->where('id', $data['id']);	
			$res = $this->db->get()->row();

			$this->db->select('*');
			$this->db->where('kategori',$data['id']);
			$this->db->where('deger',$data['name']);
			$this->db->from('kargo_fiyatlari');
			$q = $this->db->get()->row();
			if ( $q->id ) 
			{
				$this->db->where('id',$q->id);
				$this->db->update('kargo_fiyatlari',array('fiyat' => $data['value']) );
			}else{
				$ekle['kategori'] = $data['id'];
				$ekle['deger'] = $data['name'];
				$ekle['fiyat'] = $data['value'];
				$this->db->insert('kargo_fiyatlari',$ekle);
			}
			switch ($res->tip) {
				case 'lastik':
				$this->db->where('tip',$res->tip);
				$this->db->where_in('jant_capi',explode(",", $data['pk']));
				$this->db->update('urunler',array('kargo_bedeli' => $data['value'],'kargo_bedeli_birim' => "TRY"));
				return TRUE;
				break;
				case 'jant':
				$this->db->where('tip',$res->tip);
				$this->db->where('ebat',$data['pk']);
				$this->db->update('urunler',array('kargo_bedeli' => $data['value'],'kargo_bedeli_birim' => "TRY"));
				return TRUE;
			break;

			case 'mini-stepne':
				$this->db->where('tip',$res->tip);
				$this->db->where('ifnull(`set`,0) ',$data['pk'],FALSE);
				$this->db->update('urunler',array('kargo_bedeli' => $data['value'],'kargo_bedeli_birim' => "TRY"));
				break;
				default:
				return FALSE;
				break;
			}
		}else{
			return FALSE;
		}

	}


	public function editcategoryimage($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$kategori = $this->kategoriler(array('id' => $id), TRUE);
			if ($kategori) {
				$data 						= array();
				if (strlen($_FILES['resim']['name']) > 4) {
					$resim_dizin 				= $this->extraservices->ayarlar(array('name' => 'kategori_resim'), TRUE)->value;
					$upload_res 				= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'resim'));
					$data['resim'] 				= $upload_res->image;
					$this->db->where('id', $kategori->id);
					$res = $this->db->update('kategoriler', $data);
					return $res;
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	public function addcategory($data = NULL)
	{
		if (count($data) > 0) {
			$data['durum'] = 1;
			$res = $this->db->insert('kategoriler', $data);
			return $res; 
		} else {
			return FALSE;
		}
	}

	public function remove($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('kategoriler');
		return $res;
	}


	public function parametreler(){
		$this->db->select('pg.id grup_id,  pg.grup_adi, pg.sahip tip, p.id, p.adi, p.categories');
		$this->db->from('parametre_gruplari pg ');
		$this->db->join(' parametreler p ', 'p.grup = pg.id');
		$this->db->order_by('pg.sira ', 'asc');
		$this->db->order_by('p.sira ', 'asc');
		$res = $this->db->get()->result();
		$cikti = array();
		foreach ($res as $key => $value) {
			$cikti[$value->tip][$value->grup_adi][]=$value;
		}
		return $cikti;
	}
	public function editparams($id,$data){
		$this->db->query("update parametreler set categories = REPLACE(categories, '|$id|', '')" );
		foreach ($data['parametre'] as $value) {

			$this->db->query("update parametreler set categories = concat(categories, '|$id|') where id=$value");
		}

		return "ok";
	}
}

?>
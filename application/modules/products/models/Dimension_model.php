<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dimension_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function save($data){
		if(!isset($data['usttip']))
		{
			$data['usttip'] = "kategoriler";
		}
		if(isset($data['tip'])&&isset($data['deger'])&&isset($data['ust'])){
			$this->db->db_debug = false;
			return $this->db->insert('veriler', $data);
		}
		else
		{
			return false;
		}

	}


	public function delete($data){
	
		if(isset($data['id'])){
			$this->db->db_debug = false;
			return $this->db->delete('veriler', $data);
		}
		else
		{
			return false;
		}

	}

}
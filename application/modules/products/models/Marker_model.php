<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marker_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}



	public function etiketler($sdata = NULL, $row = FALSE)
	{
		$arama_data = false;
		if(isset($sdata['ara'])){
			$arama_data = $sdata['ara'];
			unset($sdata['ara']);
		}
		$this->db->select('e.* , count(distinct e.id) sayfa');
		$this->db->from('etiketler as e');
		if($arama_data){
			$arama_data = '%'.$arama_data.'%';
			$this->db->group_start();
			$this->db->or_where('adi like ',$arama_data);
			$this->db->or_where('sef_url like ',$arama_data);
			$this->db->group_end();
		}
		if(!$row ){
			$this->db->select('s.adet ');
			$this->db->join('etiket_gosterim s','s.url = e.sef_url' ,'left');
		}


		if($sdata['group_by']){
			$this->db->group_by('e.' . $sdata['group_by']);
			unset($sdata['group_by']);
		}		
		if($sdata['order_by']){
			$this->db->order_by($sdata['order_by']);
			unset($sdata['order_by']);
		}elseif(!$row ){
			$this->db->order_by('s.adet desc');
		}

		if (isset($sdata['limit']) && is_numeric($sdata['limit'])) {
			$limit = $sdata['limit'];
			if(isset($sdata['sayfa']))
				$sayfa = $sdata['sayfa'];
			else
				$sayfa = 1;
			unset($sdata['limit']);
			unset($sdata['sayfa']);
			
			$this->db->limit($limit,($sayfa - 1) * $limit);
		}
		unset($sdata['sayfa']);
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where( 'e.' . $key . ' like', '%'. trim($value) . '%');
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}



	public function edit($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$etiket = $this->etiketler(array('id' => $id), TRUE);
			if ($etiket) {
				$this->db->where('id', $etiket->id);
				$res = $this->db->update('etiketler', $data);
				return $res;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}



	public function add($data = NULL)
	{
		if (count($data) > 0) {
			$res = $this->db->insert('etiketler', $data);
			return $res; 
		} else {
			return FALSE;
		}
	}


	public function remove($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('etiketler');
		return $res;
	}



}
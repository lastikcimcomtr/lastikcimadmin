<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	public function yorumlar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('y.*');
		$this->db->from('yorumlar as y');

		$this->db->join('urunler as u', 'u.id = y.urun', 'LEFT');
		$this->db->select('u.urun_adi');

		$this->db->join('musteri_datalar as md', 'md.user_id = y.musteri', 'LEFT');
		$this->db->select('MAX(CASE WHEN md.user_meta = "adi" THEN md.user_metavalue END) as musteri_isim', FALSE);
		$this->db->select('MAX(CASE WHEN md.user_meta = "soyadi" THEN md.user_metavalue END) as musteri_soyisim', FALSE);
		$this->db->select('MAX(CASE WHEN md.user_meta = "sehir" THEN md.user_metavalue END) as musteri_sehir', FALSE);
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where('y.'.$key, $value);
			}
		}
		
		$this->db->group_by('y.id');
		$this->db->order_by('tarih','desc');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function edit($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			$this->db->where('id', $id);
			$res = $this->db->update('yorumlar', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function remove($id = NULL)
	{
		if ($id && is_numeric($id)) {
			$this->db->where('id', $id);
			$res = $this->db->delete('yorumlar');
			return $res;
		} else {
			return FALSE;
		}
	}
	public function system_answer($answer_to = NULL, $pdata = NULL){
		if($pdata['cevap']){
			$new_answer = [];
			$new_answer['musteri'] = NULL;
			$new_answer['baslik'] = '';
			$new_answer['aciklama'] = $pdata['cevap'];
			$new_answer['isim'] = 'LASTİKÇİM';
			$new_answer['soyisim'] = NULL; 
			$new_answer['evet'] = 1;
			$new_answer['hayir'] = 0;
			$new_answer['durum'] = 1;
			$new_answer['urun'] = $answer_to->urun;
			$new_answer['answer_to'] =  $answer_to->id;
			$res = $this->db->insert('yorumlar', $new_answer);
			return $res;
		}
		return false;
	}
	/*
	public function system_answer($answer_to = NULL,$pdata = NULL){
		print_r($answer_to);
		die();
		
		if($pdata['cevap']){
			$new_answer = [];
			$new_answer['musteri'] = NULL;
			$new_answer['baslik'] = '';
			$new_answer['aciklama'] = ;
			$new_answer['isim'] = 'LASTİKÇİM';
			$new_answer['soyisim'] = NULL; 
			$new_answer['evet'] = 1;
			$new_answer['hayir'] = 0;
			$new_answer['durum'] = 1;
			$new_answer['urun'] = $answer_to->urun;
			$new_answer['answer_to'] =  $answer_to->id;
			$res = $this->db->insert('yorumlar', $new_answer);
			return $res;
		}
		return false;
	}*/
	
}

?>
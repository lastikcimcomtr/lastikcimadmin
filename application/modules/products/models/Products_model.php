<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function veriler($sdata = NULL, $row = FALSE)
    {
        $this->db->select('*');
        $this->db->from('veriler');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $this->db->order_by('sira', 'asc');
        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }

        return $res;
    }

    public function siralamaparam()
    {
        $this->db->select('silik_id,adi,siralama_veri');
        $this->db->from('parametreler');
        $this->db->where('siralama', 1);
        return $this->db->get()->result();
    }


    public function anasayfasiralama()
    {
        $this->db->select('siralama_tip');
        $this->db->from('siralama');
        $this->db->where('silik_id', 'urun_idler');
        $this->db->where('tip', 'anasayfa');
        $idler = $this->db->get()->row()->siralama_tip;
        $idler = explode(",", $idler);
        foreach ($idler as $urunid) {
            $urunid = trim($urunid);
            if (is_numeric($urunid)) {
                $order[] = "if(id = {$urunid},1,0) desc";
            }
        }
        $order = implode(" , ", $order);


        $this->db->select('* ,kategori_agaci(kategori) as kategoriler');
        $this->db->from('urunler');
        $this->db->where("anasayfa_vitrini", 1);
        $this->db->order_by($order);
        $res = $this->db->get()->result();
        $cikti = array();
        foreach ($res as $key => $value) {
            $kategoriler = json_decode($value->kategoriler);
            if ($value->tip == "lastik") {
                $cikti[$value->tip][$kategoriler[0]->id][] = $value;
            } else {
                $cikti[$value->tip][] = $value;
            }
        }
        return (object)$cikti;


    }

    public function anasayfasiralamadegis($islem, $gelenurun)
    {

        $urunler = $this->anasayfasiralama();
        $sira = array();
        $i = 0;

        foreach ($urunler->lastik as $tip) {
            foreach ($tip as $urun) {
                $sira[$i] = $urun->id;
                if ($urun->id == $gelenurun) {
                    $urunidsi = $i;
                }
                ++$i;
            }
        }
        if ($islem == 'up') {
            $sira[$urunidsi] = $sira[$urunidsi - 1];
            $sira[$urunidsi - 1] = $gelenurun;
        } else {
            $sira[$urunidsi] = $sira[$urunidsi + 1];
            $sira[$urunidsi + 1] = $gelenurun;
        }

        $siralama_tip = implode(',', $sira);
        $this->db->where('silik_id', 'urun_idler');
        $this->db->where('tip', 'anasayfa');
        $this->db->update('siralama', array('siralama_tip' => $siralama_tip));
    }


    public function siralama()
    {
        $this->db->select('*');
        $this->db->from('siralama');
        $this->db->order_by('sira');
        return $this->db->get()->result();
    }

    public function siralamaedit($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('siralama', array($data['pk'] => $data['value']));
        return "OK";

    }

    public function siralamaadd($data)
    {

        if ($data['silik_id'] == "m.fiyat_grup")
            $data['siralama_tip'] = "deger";
        $this->db->insert('siralama', $data);
    }

    public function spsil($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('siralama');
    }

    public function mevsimler()
    {
        $data = array();
        $data[] = (object)array('adi' => 'Yaz', 'id' => 'yaz');
        $data[] = (object)array('adi' => 'Kış', 'id' => 'kis');
        $data[] = (object)array('adi' => '4 Mevsim', 'id' => '4m');
        return $data;
    }


    public function lastiktitle($data)
    {

        $this->db->select('adi');
        $this->db->from('markalar');
        $this->db->where('id', $data['marka']);
        $marka = $this->db->get()->row()->adi;

        if ($data['desen']) {
            $this->db->select('adi');
            $this->db->from('desenler');
            $this->db->where('id', $data['desen']);
            $desen = $this->db->get()->row()->adi;
        }

        if ($data['taban']) {
            $this->db->select('deger');
            $this->db->from('veriler');
            $this->db->where('id', $data['taban']);
            $this->db->where('tip', 'taban');
            $taban = $this->db->get()->row()->deger;
        }

        if ($data['yanak']) {
            $this->db->select('deger');
            $this->db->from('veriler');
            $this->db->where('id', $data['yanak']);
            $this->db->where('tip', 'yanak');
            $yanak = $this->db->get()->row()->deger;
        }
        if ($data['jant_capi']) {
            $this->db->select('deger');
            $this->db->from('veriler');
            $this->db->where('id', $data['jant_capi']);
            $this->db->where('tip', 'jant_capi');
            $jant_capi = $this->db->get()->row()->deger;
        }

        $rso = "";

        if ($data['grnx'])
            $rso .= $data['grnx'];

        if ($data['run_flat'])
            $rso .= $data['run_flat'];

        if ($data['seal'])
            $rso .= "SEAL";

        if ($data['oem'])
            $rso .= " " . $data['oem'];

        if ($yanak) {
            $yanak = "/{$yanak}";
        }

        if ($data['yapisi'] == "zr" || $data['yapisi'] == "Zr" || $data['yapisi'] == "ZR")
            $data['urun_adi'] = "{$marka} {$desen} {$rso} {$taban}{$yanak}ZR{$jant_capi}";
        else if ($data['yapisi'] == "vr" || $data['yapisi'] == "Vr" || $data['yapisi'] == "VR")
            $data['urun_adi'] = "{$marka} {$desen} {$rso} {$taban}{$yanak}VR{$jant_capi}";
        else if ($data['yapisi'] == "vb" || $data['yapisi'] == "Vb" || $data['yapisi'] == "VB")
            $data['urun_adi'] = "{$marka} {$desen} {$rso} {$taban}{$yanak}VB{$jant_capi}";
        else if ($data['yapisi'] == "V" || $data['yapisi'] == "v")
            $data['urun_adi'] = "{$marka} {$desen} {$rso} {$taban}{$yanak}V{$jant_capi}";
        else
            $data['urun_adi'] = "{$marka} {$desen} {$rso} {$taban}{$yanak}R{$jant_capi}";

        if ($data['kategori'] == 3) {
            $data['urun_adi'] = rtrim($data['urun_adi'], "C");
            $data['urun_adi'] = rtrim($data['urun_adi'], "c");
            $data['urun_adi'] .= "C";
        }


        $data['urun_adi'] .= " ";

        if ($data['yuk_endeksi'])
            $data['urun_adi'] .= $data['yuk_endeksi'];
        if ($data['hiz_endeksi'])
            $data['urun_adi'] .= $data['hiz_endeksi'];

        if ($data['xl_rf'])
            $data['urun_adi'] .= " " . $data['xl_rf'];

        $data['urun_adi'] = str_replace("  ", " ", $data['urun_adi']);
        $data['urun_adi'] = str_replace("  ", " ", $data['urun_adi']);

        /*if($data['mevsim'] == "kis")
        $data['urun_adi'] .= " Kış Lastiği" ;*/

        if (($data['kategori'] == 52 || ($data['kategori'] >= 54 && $data['kategori'] <= 62))) {
            if ($data['lastik_kati']) {
                $lastik_kati = " " . preg_replace("/[^0-9,.]/", "", $data['lastik_kati']) . "PR";
            } else {
                $lastik_kati = "";
            }
            if ($data['ebat']) {
                $data['urun_adi'] = "{$marka} {$desen} {$data['ebat']}" . $lastik_kati;
            } else if ($data['lastik_kati']) {
                $data['urun_adi'] .= $lastik_kati;
            }
        }
        if (($data['kategori'] <> 100 && $data['kategori'] <> 101 && $data['kategori'] <> 30 && $data['kategori'] <> 31 && $data['kategori'] <> 32 && $data['kategori'] <> 1 && $data['kategori'] <> 2 && $data['kategori'] <> 3) && ($data['yapisi'] == "TT" || $data['yapisi'] == "TL")) {
            $data['urun_adi'] .= " " . $data['yapisi'];
        }

        if (($data['kategori'] == 30 || $data['kategori'] == 31 || $data['kategori'] == 32) && isset($data['extra']) && $data['extra'] <> "") {
            $data['urun_adi'] .= " " . $data['extra'];
        }
        if ($data['kategori'] == 102) {
            $data['urun_adi'] = "{$marka} " . $data['model'] . " " . $data['ebat'] . " " . $data['pcd'] . " " . $data['et'] . " " . $data['renk'];
        }

        $data['urun_adi'] = str_replace("  ", " ", $data['urun_adi']);
        $data['urun_adi'] = str_replace("  ", " ", $data['urun_adi']);

        $data['seo_baslik'] = $data['urun_adi'];
        $data['url_duzenle'] = $this->checksefurl($data['urun_adi'], $data['id']);
        return $data;
    }

    public function tumurunsefle($page = 1)
    {
        $this->db->select("*");
        $this->db->from('urunler');
        $this->db->where('tip', 'lastik');
        $this->db->limit(1500, ($page - 1) * 1500);
        $res = $this->db->get()->result();
        foreach ($res as $urun) {
            $urun = (array)$urun;
            $urun = $this->lastiktitle($urun);
            $this->db->where('id', $urun['id']);
            $this->db->update('urunler', $urun);

            echo $urun['id'] . " - " . $urun['seo_baslik'] . " - " . $urun['desen'] . " <br>\n";
        }
    }

    public function magicsefurl($stok_kodu = 'MCH-BN'){
        $this->db->select("*");
        $this->db->from('urunler');
        $this->db->where('tip', 'yag');
        $res = $this->db->get()->result();
        foreach ($res as $urun) {
            $urun = (array)$urun;
            $urun = $this->lastiktitle($urun);
            $this->db->where('id', $urun['id']);
            $this->db->update('urunler', $urun);
        }
    }

    public function checksefurl($text, $id = 0)
    {
        $text = $this->texttourl($text);
        $this->db->select("url_duzenle");
        $this->db->from('urunler');
        if ($id) {
            $this->db->where("id <>", $id);
        }
        $this->db->group_start();
        $this->db->or_where("url_duzenle REGEXP ", $text . "/[0-9]");
        $this->db->or_where("url_duzenle REGEXP ", $text . "-[0-9]");
        $this->db->or_where("url_duzenle", $text);
        $this->db->group_end();
        $res = $this->db->get()->result();
        //print_r($res);
        if ($res) {
            $olanlar = array();
            foreach ($res as $key => $value) {
                $olanlar[] = $value->url_duzenle;
            }
            $i = 1;
            do {
                if ($i > 1)
                    $cikti = $text . "-$i";
                else
                    $cikti = $text . "-1";
                ++$i;
            } while (in_array($cikti, $olanlar));
            return $cikti;
        } else {
            return $text;
        }

    }

    public function texttourl($text)
    {
        $search = array('Ç', 'ç', 'Ğ', 'ğ', 'ı', 'İ', 'Ö', 'ö', 'Ş', 'ş', 'Ü', 'ü');
        $replace = array('c', 'c', 'g', 'g', 'i', 'i', 'o', 'o', 's', 's', 'u', 'u');
        $text = str_replace($search, $replace, $text);
        $text = str_replace("/", '-', $text);
        $text = str_replace("+", '-plus-', $text);
        $text = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9]+/', ' ', $text)));
        $text = preg_replace('/\s\s+/', ' ', $text);
        $text = trim($text);
        $text = str_replace(" ", "-", $text);
        return $text;
    }

    public function parametregruplari($sdata = NULL, $row = FALSE)
    {
        $this->db->select('*');
        $this->db->from('parametre_gruplari');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $this->db->order_by('sira', 'asc');

        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }

        return $res;
    }

    public function parametreler($sdata = NULL, $row = FALSE)
    {
        $this->db->select('*');
        $this->db->from('parametreler');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $this->db->order_by('sira', 'asc');

        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }

        return $res;
    }

    public function dovizkurlari($sdata = NULL, $row = FALSE)
    {
        $this->db->select('*');
        $this->db->from('doviz_kurlari_kullanilan');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        $this->db->order_by('id', 'asc');

        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }

        return $res;
    }

    /**/
    private function parametrelerim($sdata = NULL)
    {
        $this->db->select('GROUP_CONCAT(p.id) as id, p.tablo_adi, p.silik_id, p.extralar, pg.sahip');
        $this->db->from('parametreler as p');
        $this->db->join('parametre_gruplari as pg', 'pg.id = p.grup');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {
                $this->db->where('p.' . $key, $value);
            }
        }
        // $this->db->where('p.listeleme', 1);
        $this->db->group_by('p.silik_id');
        $res = $this->db->get()->result();
        return $res;
    }

    public function urunler($sdata = NULL, $row = NULL)
    {
        $this->db->select('u.*, kategori_agaci(u.kategori) as kategoriler, marka_agaci(u.marka) as markalar');
        $this->db->from('urunler as u');

        $this->db->join('veriler as vt', 'vt.tip = "taban" and vt.id = u.taban', 'LEFT');
        $this->db->select('vt.deger as taban_deger');

        $this->db->join('veriler as vy', 'vy.tip = "yanak" and vy.id = u.yanak', 'LEFT');
        $this->db->select('vy.deger as yanak_deger');

        $this->db->join('veriler as vjc', 'vjc.tip = "jant_capi" and vjc.id = u.jant_capi', 'LEFT');
        $this->db->select('vjc.deger as jant_capi_deger');
        if (isset($sdata['limit']) && is_numeric($sdata['limit'])) {
            $limit = $sdata['limit'];
            if (isset($sdata['sayfa']))
                $sayfa = $sdata['sayfa'];
            else
                $sayfa = 1;
            unset($sdata['limit']);
            unset($sdata['sayfa']);

            $this->db->limit($limit, ($sayfa - 1) * $limit);
        }

        if (isset($sdata['order'])) {
            $order = str_replace('-', ' ', $sdata['order']);
            unset($sdata['order']);
            $this->db->order_by($order);
        } else {
            $this->db->order_by('stok_kodu');
        }

        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {

                if (strlen($value) > 0) {
                    if ($key == "id") {

                        $this->db->where("u." . $key, $value);

                    } else {

                        if (is_string($value) || $key == "stok_kodu") {
                            $this->db->where("u." . $key . " like '%" . $value . "%'");
                        } else {
                            $this->db->where("u." . $key, $value);
                        }

                    }

                }

            }
        }
        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }
        return $res;
    }

    public function urunsayilar($sdata = null)
    {

		$time_start = microtime(true); 
        if (count($sdata) > 0) {
            $sdata = $this->aramaverisidegistir($sdata);
            foreach ($sdata as $key => $value) {
                if (strlen($value) > 0) {
                    if (!is_numeric($value)) {
                        $this->db->where("u." . $key . " like '%" . $value . "%'");
                    } else {
                        switch ($key) {
                            case 'etiket':
                                $this->db->where("( u.{$key} like  '%,{$value},%' OR u.{$key} like  '{$value},%' OR u.{$key} like  '%,{$value}' )");
                                break;
                            case 'run_flat':
                            case 'xl_rf':
                            case 'seal':
                            case 'm_s':
                                $this->db->where("( u.{$key} is not null or u.{$key}  <> '')");
                                break;
                            default:
                                $this->db->where("u." . $key, $value);
                                break;
                        }
                    }
                } elseif (is_array($value)) {
                    if ($key == 'fiyat') {
                        $this->db->where("u." . $key . " >=", $value[0]);
                        $this->db->where("u." . $key . " <=", $value[1]);
                    } else {
                        $this->db->where_in("u." . $key, $value);
                    }

                }

            }
        }
        $this->db->select('count(*) adet,max(u.fiyat) enfazla,min(u.fiyat) endusuk,u.marka,marka_agaci(u.marka) as markalar');
        $this->db->from('urunler as u');
        $this->db->group_by('u.marka');
        $res = $this->db->get()->result();
        $adet = 0;
        foreach ($res as $key => $value) {
            $adet += $value->adet;
            if (isset($enfazla)) {
                if ($enfazla < $value->enfazla) {
                    $enfazla = $value->enfazla;
                }
            } else {
                $enfazla = $value->enfazla;
            }
            if (isset($endusuk)) {
                if ($endusuk > $value->endusuk) {
                    $endusuk = $value->endusuk;
                }
            } else {
                $endusuk = $value->endusuk;
            }

            $res[$key]->markalar = json_decode($value->markalar);
        }
        $cikti = array('markalar' => $res, 'toplam' => $adet, 'enfazla' => $enfazla, 'endusuk' => $endusuk);
        return (object)$cikti;

    }

    public function aramaverisidegistir($sdata = null)
    {
        unset($sdata['limit']);
        unset($sdata['sayfa']);
        unset($sdata['order']);
        if (is_array($sdata) && count($sdata) > 0) {
            $this->db->select('*');
            $this->db->from('veriler');
            foreach ($sdata as $key => $value) {
                $this->db->or_where("tip = '$key' and deger = '$value'", null, false);
            }
            $res = $this->db->get()->result();
            if ($res) {
                foreach ($res as $veri) {
                    if (is_string($sdata[$veri->tip])) {
                        unset($sdata[$veri->tip]);
                    }
                    $sdata[$veri->tip][] = $veri->id;
                }
            }
            foreach ($sdata as $key => $value) {
                if ($key == 'kategori') {
                    $this->db->select('kategori_agaci_ters(' . $value . ') as cats');
                    $res = json_decode($this->db->get()->row()->cats);
                    unset($sdata['kategori']);
                    foreach ($res as $cat) {
                        $sdata['kategori'][] = $cat->id;
                    }
                }
            }

        }
        return $sdata;
    }


    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $urun = $this->urunler(array('id' => $id), TRUE);
            if ($urun) {
                $this->db->where('id', $id);
                $res = $this->db->delete('urunler');
                return $res;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function edit($id = NULL, $data = NULL)
    {
        if ($id && is_numeric($id) && count($data) > 0) {

            $urun = $this->urunler(array('id' => $id), TRUE);
            if ($urun) {

                $parametreler = $data['parametreler'];
                if ($parametreler && count($parametreler) > 0) {

                    $resim_sira = 1;
                    foreach ($parametreler as $key => $value) {

                        if (strpos($key, "resim") !== FALSE && strpos($key, "baslik") === FALSE && strpos($key, "aciklama") === FALSE) {

                            if (strlen($_FILES['resim_' . $key]['name']) > 4) {
                                $resim_dizin = $this->extraservices->ayarlar(array('name' => 'urun_resim'), TRUE)->value;
                                $upload_res = (object)($this->extraservices->single_file_upload('.' . $resim_dizin, 'resim_' . $key));
                                $parametreler[$key] = $upload_res->image;
                                $parametreler[$key . "_sira"] = $resim_sira;
                                $resim_sira++;
                            } else {
                                unset($parametreler[$key]);
                            }
                        }

                    }

                    $this->db->where('id', $urun->id);
                    $res = $this->db->update('urunler', $parametreler);

                    if ($res)
                        return "GUNCELLENDI";
                    else
                        return "GUNCELLENEMEDI";

                } else {
                    return "BOS";
                }

            } else {
                return "URUNYOK";
            }

        } else {
            return "BOS";
        }
    }

    public function add($tip = NULL, $pdata = NULL)
    {

        if ($tip) {
            $parametreler = $pdata['parametreler'];
            if ($parametreler && count($parametreler) > 0) {
                $urundata = array('tip' => $tip, 'tarih' => time());
                $urun_ekle = $this->db->insert('urunler', $urundata);

                if ($urun_ekle) {

                    $urun_id = $this->db->insert_id();
                    if ($urun_id && is_numeric($urun_id)) {

                        if (count($parametreler) > 0) {
                            $resim_sira = 1;
                            foreach ($parametreler as $key => $value) {

                                if (strpos($key, "resim") !== FALSE && strpos($key, "baslik") === FALSE && strpos($key, "aciklama") === FALSE) {
                                    $resim_dizin = $this->extraservices->ayarlar(array('name' => 'urun_resim'), TRUE)->value;
                                    $upload_res = (object)($this->extraservices->single_file_upload('.' . $resim_dizin, 'resim_' . $key));
                                    $parametreler[$key] = $upload_res->image;
                                    $parametreler[$key . "_sira"] = $resim_sira;
                                    $resim_sira++;
                                }

                            }
                            $parametreler['tip'] = $tip;
                            $parametreler = $this->kargofiyati($parametreler);
                            $this->db->where('id', $urun_id);
                            $res = $this->db->update('urunler', $parametreler);
                            if ($res)
                                return "EKLENDI";
                            else
                                return "EKLENEMEDI";
                        } else {
                            return "BOS";
                        }

                    } else {
                        return "URUNIDYOK";
                    }

                } else {
                    return "URUNEKLENEMEDI";
                }

            } else {
                return "BOS";
            }
        } else {
            return "BOS";
        }
    }


    public function kargofiyati($urun)
    {
        if (!isset($urun['kargo_bedeli']) || $urun['kargo_bedeli'] == '') {
            switch ($urun['tip']) {
                case 'lastik':
                    $this->db->select('deger');
                    $this->db->from('veriler');
                    $this->db->where('id', $urun['jant_capi']);
                    $jant_capi = $this->db->get()->row()->deger;

                    $this->db->where('kategori_agaci(' . $urun['kategori'] . ') like concat(\'%"\',kategori,\'"%\')', null, false);
                    $this->db->where('deger', $jant_capi);
                    break;

                case 'jant':
                    $this->db->where('kategori_agaci(' . $urun['kategori'] . ') like concat(\'%"\',kategori,\'"%\')', null, false);
                    $this->db->where('deger', $urun['ebat']);
                    break;

                case 'mini-stepne':
                    if (!isset($urun['set']) || $urun['set'] == "") {
                        $set = 0;
                    } else {
                        $set = 1;
                    }
                    $this->db->where('kategori_agaci(' . $urun['kategori'] . ') like concat(\'%"\',kategori,\'"%\')', null, false);
                    $this->db->where('deger', $set);
                    break;
            }
            $this->db->select('fiyat');
            $this->db->from('kargo_fiyatlari');
            $urun['kargo_bedeli'] = $this->db->get()->row()->fiyat;
            $urun['kargo_bedeli_birim'] = "TRY";
        }
        return $urun;
    }


    public function urunparametreguncelle($urun = NULL, $parametre = NULL, $value = NULL, $tip = NULL)
    {
        $this->db->where('id', $urun);
        $res = $this->db->update('urunler', array($parametre => $value));
        return $res;
    }

    /**/

    public function updatemultiproducts($data = NULL)
    {
        if (count($data) > 0) {
            $upres = NULL;
            // var_dump($data);
            $urunler = $data['urunler'];
            if (count($urunler) > 0) {

                foreach ($urunler as $key => $value) {
                    // $value = (object)$value;
                    if ($value['durum'] == 1) {
                        unset($value['durum']);
                        $urun_id = $key;
                        $this->db->where('id', $urun_id);
                        $upres = $this->db->update('urunler', $value);
                    }
                }
                // var_dump($upres);
                if ($upres === TRUE) {
                    return "Ürünler Başarılı Bir Şekilde Güncellenmiştir.";
                } elseif ($upres === FALSE) {
                    return "Ürünlerden Bazıları Güncellenirken Hata Meydana Geldi";
                } else {
                    return "Güncellenecek Ürün Seçimi Yapmadınız.";
                }

            } else {
                return "Güncelleme için seçilmiş ürün yok.";
            }
        } else {
            return "Güncelleme Yapacağınız Bir Ürün Yok Gibi.";
        }
    }

    public function etiketbul($data)
    {

        $this->db->select('group_concat( id SEPARATOR "," ) s');
        $this->db->from('etiketler');
        $this->db->where_in('adi', $data);
        return $this->db->get()->row()->s;

    }

    public function urun_etiketler($stok_kodu)
    {
        $this->db->select('urun.*');
        $this->db->from('urunler urun');
        $this->db->where('stok_kodu =', $stok_kodu);
        $etiketler = $this->db->get()->row()->etiket;
        $etiketler = explode(',', $etiketler);
        $this->db->select('etk.*');
        $this->db->from('etiketler etk');
        $this->db->where_in('id', $etiketler);
        $etiketler = $this->db->get()->result();
        return $etiketler;
    }

    public function etiket_ad($data, $limit = 10)
    {
        if (isset($data['q'])) {
            $this->db->select('et.*');
            $this->db->from('etiketler et');
            $this->db->where('et.adi like', '%' . $data['q'] . '%');
            $this->db->limit(0, 10);
            return $this->db->get()->result();
        }
    }


}

?>
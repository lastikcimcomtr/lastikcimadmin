<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dimension extends CI_Controller  {


  public $theme_url;
  public function __construct()
  {
    parent::__construct();
    $this->theme_url = $this->parser->theme_url();
    $this->load->model('Dimension_model', 'dimension');
    $this->load->model('Products_model', 'products');
    $this->load->model('Category_model', 'category');
  }


  public function add(){
    if ($this->input->method() == "post") {
      $pdata = $this->input->post();
      if($this->dimension->save($pdata)){
        echo "ok";
      }
      else{
        echo "no";
      }
    }
    else
    {
      $data['theme_url']      = $this->theme_url;
      $data['kategoriler'] = $this->category->kategoriler(array('tip'=>'lastik','ust'=>0));
      $this->parser->parse("dimension/add", $data);
    }
  }

  public function verilist($ust,$tip,$usttip='kategoriler'){
    echo json_encode($this->products->veriler(array('ust'=>$ust,'usttip'=>$usttip,'tip'=>$tip)));
  }



  public function delete(){
    if ($this->input->method() == "post") {
      $pdata = $this->input->post();
      if($this->dimension->delete($pdata)){
        echo "ok";
      }
      else{
        echo "no";
      }
    }
  }

}

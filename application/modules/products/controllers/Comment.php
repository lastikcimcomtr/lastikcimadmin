<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Comment_model', 'comment');
    }

    public function index()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        //$adet = $this->db->select('count(*) adet')->where('okundu =0')->from('yorumlar')->get()->row()->adet;
        //if($adet > 0){
            $this->db->where('id > 0')->update('yorumlar', array('okundu' => 1));
            usleep(200000);
        //}
        $data['yorumlar']   = $this->comment->yorumlar();
        $this->parser->parse("comment/index", $data);
    }

    public function edit($id = NULL)
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if ($id && is_numeric($id)) {
                $yorum = $this->comment->yorumlar(array('id' => $id), TRUE);
                if ($yorum && count($pdata) > 0) {
                    $this->comment->edit($id, $pdata);
                }
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function answer($id = NULL){
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if ($id && is_numeric($id)) {
                $yorum = $this->comment->yorumlar(array('id' => $id), TRUE);
                if ($yorum && count($pdata) > 0) {
					$this->comment->system_answer($yorum,$pdata);
                }
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
	}
    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $yorum = $this->comment->yorumlar(array('id' => $id), TRUE);
            if ($yorum) {
                $this->comment->remove($id);
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

}
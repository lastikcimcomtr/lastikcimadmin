<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marker extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Marker_model', 'marker');
    }

    public function index( $url = "" )    {

        $sdata = array();

        $sdata['sayfa'] = $this->input->get('sayfa');
        if(!is_numeric( $sdata['sayfa'] )){
           $sdata['sayfa']  = 1;
       }
       if ($url <> "")
        $sdata['sef_url'] = $url;
    $extra_url = '';
    if($this->input->get('etiket-ara')){
        $sdata['ara'] = $this->input->get('etiket-ara');
        $extra_url = '?etiket-ara='.$sdata['ara'];
    }
    $temizdata = $sdata;
    $toplamsatir =   $this->marker->etiketler($sdata,true);
    $this->load->library('pagination');
    $this->load->config('pagination');
    $config = $this->config->item('pagination');
    $config['total_rows'] = $toplamsatir->sayfa ;

    unset($temizdata['sayfa']);
    $config['base_url'] =  rtrim(site_url("products/marker/index/" . $url ),"/") ;
    $config['enable_query_strings'] = TRUE;
    $config['page_query_string'] = TRUE;
    $config['allow_get_array'] = TRUE;
    $config['use_page_numbers'] = TRUE;
    $config['reuse_query_string'] = true;
    $config['query_string_segment'] = 'sayfa';

    $config['first_url'] = rtrim(site_url("products/marker/index/" . $url ),"/") .$extra_url;

    $this->pagination->initialize($config);
    $sdata['limit'] = $config['per_page'];
    $sdata['group_by'] = "id";
    $data['theme_url']      = $this->theme_url; 
    $data['etiketler']    = $this->marker->etiketler($sdata);
    if($sdata['ara']) $data['ara'] = $sdata['ara'];

    $this->parser->parse("marker/index", $data);
}


public function add(){
    if ($this->input->method() == "post") {

        $pdata = $this->input->post();
        if (count($pdata) > 0) {
            $this->marker->add($pdata);
            
        } 
        redirect('products/marker/index');
        

    } else {
      $data['theme_url']      = $this->theme_url;
      $this->parser->parse("marker/add", $data);
  }
}



public function edit($id = NULL){
    if ($id && is_numeric($id)) {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if (count($pdata) > 0) {
                $this->marker->edit($id, $pdata);
            } 
        }  
        $data['theme_url']      = $this->theme_url;
        $data['etiket']    = $this->marker->etiketler(array('id' => $id),TRUE);
        $this->parser->parse("marker/edit", $data);
    }else{
        redirect('products/marker/index');
    }
}



public function modernedit($id = NULL)
{
    if ($id && is_numeric($id)) {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->db->where('id',$id)->update('etiketler',['etiket_aciklama' => $pdata['etiket_aciklama']]);
            return redirect('/products/marker/modernedit/'.$id.'?saved=1');
        }
        $gdata = $this->input->get();
        if(isset($gdata['saved'])){
            $data['saved']      = 'Kaydedildi';
        }
        $data['etiket']       = $this->marker->etiketler(array('id' => $id ),TRUE);
        $data['theme_url']      = $this->theme_url;
        $this->parser->parse("marker/modernmarkeredit", $data);
    }
}

public function remove($id = NULL){
    if ($id && is_numeric($id)) {
        $this->marker->remove($id);
    }
    redirect($_SERVER['HTTP_REFERER']);
}


}
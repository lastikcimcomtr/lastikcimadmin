<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{


    public $theme_url;

    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Products_model', 'products');
        $this->load->model('Category_model', 'category');
        $this->load->model('Brand_model', 'brand');
    }

    public function index()
    {
        if ($this->input->method() == "post") {
            $sdata = $this->input->post();
            foreach ($sdata as $key => $value) {
                if ((is_numeric($value) && $value == 0) || $value == 'null' || $value == null) {
                    unset($sdata[$key]);
                } elseif ($key == 'urun_adi') {
                    $sdata[$key] = str_replace("/", "slash", $value);
                }
                if (is_array($value)) {
                    if ($key == 'kategori') {
                        unset($sdata['kategori']);
                        foreach ($value as $deger) {
                            if ($deger != 'null')
                                $sdata['kategori'] = $deger;
                        }

                    } else {
                        $sdata[$key] = implode("-", $value);
                    }

                }
            }
            $url = $this->uri->assoc_to_uri($sdata);
            redirect(site_url("products/index/" . $url));

        } else {
            $sdata = $this->uri->uri_to_assoc();
            foreach ($sdata as $key => $value) {
                if ($key == 'marka' || $key == 'fiyat') {
                    $sdata[$key] = explode("-", $value);
                } elseif ($key == 'urun_adi') {
                    $sdata[$key] = str_replace("slash", "/", $value);
                }
            }
        }
        if (!$sdata['tip']) {
            $sdata['tip'] = "lastik";
        }
        $data = array();
        $data['tip'] = $sdata['tip'];

        if (!isset($sdata['sayfa']))
            $sdata['sayfa'] = 1;
        $urunlersayilari = $this->products->urunsayilar($sdata);
        $this->load->library('pagination');
        $this->load->config('pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $urunlersayilari->toplam;
        $temizdata = $sdata;
        unset($temizdata['sayfa']);
        foreach ($temizdata as $key => $value) {
            if (is_array($value)) {
                $temizdata[$key] = implode("-", $value);
            }
            if ($key == 'urun_adi') {
                $temizdata[$key] = str_replace("/", "slash", $value);
            }
        }
        $config['base_url'] = rtrim(site_url("products/index/" . $this->uri->assoc_to_uri($temizdata)), "/") . "/sayfa/";
        if ($sdata['sayfa'] == 1)
            $config['uri_segment'] = $this->uri->total_segments() + 1;

        $config['first_url'] = rtrim(site_url("products/index/" . $this->uri->assoc_to_uri($temizdata)), "/");
        $this->pagination->initialize($config);
        $sdata['limit'] = $config['per_page'];
        $data['theme_url'] = $this->theme_url;
        $urunler = $this->products->urunler($sdata);				
        $kategoriler = $this->category->kategoriler(array('ust' => 0, 'tip' => $data['tip']));		
        $data['urunler'] = $urunler;
        $data['kategoriler'] = $kategoriler;
        $this->parser->parse("index", $data);
    }

   public function debug()
    {
		$sdata = null;
        if (!isset($sdata['sayfa']))
            $sdata['sayfa'] = 1;
        $urunlersayilari = 10;
        $this->load->library('pagination');
        $this->load->config('pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $urunlersayilari->toplam;
        $this->pagination->initialize($config);
        $sdata['limit'] = $config['per_page'];
        $data['theme_url'] = $this->theme_url;
		$urunler = $this->db->select('*')->where('fiyat <=',0)->where('stok_miktari >',0)->from('urunler')->get()->result();
        $kategoriler = $this->category->kategoriler(array('ust' => 0, 'tip' => $data['tip']));
        $data['urunler'] = $urunler;
        $data['kategoriler'] = $kategoriler;
        $this->parser->parse("debug_miktar", $data);
    }	
	
    public function add($tip = NULL)
    {
        $data = array();
        $data['tip'] = $tip;

        /*Parametre Gruplari*/
        if ($tip) {
            $parametregruplari = $this->products->parametregruplari(array('sahip' => $tip));
            if ($parametregruplari) {
                foreach ($parametregruplari as $pkey => $pvalue) {
                    $parametregruplari[$pkey]->parametreler = $this->products->parametreler(array('grup' => $pvalue->id));
                    /*parametre grup kategorileri*/
                    $tekilcat = array();
                    foreach ($parametregruplari[$pkey]->parametreler as $param) {
                        $cats = explode('||', trim($param->categories, '|'));
                        foreach ($cats as $kategori) {
                            $tekilcat[$kategori] = 1;
                        }
                    }
                    $cikti = "";
                    foreach ($tekilcat as $kid => $onemsiz) {
                        $cikti .= "|$kid|";
                    }
                    $parametregruplari[$pkey]->categories = $cikti;
                    /*parametre grup kategorileri*/
                }
                $data['parametregruplari'] = $parametregruplari;
            }
        }
        /*Parametre Gruplari*/

        /*Kategoriler*/
        $sdata = array();
        $sdata['ust'] = 0;
        if ($tip) {
            $sdata['tip'] = $tip;
        }
        $data['kategoriler'] = $this->category->kategoriler($sdata);
        /*Kategoriler*/

        /*Markalar*/
        $sdata = array();
        $sdata['ust'] = 0;
        if ($tip) {
            $sdata['tip'] = $tip;
        }
        $data['markalar'] = $this->brand->markalar($sdata);
        /*Markalar*/

        /*Mevsimler*/
        $data['mevsimler'] = $this->products->mevsimler();
        /*Mevsimler*/

        /*Desenler*/
        $sdata = array();
        $sdata['ust'] = 0;
        if ($tip) {
            $sdata['tip'] = $tip;
        }
        $data['desenler'] = $this->brand->desenler($sdata);
        /*Desenler*/

        /*Tabanlar*/
        $sdata = array();
        if ($tip) {
            $sdata['tip'] = "taban";
        }
        $data['tabanlar'] = $this->products->veriler($sdata);
        /*Tabanlar*/

        /*Yanaklar*/
        $sdata = array();
        if ($tip) {
            $sdata['tip'] = "yanak";
        }
        $data['yanaklar'] = $this->products->veriler($sdata);
        /*Yanaklar*/

        /*Jant Çapları*/
        $sdata = array();
        if ($tip) {
            $sdata['tip'] = "jant_capi";
        }
        $data['jantcaplari'] = $this->products->veriler($sdata);
        /*Jant Çapları*/

        /*Doviz Kurları*/
        $data['kurlar'] = $this->products->dovizkurlari();
        /*Döviz Kurları*/

        /*Motor Türleri*/
        $sdata = array();
        if ($tip) {
            $sdata['tip'] = "motosiklet_turu";
        }
        $data['motorsikleturleri'] = $this->products->veriler($sdata);
        /*Motor Türleri*/

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $res = $this->products->add($tip, $pdata);
            switch ($res) {
                case 'BOS':
                    $data['status'] = "warning";
                    $data['message'] = "Boş Yerleri Doldurup Tekrar Deneyiniz.";
                    break;

                case 'EKLENDI':
                    $data['status'] = "success";
                    $data['message'] = "Ürün başarılı bir şekilde sisteme eklenmiştir.";
                    break;

                case 'URUNIDYOK':
                    $data['status'] = "danger";
                    $data['message'] = "Ürün oluşturulurken bir hata oldu.";
                    break;

                case 'URUNEKLENEMEDI':
                    $data['status'] = "danger";
                    $data['message'] = "Ürün sisteme eklenirken bir hata meydana geldi.";
                    break;

                default:
                    $data['status'] = "danger";
                    $data['message'] = "Sistemsel bir hata meydana geldi.";
                    break;
            }
        }

        $data['theme_url'] = $this->theme_url;
        $this->parser->parse("add", $data);
    }

    public function edit($id = NULL)
    {
        $urun = $this->products->urunler(array('id' => $id), TRUE);
        if ($urun) {

            $tip = $urun->tip;

            $data = array();
            $data['tip'] = $tip;


            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $res = $this->products->edit($urun->id, $pdata);
                switch ($res) {
                    case 'BOS':
                        $data['status'] = "warning";
                        $data['message'] = "Boş Yerleri Doldurup Tekrar Deneyiniz.";
                        break;

                    case 'GUNCELLENDI':
                        $user_id = 0;
                        if($this->session->userdata('login')){
                            $user_id = $this->session->userdata('login')->id;
                        }
                        $this->db->insert('takip_veri',array('dump_veri' => json_encode($pdata), 'tarih' => time(), 'old_veri' => json_encode($urun),'user_id' => $user_id));
                        $urun = $this->products->urunler(array('id' => $id), TRUE);
                        $data['status'] = "success";
                        $data['message'] = "Ürün başarılı bir şekilde güncellenmiştir.";
                        break;

                    case 'URUNYOK':
                        $data['status'] = "danger";
                        $data['message'] = "Ürün oluşturulurken bir hata oldu.";
                        break;

                    case 'GUNCELLENEMEDI':
                        $data['status'] = "danger";
                        $data['message'] = "Ürün güncellenirken bir hata meydana geldi.";
                        break;

                    default:
                        $data['status'] = "danger";
                        $data['message'] = "Sistemsel bir hata meydana geldi.";
                        break;
                }

            }


            $data['urun'] = $urun;


            /*Parametre Gruplari*/
            if ($tip) {
                $parametregruplari = $this->products->parametregruplari(array('sahip' => $tip));
                if ($parametregruplari) {
                    foreach ($parametregruplari as $pkey => $pvalue) {
                        $parametregruplari[$pkey]->parametreler = $this->products->parametreler(array('grup' => $pvalue->id));
                        /*parametre grup kategorileri*/
                        $tekilcat = array();
                        foreach ($parametregruplari[$pkey]->parametreler as $param) {
                            $cats = explode('||', trim($param->categories, '|'));
                            foreach ($cats as $kategori) {
                                $tekilcat[$kategori] = 1;
                            }
                        }
                        $cikti = "";
                        foreach ($tekilcat as $kid => $onemsiz) {
                            $cikti .= "|$kid|";
                        }
                        $parametregruplari[$pkey]->categories = $cikti;
                        /*parametre grup kategorileri*/
                    }
                    $data['parametregruplari'] = $parametregruplari;
                }
            }
            /*Parametre Gruplari*/

            /*Kategoriler*/
            $sdata = array();
            $sdata['ust'] = 0;
            if ($tip) {
                $sdata['tip'] = $tip;
            }
            $data['kategoriler'] = $this->category->kategoriler($sdata);
            /*Kategoriler*/

            /*Markalar*/
            $sdata = array();
            $sdata['ust'] = 0;
            if ($tip) {
                $sdata['tip'] = $tip;
            }
            $data['markalar'] = $this->brand->markalar($sdata);
            /*Markalar*/

            /*Mevsimler*/
            $data['mevsimler'] = $this->products->mevsimler();
            /*Mevsimler*/

            /*Desenler*/
            $sdata = array();
            $sdata['ust'] = 0;
            if ($tip) {
                $sdata['tip'] = $tip;
            }
            $data['desenler'] = $this->brand->desenler($sdata);
            /*Desenler*/

            /*Tabanlar*/
            $sdata = array();
            if ($tip) {
                $sdata['tip'] = "taban";
            }
            $data['tabanlar'] = $this->products->veriler($sdata);
            /*Tabanlar*/

            /*Yanaklar*/
            $sdata = array();
            if ($tip) {
                $sdata['tip'] = "yanak";
            }
            $data['yanaklar'] = $this->products->veriler($sdata);
            /*Yanaklar*/

            /*Jant Çapları*/
            $sdata = array();
            if ($tip) {
                $sdata['tip'] = "jant_capi";
            }
            $data['jantcaplari'] = $this->products->veriler($sdata);
            /*Jant Çapları*/

            /*Doviz Kurları*/
            $data['kurlar'] = $this->products->dovizkurlari();
            /*Döviz Kurları*/

            /*Motor Türleri*/
            $sdata = array();
            if ($tip) {
                $sdata['tip'] = "motosiklet_turu";
            }
            $data['motorsikleturleri'] = $this->products->veriler($sdata);
            /*Motor Türleri*/

            $data['theme_url'] = $this->theme_url;
            $this->parser->parse("edit", $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $this->products->remove($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function updatemultiproducts($tip = NULL)
    {
        if (is_null($tip))
            $tip = "lastik";

        $data = array();
        $sdata = array();

        $data['tip'] = $tip;

        if ($this->input->method() == "post") {
            $sdata = $this->input->post();
        }
        $data['theme_url'] = $this->theme_url;
        $urunler = $this->products->urunler($sdata);
        $kategoriler = $this->category->kategoriler(array('ust' => 0, 'tip' => $tip));

        $data['urunler'] = $urunler;
        $data['kategoriler'] = $kategoriler;
        $this->parser->parse("updatemultiproducts", $data);
    }

    public function updatemultiproductsdata()
    {
        sleep(2);
        $data = array();
        if ($this->input->method() == "post") {
            $data['status'] = "success";
            $pdata = $this->input->post();
            $data['message'] = $this->products->updatemultiproducts($pdata);
            $data['post'] = $pdata;
        } else {
            $data['status'] = "danger";
            $data['message'] = "Bu sayfaya böyle erişemezsiniz.";
        }
        echo json_encode($data);
    }

    public function siralama()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if (isset($pdata['tip'])) {
                $this->products->siralamaadd($pdata);
            }
            if (isset($pdata['id'])) {
                $this->products->siralamaedit($pdata);
            }
        }
        $data['siralamalar'] = $this->products->siralama();
        $data['theme_url'] = $this->theme_url;
        $this->parser->parse("siralama", $data);
    }

    public function spsil($id)
    {
        $this->products->spsil($id);
        redirect(site_url('products/siralama'));
    }


    public function anasayfasiralamadegis($islem, $kategori, $gelenurun)
    {
        $this->products->anasayfasiralamadegis($islem, $gelenurun);
        $data['kategori'] = $kategori;
        $this->parser->parse("anasayfasiralamadegis", $data);

    }


    public function otomatiketiket($sayfa = 1)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $data['limit'] = 1000;
        $data['sayfa'] = $sayfa;
        $data['tip'] = 'lastik';
        $urunler = $this->products->urunler($data);
        echo count($urunler);
        foreach ($urunler as $urun) {
            $marka = json_decode($urun->markalar);
            if (is_array($marka))
                $marka = $marka[0]->name;
            $etiketcikar = array();
            if ($urun->kategori == 3) {
                $ebat = $urun->taban_deger . "/" . $urun->yanak_deger . strtoupper($urun->yapisi) . $urun->jant_capi_deger . "c";
                $ebatcikar = $urun->taban_deger . "/" . $urun->yanak_deger . strtoupper($urun->yapisi) . $urun->jant_capi_deger;
                $etiketcikar[] = $ebatcikar . " Lastik Fiyatları";
                $etiketcikar[] = $ebatcikar . " " . $marka . " Lastik Fiyatları";
                if ($urun->mevsim == "kis") {
                    $etiketcikar[] = $ebatcikar . " Kış Lastiği Fiyatları";
                }
            } else {
                $yapisi = strtoupper($urun->yapisi);
                if ($yapisi == 'TL') ;
                $yapisi = "R";
                $ebat = $urun->taban_deger . "/" . $urun->yanak_deger . $yapisi . $urun->jant_capi_deger;
            }
            $etiket = array();
            $etiket[] = $ebat . " Lastik Fiyatları";
            $etiket[] = $ebat . " " . $marka . " Lastik Fiyatları";
            if ($urun->mevsim == "kis") {
                $etiket[] = $ebat . " Kış Lastiği Fiyatları";
                $etiket[] = $marka . " Kış Lastiği Fiyatları";
                $etiket[] = $marka . " Kış Lastik Fiyatları";
                $etiket[] = "Kış Lastikleri";
            }
            if ($urun->kategori == 30 || $urun->kategori == 31 || $urun->kategori == 32 || $urun->kategori == 2) {
                $etiket[] = "4x4 Arazi Lastikleri";
            }

            $yaz = array();
            $yaz['olan'] = explode(",", $urun->etiket);
            $yaz['gelen'] = explode(",", $this->products->etiketbul($etiket));
            if (count($etiketcikar) > 0) {
                $yaz['cikar'] = explode(",", $this->products->etiketbul($etiketcikar));
            } else {
                $yaz['cikar'] = array();
            }
            $yaz['son'] = array();
            foreach ($yaz['gelen'] as $key => $value) {
                if ($value)
                    $yaz['son'][trim($value)] = trim($value);
            }
            foreach ($yaz['olan'] as $key => $value) {
                if ($value && (!(count($yaz['cikar']) > 0) || !in_array(trim($value), $yaz['cikar'])))
                    $yaz['son'][trim($value)] = trim($value);
            }

            $son = implode(',', $yaz['son']);

            $cikti[] = array('id' => $urun->id, 'etiket' => $son);
        }

        $this->products->db->update_batch('urunler', $cikti, 'id');
        echo "bitti";
    }

    public function etiket_kaydet()
    {
        $pdata = $this->input->post();
        if (isset($pdata['etiket_id'])) {
            $etiketler = implode(',', $pdata['etiket_id']);
            $this->db->where('id', (int)$pdata['urun_id']);
            $this->db->update('urunler', array('etiket' => $etiketler));

        }
        echo json_encode($etiketler);
    }
	public function magic_sef_url(){
		$effected = $this->products->magicsefurl();
		echo $effected.' ürün etkilendi.';
	}
	public function magic_sef_url_etiket(){
		$effected = $this->products->magicsefurletiket();
		echo $effected.' ürün etkilendi.';
	}
	public function etiket_kis_guncelle(){
		$effected = $this->products->etiket_kis_guncelle();
		echo $effected.' ürün etkilendi.';
	}
    public function copyproduct($product_id){
        $product = $this->db->select('*')->from('urunler')->where('id',$product_id)->get()->row();
        $new_product = $product;
        $data = [];
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $product->stok_miktari = 0;
            foreach ($pdata as $key => $line) {
                if($key == 'stok_kodu' && $line == $product->stok_kodu){
                    $data['status'] = "danger";
                    $data['message'] = "Aynı stok koduyla ürün oluşturamazsınız.";
                    break;
                }else{
                    $new_product->{$key} = $line;
                }
            }
            if($data['status'] != "danger"){
                $new_product = (array) $new_product;
                unset($new_product['id']);
                $new_product =  $this->products->lastiktitle($new_product);
                $this->db->insert('urunler', $new_product); 
                $last_id = $this->db->insert_id();
                redirect(site_url('products/edit/'.$last_id));
                die();
            }
        }
        $data['product'] = $product;
        $data['theme_url'] = $this->theme_url;
        $this->parser->parse("copy_product_form", $data);
    }
	public function tatko_eksik_urunler(){
		 $sdata = $this->uri->uri_to_assoc();
		foreach ($sdata as $key => $value) {
			if ($key == 'marka' || $key == 'fiyat') {
				$sdata[$key] = explode("-", $value);
			} elseif ($key == 'urun_adi') {
				$sdata[$key] = str_replace("slash", "/", $value);
			}
		}
		
		$count_urunler = $this->db->select('count(*) as sayi')->from('_entegrasyon ent');
		$count_urunler = $count_urunler->join('urunler urn', 'urn.xml_stok_kodu = ent.stok','left');
		$count_urunler = $count_urunler->where('ent.miktar >',0)->where('ent.ignore',0)->where('ent.model!=','YOLDIÅI')->where('ent.model!=','KAMYON')->where('urn.id is NULL');
		$urunlersayilari = $count_urunler->get()->row()->sayi;
		//echo $urunlersayilari;
		//die();
		$data = array();
		$sdata['tip'] = 'lastik';
        $data['tip'] = $sdata['tip'];
        if (!isset($sdata['sayfa']))
            $sdata['sayfa'] = 1;
        $urunlersayilari = $urunlersayilari;
        $this->load->library('pagination');
        $this->load->config('pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $urunlersayilari;
        $temizdata = $sdata;
        unset($temizdata['sayfa']);
        foreach ($temizdata as $key => $value) {
            if (is_array($value)) {
                $temizdata[$key] = implode("-", $value);
            }
            if ($key == 'urun_adi') {
                $temizdata[$key] = str_replace("/", "slash", $value);
            }
        }
        $config['base_url'] = rtrim(site_url("products/tatko_eksik_urunler/" . $this->uri->assoc_to_uri($temizdata)), "/") . "/sayfa/";
        if ($sdata['sayfa'] == 1)
            $config['uri_segment'] = $this->uri->total_segments() + 1;

        $config['first_url'] = rtrim(site_url("products/tatko_eksik_urunler/" . $this->uri->assoc_to_uri($temizdata)), "/");
        $this->pagination->initialize($config);
        $sdata['limit'] = $config['per_page'];
        $data['theme_url'] = $this->theme_url;
        $urunler = $eksik_urunler;				
		$eksik_urunler = $this->db->select('ent.*')->from('_entegrasyon ent');
		$eksik_urunler = $eksik_urunler->join('urunler urn', 'urn.xml_stok_kodu = ent.stok','left');
		$eksik_urunler = $eksik_urunler->where('ent.miktar >',0)->where('ent.model!=','YOLDIÅI')->where('ent.model!=','KAMYON')->where('ent.ignore',0)->where('urn.id is NULL');
		$eksik_urunler = $eksik_urunler->limit(30,($sdata['sayfa'] * 30))->get()->result();		
        $data['urunler'] = $eksik_urunler;
        $this->parser->parse("eksik_urunler", $data);
	}

}

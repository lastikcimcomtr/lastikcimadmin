<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Category_model', 'category');
    }

    public function index($ust = 0)
    {
        if (!is_numeric($ust))
            $ust = 0;

        $data = array();
        $data['ust']            = $ust;
        if ($ust > 0) {
            $kategori = $this->category->kategoriler(array('id' => $ust), TRUE);
            $data['kategorisi'] = $kategori;
        }
        $data['tipler']         = $this->category->kategoritipleri();
        $data['theme_url']      = $this->theme_url;
        $data['kategoriler']    = $this->category->kategoriler(array('ust' => $ust));
        $data['parametreler'] = $this->category->parametreler();
        $this->parser->parse("category/index", $data);
    }

    public function addcategory()
    {
        if ($this->input->method() == "post") {

            $pdata = $this->input->post();
            if (count($pdata) > 0) {
                $this->category->addcategory($pdata);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect('products/category/index');
            }

        } else {
            redirect('products/category/index');
        }
    }

    public function editcategory($id = NULL)
    {
        if ($this->input->method() == "post") {

            if ($id && is_numeric($id)) {
                $pdata = $this->input->post();
                if (count($pdata) > 0) {
                    $this->category->editcategory($id, $pdata);
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    redirect('products/category/index');
                }
            } else {
                redirect('products/category/index');
            }

        } else {
            redirect('products/category/index');
        }
    }

    public function editcategoryimage($id = NULL)
    {
        if ($this->input->method() == "post") {

            if ($id && is_numeric($id)) {
                $this->category->editcategoryimage($id);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect('products/category/index');
            }

        } else {
            redirect('products/category/index');
        }
    }

    public function remove($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $this->category->remove($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function editparams($id){
       if ($this->input->method() == "post") {
           $pdata = $this->input->post();
           echo $this->category->editparams($id,$pdata);
       }else{
          echo 'no';
      }
  }
}
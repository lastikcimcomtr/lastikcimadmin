<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Brand_model', 'brand');
        $this->load->model('Category_model', 'category');
        $this->load->model('Products_model', 'products');
    }

    public function index($ust = 0)
    {
        if (!is_numeric($ust))
            $ust = 0;

        $data = array();
        $data['ust']            = $ust;
        if ($ust > 0) {
            $marka = $this->brand->markalar(array('id' => $ust), TRUE);
            $data['markasi'] = $marka;
        }
        $data['tipler']         = $this->brand->markatipleri();
        $data['theme_url']      = $this->theme_url;
        $data['kategoriler']    = $this->category->kategoriler(array('ust' => 0));
        $data['markalar']    = $this->brand->markalar(array('ust' => $ust));
        $this->parser->parse("brand/index", $data);
    }

    public function patterns($id = NULL , $sayfa = 1 , $order = "adi")
    {
        if ($id && is_numeric($id)) {
            $marka = $this->brand->markalar(array('id' => $id), TRUE);
            if ($marka) {
                $data                   = array();
                $data['markasi']        = $marka;
                $data['kategoriler']    = $this->category->kategoriler(array('ust' => 0, 'tip' => $marka->tip));
                $data['mevsimler']      = $this->products->mevsimler();


                $this->load->library('pagination');
                $this->load->config('pagination');
                $config = $this->config->item('pagination');
                $config['total_rows'] = count($this->brand->desenler(array('marka' => $marka->id )));
                $config['base_url']         =  rtrim(site_url("products/brand/patterns/" .$id ) ,"/") . "/";
                $config['uri_segment'] = 2;
                $config['first_url'] = rtrim(site_url("products/brand/patterns" .$id ) ,'/') ;
                $this->pagination->initialize($config);




                $data['desenler']       = $this->brand->desenler(array('marka' => $marka->id , 'limit'=>$config['per_page'] , 'sayfa'=> $sayfa ,'order' => $order));
                $data['theme_url']      = $this->theme_url;
                $this->parser->parse("brand/patterns", $data);
            } else {
                redirect('products/brand/index');
            }
        } else {
            redirect('products/brand/index');
        }
    }

    public function addpattern()
    {
        if ($this->input->method() == "post") {

            $pdata = $this->input->post();
            if (count($pdata) > 0) {
                $this->brand->addpattern($pdata);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect('products/brand/patterns');
            }

        } else {
            redirect('products/brand/patterns');
        }
    }

    public function addbrand()
    {
        if ($this->input->method() == "post") {

            $pdata = $this->input->post();
            if($pdata['kategoriler']){
               $pdata['kategoriler'] = implode(',', $pdata['kategoriler']); 
            }
            if (count($pdata) > 0) {
                $this->brand->addbrand($pdata);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect('products/brand/index');
            }

        } else {
            redirect('products/brand/index');
        }
    }

    public function editpattern($id = NULL)
    {
        if ($id && is_numeric($id)) {
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                if (count($pdata) > 0) {
                    $this->brand->editpattern($id, $pdata);
                } 
            } 
            $data['tipler']         = $this->brand->markatipleri(); 
            $data['desen']       = $this->brand->desenler(array('id' => $id ),TRUE);
            $data['theme_url']      = $this->theme_url;
            $this->parser->parse("brand/editpattern", $data);
        }else{
            redirect('products/brand/index');
        }
    }

    public function editbrand($id = NULL)
    {
        if ($id && is_numeric($id)) {
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                if (count($pdata) > 0) {
                    if($pdata['kategoriler']){
                       $pdata['kategoriler'] = implode(',', $pdata['kategoriler']); 
                    }
                    $this->brand->editbrand($id, $pdata);
                } 
            }  
            $data['tipler']         = $this->brand->markatipleri();
            $data['kategoriler']    = $this->category->kategoriler(array('ust' => 0));
            $data['theme_url']      = $this->theme_url;
            $data['marka']    = $this->brand->markalar(array('id' => $id),TRUE);
            $this->parser->parse("brand/editbrand", $data);
        }else{
            redirect('products/brand/index');
        }
    }



public function editpatternimage($id = NULL , $resim = "resim")
{
    if ($this->input->method() == "post") {

        if ($id && is_numeric($id)) {
            $this->brand->editpatternimage($id, $resim );
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect('products/brand/patterns');
        }

    } else {
        redirect('products/brand/patterns');
    }
}

public function editbrandimage($id = NULL)
{
    if ($this->input->method() == "post") {

        if ($id && is_numeric($id)) {
            $this->brand->editbrandimage($id);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect('products/brand/index');
        }

    } else {
        redirect('products/brand/index');
    }
}

public function remove($id = NULL)
{
    if ($id && is_numeric($id)) {
        $this->brand->remove($id);
    }
    redirect($_SERVER['HTTP_REFERER']);
}

public function removepattern($id = NULL)
{
    if ($id && is_numeric($id)) {
        $this->brand->removepattern($id);
    }
    redirect($_SERVER['HTTP_REFERER']);
}
public function correct_images($files){
    $result = [];
    foreach ($files as $key => $variable) {
        foreach ($variable as $key_2 => $file_row) {
            foreach ($file_row as $key_3 => $file_column) {
                $result[$key_2][$key_3][$key] = $file_column['img'];
            }
        }
    }
    return $result;
}
    public function modernedit($id = NULL)
    {
        if ($id && is_numeric($id)) {
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $this->db->where('id',$id)->update('desenler',['urun_aciklama' => $pdata['urun_aciklama'], 'desen_aciklama' => $pdata['desen_aciklama']]);
                return redirect('/products/brand/modernedit/'.$id.'?saved=1');
            }
            $gdata = $this->input->get();
            if(isset($gdata['saved'])){
                $data['saved']      = 'Kaydedildi';
            }
            $data['desen']       = $this->brand->desenler(array('id' => $id ),TRUE);
            $data['theme_url']      = $this->theme_url;
            $this->parser->parse("brand/modernedit", $data);
        }
    }

    public function renderpattern($id = NULL)
    {

        if ($id && is_numeric($id)) {
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                if (count($pdata) > 0) {
                    $pdata = $pdata['row'];
                    $files = $_FILES['row'];
                    $files = $this->correct_images($files);
                    $rendered_html = '';
                    $rendered_html .= '<div class="col-md-12">';
                    foreach ($pdata as $key => $row) {
                        if(!isset($files[$key])){
                            $files[$key] = [];
                        }
                        $output = $this->renderRow($row,$files[$key]);
                        $rendered_html .= $output;
                    }
                    $rendered_html .= '</div>';
                    $mevcut_desen = $this->db->select('*')->from('desenler')->where('id',$id)->get()->row();
                    $yan_desenler = $this->db->select('*')->from('desenler')->get()->result();
                    $this->db->where('marka',$mevcut_desen->marka)->where('adi',$mevcut_desen->adi)->update('desenler',['urun_aciklama' => $rendered_html, 'desen_aciklama' => $rendered_html]);
                }
                return redirect('/products/brand/editpattern/'.$id);
            } 
            $parse_array = [];
            $parse_array['1'] = [];
            $parse_array['1'][] = ['type' => 'input' , 'input-type' => 'text', 'name' => 'h2' , 'placeholder' => 'H2 Başlık için değer giriniz.' , 'label' => 'H2 Başlık'];
            $parse_array['2'] = [];
            $parse_array['2'][] = ['type' => 'textarea' , 'name' => 'p', 'placeholder' => 'Paragraf 1 için değer giriniz.' , 'label' => 'Paragraf 1 Yazı'];


            $parse_array['3'] = [];
            $parse_array['3'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 1' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 1 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 1 Açıklama']];

            $parse_array['4'] = [];
            $parse_array['4'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 2' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 2 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 2 Açıklama']];
            $parse_array['4'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 3' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 3 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 3 Açıklama']];

            $parse_array['5'] = [];
            $parse_array['5'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 4' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 4 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 4 Açıklama']];
            $parse_array['5'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 5' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 5 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 5 Açıklama']];
            $parse_array['5'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 6' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 6 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 6 Açıklama']];


            $parse_array['6'] = [];
            $parse_array['6'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 7' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 7 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 7 Açıklama']];

            $parse_array['7'] = [];
            $parse_array['7'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 8' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 8 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 8 Açıklama']];
            $parse_array['7'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 9' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 9 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 9 Açıklama']];

            $parse_array['8'] = [];
            $parse_array['8'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 10' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 10 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 10 Açıklama']];
            $parse_array['8'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 11' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 11 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 11 Açıklama']];
            $parse_array['8'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 12' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 12 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 12 Açıklama']];

            $parse_array['9'] = [];
            $parse_array['9'][] = ['type' => 'input' , 'input-type' => 'text', 'name' => 'h3' , 'placeholder' => 'H3 Başlık için değer giriniz.' , 'label' => 'H3 Başlık'];
            $parse_array['10'] = [];
            $parse_array['10'][] = ['type' => 'textarea' , 'name' => 'p', 'placeholder' => 'Paragraf 2 için değer giriniz.' , 'label' => 'Paragraf 2 Yazı'];


            $parse_array['11'] = [];
            $parse_array['11'][] = ['type' => 'input',  'input-type' => 'file' , 'name' => 'img' , 'placeholder' => '' , 'label' => 'Resim 13' , 'sub_input' => ['type' => 'input',  'input-type' => 'text' , 'name' => 'img-description', 'placeholder' => 'Resim 13 Açıklama İçin Yazı Giriniz.' , 'label' => 'Resim 13 Açıklama']];
            $parse_array['11'][] = ['type' => 'input',  'input-type' => 'text' , 'name' => 'video' , 'placeholder' => 'Video 1 İçin Youtube URL değer giriniz.' , 'label' => 'Video 1'];
            $parse_array['11'][] = ['type' => 'textarea' , 'name' => 'p', 'placeholder' => 'Paragraf 3 için değer giriniz.' , 'label' => 'Paragraf 3 Yazı'];

            $parse_array['12'] = [];
            $parse_array['12'][] = ['type' => 'input',  'input-type' => 'text' , 'name' => 'video' , 'placeholder' => 'Video 2 İçin Youtube URL değer giriniz.' , 'label' => 'Video 2'];


            $data['desen']       = $this->brand->desenler(array('id' => $id ),TRUE);
            $data['theme_url']      = $this->theme_url;
            $data['input_parse'] = $parse_array;
            $this->parser->parse("brand/renderpattern", $data);
        }else{
            redirect('products/brand/index');
        }
    }

    public function renderRow($row,$files){
		//print_r($row);
        $html_output = '';
        if(sizeof($row)>0){
            $item_set = 0;
            foreach ($row as $key => $row_item) { 
                if(isset($files[$key])){
                    $sub_file = $files[$key];
                }else{
                    $sub_file = [];
                }
                if(isset($row_item['img-description'])){
                    $item_set ++;
                }
                if(isset($row_item['video'])){
                    $item_set ++;
                }
            }
			//echo '<br>i:'.$item_set.'<br>';
            foreach ($row as $key => $row_item) {   
                if(isset($files[$key])){
                    $sub_file = $files[$key];
                }else{
                    $sub_file = [];
                }
                $md_div = (12/count($row));
                if($item_set == 2){
                    //$md_div = (12/(count($row) - 1));
                }
                $html_item = $this->renderRowItem($key,$row_item,$sub_file,$md_div);
                if(strlen($html_item) > 0){
                    $html_item = '<div class="col-sm-12 col-md-'.$md_div.'">'.$html_item.'</div>';
                }
                $html_output .= $html_item;
            }
        }
        if(strlen($html_output) > 0){
            $html_output = '<div class="row">'.$html_output.'</div>';
        }
		//echo '<hr>';
        return $html_output;
    }
    public function renderRowItem($item_type,$row_item,$sub_file,$md_div){
        $html_output = '';
        if($row_item['h2']){
            $html_output .= '<h2>'.$row_item['h2'].'</h2>';
        }elseif($row_item['p']){
            $html_output .= ''.$row_item['p'].'';
        }elseif($row_item['h3']){
            $html_output .= '<h3>'.$row_item['h3'].'</h2>';
        }elseif(sizeof($sub_file)>0 && strlen($sub_file['name'])>4){
            $image_u = $this->extraservices->desc_multiple_upload('./uploads/desenparser/',[$sub_file]);
            $image_link = '/uploads/desenparser/'.$image_u['images'][0];
            $html_output .= '<div class="row mb-3"><div class="col-md-12">';
            $html_output .= '<img class="img-fluid" src="'.$image_link.'"/>';
            $html_output .= '</div></div>';
        }elseif($row_item['video']){
            $html_output .= '<iframe width="100%" height="515" src="https://www.youtube.com/embed/'.$row_item['video'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        }
        if($row_item['img-description']){
            $html_output .= '<div class="row"><div class="col-md-12">';
            $html_output .= '<p>'.$row_item['img-description'].'</p>';
            $html_output .= '</div></div>';
        }
        return $html_output;
    }

}
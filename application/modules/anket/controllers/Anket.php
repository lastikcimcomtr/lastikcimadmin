<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anket extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Anket_model', 'anket');
    }

    public function index()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['anketler']   = $this->anket->anketler();
        $this->parser->parse("index", $data);
    }

    public function add(){
        if ($this->input->method() == "post") {
            $data =$this->input->post();
            $this->anket->add($data);
        }
        redirect(site_url("anket/index/"));
    }

     public function edit(){
        if ($this->input->method() == "post") {
            $data =$this->input->post();
            $this->anket->edit($data);
        }
        redirect(site_url("anket/index/"));
    }

       public function remove($id){
      
            $this->anket->remove($id);
     
        redirect(site_url("anket/index/"));
    }
}

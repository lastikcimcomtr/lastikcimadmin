<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anket_model extends CI_Model {

	
	function __construct() {
		parent::__construct();	
	}
	


	public function anketler($sdata = NULL, $row = FALSE){
		$this->db->select("*");
		$this->db->from("anketler");
		$this->db->order_by('sira asc, tarih desc');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {				
				$this->db->where($key, $value);		
				
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}


	public function secenekler($anket_id){

		$this->db->select("*");
		$this->db->from("anket_secenekler");
		$this->db->where('anket_id',$anket_id);
		$res = $this->db->get()->result();
		return $res;
	}

	public function add($data){

		$secenekler = $data['secenek'];
		unset($data['secenek']);
		$data['tarih'] = time();	
		$this->db->insert("anketler",$data);
		$anket_id = $this->db->insert_id();
		foreach ($secenekler as  $value) {
			if(trim($value) <> "")
			$this->db->insert("anket_secenekler",array('anket_id'=>	$anket_id ,'secenek'=>$value));
		}

	}
	public function remove($id){
		$this->db->where('id',$id);
		$this->db->delete("anketler");
		$this->db->where('anket_id',$id);
		$this->db->delete("anket_secenekler");
		$this->db->where('anket_id',$id);
		$this->db->delete("anket_cevaplar");
	}


	public function edit($data){

		$secenekler = $data['secenek'];
		$ekle = $data['ekle'];
		$baslangic = $data['baslangic'];
		unset($data['baslangic']);
		unset($data['secenek']);
		unset($data['ekle']);	
		$this->db->where('id',$data['id']);
		$this->db->update("anketler",$data);
		$anket_id =$data['id'];
		foreach ($secenekler as $key => $value) {
			$this->db->where('id',$key);
			if(trim($value) <> ""){				
				$this->db->update("anket_secenekler",array('secenek'=>$value ,'baslangic' => $baslangic[$key]));
			}else{
				$this->db->delete("anket_secenekler");
			}
			
		}
		foreach ($ekle as  $value) {
			if(trim($value) <> "")
				$this->db->insert("anket_secenekler",array('anket_id'=>$anket_id ,'secenek'=>$value));
		}
	}

		public function cevaplar($anket_id){
			$this->db->select("count(DISTINCT c.id) oy , c.secenek_id  , c.anket_id ");
			$this->db->from("anket_cevaplar c");
			$this->db->select("s.secenek");
			$this->db->join("anket_secenekler s", "c.secenek_id = s.id");
			$this->db->select("a.soru");
			$this->db->join("anketler a", "c.anket_id = a.id");
			$this->db->where("c.anket_id",$anket_id);
			$this->db->order_by('oy desc');
			$this->db->group_by('c.secenek_id');
			return $this->db->get()->result();
	}

}

?>
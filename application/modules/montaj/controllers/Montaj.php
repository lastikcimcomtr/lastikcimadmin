<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Montaj extends CI_Controller{


  public $theme_url;
  public function __construct(){
    parent::__construct();
    $this->theme_url = $this->parser->theme_url();
    $this->load->model('Montaj_model', 'montaj');
  }


  public function index(){      
    $data = array();
    $data['theme_url'] = $this->theme_url;            
    $this->parser->parse("index", $data);
  }


  public function add(){
   $data = array();

   if ($this->input->method() == "post") {
    $pdata  = $this->input->post();
    $sonuc = $this->montaj->ekle($pdata);
    if ($sonuc) {
      $data['status']   = "success";
      $data['message']  = "Montaj Noktası başarılı bir şekilde eklenmiştir.";
    } else {
      $data['status']   = "danger";
      $data['message']  = "Montaj Noktası  eklenirken hata meydana geldi.";
    }
  }
  $data['theme_url'] = $this->theme_url;
  $this->parser->parse("add", $data);
}

public function edit($id){
   if ($this->input->method() == "post") {
    $pdata  = $this->input->post();
    $sonuc = $this->montaj->duzenle($id , $pdata);
    if ($sonuc) {
      $data['status']   = "success";
      $data['message']  = "Montaj Noktası başarılı bir şekilde düzenlendi.";
    } else {
      $data['status']   = "danger";
      $data['message']  = "Montaj Noktası  düzenlenirken hata meydana geldi.";
    }
  }

 $data = $this->montaj->montajnoktasi($id);
 $data['theme_url'] = $this->theme_url;
 $this->parser->parse("edit", $data);
}


}

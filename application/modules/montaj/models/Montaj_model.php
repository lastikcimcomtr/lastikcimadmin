<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Montaj_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	
	public function montajNoktalari($il = 34,$ilce = NULL){
		$this->db->select('f.id,
			f.firma_adi,
			f.firma_il,
			f.firma_ilce,
			f.firma_alternatif_ilce,
			f.firma_adresi,
			f.firma_logo,
			f.firma_tel1,
			f.firma_tel2,
			f.lat,
			f.lng,
			il.il,
			ilce.ilce');
		$this->db->from('montaj_noktalari f');
		$this->db->join('iller il','f.firma_il = il.id','left');
		$this->db->join('ilceler ilce','f.firma_ilce = ilce.id','left');
		if($il)
			$this->db->where('firma_il', $il);
		if($ilce)
			$this->db->where('firma_ilce', $ilce);
		$res = $this->db->get()->result();
		
		return $res;
	}



public function montajsil($data) {
	$id = $data["id"];
	if(is_numeric($id)){
		$this->db->where('id',$id);
		$this->db->delete('montaj_noktalari');
	
		$this->db->where('firma_id',$id);
		$this->db->delete('montaj_noktasi_hizmetleri');
		return "Silindi";
	}else{
		return "Hata Oluştu";
	}

}
	public function hizmetler(){
		$this->db->select("*");
		$this->db->from("montaj_hizmetleri");
		$res = $this->db->get()->result();		
		return $res;

	}

	public function montajnoktasi($id){
		$this->db->select('*');
		$this->db->from('montaj_noktalari');
		$this->db->where('id',$id);
		$data['firma'] = $this->db->get()->row();
		$this->db->select('*');
		$this->db->from('montaj_noktasi_hizmetleri');
		$this->db->where('firma_id',$id);
		$res = $this->db->get()->result();
		$hizmetler = array();
		foreach ($res as  $value) {
			$hizmetler[$value->id_str] = [];
			$hizmetler[$value->id_str]['fiyat']  =  $value->fiyat;
			$hizmetler[$value->id_str]['note']  =  $value->note;
		}
		$data['hizmetler'] =(object) $hizmetler;
		return $data;
	}
	
	


	public function ekle($data = NULL)
	{
		if ($data && count($data) > 0) {

			if (strlen($_FILES['montaj_resim']['name']) > 4) {
				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'montaj_resim'), TRUE)->value;
				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'montaj_resim'));
				$data['firma_logo']	= $upload_res->image;
			} else {
				unset($data['firma_logo']);
			}
			if(isset($data['firma_alternatif_ilce'])){
				$data['firma_alternatif_ilce'] = implode(',', $data['firma_alternatif_ilce'] );
			}
			$hizmetler =  $data['hizmet'];
			unset($data['hizmet']);
			$res = $this->db->insert('montaj_noktalari', $data);
			$firma_id =	$this->db->insert_id();
			foreach ($hizmetler as $key => $value) {
				if($value['check'] == "on"){
					$hizmetinsert = array('firma_id' => $firma_id,'id_str'=>$key,'fiyat'=>$value['fiyat'] );
					$this->db->insert('montaj_noktasi_hizmetleri', $hizmetinsert);
				}
			}
			
			return $res;
		} else {
			return FALSE;
		}
	}
public function duzenle($id , $data = NULL)
	{
		if ($data && count($data) > 0) {

			if (strlen($_FILES['montaj_resim']['name']) > 4) {

				$resim_dizin 	= $this->extraservices->ayarlar(array('name' => 'montaj_resim'), TRUE)->value;

				$upload_res 	= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'montaj_resim'));
				$data['firma_logo']	= $upload_res->image;
			} else {
				unset($data['firma_logo']);
			}
			$hizmetler =  $data['hizmet'];
			unset($data['hizmet']);
			if(isset($data['firma_alternatif_ilce'])){
				$data['firma_alternatif_ilce'] = implode(',', $data['firma_alternatif_ilce'] );
			}
			$this->db->where('id', $id);
			$res = $this->db->update('montaj_noktalari', $data);
			$this->db->delete('montaj_noktasi_hizmetleri', array('firma_id' => $id));
			foreach ($hizmetler as $key => $value) {
				if($value['check'] == "on"){
					$hizmetinsert = array('firma_id' => $id,'id_str'=>$key,'fiyat'=>$value['fiyat'] , 'note' => $value['note']);
					$this->db->insert('montaj_noktasi_hizmetleri', $hizmetinsert);
				}
			}
			return $res;
		} else {
			return FALSE;
		}
	}

}

?>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends CI_Controller
{


    public $theme_url;

    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Orders_model', 'orders');
        /*
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        */
    }

    public function testemre($posid, $taksit)
    {
        $this->orders->posArtiTaksit($posid, $taksit);
    }

    public function index_eski()
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['orders'] = $this->orders->orders();
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("index", $data);
    }

    /**/
    public function index()
    {
        $data = array();
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            foreach ($pdata as $key => $value) {
                if (is_null($value) || strlen($value) < 1) {
                    unset($pdata[$key]);
                }
            }

            if (count($pdata) > 0) {
                $surl = ($this->uri->assoc_to_uri($pdata));
                $surl = site_url('orders/index/' . $surl);
                redirect($surl);
                exit();
            }
        }

        $sdata = $this->uri->uri_to_assoc();

        /**/


        if (!isset($sdata['email']) && !isset($sdata['cari']) && !isset($sdata['isim']) && !isset($sdata['id']) && !isset($sdata['pos_tip']) && !isset($sdata['odeme_tipi']) && !isset($sdata['durum_odeme']) && !isset($sdata['durumu'])) {
            if (!isset($sdata['tarih_min'])) {
                $sdata['tarih_min'] = date_create(date('Y-m-d'))->modify('-1 days')->format('d.m.Y');
            }

            if (!isset($sdata['tarih_max'])) {
                $sdata['tarih_max'] = date('d.m.Y');
            }
        }
        if (isset($sdata['id'])) {
            unset($sdata['tarih_min']);
            unset($sdata['tarih_max']);
        }
        if (isset($sdata['cari'])) {
            if (strlen($sdata['cari']) > 8) {
                unset($sdata['tarih_min']);
                unset($sdata['tarih_max']);
            }
        }
        if (!isset($sdata['durumu'])) {
            $sdata['durumu'] = "hatasizlar";
        }

        /**/

        /**/
        $sdatap = $sdata;
        unset($sdatap['page']);
        $pagination_url = site_url('orders/index/' . $this->uri->assoc_to_uri($sdatap));
        /**/
        if (isset($sdata['limit']) || !is_numeric($sdata['limit'])) {
            $sdata['limit'] = 30;
        }

        if (isset($sdata['isim'])) {
            $sdata['isim'] = urldecode($sdata['isim']);
        }

        if (strpos($sdata['durumu'], '-') !== false) {
            $sdata['durumu'] = explode('-',$sdata['durumu']);
        }
        $tpdata = $sdata;
        $tpdata['toplam'] = 1;

        $data['sdata'] = $sdata;
        $data['theme_url'] = $this->theme_url;
        $data['orders'] = $this->orders->orders($sdata);

        $last_uri = $this->uri->total_segments();
        $last_uri = $last_uri;
        if ($sdata['page']) {
            $last_uri = $last_uri;
        } else {
            $last_uri = $last_uri + 2;
        }
        /**/
        $config = array();
        $config["base_url"] = $pagination_url;
        $config["total_rows"] = $this->orders->orders($tpdata, true)->toplam;
        $config["cur_page"] = $sdata['page'];
        $config["per_page"] = $sdata['limit'];
        $config['prefix'] = "page/";
        $config['use_page_numbers'] = true;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="page-first">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-last">';
        $config['last_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-pre">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-number active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-number">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = "Son Sayfa";
        $config['first_link'] = "İlk Sayfa";
        $config['uri_segment'] = $last_uri;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        /**/

        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("index", $data);
    }

    /*Hatalılar*/
    public function hatalilar()
    {
        $data = array();

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            foreach ($pdata as $key => $value) {
                if (is_null($value) || strlen($value) < 1) {
                    unset($pdata[$key]);
                }
            }

            if (count($pdata) > 0) {
                $surl = ($this->uri->assoc_to_uri($pdata));
                $surl = site_url('orders/hatalilar/' . $surl);
                redirect($surl);
                exit();
            }
        }

        $sdata = $this->uri->uri_to_assoc();

        /**/
        if (!isset($sdata['durumu'])) {
            $sdata['durumu'] = "10";
        }
        /**/

        /**/
        $sdatap = $sdata;
        unset($sdatap['page']);
        unset($sdatap['durumu']);
        $pagination_url = site_url('orders/hatalilar/' . $this->uri->assoc_to_uri($sdatap));
        /**/
        if (isset($sdata['limit']) || !is_numeric($sdata['limit'])) {
            $sdata['limit'] = 30;
        }

        if (isset($sdata['isim'])) {
            $sdata['isim'] = $this->normalize_str(urldecode($sdata['isim']));
        }

        $tpdata = $sdata;
        $tpdata['toplam'] = 1;

        $data['sdata'] = $sdata;
        $data['theme_url'] = $this->theme_url;
        $data['orders'] = $this->orders->orders($sdata);

        $last_uri = $this->uri->total_segments();
        $last_uri = $last_uri;
        if ($sdata['page']) {
            $last_uri = $last_uri;
        } else {
            $last_uri = $last_uri + 2;
        }
        /**/
        $config = array();
        $config["base_url"] = $pagination_url;
        $config["total_rows"] = $this->orders->orders($tpdata, true)->toplam;
        $config["cur_page"] = $sdata['page'];
        $config["per_page"] = $sdata['limit'];
        $config['prefix'] = "page/";
        $config['use_page_numbers'] = true;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="page-first">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-last">';
        $config['last_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-pre">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-number active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-number">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = "Son Sayfa";
        $config['first_link'] = "İlk Sayfa";
        $config['uri_segment'] = $last_uri;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        /**/

        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("index", $data);
    }

    /*Hatalılar*/

    public function siparis_detaylari($id)
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['order'] = $this->orders->orders(array('id' => $id), true);
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("modal/siparis_detaylari", $data);
    }


    public function toggle_tedarikci_odeme()
    {

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $order = $this->orders->orders(array('id' => $pdata['order_id']), true);
            $tedarikci_odeme = 1;
            $base_text = $pdata['order_id'].' numaralı siparişin ';
            $data['message'] = $base_text."tedarikçi ödemesi yapıldı olarak güncellendi.";
            $data['status'] = "success";
            if($order->tedarikci_odeme > 0){
                $data['message'] = $base_text."tedarikçi ödemesi yapılmadı olarak güncellendi.";
                $data['status'] = "error";
                $tedarikci_odeme = 0;
            }
            $this->db->where('id',$pdata['order_id'])->update('siparisler',['tedarikci_odeme' => $tedarikci_odeme]);
            echo json_encode($data);
        }
    }

    public function duzenle($id)
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $siparis_data = json_encode($pdata['siparisdata']);
            $odeme_data = json_encode($pdata['odemedata']);
            $update_data = [];
            $update_data['data'] = $siparis_data;
            $update_data['odemedata'] = $odeme_data;
            $new_order = $this->db->where('id', $pdata['siparis_id'])->update('siparisler', $update_data);
            $surl = site_url('orders/duzenle/' . $pdata['siparis_id']);
            redirect($surl);
        }
        $data = array();
        $order = $this->orders->orders(array('id' => $id), true);
        $data['theme_url'] = $this->theme_url;
        $data['order'] = $order;
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $order_data = json_decode($order->data);
        $data['order_data'] = $order_data;
        $odeme_data = json_decode($order->odemedata);
        $data['odeme_data'] = $odeme_data;
        $montaj_data = json_decode($order->urunler);
        $data['urun_data'] = $montaj_data;
        $user = $_SESSION['login'];
        if (!in_array($user->email, ['berkaykilic@lastikcim.com.tr', 'harunozel@lastikcim.com.tr', 'seldakazak@lastikcim.com.tr'])) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->parser->parse("duzenle", $data);
    }

    public function carisifirla($id)
    {
        $sifirla = $this->db->where('id', $id)->update('siparisler', ['caridata' => null, 'cariadresdata_fatura' => null, 'cariadresdata_sevk' => null, 'siparis_data' => null]);
    }

    public function fatura_bilgileri($id)
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['order'] = $this->orders->orders(array('id' => $id), true);
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("modal/fatura_bilgileri", $data);
    }

    public function kargo_detaylari($id = NULL)
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $user_id = '';
            if ($this->session->userdata('login'))
                $user_id = $this->session->userdata('login')->id;
            $this->db->insert('takip_veri', ['dump_veri' => json_encode($pdata), 'tarih' => time(), 'old_veri' => $id, 'user_id' => $user_id]);
            $kargores = $this->orders->kargogir($id, $pdata);
            $data = array();
            if ($kargores) {
                $data['message'] = "Kargo Numarası Girildi";
                $data['status'] = "success";
            } else {
                $data['message'] = "Hata Oldu, Tekrar Deneyiniz";
                $data['status'] = "danger";
            }
            echo json_encode($data);
        } else {
            $data = array();
            $data['theme_url'] = $this->theme_url;
            $data['order'] = $this->orders->orders(array('id' => $id), true);
            $data['durumlar'] = $this->orders->durumlar();
            $data['kargofirmalar'] = $this->orders->kargofirmalar();
            $this->parser->parse("modal/kargo_detay", $data);
        }
    }

    public function siparis_notu($id = NULL)
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $kargores = $this->orders->siparisnotugir($id, $pdata);
            $data = array();
            if ($kargores) {
                $data['message'] = "Sipariş Notu Güncellendi";
                $data['status'] = "success";
            } else {
                $data['message'] = "Hata Oldu, Tekrar Deneyiniz";
                $data['status'] = "danger";
            }
            echo json_encode($data);
        } else {
            $data = array();
            $data['theme_url'] = $this->theme_url;
            $data['order'] = $this->orders->orders(array('id' => $id), true);
            $data['durumlar'] = $this->orders->durumlar();
            $data['kargofirmalar'] = $this->orders->kargofirmalar();
            $this->parser->parse("modal/siparis_notu", $data);
        }
    }

    public function tedarik_notu($id = NULL)
    {
        $user = $_SESSION['login'];
        if (!in_array($user->email, ['berkaykilic@lastikcim.com.tr', 'harunozel@lastikcim.com.tr', 'seldakazak@lastikcim.com.tr', 'ahmeterkan@lastikcim.com.tr', 'yilmazustun@lastikcim.com.tr'])) {
            echo '<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Tedarik Notu</h4>
		</div>
		<div class="modal-body">
			Yetkiniz Yok!
		</div>';
            return;
        }
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $kargores = $this->orders->tedariknotugir($id, $pdata);
            $data = array();
            if ($kargores) {
                $data['message'] = "Tedarik Notu Güncellendi";
                $data['status'] = "success";
            } else {
                $data['message'] = "Hata Oldu, Tekrar Deneyiniz";
                $data['status'] = "danger";
            }
            echo json_encode($data);
        } else {
            $data = array();
            $data['theme_url'] = $this->theme_url;
            $data['order'] = $this->orders->orders(array('id' => $id), true);
            $data['durumlar'] = $this->orders->durumlar();
            $data['kargofirmalar'] = $this->orders->kargofirmalar();
            $this->parser->parse("modal/tedarik_notu", $data);
        }
    }

    public function montaj_detay($id = NULL)
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['order'] = $this->orders->orders(array('id' => $id), true);
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("modal/montaj_detay", $data);
    }

    /**/
    public function siparisdetay($id)
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['order'] = $this->orders->orders(array('id' => $id), true);
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("siparisdetay", $data);
    }

    public function odemebildirim()
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['bildirimler'] = $this->orders->odemebildirimler();
        $data['durumlar'] = $this->orders->durumlar();
        $this->parser->parse("odemebildirim", $data);

    }

    public function iptaliade()
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['orders'] = $this->orders->orders(array('durumu' => 11));
        $data['durumlar'] = $this->orders->durumlar();
        $data['kargofirmalar'] = $this->orders->kargofirmalar();
        $this->parser->parse("iptaliade", $data);

    }

    public function siparis_cari_excel()
    {
        $siparis_cariler = $this->db->select('id,REPLACE(JSON_EXTRACT(caridata, "$.kod"),"\"","") as cari')->from('siparisler')->where_not_in('durumu', [1, 4, 7, 9, 10])->where('caridata is not null')->order_by('tarih', 'desc')->limit(5000)->get()->result();

        $this->load->library('excel');

        $objPHPExcel = PHPExcel_IOFactory::load("./uploads/import/" . "empty.xlsx");
        $objPHPExcel->setActiveSheetIndex(0);
        $row = 0;
        foreach ($siparis_cariler as $key => $cari_line) {
            $row++;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $cari_line->id);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $cari_line->cari);
        }
        $filename = 'siparis_carilerExcel_' . time() . '.xlsx';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //force user to download the Excel file without writing it to server's HD
        ob_end_clean();

        $objWriter->save('php://output');
    }

    public function uruntalepleri()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();

            foreach ($pdata as $key => $value) {
                if (is_null($value) || strlen($value) < 1) {
                    unset($pdata[$key]);
                }
            }

            if (count($pdata) > 0) {
                $surl = ($this->uri->assoc_to_uri($pdata));
                $surl = site_url('orders/uruntalepleri/' . $surl);
                redirect($surl);
                exit();
            }
        }
        $sdata = $this->uri->uri_to_assoc();
        $pagination_url = site_url('orders/uruntalepleri/' . $this->uri->assoc_to_uri($sdata));
        if (!$sdata['page']) {
            $sdata['page'] = 1;
        }
        if (!$sdata['limit']) {
            $sdata['limit'] = 30;
        }
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['istekler'] = $this->orders->uruntalepleri($sdata);


        $last_uri = $this->uri->total_segments();
        $last_uri = $last_uri;
        if ($sdata['page']) {
            $last_uri = $last_uri;
        } else {
            $last_uri = $last_uri + 2;
        }
        /**/
        $config = array();
        $config["base_url"] = $pagination_url;
        $config["total_rows"] = $this->orders->uruntalepleri_count($sdata);
        $config["cur_page"] = $sdata['page'];
        $config["per_page"] = $sdata['limit'];
        $config['prefix'] = "page/";
        $config['use_page_numbers'] = true;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li class="page-first">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-last">';
        $config['last_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="page-pre">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="page-next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-number active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-number">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = "Son Sayfa";
        $config['first_link'] = "İlk Sayfa";
        $config['uri_segment'] = $last_uri;
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

        $data['sdata'] = $sdata;
        $this->parser->parse("uruntalepleri", $data);

    }

    public function iletisim_talep_update()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $res = $this->orders->uruntalepleri_update($pdata);
            return $res;
        }
    }

    public function edit($id)
    {

    }

    /*27.01.2018*/
    public function posdetay($id = NULL)
    {
        $sdata = array();

        if ($id && is_numeric($id)) {

            $sdata['id'] = $id;

            $data = array();
            $data['sdata'] = $sdata;
            $data['theme_url'] = $this->theme_url;
            $data['posdetaylar'] = $this->orders->posdetaylar($sdata, true);
            $this->parser->parse("posdetay_modal", $data);

        } else {

            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $sdata = $pdata;
            } else {
                $sdata['limit'] = 100;
            }


            $data = array();
            $data['sdata'] = $sdata;
            $data['theme_url'] = $this->theme_url;
            $data['posdetaylar'] = $this->orders->posdetaylar($sdata);
            $this->parser->parse("posdetay", $data);

        }
    }

    public function tedarikciyeiletformu($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $urunler = json_decode($order->urunler);

        if (strlen($order->caridata) < 1 || strlen($order->cariadresdata_sevk) < 1) {
            echo 'Cari bilgisi oluşturulmamış.';
            die();
        }
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adresi = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adresi->no;
        $this->load->library('lastikcim');
        $adres = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        $cari = $this->lastikcim->mevcutCariyiGetirId($cari_kod);
        $musteri = array_merge($adres, $cari);
        $data = array();
        $data['musteri'] = $musteri;
        $data['castrol_extra'] = [];
        foreach ($urunler as $key => $urun) {
            if (isset($urun->tedarikci)) {
                if (strlen($urun->tedarikci) > 0) {
                    if ($urun->tedarikci == 'CASTROL') {
                        $il_ilce = explode('/', $musteri['il_ilce']);
                        $il = $this->db->from('iller')->where('il', $il_ilce[1])->get()->row();
                        if (!$il) {
                            echo 'Cari il bilgisi yanlış.';
                            die();
                        }
                        $ilce = $this->db->from('ilceler')->where('ilce', $il_ilce[0])->where('il_id', $il->id)->get()->row();
                        if (!$ilce) {
                            echo 'Cari ilçe bilgisi yanlış.';
                            die();
                        }
                        $tedarikci_data = $this->db->from('castrol_teslimatlar')->where('il_id', $il->id)->where('ilce_id', $ilce->id)->get()->row();
                        if (!$tedarikci_data) {
                            echo 'Gönderilebilir castrol tedarikçisi bulunmuyor.';
                            die();
                        }
                        $data['castrol_extra'][] = $tedarikci_data;
                    } else {
                        $tedarikci_data = $this->db->select('*')->from('tedarikciler')->where('kod', $urun->tedarikci)->get()->row();
                    }
                    if (!$tedarikci_data) {
                        echo $urun->tedarikci . ' gönderilebilir tedarikçi listesinde değil.';
                        die();
                    }
                    $urunler[$key]->tedarikci_data = ($tedarikci_data);
                }
            } else {
                echo 'Üründe belirlenmiş bir tedarikçi bulunmuyor.';
                die();
            }
        }
        $tum_tedarikciler = $this->db->select('*')->from('tedarikciler')->get()->result();
        $data['tum_tedarikciler'] = $tum_tedarikciler;
        $data['order'] = $order;
        $data['urunler'] = $urunler;
        $this->parser->parse("tedarikci/tedarikcigonder", $data);
    }

    public function tedarikciyegonder()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $send_tedarik_mail = $this->orders->send_tedarik_mail($pdata);
            if ($send_tedarik_mail != 'sent') {
                $result = [];
                $result['status'] = 'false';
                $result['type'] = $send_tedarik_mail;
                echo json_encode($result);
                die();
            }
            $pdata['tarih'] = time();
            $this->db->where('id', $pdata['siparis_no'])->update('siparisler', ['tatko_siparis_data' => json_encode($pdata)]);
        }
    }

    public function renderMail()
    {

    }

    public function carihesapac($id = NULL)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $order_data = json_decode($order->data);

        $search_str_2 = '%';
        if ($order_data->kur_email) {
            $search_str_2 .= 'kur_email":"' . $order_data->kur_email . '",';
        } else {
            $search_str_2 .= 'email":"' . $order_data->email . '",';
        }
        $search_str_2 .= '%';
        $search_str_3 = '%';
        if ($order_data->vergi_no) {
            $search_str_3 .= 'vergi_no":"' . $order_data->vergi_no . '",';
        } else {
            $search_str_3 .= 'tc_kimlik_no":"' . $order_data->tc_kimlik_no . '",';
        }
        $search_str_3 .= '%';
        $old_order = $this->db->select('*')->from('siparisler')->where('data like ', $search_str_2)->where('data like ', $search_str_3)->where('caridata is not null')->order_by('tarih desc')->limit(5)->get()->row();

        //echo $this->db->last_query();
        $data = array();
        $data['order'] = $order;
        if ($old_order) {
            $this->load->library('lastikcim');
            $cari_data = json_decode($old_order->caridata);
            $cari_kod = $cari_data->kod;
            if (strlen($cari_kod) > 4) {
                $old_adress = $this->lastikcim->mevcutAdresiGetir($cari_kod);
                $data['old_adress'] = $old_adress;
                $data['old_order'] = $old_order;
            }
        } else {
            $data['old_order'] = false;
        }
        $this->parser->parse("mikro/carihesapac", $data);
    }

    public function carisiparisac($id = NULL, $mikro = NULL)
    {

        $this->load->model('products/products_model', 'products');
        $order = $this->orders->orders(array('id' => $id), true);
        $data = array();
        $data['order'] = $order;
        $data['mikro'] = $mikro;
        $cari_data = json_decode($order->caridata, true);
        $sevk_adresi = json_decode($order->cariadresdata_sevk, true);
        if ($cari_data['kod'] && $sevk_adresi['no'] && is_numeric($sevk_adresi['no'])) {
            $mikro_data = array();

            if ($mikro <> "tatko") {
                $mikro_data['cari_kod'] = $cari_data['kod'];
                $mikro_data['cari_adres'] = "" . $sevk_adresi['no'] . "";
            }

            /**/
            $mikro_urunler = array();
            $urunler = json_decode($order->urunler);

            foreach ($urunler as $urun) {
                $kdv_orani = $urun->kdv / 100;

                $urun_detay = $this->products->urunler(array('id' => $urun->urun), true);
                $xml_stok_kodu = $urun_detay->xml_stok_kodu;
                $xml_stok_kodu_alt = $urun_detay->xml_stok_kodu_alt;
                if ((mb_strtoupper($xml_stok_kodu) == 'YOK' || strlen($xml_stok_kodu) < 2) && strlen($xml_stok_kodu_alt)) {
                    $xml_stok_kodu = $xml_stok_kodu_alt;
                }
                if ($urun_detay) {

                    if ($urun->kargo == 1 && $mikro <> "tatko") {

                        $mikro_urunler[] = array(
                            'stok_birim' => '0',
                            'stok_tip' => '1',
                            'stok_kod' => '600.02.001',
                            'stok_fiyat' => '' . $urun->kargo_bedeli . '',
                            'adet' => '' . $urun->miktar . '',
                            'stok_toplam' => '' . ($urun->miktar * $urun->kargo_bedeli) . '',
                            'stok_vergi' => '' . (($urun->miktar * $urun->kargo_bedeli) * 0.18) . ''
                        );

                    }

                    if ($urun->kdv_dahil == 1) {
                        // $kdvsiz_fiyat = $urun->fiyat - ($urun->fiyat * $kdv_orani);
                        if ($mikro == "tatko") {
                            $kdvsiz_fiyat = $urun->urun_alis_fiyati;
                        } else {
                            $kdvsiz_fiyat = ($urun->fiyat * (100 - $urun->indirim_orani) / 100) / (1 + ($kdv_orani));
                        }
                        $mikro_ekle = array(
                            'stok_birim' => '1',
                            'stok_tip' => '0',
                            'stok_kod' => '' . $xml_stok_kodu . '',
                            'stok_fiyat' => '' . $kdvsiz_fiyat . '',
                            'adet' => '' . $urun->miktar . '',
                            'stok_toplam' => '' . ($urun->miktar * $kdvsiz_fiyat) . '',
                            'stok_vergi' => '' . (($urun->miktar * $kdvsiz_fiyat) * $kdv_orani) . ''
                        );
                        if ($mikro == "tatko") {
                            $mikro_ekle['urun_detay'] = $urun_detay;
                        }
                        $mikro_urunler[] = $mikro_ekle;

                    } else {
                        if ($mikro == "tatko") {
                            $kdvsiz_fiyat = $urun->urun_alis_fiyati;
                        } else {
                            $kdvsiz_fiyat = ($urun->fiyat * (100 - $urun->indirim_orani) / 100);
                        }
                        $mikro_ekle = array(
                            'stok_birim' => '1',
                            'stok_tip' => '0',
                            'stok_kod' => '' . $xml_stok_kodu . '',
                            'stok_fiyat' => '' . $kdvsiz_fiyat . '',
                            'adet' => '' . $urun->miktar . '',
                            'stok_toplam' => '' . ($urun->miktar * $kdvsiz_fiyat) . '',
                            'stok_vergi' => '' . (($urun->miktar * $kdvsiz_fiyat) * $kdv_orani) . ''
                        );
                        if ($mikro == "tatko") {
                            $mikro_ekle['urun_detay'] = $urun_detay;
                        }
                        $mikro_urunler[] = $mikro_ekle;
                    }

                    if (count($urun->hizmetler) > 0 && $mikro <> "tatko") {
                        foreach ($urun->hizmetler as $hizmet) {
                            $hizmet_fiyat = ($hizmet->fiyat / 1.18);

                            $mikro_urunler[] = array(
                                'stok_birim' => '0',
                                'stok_tip' => '1',
                                'stok_kod' => '600.02.002',
                                'stok_fiyat' => '' . $hizmet_fiyat . '',
                                'adet' => '' . $hizmet->adet . '',
                                'stok_toplam' => '' . ($hizmet->adet * $hizmet_fiyat) . '',
                                'stok_vergi' => '' . (($hizmet->adet * $hizmet_fiyat) * 0.18) . ''
                            );

                        }
                    }
                }
            }
            $mikro_data['urunler'] = $mikro_urunler;
            $mikro_data['ozel_siparis_id'] = $order->id;
            /**/
            switch ($mikro) {
                case 'tatko':
                    $this->load->library('tatkomikro');
                    $spres = $this->tatkomikro->siparisOlustur($mikro_data);
                    break;

                case 'lastikcim':
                    $this->createTahsilat($order->id);
                    $this->load->library('lastikcim');
                    $spres = $this->lastikcim->siparisOlustur($mikro_data);
                    break;

                default:
                    $this->createTahsilat($order->id);
                    $this->load->library('lastikcim');
                    $spres = $this->lastikcim->siparisOlustur($mikro_data);
                    break;
            }

            if ($spres['siparis_id_alinan'] && is_numeric($spres['siparis_id_alinan'])) {
                if ($mikro == "tatko") {
                    $this->orders->siparismikrodata($order->id, $spres, "tatko");
                    $data['spres'] = $spres;
                } else {
                    $this->orders->siparismikrodata($order->id, $spres);
                    $data['spres'] = $spres;
                }
            }
        }

        $this->parser->parse("mikro/carisiparisac", $data);
    }
    /*27.01.2018*/

    /*29.01.2018*/
    public function mikroyacari($siparisid = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            if ($siparisid && is_numeric($siparisid)) {

                $siparis = $this->orders->orders(array('id' => $siparisid), true);
                if ($siparis) {
                    $pdata = $this->input->post();

                    $this->load->library('lastikcim');
                    $chdata = $this->lastikcim->cariHesapOlustur($pdata);
                    if ($chdata['status']) {
                        $data['status'] = $this->orders->siparismikro($siparis->id, $chdata);
                        $data['chdata'] = $chdata;
                    } else {
                        $data['status'] = false;
                        $data['chdata'] = $chdata;
                    }
                } else {
                    $data['status'] = false;
                }

            } else {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }

    public function mikroyacariadres($siparisid = NULL, $tipi = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            if ($siparisid && is_numeric($siparisid)) {

                $siparis = $this->orders->orders(array('id' => $siparisid), true);
                if ($siparis) {
                    $pdata = $this->input->post();

                    $cari_data = json_decode($siparis->caridata, true);

                    $this->load->library('lastikcim');
                    $pdata['cari_kod'] = $cari_data['kod'];
                    $chdata = $this->lastikcim->cariAdresOlustur($pdata);

                    if ($chdata['status']) {
                        $data['status'] = $this->orders->siparismikroadres($siparis->id, $chdata, $tipi);
                        $data['chdata'] = $chdata;
                    } else {
                        $data['status'] = false;
                    }
                } else {
                    $data['status'] = false;
                }

            } else {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }

    /*29.01.2018*/
    public function carihesapkopyala()
    {
        $data = [];
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $old_order = $this->db->select('id,caridata,cariadresdata_sevk,cariadresdata_fatura')->from('siparisler')->where('id', $pdata['old_id'])->get()->row();
            $new_order_updates = [];
            $new_order_updates['caridata'] = $old_order->caridata;
            //$new_order_updates['cariadresdata_sevk'] = $old_order->cariadresdata_sevk;
            //$new_order_updates['cariadresdata_fatura'] = $old_order->cariadresdata_fatura;
            $new_order = $this->db->where('id', $pdata['new_id'])->update('siparisler', $new_order_updates);
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }

    public function carihesapkopyalawa()
    {
        $data = [];
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $old_order = $this->db->select('id,caridata,cariadresdata_sevk,cariadresdata_fatura')->from('siparisler')->where('id', $pdata['old_id'])->get()->row();
            $new_order_updates = [];
            $new_order_updates['caridata'] = $old_order->caridata;
            $new_order_updates['cariadresdata_sevk'] = $old_order->cariadresdata_sevk;
            $new_order_updates['cariadresdata_fatura'] = $old_order->cariadresdata_fatura;
            $new_order = $this->db->where('id', $pdata['new_id'])->update('siparisler', $new_order_updates);
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }

    public function normalize_str($str)
    {
        return str_replace(['ü', 'Ü', 'ö', 'Ö', 'ç', 'Ç', 'ğ', 'Ğ', 'Ş', 'ş', 'İ', 'ı'], ['u', 'u', 'o', 'o', 'c', 'c', 'g', 'G', 's', 's', 'i', 'i'], $str);
    }

    public function urun_talep_toggle()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $res = $this->orders->uruntalebi_toggle($pdata);
            echo $res;
        }
    }

    public function iletisim_talebine_cevir($talep_id)
    {
        $update = $this->db->where('id', $talep_id)->update('iletisim_istekleri', ['kategori' => 'Bilgi']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function upstest($siparis_id)
    {
        $order = $this->orders->orders(array('id' => $siparis_id), true);
        $depo_data = [];
        $depo_data['4']['tedarikci_id'] = 15;
        $depo_data['4']['id'] = 13;
        $order_urunler = json_decode($order->urunler);
        foreach ($order_urunler as $key => $urun) {
            $depo_data['4']['products'][] = $urun->stok_kodu;
        }
        $order->depo_data = json_encode($depo_data);
        ini_set('display_errors', 1);
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        try {
            $this->load->library('Entegrasyonups');
        } catch (\Exception $e) {
            echo $e->getMessage();
            die();
        }
        $check = $this->entegrasyonups->processOrder($order);
        print_r($check);
    }

    public function siparisbarcodecreate()
    {
        ini_set('display_errors', 1);
        ini_set('max_execution_time', 500);
        try {
            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $pdata = $pdata['data'];
                $siparis_id = $pdata[0]['siparis_no'];
                $depo_data = [];
                $order = $this->orders->orders(array('id' => $siparis_id), true);
                $order_urunler = json_decode($order->urunler);
                foreach ($pdata as $key => $data_line) {
                    $depo_db = $this->db->select('*')->from('tedarikci_depolar')->where('id', $data_line['depo'])->get()->row();
                    if (!$depo_db) {
                        if($data_line['depo'] == '-1'){
                            unset($pdata[$key]);
                            continue;
                        }
                        echo 'Depo bulunamadı';
                        return;
                    }
                    $depo_data[$data_line['depo']]['id'] = $depo_db->id;
                    $depo_data[$data_line['depo']]['tedarikci_id'] = $depo_db->tedarikci_id;;
                    foreach ($order_urunler as $key_2 => $order_line) {
                        $urun_stok_kodu = $order_line->stok_kodu;
                        if ($urun_stok_kodu == $data_line['stok_kodu']) {
                            $depo_data[$data_line['depo']]['products'][] = $urun_stok_kodu;
                        }
                    }
                }
                $pdata = array_values($pdata);
                if ($pdata[0]['kargo_firma'] == 'CEVA') {
                    $this->load->library('Entegrasyonceva');
                    $order->depo_data = json_encode($depo_data);
                    $check = $this->entegrasyonceva->processOrder($order);
                    $this->siparisbarkodgonder($siparis_id, 'CEVA');
                } elseif ($pdata[0]['kargo_firma'] == 'MNG') {
                    $this->load->library('Entegrasyonmng');
                    $order->depo_data = json_encode($depo_data);
                    $check = $this->entegrasyonmng->processOrder($order);
                    //$this->siparisbarkodgonder($siparis_id,'CEVA');
                } else {
                    $this->load->library('Entegrasyonups');
                    $order->depo_data = json_encode($depo_data);
                    $check = $this->entegrasyonups->processOrder($order);
                    //$this->siparisbarkodgonder($siparis_id, 'UPS');
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function siparisbarkodgonder($siparis_id, $firma)
    {
        $order = $this->orders->orders(array('id' => $siparis_id), true);
        $barcodes = json_decode($order->ups_barcode);
        $all_images = json_decode($order->ups_image);
        foreach ($barcodes as $key => $barcode_no) {
            $mail_data = [];
            $mail_data['order'] = $order;
            $mail_data['barcode_no'] = $barcode_no;
            $mail_data['depo_id'] = $key;
            $mail_data['depo'] = $this->db->select('*')->from('tedarikci_depolar td')->join('tedarikciler tc', 'tc.id = td.tedarikci_id', 'left')->where('td.id', $key)->get()->row();
            $mail_data['images'] = [];
            foreach ($all_images as $key_2 => $shipment_images) {
                if ($key == $key_2) {
                    if (is_array($shipment_images->string)) {
                        foreach ($shipment_images->string as $key_3 => $shipment_image) {
                            $mail_data['images'][] = $shipment_image;
                        }
                    } else {
                        $mail_data['images'][] = $shipment_images->string;
                    }
                }
            }
            $this->orders->parseBarkodMail($mail_data, $firma);
        }

    }

    public function siparisbarcode($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $urunler = json_decode($order->urunler);
        if (strlen($order->caridata) < 1 || strlen($order->cariadresdata_sevk) < 1) {
            echo 'Cari bilgisi oluşturulmamış. Barkod oluşturulamaz.';
            die();
        }

        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adresi = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adresi->no;
        $this->load->library('lastikcim');
        $adres = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        $cari = $this->lastikcim->mevcutCariyiGetirId($cari_kod);


        $mikro_data = array_merge($adres, $cari);
        $il_mikro = explode('/', $mikro_data['il_ilce'])[1];
        $data = array();
        $data['musteri'] = $mikro_data;
        foreach ($urunler as $key => $urun) {
            $urun->ekstra_msg = [];
            if ($il_mikro == "İSTANBUL") {
                $urun->ekstra_msg[] = "İSTANBUL içerisinde barkod oluşturmaktasınız.";
            }
            if (isset($urun->tedarikci)) {
                if (strlen($urun->tedarikci) > 0) {
                    $tedarikci_data = $this->db->select('*')->from('tedarikciler')->where('kod', $urun->tedarikci)->get()->row();
                    if (!$tedarikci_data) {
                        $urun->ekstra_msg[] = $urun->tedarikci . ' gönderilebilir tedarikçi listesinde değil. Bazı ürünler için farklı kod oluşturmak istiyorsanız depoyu MANUEL seçmeniz gerekmektedir.';
                    } else {
                        $tedarikci_depolar = $this->db->select('*')->from('tedarikci_depolar')->where('tedarikci_id', $tedarikci_data->id)->get()->result();
                        if (!$tedarikci_depolar) {
                            $urun->ekstra_msg[] = $urun->tedarikci . ' için tanımlı depo adresi bulunmuyor. Gönderim deposunu MANUEL olarak seçiniz.';
                        } else {
                            $urunler[$key]->depo_data = $tedarikci_depolar;
                            $urunler[$key]->tedarikci_data = ($tedarikci_data);
                        }
                    }
                }
            } else {
                $urun->ekstra_msg[] = 'Üründe belirlenmiş bir tedarikçi bulunmuyor bütün depolar gösterilecek.';
            }
        }
        $tum_tedarikciler = $this->db->select('*')->from('tedarikciler')->get()->result();
        $tum_depolar = $this->db->select('*')->from('tedarikci_depolar')->get()->result();
        $data['tum_tedarikciler'] = $tum_tedarikciler;
        $data['tum_depolar'] = $tum_depolar;
        $data['order'] = $order;
        $data['urunler'] = $urunler;
        $this->parser->parse("tedarikci/barkodolustur", $data);
    }

    public function unarrivedpackages()
    {
        $time = time() - 10000000;
        $siparisler = $this->db->select('id')->from('islenecek_siparisler')->get()->result();
        $changed = 0;
        foreach ($siparisler as $key => $siparis) {
            $result = $this->upspackagestatus($siparis->id);
            if ($result == true) {
                $changed++;
            }
        }
        echo $changed . ' sipariş güncellendi';
    }


    public function upspackagestatus($order_id)
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $order = $this->orders->orders(array('id' => $order_id), true);
        $barcodes = json_decode($order->ups_barcode);
        if (is_array($barcodes)) {
            foreach ($barcodes as $key => $barcode) {
                return $this->checkBarcodeStatus($barcode, $order->durumu, $order->id);
            }
        } else {
            return $this->checkBarcodeStatus($barcodes, $order->durumu, $order->id);
        }
    }

    public function checkBarcodeStatusCeva($barcode, $durumu, $order_id)
    {
        /*$barcode = (array)$barcode;
        $barcode = array_values($barcode)[0];
        $this->load->library('Entegrasyoncevastatus');
        $result = $this->entegrasyonupsstatus->checkBarcodeStatus($barcode);
        $barcode_status = 8;
        if ($result['COMPLETE']) {
            $barcode_status = 5;
        } else if ($result['CANCELED']) {
            $barcode_status = 9;
        } else if ($result['HAREKET']) {
            $barcode_status = 3;
        } else {
            $barcode_status = 8;
        }
        $data = [];
        $data['order_id'] = $order_id;
        $data['durum'] = $barcode_status;
        $data['eski_durum'] = $durumu;
        if ($durumu == $barcode_status) {
            echo $order_id . ' Already Set';
        } else if (in_array($durumu, [8, 2]) && $barcode_status == 3) {
            $this->orders->durum_degis($data);
            return true;
        } else if (in_array($durumu, [8, 2, 3]) && $barcode_status == 5) {
            $this->orders->durum_degis($data);
            return true;
        }
        return false;*/
    }

    public function showBarcodeStatus($barcode)
    {
        $barcode = (array)$barcode;
        $barcode = array_values($barcode)[0];
        $this->load->library('Entegrasyonupsstatus');
        $result = $this->entegrasyonupsstatus->checkBarcodeStatus($barcode);
        print_r($result);
    }

    public function cevaunarrivedpackages()
    {
        error_reporting(E_ERROR);
        $time = time() - 1296000;
        $orders = $this->db->select('order_number,sip_no,shipment_order_number,tarih,durumu')->from('monitor_ceva')->where('tarih >', $time)->order_by('tarih')->get()->result();
        $changed = 0;
        $total = 0;
        foreach ($orders as $order) {
            $result = $this->showCevaOrderStatus($order->order_number, $order->shipment_order_number, $order->durumu, $order->sip_no);
            if ($result == true) {
                $changed++;
            }
            $total++;
        }
        echo $changed . ' sipariş güncellendi. Toplam Sipariş : ' . $total;
    }

    public function showCevaOrderStatus($order_id, $orderNumber, $durumu, $sip_no)
    {

        $this->load->library('Entegrasyoncevastatus');
        $result = $this->entegrasyoncevastatus->checkOrderStatus($orderNumber);
        if ($result) {
            if ($result == 'COMPLETE') {
                $barcode_status = 5;
            } else if ($result == 'CANCELED') {
                $barcode_status = 9;
            } else if ($result == 'HAREKET') {
                $barcode_status = 3;
            } else {
                $barcode_status = 8;
            }
        } else {
            echo $order_id . ' Result Not Found';
            echo '<hr>';
            return;
        }
        $data = [];
        $data['order_id'] = $order_id;
        $data['durum'] = $barcode_status;
        $data['eski_durum'] = $durumu;
        if ($durumu == $barcode_status) {
            echo $order_id . ' Already Set';
        } else if (in_array($durumu, [8, 2]) && $barcode_status == 3) {
            //echo 'Sip:'.$order_id.'|||||||Degis:'.$durumu.':'.$barcode_status;
            //echo '<hr>';
            $this->orders->durum_degis($data);
            return true;
        } else if (in_array($durumu, [8, 2, 3]) && $barcode_status == 5) {
            //echo 'Sip:'.$order_id.'|||||||Degis:'.$durumu.':'.$barcode_status;
            //echo '<hr>';
            $this->orders->durum_degis($data);
            return true;
        }
        return false;
    }

    public function cevaShipmentInfo($order_number)
    {
        return 0;
        ini_set('display_errors', 0);
        $orders = $this->db->select('order_number,shipment_order_number,tarih,durumu')->where('order_number', $order_number)->from('monitor_ceva')->order_by('tarih')->get()->row();
        if ($orders) {
            $order_number = $orders->shipment_order_number;
            //echo json_encode($orders);
            $this->load->library('Entegrasyoncevastatus');
            $result = $this->entegrasyoncevastatus->returnShipmentInfo($order_number);
            $data['result'] = $result;
            $this->parser->parse("tedarikci/kargoakis", $data);
        }
    }

    public function createWaybillPdf()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $pdata = $pdata['data'];
            $tedarikciler = [];
            $siparis_no = null;
            foreach ($pdata as $key => $post_item) {
                $siparis_no = $post_item['siparis_no'];
                $tedarikci = $post_item['tedarikci'];
                if (isset($tedarikciler[$tedarikci])) {
                    if ($tedarikciler[$tedarikci]['musteri_adres_sevk'] == $post_item['musteri_adres_sevk']) {
                        $tedarikciler[$tedarikci]['urunler'][] = ['urun_adi' => $post_item['urun_adi'], 'stok_kodu' => $post_item['stok_kodu'], 'urun_miktar' => $post_item['urun_miktar']];
                    } else {
                        $un_tedarikci_id = $tedarikci . '_' . uniqid();
                        $tedarikciler[$un_tedarikci_id] = $post_item;
                        $tedarikciler[$un_tedarikci_id]['urunler'][] = ['urun_adi' => $post_item['urun_adi'], 'stok_kodu' => $post_item['stok_kodu'], 'urun_miktar' => $post_item['urun_miktar']];
                    }
                } else {
                    $tedarikciler[$tedarikci] = $post_item;
                    $tedarikciler[$tedarikci]['urunler'][] = ['urun_adi' => $post_item['urun_adi'], 'stok_kodu' => $post_item['stok_kodu'], 'urun_miktar' => $post_item['urun_miktar']];
                }
            }
            foreach ($tedarikciler as $key => $tedarikci) {
                $send_irsaliye_mail = $this->orders->createWaybillPdfAndSave($tedarikci);
            }
            $result = [];
            $result['status'] = 'true';
            $result['type'] = $send_irsaliye_mail;
            $this->db->where('id', $siparis_no)->update('siparisler', ['irsaliye_data' => json_encode($tedarikciler)]);
            echo json_encode($result);
        }
    }

    public function siparisirsaliye($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $urunler = json_decode($order->urunler);
        if (strlen($order->caridata) < 1 || strlen($order->cariadresdata_sevk) < 1) {
            echo 'Cari bilgisi oluşturulmamış. İrsaliye oluşturulamaz.';
            die();
        }
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adresi = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adresi->no;
        $fatura_adresi = json_decode($order->cariadresdata_fatura);
        $fatura_adres_no = $fatura_adresi->no;
        $this->load->library('lastikcim');
        $adres = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        $fatura = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $fatura_adres_no);
        $cari = $this->lastikcim->mevcutCariyiGetirId($cari_kod);
        $fatura_data = array_merge($fatura, $cari);
        $mikro_data = array_merge($adres, $cari);
        $data['musteri'] = $mikro_data;
        $data['fatura'] = $fatura_data;
        $data['order'] = $order;
        $data['urunler'] = $urunler;
        $tum_tedarikciler = $this->db->select('*')->from('tedarikciler')->get()->result();
        $tum_depolar = $this->db->select('*')->from('tedarikci_depolar')->get()->result();
        $data['tum_tedarikciler'] = $tum_tedarikciler;
        $data['tum_depolar'] = $tum_depolar;
        $this->parser->parse("tedarikci/irsaliyegonder", $data);
    }

    public function montajDataKaydet()
    {
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $this->orders->montajDataOlustur($pdata['data']);
            $result = [];
            $result['status'] = 'true';
            $result['type'] = 'success';

        } else {
            $result = [];
            $result['status'] = 'false';
            $result['type'] = 'error';
        }
        echo json_encode($result);
    }

    public function sendMontajMail($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $this->orders->sendMontajMail($order);
        $result = [];
        $result['status'] = 'true';
        $result['type'] = 'success';
        echo json_encode($result);
    }

    public function createTahsilat($id)
    {
        $siparis = $this->db->where('id', $id)->select('*')->from('siparisler')->get()->row();
        if ($siparis) {
            $odeme_data = json_decode($siparis->odemedata);
            $hizmet_kodu = '';
            $odeme_bilgisi = [];
            $tahsilat_sira_no = $this->db->select('*')->from('ayarlar')->where('name', 'odeme_fisi_no')->get()->row();
            $tahsilat_sira_no = $tahsilat_sira_no->value;
            if (isset($odeme_data->banka)) {
                $banka = $this->db->select('*')->from('banka_bilgileri')->where('id', $odeme_data->banka)->get()->row();
                $odeme_bilgisi['hizmet_kodu'] = $banka->banka_hizmet_kodu;
                $odeme_bilgisi['baslangic'] = 0;
                $odeme_bilgisi['aralik'] = 0;
                $odeme_bilgisi['taksit'] = 1;
                $odeme_bilgisi['tutar'] = (float)$odeme_data->son_fiyat;
            } elseif (isset($odeme_data->sanalpos)) {
                $pos = $this->db->select('*')->from('sanalposlar')->where('id', $odeme_data->sanalpos)->get()->row();
                $posdata = json_decode($pos->data);
                $odeme_bilgisi['hizmet_kodu'] = $posdata->banka_hizmet_kodu;
                $odeme_bilgisi['baslangic'] = $posdata->vade_bas_gun;
                $odeme_bilgisi['aralik'] = $posdata->vade_aralik;
                $odeme_bilgisi['taksit'] = $odeme_data->installment;
                if ($odeme_bilgisi['aralik'] == 0 || $odeme_bilgisi['taksit'] == 0) {
                    $odeme_bilgisi['taksit'] = 1;
                }
                $odeme_bilgisi['tutar'] = (float)$odeme_data->son_fiyat;
            } else {
                echo 'ERROR: BANKA HİZMET KODU HATASI';
            }
            $this->load->library('lastikcim');
            $tahsilat = $this->lastikcim->odemeVerisiGir($siparis, $tahsilat_sira_no, $odeme_bilgisi);
            $tahsilat_sira_no = $tahsilat_sira_no + 1;
            $tahsilat_sira_no = $this->db->where('name', 'odeme_fisi_no')->update('ayarlar', ['value' => $tahsilat_sira_no]);
        }
    }

    public function castrolPostOrder($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $urunler = json_decode($order->urunler);
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adresi = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adresi->no;
        $fatura_adresi = json_decode($order->cariadresdata_fatura);
        $fatura_adres_no = $fatura_adresi->no;
        $this->load->library('lastikcim');
        $sevk = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        $fatura = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $fatura_adres_no);
        $cari = $this->lastikcim->mevcutCariyiGetirId($cari_kod);
        $sevk_ilce = explode('/', $sevk['il_ilce'])[0];
        $sevk_il = explode('/', $sevk['il_ilce'])[1];
        $find_il = $this->db->where('il', $sevk_il)->from('iller')->get()->row();
        $find_il_adi = $find_il->il;
        $find_ilce = $this->db->where('il_id', $find_il->id)->where('ilce', $sevk_ilce)->from('ilceler')->get()->row();
        $find_ilce_adi = $find_ilce->ilce;
        $sevk['il'] = $find_il_adi;
        $sevk['ilce'] = $find_ilce_adi;


        $fatura_ilce = explode('/', $fatura['il_ilce'])[0];
        $fatura_il = explode('/', $fatura['il_ilce'])[1];
        $find_il = $this->db->where('il', $fatura_il)->from('iller')->get()->row();
        $find_il_adi = $find_il->il;
        $find_ilce = $this->db->where('il_id', $find_il->id)->where('ilce', $fatura_ilce)->from('ilceler')->get()->row();
        $find_ilce_adi = $find_ilce->ilce;
        $fatura['il'] = $find_il_adi;
        $fatura['ilce'] = $find_ilce_adi;


        $post_values = [];
        $post_values['auth'] = ['userName' => 'transferLastikcim', 'password' => 'L123-Tik'];
        $post_values['id'] = 'lstkcm-' . $id;
        $post_values['orderNumber'] = 'lstkcm-' . $id;
        $post_values['buyer'] = [];
        if ($cari['vdaire_adi']) {
            $post_values['buyer']['id'] = $id;
            $post_values['buyer']['fullName'] = $cari['cari_unvan1'];
            $post_values['buyer']['taxId'] = $cari['vdaire_no'];
            $post_values['buyer']['taxOffice'] = $cari['vdaire_adi'];
            $post_values['buyer']['email'] = $cari['email'];
            $post_values['buyer']['tcId'] = '';
        } else {
            $post_values['buyer']['id'] = $id;
            $post_values['buyer']['fullName'] = $cari['cari_unvan1'];
            $post_values['buyer']['taxId'] = '';
            $post_values['buyer']['taxOffice'] = '';
            $post_values['buyer']['email'] = $cari['email'];
            $post_values['buyer']['tcId'] = $cari['vdaire_no'];
        }
        $post_values['itemList'] = [];
        foreach ($urunler as $urun_line) {
            if ($urun_line->tedarikci == "CASTROL") {
                $item = [];
                $item['productId'] = $urun_line->id;
                $item['deliveryFeeType'] = "2";
                $item['productSellerCode'] = $urun_line->stok_kodu;
                $item['dueAmount'] = round($urun_line->fiyat * (100 - $urun_line->indirim_orani) / 100, 2);
                $item['installmentChargeWithVAT'] = 0;
                $item['price'] = (float)$urun_line->liste_fiyati;
                $item['totalMallDiscountPrice'] = $item['price'] - $item['dueAmount'];
                $item['quantity'] = (int)$urun_line->miktar;
                $item['attributes'] = [['name' => 'tip', 'value' => 'yag']];
                $item['commission'] = 0;
                $item['productName'] = $urun_line->urun_adi;
                $post_values['itemList'][] = $item;
            }
        }
        $post_values['billingAddress'] = [];
        $post_values['billingAddress']['address'] = $fatura['tam_adres'];
        $post_values['billingAddress']['fullName'] = $cari['cari_unvan1'];
        $post_values['billingAddress']['city'] = $fatura['il'];
        $post_values['billingAddress']['district'] = $fatura['ilce'];
        $post_values['billingAddress']['neighborhood'] = '';
        $post_values['billingAddress']['postalCode'] = $fatura['posta_kodu'];
        $post_values['billingAddress']['gsm'] = $fatura['telefon'];
        if ($cari['vdaire_adi']) {
            $post_values['billingAddress']['tcId'] = '';
            $post_values['billingAddress']['taxId'] = $cari['vdaire_no'];
            $post_values['billingAddress']['taxHouse'] = $cari['vdaire_adi'];
        } else {
            $post_values['billingAddress']['tcId'] = $cari['vdaire_no'];
            $post_values['billingAddress']['taxId'] = '';
            $post_values['billingAddress']['taxHouse'] = '';
        }
        $post_values['shippingAddress'] = [];
        $sevk_il_ilce = explode('/', $sevk['il_ilce']);
        $post_values['shippingAddress']['address'] = $sevk['tam_adres'];
        $post_values['shippingAddress']['fullName'] = $cari['cari_unvan1'];
        $post_values['shippingAddress']['city'] = $sevk['il'];
        $post_values['shippingAddress']['district'] = $sevk['ilce'];
        $post_values['shippingAddress']['neighborhood'] = '';
        $post_values['shippingAddress']['postalCode'] = $sevk['posta_kodu'];
        $post_values['shippingAddress']['gsm'] = $sevk['telefon'];
        if ($cari['vdaire_adi']) {
            $post_values['shippingAddress']['tcId'] = '';
            $post_values['shippingAddress']['taxId'] = $cari['vdaire_no'];
            $post_values['shippingAddress']['taxHouse'] = $cari['vdaire_adi'];
        } else {
            $post_values['shippingAddress']['tcId'] = $cari['vdaire_no'];
            $post_values['shippingAddress']['taxId'] = '';
            $post_values['shippingAddress']['taxHouse'] = '';
        }
        $post_values['createDate'] = gmdate("d-m-Y H:i", $order->tarih);
        $post_json = json_encode($post_values, JSON_UNESCAPED_UNICODE);
        $ch = curl_init('http://beta.rutofis.saha724.com/SahaBridgeAPI/api/Order/Post');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post_json))
        );
        $result = curl_exec($ch);
        echo $result;
    }

    public function createInjection($id)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $urunler = json_decode($order->urunler);
        $cari_data = json_decode($order->caridata);
        $cari_kod = $cari_data->kod;
        $sevk_adresi = json_decode($order->cariadresdata_sevk);
        $sevk_adres_no = $sevk_adresi->no;
        $fatura_adresi = json_decode($order->cariadresdata_fatura);
        $fatura_adres_no = $fatura_adresi->no;
        $this->load->library('lastikcim');
        $sevk = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $sevk_adres_no);
        $fatura = $this->lastikcim->mevcutAdresiGetirId($cari_kod, $fatura_adres_no);
        $cari = $this->lastikcim->mevcutCariyiGetirId($cari_kod);
        $sevk_ilce = explode('/', $sevk['il_ilce'])[0];
        $sevk_il = explode('/', $sevk['il_ilce'])[1];
        $find_il = $this->db->where('il', $sevk_il)->from('iller')->get()->row();
        $find_il_adi = $find_il->il;
        $find_ilce = $this->db->where('il_id', $find_il->id)->where('ilce', $sevk_ilce)->from('ilceler')->get()->row();
        $find_ilce_adi = $find_ilce->ilce;
        $sevk['il'] = $find_il_adi;
        $sevk['ilce'] = $find_ilce_adi;

        $fatura_ilce = explode('/', $fatura['il_ilce'])[0];
        $fatura_il = explode('/', $fatura['il_ilce'])[1];
        $find_il = $this->db->where('il', $fatura_il)->from('iller')->get()->row();
        $find_il_adi = $find_il->il;
        $find_ilce = $this->db->where('il_id', $find_il->id)->where('ilce', $fatura_ilce)->from('ilceler')->get()->row();
        $find_ilce_adi = $find_ilce->ilce;
        $fatura['il'] = $find_il_adi;
        $fatura['ilce'] = $find_ilce_adi;
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        #ORDER DEFINING 
        $this->load->library('lastikcrm');
        $definedOrder = $this->lastikcrm->defineOrder($order, $fatura, $sevk, $cari);

    }

    public function barcodeDataFromResult($siparis_id)
    {
        $order = $this->orders->orders(array('id' => $siparis_id), true);
        $this->load->library('entegrasyonceva');
        $this->entegrasyonceva->parseFromUpsResult($siparis_id, $order->ups_result);
    }

    public function sendMusteriPointSms($siparis_id){
        $order = $this->orders->orders(array('id' => $siparis_id), true);
        $this->orders->musteri_montaj_sms($order);
    }
}

?>
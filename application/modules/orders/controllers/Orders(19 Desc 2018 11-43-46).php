<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller{


  public $theme_url;
  public function __construct(){
    parent::__construct();
    $this->theme_url = $this->parser->theme_url();
    $this->load->model('Orders_model', 'orders');
    /*
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    */
}

public function testemre($posid, $taksit){
    $this->orders->posArtiTaksit($posid,$taksit);
}

public function index_eski(){      
    $data = array();
    $data['theme_url']      = $this->theme_url;   
    $data['orders']         = $this->orders->orders(); 
    $data['durumlar']       = $this->orders->durumlar();       
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("index", $data);
}
/**/
public function index() {      
    $data                   = array();

    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        foreach ($pdata as $key => $value) {
            if (is_null($value) || strlen($value) < 1) {
                unset($pdata[$key]);
            }
        }
        
        if (count($pdata) > 0) {
            $surl = ($this->uri->assoc_to_uri($pdata));
            $surl = site_url('orders/index/'.$surl);
            redirect($surl);
            exit();
        }
    }

    $sdata = $this->uri->uri_to_assoc();

    /**/
	
	
    if (!isset($sdata['isim']) && !isset($sdata['id']) && !isset($sdata['pos_tip']) && !isset($sdata['odeme_tipi']) && !isset($sdata['durum_odeme']) && !isset($sdata['durumu']) ) {
		if (!isset($sdata['tarih_min'])) {
			$sdata['tarih_min'] = date_create(date('Y-m-d'))->modify('-3 days')->format('d.m.Y');
		}
		
		if (!isset($sdata['tarih_max'])) {
			$sdata['tarih_max'] = date('d.m.Y');
		}
	}
	if(isset($sdata['id'])){
		unset($sdata['tarih_min']);
		unset($sdata['tarih_max']);
	}
    if (!isset($sdata['durumu'])) {
    	$sdata['durumu'] = "hatasizlar";
    }
    /**/

    /**/
    $sdatap = $sdata;
    unset($sdatap['page']);
    $pagination_url = site_url('orders/index/'.$this->uri->assoc_to_uri($sdatap));
    /**/
    if (isset($sdata['limit']) || !is_numeric($sdata['limit'])) {
        $sdata['limit'] = 30;
    }
    
    if (isset($sdata['isim'])) {
        $sdata['isim'] = urldecode($sdata['isim']);
    }

    $tpdata = $sdata;
    $tpdata['toplam'] = 1;

    $data['sdata']          = $sdata;
    $data['theme_url']      = $this->theme_url;
    $data['orders']         = $this->orders->orders($sdata);
    
    $last_uri = $this->uri->total_segments();
    $last_uri = $last_uri;
    if ($sdata['page']) {
        $last_uri = $last_uri;
    } else {
        $last_uri = $last_uri + 2;
    }
    /**/
    $config = array();
    $config["base_url"]         = $pagination_url;
    $config["total_rows"]       = $this->orders->orders($tpdata, true)->toplam;
    $config["cur_page"]         = $sdata['page'];
    $config["per_page"]         = $sdata['limit'];
    $config['prefix']           = "page/";
    $config['use_page_numbers'] = true;
    $config['num_links']        = 5;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_tag_open']   = '<li class="page-first">';
    $config['first_tag_close']  = '</li>';
    $config['last_tag_open']    = '<li class="page-last">';
    $config['last_tag_close']   = '</li>';
    $config['prev_tag_open']    = '<li class="page-pre">';
    $config['prev_tag_close']   = '</li>';
    $config['next_tag_open']    = '<li class="page-next">';
    $config['next_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="page-number active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-number">';
    $config['num_tag_close']    = '</li>';
    $config['last_link']        = "Son Sayfa";
    $config['first_link']       = "İlk Sayfa";
    $config['uri_segment']      = $last_uri;
    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $data["links"]          = $this->pagination->create_links();
    /**/

    $data['durumlar']       = $this->orders->durumlar();
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("index", $data);
}

/*Hatalılar*/
public function hatalilar() {      
    $data                   = array();

    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        foreach ($pdata as $key => $value) {
            if (is_null($value) || strlen($value) < 1) {
                unset($pdata[$key]);
            }
        }
        
        if (count($pdata) > 0) {
            $surl = ($this->uri->assoc_to_uri($pdata));
            $surl = site_url('orders/hatalilar/'.$surl);
            redirect($surl);
            exit();
        }
    }

    $sdata = $this->uri->uri_to_assoc();

    /**/
    if (!isset($sdata['durumu'])) {
    	$sdata['durumu'] = "10";
    }
    /**/

    /**/
    $sdatap = $sdata;
    unset($sdatap['page']);
    unset($sdatap['durumu']);
    $pagination_url = site_url('orders/hatalilar/'.$this->uri->assoc_to_uri($sdatap));
    /**/
    if (isset($sdata['limit']) || !is_numeric($sdata['limit'])) {
        $sdata['limit'] = 30;
    }
    
    if (isset($sdata['isim'])) {
        $sdata['isim'] = $this->normalize_str(urldecode($sdata['isim']));
    }

    $tpdata = $sdata;
    $tpdata['toplam'] = 1;

    $data['sdata']          = $sdata;
    $data['theme_url']      = $this->theme_url;
    $data['orders']         = $this->orders->orders($sdata);
    
    $last_uri = $this->uri->total_segments();
    $last_uri = $last_uri;
    if ($sdata['page']) {
        $last_uri = $last_uri;
    } else {
        $last_uri = $last_uri + 2;
    }
    /**/
    $config = array();
    $config["base_url"]         = $pagination_url;
    $config["total_rows"]       = $this->orders->orders($tpdata, true)->toplam;
    $config["cur_page"]         = $sdata['page'];
    $config["per_page"]         = $sdata['limit'];
    $config['prefix']           = "page/";
    $config['use_page_numbers'] = true;
    $config['num_links']        = 5;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_tag_open']   = '<li class="page-first">';
    $config['first_tag_close']  = '</li>';
    $config['last_tag_open']    = '<li class="page-last">';
    $config['last_tag_close']   = '</li>';
    $config['prev_tag_open']    = '<li class="page-pre">';
    $config['prev_tag_close']   = '</li>';
    $config['next_tag_open']    = '<li class="page-next">';
    $config['next_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="page-number active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-number">';
    $config['num_tag_close']    = '</li>';
    $config['last_link']        = "Son Sayfa";
    $config['first_link']       = "İlk Sayfa";
    $config['uri_segment']      = $last_uri;
    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $data["links"]          = $this->pagination->create_links();
    /**/

    $data['durumlar']       = $this->orders->durumlar();
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("index", $data);
}
/*Hatalılar*/

public function siparis_detaylari($id) {
    $data = array();
    $data['theme_url']      = $this->theme_url;   
    $data['order']          = $this->orders->orders(array('id'=>$id),true); 
    $data['durumlar']       = $this->orders->durumlar();       
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("modal/siparis_detaylari", $data);
}

public function fatura_bilgileri($id) {
    $data = array();
    $data['theme_url']      = $this->theme_url;   
    $data['order']          = $this->orders->orders(array('id'=>$id),true); 
    $data['durumlar']       = $this->orders->durumlar();       
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("modal/fatura_bilgileri", $data);
}

public function kargo_detaylari($id = NULL)
{
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        $kargores = $this->orders->kargogir($id, $pdata);
        $data = array();
        if ($kargores) {
            $data['message'] = "Kargo Numarası Girildi";
            $data['status']  = "success";
        } else {
            $data['message'] = "Hata Oldu, Tekrar Deneyiniz";
            $data['status']  = "danger";
        }
        echo json_encode($data);
    } else {
        $data = array();
        $data['theme_url']      = $this->theme_url;   
        $data['order']          = $this->orders->orders(array('id'=>$id),true); 
        $data['durumlar']       = $this->orders->durumlar();       
        $data['kargofirmalar']  = $this->orders->kargofirmalar();
        $this->parser->parse("modal/kargo_detay", $data);
    }
}

public function siparis_notu($id = NULL)
{
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        $kargores = $this->orders->siparisnotugir($id, $pdata);
        $data = array();
        if ($kargores) {
            $data['message'] = "Sipariş Notu Güncellendi";
            $data['status']  = "success";
        } else {
            $data['message'] = "Hata Oldu, Tekrar Deneyiniz";
            $data['status']  = "danger";
        }
        echo json_encode($data);
    } else {
        $data = array();
        $data['theme_url']      = $this->theme_url;   
        $data['order']          = $this->orders->orders(array('id'=>$id),true); 
        $data['durumlar']       = $this->orders->durumlar();       
        $data['kargofirmalar']  = $this->orders->kargofirmalar();
        $this->parser->parse("modal/siparis_notu", $data);
    }
}

public function montaj_detay($id = NULL)
{
    $data = array();
    $data['theme_url']      = $this->theme_url;   
    $data['order']          = $this->orders->orders(array('id'=>$id),true); 
    $data['durumlar']       = $this->orders->durumlar();       
    $data['kargofirmalar']  = $this->orders->kargofirmalar();
    $this->parser->parse("modal/montaj_detay", $data);
}
/**/
public function siparisdetay($id){
    $data = array();
    $data['theme_url'] = $this->theme_url;   
    $data['order'] = $this->orders->orders(array('id'=>$id),true); 
    $data['durumlar'] = $this->orders->durumlar();       
    $data['kargofirmalar'] = $this->orders->kargofirmalar();   
    $this->parser->parse("siparisdetay", $data);
}

public function odemebildirim(){  
    $data = array();
    $data['theme_url'] = $this->theme_url;
    $data['bildirimler'] = $this->orders->odemebildirimler();  
    $data['durumlar'] = $this->orders->durumlar();        
    $this->parser->parse("odemebildirim", $data);
    
}
public function iptaliade(){  
    $data = array();
    $data['theme_url'] = $this->theme_url;   
    $data['orders'] = $this->orders->orders(array('durumu'=>11)); 
    $data['durumlar'] = $this->orders->durumlar();       
    $data['kargofirmalar'] = $this->orders->kargofirmalar();          
    $this->parser->parse("iptaliade", $data);
    
}


public function uruntalepleri(){  
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
		
        foreach ($pdata as $key => $value) {
            if (is_null($value) || strlen($value) < 1) {
                unset($pdata[$key]);
            }
        }
        
        if (count($pdata) > 0) {
            $surl = ($this->uri->assoc_to_uri($pdata));
            $surl = site_url('orders/uruntalepleri/'.$surl);
            redirect($surl);
            exit();
        }
    }
    $sdata = $this->uri->uri_to_assoc();
    $pagination_url = site_url('orders/uruntalepleri/'.$this->uri->assoc_to_uri($sdata));
	if(!$sdata['page']){
		$sdata['page'] = 1;
	}
	if(!$sdata['limit']){
		$sdata['limit'] = 30;
	}
    $data = array();
    $data['theme_url'] = $this->theme_url;   
    $data['istekler'] = $this->orders->uruntalepleri($sdata); 
	
	
	$last_uri = $this->uri->total_segments();
    $last_uri = $last_uri;
    if ($sdata['page']) {
        $last_uri = $last_uri;
    } else {
        $last_uri = $last_uri + 2;
    }
    /**/
    $config = array();
    $config["base_url"]         = $pagination_url;
    $config["total_rows"]       = $this->orders->uruntalepleri_count();
    $config["cur_page"]         = $sdata['page'];
    $config["per_page"]         = $sdata['limit'];
    $config['prefix']           = "page/";
    $config['use_page_numbers'] = true;
    $config['num_links']        = 5;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_tag_open']   = '<li class="page-first">';
    $config['first_tag_close']  = '</li>';
    $config['last_tag_open']    = '<li class="page-last">';
    $config['last_tag_close']   = '</li>';
    $config['prev_tag_open']    = '<li class="page-pre">';
    $config['prev_tag_close']   = '</li>';
    $config['next_tag_open']    = '<li class="page-next">';
    $config['next_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="page-number active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-number">';
    $config['num_tag_close']    = '</li>';
    $config['last_link']        = "Son Sayfa";
    $config['first_link']       = "İlk Sayfa";
    $config['uri_segment']      = $last_uri;
    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $data["links"] = $this->pagination->create_links();
	
	$data['sdata'] = $sdata;
    $this->parser->parse("uruntalepleri", $data);
    
}

public function iletisim_talep_update(){
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
		$res = $this->orders->uruntalepleri_update($pdata);
		return $res;
	}
}

public function edit($id){

}

    /*27.01.2018*/
    public function posdetay($id = NULL)
    {
        $sdata = array();

        if ($id && is_numeric($id)) {
            
            $sdata['id'] = $id;

            $data = array();
            $data['sdata'] = $sdata;
            $data['theme_url'] = $this->theme_url;
            $data['posdetaylar'] = $this->orders->posdetaylar($sdata, true);
            $this->parser->parse("posdetay_modal", $data); 

        } else {

            if ($this->input->method() == "post") {
                $pdata = $this->input->post();
                $sdata = $pdata;
            } else {
            	$sdata['limit'] = 100;
            }


            $data = array();
            $data['sdata'] = $sdata;
            $data['theme_url'] = $this->theme_url;
            $data['posdetaylar'] = $this->orders->posdetaylar($sdata);
            $this->parser->parse("posdetay", $data); 

        }
    }

    public function carihesapac($id = NULL)
    {
        $order = $this->orders->orders(array('id' => $id), true);
        $data = array();
        $data['order'] = $order;
        $this->parser->parse("mikro/carihesapac", $data); 
    }

    public function carisiparisac($id = NULL, $mikro = NULL)
    {
        $this->load->model('products/products_model', 'products');
        $order = $this->orders->orders(array('id' => $id), true);
        $data = array();
        $data['order'] = $order;
        $data['mikro'] = $mikro;

        $cari_data   = json_decode($order->caridata, true);
        $sevk_adresi = json_decode($order->cariadresdata_sevk, true);

        if ($cari_data['kod'] && $sevk_adresi['no'] && is_numeric($sevk_adresi['no'])) {
            $mikro_data = array();
            
            if ($mikro <> "tatko") {
                $mikro_data['cari_kod'] = $cari_data['kod'];
                $mikro_data['cari_adres'] = "".$sevk_adresi['no']."";
            }

            /**/
            $mikro_urunler = array();
            $urunler = json_decode($order->urunler);
            foreach ($urunler as $urun) {                
                $kdv_orani = $urun->kdv / 100;

                $urun_detay = $this->products->urunler(array('id' => $urun->urun), true);
                if ($urun_detay) {

                    if ($urun->kargo == 1 && $mikro <> "tatko") {

                        $mikro_urunler[] = array(
                            'stok_birim' => '0', 
                            'stok_tip' => '1', 
                            'stok_kod' => '600.02.001',
                            'stok_fiyat' => ''.$urun->kargo_bedeli.'', 
                            'adet' => ''.$urun->miktar.'', 
                            'stok_toplam' => ''.($urun->miktar * $urun->kargo_bedeli).'', 
                            'stok_vergi' => ''.(($urun->miktar * $urun->kargo_bedeli) * 0.18).''
                        );

                    }

                    if ($urun->kdv_dahil == 1) {
                        // $kdvsiz_fiyat = $urun->fiyat - ($urun->fiyat * $kdv_orani);
                        $kdvsiz_fiyat = $urun->fiyat /(1 + ($kdv_orani));
                        $mikro_urunler[] = array(
                            'stok_birim' => '1', 
                            'stok_tip' => '0', 
                            'stok_kod' => ''.$urun_detay->xml_stok_kodu.'', 
                            'stok_fiyat' => ''.$kdvsiz_fiyat.'', 
                            'adet' => ''.$urun->miktar.'', 
                            'stok_toplam' => ''.($urun->miktar * $kdvsiz_fiyat).'', 
                            'stok_vergi' => ''.(($urun->miktar * $kdvsiz_fiyat) * $kdv_orani).''
                        );

                    } else {
                        $mikro_urunler[] = array(
                            'stok_birim' => '1', 
                            'stok_tip' => '0', 
                            'stok_kod' => ''.$urun_detay->xml_stok_kodu.'', 
                            'stok_fiyat' => ''.$urun->fiyat.'', 
                            'adet' => ''.$urun->miktar.'', 
                            'stok_toplam' => ''.($urun->miktar * $urun->fiyat).'', 
                            'stok_vergi' => ''.(($urun->miktar * $urun->fiyat) * $kdv_orani).''
                        );
                    }

                    

                    if (count($urun->hizmetler) > 0 && $mikro <> "tatko") {
                        foreach ($urun->hizmetler as $hizmet) {
                            
                            $mikro_urunler[] = array(
                                'stok_birim' => '0', 
                                'stok_tip' => '1', 
                                'stok_kod' => '600.02.002',
                                'stok_fiyat' => ''.$hizmet->fiyat.'', 
                                'adet' => ''.$hizmet->adet.'', 
                                'stok_toplam' => ''.($hizmet->adet * $hizmet->fiyat).'', 
                                'stok_vergi' => ''.(($hizmet->adet * $hizmet->fiyat) * 0.18).''
                            );

                        }
                    }
                }
            }
            $mikro_data['urunler'] = $mikro_urunler;
            $mikro_data['ozel_siparis_id'] = $order->id;
            /**/

            switch ($mikro) {
                case 'tatko':
                    $this->load->library('tatkosiparis');
                    $spres = $this->tatkosiparis->siparisOlustur($mikro_data);
                break;

                case 'lastikcim':
                    $this->load->library('lastikcim');
                    $spres = $this->lastikcim->siparisOlustur($mikro_data);
                break;
                
                default:
                    $this->load->library('lastikcim');
                    $spres = $this->lastikcim->siparisOlustur($mikro_data);
                break;
            }
            
            if ($spres['siparis_id_alinan'] && is_numeric($spres['siparis_id_alinan'])) {
                if ($mikro == "tatko") {
                    $this->orders->siparismikrodata($order->id, $spres, "tatko");
                    $data['spres'] = $spres;
                } else {
                    $this->orders->siparismikrodata($order->id, $spres);
                    $data['spres'] = $spres;
                }
            }
        }

        $this->parser->parse("mikro/carisiparisac", $data); 
    }
    /*27.01.2018*/

    /*29.01.2018*/
    public function mikroyacari($siparisid = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            if ($siparisid && is_numeric($siparisid)) {

                $siparis = $this->orders->orders(array('id' => $siparisid), true);
                if ($siparis) {
                    $pdata = $this->input->post();
                    
                    $this->load->library('lastikcim');
                    $chdata = $this->lastikcim->cariHesapOlustur($pdata);

                    if ($chdata['status']) {
                        $data['status'] = $this->orders->siparismikro($siparis->id, $chdata);
                        $data['chdata'] = $chdata;
                    } else {
                        $data['status'] = false;
                        $data['chdata'] = $chdata;
                    }
                } else {
                    $data['status'] = false;
                }

            } else {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }

    public function mikroyacariadres($siparisid = NULL, $tipi = NULL)
    {
        $data = array();
        if ($this->input->method() == "post") {
            if ($siparisid && is_numeric($siparisid)) {

                $siparis = $this->orders->orders(array('id' => $siparisid), true);
                if ($siparis) {
                    $pdata = $this->input->post();
                    
                    $cari_data = json_decode($siparis->caridata, true);

                    $this->load->library('lastikcim');
                    $pdata['cari_kod'] = $cari_data['kod'];
                    $chdata = $this->lastikcim->cariAdresOlustur($pdata);

                    if ($chdata['status']) {
                        $data['status'] = $this->orders->siparismikroadres($siparis->id, $chdata, $tipi);
                        $data['chdata'] = $chdata;
                    } else {
                        $data['status'] = false;
                    }
                } else {
                    $data['status'] = false;
                }

            } else {
                $data['status'] = false;
            }
        } else {
            $data['status'] = false;
        }
        echo json_encode($data);
    }
    /*29.01.2018*/
	
	public function normalize_str($str){
		return str_replace(['ü', 'Ü', 'ö', 'Ö','ç','Ç','ğ','Ğ','Ş','ş','İ','ı'], ['u', 'u', 'o', 'o','c','c','g','G','s','s','i','i'], $str);
	}

}

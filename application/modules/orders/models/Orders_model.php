<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**/
    public function kargogir($id = NULL, $data = NULL)
    {
        if ($id && is_numeric($id) && $data) {
            $this->db->insert('takip_veri', ['dump_veri' => json_encode($data), 'tarih' => time(), 'old_veri' => $id, 'user_id' => 'kargogir']);
            foreach ($data as $key => $value) {
                if (is_null($value) || strlen($value) < 1) {
                    unset($data[$key]);
                }
            }
            $this->db->where('id', $id);
            if (count($data) > 0) {
                return $this->db->update('siparisler', array('kargo_takip' => json_encode($data)));
            } else {
                return $this->db->update('siparisler', array('kargo_takip' => NULL));
            }
        } else {
            return false;
        }
    }

    public function posArtiTaksit($posID = NULL, $taksitAdet = 0, $taksitDon = FALSE)
    {
        if (is_numeric($posID)) {
            if ($taksitAdet == 0) {
                if ($taksitDon == TRUE) {
                    return "1";
                } else {
                    echo " ( Tek Çekim ) ";
                }
            } else {
                if ($taksitAdet > 1) {
                    $taksitsp = $taksitAdet - 1;
                } else {
                    $taksitsp = $taksitAdet;
                }
                $posDetay = $this->db->query("select oranlar from sanalposlar where id = '" . $posID . "'")->row();
                if ($posDetay) {
                    $data = $posDetay->oranlar;
                    $data = json_decode($data);
                    if ($data->oran_arti[$taksitsp]) {
                        if ($taksitDon == TRUE) {
                            return $taksitAdet + $data->oran_arti[$taksitsp];
                        } else {
                            echo " ( " . $taksitAdet . " + " . $data->oran_arti[$taksitsp] . " Taksit ) ";
                        }
                    } else {
                        if ($taksitDon == TRUE) {
                            return $taksitAdet;
                        } else {
                            echo " ( " . $taksitAdet . " Taksit ) ";
                        }
                    }
                }
            }
        }
    }

    public function siparisnotugir($id = NULL, $data = NULL)
    {
        if ($id && is_numeric($id)) {

            $this->db->where('id', $id);
            if (is_null($data['siparisnotu']) || strlen($data['siparisnotu']) < 1) {
                return $this->db->update('siparisler', array('siparis_notu' => NULL));
            } else {
                return $this->db->update('siparisler', array('siparis_notu' => $data['siparisnotu']));
            }

        } else {
            return false;
        }
    }

    public function tedariknotugir($id = NULL, $data = NULL)
    {
        if ($id && is_numeric($id)) {
            $data = json_encode($data);
            $this->db->where('id', $id);
            return $this->db->update('siparisler', array('tedarik_not' => $data));
        } else {
            return false;
        }
    }

    /**/

    public function orders($data = null, $row = false)
    {
        $page = 1;
        $toplam = false;
        if (isset($data['page']) && is_numeric($data['page'])) {
            $page = $data['page'];
            unset($data['page']);
        }
        if (isset($data['toplam']) && is_numeric($data['toplam']) && $data['toplam'] == 1) {
            unset($data['toplam']);
            $toplam = true;
            $this->db->select('count(s.id) as toplam');
        } else {
            if ($row) {
                $this->db->select('s.*');
            } else {
                $select_query = 's.id, s.kargo_tipi, s.odeme_tipi, s.durumu, s.tarih, s.user_id, s.`data`, s.urunler, s.odemedata, s.siparis_notu, s.kargo_takip, s.ip, s.iade_mesaj, s.iade_sebep, s.iade_talep_tarih, s.caridata, s.cariadresdata_fatura, s.tedarikci_odeme, s.cariadresdata_sevk, s.siparis_data, s.hash_value, s.temp_on_proccess, s.temp_transfer, s.tatko_siparis_data, s.tedarik_not, s.ups_barcode, LEFT(s.ups_image,2), ups_link, LEFT(s.ups_result , 2), s.irsaliye_data, s.montaj_arac_bilgisi, s.stock_reduced';
                $this->db->select($select_query);
            }
        }
        $this->db->from('siparisler as s');
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if ($key == 'limit') {
                    if ($toplam === false) {
                        $this->db->limit($value, (($page - 1) * $value));
                    }
                } else if ($key == 'durumu') {
                    if ($value <> "hatasizlar") {
                        if(is_array($value)){
                            $this->db->where_in('s.durumu',$value);
                        }else{
                            $this->db->where('s.durumu', $value);
                        }
                    } else {
                        $this->db->where('s.durumu <> 10');
                    }
                } else if ($key == 'durum_odeme') {
                    $this->db->where('pd.durum_odeme', $value);
                } else if ($key == 'pos_tip') {
                    $this->db->where('pd.pos_tip', $value);
                } else if ($key == 'tarih_min') {
                    $this->db->where('s.tarih > ', strtotime($value . " 00:00"));
                } else if ($key == 'tarih_max') {
                    $this->db->where('s.tarih < ', strtotime($value . " 23:59"));
                } else if ($key == 'cari') {
                    $value = str_replace('"', '', $value);
                    $this->db->like('s.caridata', urldecode($value));
                } else if ($key == 'email') {
                    $value = json_encode($value);
                    $value = str_replace('"', '', $value);
                    $this->db->like('s.data', urldecode($value));
                }  else if ($key == 'isim') {
                    $value = json_encode($value);
                    $value = str_replace('"', '', $value);
                    $this->db->like('s.data', urldecode($value));
                } else {
                    $this->db->where("s." . $key, $value);
                }
            }
        }
        /*
        if ( (isset($data['durum_odeme']) || isset($data['pos_tip'])) ) {
            $this->db->join('payment_data pd', 'pd.siparis_id = s.id', 'LEFt');
            if ($toplam === false) {
                $this->db->select('pd.durum_guvenlik, pd.durum_odeme, pd.pos_tip, pd.id as posdetayid');
                $this->db->group_by('pd.id');
            }
        }
        */

        if (!$toplam) {
            $this->db->join('payment_data pd', 's.id = pd.siparis_id', 'left');
            if ($toplam === false) {
                $this->db->select('pd.durum_guvenlik, pd.durum_odeme, pd.pos_tip, pd.id as posdetayid');
                // $this->db->group_by('pd.id');
            }
            $this->db->where('s.odeme_tipi >',0);
        }
        /*$this->db->join('odeme_tipleri ot', 'ot.id = s.odeme_tipi');
        if ($toplam === false) {
            $this->db->select('ot.odeme_tipi  odeme_tipi_adi');
        }*/
        $this->db->join('siparis_durumlari sd', 'sd.id = s.durumu');
        if ($toplam === false) {
            $this->db->select('sd.durum');
        }
        if ($toplam === false) {
            $this->db->group_by('s.id');
            $this->db->order_by('s.tarih desc');
        }
        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }
        return $res;
    }

    public function hepsitoplam($type = 1)
    {
        $this->db->select('count(s.id) as toplam');
        $this->db->from('siparisler as s');
        if ($type <= 3) {
            $time = 0;
            if ($type == 1) {
                $current_date = (int)date('d');
                $current_hour = (int)date('H');
                $current_min = (int)date('i');
                if ($current_hour > 18) {
                } else if ($current_hour == 18) {
                    if ($current_min < 30) {
                        $current_date--;
                    }
                } else {
                    $current_date--;
                }
                $time = mktime(18, 30, 0, date('m'), $current_date, date('Y'));
            } else if ($type == 2) {
                $time = mktime(0, 0, 0, date('m'), 1, date('Y'));
            } else if ($type == 3) {
                $time = mktime(0, 0, 0, 1, 1, date('Y'));
            }
            $this->db->where('s.tarih >', $time);
        }
        $this->db->where('s.durumu <>', 10);
        $this->db->where('s.durumu <>', 7);
        return $this->db->get()->row()->toplam;
    }

    public function toplamtutar($order_id = NULL)
    {
        $order = $this->orders(array('id' => $order_id), true);
        var_dump($order);
    }


    public function uruntalepleri($data)
    {
        $this->db->select('ii.*,u.urun_adi');
        $this->db->from('iletisim_istekleri ii');
        $this->db->join('urunler u', 'ii.urun_id = u.id');
        if ($data['adi']) {
            $this->db->where('ii.adi like \'%' . $data['adi'] . '%\'');
        }
        if ($data['soyadi']) {
            $this->db->where('ii.soyadi like \'%' . $data['soyadi'] . '%\'');
        }
        if ($data['urun_adi']) {
            $this->db->where('u.urun_adi like \'%' . $data['urun_adi'] . '%\'');
        }
        $this->db->order_by('ii.tarih desc');
        $this->db->limit($data['limit'], (($data['page'] - 1) * $data['limit']));

        return $this->db->get()->result();
    }

    public function uruntalepleri_count($data)
    {
        $this->db->select('count(ii.id) adet');
        $this->db->from('iletisim_istekleri ii');
        $this->db->join('urunler u', 'ii.urun_id = u.id');
        $this->db->group_start();
        $this->db->where('ii.kategori', 'Ürün Talep');
        $this->db->or_where('ii.kategori', 'Fiyat Düşünce Haber');
        $this->db->group_end();
        if ($data['adi']) {
            $this->db->where('ii.adi like \'%' . $data['adi'] . '%\'');
        }
        if ($data['soyadi']) {
            $this->db->where('ii.soyadi like \'%' . $data['soyadi'] . '%\'');
        }
        if ($data['urun_adi']) {
            $this->db->where('u.urun_adi like \'%' . $data['urun_adi'] . '%\'');
        }
        return $this->db->get()->row()->adet;
    }


    public function uruntalepleri_update($data)
    {
        $this->db->select('ii.*,u.urun_adi urun_adi,u.stok_miktari stok_miktari,u.url_duzenle url_duzenle');
        $this->db->from('iletisim_istekleri ii');
        $this->db->join('urunler u', 'ii.urun_id = u.id');
        $ii = $this->db->where('ii.id ', $data['id'])->get()->row();
        if ($data['note'] && $data['tedarik_fiyati'] && $data['satis_fiyati'] && $data['tedarikci']) {
            $this->db->where('id ', $data['id'])->update('iletisim_istekleri', array('note' => $data['note'], 'tedarikci' => $data['tedarikci'], 'satis_fiyati' => $data['satis_fiyati'], 'tedarik_fiyati' => $data['tedarik_fiyati']));
            $sending = array();
            $this->load->model('mail/Mail_model', 'mail');
            $sending['iletisim_talep'] = $ii;
            $sending['satis_fiyati'] = $data['satis_fiyati'];
            $html = $this->parser->parse('modules/mail/stogageldi', $sending, true);
            $mail_status = $this->mail->htmlmail(array('email' => $ii->email, 'title' => 'Beklediğini Ürün Stoğa Geldi', 'html' => $html));
        }
    }

    public function send_tedarik_mail($data)
    {
        $message = 'OK';
        try {
            $tedarikci = $this->db->select('*')->from('tedarikciler')->where('kod', $data['tedarikci'])->get()->row();
            if(!$tedarikci){
                $tedarikci = $this->db->select('*')->from('castrol_teslimatlar')->where('id',$data['tedarikci'])->get()->row();
            }
            if ($tedarikci->musteri_adres == 0) {
                $data['musteri_adres'] = '';
            }
            $this->load->model('mail/Mail_model', 'mail');
            $html = $this->parser->parse('modules/mail/tedarikmail', $data, true);
            if ($tedarikci->mail == 'berkaykilic@lastikcim.com.tr') {
                $debug_mode = 1;
                $cc_addresses = [];
            } else {
                $cc_addresses = ['siparis@lastikcim.com.tr'];
            }
            if ($tedarikci->mail_2) {
                $cc_addresses[] = $tedarikci->mail_2;
            }
            if ($tedarikci->mail_3) {
                $cc_addresses[] = $tedarikci->mail_3;
            }
            if ($tedarikci->mail) {
                $mail_status = $this->mail->htmlmailcc(array('email' => $tedarikci->mail, 'title' => 'LASTİKCİM ' . $data['siparis_no'], 'html' => $html), $cc_addresses);
                //$mail_status = $this->mail->htmlmailcc(array('email'=> 'berkaykilic@lastikcim.com.tr' ,'title'=>'LASTİKCİM '.$data['siparis_no'], 'html'=> $html),['berkay.klc.en@gmail.com']);
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        try {
            $ext_order = $this->db->select('*')->from('ext_orders')->where('order_id', $data['siparis_no'])->where('ext_user_id', $tedarikci->id)->get()->result();
            if (count($ext_order) < 1) {
                $products = [];
                $products[] = ['product_name' => $data['urun_adi'], 'product_code' => $data['stok_kodu'], 'product_amount' => $data['urun_miktar'], 'product_dot' => $data['uretim_tarihi'],  'product_cost' => $data['urun_maliyet']];
                $this->db->insert('ext_orders', ['order_id' => $data['siparis_no'], 'customer_name' => $data['musteri_isim'], 'customer_address' => $data['musteri_adres'], 'ext_user_id' => $tedarikci->id, 'products' => json_encode($products)]);
            } else {
                $ext_order = $ext_order[0];
                $products = json_decode($ext_order->products);
                $products[] = ['product_name' => $data['urun_adi'], 'product_code' => $data['stok_kodu'], 'product_amount' => $data['urun_miktar'],  'product_cost' => $data['urun_maliyet']];
                $this->db->where('id', $ext_order->id)->update('ext_orders', ['products' => json_encode($products)]);
            }
            $this->db->insert('takip_veri', ['dump_veri' => $message . ':' . $mail_status, 'tarih' => time(), 'old_veri' => $data['siparis_no']]);
        } catch (\Exception $e) {
            return $mail_status;
        }
        if ($debug_mode == 1) {
            return 'debug';
        } else {
            return $mail_status;
        }
    }

    public function musteri_montaj_sms($order){
        $musteri_data = json_decode($order->data);
        if ($musteri_data->tur_sec == "bireysel") {
            $telefon = $musteri_data->cep_telefonu;
        } else {
            $telefon = $musteri_data->kur_cep_telefonu;
        }

        $urunler = json_decode($order->urunler);

        foreach ($urunler as $key => $urun) {
            if (!isset($urun->hizmetler)) {
                continue;
            }
            $hizmetler = $urun->hizmetler;
            foreach ($hizmetler as $key => $hizmet) {
                $firma_id = $hizmet->firma_id;
            }
        }
        $firma = $this->db->select('id,firm_real_tel,firm_real_name,firm_real_adresi')->from('montaj_noktalari')->where('id', $firma_id)->get()->row(); 
        $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin montaj noktasına teslim edilmiştir. Montaj Noktası Bilgileri: '.$firma->firm_real_name.' - ' . $firma->firm_real_tel . ' - '.$firma->firm_real_adresi.'. Lastikcim.com.tr', [$telefon]);
        // ıdli siparişiniz montaj noktasına teslim edilmiştir. # Montaj Noktasi bilgileri : #### - Adres
    }


    public function durumlar()
    {
        $this->db->select("*");
        $this->db->from("siparis_durumlari");
        $this->db->order_by('orders asc');
        return $this->db->get()->result();
    }


    public function odemebildirimler()
    {

        $this->db->select('o.sipid , o.tarih  as otarih, o.not , o.durum as odurum');
        $this->db->from('odeme_bildirimi as o');
        $this->db->select('s.*');
        $this->db->join('siparisler s', 's.id = o.sipid', 'left');
        $this->db->order_by('o.tarih desc');
        return $this->db->get()->result();
    }


    public function kargofirmalar()
    {
        $this->db->select("*");
        $this->db->from("kargo_firmalari");
        return $this->db->get()->result();
    }

    public function siparisnotu($data)
    {
        $this->db->where("id", $data['id']);
        $this->db->update('siparisler', array('siparis_notu' => $data['not']));
        return "Sipariş Notu Güncellendi";
    }

    public function durum_degis($data)
    {
        $data_init = $data;
        $order_1 = $this->db->select('*')->from('siparisler')->where('id', $data['order_id'])->get()->row();
        $durumu = $order_1->durumu;
        $active_arr = array(2, 3, 8, 11);
        $passive_arr = array(1, 4, 5, 6, 7, 9, 10);
        $ac_1 = array(2, 3, 5, 8, 11);
        $ac_2 = array(1, 4, 6, 7, 8, 10);
        if (in_array($durumu, array(3, 8, 5)) && in_array((int)$data['durum'], array(7, 2, 1, 10))) {
            $data['status'] = 'error';
            $data['msg'] = 'Hatalı sipariş durumu geçişi!';
            $data['durumu'] = $durumu;
            return $data;
        }
        $order_items = json_decode($order_1->urunler);
        foreach ($order_items as $item) {
            $temp_urun = $this->db->select('stok_miktari,xml_stok_kodu_alt,xml_stok_kodu')->from('urunler')->where('id', $item->urun)->get()->row();
            $stok_miktari = $temp_urun->stok_miktari;
            $xml_stok_kodu = $temp_urun->xml_stok_kodu;
            $xml_stok_kodu_alt = $temp_urun->xml_stok_kodu_alt;
            if (in_array($durumu, $ac_2) && in_array((int)$data['durum'], $ac_1)) {
                $yeni_miktar = $stok_miktari - $item->miktar;
                $this->db->where("id", $item->urun);
                $this->db->update('urunler', array('stok_miktari' => $yeni_miktar));
                if ($yeni_miktar <= 0) {
                    $this->db->insert('takip_veri',['dump_veri' => json_encode($item) , 'tarih' => time(),  'old_veri' => $data['order_id'].'_'.$yeni_miktar]);
                    if ($xml_stok_kodu_alt && !$xml_stok_kodu) {
                        $this->db->where("id", $item->urun);
                        $this->db->update('urunler', array('xml_stok_kodu' => $xml_stok_kodu_alt));
                    }
                }
            } else if (in_array($durumu, $ac_1) && in_array((int)$data['durum'], $ac_2)) {
                //if ($stok_miktari > $item->miktar) {
                    $this->db->insert('takip_veri',['dump_veri' => json_encode($item) , 'tarih' => time(),  'old_veri' => $data['order_id'].'_ip_'.($stok_miktari + $item->miktar)]);
                    $this->db->where("id", $item->urun);
                    $this->db->update('urunler', array('stok_miktari' => ($stok_miktari + $item->miktar)));
                //}
            }
        }
        $this->db->where("id", $data['order_id']);
        $this->db->update('siparisler', array('durumu' => $data['durum']));
        if (in_array($data['durum'], [4, 9])) {
            $this->load->library('lastikcim');
            $mikro_iptal = $this->lastikcim->siparisIptalEt($data['order_id']);
        }
        $order = $this->orders(array('id' => $data['order_id']), true);
        $data = json_decode($order->data);
        if ($data->email)
            $mail['email'] = $data->email;
        else {
            $mail['email'] = $data->kur_email;
        }
        if ($data->kur_cep_telefonu) {
            $data_phone = $data->kur_cep_telefonu;
        } else {
            $data_phone = $data->cep_telefonu;
        }

        if (isset($data->email)) {
            $email = $data->email;
        } elseif (isset($data->kur_email)) {
            $email = $data->kur_email;
        } else {
            $email = "siparis@lastikcim.com.tr";
        }
        $this->load->model('mail/Mail_model', 'mail');
        if ((int)$order->durumu == 8) {
            //data->msg = $email;
            $mail_template = json_decode($this->extraservices->ayarlar('siparisMailText-8', true)->value);
            $mail_template->mail_aciklamasi = str_replace('#siparisid#', '<a class="id">' . $order->id . '</a>', $mail_template->mail_aciklamasi);
            $sending = array();
            $sending['order'] = $order;
            $sending['mail_template'] = $mail_template;
            $html = $this->parser->parse('modules/mail/siparishazirlaniyor', $sending, true);
            //$data->html = $html;
            $mail_status = $this->mail->htmlmail(array('email' => $email, 'title' => $mail_template->mail_basligi1, 'html' => $html));
            //$data->mail_status = $mail_status;
        } else if ((int)$order->durumu == 3) {
            if ($order->kargo_takip) {
                //$data->msg = $email;
                $sending = array();
                $sending['order'] = $order;
                $kargo = json_decode($order->kargo_takip);
                $kargo_firma = $this->extraservices->kargofirma((int)$kargo->firma);
                $mail_template = json_decode($this->extraservices->ayarlar('siparisMailText-3', true)->value);
                $mail_template->mail_aciklamasi = str_replace('#siparisid#', '<a class="id">' . $order->id . '</a>', $mail_template->mail_aciklamasi);
                $sending['mail_template'] = $mail_template;
                $sending['kargo_firma'] = $kargo_firma;
                $sending['kargo'] = $kargo;
                $data->kargo = $kargo;
                if($kargo->firma == 2) {
                    $monitor_order_number = $this->db->select('*')->from('monitor_ceva')->where('order_number', $order->id)->get()->row();
                    if($monitor_order_number){
                        $link = "https://trweb01.cevalogistics.com/siparis-musteri/m/".$monitor_order_number->shipment_order_number."/9420";
                        $sending['link'] = $link;
                    }
                }
                $html = $this->parser->parse('modules/mail/kargoverildi', $sending, true);
                $mail_status = $this->mail->htmlmail(array('email' => $email, 'title' => $mail_template->mail_basligi1, 'html' => $html));
                $data->mail_status = $mail_status;
                if ($kargo->firma == 12) {
                    $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin lojistik firmasına teslim edilmiştir. Takip No: ' . $kargo->takipnumarasi . '. Faturanız  5 iş günü içerisinde e-mail adresinize e-fatura olarak gönderilecektir. Takip Etmek Icin 08508111101 - lastikcim.com.tr', [$data_phone]);
                } elseif($kargo->firma == 2){
                    $monitor_order_number = $this->db->select('*')->from('monitor_ceva')->where('order_number',$order->id)->get()->row();
                    if($monitor_order_number){
                        $link = "https://trweb01.cevalogistics.com/siparis-musteri/m/".$monitor_order_number->shipment_order_number."/9420";
                        $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin logistik firmasına teslim edildi.' . $kargo_firma->firma_adi . ' takip: ' . $link . '. Faturanız 5 iş günü içerisinde e-mail adresinize e-fatura olarak gönderilecektir.', [$data_phone]);
                    }
                } else {
                    $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin kargoya verildi.' . $kargo_firma->firma_adi . ' takip no: ' . $kargo->takipnumarasi . '. Faturanız 5 iş günü içerisinde e-mail adresinize e-fatura olarak gönderilecektir.', [$data_phone]);
                }
            } else {
                $data = array();
                $data['status'] = 'error';
                $data['msg'] = 'Kargo bilgileri girilmeden kargoya verildi durumuna geçiş yapılamaz.';
                $data['durumu'] = $durumu;
                return $data;
            }
        } else {
            $mail_template = json_decode($this->extraservices->ayarlar('siparisMailText-' . ((int)$order->durumu), true)->value);
            $mail_template->mail_aciklamasi = str_replace('#siparisid#', '<a class="id">' . $order->id . '</a>', $mail_template->mail_aciklamasi);
            $sending = array();
            $sending['order'] = $order;
            $sending['mail_template'] = $mail_template;
            $html = $this->parser->parse('modules/mail/siparishazirlaniyor', $sending, true);
            //$data->html = $html;
            $mail_status = $this->mail->htmlmail(array('email' => $email, 'title' => $mail_template->mail_basligi1, 'html' => $html));
        }
        if ($data_init['durum'] == 2) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin alindi. En gec 3 is gunu iceresinde kargo islemlerin baslayacaktir. 08508111101 - lastikcim.com.tr', [$data_phone]);
        } else if ($data_init['durum'] == 7) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisin alindi, onay icin odemeni bekliyoruz. Banka hesap bilgileri : https://bit.ly/2QhfhdK', [$data_phone]);
        } else if ($data_init['durum'] == 11) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisinin iptal / iade talebi alindi. Cagri merkezimiz seninle irtibata gececek. 08508111101 - lastikcim.com.tr', [$data_phone]);
        } else if ($data_init['durum'] == 9) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisinin iade talebi bankana iletildi. 08508111101 - lastikcim.com.tr', [$data_phone]);
        } else if ($data_init['durum'] == 4) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisinin iptal talebi bankana iletildi. 08508111101 - lastikcim.com.tr', [$data_phone]);
        } else if ($data_init['durum'] == 5) {
            $this->send_sms('Merhaba, ' . $order->id . ' numarali siparisindeki ürünler teslim edilmiştir. Faturan mail adresine e-fatura olarak gönderilecektir. 08508111101 - lastikcim.com.tr', [$data_phone]);
        }
        return $data;
    }

    /*27.01.2018*/
    public function posdetaylar($sdata = NULL, $row = NULL)
    {
        $this->db->select('*');
        $this->db->from('payment_data');
        if (count($sdata) > 0) {
            foreach ($sdata as $key => $value) {

                switch ($key) {
                    case 'limit':
                        $this->db->limit($value);
                        break;

                    default:
                        if ($value || $value == "0") {
                            $this->db->like($key, $value);
                        }
                        break;
                }

            }
        }

        $this->db->order_by('id', 'desc');

        if ($row) {
            $res = $this->db->get()->row();
        } else {
            $res = $this->db->get()->result();
        }

        return $res;
    }

    public function siparismikro($id = NULL, $data = NULL)
    {
        if ($id && is_numeric($id)) {
            $data = json_encode($data);
            $this->db->where('id', $id);
            return $this->db->update('siparisler', array('caridata' => $data));
        } else {
            return false;
        }
    }

    public function siparismikroadres($id = NULL, $data = NULL, $tipi = NULL)
    {
        if ($id && is_numeric($id)) {
            $data = json_encode($data);
            $this->db->where('id', $id);
            return $this->db->update('siparisler', array('cariadresdata_' . $tipi => $data));
        } else {
            return false;
        }
    }

    public function siparismikrodata($id = NULL, $data = NULL, $mikro = NULL)
    {
        if ($id && is_numeric($id)) {
            $data = json_encode($data);
            $this->db->where('id', $id);
            if ($mikro == "tatko") {
                return $this->db->update('siparisler', array('tatko_siparis_data' => $data));
            } else {
                return $this->db->update('siparisler', array('siparis_data' => $data));
            }
        } else {
            return false;
        }
    }

    public function hediyeceklerim($user_id = NULL)
    {
        if ($user_id && is_numeric($user_id)) {
            // $sql_kodu = "select * from hediye_cekleri where gcr_tp = 'member' and (ngcr like '".$user_id."%' or ngcr like '".$user_id.",%' or ngcr like '%,".$user_id.",%') and durum = '1' and bitt >= ".time();
            $sql_kodu = "SELECT hc.*, count(hck.id) AS kullanim FROM hediye_cekleri AS hc LEFT JOIN hediye_cekleri_kullanimlar AS hck ON hck.cek = hc.id and hck.durum = 1 AND hck.musteri = " . $user_id . " WHERE hc.gcr_tp = 'member' AND (	hc.ngcr = '" . $user_id . "' OR hc.ngcr LIKE '" . $user_id . ",%' OR hc.ngcr LIKE '%," . $user_id . "' OR hc.ngcr LIKE '%," . $user_id . ",%' ) AND hc.durum = '1' group by hc.id";

            $res = $this->db->query($sql_kodu)->result();

            return $res;
        } else {
            return FALSE;
        }
    }

    public function siparis_hediyecekleri($siparis_id = NULL)
    {
        if ($siparis_id && is_numeric($siparis_id)) {
            $result = $this->db->select('hc.*')->from('hediye_cekleri_kullanimlar hck')->from('hediye_cekleri hc')->where('hck.siparis', $siparis_id)->where('hc.id = hck.cek')->get()->result();
            return $result;
        }
    }

    public function send_sms($msg, $numbers)
    {
        $numbers = $this->clearNumbers($numbers);
        $msg = preg_replace('/ü/', 'u', $msg);
        $msg = preg_replace('/Ü/', 'U', $msg);
        $msg = preg_replace('/İ/', 'I', $msg);
        $msg = preg_replace('/ı/', 'i', $msg);
        $msg = preg_replace('/ş/', 's', $msg);
        $msg = preg_replace('/Ş/', 'S', $msg);
        $msg = preg_replace('/ö/', 'o', $msg);
        $msg = preg_replace('/Ö/', 'O', $msg);
        $msg = preg_replace('/ğ/', 'g', $msg);
        $msg = preg_replace('/ç/', 'c', $msg);
        $msg = preg_replace('/Ç/', 'C', $msg);
        $config = $this->SMSConfig();
        $text_send = "&message=" . urlencode($msg);
        $numbers_send = "&gsmno=" . implode(',', $numbers);
        $url = "http://api.netgsm.com.tr/sms/send/get/?" . $config . $text_send . $numbers_send;
        $url = str_replace("\0", "", $url);
        $ch = curl_init();
        if (!$ch) {
            echo "Couldn't initialize a cURL handle";
            return false;
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }

    public function clearNumbers($numbers)
    {
        foreach ($numbers as $key => $number) {
            $numbers[$key] = preg_replace("/[^0-9]/", "", $number);
            if ($numbers[$key][0] == 0) {
                $numbers[$key][0] = '';
            }
        }
        return $numbers;
    }

    public function SMSConfig()
    {
        $config = [];
        $config['usercode'] = '8503462650';
        $config['password'] = '3SHP2NTU';
        $config['msgheader'] = 'LASTIKCIM';
        $config['dil'] = 'TR';
        $text = [];
        foreach ($config as $key => $conf) {
            array_push($text, $key . '=' . $conf);
        }
        $text = implode('&', $text);
        return $text;
    }

    public function uruntalebi_toggle($data)
    {
        $talep = $this->db->where('id', $data['id'])->from('iletisim_istekleri')->get()->row();
        $deger = true;
        $note = null;
        if (is_null($talep->note)) {
            $deger = false;
            $note = 'Manuel Onaylandı';
        }
        $astx = $this->db->where('id', $data['id'])->update('iletisim_istekleri', ['okundu' => $deger, 'note' => $note]);
        return true;
    }

    public function parseBarkodMail($data,$firma)
    {
        $image_links = [];
        $type = 'png';
        foreach ($data['images'] as $key => $barkod_image) {
            $a = strstr($barkod_image, '9j', true);
            if($a == "/"){
                $type = 'jpeg';
            }

            $img = base64_decode(str_replace('data:image/png;base64,', '', $barkod_image));
            $file = './uploads/barcodes/' . $data['barcode_no'] . '_' . $key . '_' . uniqid() . '.'.$type;
            file_put_contents($file, $img);
            $image_links[] = $file;
        }

        if (sizeof($image_links) < 1) {
            echo 'no image link';
            return;
        }
        ini_set('display_errors', 1);
        error_reporting(E_ERROR | E_WARNING | E_PARSE);
        $this->load->library('Pdf');
        $pdf = new FPDF();
        $image_pages = array_chunk($image_links, 4);
        foreach ($image_pages as $key => $image_page) {
            $pdf->AddPage();
            foreach ($image_page as $key => $image) {
                if ($key == 0) {
                    $t = 5;
                    $l = 5;
                } else if ($key == 1) {
                    $t = 5;
                    $l = 130;
                } else if ($key == 2) {
                    $t = 115;
                    $l = 5;
                } else if ($key == 3) {
                    $t = 115;
                    $l = 130;
                }
                $pdf->Image($image, $t, $l, 90, strtoupper($type));
            }
        }
        $pdf_name = './uploads/barcodes/' . $data['barcode_no'][0] . '_' . $key . '_' . uniqid() . '.pdf';
        $pdf_files = [];
        $pdf_files[] = $pdf_name;
        $pdf->Output('F', $pdf_name, 1);

        $this->load->model('mail/Mail_model', 'mail');
        $html = $firma.' KARGO Çağrınız Oluşturulmuştur.';
        $order_data = json_decode($data['order']->data);
        if ($order_data->tur_sec == "bireysel") {
            $adi = $order_data->adi . ' ' . $order_data->soyadi;
        } else {
            $adi = $order_data->ticari_unvani;
        }

        $cc_addresses = ['siparis@lastikcim.com.tr'];
        if ($data['depo']->mail_2) {
            $cc_addresses[] = $data['depo']->mail_2;
        }
        if ($data['depo']->mail_3) {
            $cc_addresses[] = $data['depo']->mail_3;
        }
        if ($data['depo']->ext_mail) {
            $cc_addresses[] = $data['depo']->ext_mail;
        }

        if (in_array($data['order']->durumu, [9, 11])) {
            $mail_status = $this->mail->htmlmailccwithattachments(array('email' => 'siparis@lastikcim.com.tr', 'title' => 'LASTİKCİM İADE ' . $data['order']->id . ' - ' . $adi, 'html' => $html), [], $pdf_files);
        } else {
            $mail_status = $this->mail->htmlmailccwithattachments(array('email' => $data['depo']->mail, 'title' => 'LASTİKCİM ' . $data['order']->id . ' - ' . $adi, 'html' => $html), $cc_addresses, $pdf_files);
        }
        try {
            $ext_order = $this->db->where('order_id', $data['order']->id)->from('ext_orders')->select('*')->get()->result();
            if (count($ext_order) > 0) {
                $ext_order = $ext_order[0];
                if ($ext_order->barcode_pdf) {
                    $barcodes = json_decode($ext_order->barcode_pdf);
                } else {
                    $barcodes = [];
                }
                $barcodes[] = $pdf_name;
                $this->db->where('order_id', $data['order']->id)->update('ext_orders', ['barcode_pdf' => json_encode($barcodes)]);
            }
        } catch (\Exception $e) {
            $ext_order = [];
        }

        if (!in_array($data['order']->durumu, [9, 11])) {
            $update_data = [];
            $update_data['takipnumarasi'] = $data['barcode_no'];
            if(is_iterable($update_data['takipnumarasi'])){
                $update_data['takipnumarasi'] = $update_data['takipnumarasi'][0];
            }
            $firma = $this->db->select('id')->from('kargo_firmalari')->where('kod',$firma)->get()->row();
            $update_data['firma'] = $firma->id;
            if($update_data['firma'] == 2){
                $update_data['takipnumarasi'] = substr($update_data['takipnumarasi'],6,9);
            }
            $this->kargogir($data['order']->id, $update_data);
        }
    }

    public function createWaybillPdfAndSave($data)
    {
        $this->load->library('Updf');
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $pdf = new TFPDF();
        $pdf->addPage();
        $pdf->Image('./temalar/tema/assets/img/SEVKIRSALIYE.png', -5, -5, 216, 'PNG');

        $pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
        $pdf->SetFont('DejaVu', '', 14);
        $pdf->SetFontSize(10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(21, 37.2);
        $syn = $data['musteri_adres_fatura'];
        $pdf->SetRightMargin(120);
        $pdf->Write(4, $syn);
        $vdaire = '-';
        $vdaire = $data['musteri_vergi_daire'];
        $vaireno = $data['musteri_vergi_no'];
        $pdf->SetXY(35, 63.7);
        $pdf->Write(4, $vdaire);
        $pdf->SetXY(35, 69);
        $pdf->Write(4, $vaireno);
        $pdf->SetRightMargin(0);
        $duz_trh = date("d.m.Y");
        //$duz_trh = '05.03.2020';
        //$duz_trh = '31.12.2019';
        $pdf->SetXY(159, 66.6);
        $pdf->Write(4, $duz_trh);
        //Alt Sevk Adresi
        $pdf->SetRightMargin(100);
        $sevk_adres = ($data['musteri_adres_sevk']);
        $pdf->SetXY(28, 220);
        $pdf->Write(4, $sevk_adres);
        //Sipariş No
        $pdf->SetRightMargin(0);
        $pdf->SetXY(138, 220);
        $pdf->Write(4, 'Sipariş Numarası:' . $data['siparis_no']);
        //İrsaliye No
        $pdf->SetFontSize(12);
        $siradaki_irsaliye_no = $this->extraservices->ayarlar('irsaliye_no', true)->value;
        $pdf->SetRightMargin(0);
        $pdf->SetXY(144, 60.4);
        $pdf->Write(4, $siradaki_irsaliye_no);
        $seri = 'A';
        $pdf->SetRightMargin(0);
        $pdf->SetXY(142, 54.2);
        $pdf->Write(4, $seri);
        $pdf->SetFontSize(10);
        $line_1 = 98;
        $toplam_adet = 0;
        foreach ($data['urunler'] as $key => $urun) {
            $pdf->SetRightMargin(0);
            $pdf->SetXY(24, $line_1);
            $pdf->Write(4, $urun['stok_kodu']);
            $pdf->SetXY(64, $line_1);
            $urun_adi = preg_replace("/\([^)]+\)/", "", $urun['urun_adi']); // 'ABC '
            $pdf->Write(4, $urun_adi);
            $pdf->SetXY(178, $line_1);
            $pdf->Write(4, $urun['urun_miktar']);
            $toplam_adet += $urun['urun_miktar'];
            $line_1 += 10.4;
        }

        $pdf->SetXY(24, 210.4);
        $pdf->Write(4, 'Miktar Toplamı: ' . $toplam_adet);
        $pdf_name = './uploads/sevk/' . $data['siparis_no'] . '_' . uniqid() . '.pdf';
        //$pdf_name = './uploads/sevk/'.$data['siparis_no'].'_'.uniqid().'.pdf';
        $pdf_files = null;
        $pdf_files = [];
        $pdf_files[] = $pdf_name;
        $pdf->Output($pdf_name, 'F', 1);

        $this->load->model('mail/Mail_model', 'mail');
        $html = 'Sipariş İrsaliyeniz Oluşturulmuştur. İrsaliye No: A' . $siradaki_irsaliye_no;
        $adi = $data['musteri_isim'];

        $tedarikci = $this->db->where('kod', $data['tedarikci'])->from('tedarikciler')->select('*')->get()->row();
        $cc_addresses = ['siparis@lastikcim.com.tr'];
        if ($tedarikci->mail_2) {
            $cc_addresses[] = $tedarikci->mail_2;
        }
        if ($tedarikci->mail_3) {
            $cc_addresses[] = $tedarikci->mail_3;
        }

        $mail_status = $this->mail->htmlmailccwithattachments(array('email' => $tedarikci->mail, 'title' => 'LASTİKCİM IRS ' . $data['siparis_no'] . ' - ' . $adi, 'html' => $html), $cc_addresses, $pdf_files);
        //$mail_status = $this->mail->htmlmailccwithattachments(array('email'=> 'berkay.klc.en@gmail.com' ,'title'=>'LASTİKCİM IRS '.$data['siparis_no'].' - '.$adi, 'html'=> $html), ['berkaykilic@lastikcim.com.tr'], $pdf_files);

        $this->db->where('name', 'irsaliye_no')->update('ayarlar', ['value' => ($siradaki_irsaliye_no + 1)]);
        try {
            $ext_order = $this->db->where('order_id', $data['siparis_no'])->from('ext_orders')->select('*')->get()->result();
            if (count($ext_order) > 0) {
                $ext_order = $ext_order[0];
                if ($ext_order->irsaliye_pdf) {
                    $irsaliye = json_decode($ext_order->irsaliye_pdf);
                } else {
                    $irsaliye = [];
                }
                $irsaliye[] = $pdf_name;
                $this->db->where('order_id', $data['siparis_no'])->update('ext_orders', ['irsaliye_pdf' => json_encode($irsaliye)]);
            }
        } catch (\Exception $e) {
            $ext_order = [];
        }
        return $mail_status;
    }

    public function montajDataOlustur($mdata)
    {
        $montaj_data = json_encode($mdata);
        $insert = $this->db->where('id', $mdata['siparis_id'])->update('siparisler', ['montaj_arac_bilgisi' => $montaj_data]);
    }

    public function sendMontajMail($order)
    {
        $montaj_arac_bilgisi = json_decode($order->montaj_arac_bilgisi);
        if (!$montaj_arac_bilgisi->marka || !$montaj_arac_bilgisi->model || !$montaj_arac_bilgisi->plaka) {
            return 'false';
        }

        $musteri_data = json_decode($order->data);
        if ($musteri_data->tur_sec == "bireysel") {
            $telefon = $musteri_data->cep_telefonu;
            $adi = $musteri_data->adi . ' ' . $musteri_data->soyadi;
        } else {
            $telefon = $musteri_data->kur_cep_telefonu;
            $adi = $musteri_data->ticari_unvani;
        }

        $urunler = json_decode($order->urunler);

        $this->load->library('Updf');

        $pdf = new TFPDF();
        $pdf->addPage();
        $pdf->Image('./temalar/tema/assets/img/MONTAJ_NOKTASI_SERVIS_FORMU.png', -4, -5, 216, 'PNG');


        $pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
        $pdf->SetFont('DejaVu', '', 14);
        $pdf->SetFontSize(10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetRightMargin(114);
        $pdf->SetLeftMargin(50);
        $pdf->SetXY(50, 43);
        $pdf->Write(4, $adi);
        $pdf->SetXY(50, 52.6);
        $pdf->Write(4, $telefon);
        $pdf->SetXY(50, 58);
        $pdf->Write(4, $order->id);
        //$order_time = date('r', );
        $order_time = new DateTime('@' . $order->tarih);
        $pdf->SetXY(50, 63.4);
        $pdf->Write(4, $order_time->format('d-m-Y'));

        $line_1 = 86;
        $montaj_adet = 0;
        $sibop_adet = 0;
        $nitrojen_adet = 0;
        $rot_adet = 0;
        $saklama_adet = 0;
        $firma = '';
        foreach ($urunler as $key => $urun) {
            if (!isset($urun->hizmetler)) {
                continue;
            }
            $hizmetler = $urun->hizmetler;
            foreach ($hizmetler as $key => $hizmet) {
                if ($hizmet->is_mnt == 1) {
                    $montaj_adet += $hizmet->adet;
                } else if ($hizmet->id_str == 'depolama') {
                    $saklama_adet += $hizmet->adet;
                } else if ($hizmet->id_str == 'balans_ayari') {
                    $rot_adet += $hizmet->adet;
                } else if ($hizmet->id_str == 'nitrojen_dolum') {
                    $nitrojen_adet += $hizmet->adet;
                } else if ($hizmet->id_str == 'plastik_sibop' || $hizmet->id_str == 'sibop_degisimi') {
                    $sibop_adet += $hizmet->adet;
                }
                $firma_id = $hizmet->firma_id;
                $firma = $hizmet->firma_adi;
            }
            $pdf->SetRightMargin(0);
            $pdf->SetXY(18, $line_1);
            $pdf->Write(4, $urun->stok_kodu);
            $pdf->SetXY(71, $line_1);
            $urun_adi = preg_replace("/\([^)]+\)/", "", $urun->urun_adi);
            $pdf->Write(4, $urun_adi);
            $pdf->SetXY(170, $line_1);
            $pdf->Write(4, $urun->miktar);
            $line_1 += 5.4;
        }

        if ($firma == '') {
            echo 'Hizmet Bulunamadı.';
            die();
        }

        $firma_mail = $this->db->select('id,firm_real_mail,firm_real_name')->from('montaj_noktalari')->where('id', $firma_id)->get()->row();
        $pdf->SetFontSize(12);
        $pdf->SetRightMargin(0);
        $pdf->SetXY(50, 124.6);
        $pdf->Write(4, ($montaj_adet == 0 ? 'X' : $montaj_adet));
        $pdf->SetXY(50, 130.3);
        $pdf->Write(4, ($sibop_adet == 0 ? 'X' : $sibop_adet));

        $pdf->SetXY(50, 136.0);
        $pdf->Write(4, ($nitrojen_adet == 0 ? 'X' : $nitrojen_adet));

        $pdf->SetXY(50, 141.6);
        $pdf->Write(4, ($rot_adet == 0 ? 'X' : $rot_adet));

        $pdf->SetXY(50, 147.2);
        $pdf->Write(4, ($saklama_adet == 0 ? 'X' : $saklama_adet));

        $pdf->SetLeftMargin(0);

        $pdf->SetXY(28, 163.6);
        $pdf->Write(4, $montaj_arac_bilgisi->marka);

        $pdf->SetXY(28, 169);
        $pdf->Write(4, $montaj_arac_bilgisi->model);


        $pdf->SetXY(28, 174.6);
        $pdf->Write(4, $montaj_arac_bilgisi->model_yil);

        $pdf->SetXY(28, 180.2);
        $pdf->Write(4, $montaj_arac_bilgisi->motor);


        $pdf->SetXY(28, 185.6);
        $pdf->Write(4, $montaj_arac_bilgisi->kilometre);

        $pdf->SetXY(28, 191.2);
        $pdf->Write(4, $montaj_arac_bilgisi->plaka);

        $pdf->SetXY(116, 44);
        $pdf->Write(4, $firma_mail->firm_real_name);

        $pdf_name = './uploads/montaj_form/MONTAJ_FORM_' . $order->id . '_' . time() . '.pdf';
        $pdf_files = null;
        $pdf_files = [];
        $pdf_files[] = $pdf_name;
        $pdf->Output($pdf_name, 'F', 1);
        //$pdf->Output($pdf_name, 'F' ,1);

        $firma_mail = $firma_mail->firm_real_mail;
        if (!isset($firma_mail)) {
            $firma_mail = 'montaj@lastikcim.com.tr';
        }
        $this->load->model('mail/Mail_model', 'mail');
        $html = 'Montaj Formunuz Oluşturulmuştur. Sipariş No: ' . $order->id;
        $cc_addresses = [];
        $cc_addresses[] = 'siparis@lastikcim.com.tr';
        $cc_addresses[] = 'destek@lastikcim.com.tr';
        $mail_status = $this->mail->htmlmailccwithattachments(array('email' => $firma_mail, 'title' => 'LASTİKCİM M.FORMU ' . $order->id . ' - ' . $adi, 'html' => $html), $cc_addresses, $pdf_files);
        return 'true';

    }
}

?>

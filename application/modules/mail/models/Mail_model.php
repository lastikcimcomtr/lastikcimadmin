<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail_model extends CI_Model {
	
	private $theme_url;
	function __construct() {
		parent::__construct();
		// load email library
		$this->load->library('email');
		$config = Array('protocol' => $this->extraservices->ayarlar('protocol',true)->value,
			'smtp_host' => $this->extraservices->ayarlar('smtp_host',true)->value,
			'smtp_port' => $this->extraservices->ayarlar('smtp_port',true)->value,
			'smtp_user' => $this->extraservices->ayarlar('smtp_user',true)->value,
			'smtp_pass' => $this->extraservices->ayarlar('smtp_pass',true)->value,
			'mailtype'  => 'html',
			'charset'   => 'utf-8');
		$this->email->initialize($config);
		$this->theme_url = $this->parser->theme_url();
	}

	
	public function norm($data){
		$this->email
		->from($this->extraservices->ayarlar('smtp_user',true)->value, $this->extraservices->ayarlar('mail_isim',true)->value)
		->to($data['email'])
		->subject($data['title'])
		->message($this->parser->parse('mail_default', array('title' => $data['title'] , 'email' => $data['email'] ,'text'=> $data['text']), TRUE))
		->set_mailtype('html');
		return $this->email->send();

	} 
	
	public function reminder($data){
		$this->email
		->from($this->extraservices->ayarlar('smtp_user',true)->value, $this->extraservices->ayarlar('mail_isim',true)->value)
		->to($data['email'])
		->subject('Şifre Hatırlatma')
		->message($this->parser->parse('mail_reminder', array("template" => "mail/mail_reminder", 'title' => 'Şifre Hatırlatma', 'email' => $data['email'] ,'code'=>$data['code']), TRUE))
		->set_mailtype('html');
		return $this->email->send();

	}
	public function htmlmail($data)
    {
        $this->email
            ->from($this->extraservices->ayarlar('smtp_user', true)->value, $this->extraservices->ayarlar('mail_isim', true)->value)
            ->to($data['email'])
            ->subject($data['title'])
            ->message($data['html'])
            ->set_mailtype('html');
		if($this->email->send()){
            return 'sent';
		}else{
			return $this->email->print_debugger();
		}
    }

	public function htmlmailcc($data,$cc_adresses)
    {
        $email_sender = $this->email
            ->from($this->extraservices->ayarlar('smtp_user', true)->value, $this->extraservices->ayarlar('mail_isim', true)->value)
            ->to($data['email'])
            ->subject($data['title'])
            ->message($data['html'])
            ->set_mailtype('html')
            ->cc($cc_adresses);
		if($this->email->send()){
            return 'sent';
		}else{
			return $this->email->print_debugger();
		}
    }


	public function htmlmailccwithattachments($data,$cc_adresses,$attachments)
    {
		$this->email->clear(TRUE);
        $email_sender = $this->email
            ->from($this->extraservices->ayarlar('smtp_user', true)->value, $this->extraservices->ayarlar('mail_isim', true)->value)
            ->to($data['email'])
            ->subject($data['title'])
            ->message($data['html'])
            ->set_mailtype('html')
            ->cc($cc_adresses);
	    foreach ($attachments as $key => $attachment) {
	    	$this->email->attach($attachment);
	    }
		if($this->email->send()){
            return 'sent';
		}else{
			return $this->email->print_debugger();
		}
    }

}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Importers_model extends CI_Model {
	
	function __construct() {
		parent::__construct();/*
		ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);	*/

}

public function kategoridenevar($id = NULL, $neyi = "*")
{
	$this->db->select($neyi);
	$this->db->from('parametreler');

	if ($id && is_numeric($id))
		$this->db->like('categories', "|".$id."|");

	$this->db->group_by('silik_id');
	$this->db->order_by('adi', 'asc');
	$res = $this->db->get()->result();
	return $res;
}

public function mikro($pdata = NULL, $category = NULL)
{
	$data = array();

	$urunler = $pdata['urunler'];
	if (count($urunler) > 0) {
		if ($category && is_numeric($category)) {
			$parametreler = $this->importers->kategoridenevar($category, "silik_id as parametre");
		}
		$data['parametreler'] 	= $parametreler;
		$data['urunler']		= $urunler;

		$eklenecekler = array();

		foreach ($urunler as $key => $value) {
			if ($value['durum'] == 1) {

				$eklenecekler[] = $value;

			}
		}
		if (count($eklenecekler) == 0) {
			$data['status'] = "danger";
			$data['message']= "Eklemek için seçilmiş bir ürün yok gibi.";
		} else {

			$ekleneceklerden 	= array_keys($eklenecekler[0]);
			$kimkime 			= array();

			foreach ($ekleneceklerden as $eklenecek) {						
				switch ($eklenecek) {
					case 'sto_kod':
					$kimkime[$eklenecek] = "stok_kodu";
					break;

					case 'sto_isim':
					$kimkime[$eklenecek] = "urun_adi";	
					break;

					case 'miktar':
					$kimkime[$eklenecek] = "stok_miktari";	
					break;

					case 'fiyat':
					$kimkime[$eklenecek] = "urun_alis_fiyati";	
					break;

					case 'doviz':
					$kimkime[$eklenecek] = "urun_alis_fiyati_birim";	
					break;

					case 'tabani':
					$kimkime[$eklenecek] = "taban";
					break;

					case 'yanagi':
					$kimkime[$eklenecek] = "yanak";	
					break;

					case 'jantcapi':
					$kimkime[$eklenecek] = "jant_capi";
					break;

					case 'yuk':
					$kimkime[$eklenecek] = "yuk_endeksi";	
					break;

					case 'hiz':
					$kimkime[$eklenecek] = "hiz_endeksi";	
					break;

					case 'lastikdeseni':
					$kimkime[$eklenecek] = "desen";	
					break;

					case 'resim1':
					$kimkime[$eklenecek] = "resim_1";	
					break;

					case 'resim2':
					$kimkime[$eklenecek] = "resim_2";	
					break;

					case 'resim3':
					$kimkime[$eklenecek] = "resim_3";	
					break;

					case 'xlrf':
					$kimkime[$eklenecek] = "xl_rf";	
					break;

					case 'rftzprofssrzps':
					$kimkime[$eklenecek] = "run_flat";	
					break;

					case 'kati':
					$kimkime[$eklenecek] = "lastik_kati";	
					break;

					case 'yakit':
					$kimkime[$eklenecek] = "yakit_sarfiyati";	
					break;

					case 'islak':
					$kimkime[$eklenecek] = "islak_yol_tutusu";	
					break;

					case 'gurultu':
					$kimkime[$eklenecek] = "gurultu_seviyesi";	
					break;

					case 'mrk_ismi':
					$kimkime[$eklenecek] = "marka";	
					break;

					case 'mevsim':
					$kimkime[$eklenecek] = "mevsim";	
					break;

					case 'kategori':
					$kimkime[$eklenecek] = "kategori";
					break;

					case 'stok_kodu':
					$kimkime[$eklenecek] = "xml_stok_kodu";	
					break;
				}
			}

			foreach ($eklenecekler as $eklenecek) {

				$add_data = array();
				foreach ($eklenecek as $ekkey => $ekvalue) {	
					foreach ($kimkime as $kkey => $kvalue) {
						if ($ekkey == $kkey) {

							$ekvalue = trim($ekvalue);
							if ($ekvalue == "TL") {
								$ekvalue = "TRY";
							}

							$add_data[$kvalue] = $ekvalue;
						}
					}
				}
				$add_data['tarih']		= time();
				$add_data['tip']		= "lastik";
				$add_data['kategori'] 	= $category;			
				$res = $this->db->insert('urunler', $add_data);
			}

			$data['status'] = "success";
			$data['message']= "Ürünler başarılı bir şekilde eklenmiştir.";
		}
	} else {
		$data['status'] = "danger";
		$data['message']= "Herhangi bir ürün bilgisine ulaşılamadı.";
	}

	return $data;
}

public function aktarimdakiler($sdata = NULL, $row = FALSE)
{
	$this->db->select('*');
	$this->db->from('aktarimdakiler');
	if (count($sdata) > 0) {
		foreach ($sdata as $key => $value) {
			$this->db->where($key, $value);
		}
	}
	if ($row) {
		$res = $this->db->get()->row();
	} else {
		$res = $this->db->get()->result();
	}
	return $res;
}


public function savelist($data){
	$data = $this->veriduzenle($data);
	if(!$data['urun']){
		return $data;
	}
	$this->load->model('products/Products_model', 'products');
	$return['update'] = 0;
	$return['add'] = 0;
	foreach ($data['urun'] as $urun) {
            $urun['tarih'] = time();
		if(isset($urun['stok_kodu']) && strlen($urun['stok_kodu']) > 0){
			$this->db->select('id');
			$this->db->from('urunler');
			$this->db->where("stok_kodu",$urun['stok_kodu']);
			$this->db->where("marka",$urun['marka']);
			$varmi = $this->db->get()->row();
			if($varmi->id){
				$this->db->where('id',$varmi->id);
				$this->db->update('urunler', $urun);
				++$return['update'];
			}else{
				if(!$urun['tip'])
					$urun['tip']		= "lastik";
				if(!$urun['fiyat'])
					$urun['fiyat'] = "0";
				if(!$urun['fiyat_birim'])
					$urun['fiyat_birim'] = "TRY";
				if(!$urun['kdv'])
					$urun['kdv'] = 18;
				if(!$urun['kdv_dahil'])
					$urun['kdv_dahil'] = 1;
				if(!$urun['stok_miktari'])
					$urun['stok_miktari'] = 0;
				if(!$urun['garanti_suresi'])
					$urun['garanti_suresi'] = 24;
				if(	$urun['tip'] == "lastik"){
					$urun = $this->products->lastiktitle($urun);
				}else if($urun['tip'] == "jant" && !$urun['url_duzenle']){
					$urun['url_duzenle'] =$this->products->checksefurl($urun['seo_baslik']);
				}
				$this->db->insert('urunler', $urun);
				++$return['add'];
			}
		}
	}
	return $return;
}


		public function add_excel(){
			$allowed = array(
				"xls" => array( "application/vnd.ms-excel" ),
				"xlsx" => array(
					"application/vnd.ms-excel",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
				)
			);
			if(isset($_FILES["dosya"]["error"])){
				if($_FILES["dosya"]["error"] > 0){
					echo "Error: " . $_FILES["dosya"]["error"] . "<br>";
				} else{
					if ( in_array( $_FILES["dosya"]['type'], $allowed['xlsx'] ) ) {
						$tname = $_FILES["dosya"]["tmp_name"];
						$dismi = $_FILES["dosya"]["name"];
						$varmi = $this->aktarimdakiler(array('dosya' => $dismi ) , TRUE);
						if($varmi){
							$dismi = rand(0,100) . "_" . $dismi ;
						}
						$basepath = str_replace("/system/", "", BASEPATH);
						$dosyayolu = $basepath . $this->extraservices->ayarlar(array('name' => 'excel_dosya'), TRUE)->value .  $dismi ;
					
						unlink($dosyayolu);		
						move_uploaded_file($tname, $dosyayolu);
						$data = $this->input->post();
						$save['dosya'] = $dismi;
						$save['zaman'] = time();
						$save['tip'] = "excel";
						if(is_numeric($data['parametre'])){
							$param = $this->aktarimdakiler(array('id' => $data['parametre'] ) , TRUE);
							if($param->parametreler)
							{
								$save['parametreler'] = $param->parametreler;
							}
						}
						$this->db->insert('aktarimdakiler', $save);
						redirect($_SERVER['HTTP_REFERER']);
					}
					else
					{
						echo 	"dosya tipi hatalı" ;
					}
				}

			}
		}


public function veriduzenle($data){

	$mevsim = array( "YAZ","KIŞ","4 MEVSİM","4 MEVSIM","4MEVSİM","4MEVSIM","Dört Mevsim","SUMMER","WINTER","ALL SEASON");
	$mevsims = array("yaz","kis","4m"      ,"4m"      ,"4m"     ,"4m"     ,"4m"         ,"yaz"   ,"kis"   ,"4m");
	$yapisi = array("R","ZR");
	$yapisis = array("r","zr");

	foreach ($data['urun'] as $key => $urun) { 
		unset($data['urun'][$key]['secim']);
		if(isset($data['marka']) && is_numeric($data['marka']) && $data['marka'] <> 0)
			$data['urun'][$key]['marka'] = $data['marka'];
		elseif($urun['marka']){
			$marka = $this->markaal($urun['marka']);
			if($marka)
				$data['urun'][$key]['marka'] = $marka;
		}

		if(isset($data['kategori']) && is_numeric($data['kategori']))
			$data['urun'][$key]['kategori'] = $data['kategori'];
		if(/*is_numeric($data['urun'][$key]['kategori']) && */is_numeric($data['urun'][$key]['marka']) ){	
			foreach ($urun as $param => $deger) {
				switch (TRUE) {
					case  $param == 'mevsim':
					$data['urun'][$key][$param] = str_replace($mevsim,$mevsims, $deger);
					break;	
					case $param == 'yapisi':
					$data['urun'][$key][$param] = str_replace($yapisi,$yapisis, $deger);
					break;
					case strpos($param , "etiket_") !== false :
					$sonuc = $this->etiketbul($deger);
					if($sonuc){
						if(!isset($data['urun'][$key]['etiket'])){
							$data['urun'][$key]['etiket'] = $sonuc;
						}else{
							$data['urun'][$key]['etiket'] .= ",". $sonuc;
						}
					}
					unset($data['urun'][$key][$param]);
					break;
					case strpos($param , "resim_") !== false :
					if( $deger <> ''){
						$basepath = str_replace("/system/", "", BASEPATH);
						$dosyaismi = $data['urun'][$key]['marka'] ."-". rand(0,100) . "-" .   end(explode("/", $deger));
						$dosyayolu = $basepath . $this->extraservices->ayarlar(array('name' => 'urun_resim'), TRUE)->value . "/". $dosyaismi ;
						file_put_contents($dosyayolu , file_get_contents($deger));
						$data['urun'][$key][$param] = 	$dosyaismi ;
					}
					break;
						/*case 'hiz_endeksi':
						if (strlen($deger) >  2)
						{
							$data['urun'][$key]['yuk_endeksi'] =preg_replace("/[^0-9,.]/", "", $deger);
							$data['urun'][$key]['hiz_endeksi'] =preg_replace('/[0-9]+/', '', $deger);
						}
						break;
						case 'yuk_endeksi':
						if (!is_numeric($deger))
						{
							$data['urun'][$key]['yuk_endeksi'] =preg_replace("/[^0-9,.]/", "", $deger);
							$data['urun'][$key]['hiz_endeksi'] =preg_replace('/[0-9]+/', '', $deger);
						}
						break;*/
						case $param == 'taban':
						case $param == 'yanak':
						case $param == 'jant_capi':
						$data['urun'][$key][$param] = $this->verilerdenal($param,$deger,$data['urun'][$key]['kategori']);
						break;

						case $param == 'desen':
						$data['urun'][$key]['mevsim'] = str_replace($mevsim,$mevsims, 	$data['urun'][$key]['mevsim'] );
						$data['urun'][$key][$param] = $this->desenal(trim($deger),$data['urun'][$key]['kategori'],$data['urun'][$key]['marka'],$data['urun'][$key]['mevsim']);
						if(!$data['urun'][$key][$param]){
							$hata_stok_kodu = $data['urun'][$key]['stok_kodu'];
							$data = [];
						   $data['status'] = "error";
						   $data['message']= $hata_stok_kodu." kodlu ürünün ".$deger." deseni olmadığından işlem iptal edilmiştir.";
						   $data['urun'] = false;
							return $data;;
						}
						break;
					}

					/*if( strlen($data['urun'][$key][$param]) < 1 ){
						unset($data['urun'][$key][$param]);
					}*/
				} 
			}else{
				unset($data['urun'][$key]);
			}
		}
		return $data;
	}


	public function etiketbul($etiket , $ekle = true){
		if(isset($etiket) && $etiket <> ""){
			$this->db->select('id');
			$this->db->from('etiketler');
			$this->db->where('adi',$etiket);
			$res = $this->db->get()->row();
			if($res){
				return $res->id;
			}else if($ekle){
				$this->db->insert('etiketler', array("adi" => $etiket));
				return  $this->db->insert_id();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function verilerdenal($param,$deger,$category){
		if(isset($deger) && $deger <> ""){

			switch ($param) {
				case 'taban':
				case 'yanak':
				case 'jant_capi':
				$deger =	preg_replace("/[^0-9,.]/", "", $deger);
				break;
			}
			$this->db->select("id");
			$this->db->from("veriler");
			$this->db->where("tip",$param);
			$this->db->where("deger",$deger);
			$this->db->where("ust",$category);	
			$this->db->where("usttip","kategoriler");	
			$veri  = $this->db->get()->row();
			if($veri){
				return $veri->id;
			}
			else{
				$add_data['tip']		= $param;
				$add_data['deger']		= $deger;
				$add_data['ust'] 		= $category;	
				$add_data['usttip'] 	= "kategoriler";			
				$this->db->insert('veriler', $add_data);
				return  $this->db->insert_id();
			}
		}
		else {
			return $deger;
		}
	}


	public function desenal($deger,$category,$marka,$mevsim){
		if(isset($deger) && $deger <> ""){
			$this->db->select("id");
			$this->db->from("desenler");
			$this->db->where("adi", trim($deger));
			$this->db->where("marka",$marka);
			$this->db->where("kategori",$category);	
			$veri  = $this->db->get()->row();
			if($veri){
				return $veri->id;
			}
			else{
				return false;
				$add_data['adi']		= $deger;
				$add_data['marka']		= $marka;
				$add_data['kategori'] 	= $category;	
				$add_data['mevsim'] 	= $mevsim;	
				$add_data['tip'] 	= "lastik";	
				$add_data['durum'] 	= 1;			
				$this->db->insert('desenler', $add_data);
				return  $this->db->insert_id();
			}
		}
		else {
			return $deger;
		}
	}

	public function markaal($str){
		$this->db->select("id");
		$this->db->from("markalar");
		$this->db->where("adi", trim($str));
		$veri  = $this->db->get()->row();
		if($veri){
			return   $veri->id ;
		}
		else
			return FALSE;
	}


	public function duzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id)) {
			$data 	= json_encode($data);
			$updata = array('parametreler' => $data);
			$this->db->where('id', $id);
			$res = $this->db->update('aktarimdakiler', $updata);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function sil($id = NULL)
	{
		$this->db->where('id', $id);
		$res = $this->db->delete('aktarimdakiler');
		return $res;
	}

	public function mikrodanupdate($stok_kodu = NULL, $data = NULL)
	{
		$mikro_column = $this->extraservices->ayarlar(array('name' => 'mikro_column'), TRUE)->value;
		$this->db->where($mikro_column, $stok_kodu);
		$res = $this->db->update('urunler', $data);
		return $res;
	}
	public function ferhat_close_removed($apiurl){
    	ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		$result_set = $this->db->select('id,stok_kodu,fd_entegrasyon,fiyat,stok_miktari,fd_onoff')->from('fd_ent_exist')->where('fd_onoff',1)->where('stok_miktari > 0')->get()->result();
		$array_items = [];
		foreach ($xml->urun as $value){ 
			if($value->category1 == 'LASTIK'){
				array_push($array_items,(string)$value->model);
			}
		}
		$off_set = [];
		foreach ($result_set as $key => $item) {
			if(!in_array($item->fd_entegrasyon, $array_items)){
				array_push($off_set,$item);
			}
		}	
		foreach ($off_set as $key => $off_item) {
			$updated = [];
			$updated['fd_onoff'] = 0;
			$updated['stok_miktari'] = 0;
			$updated['tedarikci'] = null;
			$update_query = $this->db->where('id',$off_item->id)->update('urunler', $updated);
		}
	}
	
	public function ferhat_deger_xml($type = 1,$apiurl, $item_exist = true){
		
    	ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		$apiurl = 'https://b2b.degeras.com/xmls/'.urlencode('araç').'.xml';
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		if ($type == 1) {
			$array_items = [];
			foreach ($xml->urun as $value){ 
				if($value->category1 == 'LASTIK' || $value->category1 == 'GK01'){
					array_push($array_items,(array)$value);
				}
			} 
			
			$array_items = $this->ferhat_deger_filter($array_items, $item_exist, 4);
			foreach ($array_items as $item){ 
				$this->ferhat_deger_urun_isle($item);
			}
					
			return $xml;
		} else {
			return 'type invalid';
		}
	}

	public function clearDot($dot){
		$dot = preg_replace("/[^0-9]/", "", $dot);
		if(strlen($dot) == 8){
			$dot = substr($dot, 0, 4);
		}else if(strlen($dot) == 4){
			$dot = "20".substr($dot, 2 ,2);
		}else{
			$dot = null;
		}
		return $dot;
	}
	public function ferhat_deger_filter($array_items, $item_exist, $urun_min = 0){
		$array_item_result = [];
		$model_array = array_column($array_items,'model');
		$on_set = [];
		$result_set = $this->db->select('id,stok_kodu,fd_entegrasyon,fiyat,stok_miktari,fd_onoff')->from('fd_ent_exist')->get()->result();
		foreach($array_items as $item){
			if($item['quantity'] >= $urun_min){
					$exist_key = $this->search_fd_array($item['model'],$result_set);

					$exist = false;
					if($exist_key){
						$exist = $result_set[$exist_key];
						if($exist->fd_onoff == false){
							//array_push($on_set,$exist);
						}
						unset($result_set[$exist_key]);
					}
					if(!$item_exist && $exist==false){
						array_push($array_item_result,$item);
					}

					else if($exist->stok_kodu){
							$item['id'] = $exist->id;
							$item['mevcut_fiyat'] = $exist->fiyat;
							$item['stok_miktar'] = $exist->stok_miktari;
							$item['price'] = round(((float)$item['price'])/0.87);
							$item['dot'] = $this->clearDot($item['dot']);
							if($item['dot'] == null){
								unset($item['dot']);
							}
							if($exist->fd_entegrasyon){
								if($item['quantity'] < $urun_min){
									array_push($result_set,$exist);
								}else if($item['quantity'] >= $urun_min && $exist->stok_miktari<$urun_min){
									array_push($on_set,$exist);
									array_push($array_item_result,$item);
								}else if($item['quantity'] >= $urun_min && $exist->stok_miktari>=$urun_min){
									if(((float)$item['price'])<((float)$item['mevcut_fiyat'])){
										echo '<p>'.$item['price'].'--*--'.$item['mevcut_fiyat'].'</p>';
										array_push($on_set,$exist);
										array_push($array_item_result,$item);
									}else{
										//array_push($result_set,$exist);
										if($exist->fd_onoff == 1){
											array_push($array_item_result,$item);
										}
									}
								}else{
									//array_push($result_set,$exist);
								}
							}
					}else{
						
					}
			}		
		}
	

		//$this->items_toggle_from_fd($result_set,false);
		$this->items_toggle_from_fd($on_set,true);
		return $array_item_result;
	}
	function search_fd_array($id, $array) {
	   foreach ($array as $k => $val) {
		   if($val->fd_entegrasyon == $id){
			   return $k;
		   }
	   }
	   return false;
	}

	public function ferhat_deger_urun_isle($item){
		$updated = [];
		$updated['fiyat'] = $item['price'];
		$updated['urun_alis_fiyati'] = ($item['price'] * 0.95);
		$updated['stok_miktari'] = $item['quantity'];
		$updated['tedarikci'] = 'FERHATDEGER';
		if(isset($item['dot'])){
			$updated['uretim_tarihi'] = $item['dot'];
		}
		$update_query = $this->db->where('id =',$item['id'])->update('urunler', $updated);
		echo '<hr>WORKED---ID:'.$item['id'].' URUN ADI:'.$item['urun_adi'].' STOK MIKTARI:'. $item['quantity'].'<hr>';
		
	}
	
	public function items_toggle_from_fd($items,$value = false){
		foreach($items as $item){
			$this->db->where('id =',$item->id);
			$this->db->update('urunler', array('fd_onoff' => $value));
		}
	}

	public function nokian_ent_xml($apiurl,$ne = false){
		ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		$array_items = [];
		$not_exist = [];
		foreach ($xml->Urun as $value){ 
				$exist = $this->db->select('id,stok_kodu,nokian_ent,stok_miktari')->from('urunler')->where('marka = 11')->where('nokian_ent =',$value->Kod)->get()->row();
				if($exist->id){
					$value->id = $exist->id;
					array_push($array_items,$value);
				}else if($ne){
					array_push($not_exist,$value);
				}
		} 
		if($ne){
			echo json_encode($not_exist);
			die();
		}
		$nokian_kar_marjlari = $this->extraservices->ayarlar(array('name' => 'nokian_kar_marji_orani'), TRUE)->value;
		$nokian_kar_marjlari = (array) json_decode($nokian_kar_marjlari);

		$nokian_iskonto_marjlari = $this->extraservices->ayarlar(array('name' => 'nokian_alis_maliyet'), TRUE)->value;
		$nokian_iskonto_marjlari = (array) json_decode($nokian_iskonto_marjlari);

		foreach($array_items as $item){	
			$fiyat_t = $item->Fiyat;	
			$uretim_tarihi = 2000;
			$yakit = null;
			$gurultu = null;
			$zemin = null;
			foreach($item->Kategori->Ozellikler->Ozellik as $ozellik){
				if($ozellik['no'] == '354080041'){
					$uretim_tarihi = $ozellik[0];
				}
				if($ozellik['no'] == '354080049'){
					$yakit = $ozellik[0];
				}
				if($ozellik['no'] == '354080051'){
					$gurultu = $ozellik[0];
				}
				if($ozellik['no'] == '354080029'){
					$zemin = $ozellik[0];
				}
			}
			$liste_fiyati = $fiyat_t;
			$marj_carpan = $nokian_kar_marjlari[''.$uretim_tarihi];
			$fiyat_t = ($fiyat_t * $marj_carpan);
			$fiyat_t = ceil($fiyat_t);
			$isk_carpan = $nokian_iskonto_marjlari[''.$uretim_tarihi];
			$fiyat_alis = ($liste_fiyati * $isk_carpan);
			$fiyat_alis = str_replace(',','.',round((float)$fiyat_alis, 2));
			$this->db->where('id =',$item->id);
			if($item->HazirlamaSuresi > 3){
				$this->db->update('urunler',array('stok_miktari' => 0, 'fiyat' => $fiyat_t,'liste_fiyati' => $liste_fiyati,'urun_alis_fiyati' => $fiyat_alis ,'uretim_tarihi' => $uretim_tarihi, 'apizaman' => time(), 'tedarikci' => 'YORKAN'));
			}else{
				$this->db->update('urunler',array('stok_miktari' => $item->Stoklar->Stok->Miktar, 'fiyat' => $fiyat_t,'liste_fiyati' => $liste_fiyati,'urun_alis_fiyati' => $fiyat_alis , 'uretim_tarihi' => $uretim_tarihi, 'tedarikci' => 'YORKAN', 'islak_yol_tutusu' => $zemin, 'gurultu_seviyesi' => $gurultu,'yakit_sarfiyati'=>$yakit, 'apizaman' => time()));
			}
		}
	}
	
	public function nokian_ent_xml_all($apiurl){
		ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		$array_items = [];
		foreach ($xml->Urun as $value){ 
			$item = [];
			$item['adi'] = $value->Baslik;
			$item['kod'] = $value->Kod;
			foreach($value->Kategori->Ozellikler->Ozellik as $ozellik){
				$ozellik = (array) $ozellik;
				$item[$ozellik['@attributes']['isim']] = $ozellik[0];
			}
			array_push($array_items,$item);
		} 
		
		foreach($array_items as $item){				
			echo json_encode($item);
			echo ',';
		}	
	}
	public function freze_ent_xml_all($apiurl){
		ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		$array_items = [];
		$freze_urunler = $this->db->select('stok_kodu')->from('urunler')->where('tedarikci','FREZE')->get()->result_array();
		$freze_urunler = array_map (function($value){
		    return (string)$value['stok_kodu'];
		} , $freze_urunler);
		$guncellenecekler = [];
		foreach ($xml->Urun as $value){ 
			$item = [];
			//$item['adi'] = (string)$value->Baslik;
			$item['kod'] = str_replace(PHP_EOL, "", (string)$value->Kod);
			$item['urun_alis_fiyati'] = ceil(floatval((string)$value->byfiyat) + 8);
			$item['fiyat'] = ceil(floatval((string)$value->skfiyat) + 8);
			$item['stok_miktari'] = (string)$value->StokAdedi;
			$item['liste_fiyati'] = 1.2 * (floatval($value->skfiyat) + 8);
			$kod = $item['kod'];
			if(in_array($kod,$freze_urunler)){
				$guncellenecekler[] = $item;
			}
		} 
		$this->update_freze_urunler($guncellenecekler);
	}
	public function update_freze_urunler($urun_list){
		foreach($urun_list as $urun){
			$update_urun = $this->db->where('stok_kodu',$urun['kod']);
			unset($urun['kod']);
			$update_urun = $update_urun->where('tip','aksesuar')->update('urunler',$urun);
		}
	}
	public function senturk_ent_xml_all($apiurl){
		ini_set('memory_limit', '2G');
		ini_set('max_execution_time', 600);
		if(!$apiurl){
			return 'Url isn\'t defined';
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $apiurl);
		$response = curl_exec($ch);
		curl_close($ch);
		$xml = simplexml_load_string($response);
		$array_items = [];
		$senturk_urunler = $this->db->select('stok_kodu')->from('urunler')->where('tedarikci','SENTURK')->order_by('apizaman')->limit(200)->get()->result_array();
		$senturk_urunler = array_map (function($value){
		    return (string)str_replace('SE', '', $value['stok_kodu']);
		} , $senturk_urunler);
		foreach ($xml->urun as $value){ 
			$item = [];
			//$item['adi'] = (string)$value->Baslik;
			$item['kod'] = str_replace(PHP_EOL, "", (string)$value->id);
			$item['urun_alis_fiyati'] = ceil(floatval((string)$value->urunindirimlifiyat)) * 0.85;
			$item['fiyat'] = ceil(floatval((string)$value->urunindirimlifiyat) + 8);
			$item['stok_miktari'] = (string)$value->Quantity;
			$item['liste_fiyati'] = 1.3 * ceil(floatval((string)$value->urunindirimlifiyat))  + 8;
			$kod = $item['kod'];
			if(in_array($kod,$senturk_urunler)){
				$guncellenecekler[] = $item;
			}
		} 
		$this->update_senturk_urunler($guncellenecekler);
	}

	public function update_senturk_urunler($urun_list){
		foreach($urun_list as $urun){
			$urun['apizaman'] = time();
			$update_urun = $this->db->where('stok_kodu','SE'.$urun['kod'])->where('tedarikci','SENTURK');
			unset($urun['kod']);
			$update_urun = $update_urun->where('tip','aksesuar')->update('urunler',$urun);
		}
	}
}

?>
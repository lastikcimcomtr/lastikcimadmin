<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Importers extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
    	ini_set('memory_limit', '2G');
      ini_set('upload_max_filesize', '64M');
      ini_set('post_max_size', '64M');

		  ini_set('max_execution_time', 600);

      parent::__construct();
      $this->theme_url = $this->parser->theme_url();
      $this->load->model('Importers_model', 'importers');
  }

  public function yeniaktarim()
  {
      $basla = microtime();
      $this->load->library('Tatkomiktar');
      $aktarimlar = $this->tatkomiktar->aktarimyeni();
      foreach ($aktarimlar as $aktarim) if (strlen($aktarim['stok']) > 6) {

          /**/
          $fiyat    = $aktarim['fiyat'];
          $fiyat_s  = $this->tatkomiktar->fiyathesapla(NULL, NULL, $aktarim['fiyat']);
          $miktar   = $this->tatkomiktar->miktarhesapla($aktarim['miktar']);
          /**/

          if ($fiyat_s > 0) {
              if ($miktar > 0) {

                  $updata = array();
                  $updata['urun_alis_fiyati'] = $fiyat;
                  $updata['fiyat']            = $fiyat_s;
                  $updata['stok_miktari']     = $miktar;
                  $this->tatkomiktar->guncelle($aktarim['stok'], $updata);

              } else {

                  $updata = array();
                  $updata['stok_miktari']     = $miktar;
                  $this->tatkomiktar->guncelle($aktarim['stok'], $updata);

              }
              echo $aktarim['stok']." - Güncellendi. <br />";
              usleep(250000);
          }
      }

      $son = microtime();

      echo abs($basla - $son)." saniyede yüklendi";

  }

  public function miktarlar()
  {
    $this->load->library('Tatkomiktar');
    // $this->tatkomiktar->stokkodlari();
  }

  public function mikro($category = NULL)
  {
     if ($category) {
         $this->load->library('Tatko');

         if ($this->input->method() == "post") {
          $sdata = $this->input->post('sdata');
          $sdata['min'] = 1;
      } else {
          $sdata = array();
          $sdata['min'] = 1;
      }

      $stoklar = $this->tatko->stokAraYeni($sdata);
      $stoklar = json_decode($stoklar);

      $kategoriler 	= $this->tatko->stokAraYeni(array('select' => 'distinct dbo.fn_AnaGrupIsmi(sto_anagrup_kod) as kategori, sto_anagrup_kod as kod', 'min' => 1));
      $kategoriler 	= json_decode($kategoriler);

      $markalar 		= $this->tatko->stokAraYeni(array('select' => 'distinct dbo.fn_MarkaIsmi(sto_marka_kodu) as marka, sto_marka_kodu as kod', 'min' => 1));
      $markalar 		= json_decode($markalar);

      $desenler 		= $this->tatko->stokAraYeni(array('select' => 'distinct lastikdeseni', 'min' => 1));
      $desenler 		= json_decode($desenler);

      $tabanlar 		= $this->tatko->stokAraYeni(array('select' => 'distinct tabani', 'min' => 1));
      $tabanlar 		= json_decode($tabanlar);

      $yanaklar		= $this->tatko->stokAraYeni(array('select' => 'distinct yanagi', 'min' => 1));
      $yanaklar		= json_decode($yanaklar);

      $jantlar		= $this->tatko->stokAraYeni(array('select' => 'distinct jantcapi', 'min' => 1));
      $jantlar		= json_decode($jantlar);

      $data = array();
      $data['sdata'] 			= $sdata;
      $data['theme_url']  	= $this->theme_url;
      $data['stoklar']    	= $stoklar->results;
      $data['kategoriler']	= $kategoriler->results;
      $data['markalar']   	= $markalar->results;
      $data['desenler']   	= $desenler->results;
      $data['tabanlar']   	= $tabanlar->results;
      $data['yanaklar']   	= $yanaklar->results;
      $data['jantlar']    	= $jantlar->results;

      $data['secilikategori']	= $category;
      $data['parametreler'] 	= $this->importers->kategoridenevar($category);

      $this->parser->parse("mikro", $data);
  } else {
      redirect($_SERVER['HTTP_REFERER']);
  }
}

public function insert($method = NULL, $category = NULL)
{
 $data = array();
 if ($this->input->method() == "post") {
  $pdata = $this->input->post();
  switch ($method) {
   case 'mikro':
   $data = $this->importers->mikro($pdata, $category);
   break;

   case 'tatko':
   $data = $this->importers->tatko($pdata, $category);
   break;

   default:
   $data['status'] = "danger";
   $data['message'] = "Ürünler eklenirken sorun çıktı.";
   break;
}

} else {
  $data['status'] = "danger";
  $data['message'] = "Ürünler eklenirken sorun çıktı.";
}

echo json_encode($data);
}

public function xml($id = NULL)
{
    	if ($id && is_numeric($id)) {

    		$dosya = $this->importers->aktarimdakiler(array('id' => $id, 'tip' => 'xml'), TRUE);
    		if ($dosya) {

    			$this->load->model('products/Products_model', 'products');
               $kurlar = $this->products->dovizkurlari();
               if(substr($dosya->dosya, 0, 4 ) === "http"){
                $xml = simplexml_load_file($dosya->dosya);
            }else{
               $xml = simplexml_load_file(".".$this->extraservices->ayarlar(array('name' => 'xml_dosya'), TRUE)->value.$dosya->dosya);
           }
           $arr_data = array();
           $arr_data[$xml->getName()] = array();


           foreach ($xml->children() as $child)
           {
            $arr_data[$xml->getName()][$child->getName()][] = $child;
        }
        $kimdenalalim = array_keys($arr_data[$xml->getName()]);

        $kimdenalalim = $arr_data[$xml->getName()][$kimdenalalim[0]];

        $parametreler = $this->importers->kategoridenevar();

        if (count($kimdenalalim) > 0) {

         reset($kimdenalalim);
         list($fkey, $fvalue) = each($kimdenalalim);
         $anahtarlar = array_keys((array)$kimdenalalim[$fkey]);

         $data = array();
         $data['ornekveri']		= $kimdenalalim[$fkey];
         $data['anahtarlar'] 	= $anahtarlar;
         $data['veriler']		= $arr_data;
         $data['parametreler'] 	= $parametreler;
         $data['kurlar']			= $kurlar;
         $data['theme_url']  	= $this->theme_url;

         if ($this->input->method() == "post") {
          $pdata = $this->input->post();
          $res = $this->importers->duzenle($id, $pdata);
          if ($res) {
           $dosya = $this->importers->aktarimdakiler(array('id' => $id, 'tip' => 'xml'), TRUE);

           $data['status'] = "success";
           $data['message']= "Parametreler başarılı bir şekilde kayıt edilmiştir.";
       } else {
           $data['status'] = "danger";
           $data['message']= "Parametreler düzenlenirken bir hata meydana geldi.";
       }
   }
   $data['dosya']			= $dosya;
   $this->parser->parse("xml", $data);
}

} else {
   redirect("importers/xml");
}

} else {
  $data = array();
  $data['xmls'] 	= $this->importers->aktarimdakiler(array('tip' => 'xml'));
  $data['theme_url'] 	= $this->theme_url;
  $this->parser->parse("xml_list", $data);
}
}

public function excel($id = NULL)
{
 if ($id && is_numeric($id)) {

  $dosya = $this->importers->aktarimdakiler(array('id' => $id, 'tip' => 'excel'), TRUE);
  if ($dosya) {

   $this->load->model('products/Products_model', 'products');
   $kurlar = $this->products->dovizkurlari();

   $this->load->library('excel');

   $objPHPExcel = PHPExcel_IOFactory::load(".".$this->extraservices->ayarlar(array('name' => 'excel_dosya'), TRUE)->value.$dosya->dosya);

   $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

   foreach ($cell_collection as $cell) {
    $column 	= $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
    $row 		= $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

    $arr_data[$row][$column] = $data_value;
}

$parametreler = $this->importers->kategoridenevar();

if (count($arr_data) > 0) {
 $anahtarlar = array_keys($arr_data[1]);
 $data = array();
 $data['anahtarlar'] 	= $anahtarlar;
 $data['veriler']		= $arr_data;
 $data['parametreler'] 	= $parametreler;
 $data['kurlar']			= $kurlar;
 $data['theme_url']  	= $this->theme_url;
 if ($this->input->method() == "post") {
  $pdata = $this->input->post();

  $res = $this->importers->duzenle($id, $pdata);
  if ($res) {
   $dosya = $this->importers->aktarimdakiler(array('id' => $id, 'tip' => 'excel'), TRUE);

   $data['status'] = "success";
   $data['message']= "Parametreler başarılı bir şekilde kayıt edilmiştir.";
} else {
   $data['status'] = "danger";
   $data['message']= "Parametreler düzenlenirken bir hata meydana geldi.";
}
}
$data['dosya']			= $dosya;
$this->parser->parse("excel", $data);
}

} else {
   redirect("importers/excel");
}

} else {
  $data = array();
  $data['excels'] 	= $this->importers->aktarimdakiler(array('tip' => 'excel'));
  $data['theme_url'] 	= $this->theme_url;
  $this->parser->parse("excel_list", $data);
}
}

public function liste($id , $cat = null){
// kullanmıyoruz burayı
 if(is_null($cat)){
  $this->load->model('products/Category_model', 'category');
  $data['kategoriler'] = $this->category->kategoriler(array('tip'=>'lastik','ust'=>0));
  $data['theme_url'] 	= $this->theme_url;
  $data['id'] 	= $id;
  $this->parser->parse("categorisec", $data);
}else if(is_numeric($id)){
  $dosya = $this->importers->aktarimdakiler(array('id' => $id), TRUE);
  $excelparam = (array)json_decode($dosya->parametreler);
  $excelparam = array_flip($excelparam);
  $parametreler = $this->importers->kategoridenevar( $cat );
  $data['theme_url'] 	= $this->theme_url;
  $data['kategori'] 	= $cat;
  foreach ($parametreler as $key => $value) {
    $data['parametreler'][$value->silik_id] =  $value;
}
$this->load->model('products/Products_model', 'products');
$data['kurlar'] = $this->products->dovizkurlari();

$this->load->model('products/Brand_model', 'brand');
$data['markalar'] = $this->brand->markalar();
switch ($dosya->tip) {
   case 'excel':

   $this->load->library('excel');


   $objPHPExcel = PHPExcel_IOFactory::load(".".$this->extraservices->ayarlar(array('name' => 'excel_dosya'), TRUE)->value.$dosya->dosya);

   $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

   foreach ($cell_collection as $cell) {
    $column 	= $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
    if($excelparam[$column]){
     $row 		= $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
     $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

     $data['veriler'][$row][$excelparam[$column]] = $data_value;
 }
}

break;

default:
redirect("importers/excel");
break;
}
$this->parser->parse("liste", $data);

} else {
  redirect("importers/excel");
}

}

public function add_excel()
{

      $this->importers->add_excel();

}


public function kormetalxml($id ,$sayfa = 1, $tip = 'jant' ){

	if (is_numeric($id)) {

		$dosya = $this->importers->aktarimdakiler(array('id' => $id, 'tip' => 'xml'), TRUE);
		if ($dosya) {

			$this->load->model('products/Products_model', 'products');
			$kurlar = $this->products->dovizkurlari();
			if(substr($dosya->dosya, 0, 4 ) === "http"){
				$xml = simplexml_load_file($dosya->dosya);
			}else{
				$xml = simplexml_load_file(".".$this->extraservices->ayarlar(array('name' => 'xml_dosya'), TRUE)->value.$dosya->dosya);
			}

			foreach ($xml->children() as $child) {
				$urun = array();

				if( $child->Kategori['no'] == "1002976"){
					$ozellikler = $child->Kategori->Ozellikler->Ozellik;
					foreach ($ozellikler as $key => $value) {
						if(is_object($value['isim']) &&  $value['isim']->__toString() == "Marka"){
							$urun['marka'] = trim($value->__toString());
						}
						if(is_object($value['isim']) &&  $value['isim']->__toString() == "Jant Çapı (Inç)"){
							$jant_capi = trim(str_replace("İnç", "", $value->__toString()));
						}
						if(is_object($value['isim']) &&  $value['isim']->__toString() == "Jant Genişliği"){
							$jant_eni = trim(str_replace("İnç", "", $value->__toString()));
						} if(is_object($value['isim']) &&  $value['isim']->__toString() == "Bijon Sayısı"){
							$bijon_adeti = trim(str_replace("İnç", "", $value->__toString()));
						} if(is_object($value['isim']) &&  $value['isim']->__toString() == "Bijon Aralığı"){
							$bijon_aralik = trim(str_replace("İnç", "", $value->__toString()));
						}
						if(is_object($value->isim) &&  $value->isim->__toString() == "Renk"){
							$urun['renk'] = trim(str_replace("İnç", "", $value->__toString()));
						}
					}

					$resimler = $child->Resimler->Resim;
					$resim = array();
					foreach ($resimler as $key => $value) {
						$resim[] = $value->__toString();
					}
					list($urun['resim_1'],$urun['resim_2'],$urun['resim_3'],$urun['resim_4'],$urun['resim_5']) = $resim;
					$urun['stok_kodu'] = $child->Kod->__toString();
					$urun['stok_miktari'] = $child->Stoklar->Stok->Miktar->__toString();
					$urun['fiyat'] = round($child->Fiyat->__toString());
					$urun['tip'] = 'jant';
					$urun['urun_adi'] = $child->Baslik->__toString();
					$urun['seo_baslik'] = $child->Baslik->__toString();
					$urun['kategori'] = 102;
					$urun['ebat'] = $jant_eni."X".$jant_capi;
					$urun['pcd'] = $bijon_adeti."X".$bijon_aralik;
					$urun['et']=reset(array_filter(preg_split("/\D+/",  end(explode("ET",$urun['seo_baslik'])))));
					$urun['etiket_1'] = $jant_eni."x".$jant_capi ." " .$bijon_adeti."x".$bijon_aralik . " ET".$urun['et'];
					$urun['etiket_2'] = $jant_eni."x".$jant_capi ." " .$bijon_adeti."x".$bijon_aralik ;
					$data['urun'][]=$urun;
				}
			}
			$limit = 20;
			$start = ($sayfa - 1) * $limit;
			$end = $start + $limit;
			foreach ($data['urun'] as $key => $value) {
				if($key > $end || $key < $start){
					unset($data['urun'][$key]);
				}

			}

			if(!is_null($message)){
				echo  $message;
			}else{
				$veri = $this->importers->savelist($data) ;
				if($veri['update'] > 0 ) {
					echo $veri['update'] . " adet urun güncellendi ";
				}
				if($veri['add'] > 0 ) {
					echo $veri['add'] . " adet urun eklendi";
				}
				if(($veri['add']  +  $veri['update']) == 0 ) {
					echo "Hiçbir işlem yapılmadı";
				}

			}
		}
	}
}

public function listekaydet($id){

 if(is_numeric($id)){
  $dosya = $this->importers->aktarimdakiler(array('id' => $id), TRUE);
  $excelparam = (array)json_decode($dosya->parametreler);
  foreach ($excelparam as $key => $params) {
    if(is_array($params)){
        foreach ($params as $key2 => $param) {
           $excelparam[$key."_".$key2 ]= $param;
       }
       unset($excelparam[$key]);
   }
}
$excelparam = array_flip($excelparam);
$data['marka'] = $dosya->marka;
switch ($dosya->tip) {
   case 'excel':
   $this->load->library('excel');
   $objPHPExcel = PHPExcel_IOFactory::load(".".$this->extraservices->ayarlar(array('name' => 'excel_dosya'), TRUE)->value.$dosya->dosya);
   $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
   foreach ($cell_collection as $cell) {
    $column 	= $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
    if($excelparam[$column]){

     $row 		= $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
     $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
     $data['urun'][$row][$excelparam[$column]] = $data_value;
 }
}
$key_parameters = array_keys($data['urun'][1]);
foreach ($data['urun'] as $key => $urun_line) {
  $urun_keys = array_keys($urun_line);
  $diff = array_diff($key_parameters, $urun_keys);
  if(sizeof($diff) > 0){
    foreach ($diff as $diff_value) {
      $data['urun'][$key][$diff_value] = '';
    }
  }
}
break;
default:
$message =  "Bu dosya tipi Ayarlanmamış";
break;
}
}
else{
  $message = "İçerik Bulunamadı";

}

if($message){
 echo  $message;
}else{
  $veri = $this->importers->savelist($data) ;
  if(isset($veri['urun'])){
    if($veri['status'] == 'error'){
        echo $veri['message'];
    }
  }
  if($veri['update'] > 0 ) {
   echo $veri['update'] . " adet urun güncellendi ";
}
if($veri['add'] > 0 ) {
   echo $veri['add'] . " adet urun eklendi";
}
if($veri['add']  +  $veri['update'] == 0 ) {
   echo "Hiçbir işlem yapılmadı";
}

}

}

public function tuf($page = 1){
  $this->load->model('products/Products_model', 'products');
  $this->products->tumurunsefle($page);

}


public function remove($id = NULL)
{
 if ($id && is_numeric($id)) {
  $this->importers->sil($id);
}
redirect($_SERVER['HTTP_REFERER']);
}

private function satisfiyatihesapla($jant = NULL, $marka = NULL, $fiyat = NULL)
{
	switch ($marka) {
		/*
		case 'MICHELIN':
		case 'GOOD':
		case 'PIR':
		case 'CONT':
		case 'BRD':
		case 'LAS':
		case 'KLB':
			if ($jant >= 13 && $jant <= 14) {
				$oran = 5;
			} elseif ($jant >= 15 && $jant <= 16) {
				$oran = 5;
			} else {
				$oran = 5;
			}
		break;

		case 'TAURUS':
		case 'STRIAL':
		case 'WTF':
		case 'MAR':
		case 'VTK':
		case 'ZTM':
		case 'MIL':
			if ($jant >= 13 && $jant <= 14) {
				$oran = 10;
			} elseif ($jant >= 15 && $jant <= 16) {
				$oran = 10;
			} else {
				$oran = 15;
			}
		break;

		case 'NO':
		case 'PET':
		case 'STAR':
		case 'BFGOODRICH':
			if ($jant >= 13 && $jant <= 14) {
				$oran = 10;
			} elseif ($jant >= 15 && $jant <= 16) {
				$oran = 10;
			} else {
				$oran = 15;
			}
		break;
		*/
        case 'MICHELIN':
            $oran = 5;
            break;
        case 'GOOD':
            $oran = 5;
            break;
        case 'PIR':
            $oran = 5;
            break;
        case 'CONT':
            $oran = 5;
            break;
        case 'LAS':
            $oran = 5;
            break;
        case 'BRD':
            $oran = 5;
            break;
        case 'PET':
            $oran = 5;
            break;
        case 'HAN':
            $oran = 5;
            break;
        case 'NO':
            $oran = 5;
            break;
        case 'BFGOODRICH':
            $oran = 5;
            break;
        case 'SA':
            $oran = 5;
            break;
        case 'DBC':
            $oran = 5;
            break;
        case 'KLB':
            $oran = 5;
            break;

        case 'MAR':
            $oran = 5;
            break;
        case 'STRIAL':
            $oran = 5;
            break;
        case 'VITOUR':
            $oran = 5;
            break;
        case 'GDR':
            $oran = 5;
            break;
        default:
            $oran = 5;
            break;
	}

	$fiyat = $fiyat / ((100-$oran)/100);
  	$fiyat = $fiyat;

  	/*24.01.2018*/
  	$kdvekle = (($fiyat * 18)/100);
  	$fiyat = $fiyat+$kdvekle;
  	/*24.01.2018*/

  	$fiyat = round($fiyat, 0, PHP_ROUND_HALF_DOWN);
	return $fiyat;
}

public function impotest(){
  $this->load->library('tatko');
  $data = $this->tatko->markalar();
  echo $data;
}

public function mikroupdate()
{
    $this->load->library('tatko');
        $aktarimlar = $this->tatko->aktarimlar();
        $aktarimlar = json_decode($aktarimlar);
        $aktarimlar = $aktarimlar->results;
        if (count($aktarimlar) > 0) {

            foreach ($aktarimlar as $aktarim) {

                /**/
                $stok = ($this->tatko->stokAraYeni(array('sto_kod' => $aktarim->keydata), TRUE));
                $stok = json_decode($stok);
                $stok = $stok->results[0];

                if ($stok->miktar < 0) {
                	$stok->miktar = 0;
                }

                var_dump($stok);


                if ($stok) {
                	$stok_jant_cp = filter_var($stok->jantcapi, FILTER_SANITIZE_NUMBER_INT);
                    $stok_kodu    = $stok->sto_kod;
                    $stok_marka   = $stok->sto_marka_kodu;
                    $stok_miktari = $stok->miktar;
                    $stok_fiyati  = $stok->fiyat;
                    $stok_fiyatb  = $stok->doviz;
                    $stok_satis_fiyati = $this->satisfiyatihesapla($stok_jant_cp, $stok_marka, $stok_fiyati);

                    if ($stok_satis_fiyati > 0) {
                    	/*Fiyat 0'dan Büyük ise*/
	                    switch ($stok_fiyatb) {
	                        case 'TL':
	                        	$stok_fiyatb = "TRY";
	                        break;

	                        default:
	                          $stok_fiyatb = "TRY";
	                        break;
	                    }

	                    $res = $this->importers->mikrodanupdate($stok_kodu, array('stok_miktari' => $stok_miktari, 'urun_alis_fiyati' => $stok_fiyati, 'urun_alis_fiyati_birim' => $stok_fiyatb, 'fiyat' => $stok_satis_fiyati, 'fiyat_birim' => $stok_fiyatb));

	                    if ($res) {
	                        $ures = $this->tatko->updateaktarim($aktarim->aktarimrecno);
	                    } else {
	                        $ures = FALSE;
	                    }
	                    /*Fiyat 0'dan Büyük ise*/
	                } else {
	                	$ures = $this->tatko->updateaktarim($aktarim->aktarimrecno);
	                }

                } else {
                    $ures = $this->tatko->updateaktarim($aktarim->aktarimrecno);
                }
                /**/

            }

        } else {
            echo "Aktarımda Bekleyen İşlem Yok";
        }

    }
	public function ferhat_deger(){
		$result = $this->importers->ferhat_deger_xml(1,'https://b2b.degeras.com/xmls/araç.xml',true);
		echo $result;
	}
  public function ferhat_close_removed(){
    $result = $this->importers->ferhat_close_removed('https://b2b.degeras.com/xmls/araç.xml');
    echo $result;
  }
	public function ferhat_deger_ne(){
		$result = $this->importers->ferhat_deger_xml(1,'https://b2b.degeras.com/xmls/araç.xml',false);
		echo $result;
	}
	
	public function molla_ent(){
		$result = $this->importers->molla_xml('https://www.b2b-mollaoglu.com/index.php?url=xml_export/sistem2',true);
		echo $result;
	}
	public function nokian_enteg(){
		$result = $this->importers->nokian_ent_xml('http://kmwheels.net/xml/STANDARTLASTIK.xml');
		echo $result;
	}
	
	public function nokian_ne(){
		$result = $this->importers->nokian_ent_xml('http://kmwheels.net/xml/STANDARTLASTIK.xml',true);
		echo $result;
	}
	public function nokian_all(){
		$result = $this->importers->nokian_ent_xml_all('http://kmwheels.net/xml/STANDARTLASTIK.xml');
		echo $result;		
	}
  public function freze_all(){
    $result = $this->importers->freze_ent_xml_all('https://www.freze.com.tr/image/xml/urunler.xml');
    echo $result;   
  }
  public function senturk_all(){
    $result = $this->importers->senturk_ent_xml_all('https://www.senturkotoaksesuar.com/outputxml/index.php?xml_service_id=4');
    echo $result;   
  }
  public function update_urun_kategories(){
    $updates = file_get_contents('http://admin.lastikcim.com.tr/temalar/tema/assets/kategori_update.json');
    $updates = json_decode($updates);
    $categories = [];
    foreach($updates as $item)
    { 
        $categories[$item->kategori][] = $item;
    }
    foreach ($categories as $key => $category) {
       $categories[$key] =   array_map(create_function('$o', 'return $o->stok_kodu;'), $category);
    }
    //print_r($updates);
    foreach ($categories as  $key => $category) {
      $update = $this->db->where_in('stok_kodu',$category)->where('tedarikci','senturk')->update('urunler',['kategori' => $key]);
    }
    die();
    foreach($updates as $item){
      $update = $this->db->where('stok_kodu',$item->stok_kodu)->where('tedarikci','senturk')->update('urunler',['kategori' => $item->kategori]);
    }
  }
	
	public function add_hizmetler(){
		$hizmetler = [['isim'=>'standart_montaj','fiyat'=>18],['isim'=>'hp_montaj','fiyat'=>18],['isim'=>'uhp_montaj','fiyat'=>22]];
		$firmalar = [58,59,60,62,64,66,72,76,77,78,79,83,84,87,88,89,259,260,92,94,96,97,98,99,101,102,104,105,106,107,110,111,113,114,115,117,118,119,120,122,123,124,126,127,128,129,130,131,133,134,135,136,137,140,141,142,145,146,148,150,151,152,155,156,157,158,159,161,162,163,165,166,167,169,171,172,173,175,176,177,178,179,180,182,183,184,185,186,190,193,194,196,197,202,203,204,205,206,207,208,211,212,262,214,215,216,217,218,219,220,221,222,223,224,261,226,227];
		foreach ($firmalar as $firma){
			foreach($hizmetler as $hizmet){
				$item = [];
				$item['firma_id'] = $firma;
				$item['id_str'] = $hizmet['isim'];
				$item['fiyat'] = $hizmet['fiyat'];
				$this->db->insert('montaj_noktasi_hizmetleri', $item);
			}
		}
	}

  public function etiket_degistir(){
    ini_set('max_execution_time', 60000);
    $updates = file_get_contents('http://admin.lastikcim.com.tr/temalar/tema/assets/etiket_degisecek.json');
    $updates = json_decode($updates);
  
    foreach($updates as $item){
      $v_full = ','.$item->eski_id.',';
      $v_full_yeni = ','.$item->yeni_id.',';
      $v_before = ','.$item->eski_id;
      $v_before_yeni = ','.$item->yeni_id;
      $v_end = $item->eski_id.',';
      $v_end_yeni = $item->yeni_id.',';
      $replace_1 = "REPLACE(etiket,'".$v_full."','".$v_full_yeni."')";
      $replace_2 = "ReplaceLast(etiket,'".$v_before."','".$v_before_yeni."')";
      $replace_3 = "ReplaceFirst(etiket,'".$v_end."','".$v_end_yeni."')";
      $update = $this->db->where('etiket like ','%'.$v_full.'%');
      $update = $update->set('etiket',$replace_1,FALSE);
      $update = $update->update('urunler');

      $update = $this->db->where('etiket like ','%'.$v_before.'');
      $update = $update->set('etiket',$replace_2,FALSE);
      $update = $update->update('urunler');
    
      $update = $this->db->where('etiket like ',''.$v_end.'%');
      $update = $update->set('etiket',$replace_3,FALSE);
      $update = $update->update('urunler');  

      $update = $this->db->where('etiket',$item->eski_id);
      $update = $update->set('etiket',$item->yeni_id,FALSE);
      $update = $update->update('urunler');  
      $delete_old = $this->db->where('id',$item->eski_id)->delete('etiketler');
      $delete_slug = $this->db->where('controller like \'%/'.$item->eski_id.'\'')->delete('etiket_slug');
    }
  }
  public function etiket_sil(){
    ini_set('max_execution_time', 60000);
    $updates = file_get_contents('http://admin.lastikcim.com.tr/temalar/tema/assets/etiket_silenecek.json');
    $updates = json_decode($updates);
  
    foreach($updates as $item){
      $v_full = ','.$item->eski_id.',';
      $v_before = ','.$item->eski_id;
      $v_end = $item->eski_id.',';
      $replace_1 = "REPLACE(etiket,'".$v_full."','')";
      $replace_2 = "ReplaceLast(etiket,'".$v_before."','')";
      $replace_3 = "ReplaceFirst(etiket,'".$v_end."','')";
      $update = $this->db->where('etiket like ','%'.$v_full.'%');
      $update = $update->set('etiket',$replace_1,FALSE);
      $update = $update->update('urunler');

      $update = $this->db->where('etiket like ','%'.$v_before.'');
      $update = $update->set('etiket',$replace_2,FALSE);
      $update = $update->update('urunler');
    
      $update = $this->db->where('etiket like ',''.$v_end.'%');
      $update = $update->set('etiket',$replace_3,FALSE);
      $update = $update->update('urunler');  

      $update = $this->db->where('etiket',$item->eski_id);
      $update = $update->set('etiket','NULL',FALSE);
      $update = $update->update('urunler');  
      $delete_old = $this->db->where('id',$item->eski_id)->delete('etiketler');
      $delete_slug = $this->db->where('controller like \'%/'.$item->eski_id.'\'')->delete('etiket_slug');
    }
  }
  public function etiket_update(){
    ini_set('max_execution_time', 60000);
    ini_set('memory_limit','128M');
    $updates = file_get_contents('http://admin.lastikcim.com.tr/temalar/tema/assets/etiket_update.json');
    $updates = json_decode($updates);
    foreach($updates as $item){
      $update = $this->db->where('id',$item->id);
      $update = $update->set('keywords','\''.$item->keywords.'\'',FALSE);
      //$update = $update->set('adi','\''.$item->adi.'\'',FALSE);
      //$update = $update->set('sef_url','\''.$item->sef_url.'\'',FALSE);
      //$update = $update->set('eski_url','\''.$item->eski_url.'\'',FALSE);
      $update = $update->update('etiketler');  
    }
  }
}

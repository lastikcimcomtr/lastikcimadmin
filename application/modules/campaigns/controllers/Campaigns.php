<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Campaigns_model', 'campaigns');
    }

    public function points()
    {
        $data = array();
        $data['theme_url']  = $this->theme_url;
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            foreach ($pdata as $key => $value) {
                $res = $this->extraservices->ayarla($key, $value);

                if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Puan Sistemi Ayarları Başarılı Bir Şekilde Güncellenmiştir.";
                }
            }
        }
        $data['ayarlar']    = $this->extraservices->ayarlar(array('group' => 7));
        $this->parser->parse("points", $data);
    }

    public function specialcampaigns()
    {
        $this->load->model('members/Members_model', 'members');
        
        $data = array();
        $data['theme_url']          = $this->theme_url;
        $data['ozelkampanyalar']    = $this->campaigns->ozelkampanyalar();
        $data['gruplar']    = $this->members->gruplar();
        $this->parser->parse("specialcampaigns", $data);
    }

    public function removespecial($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $this->campaigns->oksil($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function giftvouchers()
    {
        $data = array();
        $data['theme_url']      = $this->theme_url;
        $data['hediyecekleri']  = $this->campaigns->hediyecekleri();
        $this->parser->parse("giftvouchers", $data);
    }

    public function addgiftvoucher()
    {
        $this->load->model('members/Members_model', 'members');

        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['gruplar']    = $this->members->gruplar();
        
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $addres = $this->campaigns->hcekle($pdata);
            if ($addres) {
                $data['status']     = "success";
                $data['message']    = "Hediye çeki başarılı bir şekilde eklenmiştir.";
            } else {
                $data['hediyeceki'] = (object)$pdata;
                $data['status']     = "danger";
                $data['message']    = "Hediye çeki eklenirken hata meydana geldi.";
            }
        }

        $this->parser->parse("addgiftvoucher", $data);
    }
   public function addordergiftvoucher()
    {
        $this->load->model('members/Members_model', 'members');
        $this->load->model('orders/Orders_model', 'orders');

        $data = array();
        $data['theme_url']  = $this->theme_url;
        $data['gruplar']    = $this->members->gruplar();
        
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $addres = $this->campaigns->hcekle($pdata);
			$order =  $this->orders->orders(['id' => $pdata['siparis_id']],true);
			$order_phone = json_decode($order->data)->cep_telefonu;
            if(strlen($order_phone)<=1){
                $order_phone = json_decode($order->data)->kur_cep_telefonu;
            }
			$message = 'Talebinize istinaden tarafiniza '.$pdata['dgr'].'TL tutarinda hediye ceki tanimlanmistir. Kod : '.$pdata['cek_kodu'].'.';
			$this->orders->send_sms($message,[$order_phone]);
            if ($addres) {
                $data['status']     = "success";
                $data['message']    = "Hediye çeki başarılı bir şekilde eklenmiştir.";
            } else {
                $data['hediyeceki'] = (object)$pdata;
                $data['status']     = "danger";
                $data['message']    = "Hediye çeki eklenirken hata meydana geldi.";
            }
        }

        $this->parser->parse("addordgiftvoucher", $data);
    }

    public function editgiftvoucher($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $hediyeceki         = $this->campaigns->hediyecekleri(array('id' => $id), TRUE);
            if ($hediyeceki) {

                $this->load->model('members/Members_model', 'members');
                $data = array();
                $data['theme_url']  = $this->theme_url;
                $data['gruplar']    = $this->members->gruplar();
                
                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $upres = $this->campaigns->hcduzenle($hediyeceki->id, $pdata);
                    if ($upres) {
                        $data['status']     = "success";
                        $data['message']    = "Hediye çeki başarılı bir şekilde düzenlenmiştir.";

                        $hediyeceki         = $this->campaigns->hediyecekleri(array('id' => $id), TRUE);
                    } else {
                        $data['status']     = "danger";
                        $data['message']    = "Hediye çeki düzenlenirken hata meydana geldi.";
                    }
                }

                $data['hediyeceki'] = $hediyeceki;
                $this->parser->parse("editgiftvoucher", $data);

            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function removegiftvoucher($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $this->campaigns->hcsil($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
	
	public function manage(){
		$this->load->model('members/Members_model', 'members');
		$data=[];
		$data['campaigns'] = $this->campaigns->campaigns($id = false);
		$data['theme_url']  = $this->theme_url;
		$data['gruplar']    = $this->members->gruplar();
		$this->parser->parse("campaigns", $data);
	}	
	
	public function addcampaign(){
		$this->load->model('members/Members_model', 'members');
		$data = [];
		if ($this->input->method() == "post") {
			$pdata = [];
			$pdata = $this->input->post();
			$res = $this->campaigns->createcampaign($pdata);
			if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Kampanya Başarı ile Oluşturuldu.";
					echo json_encode($data);
			}
		}else{
			$data['reqgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'req');
			$data['resgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'res');
			$data['theme_url']  = $this->theme_url;
			$data['gruplar']    = $this->members->gruplar();
			$this->parser->parse("addcampaign", $data);
		}
	}
	
	public function editcampaign($id){
		$this->load->model('members/Members_model', 'members');
		$data = [];
		if ($this->input->method() == "post") {
			$pdata = [];
			$pdata = $this->input->post();
			$res = $this->campaigns->editcampaign($pdata);
			if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Kampanya Başarı ile Güncellenmiştir.";
					echo json_encode($data);
			}
		}else{
		$res = $this->campaigns->campaign_groups($id);
		$campaign = $this->campaigns->campaigns($id);
		$data['detail'] = $res;
		$data['mst'] = $campaign;
		$data['theme_url']  = $this->theme_url;
		$data['reqgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'req');
		$data['resgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'res');
		$data['gruplar']    = $this->members->gruplar();
		$this->parser->parse("campaigndetail", $data);
		}
	}
	
	public function removecampaign($id){
			$res = $this->campaigns->removecampaign($id);
			if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Kampanya Başarı ile Silindi.";
					echo json_encode($data);
			}
			return redirect($_SERVER['HTTP_REFERER']);
	}
	
	
	public function reqgroups(){
		$this->load->model('members/Members_model', 'members');
		$data=[];
		$data['reqgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'req');
		$data['theme_url']  = $this->theme_url;
		$data['gruplar']    = $this->members->gruplar();
		$this->parser->parse("reqresgroups", $data);
	}
	
	public function resgroups(){
		$this->load->model('members/Members_model', 'members');
		$data=[];
		$data['reqgroups'] = $this->campaigns->reqresgroups($id = false,$req_res = 'res');
		$data['theme_url']  = $this->theme_url;
		$data['gruplar']    = $this->members->gruplar();
		$this->parser->parse("reqresgroups", $data);
	}
	
	public function editreqresgroup($id){
		$this->load->model('members/Members_model', 'members');
		$data = [];
		if ($this->input->method() == "post") {
			$pdata = $this->input->post()['all'];
			$res = $this->campaigns->editreqresgroup($pdata);
			if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Grup Başarı ile Güncellendi.";
					echo json_encode($data);
			}
		}else{
			$group = $this->campaigns->reqresgroups($id);
			$availables = $this->campaigns->resreq_items($id);
			$detail = $this->campaigns->resreq_items($id,true);
			$data['group'] = $group;
			$data['detail'] = $detail;
			$data['availables'] = $availables;
			$item_search = [];
			$item_search['type'] = ($group->req_res == 'req' ? 1 : 2);
			$data['campaign_items'] = $this->campaigns->campaign_items($item_search,false);
			$data['theme_url']  = $this->theme_url;
			$data['gruplar']    = $this->members->gruplar();
			$this->parser->parse("editreqresgroup", $data);
		}
	}
	
	public function addresreqgroup(){
		if ($this->input->method() == "post") {
            $pdata = $this->input->post()['all'];
			$res = $this->campaigns->savereqresgroup($pdata);
			if ($res) {
                    $data['status']     = "success";
                    $data['message']    = "Grup Başarı ile Oluşturuldu.";
					echo json_encode($data);
			}
		}else{
			$gdata = [];
			$gdata = $this->input->get();
			$this->load->model('members/Members_model', 'members');
			$data = array();
			$data['campaign_items'] = $this->campaigns->campaign_items($gdata,false);
			$data['theme_url']  = $this->theme_url;
			$data['gruplar']    = $this->members->gruplar();
			if($gdata['type']){
				$data['req_res'] = ($gdata['type'] == 1 ? 'req' : 'res');
			}
			$this->parser->parse("addreqresgroup", $data);
		}
	}


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}
	
	public function hediyecekleri($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('hediye_cekleri');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function hcduzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			$this->db->where('id', $id);
			
			$data['bast'] = strtotime($data['bast']." 00:00:00");
			$data['bitt'] = strtotime($data['bitt']." 23:59:59");

			$data['zmn'] = time();
			$data['usr'] = $this->session->userdata('login')->id;
			$res = $this->db->update('hediye_cekleri', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function hcekle($data = NULL)
	{
		if (count($data) > 0) {
			$data['bast'] = strtotime($data['bast']." 00:00:00");
			$data['bitt'] = strtotime($data['bitt']." 23:59:59");

			$data['zmn'] = time();
			$data['usr'] = $this->session->userdata('login')->id;
			$res = $this->db->insert('hediye_cekleri', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function hcsil($id = NULL)
	{
		$this->db->where('id', $id);
		return $this->db->delete('hediye_cekleri');
	}

	public function ozelkampanyalar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('ok.*, mg.grup as uye_grubu_adi');
		$this->db->from('ozel_kampanyalar as ok');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where("ok.".$key, $value);
			}
		}
		$this->db->join('musteri_gruplari as mg', 'mg.id = ok.uye_grubu', 'LEFT');
		$this->db->group_by('ok.id');
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function okduzenle($id = NULL, $data = NULL)
	{
		if ($id && is_numeric($id) && count($data) > 0) {
			$this->db->where('id', $id);
			$data['zaman'] = time();
			$data['kullanici'] = $this->session->userdata('login')->id;
			$res = $this->db->update('ozel_kampanyalar', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function okekle($data = NULL)
	{
		if (count($data) > 0) {
			$data['zaman'] 		= time();
			$data['kullanici'] 	= $this->session->userdata('login')->id;
			$res = $this->db->insert('ozel_kampanyalar', $data);
			return $res;
		} else {
			return FALSE;
		}
	}

	public function oksil($id = NULL)
	{
		$this->db->where('id', $id);
		return $this->db->delete('ozel_kampanyalar');
	}
	
	public function campaigns($id = NULL){
		$res = $this->db->select('kmp.*');
		if($id){
			$res = $res->where('kmp.id',$id);
		}
		$res = $res->from('kampanya kmp')->get();
		if($id){
			return $res->row();
		}else{
			return $res->result();
		}
	}
	public function createcampaign($data = null){
		if($data){
			$cmpdata = [];
			$cmpdata['name'] = $data['company_name'];
			$cmpdata['start_date'] = $data['start_date'];
			$cmpdata['end_date'] = $data['end_date'];
			$cmpdata['description'] = $data['description'];
			$cmpdata['only_user'] = $data['only_user'];	
			$res = $this->db->insert('kampanya', $cmpdata);
			$kmp_id = $this->db->insert_id();
			foreach($data['req_groups'] as $req_group){
				$cmp_rel = [];
				$cmp_rel['kampanya_id'] = $kmp_id;
				$cmp_rel['req_id'] = $req_group;
				$res = $this->db->insert('kampanya_req', $cmp_rel);
			}
			foreach($data['res_groups'] as $res_group){
				$cmp_rel = [];
				$cmp_rel['kampanya_id'] = $kmp_id;
				$cmp_rel['req_id'] = $res_group;
				$res = $this->db->insert('kampanya_req', $cmp_rel);
			}	
			return true;
		}
	}
	public function editcampaign($data = null){
		if($data){
			$kmp_id = $data['id'];
			$cmpdata = [];
			$cmpdata['name'] = $data['company_name'];
			$cmpdata['start_date'] = $data['start_date'];
			$cmpdata['end_date'] = $data['end_date'];
			$cmpdata['description'] = $data['description'];	
			$cmpdata['only_user'] = $data['only_user'];	
			$res = $this->db->where('id', $kmp_id);
			$res = $res->update('kampanya', $cmpdata);
			$del = $this->db->where('kampanya_id',$kmp_id)->delete('kampanya_req');
			foreach($data['req_groups'] as $req_group){
				$cmp_rel = [];
				$cmp_rel['kampanya_id'] = $kmp_id;
				$cmp_rel['req_id'] = $req_group;
				$res = $this->db->insert('kampanya_req', $cmp_rel);
			}
			foreach($data['res_groups'] as $res_group){
				$cmp_rel = [];
				$cmp_rel['kampanya_id'] = $kmp_id;
				$cmp_rel['req_id'] = $res_group;
				$res = $this->db->insert('kampanya_req', $cmp_rel);
			}				
			return true;
		}
	}
	public function removecampaign($id = null){
		if($id){
			$del_1 = $this->db->where('kampanya_id',$id)->delete('kampanya_req');
			$del_2 = $this->db->where('id',$id)->delete('kampanya');
			return true;
		}else return false;
	}
	
	public function reqresgroups($id = NULL,$req_res = NULL){
		$res = $this->db->select('krg.*');
		if($id){
			$res = $res->where('krg.id',$id);
		}
		if($req_res){
			$res = $res->where('req_res',$req_res);
		}
		$res = $res->from('kampanya_req_group krg')->get();
		if($id){
			return $res->row();
		}else{ 
			return $res->result();
		}
	}
	
	public function resreq_items($id = null, $detailed = false){
		if($id){
			if($detailed){
				$res = $this->db->select('*')->from('kampanya_item ki')->from('kampanya_item_req kir')->where('kir.item_id = ki.id')
				->where('kir.req_id',$id)->get()->result();
				return $res;
			}
			else{
				$res = $this->db->select('ki.id')->from('kampanya_item ki')->from('kampanya_item_req kir')->where('kir.item_id = ki.id')
				->where('kir.req_id',$id)->get()->result_array();
				$result = [];
				foreach ($res as $item){
					array_push($result,$item['id']);
				} 
				return $result;
			}
		}
	}
	
	public function campaign_items($data,$row = false){
		$res = $this->db->select('*');
		foreach($data as $key => $var){
			$res = $res->where($key,$var);
		}
		$res = $res->from('kampanya_item')->get();
		if($row){
			return $res->row();
		}else{ 
			return $res->result();
		}
	}
	
	public function campaign_groups($id = null){
		if($id){
			$res = $this->db->select('krg.id')->from('kampanya_req kr')->from('kampanya_req_group krg')->where('krg.id = kr.req_id')->where('kr.kampanya_id',$id)->get()->result_array();
			$result = [];
			foreach ($res as $item){
				array_push($result,$item['id']);
			} 
			return $result;
		}
	}
	
	
	public function savereqresgroup($data = null){
		$data = json_decode($data);
		if($data){
			$check = $this->checkreqresgroup($data);
			if($check){
				$kampanya_req_group = [];
				$kampanya_req_group['group_name'] = $check['group_name'];
				$kampanya_req_group['req_res'] = $check['req_res'];
				$kampanya_req_group['group_type'] = $check['group_type'];
				$res = $this->db->insert('kampanya_req_group', $kampanya_req_group);
				$kmp_grp_id = $this->db->insert_id();
				foreach($check['items'] as $item){
					$new_item = array('item_id' => $item['cmp_item_id'] , 'req_id' => $kmp_grp_id );
					if($item['param_1']){ $new_item['param_1'] = $item['param_1'];}
					if($item['param_2']){ $new_item['param_2'] = $item['param_2'];}
					if($item['exl']){ $new_item['exl'] = ($item['exl'] == 'on' ? 1 : 0);}
					$res = $this->db->insert('kampanya_item_req', $new_item);
				}
				return true;
			}else return $check;
		}
	}
	public function editreqresgroup($data = null){
		$data = json_decode($data);
		if($data){
			$check = $this->checkreqresgroup($data);
			if($check){
				$req_group_id = $check['id'];
				$kampanya_req_group = [];
				$kampanya_req_group['group_name'] = $check['group_name'];
				$kampanya_req_group['req_res'] = $check['req_res'];
				$kampanya_req_group['group_type'] = $check['group_type'];
				$res = $this->db->where('id',$req_group_id)->update('kampanya_req_group', $kampanya_req_group);
				$kmp_grp_id = $req_group_id;
				$this->db->where('req_id',$req_group_id)->delete('kampanya_item_req');
				foreach($check['items'] as $item){
					$new_item = array('item_id' => $item['cmp_item_id'] , 'req_id' => $kmp_grp_id );
					if($item['param_1']){ $new_item['param_1'] = $item['param_1'];}
					if($item['param_2']){ $new_item['param_2'] = $item['param_2'];}
					if($item['exl']){ $new_item['exl'] = ($item['exl'] == 'on' ? 1 : 0);}
					$res = $this->db->insert('kampanya_item_req', $new_item);
				}
				return true;
			}else return $check;
		}	
		
	}
	public function checkreqresgroup($data = null){
		$parser = [];
		$parser['items'] =[];
		foreach($data as $key_i => $data_item){
			$parse_item = [];
			foreach($data_item as $key => $item){
				if($item->name == 'group_type'){
					$parser['group_type'] = $item->value;
				}else if($item->name == 'group_name'){
					$parser['group_name'] = $item->value;
				}else if($item->name == 'req_res'){
					$parser['req_res'] = $item->value;
				}else if($item->name == 'id'){
					$parser['id'] = $item->value;
				}else{
					$parse_item[$item->name] = $item->value;
				}
			}
			if(sizeOf($parse_item) > 0){
				array_push($parser['items'],$parse_item);
			}
		}
		
		if(strlen($parser['group_name']) < 5 ){
			return false;
		}else if($parser['group_type'] !== 'AND' && $parser['group_type'] !== 'OR'){
			return false;
		}else if($parser['req_res'] !== 'req' && $parser['req_res'] !== 'res'){
			return false;
		}else{
			$items = $parser['items'];
			foreach($items as $item){
				if($item['cmp_item_id'] == 1){
					$res = $this->db->select('count(id) cnt')->from('kategoriler')->where('id',(int)$item['param_1'])->get()->row();
					if($res->cnt == 0){
						print_r( array('type' => 'error' , 'message' => 'Invalid Category Id'));
						return false;
					}
					//echo 'Param1:'.$item['param_1'];
				}else if($item['cmp_item_id'] == 2){
					$res = $this->db->select('count(id) cnt')->from('markalar')->where('id',(int)$item['param_1'])->get()->row();
					if($res->cnt == 0){
						print_r( array('type' => 'error' , 'message' => 'Invalid Brand Id'));
						return false;
					}
				}else if($item['cmp_item_id'] == 10){
					$res = $this->db->select('count(id) cnt')->from('urunler')->where('id',(int)$item['param_1'])->get()->row();
					if($res->cnt == 0){
						print_r( array('type' => 'error' , 'message' => 'Invalid Product Id'));
						return false;
					}					
				}
			}
		}
		return $parser;
	}
}

?>
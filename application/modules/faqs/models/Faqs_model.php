<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}
	
	public function kategoriler($sdata = NULL, $row = FALSE) 
	{
		$this->db->select('*');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		$this->db->from('sss_kategoriler');
		$this->db->order_by('sira', 'asc');

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	public function sorular($sdata = NULL, $row = FALSE) 
	{
		$this->db->select('ss.*');
		
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where("ss.".$key, $value);
			}
		}
		$this->db->select('sk.adi as kategori_adi');
		$this->db->join('sss_kategoriler as sk', 'sk.id = ss.kategori', 'LEFT');
		$this->db->from('sss_sorular as ss');
		// $this->db->order_by('ss.sira', 'asc');
		$this->db->order_by('ss.kategori', 'asc');

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	/**/
	public function sil($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('sss_sorular');
	}

	public function ekle($data = NULL)
	{
		if ($data) {
			return $this->db->insert('sss_sorular', $data);
		} else {
			return FALSE;
		}
	}

	public function duzenle($data = NULL, $id = NULL)
	{
		if ($data && $id && is_numeric($id)) {
			$this->db->where('id', $id);
			return $this->db->update('sss_sorular', $data);
		} else {
			return FALSE;
		}
	}
	/**/
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Faqs_model', 'faqs');
    }

    public function index($id = NULL)
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $data['kategoriler'] = $this->faqs->kategoriler();

        if ($id && is_numeric($id)) {
            $kategori = $this->faqs->kategoriler(array('id' => $id), TRUE);
        }

        if ($kategori) {
            $data['detay']      = $kategori;
            $data['sorular']    = $this->faqs->sorular(array('kategori' => $kategori->id));
        } else {
            $data['sorular']    = $this->faqs->sorular();
        }

        $this->parser->parse("index", $data);
    }

    public function add()
    {
    	$data = array();
        $data['theme_url'] = $this->theme_url;
        $data['kategoriler'] = $this->faqs->kategoriler();

        if ($this->input->method() == "post") {
        	$pdata = $this->input->post();

        	$add = $this->faqs->ekle($pdata);
        	if ($add) {
        		$data['message'] = "Sık Sorulan Sorulara Eklendi";
        		$data['status'] = "success";
        	} else {
        		$data['message'] = "Sık Sorulan Sorulara Eklenemedi";
        		$data['status'] = "danger";
        	}
        }

        $this->parser->parse('add', $data);
    }

    public function edit($id = NULL)
    {
    	if ($id && is_numeric($id)) {
    		$soru = $this->faqs->sorular(array('id' => $id), TRUE);
    		if ($soru) {

    			$data = array();
		        $data['theme_url'] = $this->theme_url;
		        $data['kategoriler'] = $this->faqs->kategoriler();
		        if ($this->input->method() == "post") {
		        	$pdata = $this->input->post();

		        	$add = $this->faqs->duzenle($pdata, $id);
		        	if ($add) {
		        		$data['message'] = "Sık Sorulan Sorularda Düzenlendi";
		        		$data['status'] = "success";
		        		$soru = $this->faqs->sorular(array('id' => $id), TRUE);
		        	} else {
		        		$data['message'] = "Sık Sorulan Sorularda Düzenlenemedi";
		        		$data['status'] = "danger";
		        	}
		        }
		        $data['soru'] = $soru;
		        $this->parser->parse('edit', $data);

    		} else {
    			redirect('faqs/index');
    		}
    	} else {
    		redirect('faqs/index');
    	}
    }

    public function remove($id = NULL)
    {
    	if ($id && is_numeric($id)) {
    		$this->faqs->sil($id);
    	}
    	redirect($_SERVER['HTTP_REFERER']);
    }

}

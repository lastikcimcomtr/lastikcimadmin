<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sayac extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Sayac_model', 'sayac');
    }

    public function index()
    {
        if ($this->input->method() == "post") {
            $sdata =$this->input->post();
            foreach ($sdata as $key => $value) {

                $sdata[$key] = str_replace("@", "1at1", $value);
                if((is_numeric($value) && $value == 0) || $value == 'null' || $value == null ){
                    unset($sdata[$key]);
                }
                if(is_array($value)){     
                    $sdata[$key] =  implode ("-",$value);     
                }
            }
            $url = $this->uri->assoc_to_uri($sdata);
            redirect(site_url("sayac/index/" . $url));
        }else{
            $sdata  = $this->uri->uri_to_assoc();
            foreach ($sdata as $key => $value) {
                $value = urldecode($value);
                if( $key == 'tarih' ){
                    $sdata[$key] =  explode("-",$value);
                }
            }

        }

        $temizdata = $sdata;
   
        $sdata['order_by'] = "ziyaret desc";
        //$sdata['stok'] = 0;
        //$sdata['class'] = "products";
        $sdata['url '] = "lastikcim.com.tr";
        //$sdata['url not like'] = "%/sayfa/%";
        $toplamsatir =   $this->sayac->sayac($sdata,true);
        

        $this->load->library('pagination');
        $this->load->config('pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $toplamsatir->sayfa ;
     
        unset($temizdata['sayfa']);
        foreach ($temizdata as $key => $value) {
            if(is_array($value)){
                $temizdata[$key] =  implode ("-",$value);
            }

        }
        $config['base_url']         =  rtrim(site_url("sayac/index/" . $this->uri->assoc_to_uri($temizdata)) ,"/") . "/sayfa/";
        if($sdata['sayfa'] == 1)
            $config['uri_segment'] = $this->uri->total_segments() + 1;

        $config['first_url'] = rtrim(site_url("sayac/index/" . $this->uri->assoc_to_uri($temizdata)) ,"/") ;

        $this->pagination->initialize($config);
        $sdata['limit'] = $config['per_page'];



        $sdata['group_by'] = "url";

       
        $data = $sdata;
        $data['theme_url'] = $this->theme_url;   
        $data['veriler'] = $this->sayac->sayac($sdata); 
        $data['toplam'] = $toplamsatir; 
        $this->parser->parse("index", $data);
    }

    public function banner_sayac()
    {
        if ($this->input->method() == "post") {
            $sdata =$this->input->post();
            foreach ($sdata as $key => $value) {

                $sdata[$key] = str_replace("@", "1at1", $value);
                if((is_numeric($value) && $value == 0) || $value == 'null' || $value == null ){
                    unset($sdata[$key]);
                }
                if(is_array($value)){     
                    $sdata[$key] =  implode ("-",$value);     
                }
            }
            $url = $this->uri->assoc_to_uri($sdata);
            redirect(site_url("sayac/banner_sayac/" . $url));
        }else{
            $sdata  = $this->uri->uri_to_assoc();
            foreach ($sdata as $key => $value) {
                $value = urldecode($value);
                if( $key == 'tarih' ){
                    $sdata[$key] =  explode("-",$value);
                }
            }

        }

        $temizdata = $sdata;
   
        $sdata['order_by'] = "ziyaret desc";
        //$sdata['stok'] = 0;
        //$sdata['class'] = "products";
        $sdata['url '] = "lastikcim.com.tr";
        //$sdata['url not like'] = "%/sayfa/%";
        $toplamsatir =   $this->sayac->banner_sayac($sdata,true);
        

        $this->load->library('pagination');
        $this->load->config('pagination');
        $config = $this->config->item('pagination');
        $config['total_rows'] = $toplamsatir->sayfa ;
     
        unset($temizdata['sayfa']);
        foreach ($temizdata as $key => $value) {
            if(is_array($value)){
                $temizdata[$key] =  implode ("-",$value);
            }

        }
        $config['base_url']         =  rtrim(site_url("sayac/banner_sayac/" . $this->uri->assoc_to_uri($temizdata)) ,"/") . "/sayfa/";
        if($sdata['sayfa'] == 1)
            $config['uri_segment'] = $this->uri->total_segments() + 1;

        $config['first_url'] = rtrim(site_url("sayac/banner_sayac/" . $this->uri->assoc_to_uri($temizdata)) ,"/") ;

        $this->pagination->initialize($config);
        $sdata['limit'] = $config['per_page'];



        $sdata['group_by'] = "url";

       
        $data = $sdata;
        $data['theme_url'] = $this->theme_url;   
        $data['veriler'] = $this->sayac->banner_sayac($sdata); 
        $data['toplam'] = $toplamsatir; 
        $this->parser->parse("index_banner", $data);
    }




}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sayac_model extends CI_Model {

	
	function __construct() {
		parent::__construct();	
	}
	
	public function sayac($sdata = NULL, $row = FALSE){
		$this->db->select("count(*) ziyaret ,count(distinct url) sayfa ,count(distinct session_id) oturum , sayac.* ");
		$this->db->from("sayac");
		if($sdata['group_by']){
			$this->db->group_by($sdata['group_by']);
			unset($sdata['group_by']);
		}		
		if($sdata['order_by']){
			$this->db->order_by($sdata['order_by']);
			unset($sdata['order_by']);
		}

if (isset($sdata['limit']) && is_numeric($sdata['limit'])) {
			$limit = $sdata['limit'];
			if(isset($sdata['sayfa']))
				$sayfa = $sdata['sayfa'];
			else
				$sayfa = 1;
			unset($sdata['limit']);
			unset($sdata['sayfa']);
			
			$this->db->limit($limit,($sayfa - 1) * $limit);
		}
	unset($sdata['sayfa']);
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {				
				if(is_numeric($value)){
					$this->db->where($key, $value);
				}elseif(is_array($value)){
					$startdate = strtotime($value[0]);
					$enddate = strtotime($value[1]);
					$enddate = $enddate+ 86400;
					
					$this->db->where($key . ' >=', $startdate );				
					$this->db->where($key . ' <=', $enddate );
				}else{
					if (strpos($key, 'not like') !== false) {
						$this->db->where($key ,$value );
					}else{
						$this->db->where($key . ' like', "%". $value."%" );
					}
					
				}
				
			}
		}
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}

	
	public function banner_sayac($sdata = NULL, $row = FALSE){
		$this->db->select("count(*) ziyaret ,count(distinct url) sayfa ,count(distinct session_id) oturum , sayac.* ");
		$this->db->group_start();
		$this->db->where('param not like','%/#%');
		$this->db->where('param like','%targets%');
		$this->db->group_end();
		$this->db->from("sayac");
		if($sdata['order_by']){
			$this->db->order_by($sdata['order_by']);
			unset($sdata['order_by']);
		}

		$this->db->group_by('param');		
		if (isset($sdata['limit']) && is_numeric($sdata['limit'])) {
			$limit = $sdata['limit'];
			if(isset($sdata['sayfa']))
				$sayfa = $sdata['sayfa'];
			else
				$sayfa = 1;
			unset($sdata['limit']);
			unset($sdata['sayfa']);
			
			$this->db->limit($limit,($sayfa - 1) * $limit);
		}
		unset($sdata['sayfa']);
		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}
		return $res;
	}


}

?>
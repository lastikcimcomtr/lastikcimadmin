<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}

	private function codeigniter_escape_str($str, $like = FALSE)
	{
	    if (is_array($str))
	    {
	        foreach ($str as $key => $val)
	        {
	            $str[$key] = $this->escape_str($val, $like);
	        }

	        return $str;
	    }

	    if (function_exists('mysqli_real_escape_string') AND is_object($this->conn_id))
	    {
	        $str = mysqli_real_escape_string($this->conn_id, $str);
	    }
	    else
	    {
	        $str = addslashes($str);
	    }

	    // escape LIKE condition wildcards
	    if ($like === TRUE)
	    {
	        $str = str_replace(array('%', '_'), array('\\%', '\\_'), $str);
	    }

	    return $str;
	}
	
	public function gruplar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('ayarlar_gruplari');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function ayarlar($sdata = NULL, $row = FALSE)
	{
		$this->db->select('*');
		$this->db->from('ayarlar');
		if (count($sdata) > 0) {
			foreach ($sdata as $key => $value) {
				$this->db->where($key, $value);
			}
		}

		if ($row) {
			$res = $this->db->get()->row();
		} else {
			$res = $this->db->get()->result();
		}

		return $res;
	}

	public function odemetipleri(){
		$this->db->select('*');
		$this->db->from('odeme_tipleri');
		$this->db->where('durumu', 1);
		$this->db->order_by('sirano');
		return $this->db->get()->result();
	}

	public function siparisDurumlari(){
		$this->db->select('*');
		$this->db->from('siparis_durumlari');
		return $this->db->get()->result();
	}

	public function siparisDurumKaydet($durumID = NULL, $data = NULL)
	{
		$row = $this->db->query("select id from ayarlar where name = 'siparisMailText-".$durumID."'")->row();
		if($row){
			$upd = $this->db->query("update ayarlar set value = '".$this->codeigniter_escape_str(json_encode($data))."' where id = '".$row->id."'");
			if($upd){
				return array("status" => "success", "message" => "Mail Metin Ayarları Güncellendi");
			}else{
				return array("status" => "warning", "message" => "Mail Metin Ayarları Güncellenemiyor");
			}
		}else{
			$upd = $this->db->query("insert into ayarlar set value = '".$this->codeigniter_escape_str(json_encode($data))."', name = 'siparisMailText-".$durumID."'");
			if($upd){
				return array("status" => "success", "message" => "Mail Metin Ayarları Güncellendi");
			}else{
				return array("status" => "warning", "message" => "Mail Metin Ayarları Güncellenemiyor");
			}
		}
	}

	public function getSiparisData($durumID = NULL)
	{
		return $this->db->query("select * from ayarlar where name = 'siparisMailText-".$durumID."'")->row();
	}

	public function bankalar(){
		$this->db->select('*');
		$this->db->from('banka_bilgileri');
		return $this->db->get()->result();
	}

	public function  bankadelete($data){

		$this->db->where('id',$data['id']);
		$this->db->delete('banka_bilgileri');

		return "Banka silindi";
	}

	public function bankaedit($data){
		$this->db->where('id',$data['id']);
		$this->db->update('banka_bilgileri', $data);
		return "Banka Güncellendi";
	}
	
	public function bankaadd($data){
		$this->db->insert('banka_bilgileri', $data);
		return "Banka Eklendi";
	}
	public function sanalposlar(){
		
		$this->db->select('*');
		$this->db->from('sanalposlar');
		return $this->db->get()->result();

	}

	public function postipleri(){
		$this->db->select('*');
		$this->db->from('pos_tipleri');
		return $this->db->get()->result();
	}

	public function posoranset($data){
		$id = $data['id'];
		unset($data['id']);
		$oranlar = json_encode($data);
		$this->db->where('id', $id);
		if($this->db->update('sanalposlar', array('oranlar'=>$oranlar))){
			return "Sanal Pos Oranları Güncellendi..";
		}else{
			return "Sanal Pos Oranları Güncellenemiyor. Sistem yöneticisi ile iletişime geçiniz.";
		}

	}
	public function posdurumset($data){
		if($data['durum'] == "ana_pos"){
			$this->db->where('id <>', 0);
			$this->db->update('sanalposlar', array('ana_pos'=>'0'));
		}

		$this->db->where('id', $data['id']);
		if($this->db->update('sanalposlar', array($data['durum']=>$data['islem']))){
			return "Sanal Pos Oranları Güncellendi..";
		}else{
			return "Sanal Pos Oranları Güncellenemiyor. Sistem yöneticisi ile iletişime geçiniz.";
		}


	}

	public function posedit($data){
		$data['data'] = json_encode($data['data']);
		$this->db->where('id', $data['id']);
		if($this->db->update('sanalposlar', $data)){
			return "Sanal Pos Güncellendi..";
		}else{
			return "Sanal Pos Güncellenemiyor. Sistem yöneticisi ile iletişime geçiniz.";
		}
	}


	public function poseditaciklama($data){
		$data['data'] = json_encode($data['data']);
		if($data['pos_aciklama_kisa']){
			$this->db->where('id', $data['id']);
			$this->db->update('sanalposlar', array('pos_aciklama_kisa'=>$data['pos_aciklama_kisa']));
		}
        if($data['kampanya_min_tutar']){
            $this->db->where('id', $data['id']);
            $this->db->update('sanalposlar', array('kampanya_min_tutar'=>$data['kampanya_min_tutar']));
        }
		$this->db->where('id', $data['id']);
		if($this->db->update('sanalposlar', array('pos_aciklama'=>$data['pos_aciklama']))){
			return "Sanal Pos Açıklama Güncellendi..";
		}else{
			return "Sanal Pos Açıklama Güncellenemiyor. Sistem yöneticisi ile iletişime geçiniz.";
		}
	}

	public function posadd($data){
		$data['data'] = json_encode($data['data']);
		if($this->db->insert('sanalposlar', $data)){
			return "Sanal Pos Eklendi..";
		}else{
			return "Sanal Pos Eklenemedi. Sistem yöneticisi ile iletişime geçiniz.";
		}

	}

	public function spsil($id){
		$this->db->where('id',$id);
		$this->db->delete('sanalposlar');	

	}

	public function ayarlariguncelle($data = NULL)
	{
		$parametreler = $data['parametreler'];
		if ($parametreler && count($parametreler) > 0) {
			foreach ($parametreler as $parametre) {
				$name 	= $parametre;
				$value 	= $data[$parametre];
				if (empty($value) || !isset($value) || is_null($value)) {
					// $value = 0;
					$value = NULL;
				}
				$res = $this->extraservices->ayarla($name, $value);
			}
			return $res;
		} else {
			return FALSE;
		}
	}

	public function editspimage($id = NULL)
	{
		if ($id && is_numeric($id)) {
		
				$data 						= array();
				if (strlen($_FILES['resim']['name']) > 4) {
					$resim_dizin 				= $this->extraservices->ayarlar(array('name' => 'sanalpos_resim'), TRUE)->value;
					$upload_res 				= (object)($this->extraservices->single_file_upload('.'.$resim_dizin, 'resim'));
					$data['resim'] 				= $upload_res->image;
					$this->db->where('id', $id);
					$res = $this->db->update('sanalposlar', $data);
					return $res;
				} else {
					return FALSE;
				}
			
		} else {
			return FALSE;
		}
	}
}

?>
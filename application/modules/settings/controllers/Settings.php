<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller  {


    public $theme_url;
    public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Settings_model', 'settings');
    }

    public function group($id = NULL)
    {
        if ($id && is_numeric($id)) {
            $grup = $this->settings->gruplar(array('id' => $id), TRUE);
            if ($grup) {

                $data = array();
                $data['grup']       = $grup;

                if ($this->input->method() == "post") {
                    $pdata = $this->input->post();
                    $upres = $this->settings->ayarlariguncelle($pdata);
                    if ($upres) {
                        $data['status'] = "success";
                        $data['message'] = "Ayarlar başarılı bir şekilde güncellendi.";
                    } else {
                        $data['status'] = "danger";
                        $data['message'] = "Ayarlar güncellenemedi.";
                    }
                }

                $data['ayarlar']    = $this->settings->ayarlar(array('group' => $grup->id));
                $data['theme_url']  = $this->theme_url;

                $this->parser->parse("group", $data);

            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    public function payments(){

        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $upres = $this->settings->ayarlariguncelle($pdata);
            if ($upres) {
                $data['status'] = "success";
                $data['message'] = "Ayarlar başarılı bir şekilde güncellendi.";
            } else {
                $data['status'] = "danger";
                $data['message'] = "Ayarlar güncellenemedi.";
            }
        }
        $data['odemetipleri']  = $this->settings->odemetipleri(); 
        $data['bankalar']  = $this->settings->bankalar(); 
        $data['sanalposlar']  = $this->settings->sanalposlar();
        $data['ayarlar']    = $this->settings->ayarlar(array('group' => 12));
        $data['theme_url']  = $this->theme_url;
           // print_r($data);
        $this->parser->parse("payments", $data);
    }

    public function mailtext(){
        if ($this->input->method() == "post") {
            $data = $this->settings->siparisDurumKaydet($this->input->post("durumID"), $this->input->post());
            $data["updTab"] = $this->input->post("durumID");
        }else{
            $data = array();
            $data["updTab"] = "1";
        }
        $data['theme_url']  = $this->theme_url;
        $data['siparisDurumlari'] = $this->settings->siparisDurumlari();
        $this->parser->parse("mailtext", $data);
    }

    public function spsil ($id){
        $this->settings->spsil($id);
        redirect(site_url('settings/payments'));
    }
    public function editspimage($id = NULL)
    {
        if ($this->input->method() == "post") {
            
            if ($id && is_numeric($id)) {
                $this->settings->editspimage($id);
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                redirect(site_url('settings/payments'));
            }

        } else {
             redirect(site_url('settings/payments'));
        }
    }
}

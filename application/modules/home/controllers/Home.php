<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Home_model', 'home');
    }

    public function index()
    { 
		$this->load->model('orders/Orders_model', 'orders'); 
        $data = array();
		$sdata = array('toplam' => 1);
		$sdata['durumu'] = 2;
        $total_onay_bekleyen = $this->orders->orders($sdata,true)->toplam;
        $data['total_onay_bekleyen'] = ['adet' => $total_onay_bekleyen , 'link' => '/orders/index/durumu/2'];
        $sdata['durumu'] = [6,8];
        $kargoya_verilmeyen = $this->orders->orders($sdata,true)->toplam;
        $data['kargoya_verilmeyen'] = ['adet' => $kargoya_verilmeyen , 'link' => '/orders/index/durumu/6-8'];

        $sdata['durumu'] = 11;
        $iptal_talebi = $this->orders->orders($sdata,true)->toplam;
        $data['iptal_talebi'] =  ['adet' => $iptal_talebi , 'link' => '/orders/index/durumu/11'];

        $sdata['durumu'] = 3;
        $kargoya_verildi = $this->orders->orders($sdata,true)->toplam;
        $data['kargoya_verildi'] = ['adet' => $kargoya_verildi , 'link' => '/orders/index/durumu/3'];

        $sdata['durumu'] = [8];
        $sdata['tarih_max'] = date_create(date('Y-m-d'))->modify('-3 days')->format('d.m.Y');
        $geciken_siparis_1 = $this->orders->orders($sdata,true)->toplam;
        $data['geciken_hazirlaniyor'] = ['adet' => $geciken_siparis_1 , 'link' => '/orders/index/durumu/8/tarih_max/'.$sdata['tarih_max']];
        $sdata['durumu'] = [6];
        $sdata['tarih_max'] = date_create(date('Y-m-d'))->modify('-7 days')->format('d.m.Y');
        $geciken_siparis_2 = $this->orders->orders($sdata,true)->toplam;
        $data['geciken_tedarik'] = ['adet' => $geciken_siparis_2 , 'link' => '/orders/index/durumu/6/tarih_max/'.$sdata['tarih_max']];



        $sdata['durumu'] = 3;
        $sdata['tarih_max'] = date_create(date('Y-m-d'))->modify('-5 days')->format('d.m.Y');
        $kargoda_bekleyen = $this->orders->orders($sdata,true)->toplam;
        $data['kargoda_bekleyen'] = ['adet' => $kargoda_bekleyen , 'link' => '/orders/index/durumu/3/tarih_max/'.$sdata['tarih_max']];

        /*
		$total_onay_bekleyen = $this->orders->orders($sdata,true)->toplam;
		$data['total_siparis_day'] = $this->orders->hepsitoplam(1);
		$data['total_siparis_month'] = $this->orders->hepsitoplam(2);
		$data['total_siparis'] = $this->orders->hepsitoplam(4);
		$data['yeni_musteri_day'] = $this->home->yenimusteri(1);
		$data['yeni_musteri_week'] = $this->home->yenimusteri();
		$data['yeni_musteri_total'] = $this->home->yenimusteri(7,true);
        $data['gunluk_siparis_tutari'] = $this->home->toplam_siparis_tutari(1);*/
/*        */


        $data['theme_url'] = $this->theme_url;
        
        $this->parser->parse("index", $data); 
    }
    public function arama_trend(){
        $data = array();
        $data['most_searched_empty'] = $this->home->top_searched_empty(20);
        $data['last_searched'] = $this->home->last_searches(20);
        $data['search_trends'] = array_chunk($this->home->search_trends(40), 20);
        $data['most_searched_keywords'] = $this->home->top_searched(20);

        $data['theme_url'] = $this->theme_url;
        $this->parser->parse("arama_trend", $data); 
    }

    public function iletisim(){  
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
		  foreach ($pdata as $key => $value) {
            if (is_null($value) || strlen($value) < 1) {
                unset($pdata[$key]);
            }
        }        
        if (count($pdata) > 0) {
            $surl = ($this->uri->assoc_to_uri($pdata));
            $surl = site_url('home/iletisim/'.$surl);
            redirect($surl);
            exit();
        }
    }
	  $sdata = $this->uri->uri_to_assoc();
    $pagination_url = site_url('home/iletisim/'.$this->uri->assoc_to_uri($sdata));
	if(!$sdata['page']){
		$sdata['page'] = 1;
	}
	if(!$sdata['limit']){
		$sdata['limit'] = 30;
	}
    $data = array();
    $data['theme_url'] = $this->theme_url;   
    $data['istekler'] = $this->home->iletisimistekleri($sdata); 
		
	$last_uri = $this->uri->total_segments();
    $last_uri = $last_uri;
    if ($sdata['page']) {
        $last_uri = $last_uri;
    } else {
        $last_uri = $last_uri + 2;
    }
	    $config = array();
    $config["base_url"]         = $pagination_url;
    $config["total_rows"]       = $this->home->iletisimistekleri_count($sdata);
    $config["cur_page"]         = $sdata['page'];
    $config["per_page"]         = $sdata['limit'];
    $config['prefix']           = "page/";
    $config['use_page_numbers'] = true;
    $config['num_links']        = 5;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_tag_open']   = '<li class="page-first">';
    $config['first_tag_close']  = '</li>';
    $config['last_tag_open']    = '<li class="page-last">';
    $config['last_tag_close']   = '</li>';
    $config['prev_tag_open']    = '<li class="page-pre">';
    $config['prev_tag_close']   = '</li>';
    $config['next_tag_open']    = '<li class="page-next">';
    $config['next_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="page-number active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-number">';
    $config['num_tag_close']    = '</li>';
    $config['last_link']        = "Son Sayfa";
    $config['first_link']       = "İlk Sayfa";
    $config['uri_segment']      = $last_uri;
    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $data["links"] = $this->pagination->create_links();
	$data['sdata'] = $sdata;
	
    $this->parser->parse("iletisim", $data);
    
}
public function iletisim_talep_gonder(){
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
		$res = $this->home->iletisimtalebi_update($pdata);
		return $res;
	}
}

public function iletisim_talep_toggle(){
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        $res = $this->home->iletisimtalebi_toggle($pdata);
        echo $res;
    }
}

public function entegrasyondurum()
{
    /**/
    $this->load->model('entegrasyon/Entegrasyon_model', 'entegrasyon');
    $sdata = array();
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        $sdata = $pdata;
    }
    /**/
    $data = array();
    $data['sdata'] = $sdata;
    $data['theme_url'] = $this->theme_url;   
    $data['sonurunler'] = $this->entegrasyon->guncellenenler($sdata); 
    $this->parser->parse("entegrasyondurum", $data);
}

    public function login()
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        $kontrol = 'N';
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            if($pdata['dogrulamakod']){
                $login = $this->home->login($pdata);
                switch ($login) {
                    case 'YES':
                        redirect('/');
                    break;
                    
                    default:
                        $data['message']    = "Giriş Yapılamıyor.<br />Bilgileriniz Kontrol Edip Tekrar Deneyiniz.";
                        $data['status']     = "danger";
                    break;
                }
            }else{
                $kontrol = $this->kontrol($pdata);
                if($kontrol != "YES"){
                    redirect('/');
                }
            }
            //$this->session->set_userdata($pdata);
            /**/
        }
        if($kontrol == "YES"){
            $data = $pdata;
            $data['theme_url'] = $this->theme_url;
            $this->parser->parse("dogrulama", $data);
        }else{
            $this->parser->parse("login", $data);
        }
    }

    public function kontrol($data){
        $this->db->select('ce.*');
        foreach ($data as $key => $value) {
            if($key!="sifre")
            $this->db->where('ce.'.$key, $value);
        }       
        $this->db->from('kullanicilar as ce');
        $this->db->where('ce.durum', 1);
        $this->db->where('ce.sil', 0);
        $res = $this->db->get()->row();
        if(!password_verify($data['sifre'], $res->sifre)){
            return "NO";
        }
        if ($res) {
            $kod = $this->generateRandomString();
            $this->load->model('orders/Orders_model', 'orders'); 
            $update = $this->db->where('id',$res->id)->update('kullanicilar',['dogrulamakod' => $kod]);
            $this->orders->send_sms('KOD: '.$kod,[$res->gsm_no]);
            return "YES";
        }else {
                return "NO";
        }
    }
    public function test()
    {
        $parametreler = $this->csf->parametreler();
        $tablo_adi    = "urunler";
        $this->csf->tabloeksikmi($parametreler, $tablo_adi);
    }
    /*
    public function tatkotest() {
 	    $this->load->library('tatko');
 	    $res = ($this->tatko->aktarimlar(array('keydata' => 'MCH-BN-00751472')));
        echo $res;
        // var_dump($this->tatko->stokAraYeni(array('sto_kod' => 'MCH-KM-0492211'), TRUE));
    }
    */

    public function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function logout()
    {
        $this->home->logout();
        redirect(site_url('home/login'));
    }

    public function uploadImage(){
         ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        reset($_FILES);
        $temp = current($_FILES);

        if (is_uploaded_file($temp['tmp_name'])) {
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.1 400 Invalid file name,Bad request");
                return;
            }
            
            // Validating File extensions
            if (! in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array(
                "gif",
                "jpg",
                "png"
            ))) {
                header("HTTP/1.1 400 Not an Image");
                return;
            }
            
            $fileName = "uploads/images/" . $temp['name'];
            $path = $temp['name'];

            move_uploaded_file($temp['tmp_name'], $fileName);
            
            // Return JSON response with the uploaded file path.
            echo json_encode(array(
                'file_path' => $path,
                'location' => $path
            ));
        }
        die();
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller  {


    public $theme_url;
 	public function __construct()
    {
        parent::__construct();
        $this->theme_url = $this->parser->theme_url();
        $this->load->model('Home_model', 'home');
    }

    public function index()
    { 
		$this->load->model('orders/Orders_model', 'orders'); 
        $data = array();
		$sdata = array('toplam' => 1);
		$sdata['durumu'] = 1;
		$total_onay_bekleyen = $this->orders->orders($sdata,true)->toplam;
		$data['total_siparis_day'] = $this->orders->hepsitoplam(1);
		$data['total_siparis_month'] = $this->orders->hepsitoplam(2);
		$data['total_siparis'] = $this->orders->hepsitoplam(4);
		$data['total_onay_bekleyen'] = $total_onay_bekleyen;
		$data['yeni_musteri_day'] = $this->home->yenimusteri(1);
		$data['yeni_musteri_week'] = $this->home->yenimusteri();
		$data['yeni_musteri_total'] = $this->home->yenimusteri(7,true);
        $data['theme_url'] = $this->theme_url;
        $this->parser->parse("index", $data); 
    }

    public function iletisim(){  
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
		  foreach ($pdata as $key => $value) {
            if (is_null($value) || strlen($value) < 1) {
                unset($pdata[$key]);
            }
        }        
        if (count($pdata) > 0) {
            $surl = ($this->uri->assoc_to_uri($pdata));
            $surl = site_url('home/iletisim/'.$surl);
            redirect($surl);
            exit();
        }
    }
	  $sdata = $this->uri->uri_to_assoc();
    $pagination_url = site_url('home/iletisim/'.$this->uri->assoc_to_uri($sdata));
	if(!$sdata['page']){
		$sdata['page'] = 1;
	}
	if(!$sdata['limit']){
		$sdata['limit'] = 30;
	}
    $data = array();
    $data['theme_url'] = $this->theme_url;   
    $data['istekler'] = $this->home->iletisimistekleri($sdata); 
		
	$last_uri = $this->uri->total_segments();
    $last_uri = $last_uri;
    if ($sdata['page']) {
        $last_uri = $last_uri;
    } else {
        $last_uri = $last_uri + 2;
    }
	    $config = array();
    $config["base_url"]         = $pagination_url;
    $config["total_rows"]       = $this->home->iletisimistekleri_count();
    $config["cur_page"]         = $sdata['page'];
    $config["per_page"]         = $sdata['limit'];
    $config['prefix']           = "page/";
    $config['use_page_numbers'] = true;
    $config['num_links']        = 5;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_tag_open']   = '<li class="page-first">';
    $config['first_tag_close']  = '</li>';
    $config['last_tag_open']    = '<li class="page-last">';
    $config['last_tag_close']   = '</li>';
    $config['prev_tag_open']    = '<li class="page-pre">';
    $config['prev_tag_close']   = '</li>';
    $config['next_tag_open']    = '<li class="page-next">';
    $config['next_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="page-number active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li class="page-number">';
    $config['num_tag_close']    = '</li>';
    $config['last_link']        = "Son Sayfa";
    $config['first_link']       = "İlk Sayfa";
    $config['uri_segment']      = $last_uri;
    $this->load->library('pagination');
    $this->pagination->initialize($config);
    $data["links"] = $this->pagination->create_links();
	$data['sdata'] = $sdata;
    $this->parser->parse("iletisim", $data);
    
}

public function entegrasyondurum()
{
    /**/
    $this->load->model('entegrasyon/Entegrasyon_model', 'entegrasyon');
    $sdata = array();
    if ($this->input->method() == "post") {
        $pdata = $this->input->post();
        $sdata = $pdata;
    }
    /**/
    $data = array();
    $data['sdata'] = $sdata;
    $data['theme_url'] = $this->theme_url;   
    $data['sonurunler'] = $this->entegrasyon->guncellenenler($sdata); 
    $this->parser->parse("entegrasyondurum", $data);
}

    public function login()
    {
        $data = array();
        $data['theme_url'] = $this->theme_url;
        if ($this->input->method() == "post") {
            $pdata = $this->input->post();
            $login = $this->home->login($pdata);
            switch ($login) {
                case 'YES':
                    redirect('/');
                break;
                
                default:
                    $data['message']    = "Giriş Yapılamıyor.<br />Bilgileriniz Kontrol Edip Tekrar Deneyiniz.";
                    $data['status']     = "danger";
                break;
            }
        }
        $this->parser->parse("login", $data);
    }

    
    public function test()
    {
        $parametreler = $this->csf->parametreler();
        $tablo_adi    = "urunler";
        $this->csf->tabloeksikmi($parametreler, $tablo_adi);
    }
    /*
    public function tatkotest() {
 	    $this->load->library('tatko');
 	    $res = ($this->tatko->aktarimlar(array('keydata' => 'MCH-BN-00751472')));
        echo $res;
        // var_dump($this->tatko->stokAraYeni(array('sto_kod' => 'MCH-KM-0492211'), TRUE));
    }
    */
    public function logout()
    {
        $this->home->logout();
        redirect(site_url('home/login'));
    }

    /**/
    public function caritest()
    {
        /*
        $this->load->library('lastikcim');

        $chdata = array();
        $chdata['cari_unvan_1']         = "Deneme 1";
        $chdata['cari_unvan_2']         = "Deneme 2";
        $chdata['cari_vergi_dairesi']   = "Deneme Daire";
        $chdata['cari_vergi_numarasi']  = "Deneme No";
        $chdata['cari_email']           = "deneme@dgasd.com";
        $chres = $this->lastikcim->cariHesapOlustur($chdata);        
        
        if ($chres['status']) {
            echo "Cari Hesap Oluşturuldu ## ";
            $cadata = array();
            $cadata['cari_kod']     = $chres['kod'];
            $cadata['cadde']        = "cadde";
            $cadata['sokak']        = "sokak";
            $cadata['posta_kodu']   = "34920";
            $cadata['ilce']         = "Sultanbeyli";
            $cadata['il']           = "Istanbul";
            $cadata['ulke']         = "Türkiye";
            $cadata['ulke_kodu']    = "90";
            $cadata['bolge_kodu']   = "216";
            $cadata['tel_1']        = "3989808";
            $cadata['tel_2']        = "5382637785";
            $cares = $this->lastikcim->cariAdresOlustur($cadata);
            if ($cares['status']) {
                echo "Cari Adres Oluşturuldu";
            } else {
                echo "Cari Adres Oluşturulamadı";
            }
        } else {
            echo "Cari Hesap Oluşturulamadı";
        }

        $spdata = array();
        $spdata['cari_kod'] = "120.01.999";
        $spdata['cari_adres'] = "1";
        
        $spdata['urunler']      = array();
        $spdata['urunler'][]    = array('stok_birim' => '1', 'stok_tip' => '0', 'stok_kod' => 'GDY-BN-W-00532474', 'stok_fiyat' => '400', 'adet' => '4', 'stok_toplam' => '1600', 'stok_vergi' => '288');
        $spdata['urunler'][]    = array('stok_birim' => '1', 'stok_tip' => '0', 'stok_kod' => 'MRS-BN-W-02209203', 'stok_fiyat' => '950', 'adet' => '4', 'stok_toplam' => '3800', 'stok_vergi' => '684');

        $spdata['urunler'][]    = array('stok_birim' => '0', 'stok_tip' => '1', 'stok_kod' => '600.02.002', 'stok_fiyat' => '15', 'adet' => '4', 'stok_toplam' => '60', 'stok_vergi' => '10.8');
        $spdata['urunler'][]    = array('stok_birim' => '0', 'stok_tip' => '1', 'stok_kod' => '600.02.001', 'stok_fiyat' => '15', 'adet' => '4', 'stok_toplam' => '60', 'stok_vergi' => '10.8');

        var_dump($this->lastikcim->siparisOlustur($spdata));
        */

        /*
        $this->load->library('tatkosiparis');
        
        $spdata = array();
        $spdata['cari_kod'] = "120.34.00999";
        
        $spdata['urunler']      = array();
        $spdata['urunler'][]    = array('stok_kod' => 'GDY-BN-W-00532474', 'stok_fiyat' => '250', 'adet' => '4', 'stok_toplam' => '1000', 'stok_vergi' => '180');
        $spdata['urunler'][]    = array('stok_kod' => 'MRS-BN-W-02209203', 'stok_fiyat' => '500', 'adet' => '4', 'stok_toplam' => '2000', 'stok_vergi' => '360');

        $sipres = $this->tatkosiparis->siparisOlustur($spdata);
        var_dump($sipres);
        */
        /*
        $this->load->library('tatkosiparis');
        
        $spdata = array();        
        $spdata['urunler']      = array();
        $spdata['urunler'][]    = array('stok_kod' => 'GDY-BN-W-00532474', 'stok_fiyat' => '250', 'adet' => '4', 'stok_toplam' => '1000', 'stok_vergi' => '180');
        $spdata['urunler'][]    = array('stok_kod' => 'MRS-BN-W-02209203', 'stok_fiyat' => '500', 'adet' => '4', 'stok_toplam' => '2000', 'stok_vergi' => '360');

        $sipres = $this->tatkosiparis->siparisOlustur($spdata);
        var_dump($sipres);
        */
    }
    /**/

}

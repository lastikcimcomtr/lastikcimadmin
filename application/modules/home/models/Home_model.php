<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {
	
	function __construct() {
		parent::__construct();	
	}
	
	public function login($data = NULL)
	{
		if (count($data) > 0) { 
			$this->db->select('ce.*');
			foreach ($data as $key => $value) {
				if($key!="sifre")
				$this->db->where('ce.'.$key, $value);
			}		
			$this->db->from('kullanicilar as ce');
			$this->db->where('ce.durum', 1);
			$this->db->where('ce.sil', 0);

			$res = $this->db->get()->row();

	        if(!password_verify($data['sifre'], $res->sifre)){
	            return "NO";
	        }
			if ($res) {
				$this->session->set_userdata('login', $res);
				$this->session->set_userdata('login_time', time());
				$insert['kullanici'] =$res->id;
				$insert['ip'] =$_SERVER['REMOTE_ADDR'];
				$insert['tarih'] =time();
				$insert['modul_method'] ='home/login';
				$insert['aciklama'] = 'Kullanıcı Giriş Yaptı';
				$this->db->insert("kullanici_islem_kayitlari",$insert);
				$this->db->where('id', $res->id);
				$this->db->update("kullanicilar",array('sonip'=>$insert['ip'] ,'songiris'=>$insert['tarih']));				
				return "YES";
			} else {
				return "NO";
			}
		} else {
			return "BOS";
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('login');
		$this->session->unset_userdata('login_time');
	}
	public function hash_password($password){
	   return password_hash($password, PASSWORD_BCRYPT);
	}


	public function iletisimistekleri($data){
		$this->db->select('ii.*');
		$this->db->from('iletisim_istekleri ii');
		$this->db->not_like('ii.kategori','Ürün Talep');
		$this->db->not_like('ii.kategori','Fiyat Düşünce Haber');
		if($data['adi']){
			$this->db->where('ii.adi like \'%'.$data['adi'].'%\'');
		}
		if($data['soyadi']){
			$this->db->where('ii.soyadi like \'%'.$data['soyadi'].'%\'');
		}
	    $this->db->order_by('ii.tarih desc');
		$this->db->limit($data['limit'], (($data['page'] - 1) * $data['limit']));
		$res = $this->db->get()->result();
		return $res;
	}
	public function iletisimistekleri_count($data){
		$this->db->select('count(ii.id) adet');
		$this->db->from('iletisim_istekleri ii');
		$this->db->not_like('ii.kategori','Ürün Talep');
		$this->db->not_like('ii.kategori','Fiyat Düşünce Haber');
		if($data['adi']){
			$this->db->where('ii.adi like \'%'.$data['adi'].'%\'');
		}
		if($data['soyadi']){
			$this->db->where('ii.soyadi like \'%'.$data['soyadi'].'%\'');
		}
		return $this->db->get()->row()->adet;
	}
	public function iletisimtalebi_update($data){
			$iletisim_talebi = $this->db->select('*')->from('iletisim_istekleri')->where('id',$data['id'])->get()->row();
			$this->load->model('mail/Mail_model', 'mail');
			$sending = [];
			$sending['iletisim_talep'] = $iletisim_talebi;
			$sending['note'] = $data['note'];
			$save = [];
			$save['okundu'] = true;
			$save['note'] = $data['note'];
			$astx = $this->db->where('id',$data['id'])->update('iletisim_istekleri',$save);
			$html = $this->parser->parse('modules/mail/cevapver',$sending,true);
			$mail_status = $this->mail->htmlmail(array('email'=> $iletisim_talebi->email ,'title'=>'Lastikcim İletişim Talebiniz Hakkında', 'html'=> $html));	
	}
	public function iletisimtalebi_toggle($data){
		$talep = $this->db->where('id',$data['id'])->from('iletisim_istekleri')->get()->row();
		$deger = true;
		$note = null;
		if(is_null($talep->note)){
			$deger = false;
			$note = 'Manuel Onaylandı';
		}
		$astx = $this->db->where('id',$data['id'])->update('iletisim_istekleri',['okundu' => $deger , 'note' => $note]);
		return true;
	}	
	public function yenimusteri($days = 7,$all = false){
		$time = time();
		$time = $time - ($days * 60 * 60 * 24);
		$this->db->select('count(ms.id) as total');
		$this->db->from('musteri_hesaplari ms');
		if(!$all){
			$this->db->where('ms.kayit_tarihi >' , $time);
		}
		return $this->db->get()->row()->total;
	}
	public function top_searched($limit){
		$searches = $this->db->select('search,count(id) adet')->from('search_cache')->where('length(search) > 5')->group_by('search')->order_by('adet desc')->order_by('id desc')->limit($limit)->get()->result();
		return $searches;
	}

	public function top_searched_empty($limit){
		$searches = $this->db->select('search,count(id) adet')->from('search_cache')->where('length(search) > 5')->where('result','[]')->group_by('search')->order_by('adet desc')->order_by('id desc')->limit($limit)->get()->result();
		return $searches;
	}

	public function last_searches($limit){
		$searches = $this->db->select('ip,time, max(search) search')->from('search_cache')->group_by('ip')->order_by('time desc')->limit($limit)->get()->result();
		return $searches;
	}

	public function search_trends($limit){
		$searches = $this->db->select('*')->from('search_trends')->limit($limit)->get()->result();
		return $searches;
	}

	public function toplam_siparis_tutari($day){
		$after = time();
		$after -=  (3600 * 24 * $day);
		$siparisler = $this->db->select('*')->from('siparisler')->where('tarih >'.$after)->where_in('durumu',[2,3,5,8])->get()->result();
		$result = [];
		$toplam = 0;
		$result['sayi'] = sizeOf($siparisler);
		foreach ($siparisler as $key => $siparis) {
			$odemedata = json_decode($siparis->odemedata);
			if($odemedata->son_fiyat){
				$toplam = $toplam + (float)$odemedata->son_fiyat;
			}
		}
		$result['toplam'] = $toplam;
		return $result;
	}
}

?>